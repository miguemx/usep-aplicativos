<?php
namespace App\Entities;

use CodeIgniter\Entity;

class CCTS extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'id'        => null,
        'nombre'    => null,
        'sep'       => null,
        'calle'     => null,
        'numero'    => null,
        'estado'    => null,
        'municipio' => null,
        'localidad' => null,
        'colonia'   => null,
        'cp'        => null,
        'tipo'      => null,
        'nivel'     => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id'        => 'cct_cct',
        'nombre'    => 'cct_nombre',
        'sep'       => 'cct_tipo_sep',
        'calle'     => 'cct_calle',
        'numero'    => 'cct_num',
        'estado'    => 'cct_estado',
        'municipio' => 'cct_municipio',
        'localidad' => 'cct_localidad',
        'colonia'   => 'cct_colonia',
        'cp'        => 'cct_cp',
        'tipo'      => 'cct_tipo',
        'nivel'     => 'cct_nivel',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}