create view view_horario_kardex as

SELECT *  
FROM `aca_grupos_alumnos` 
INNER JOIN aca_grupos ON gpoalumno_grupo=grupo_id
INNER JOIN aca_periodos ON grupo_periodo=periodo_id
INNER JOIN aca_materias ON grupo_materia=materia_clave