<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Credencial extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'alumno' => null,
        'foto' => null,
        'identificacion' => null,
        'comentarioFoto' => null,
        'comentarioIdentificacion' => null,
        'valida' => null,
        'revisada' => null,
        'vigencia' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'alumno' => 'credencial_alumno',
        'foto' => 'credencial_foto',
        'identificacion' => 'credencial_identificacion',
        'comentarioFoto' => 'comentario_foto',
        'comentarioIdentificacion' => 'comentario_identificacion',
        'valida' => 'credencial_valida',
        'revisada' => 'credencial_revisada',
        'vigencia' => 'credencial_vigencia',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
