
ALTER TABLE `aca_grupos` ADD `grupo_lunes` VARCHAR(20) NULL AFTER `grupo_domingo_fin`, ADD `grupo_martes` VARCHAR(20) NULL AFTER `grupo_lunes`, ADD `grupo_miercoles` VARCHAR(20) NULL AFTER `grupo_martes`, ADD `grupo_jueves` VARCHAR(20) NULL AFTER `grupo_miercoles`, ADD `grupo_viernes` VARCHAR(20) NULL AFTER `grupo_jueves`, ADD `grupo_sabado` VARCHAR(20) NULL AFTER `grupo_viernes`, ADD `grupo_domingo` VARCHAR(20) NULL AFTER `grupo_sabado`, ADD `grupo_max` INT NULL COMMENT 'MAximo de ocupantes por gruop' AFTER `grupo_domingo`, ADD `grupo_ocupado` INT NULL COMMENT 'lugares ocupados en el grupo' AFTER `grupo_max`;

ALTER TABLE `aca_grupos`
  DROP `grupo_lunes_inicio`,
  DROP `grupo_lunes_fin`,
  DROP `grupo_martes_inicio`,
  DROP `grupo_martes_fin`,
  DROP `grupo_miercoles_inicio`,
  DROP `grupo_miercoles_fin`,
  DROP `grupo_jueves_inicio`,
  DROP `grupo_jueves_fin`,
  DROP `grupo_viernes_inicio`,
  DROP `grupo_viernes_fin`,
  DROP `grupo_sabado_inicio`,
  DROP `grupo_sabado_fin`,
  DROP `grupo_domingo_inicio`,
  DROP `grupo_domingo_fin`;
