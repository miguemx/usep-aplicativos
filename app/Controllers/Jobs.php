<?php

namespace App\Controllers;

use App\Models\RosModel;
use App\Models\AlumnosModel;
use App\Models\AlumnoGrupoHistorialModel;
use App\Models\GrupoModel;
use App\Models\GrupoAlumnoModel;
use App\Models\MateriaModel;
use App\Models\PagosAlumnosModel;
use App\Models\PeriodoModel;
use App\Models\ActaHistorial;
use App\Models\AspirantesGeneralesModel;
use App\Models\AspirantesRespuestasModel;
use App\Models\AulasModel;
use App\Models\AulasGrupoModel;
use App\Entities\Ros;
use App\Entities\GrupoAlumno;
use App\Entities\AulasGrupos;
use App\Models\CredencialModel;
use App\Helpers\RosFile;
use CodeIgniter\CLI\Console;
use PhpParser\Node\Stmt\Else_;

use App\Controllers\EstadisticasEvaluacionDocente;
use App\Libraries\GoogleClassroomLibrary;
use App\Libraries\PagosEstudiantes;

class Jobs extends BaseController
{
    private $alumnosModel;
    private $alumnoHistorialModel;
    private $periodoModel;
    private $grupoAlumnoModel;
    private $grupoModel;
    private $materiasModel;
    private $credencialModel;
    private $AulasModel;
    private $AulasGrupoModel;
    private $EstadisticasEvaluacionDocenteController;

    private $historialActa;

    private $alumnosgruposmodel;
    public function __construct()
    {
        $this->rosDir = WRITEPATH . 'pagos/';
        $this->periodoModel = new PeriodoModel();
        $this->historialActa = new ActaHistorial();
        $this->alumnosModel = new AlumnosModel();
        $this->alumnoHistorialModel = new AlumnoGrupoHistorialModel();
        $this->grupoAlumnoModel = new GrupoAlumnoModel();
        $this->grupoModel = new GrupoModel();
        $this->materiasModel = new MateriaModel();
        $this->credencialModel = new CredencialModel();
        $this->AulasModel = new AulasModel();
        $this->AulasGrupoModel = new AulasGrupoModel();
        $this->EstadisticasEvaluacionDocenteController = new EstadisticasEvaluacionDocente();
    }

    public function index()
    {
        echo "Jobs";
    }

    /**
     * procesa todos los archivos ROS subudos al servidor
     */
    public function procesaros($cotejar='0')
    {
        ini_set('memory_limit', '256M');
        echo $this->logdate() . "Iniciando proceso de archivos ROS... \n";
        $rosFile = new RosFile();
        $rosFile->getLista();
        echo $this->logdate() .$rosFile->totalArchivos. " Archivos encontrados. \n";
        $rosFile->procesa();
        if ( $cotejar == '1' ) $this->cotejaros();
        echo $this->logdate() . "Finalizado el procesamiento de archivos ROS \n";
    }

    /**
     * coteja todos los pagos reportados del ROS contra los registrados por los estudiantes
     */
    public function cotejaros() {
        ini_set('memory_limit', '256M');
        $rosModel = new RosModel();
        $pagosEstudiantes = new PagosEstudiantes();
        echo $this->logdate() . "Iniciando cotejo de pagos desde el ROS... \n";
        $noCotejados = $rosModel->findNoCotejados();
        foreach ( $noCotejados as $pagoRos ) {
            if ( $pagoRos->estatusServicio == 'OTORGADO' ) {
                $pagosEstudiantes->coteja( $pagoRos->referencia );
            }
        }
        echo $this->logdate() . "Finaliza cotejo de pagos desde el ROS... \n";
    }

    /**
     * coteja todos los pagos que están en estado EN PROCESO para buscar alguna referencia en el ROS
     */
    public function cotejapagos() {

    }
    

    /**
     * genera un archivo de log de salida de acuerdo a los errores reportados en un arreglo de mensajes
     * @param archivo el nombre del archivo original
     * @param errores el arreglo con los errores a reportar
     */
    private function reportaErrores($archivo, $errores)
    {
        $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . $archivo . '.log', "w");
        if ($file) {
            fwrite($file, " LISTADO DE ERRORES PARA EL ARCHIVO $archivo \n");
            foreach ($errores as $error) {
                fwrite($file, $error . "\n");
            }
            fclose($file);
        }
    }

    /**
     * devuelve la cadena a imprimir en las salidas de los scripts
     * @param type el tipo de información a mostrar.
     * @return string con la informacion de la fecha a poner
     */
    private function logdate($type = null)
    {
        $logdate = "[" . date("Y-m-d H:i:s") . "]";

        $logdate .= " - ";
        if (is_string($type)) $logdate .= "[$type] - ";
        return  $logdate;
    }
    /**
     * proceso para pasar alumnos de grupo - obtiene todos los alumnos que va mandando por ciclos a obtener listas regresa un arreglo 
     * @param alumnos arreglo de alumnos a procesar
     */
    public function procesoreinscripcion()
    {
        echo $this->logdate() . "Inicio del proceso de reinscripcion de alumnos \n";
        ini_set('memory_limit', '256M');
        $alumno = new AlumnosModel();
        $lista = $alumno->findssall();
        $contador = 0;
        foreach ($lista as $key) {
            $respuesta = $this->obtenerlistas($key->alumno_id, $contador, $key->alumno_semestre);
            if ($respuesta['status'] == 0) {
                $arr_errores[] = $respuesta['mensaje'];
            } else {
                $arr_insertados[] = $respuesta['mensaje'];
            }
            $contador++;
        }
        if (isset($arr_insertados)) {

            $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . 'alumnos_insertados' . '.log', "w");
            if ($file) {
                fwrite($file, " LISTADO INSERCIONES ALUMNOS INSERTADOS \n");
                foreach ($arr_insertados as $error) {
                    foreach ($error as $key) {
                        fwrite($file, $key . "\n");
                    }
                }
                fclose($file);
            }
        }
        if (isset($arr_errores)) {

            $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . 'alumnos_errores' . '.log', "w");
            if ($file) {
                fwrite($file, " LISTADO ERRORES EN LA INSERCION DE ALUMNOS \n");
                foreach ($arr_errores as $error) {
                    foreach ($error as $key) {
                        fwrite($file, $key . "\n");
                    }
                }
                fclose($file);
            }
        }
        echo $this->logdate() . "Fin del proceso de reinscripcion de alumnos \n <br>";
    }

    /**
     * regresa un mensaje con una clasificacion de estatus, si es 1 inserta, si es 0 agrega errores
     * 
     */
    public function obtenerlistas($id, $contador, $semestre)
    {
        $aux = 0;
        $datenow = date('Y-m-d H:i:s');
        $grupo = new AlumnoGrupoHistorialModel();
        $respuesta = $grupo->gethistoria($id);
        $bandera_insertado = 0;
        $status = 0;
        $mensaje = null;
        if ($respuesta) {
            $carrera = $respuesta[0]->carrera_id;
            $semestre_siguiente = $respuesta[0]->alumno_semestre + 1;
            $materia = new MateriaModel();
            $materia_historia = $materia->buscarsemestre($semestre_siguiente, $carrera);
            $arr_prerrequisitos_negadas = [];
            $arr_prerrequisitos_aprovado = [];
            $grupo_siguiente = "";
            $bandera_coorrequisito = 0;
            $bandera_grupo_correcto = 0;
            foreach ($materia_historia as $key) {
                if ($arr_series = json_decode($key->materia_seriacion, true)) {
                    foreach ($arr_series['prerrequisitos']  as $serie) {
                        foreach ($respuesta as $arr_datos) { //recorre todos los prerrequisitos 
                            $spl1 = str_split($arr_datos->grupo_idc); //separamos el idc para obtener la siguiente seccion
                            if (count($spl1) == 10) {
                                $aux = $grupo_siguiente;
                                $grupo_siguiente = ($spl1[7] + 1) . $spl1[8] . $spl1[9]; //pasamos la seccion al siguiente periodo solo si el idc cumple con la longitud de caracteres 
                                if ($grupo_siguiente < $aux) {
                                    $grupo_siguiente = $aux;
                                }
                                $bandera_seccion_siguiente = true;
                            } else {
                            }
                            if (isset($arr_series['correquisito'])) {
                                if ($bandera_coorrequisito == 0) {
                                    foreach ($arr_series['correquisito'] as $core) {
                                        if ($serie == $core) {
                                            $bandera_coorrequisito = 1;
                                            $arr_prerrequisitos_aprovado[] = $key->materia_clave;
                                            $arr_materias_siguienteperiodo[] = $key->materia_clave;
                                        }
                                    }
                                }
                            } else {
                                if ($arr_datos->materia_clave == $serie) {
                                    if ($arr_datos->aprobada == 1) {
                                        $arr_prerrequisitos_aprovado[] = $key->materia_clave;
                                        $arr_materias_siguienteperiodo[] = $key->materia_clave;
                                    }
                                } else {
                                    $arr_prerrequisitos_negadas[] = $key->materia_clave;
                                }
                            }
                        }
                    }
                } else {
                    $arr_materias_siguienteperiodo[] = $key->materia_clave;
                }
            }
            $grupos = new GrupoModel();
            if ($carrera == 1) {
                $buscar = "LEO-$grupo_siguiente";
            } else if ($carrera == 2) {
                $buscar = "MED-$grupo_siguiente";
                /* echo " $grupo_siguiente <br>"; */
                if ($grupo_siguiente == 201) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 202) {
                    $arr_materias_siguienteperiodo[] = "OPT-112";
                }
                if ($grupo_siguiente == 203) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 204) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 205) {
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if ($grupo_siguiente == 206) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 207) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 208) {
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if ($grupo_siguiente == 209) {
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if ($grupo_siguiente == 210) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 401) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 402) {
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if ($grupo_siguiente == 403) {
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if ($grupo_siguiente == 404) {
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if ($grupo_siguiente == 405) {
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if ($grupo_siguiente == 406) {
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if ($grupo_siguiente == 407) {
                    $arr_materias_siguienteperiodo[] = "OPT-112";
                }
                if ($grupo_siguiente == 408) {
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                $arr_materias_siguienteperiodo[] = "MC-TUT";
            }
            /* var_dump($arr_materias_siguienteperiodo);
            die(); */
            $arr_grupos = $grupos->buscargrupoclave($buscar, "2022B");     // llama a la funcion  buscargrupoclave que regresa los grupos disponibles para esa seccion disponibles en el siguiente periodo      
            if ($arr_grupos) {
                foreach ($arr_grupos as $arr_grupo) {
                    $bandera_grupo_correcto = 0;
                    foreach ($arr_materias_siguienteperiodo as $materias_siguientes) {
                        if ($arr_grupo->grupo_materia == $materias_siguientes) {
                            $grupo_alumnoEnt = new GrupoAlumno();
                            $grupo_alumnoModel = new GrupoAlumnoModel();
                            $grupo_alumnoEnt->alumno = $id;
                            $grupo_alumnoEnt->grupo = $arr_grupo->grupo_id;
                            $bandera_grupo_correcto = 1;
                            try {
                                $status = 1;
                                //inserta en el modelo la entidad
                                $grupo_alumnoModel->insert($grupo_alumnoEnt); //-------------------------------------------------------------------------------------------------------------------------------------------------------------
                                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Alumno insertado correctamente [grupo: $arr_grupo->grupo_id][materia: $materias_siguientes]";
                                //cambiar de semestre al alumno y deshabilitar la carga de materias para el 
                                $bandera_insertado = 1;
                            } catch (\Exception $ex) {
                                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Error al insertar en [grupo: $arr_grupo->grupo_id][materia: $materias_siguientes]";
                                //echo "Error en captura [alumno_id : $id] [grupo_id : $arr_grupo->grupo_id] ";
                            }
                        }
                    }
                    if ($bandera_grupo_correcto == 0) {
                        $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Alumno no puede ser registrado en el grupo por idc de grupo incorrecto, no hay coincidencias [grupo: $arr_grupo->grupo_id][materia: $arr_grupo->grupo_materia]";
                    }
                }
            } else {
                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> No se encontraron grupos para el periodo siguiente [clave: $buscar][periodo: 2022B][$buscar]";
            }
        } else {
            $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> No se encontraron registros de grupos al que pertenezca el alumno";
        }
        if ($bandera_insertado == 1) {
            $credencial = new CredencialModel();
            $findcred =  $credencial->find($id);
            if ($findcred) {
                $findcred->vigencia =  '07/2022';
                try {
                    $credencial->update($id, $findcred); //------------------------------------------------------------------------
                    $alumno_status[] = "[$datenow][alumno: $id]=> Se actualizo la vigencia de credencial";
                } catch (\Exception $ex) {
                    $alumno_status[] = "[$datenow][alumno: $id]=> No se pudo actualizar la vigencia de credencial";
                }
            }
            $arr_update = [
                'alumno_periodo ' => "2022B",
                'alumno_semestre' => ($semestre + 1)
            ];
            $alumno_update_semestre = new AlumnosModel();
            try {
                $alumno_update_semestre->updateperiodo($id, $arr_update); //---------------------------------------------------------
                $alumno_status[] = "[$datenow][alumno: $id]=> Alumno cambiado a periodo 2022B ";
            } catch (\Exception $ex) {
                $alumno_status[] = "[$datenow][alumno: $id]=> Alumno no pudo ser cambiado a periodo 2022B ";
            }
        }

        $log_proceso = [
            'status' => $status,
            'mensaje' => $alumno_status
        ];
        return $log_proceso;
    }

    /**
     * Esta funcion recorre la tabla de grupos para actualizar el folio de los grupos, las tutorias se verifican en null
     * 
     */
    public function updatefolio()
    {
        $periodo = $this->periodoModel->getPeriodoEstatus("CALIFICACIONES");
        $idperiodo = $periodo[0]->periodo_id;
        $folio = $this->historialActa->getid();
        $grupomodl = new GrupoModel();
        $resp = $grupomodl->getgrupos($idperiodo);
        $tutorias = $grupomodl->updategrupostut();
        $folio = $folio + 1;
        foreach ($resp as $key) {
            echo "$key->grupo_id : $key->grupo_periodo : $key->grupo_folio ---> $key->grupo_materia <br>";
            $id = $key->grupo_id;
            $arr_update = [
                'grupo_folio' => $folio
            ];
            /*             var_dump($arr_update);
            die(); */
            $grupomodl->updategrupo($id, $arr_update);
            $folio++;
        }
    }
    public function bajas()
    {
        echo $this->logdate() . "Proceso de bajas de alumnos iniciado... \n";
        $conta = 1;
        $arr_bajas = [];
        $alumno = new AlumnosModel();
        $lista = $alumno->findssall();
        foreach ($lista as $key) {
            $bandera = $this->excesodetalento($key->alumno_id, $conta);
            if ($bandera['status'] === true) {
                $arr_bajas[] = $bandera['mensaje'];
                $conta++;
            }
        }
        if ($arr_bajas) {
            $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . "Catalogo de Alumnos de Baja" . '.log', "w");
            if ($file) {
                fwrite($file, " LISTADO DE ALUMNOS ELIMINADOS POR EL PROCESO \n");
                foreach ($arr_bajas as $error) {
                    fwrite($file, $error . "\n");
                }
                fclose($file);
            }
        }
        echo $this->logdate() . "Proceso de bajas de alumnos finalizado [Revisa los Logs generados] \n";
    }
    public function excesodetalento($alumno, $conta)
    {
        $status = false;
        $bandera_baja = "";
        $arr_materias = [];
        $grupo = new AlumnoGrupoHistorialModel();
        $respuesta = $grupo->historialperiodo($alumno, '2021A');
        $contador = 0;
        $bandera_comentario = "";
        foreach ($respuesta as $historia) {
            if ($historia->gpoalumno_extraordinario != null) {
                $calificacion = $historia->gpoalumno_extraordinario;
            } else if ($historia->gpoalumno_ordinario === null) {
                $calificacion = 10;
            } else {
                $calificacion = $historia->gpoalumno_ordinario;
            }
            if ($calificacion <= 5) {
                $arr_materias[] = $historia->materia_clave;
                $contador++;
            }
        }
        $datenow2 = date('Y-m-d H:i:s');
        if ($contador >= 4) {
            $bandera_comentario = "Alumno con mas de 3 materias reprobadas en el periodo 2021A";
            $bandera_baja = "[$datenow2][$conta][alumno: $alumno]=> alumno con mas de 3 materias reprobadas";
        }
        $aux = 0;
        $historiacompleta = new AlumnoGrupoHistorialModel();
        $grupostotales = $historiacompleta->gethistoria($alumno);
        if ($arr_materias) {
            foreach ($arr_materias as $materia) {
                $aux = 0;
                foreach ($grupostotales as $key) {
                    if ($key->materia_clave == $materia && $key->aprobada == 0) {
                        $aux++;
                    }
                }
                if ($aux > 1) {
                    $bandera_comentario = "Alumno reprobado en su materia recursada";
                    $bandera_baja = "[$datenow2][$conta][alumno: $alumno]=> Alumno reprobó el recurso ";
                }
            }
        }

        if (isset($bandera_baja)) {
            if ($bandera_baja != null) {
                $alumnomodel =  new AlumnosModel();
                $datos = $alumnomodel->find($alumno);
                if ($datos) {
                    $datos->comentario = $bandera_comentario;
                    try {
                        $alumnomodel->update($alumno, $datos);
                        $alumnomodel->delete($alumno);
                        $status = true;
                    } catch (\Exception $ex) {
                        $status = true;
                        $bandera_baja = "[$datenow2][$conta][alumno: $alumno]=> Error al dar de baja al alumno [error:  $ex ]";
                    }
                }
            }
        }
        $log = [
            'status' => $status,
            'mensaje' => $bandera_baja
        ];
        return $log;
    }
    public function nuevosemestre()
    {
        $this->bajas();
        $this->procesoreinscripcion();
    }

    //----------------------------------------------------------v.2 proceso de reinscripcion ----------------------------
    /**
     * recorre el listado completo de alumnos y determina respecto a calificaciones si pueden continuar inscritos
     * 1.-verificar si reprobo mas de 3 materias en el semestre actual
     * 2.-Verificar si reprobo un recursamiento
     */
    public function alumnosReprobados()
    {
        //$lista_alumnos = $this->alumnosModel->getByEstatus('ACTIVO');
        $lista_alumnos = $this->alumnosModel->getByEstatusPeriodo('ACTIVO');
        $bajas = false;

        $periodo = ($this->periodoModel->getPeriodoEstatus('CALIFICACIONES'));
        if ($periodo) {
            $contador_bajas = 1;
            foreach ($lista_alumnos as $alumno) {
                $bandera_baja = false;
                $datenow2 = date('Y-m-d H:i:s');
                $materias_reprobadas = false;
                $contador_reprobadas = 0;
                $matrias_periodo_actual = $this->alumnoHistorialModel->historialperiodo($alumno->id, $periodo[0]->id);
                if ($matrias_periodo_actual) {
                    foreach ($matrias_periodo_actual as $materia) {
                        // $calificacion = (($materia->pago == 0) ? $materia->ordinario : $materia->extraordinario);
                        // echo "$alumno->id <br>";     
                        if ($materia->aprobada == 0) {
                            $materias_reprobadas[] = $materia->materia;
                            $contador_reprobadas++;
                        }
                    }
                    if ($contador_reprobadas > 3) {
                        $comentario =  "Alumno con mas de 3 materias reprobadas en el periodo " . $periodo[0]->id . "<br>";
                        $bandera = $this->eliminarAlumno($alumno->id, $comentario);
                        $bandera_baja = $bandera;
                        $bandera = (($bandera) ? ' Alumno con mas de 3 materias reprobadas' : 'Alumno con mas de 3 materias reprobadas [error => No se pudo eliminar al alumno]');
                        $bajas[] = "[$datenow2][$contador_bajas][alumno: $alumno->id]=>" . $bandera;
                        $contador_bajas++;
                    } else if ($materias_reprobadas && $bandera_baja == false) {
                        $historial = $this->alumnoHistorialModel->gethistoria($alumno->id);
                        $nombre_materia = '';
                        foreach ($materias_reprobadas as $clave_materia) {
                            if ($bandera_baja == false) {
                                $contador = 0;
                                foreach ($historial as $grupo) {
                                    if ($clave_materia == $grupo->materia) {
                                        if ($grupo->aprobada == 0) {
                                            $contador++;
                                            $nombre_materia = "$grupo->materia $grupo->mat_nombre";
                                        }
                                    }
                                }
                                if ($contador > 1) {
                                    $comentario =   "Alumno ha reprobado el recursamiento  $nombre_materia <br>";
                                    $bandera = $this->eliminarAlumno($alumno->id, $comentario);
                                    $bandera_baja = $bandera;
                                    $bandera = (($bandera) ? ' Alumno ha reprobado el recursamiento' : 'Alumno ha reprobado el recursamiento [error => No se pudo eliminar al alumno]');
                                    $bajas[] = "[$datenow2][$contador_bajas][alumno: $alumno->id]=>" . $bandera;
                                    $contador_bajas++;
                                }
                            }
                        }
                    }
                } else {
                    echo 'No se encontraron materias en este periodo <br>';
                }
            }
        } else {
            echo 'no hay periodo <br>';
        }
        if ($bajas) {
            $this->reportaErrores('Alumnos Expulsados', $bajas);
        }
        echo 'fin del proceso <br>';
    }


    /**
     * funcion para asignar califiaciones en aleatorio
     */
    public function fakerCalificaciones()
    {
        ini_set('memory_limit', '256M');
        $periodo = ($this->periodoModel->getPeriodoEstatus('CALIFICACIONES'));
        $lista_grupos = $this->grupoModel->getByPeriodo($periodo[0]->id);
        foreach ($lista_grupos as $grupo) {
            if ($grupo->captura == null) {
                $alumnos_grupo = $this->grupoAlumnoModel->getByGrupo($grupo->id);
                foreach ($alumnos_grupo as $alumno) {
                    if ($alumno->ordinario == null && $alumno->extraordinario == null) {
                        $bandera_calif = random_int(1, 2);
                        if ($bandera_calif == 1) {
                            $calificacion = random_int(5, 10);
                            $alumno->ordinario = $calificacion;
                            $alumno->aprobada = (($calificacion > 5) ? 1 : 0);
                            $this->grupoAlumnoModel->update($alumno->id, $alumno);
                        } else {
                            $calificacion = random_int(5, 10);
                            $alumno->extraordinario = $calificacion;
                            $alumno->aprobada = (($calificacion > 5) ? 1 : 0);
                            $this->grupoAlumnoModel->update($alumno->id, $alumno);
                        }
                    }
                }
                $grupo->captura = date('Y-m-d H:i:s');
                $this->grupoModel->update($grupo->id, $grupo);
            }
        }
        echo 'fin de proceso';
    }

    /**
     * funcion para mostrar el historial del alumno seleccionado
     * @param alumno ID del alumno
     */
    public function historialAlumno($alumno)
    {
        $historial = $this->alumnoHistorialModel->gethistoria($alumno);
        foreach ($historial as $grupo) {
            echo "$grupo->grupo $grupo->periodo $grupo->materia $grupo->mat_nombre $grupo->ordinario $grupo->extraordinario $grupo->aprobada <br>";
        }
    }

    /**
     * funcion para dar de baja a un alumno
     * @param id ID del alumno
     * @param comentarios motivo por el que se da la baja
     */
    public function eliminarAlumno($id, $comentario)
    {
        $alumno = $this->alumnosModel->find($id);
        try {
            $alumno->comentario = $comentario;
            $alumno->status = 'BAJA POR REGLAMENTO';
            $this->alumnosModel->update($id, $alumno);
            $this->alumnosModel->delete($id);
            log_message('info', 'Alumno dado de baja por ' . $comentario);
            return true;
        } catch (\Exception $ex) {
            log_message('info', "No se pudo eliminar el alumno $id [error:  $ex ]");
            return false;
        }
    }

    /**
     * recorre la lista de alumnos completos
     * @param archivo nombre del archivo a buscar
     */
    public function Reinscripccion($archivo = false)
    {
        ini_set('memory_limit', '256M');
        $url = ('/listas2022A/' . $archivo . ".csv");
        $listas_excel = $this->getListasCSV($url);
        // $limite = 0;
        $arr_registros = [];
        if ($listas_excel) {
            foreach ($listas_excel as $row) {
                // echo $row['alumno'] . " " . $row['seccion'] . "<br>";
                $alumno = $this->alumnosModel->find($row['alumno']);
                if ($alumno) {
                    if ($alumno->status == 'ACTIVO') {
                        $registro  = $this->RequisitosMaterias($row['alumno'], $row['seccion']);
                        if ($registro) {
                            $arr_registros[] = $registro;
                        }
                    } else {
                        $arr_registros[] =  "[" . date('Y-m-d H:i:s') . "][alumno: $alumno] Alumno con estatus de " . $alumno->status;
                    }
                }
            }
        }
        $archivo = 'Proceso de Reinscripccion de Alumnos ' . $archivo;
        $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . $archivo . '.log', "w");
        if ($file) {
            fwrite($file, " LISTADO DE ERRORES PARA EL ARCHIVO $archivo \n");
            foreach ($arr_registros as $datos) {
                foreach ($datos as $cadena) {
                    fwrite($file, $cadena . "\n");
                }
            }
            fclose($file);
            echo $this->logdate() . "Finalizado el procesamiento de archivos \n";
        }
    }

    /**
     * recorre las materias del alumno y verifica si cumple con los prerrequisitos o correquisitos
     * @param alumno ID del alumno
     * @param seccion ID de seccion a la que pertenecera el alumno
     */
    public function RequisitosMaterias($alumno, $seccion, $periodo = false)
    {
        if (!$periodo) {
            $periodo = ($this->periodoModel->getPeriodoEstatus('INICIO'));
            $periodo = $periodo[0]->id;
        }
        $datos_alumno = ($this->alumnosModel->find($alumno));
        $lista_grupos =  $this->grupoModel->getBySeccion($seccion, $periodo);
        $materia_semestre = $datos_alumno->semestre;
        $log_registros = false;
        $comentario = false;
        if ($lista_grupos) {
            foreach ($lista_grupos as $grupo) {
                $bandera_insersion = false;
                $materia = $this->materiasModel->find($grupo->materia);
                $materia_semestre = (($materia->semestre) ? $materia->semestre : $materia_semestre);
                if ($materia->serie) {
                    $serie =  json_decode($materia->serie, true);
                    if (isset($serie['prerrequisitos'])) {
                        foreach ($serie['prerrequisitos']  as $prerrequisitos) {
                            $lista_materia_cursada = $this->alumnoHistorialModel->getByMateria($alumno, $prerrequisitos);
                            if ($lista_materia_cursada) {
                                foreach ($lista_materia_cursada as $materia_lista) {
                                    if ($bandera_insersion == false) {
                                        if ($materia_lista->aprobada == 1) {
                                            $bandera_insersion = true;
                                            $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno][grupo: $grupo->id][MATERIA: $grupo->materia] " . $this->insertarAlumno($alumno, $grupo->id);
                                        } else {
                                            $bandera_insersion = false;
                                            $comentario = 'Requisito no aprobado';
                                        }
                                    }
                                }
                            } else {
                                if (isset($serie['correquisito'])) {
                                    foreach ($serie['correquisito']  as $correquisito) {
                                        if ($bandera_insersion == false) {
                                            if ($correquisito == $prerrequisitos) {
                                                $bandera_insersion = true;
                                                $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno][grupo: $grupo->id][MATERIA: $grupo->materia] " . $this->insertarAlumno($alumno, $grupo->id);
                                            } else {
                                                $bandera_insersion = false;
                                            }
                                        }
                                    }
                                } else {
                                    $comentario = "Materia no cursada, se niega el registro ";
                                }
                            }
                        }
                    }
                } else {
                    $comentario = 'sin prerrequisito insertar alumno';
                    $bandera_insersion = true;
                    $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno][grupo: $grupo->id][MATERIA: $grupo->materia] Materia sin prerrequisito, " . $this->insertarAlumno($alumno, $grupo->id);
                }
                if ($bandera_insersion == false) $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno][grupo: $grupo->id][MATERIA: $grupo->materia] " . $comentario;
            }
        } else {
            echo "no hay grupos disponibles";
        }
        // if ($bandera_insersion) {
        //     if ($this->cambiarAlumnoSemestre($alumno, $periodo,$materia_semestre)) {
        //         if ($this->altualizarCredencial($alumno, $periodo[0]->id)) {
        //             $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno] Se ha realizado la actualizacion del periodo, semestre y credencial del alumno";
        //         } else {
        //             $log_registros[] =  "[" . date('Y-m-d H:i:s') . "][alumno: $alumno] " . 'Hubo un problema a la hora de actualizar la credencial del alumno ';
        //         }
        //     } else {
        //         $log_registros[] = "[" . date('Y-m-d H:i:s') . "][alumno: $alumno] " . 'Hubo un problema a la hora de actualizar al estudiante al siguente periodo ' . $periodo[0]->id;
        //     }
        // }
        return $log_registros;
    }

    /**
     * funcion para cambiar de periodo y semestre al alumno
     * @param alumno ID del alumno
     * @param periodo ID del periodo 
     * @return true si logra actualizar al alumno al periodo
     * @return false si hubo un error al actualizar
     */
    public function cambiarAlumnoSemestre($alumno, $periodo, $materia_semestre)
    {
        $datos_alumno = $this->alumnosModel->find($alumno);
        try {
            $datos_alumno->semestre = $materia_semestre;
            $datos_alumno->periodo = $periodo;
            $this->alumnosModel->update($alumno, $datos_alumno);
            return true;
        } catch (\Exception $ex) {
            log_message('info', "No se pudo actualizar el alumno $datos_alumno->id [error:  $ex ]");
            return false;
        }
    }

    /**
     * actualiza la fecha de vigencia de la credencial
     * @param alumno ID del alumno
     * @param fecha fecha que se asigna como vigencia de la credencial
     * @return true si logra actualizar la credencial del alumno al periodo en inicio
     * @return false si hubo un error al actualizar
     */
    public function altualizarCredencial($alumno, $periodo)
    {
        $periodo = $this->periodoModel->find($periodo);
        $credencial = $this->credencialModel->find($alumno);
        // echo $credencial->vigencia;
        try {
            $datos = explode('-', $periodo->fin);
            $credencial->vigencia = $datos[1] . "/" . $datos[2];
            $this->credencialModel->update($alumno, $credencial);
            return true;
        } catch (\Exception $ex) {
            log_message('info', "No se pudo actualizar el alumno $alumno [error:  $ex ]");
            return false;
        }
    }


    /**
     * recorre el archivo csv y descompone en arreglo de id de alumnos
     */
    public function getListasCSV($archivo)
    {
        $row = 1;
        $seccion_arr = false;
        if (($handle = fopen(WRITEPATH . $archivo, "r")) !== FALSE) {
            $arr_alumnos = [];
            /*  $data = fgetcsv($handle, 1000, ",");
            $seccion =  explode(' ', $data[0]);
            $seccion_arr[] = $seccion[1]; */
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 1) {
                    $arr_alumnos[] = ['alumno' => $data[0], 'seccion' => $data[1]];
                }
                $row++;
            }
            fclose($handle);
            return $arr_alumnos;
        }
    }

    /**
     * Agrega el registro del aspirante en el grupo seleccionado
     * @param alumno ID del alumno
     * @param grupo ID del grupo
     * @return true si la eliminacion fue correcta
     * @return false si no se pudo realizar la eliminacion
     */
    public function insertarAlumno($alumno, $grupo)
    {
        if (!$this->grupoAlumnoModel->getRegistroAlumnoGrupo($alumno, $grupo)) {
            $grupo_alumnoEnt = new GrupoAlumno();
            $grupo_alumnoEnt->grupo = $grupo;
            $grupo_alumnoEnt->alumno = $alumno;
            try {
                $this->grupoAlumnoModel->insert($grupo_alumnoEnt);
                log_message('info', "Alumno $alumno insertado en el grupo $grupo");
                return "Alumno insertado en el grupo seleccionado";
            } catch (\Exception $ex) {
                log_message('info', "No se pudo insertar el alumno $alumno [error:  $ex ]");
                return "No se pudo insertar el alumno [error:  $ex ]";
            }
        } else {
            return 'Ya registrado anteriormente';
        }
    }


    public function listasEjemplo()
    {
        $listas = $this->alumnosModel->arrpruebas();
        foreach ($listas as $alumno) {
            echo "$alumno->id, $alumno->correo, $alumno->nombre <br>";
        }
    }

    /**
     * llena la tabla de asigna espacios físicos a las clases
     */
    public function asignaAulas()
    {
        $salones      = $this->AulasModel->getSelAulas();
        $periodo      = "2022A";
        $gruposinaula = $this->AulasGrupoModel->getbyperiodo($periodo);
        $totalsalones = count($salones);
        $id_sinAula   = array();
        $id_sinAct    = array();
        foreach ($gruposinaula as $sin_aula) {
            echo "entra al ciclo del conjunto de GRUPOS por asignar AULAS idc: $sin_aula->gpoaula_grupo con id: $sin_aula->gpoaula_id  <br>";
            $idreg        = $sin_aula->gpoaula_id;
            $reg2upd      = $this->AulasGrupoModel->find($idreg);
            $salonesRev   = 0;
            foreach ($salones as $aulas) {
                echo "entra al ciclo del conjunto de aulas por asignar y se revisa el aula: $aulas->aula_id<br>";
                $aula        = $aulas->aula_id;
                $dia         = $reg2upd->gpoaula_dia;
                $hora        = $reg2upd->gpoaula_inicio;
                $id          = $reg2upd->gpoaula_id;
                $idarray[]   = $reg2upd->gpoaula_id;
                $aulas_grupo = $this->AulasGrupoModel->getGrupoHoraDiaLugar($aula, $dia, $hora, $id, $periodo);
                $salonesRev++;
                if (count($aulas_grupo) == 0) {
                    $reg2upd->gpoaula_aula = $aulas->aula_id;
                    echo "El IDC: " . $reg2upd->gpoaula_grupo . "en el día " . $reg2upd->gpoaula_dia . " de las " . $reg2upd->gpoaula_inicio . " a las " . $reg2upd->gpoaula_fin . "se esta asignando al aula $aulas->aula_id ...<br>";
                    //$this->AulasGrupoModel->update($id, $reg2upd);
                    try {
                        $this->AulasGrupoModel->update($id, $reg2upd);
                    } catch (\Exception $ex) {
                        array_push($id_sinAct, $id);
                        log_message('info', "No se pudo actualizar el idc $reg2upd->gpoaula_grupo [error:  $ex ]");
                    }/**/
                    echo "<b> El IDC: " . $reg2upd->gpoaula_grupo . "en el día " . $reg2upd->gpoaula_dia . " de las " . $reg2upd->gpoaula_inicio . " a las " . $reg2upd->gpoaula_fin . "se asignó al aula $aulas->aula_id exitosamente... </b><br>";
                    break;
                } else {
                    if ($salonesRev == $totalsalones) {
                        echo "El IDC: " . $reg2upd->gpoaula_grupo . "en el día " . $reg2upd->gpoaula_dia . " de las " . $reg2upd->gpoaula_inicio . " a las " . $reg2upd->gpoaula_fin . "no se pudo asignar espacio <br>";
                        array_push($id_sinAula, $id);
                    }
                }
            }
            echo "sale del cliclo para el grupo $id con $salonesRev comparadas para asignar <br> con los siguientes valores: <br> ";
            echo "Aula: $aula día: $dia hora: $hora id de registro de aula: $id <br> ";
        }

        echo "los id de la tabla de horarios que no tienen espacio físico fueron: " . count($id_sinAula) . " y son las siguientes: <br> <pre>";
        print_r($id_sinAula);
        echo "los id de la tabla de horarios que no se actualizaron por alguna razon y hay que verificar el log fueron:" . count($id_sinAct) . " y son las siguientes: <br>  <pre>";
        print_r($id_sinAct);
        echo "fin del Job...";
    }

    /**
     * 
     */
    public function horariosLeo2022a()
    {
        $filename = WRITEPATH . '/listas2022A/LEO_horarios.csv';
        $file = fopen($filename, 'r');
        if ($file) {
            $fila = 1;
            while ($row = fgetcsv($file)) {
                for ($i = 8; $i < 14; $i++) {
                    $datosInsert = $this->getDiaHoraLEO($i, $row[$i]);
                    if ($datosInsert !== false) {
                        $this->insertaHora($row[15], $datosInsert[0], $datosInsert[1], $datosInsert[2], $fila, '1');
                    }
                }
                $fila++;
            }
            fclose($file);
        }
    }

    /**
     * 
     */
    public function horariosMed2022a()
    {
        $filename = WRITEPATH . '/listas2022A/LMC_horarios.csv';
        $file = fopen($filename, 'r');
        if ($file) {
            $fila = 1;
            while ($row = fgetcsv($file)) {
                for ($i = 3; $i < 9; $i++) {
                    $datosInsert = $this->getDiaHoraLMC($i, $row[$i]);
                    if ($datosInsert !== false) {
                        $this->insertaHora($row[9], $datosInsert[0], $datosInsert[1], $datosInsert[2], $fila, '1');
                    }
                }
                $fila++;
            }
            fclose($file);
        }
    }

    /**
     * obtiene dia y hora para insertar con formato de enfermeria
     * @param index el indice de la columna
     * @param valor el valor de la columna
     * @return datos arreglo de la siguiente forma: [ 0=>dia en letra, 1=>hora inicio, 2=>hora fin ]; false en caso de no haber nada que insertar
     */
    private function getDiaHoraLEO($index, $valor)
    {
        $datos = false;
        if (strlen($valor) > 2) {
            switch ($index) {
                case 8:
                    $datos[0] = 'LUNES';
                    break;
                case 9:
                    $datos[0] = 'MARTES';
                    break;
                case 10:
                    $datos[0] = 'MIÉRCOLES';
                    break;
                case 11:
                    $datos[0] = 'JUEVES';
                    break;
                case 12:
                    $datos[0] = 'VIERNES';
                    break;
                case 13:
                    $datos[0] = 'SÁBADO';
                    break;
                default:
                    $datos[0] = 'DOMINGO';
                    break;
            }
            try {
                $horas = explode('a', $valor, 2);
                $datos[1] = preg_replace("(\D)", '', $horas[0]) . ':00';
                $datos[2] = preg_replace("(\D)", '', $horas[1]);
                if (strlen($datos[1]) == 4) $datos[1] = '0' . $datos[1];
                if (strlen($datos[2]) == 3) $datos[2] = '0' . $datos[2];
                if (strlen($datos[2]) <= 2) $datos[2] = $datos[2] . '00';
                $datos[2] = substr($datos[2], 0, 2) . ':' . substr($datos[2], 2, 2);
            } catch (\Exception $ex) {
                echo $ex->getMessage() . "\n";
            }
        }
        return $datos;
    }

    /**
     * obtiene dia y hora para insertar con formato de enfermeria
     * @param index el indice de la columna
     * @param valor el valor de la columna
     * @return datos arreglo de la siguiente forma: [ 0=>dia en letra, 1=>hora inicio, 2=>hora fin ]; false en caso de no haber nada que insertar
     */
    private function getDiaHoraLMC($index, $valor)
    {
        $datos = false;
        if (strlen($valor) > 4) {
            switch ($index) {
                case 3:
                    $datos[0] = 'LUNES';
                    break;
                case 4:
                    $datos[0] = 'MARTES';
                    break;
                case 5:
                    $datos[0] = 'MIÉRCOLES';
                    break;
                case 6:
                    $datos[0] = 'JUEVES';
                    break;
                case 7:
                    $datos[0] = 'VIERNES';
                    break;
                case 8:
                    $datos[0] = 'SÁBADO';
                    break;
                default:
                    $datos[0] = 'DOMINGO';
                    break;
            }
            try {
                $horas = explode('-', $valor, 2);
                $datos[1] = preg_replace("(\D)", '', $horas[0]);
                $datos[2] = preg_replace("(\D)", '', $horas[1]);
                if (strlen($datos[1]) < 4) $datos[1] = '0' . $datos[1];
                if (strlen($datos[2]) == 3) $datos[2] = '0' . $datos[2];
                if (strlen($datos[2]) <= 2) $datos[2] = $datos[2] . '00';
                $datos[1] = substr($datos[1], 0, 2) . ':' . substr($datos[1], 2, 2);
                $datos[2] = substr($datos[2], 0, 2) . ':' . substr($datos[2], 2, 2);
            } catch (\Exception $ex) {
                echo "ERROR: " . $ex->getMessage() . " -- $valor --  en linea $index \n";
            }
        }
        return $datos;
    }

    /**
     * realiza la insercion de una hora para determinado grupo en determinado dia
     * @param grupo el IDC o Id del grupo
     * @param dia el dia de la semana
     * @param inicio la hora de inicio en formato hora 00:00
     * @param fin la hora de fin en formato hora
     * @param fila el numero de fila del excel
     * @param aula el ID del aula
     * @todo revisar si conviene conservar este script para obtener el periodo de forma dinamica
     */
    private function insertaHora($grupo, $dia, $inicio, $fin, $fila, $aula)
    {
        $datos = [
            'grupo' => $grupo,
            'dia' => mb_strtoupper($dia),
            'inicio' => $inicio,
            'fin' => $fin,
            'aula' => $aula,
            'periodo' => '2022A',
        ];
        $hora = new AulasGrupos($datos);
        try {
            $this->AulasGrupoModel->insert($hora);
            echo "INSERTADO en grupo $grupo el dia $dia a las $inicio \n";
        } catch (\Exception $ex) {
            echo "ERROR al insertar fila $fila: " . $ex->getMessage() . "\n";
        }
    }

    /**
     * renombra las fotos de los aspirantes para que sea "matricula/Folio.extensión"
     * 
     * 
     */
    public function fotos_aspirantes()
    {
        ini_set('memory_limit', '256M');
        set_time_limit(3600);
        $aspirantes = new AspirantesGeneralesModel();
        $respuestasModel = new AspirantesRespuestasModel();
        $aspirantes_ex = $aspirantes->asp_exitosos();
        $campofoto = 52;
        $pathtocopy =  WRITEPATH . 'aspirantesfotosfolio/';
        foreach ($aspirantes_ex as $exitoso) {
            $aspirante = $exitoso->aspirante_id;
            $arch_foto = $respuestasModel->contestado($campofoto, $exitoso->aspirante_id);
            $pathorigin = WRITEPATH . 'admision/2022A/' . $aspirante . '/' . $arch_foto->respuesta_valor;
            $nombrearch = $exitoso->aspirante_id;
            $nombrearchsnext = $nombrearch . '.jpg';
            //copy($pathorigin,$arch_foto->respuesta_valor);
            //rename($arch_foto->respuesta_valor,$nombrearch.'.jpg');
            if (file_exists($pathorigin)) {
                //echo "Archivo encontrado: $pathorigin...\n";
                var_dump($pathorigin, $arch_foto->respuesta_valor);
                echo "<br>";
                if (!copy($pathorigin, $pathtocopy . $nombrearchsnext)) {
                    echo "Error al copiar $arch_foto...\n";
                }
            }
        }
        echo "fin del job";
    }

    /**
     * prepara Google Classroom para los cursos de un periodo determinado
     * @param periodo el periodo en el cual se van a crear los grupos de classroom
     * @param archivar 1 para indicar que se van a archivar los cursos existentes; 0 para mantenerlos
     */
    public function classroomPeriodo($periodo,$archivar) {
        ini_set('memory_limit', '512M');
        set_time_limit(86400);
        echo $this->logdate()."Inicio periodo en Classroom ========================================\n";
        $classroom = new GoogleClassroomLibrary();
        if ($archivar == '1') {
            echo $this->logdate() . "Inicio de archivado de cursos ******\n";
            $classroom->archivaTodos();
            echo $this->logdate() . "Fin de archivado de cursos ******\n";
        }
        $this->creaCursos($classroom, $periodo);
        $this->agregaAlumnos($periodo, null, $classroom);
        echo $this->logdate() . "Fin de periodo en Classroom ========================================\n";
    }

    /**
     * ejecuta la creación de los cursos de classroom
     * @param classroom la instancia de la librería classroom
     * @param periodo el Id del periodo a crear los cursos
     * @todo poner los correos correctos de los directores
     */
    private function creaCursos($classroom, $periodo)
    {
        $gruposModel = new \App\Models\GrupoModel();
        $gruposMateriasModel = new \App\Models\GruposMateriasModel();
        $grupos = $gruposMateriasModel->buscar( [ 'periodo'=>$periodo ] );
        $i=0;
        foreach( $grupos as $grupo ) {
            $gpo = $gruposModel->find( $grupo->grupo );
            if ( is_null($gpo->classroom) ) {
                $correoDir = '';
                if ( $grupo->carrera == '1' ) $correoDir = 'maribel.perez@usalud.edu.mx'; 
                else $correoDir = 'hector.lopez@usalud.edu.mx';// @TODO poner los correos reales de los directores
                
                $maestro = $grupo->correo;
                if ( !$maestro ) $maestro = 'webmaster@usalud.edu.mx';
                $correosMaestros = [ $maestro, $correoDir ];
                // $correosMaestros = [ 'miguel.silva@usalud.edu.mx', $correoDir ]; // comentar esta linea para evitar debug
                $curso = $classroom->crea( $grupo->materiaNombre.' ('.$grupo->periodo.')', $grupo->clave, $grupo->grupo, 'En horario', $correosMaestros );
                if ( $curso !== false ) {
                    $gpo->classroom = $curso->id;
                    $gpo->enrollment = $curso->getEnrollmentCode();
                    $gruposModel->save($gpo);
                    echo $this->logdate()." curso creado y grupo actualizado: ".$grupo->grupo."\n";
                }
                else {
                    echo $this->logdate()."ERROR: ".$grupo->grupo." no cuenta con curso debido a ".$classroom->getLastError()."\n";
                }
            }
            
            $i++;
            if ( $i%5 == 0 ) sleep(15);
            // if ( $i%20 == 0 ) break;
        }
    }

    /**
     * agrega alumnos a los cursos de un periodo
     * @param periodo el periodo a utilizar
     * @param seccion la seccion a insertar
     * @param classroom la instancia de la librería de classroom (opcional porque se puede invocar desde la creacion de cursos o bien directo)
     */
    public function agregaAlumnos( $periodo, $seccion=null, $classroom=null ) {
        set_time_limit(86400);
        ini_set('memory_limit', '512M');
        echo $this->logdate()."Iniciando el cargado de alumnos.....\n";
        if ( is_null($classroom) ) {
            $classroom = new GoogleClassroomLibrary();
        }
        $gruposModel = new \App\Models\GrupoModel();
        $grupos = $gruposModel->getgrupos($periodo);
        $i=0;
        foreach ($grupos as $grupo) {
            echo "Grupo $grupo->id :: $grupo->classroom ------- \n";
            if ( !is_null($seccion) ) {
                if ( $grupo->clave != $seccion ) {
                    continue;
                }
            }
            if (strlen($grupo->classroom) > 1) {
                echo "Agregar $grupo->clave a $grupo->classroom \n";
                log_message( 'notice', "Agregando $grupo->idc de la seccion $grupo->clave." );
                $this->agregaAlumnosCurso($classroom, $grupo->classroom, $grupo->idc, $grupo->enrollment);
            }
            $i++;
          //  if ( $i%5 == 0 ) break;
        }
        echo $this->logdate() . " -------- Finalizado el cargado de alumnos.....\n";
    }

    /**
     * agrega alumnos a un cusros de classroom
     */
    private function agregaAlumnosCurso($classroom, $courseId, $idc, $enrollmentCode)
    {
        $gruposAlumnosModel = new \App\Models\AlumnosCalificacionModel();
        if (!$courseId) {
            return false;
        }
        echo "GRUPO $idc con course: $courseId \n";
        $correosAlumnos = [
            'dummy1@usalud.edu.mx', 'dummy2@usalud.edu.mx', 'dummy3@usalud.edu.mx', 'dummy4@usalud.edu.mx', 'dummy5@usalud.edu.mx', 'dummy6@usalud.edu.mx',
            'dummy7@usalud.edu.mx', 'dummy8@usalud.edu.mx', 'dummy9@usalud.edu.mx', 'dummy10@usalud.edu.mx', 'dummy11@usalud.edu.mx', 'dummy12@usalud.edu.mx',
            'dummy13@usalud.edu.mx', 'dummy14@usalud.edu.mx', 'dummy15@usalud.edu.mx', 'dummy16@usalud.edu.mx', 'dummy17@usalud.edu.mx', 'dummy18@usalud.edu.mx',
            'dummy19@usalud.edu.mx', 'dummy20@usalud.edu.mx', 'dummy21@usalud.edu.mx', 'dummy22@usalud.edu.mx', 'dummy23@usalud.edu.mx', 'dummy24@usalud.edu.mx',
            'dummy25@usalud.edu.mx', 'dummy26@usalud.edu.mx', 'dummy27@usalud.edu.mx', 'dummy28@usalud.edu.mx', 'dummy29@usalud.edu.mx', 'dummy30@usalud.edu.mx',
            'dummy31@usalud.edu.mx', 'dummy32@usalud.edu.mx', 'dummy33@usalud.edu.mx', 'dummy34@usalud.edu.mx', 'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx',
            'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx', 'dummy1@usalud.edu.mx',
        ];
        $alumnos = $gruposAlumnosModel->getByGrupo($idc);
        $i = 0;
        foreach ($alumnos as $alumno) {
            $correoAlumno = $alumno->correo;
            // $correoAlumno = $correosAlumnos[$i]; // TODO comentar esta linea en producción
            try {
                if ( $gruposAlumnosModel->existeclassroom($correoAlumno,$idc) == false ) {
                    $classroom->agregaEstudiante($courseId, $enrollmentCode, $correoAlumno);
                    $gruposAlumnosModel->registraclassroom($correoAlumno,$idc);
                    echo $this->logdate() . "Alumno $correoAlumno agregado al grupo $courseId con ID $idc . \n";
                    log_message( 'notice', "Alumno $correoAlumno agregado a $idc con classroom $courseId." );
                    $i++;
                    if ( $i%5 == 0 ) sleep(15);
                }
                else {
                    log_message( 'notice', "Alumno $correoAlumno SALTADO en $idc con classroom $courseId." );
                }
            } catch (\Exception $ex) {
                echo $this->logdate() . "Alumno $correoAlumno no registrado en $courseId debido a " . $ex->getMessage() . "\n";
            }
            
        }
        echo "\n\n";
    }

    public function correcion($archivo, $materia)
    {
        ini_set('memory_limit', '256M');
        $url = ('/listas2022A/' . $archivo . ".csv");
        $listas_excel = $this->getListasCSV($url);
        // $limite = 0;
        $arr_registros = [];
        if ($listas_excel) {
            foreach ($listas_excel as $row) {
                $alumno = $this->alumnosModel->find($row['alumno']);
                if ($alumno) {
                    if ($alumno->status == 'ACTIVO') {
                        $registro  =  $this->asignarMateria($row['alumno'], $row['seccion'], $materia);
                        if ($registro) {
                            $arr_registros[] = $registro;
                        }
                    } else {
                        $arr_registros[] =  "[" . date('Y-m-d H:i:s') . "][alumno: $alumno] Alumno con estatus de " . $alumno->status;
                    }
                }
            }
        }

        echo $this->logdate() . "Finalizado el procesamiento de archivos \n";
        echo "<pre>";
        echo json_encode($arr_registros);
        echo "</pre>";
    }

    /**
     * asigna la materia seleccionada del alumno
     */
    public function asignarMateria($alumno, $seccion, $materia)
    {
        $grupos = $this->grupoModel->getBySeccion($seccion, '2022A');
        foreach ($grupos as $grupo) {
            if ($grupo->materia == $materia) {
                return  "[" . date('Y-m-d H:i:s') . "][alumno: $alumno][grupo: $grupo->id][seccion: $seccion][MATERIA: $grupo->materia] " . $this->insertarAlumno($alumno, $grupo->id) . "<br>";
            }
        }
    }

    public function CambioSecciones($alumno, $seccion, $periodo)
    {
        $registro  = $this->RequisitosMaterias($alumno, $seccion,$periodo);
        echo $this->logdate() . "Finalizado el procesamiento \n";
        echo "<pre>";
        echo var_dump($registro);
        echo "</pre>";
    }

    public function estadisticadocente ($evaluacion){
        log_message("notice", "Ha comenzado el proceso que calcula la evaluación docente...");
        $variable =$this->EstadisticasEvaluacionDocenteController->generartxtEvaluacionMateria($evaluacion);
        log_message("notice", "Fin del proceso.");
    }

}
