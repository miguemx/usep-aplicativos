<?php
namespace App\Controllers;

use App\Models\AlumnosSegurosModel;
use App\Models\AlumnosModel;
use App\Models\Carrerasmodel;

class SeguroFacultativo extends BaseController {

    public function index() {
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * renderiza la lista de los estudiantes que han solicitado seguro facultativo
     */
    public function estudiantes() {
        $segurosModel = new AlumnosSegurosModel();
        $cuales = 'TODOS';
        $filtros = array();
        if( $this->request->getPost('seleccionar') ) {
            $cuales = $this->request->getPost('seleccionar');
        }
        if ( $this->request->getPost('fechaSubido') ) {
            $filtros['fechaDescarga'] = $this->request->getPost('fechaSubido');
        }
        $data['seguros'] = $this->getEstudiantes($cuales,$filtros);
        $data['fechasDescarga'] = $segurosModel->getFechasDescarga();
        $data['menu'] = $this->sesion->get('menu');
        $data['session'] = $this->sesion;
        return view('segurofacultativo/listado', $data);
    }

    /**
     * muestra la ventana para visualizar el documento colocado y los datos generales del alumno;
     * guarda los datos si se viene de una petición post
     */
    public function ver($matricula) {
        $alumnosModel = new AlumnosModel();
        $alumno = $alumnosModel->getInfo( $matricula );
        $segurosModel = new AlumnosSegurosModel();
        $seguro = $segurosModel->find( $matricula );
        if ( is_null($alumno) ) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            exit();
        }
        if ( !is_null( $this->request->getPost('gf') ) ) { 
            if ( !is_null($seguro) ) { $notificacion = '';
                $seguro->ssn = $this->request->getPost('ssn');
                $seguro->comentarios = $this->request->getPost('observaciones');
                if ( $this->request->getPost('completado') === '1' ) {
                    $seguro->status = 'COMPLETADO';
                    $notificacion = 'Tu solicitud de afiliación al seguro facultativo ha sido valida da éxitosamente, te sugerimos consultar nuevamente tu vigencia de derechos en un lapso de 8 días hábiles.';
                } 
                else {
                    $seguro->status = 'OBSERVADO';
                    $notificacion = 'Tu expediente de seguro facultativo tiene algunas observaciones. Por favor accede a Servicios Escolares para corregir lo que se te indica. '.$seguro->comentarios;
                } 
                try {
                    $segurosModel->save( $seguro );
                    $alumno->ssn = $seguro->ssn;
                    $this->notifica( $alumno->correo, $notificacion );
                    return redirect()->to('/SeguroFacultativo/Estudiantes');
                }
                catch(\CodeIgniter\Database\Exceptions\DataException $ex) {
                    echo 'faculta.'.$ex->getMessage();
                }
            }
        }
        $data['seguro'] = $seguro;
        $data['alumno'] = $alumno;
        $data['menu'] = $this->sesion->get('menu');
        return view('segurofacultativo/ver', $data);
    }

    /**
     * elimina el registro de seguro facultativo de un estudiante
     */
    public function reiniciar() {
        $matricula = $this->request->getPost('alumno');
        if ( $matricula !== null ) {
            try {
                $segurosModel = new AlumnosSegurosModel();
                $segurosModel->delete( $matricula, true );
                $this->sesion->setFlashdata('msg','Se ha reiniciado el status de seguro facultativo para '.$matricula.'.');
            }catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                $this->sesion->setFlashdata('error','Ocurrió un error al reinciar el status del estudiante.');
            }
        }else {
            $this->sesion->setFlashdata('error','Por favor proporciona un estudiante correcto.');
        }
        return redirect()->to('/SeguroFacultativo/Estudiantes');
    }

    /**
     * Descarga la plantilla a enviar al IMSs
     */
    public function imss() {
        $data = '';
        if( $this->request->getPost('descargar') === 'plantilla' ) {
            $post = $this->request->getPost();
            $seguroModel = new AlumnosSegurosModel();
            $alumnosModel = new AlumnosModel();
            foreach ( $post as $key=>$value ) {
                if ( preg_match( "/^[0-9]{9}$/", $key ) ) {
                    try {
                        $alumno = $alumnosModel->withDeleted()->find( $key );
                        $seguro = $seguroModel->withDeleted()->find( $key );
                        if ( !is_null($alumno) && !is_null($seguro) ) {
                            if ( $seguro->status === 'COMPLETADO' || $seguro->status === 'ENVIADO' ) {
                                $seguro->status = 'ENVIADO';
                                $data .= $this->campoImss( env('imss.regpatronal'), env('imss.lng.regpatronal') );
                                $data .= $this->campoImss( $seguro->ssn, env('imss.lng.ssn') );
                                $data .= $this->campoImss( $alumno->apPaterno, env('imss.lng.apPaterno') );
                                $data .= $this->campoImss( $alumno->apMaterno, env('imss.lng.apMaterno') );
                                $data .= $this->campoImss( $alumno->nombres, env('imss.lng.nombre') );
                                $data .= $this->campoImss( $seguro->salario, env('imss.lng.salario') );
                                $data .= $this->campoImss( $seguro->generico6, env('imss.lng.gen6') );
                                $data .= $this->campoImss( $seguro->tipoContratacion, env('imss.lng.tipoContratacion') );
                                $data .= $this->campoImss( $seguro->tipoSalario, env('imss.lng.tipoSalario') );
                                $data .= $this->campoImss( $seguro->semana, env('imss.lng.jornada') );
                                $data .= $this->campoImss( date("dmY"), env('imss.lng.fecha') );
                                $data .= $this->campoImss( $seguro->umf, env('imss.lng.umf'), true );
                                $data .= $this->campoImss( $seguro->gen2, env('imss.lng.gen2') );
                                $data .= $this->campoImss( $seguro->tipoMovimiento, env('imss.lng.tipoMov') );
                                $data .= $this->campoImss( $seguro->guia, env('imss.lng.guia') );
                                $data .= $this->campoImss( $alumno->id, env('imss.lng.matricula'), true );
                                $data .= $this->campoImss( $seguro->gen1, env('imss.lng.gen1') );
                                $data .= $this->campoImss( $alumno->curp, env('imss.lng.curp') );
                                $data .= $this->campoImss( $seguro->formato, env('imss.lng.formato') );
                                $data .= "\n";
                                $seguro->fechaDescarga = date('Y-m-d');
                                $seguroModel->save( $seguro );
                            }
                        }
                    }
                    catch(\Exception $ex) {
                        echo ' -- ERROR: ' . $ex->getMessage();
                    }
                }
            }
        }
        if ( strlen($data) < 1 ) $data = '(VACIO)';
        return $this->response->download('USEP_IMSS_'.date("Y-m-d_H-i"), $data);
    }

    /**
     * devuelve la lista de estudiantes que solicitan seguro facultativo
     */
    private function getEstudiantes($cuales='todos',$filtros=array()) { 
        $alumnosSegurosModel = new AlumnosSegurosModel();
        $listado = $alumnosSegurosModel->getAll($cuales, $filtros);
        return $listado;
    }

    /**
     * construye el campo para el archivo del IMSS basado en las reglas establecidas
     * @param valor el valor que debe ir en la celda
     * @param longutud la longitud que debe tener el campo
     * @param zf (opcional) indica si se debe completar el valor con ceros a la izquierda
     * @return valor el campo en cadena contruido con los valores necesarios
     */
    private function campoImss( $valor, $longitud, $zf=false ) {
        $valor = str_replace( 'Á', 'A', $valor );
        $valor = str_replace( 'É', 'E', $valor );
        $valor = str_replace( 'Í', 'I', $valor );
        $valor = str_replace( 'Ó', 'O', $valor );
        $valor = str_replace( 'Ú', 'U', $valor );
        $valor = str_replace( 'Ü', 'U', $valor );
        $valor = str_replace( 'Ñ', '#', $valor );
        $longitud = intval( $longitud );
        $lng = mb_strlen($valor);
        if ( $lng <= $longitud ) {
            $faltantes = $longitud - $lng;
            if ( !$zf ) {
                for ( $i=0; $i<$faltantes; $i++ ) $valor .= ' ';
            }
            else {
                $ceros = '';
                for ( $i=0; $i<$faltantes; $i++ ) $ceros .= '0';
                $valor = $ceros.$valor;
            }
        }
        else {
            $valor = substr( $valor, 0, $longitud );
        }
        return $valor;
    }

}