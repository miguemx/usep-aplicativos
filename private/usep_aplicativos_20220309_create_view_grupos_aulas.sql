ALTER TABLE `aca_grupos_aulas` ADD `gpoaula_periodo` VARCHAR(7) NOT NULL AFTER `gpoaula_aula`; 
UPDATE `aca_grupos_aulas` SET `gpoaula_periodo` ='2022B';
ALTER TABLE `aca_grupos_aulas` ADD FOREIGN KEY (`gpoaula_periodo`) REFERENCES `aca_periodos`(`periodo_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


CREATE VIEW view_grupo_aulas AS SELECT
    aca_grupos_aulas.gpoaula_grupo,
    aca_aulas.aula_id,
    aca_aulas.aula_nombre,
    aca_grupos_aulas.gpoaula_dia,
    aca_grupos_aulas.gpoaula_inicio,
    aca_grupos_aulas.gpoaula_fin,
    aca_grupos_aulas.gpoaula_periodo,
    aca_grupos.grupo_clave,
    aca_grupos.grupo_materia,
    aca_materias.materia_nombre,
    aca_grupos.grupo_docente,
    CONCAT(
        aplicativo_empleados.empleado_ap_paterno,
        " ",
        aplicativo_empleados.empleado_ap_materno,
        " ",
        aplicativo_empleados.empleado_nombre
    ) AS nombre_docente
FROM
    aca_aulas
INNER JOIN aca_grupos_aulas ON aca_grupos_aulas.gpoaula_aula = aca_aulas.aula_id
INNER JOIN aca_grupos ON aca_grupos.grupo_id = aca_grupos_aulas.gpoaula_grupo
INNER JOIN aca_materias ON aca_materias.materia_clave = aca_grupos.grupo_materia
INNER JOIN aplicativo_empleados ON aplicativo_empleados.empleado_id = aca_grupos.grupo_docente;