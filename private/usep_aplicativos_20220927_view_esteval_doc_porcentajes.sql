create view view_est_eval_doc_porcentajes as(
SELECT
        IF(
            a.preguntas_clasificacion = 1,
            COUNT(a.respuestas_valor),
            0
        ) * CASE WHEN a.respuestas_valor = 'EXCELENTE' THEN 4 WHEN a.respuestas_valor = 'BUENO' THEN 3 WHEN a.respuestas_valor = 'REGULAR' THEN 2 WHEN a.respuestas_valor = 'DEFICIENTE' THEN 1 WHEN a.respuestas_valor = 'NULO' THEN 0
END AS clasificacion1,
IF(
    a.preguntas_clasificacion = 2,
    COUNT(a.respuestas_valor),
    0
) * CASE WHEN a.respuestas_valor = 'EXCELENTE' THEN 4 WHEN a.respuestas_valor = 'BUENO' THEN 3 WHEN a.respuestas_valor = 'REGULAR' THEN 2 WHEN a.respuestas_valor = 'DEFICIENTE' THEN 1 WHEN a.respuestas_valor = 'NULO' THEN 0
END AS clasificacion2,
IF(
    a.preguntas_clasificacion = 3,
    COUNT(a.respuestas_valor),
    0
) * CASE WHEN a.respuestas_valor = 'EXCELENTE' THEN 4 WHEN a.respuestas_valor = 'BUENO' THEN 3 WHEN a.respuestas_valor = 'REGULAR' THEN 2 WHEN a.respuestas_valor = 'DEFICIENTE' THEN 1 WHEN a.respuestas_valor = 'NULO' THEN 0
END AS clasificacion3,
IF(
    a.preguntas_clasificacion = 4,
    COUNT(a.respuestas_valor),
    0
) * CASE WHEN a.respuestas_valor = 'EXCELENTE' THEN 4 WHEN a.respuestas_valor = 'BUENO' THEN 3 WHEN a.respuestas_valor = 'REGULAR' THEN 2 WHEN a.respuestas_valor = 'DEFICIENTE' THEN 1 WHEN a.respuestas_valor = 'NULO' THEN 0
END AS clasificacion4,
a.respuestas_valor AS respuestas_valor,
a.materia_clave AS materia_clave,
a.materia_nombre AS materia_nombre,
a.materia_semestre AS materia_semestre,
a.materia_carrera AS materia_carrera,
a.empleado_id AS empleado_id,
a.empleado_ap_paterno AS empleado_ap_paterno,
a.empleado_ap_materno AS empleado_ap_materno,
a.empleado_nombre AS empleado_nombre,
a.gpoalumno_grupo AS gpoalumno_grupo,
a.grupo_clave AS grupo_clave,
a.evaluacion_id AS evaluacion_id
    
FROM
    usep_aplicativos.view_est_eval_doc a
WHERE
    a.preguntas_tipo = 4
GROUP BY
    a.evaluacion_id,
    a.empleado_id,
    a.materia_clave,
    a.grupo_clave,
    a.gpoalumno_grupo,
    a.preguntas_clasificacion,
    a.respuestas_valor
    
    
ORDER BY
    a.evaluacion_id,
    a.empleado_id,
    a.materia_clave,
    a.grupo_clave,
    a.gpoalumno_grupo,
    a.preguntas_clasificacion,
    a.respuestas_valor);