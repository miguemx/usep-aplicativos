<?php

namespace App\Controllers;

use App\Models\RosModel;
use App\Models\AlumnosModel;
use App\Models\AlumnoGrupoHistorialModel;
use App\Models\GrupoModel;
use App\Models\GrupoAlumnoModel;
use App\Models\MateriaModel;
use App\Models\PagosAlumnosModel;

use App\Entities\Ros;
use App\Entities\GrupoAlumno;
use App\Models\CredencialModel;
use CodeIgniter\CLI\Console;

class Jobs extends BaseController
{

    private $rosDir;
    private $sufixRosDirProcesar = 'procesar/';
    private $sufixRosDirProcesados = 'completados/';

    public function __construct()
    {
        $this->rosDir = WRITEPATH . 'pagos/';
    }

    public function index()
    {
        echo "Jobs";
    }

    /**
     * procesa todos los archivos ROS subudos al servidor
     */
    public function procesaros()
    {
        ini_set('memory_limit', '256M');
        echo $this->logdate() . "Iniciando proceso de archivos ROS... \n";
        $archivos = $this->getListRos();
        foreach ($archivos as $archivo) {
            $result = $this->guardaRos($archivo);
            if ($result === true) {
                echo $this->logdate() . "Archivo == $archivo == procesado con exito \n";
                copy($this->rosDir . $this->sufixRosDirProcesar . $archivo, $this->rosDir . $this->sufixRosDirProcesados . $archivo);
                unlink($this->rosDir . $this->sufixRosDirProcesar . $archivo);
            } else {
                $this->reportaErrores($archivo, $result);
            }
        }
        echo $this->logdate() . "Finalizado el procesamiento de archivos ROS \n";
    }

    /**
     * obtiene la lista de los archivos para procesar subdos al server
     */
    private function getListRos()
    {
        $dirProcesar = $this->rosDir . $this->sufixRosDirProcesar;
        $filesProcesar = scandir($dirProcesar);
        $fileList = [];
        foreach ($filesProcesar as $fileProcesar) {
            if (!is_dir($fileProcesar)) $fileList[] = $fileProcesar;
        }
        echo $this->logdate() . count($fileList) . " archivos encontrados. \n";
        return $fileList;
    }

    /**
     * abre el archivo definidio y lo procesa para guardar los pagos ROS
     * @param archivo una cadena con el nombre completo del archivo
     * @return true en caso de no existir ningun error
     * @return errors array con los errores detectados durante la carga
     */
    private function guardaRos($archivo)
    {
        $rosModel = new RosModel();
        $pagoAlumnosModel = new PagosAlumnosModel();
        $errors = [];
        $file = fopen($this->rosDir . $this->sufixRosDirProcesar . $archivo, 'r');
        if ($file) {
            $i = 0;
            while ($row = fgetcsv($file)) {
                if ($i > 0) {
                    $ros = new Ros();
                    $ros->fechaPago = $row[0];
                    $ros->nombre = $row[1];
                    $ros->concepto = $row[2];
                    $ros->cuenta = $row[3];
                    $ros->descCuenta = $row[4];
                    $ros->importe = $row[5];
                    $ros->folioInteligente = $row[6];
                    $ros->referencia = $row[7];
                    $ros->estatusServicio = $row[8];
                    $ros->fechaOtorgado = $row[9];
                    $ros->usuario = $row[10];
                    $ros->unidadResponsable = $row[11];
                    $ros->beneficiario = $row[12];
                    $ros->cotejado = '0';
                    $ros->matricula = null;
                    try {
                        $rosModel->insert($ros);
                        $ros->id = $rosModel->lastId();
                        $pagoAlumno = $pagoAlumnosModel->getEnEsperaByReferencia($ros->referencia);
                        if (!is_null($pagoAlumno)) {
                            $rosModel->coteja($ros->referencia, $pagoAlumno);
                        }
                    } catch (\Exception $ex) {
                        $errors[] = 'Linea ' . $i . '.';
                        log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                    }
                }
                $i++;
            }
            fclose($file);
        }
        if (count($errors) == 0) return true;
        else return $errors;
    }

    /**
     * genera un archivo de log de salida de acuerdo a los errores reportados en un arreglo de mensajes
     * @param archivo el nombre del archivo original
     * @param errores el arreglo con los errores a reportar
     */
    private function reportaErrores($archivo, $errores)
    {
        $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . $archivo . '.log', "w");
        if ($file) {
            fwrite($file, " LISTADO DE ERRORES PARA EL ARCHIVO $archivo \n");
            foreach ($errores as $error) {
                fwrite($file, $error . "\n");
            }
            fclose($file);
        }
    }

    /**
     * devuelve la cadena a imprimir en las salidas de los scripts
     * @param type el tipo de información a mostrar.
     * @return string con la informacion de la fecha a poner
     */
    private function logdate($type = null)
    {
        $logdate = "[" . date("Y-m-d H:i:s") . "]";

        $logdate .= " - ";
        if (is_string($type)) $logdate .= "[$type] - ";
        return  $logdate;
    }
    /**
     * proceso para pasar alumnos de grupo - obtiene todos los alumnos que va mandando por ciclos a obtener listas regresa un arreglo 
     * @param alumnos arreglo de alumnos a procesar
     */
    public function procesoreinscripcion()
    {
        echo $this->logdate() . "Inicio del proceso de reinscripcion de alumnos \n";
        ini_set('memory_limit', '256M');
        $alumno = new AlumnosModel();
        $lista = $alumno->findssall();
        $contador = 0;
        foreach ($lista as $key) {
            $respuesta = $this->obtenerlistas($key->alumno_id, $contador, $key->alumno_semestre);
            if ($respuesta['status'] == 0) {
                $arr_errores[] = $respuesta['mensaje'];
            } else {
                $arr_insertados[] = $respuesta['mensaje'];
            }
            $contador++;
        }
       if (isset ($arr_insertados)){

           $file = fopen( WRITEPATH.'logs/'.date("YmdHis").'alumnos_insertados'.'.log', "w" );
           if ( $file ) {
               fwrite( $file, " LISTADO INSERCIONES ALUMNOS INSERTADOS \n" );
               foreach ( $arr_insertados as $error ) {
                   foreach ($error as $key ) {
                       fwrite( $file, $key."\n" );
                   }
               }
               fclose ( $file );
           }
       }
       if (isset($arr_errores)){

           $file = fopen( WRITEPATH.'logs/'.date("YmdHis").'alumnos_errores'.'.log', "w" );
           if ( $file ) {
               fwrite( $file, " LISTADO ERRORES EN LA INSERCION DE ALUMNOS \n" );
               foreach ( $arr_errores as $error ) {
                   foreach ($error as $key ) {
                       fwrite( $file, $key."\n" );
                   }
               }
               fclose ( $file );
           }

       }
       echo $this->logdate() . "Fin del proceso de reinscripcion de alumnos \n <br>";
    }

    /**
     * regresa un mensaje con una clasificacion de estatus, si es 1 inserta, si es 0 agrega errores
     * 
     */
    public function obtenerlistas($id, $contador, $semestre)
    {
        $aux=0;
        $datenow = date('Y-m-d H:i:s');
        $grupo = new AlumnoGrupoHistorialModel();
        $respuesta = $grupo->gethistoria($id);
        $bandera_insertado = 0;
        $status = 0;
        $mensaje = null;
        if ($respuesta) {
            $carrera = $respuesta[0]->carrera_id;
            $semestre_siguiente = $respuesta[0]->alumno_semestre + 1;
            $materia = new MateriaModel();
            $materia_historia = $materia->buscarsemestre($semestre_siguiente, $carrera);
            $arr_prerrequisitos_negadas = [];
            $arr_prerrequisitos_aprovado = [];
            $grupo_siguiente = "";
            $bandera_coorrequisito = 0;
            $bandera_grupo_correcto = 0;
            foreach ($materia_historia as $key) {
                if ($arr_series = json_decode($key->materia_seriacion, true)) {
                    foreach ($arr_series['prerrequisitos']  as $serie) {
                        foreach ($respuesta as $arr_datos) { //recorre todos los prerrequisitos 
                            $spl1 = str_split($arr_datos->grupo_idc); //separamos el idc para obtener la siguiente seccion
                            if (count($spl1) == 10) {
                                $aux= $grupo_siguiente;
                                $grupo_siguiente = ($spl1[7] + 1) . $spl1[8] . $spl1[9]; //pasamos la seccion al siguiente periodo solo si el idc cumple con la longitud de caracteres 
                                if ($grupo_siguiente < $aux ) {
                                    $grupo_siguiente= $aux;
                                }
                                $bandera_seccion_siguiente = true;
                            } else {
                            }
                            if (isset($arr_series['correquisito'])) {
                                if ($bandera_coorrequisito == 0) {
                                    foreach ($arr_series['correquisito'] as $core) {
                                        if ($serie == $core) {
                                            $bandera_coorrequisito = 1;
                                            $arr_prerrequisitos_aprovado[] = $key->materia_clave;
                                            $arr_materias_siguienteperiodo[] = $key->materia_clave;
                                        }
                                    }
                                }
                            } else {
                                if ($arr_datos->materia_clave == $serie) {
                                    if ($arr_datos->aprobada == 1) {
                                        $arr_prerrequisitos_aprovado[] = $key->materia_clave;
                                        $arr_materias_siguienteperiodo[] = $key->materia_clave;
                                        
                                    }
                                } else {
                                    $arr_prerrequisitos_negadas[] = $key->materia_clave;
                                }
                            }
                        }
                    }
                } else {
                    $arr_materias_siguienteperiodo[] = $key->materia_clave;
                }
            }
            $grupos = new GrupoModel();
            if ($carrera == 1) {
                $buscar = "LEO-$grupo_siguiente";
            } else if ($carrera == 2) {
                $buscar = "MED-$grupo_siguiente";
                /* echo " $grupo_siguiente <br>"; */
                if($grupo_siguiente==301){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==302){
                    $arr_materias_siguienteperiodo[] = "OPT-112";
                }
                if($grupo_siguiente==303){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==304){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==305){
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if($grupo_siguiente==306){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==307){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==308){
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if($grupo_siguiente==309){
                    $arr_materias_siguienteperiodo[] = "OPT-113";
                }
                if($grupo_siguiente==310){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==501){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==502){
                    $arr_materias_siguienteperiodo[] = "OPT-111";
                }
                if($grupo_siguiente==503){
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if($grupo_siguiente==504){
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if($grupo_siguiente==505){
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if($grupo_siguiente==506){
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                if($grupo_siguiente==507){
                    $arr_materias_siguienteperiodo[] = "OPT-112";
                }
                if($grupo_siguiente==508){
                    $arr_materias_siguienteperiodo[] = "OPT-114";
                }
                $arr_materias_siguienteperiodo[] = "MC-TUT";
            }
            /* var_dump($arr_materias_siguienteperiodo);
            die(); */
            $arr_grupos = $grupos->buscargrupoclave($buscar, "2022A");     // llama a la funcion  buscargrupoclave que regresa los grupos disponibles para esa seccion disponibles en el siguiente periodo      
            if ($arr_grupos) {
                foreach ($arr_grupos as $arr_grupo) {
                    $bandera_grupo_correcto = 0;
                    foreach ($arr_materias_siguienteperiodo as $materias_siguientes) {
                        if ($arr_grupo->grupo_materia == $materias_siguientes) {
                            $grupo_alumnoEnt = new GrupoAlumno();
                            $grupo_alumnoModel = new GrupoAlumnoModel();
                            $grupo_alumnoEnt->alumno = $id;
                            $grupo_alumnoEnt->grupo = $arr_grupo->grupo_id;
                            $bandera_grupo_correcto = 1;
                            try {
                                $status = 1;
                                //inserta en el modelo la entidad
                                $grupo_alumnoModel->insert($grupo_alumnoEnt);//-------------------------------------------------------------------------------------------------------------------------------------------------------------
                                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Alumno insertado correctamente [grupo: $arr_grupo->grupo_id][materia: $materias_siguientes]";
                                //cambiar de semestre al alumno y deshabilitar la carga de materias para el 
                                $bandera_insertado = 1;
                            } catch (\Exception $ex) {
                                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Error al insertar en [grupo: $arr_grupo->grupo_id][materia: $materias_siguientes]";
                                //echo "Error en captura [alumno_id : $id] [grupo_id : $arr_grupo->grupo_id] ";
                            }
                        }
                    }
                    if ($bandera_grupo_correcto == 0) {
                        $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> Alumno no puede ser registrado en el grupo por idc de grupo incorrecto, no hay coincidencias [grupo: $arr_grupo->grupo_id][materia: $arr_grupo->grupo_materia]";
                    }
                }
            } else {
                $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> No se encontraron grupos para el periodo siguiente [clave: $buscar][periodo: 2022A][$buscar]";
            }
        } else {
            $alumno_status[] = "[$datenow][ciclo del proceso: $contador][alumno: $id]=> No se encontraron registros de grupos al que pertenezca el alumno";
        }
        if ($bandera_insertado == 1) {
            $credencial = new CredencialModel();
            $findcred =  $credencial->find($id);
            if ($findcred) {
                $findcred->vigencia =  '07/2022';
                try{
                    $credencial->update($id,$findcred);//------------------------------------------------------------------------
                    $alumno_status[] = "[$datenow][alumno: $id]=> Se actualizo la vigencia de credencial";
                }catch  (\Exception $ex){
                    $alumno_status[] = "[$datenow][alumno: $id]=> No se pudo actualizar la vigencia de credencial";
                }
            }
            $arr_update = [
                'alumno_periodo ' => "2022A",
                'alumno_semestre' => ($semestre + 1)
            ];
            $alumno_update_semestre = new AlumnosModel();
            try {
                $alumno_update_semestre->updateperiodo($id, $arr_update);//---------------------------------------------------------
                $alumno_status[] = "[$datenow][alumno: $id]=> Alumno cambiado a periodo 2022A ";
            } catch (\Exception $ex) {
                $alumno_status[] = "[$datenow][alumno: $id]=> Alumno no pudo ser cambiado a periodo 2022A ";
            }
        }

        $log_proceso = [
            'status' => $status,
            'mensaje' => $alumno_status
        ];
        return $log_proceso;
    }

    /**
     * Esta funcion recorre la tabla de grupos para actualizar el folio de los grupos, las tutorias se verifican en null
     * 
     */
    public function updatefolio()
    {
        $grupomodl = new GrupoModel();
        $resp = $grupomodl->getgrupos();
        $tutorias = $grupomodl->updategrupostut();
        $folio = 176;
        foreach ($resp as $key) {
            echo "$key->grupo_id : $key->grupo_periodo : $key->grupo_folio ---> $key->grupo_materia <br>";
            $id = $key->grupo_id;
            $arr_update = [
                'grupo_folio' => $folio
            ];
            $grupomodl->updategrupo($id, $arr_update);
            $folio++;
        }
    }
    public function bajas(){
        echo $this->logdate() . "Proceso de bajas de alumnos iniciado... \n";
        $conta=1;
        $arr_bajas=[];
        $alumno = new AlumnosModel();
        $lista = $alumno->findssall();
        foreach ($lista as $key) {
           $bandera = $this->excesodetalento($key->alumno_id,$conta);
           if ($bandera['status']===true) {
              $arr_bajas[]=$bandera['mensaje'];
              $conta++;
           }
        }
        if ($arr_bajas) {
            $file = fopen(WRITEPATH . 'logs/' . date("YmdHis") . "Catalogo de Alumnos de Baja" . '.log', "w");
            if ($file) {
                fwrite($file, " LISTADO DE ALUMNOS ELIMINADOS POR EL PROCESO \n");
                foreach ($arr_bajas as $error) {
                    fwrite($file, $error . "\n");
                }
                fclose($file);
            }
        }
        echo $this->logdate() . "Proceso de bajas de alumnos finalizado [Revisa los Logs generados] \n";
    }
    public function excesodetalento($alumno, $conta ){
        $status = false;
        $bandera_baja= "";
        $arr_materias=[];
        $grupo = new AlumnoGrupoHistorialModel();
        $respuesta = $grupo->historialperiodo($alumno,'2021A');
        $contador=0;
        $bandera_comentario="";
        foreach($respuesta as $historia){
            if( $historia->gpoalumno_extraordinario != null){
                $calificacion = $historia->gpoalumno_extraordinario;
            }else if( $historia->gpoalumno_ordinario === null ){
                $calificacion = 10;
            }else{
                $calificacion = $historia->gpoalumno_ordinario;
            }
            if ( $calificacion <= 5 ) {
                $arr_materias[] = $historia->materia_clave;
                $contador++;
            }
        }
        $datenow2 = date('Y-m-d H:i:s');
        if ($contador >= 4) {
            $bandera_comentario = "Alumno con mas de 3 materias reprobadas en el periodo 2021A";
            $bandera_baja= "[$datenow2][$conta][alumno: $alumno]=> alumno con mas de 3 materias reprobadas";
        }
        $aux =0;
        $historiacompleta = new AlumnoGrupoHistorialModel();
        $grupostotales = $historiacompleta->gethistoria($alumno);
        if ( $arr_materias ) {
                foreach($arr_materias as $materia){
                    $aux=0;
                    foreach($grupostotales as $key ){
                        if ( $key->materia_clave == $materia && $key->aprobada == 0 ) {
                            $aux++;
                        }
                    }
                    if ($aux > 1) {
                        $bandera_comentario = "Alumno reprobado en su materia recursada";
                        $bandera_baja= "[$datenow2][$conta][alumno: $alumno]=> Alumno reprobó el recurso ";
                    }
                }
        }

        if ( isset($bandera_baja)  ) {
            if ($bandera_baja != null) {
                $alumnomodel =  new AlumnosModel();
                $datos = $alumnomodel->find($alumno);
                if ( $datos) {
                    $datos->comentario = $bandera_comentario;
                    try{
                        $alumnomodel->update($alumno,$datos);
                        $alumnomodel->delete($alumno);
                        $status = true;
                    }catch (\Exception $ex){
                        $status = true;
                        $bandera_baja= "[$datenow2][$conta][alumno: $alumno]=> Error al dar de baja al alumno [error:  $ex ]";
                    }
                }
            }
        }
        $log= [
            'status'=> $status,
            'mensaje' => $bandera_baja
        ];
        return $log;
    }
    public function nuevosemestre(){
        $this->bajas();
        $this->procesoreinscripcion();
    }
}
