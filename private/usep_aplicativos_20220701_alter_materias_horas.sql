-- cambios de horas totales en materias 
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-29';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-30';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-31';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-32';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-33';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-34';

UPDATE `aca_materias` SET `materia_horas_semanales` = '25' WHERE `aca_materias`.`materia_clave` = 'LEO-35';

UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-15';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-16';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-17';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-18';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-19';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-20';

UPDATE `aca_materias` SET `materia_horas_semanales` = '25' WHERE `aca_materias`.`materia_clave` = 'LEO-21';

UPDATE `aca_materias` SET `materia_horas_semanales` = '2' WHERE `aca_materias`.`materia_clave` = 'LEO-07';
UPDATE `aca_materias` SET `materia_horas_semanales` = '2' WHERE `aca_materias`.`materia_clave` = 'LEO-06';
UPDATE `aca_materias` SET `materia_horas_semanales` = '2' WHERE `aca_materias`.`materia_clave` = 'LEO-05';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-04';
UPDATE `aca_materias` SET `materia_horas_semanales` = '2' WHERE `aca_materias`.`materia_clave` = 'LEO-03';
UPDATE `aca_materias` SET `materia_horas_semanales` = '4' WHERE `aca_materias`.`materia_clave` = 'LEO-02';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'LEO-01';

-- MATERIAS MED
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-111';
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-112';
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-113';
UPDATE `aca_materias` SET `materia_horas_semanales` = '6' WHERE `aca_materias`.`materia_clave` = 'MC-114';
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-115';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-116';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-117';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-TUT';

UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-211';
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-212';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-213';
UPDATE `aca_materias` SET `materia_horas_semanales` = '2' WHERE `aca_materias`.`materia_clave` = 'MC-214';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-215';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-216';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'OPT-112';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'OPT-113';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'OPT-114';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-218';

UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-SIM';
UPDATE `aca_materias` SET `materia_horas_semanales` = '5' WHERE `aca_materias`.`materia_clave` = 'MC-311';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-312';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-313';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-314';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-315';
UPDATE `aca_materias` SET `materia_horas_semanales` = '3' WHERE `aca_materias`.`materia_clave` = 'MC-316';
UPDATE `aca_materias` SET `materia_horas_semanales` = '4' WHERE `aca_materias`.`materia_clave` = 'MC-317';
UPDATE `aca_materias` SET `materia_horas_semanales` = '4' WHERE `aca_materias`.`materia_clave` = 'MC-318';