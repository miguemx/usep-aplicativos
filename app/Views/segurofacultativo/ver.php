<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">

    <h2>Seguro facultativo</h2>
    <hr />
    <div class="row">
        <div class="col-sm-6">
            <form method="post">
                <div class="col-sm-12">
                    <label for="apPaterno" class="form-label">Nombre Completo</label>
                    <input type="text" class="form-control" id="nombre" value="<?php echo $alumno->apPaterno.' '.$alumno->apMaterno.' '.$alumno->nombres.' '; ?>" disabled="disabled" />
                </div>
                <div class="col-sm-12">
                    <label for="apPaterno" class="form-label">CURP</label>
                    <input type="text" class="form-control" id="curp" value="<?php echo $alumno->curp ?>" disabled="disabled" />
                </div>
                <div class="col-sm-12">
                    <label for="apPaterno" class="form-label">Licenciatura</label>
                    <input type="text" class="form-control" id="carrera" value="<?php echo $alumno->carrera->nombre ?>" disabled="disabled" />
                </div>

                <div class="col-sm-12">
                    <label for="apPaterno" class="form-label">NSS</label>
                    <input type="text" class="form-control" id="ssn" name="ssn" value="<?php echo $alumno->ssn; ?>"  />
                </div>

                <div class="col-sm-12">
                    <label for="apPaterno" class="form-label">Observaciones</label>
                    <textarea class="form-control" name="observaciones"><?php echo $seguro->comentarios ?></textarea>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="chkCompletado" name="completado">
                    <label class="form-check-label" for="chkCompletado">
                        El expediente está correcto
                    </label>
                </div>

                <button type="submit" name="gf" value="1" class="btn btn-secondary">
                    Guardar
                </button>

            </form>
        </div>

        <div class="col-sm-6">
            <div  id="example1"></div>
        </div>
    </div>
    
    


    <script src="https://unpkg.com/pdfobject@2.2.5/pdfobject.min.js "></script>
    <script>PDFObject.embed('<?php echo base_url("RenderFiles/Seguro")."/".$alumno->id; ?>', "#example1");</script>
    <style>
        .pdfobject-container { height: 400px; border: 1rem solid rgba(0,0,0,.1); }
    </style>
    
    
</div>

<?php echo $this->endSection() ?>