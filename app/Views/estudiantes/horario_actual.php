<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">

    <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
        <div class="text-center form-group mt-4">
            <h4>
                <?php echo "Horario del periodo ".$periodo ;
                
                ?>
            </h4>
            <?php if (isset($mensajeperiodo)) : ?>
                <h5 >Estimado estudiante USEP, este horario es de manera provisional, el cual podría cambiar para su beneficio.</h5>
            <?php endif; ?>
        </div>
        <table class="table table-borderless table-hover mt-3 text-center">
            <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                <tr>
                    <th scope="row">HORA</th>
                    <th>LUNES</th>
                    <th>MARTES</th>
                    <th>MIERCOLES</th>
                    <th>JUEVES</th>
                    <th>VIERNES</th>
                    <th>SABADO</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $indicador = 0;
                $hora = 7;
                ?>
                <?php $bandera_row_dias = [0, 0, 0, 0, 0, 0, 0]; ?>
                <?php while ($hora < 21) : ?>
                    <?php $index = 0; ?>
                    <tr>
                        <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                        <?php foreach ($dias as $dia) : ?>
                            <?php //echo json_encode($bandera_row_dias)." $indicador <br>" ?>
                            <?php $bandera_coincidencia = false; ?>
                            <?php foreach ($grupos as $grupo) : ?>
                                <?php if ($grupo->dia == $dia) : ?>
                                    <?php if ($grupo->h_ini == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                        <?php
                                        $inicio = explode(':', $grupo->h_ini);
                                        $fin = explode(':', $grupo->h_fin);
                                        $row_span = date_diff( date_create($grupo->h_ini),date_create($grupo->h_fin));
                                        //$diferencia_minutos = (intval($fin[1]) - intval($inicio[1]));
                                        ?>
                                        <td class="align-middle" rowspan="<?php echo (($row_span->h >= 1)?$row_span->h:1) ;  ?>">
                                            <div <?php echo $grupo->h_inicio . " " . $grupo->h_fin ?>>
                                                <?php if ($row_span->h  >= 1) {
                                                    $bandera_row_dias[$index] += $row_span->h;
                                                } ?>
                                                <?php echo  $grupo->materia . '<br>' . (($grupo->iddocente == 1) ? 'Por asignar' : mb_strtoupper($grupo->app_docente . ' ' . $grupo->apm_docente . ' ' . $grupo->n_docente)) . "<br>".(($grupo->aula==1 || $grupo->aula=='SM'||$grupo->aula==101)?'EN LINEA':$grupo->aul_nombre) . "<br>". $grupo->seccion //. "<br> $grupo->dia $grupo->h_ini $grupo->h_fin" ?>
                                                <?php //die(); ?>
                                            </div>
                                        </td>
                                        <?php $bandera_coincidencia = true; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if (!$bandera_coincidencia) : ?>
                                <?php if ($bandera_row_dias[$index] > 1) : ?>
                                    <?php $bandera_row_dias[$index]--; ?>
                                <?php else : ?>
                                    <td style="background-color: #bdb994;">
                                        <?php //echo $dia . " " . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59'))." ".$indicador ?>
                                    </td>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $index++; ?>
                            <?php $indicador++; ?>
                        <?php endforeach; ?>
                    </tr>
                    <?php $hora++; ?>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo $this->endSection() ?>