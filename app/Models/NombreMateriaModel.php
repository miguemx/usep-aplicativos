<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\NombreMateria;

class NombreMateriaModel extends Model {
    protected $table      = 'view_grupo_detalles';
    protected $primaryKey = 'grupo_id';

    protected $returnType    = 'App\Entities\NombreMateria';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca un grupo con espacio para asignar a un alumno
     */
    

    public function buscamaterias($periodo,$docente){
        $consulta = $this->where('periodo_id',$periodo)->where('empleado_id',$docente)->findAll();
        
        return $consulta;
    }

}