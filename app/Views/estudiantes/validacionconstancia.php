<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>



<div style="font-family: Arial, Helvetica, sans-serif;  background-color: #8a0933; height: 25px; width: 100%; border: 1px solid #8a0933">
    &nbsp;
</div>
<div style="font-family: Arial, Helvetica, sans-serif; background-image: url('<?php echo base_url('img/fondos/constancia_cuadros.png') ?>'); background-repeat: no-repeat; background-position: right; background-size: 70%; height: 250px; width: 100%; border-top: 1px solid #8a0933">
    &nbsp;
</div>
<div class="container">
    <h3>Constancia de estudios <?php echo $folio ?> encontrada</h3>

    La constancia de estudios presentada, corresponde a:<br />
    <strong>Estudiante: </strong> <?php echo $nombre; ?><br />
    <strong>Carrera: </strong> <?php echo $carrera; ?><br />
    <strong>Periodo: </strong> <?php echo $periodo; ?><br />

</div>

<img src="<?php echo base_url('img/fondos/constancia_footer_fondo.png'); ?>" />


<?php echo $this->endSection() ?>