<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AspirantesTodosCamposModel extends Model {
    protected $table      = 'view_aspirantes_campostodos';
    protected $primaryKey = 'campo_id';

    protected $returnType    = 'App\Entities\CampoTodos';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * obtiene todos los campos de un periodo en particular
     * @param periodo el Id del periodo
     * @return campos los campos encontrados para el periodo, ordenados por etapa y formulario, en arreglo, o vacío si no existen
     */
    public function periodo($periodo) {
        $campos = $this->where( 'etapa_periodo', $periodo )->findAll();
        return $campos;
    }

    /**
     * devuelve los campos de un periodo, usando como llave del arreglo el ID del campo
     * @param periodo el Id del periodo
     * @return campos los campos en un arreglo con las llaves de Id del campo, o vacío si no encuentra
     */
    public function periodoArray( $periodo ) {
        $camposDb = $this->periodo( $periodo );
        $campos = [];
        foreach ( $camposDb as $campo ) {
            $campos[ $campo->id ] = $campo;
        }
        return $campos;
    }
    
}