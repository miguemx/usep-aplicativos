<?php

namespace App\Entities;

use CodeIgniter\Entity;

class EvaluacionesPreguntas extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'formulario' => null,
        'titulo' => null,
        'nombre' => null,
        'tipo' => null,
        'instucciones' => null,
        'orden' => null,
        'obligatorio' => null,
        'clasificacion' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'id' => 'preguntas_id',
        'formulario' => 'preguntas_formulario',
        'titulo' => 'preguntas_titulo',
        'nombre' => 'preguntas_titulo',
        'tipo' => 'preguntas_tipo',
        'orden' => 'preguntas_orden',
        'obligatorio' => 'preguntas_obligatorio',
        'clasificacion' => 'preguntas_clasificacion',

        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
