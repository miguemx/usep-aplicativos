-- creación del rol de subdirección académica
INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES ('20', 'SUBDIRECCION ACADEMICA', 'subacademica');

-- creación de los permisons para el nuevo rol
INSERT INTO `aplicativo_permisos` (`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES
(20, 'bibliotecas', '*'),
(20, 'escolar', 'capturacalificaciones'),
(20, 'estudiantes', '*'),
(20, 'home', '*'),
(20, 'renderfiles', '*'),
(20, 'escolar', 'buscagrupo'),
(20, 'escolar', 'capturacalificacionesporgrupo'),
(20, 'docente', '*'),
(20, 'escolar', 'listaescolar'),
(20, 'escolar', 'generalistapdf'),
(20, 'escolar', 'consultatablalista'),
(20, 'docente', '*'),
(20, 'programacionacademica', 'menu'),
(20, 'programacionacademica', 'mapaaulas'),
(20, 'programacionacademica', 'main'),
(20, 'programacionacademica', 'aulavirtual'),
(20, 'programacionacademica', 'getlistadohorario'),
(20, 'programacionacademica', 'horariodocente'),
(20, 'programacionacademica', 'horariosecciones'),
(20, 'bibliotecas', '*'),
(20, 'calificaciones', '*'),
(20, 'estudiantes', '*'),
(20, 'home', '*'),
(20, 'renderfiles', '*'),
(20, 'docente', '*'),
(20, 'escolar', 'listaescolar'),
(20, 'escolar', 'consultatablalista'),
(20, 'escolar', 'generalistapdf');

--asignación de rol a usuario especificado
UPDATE `aplicativo_usuarios` SET `usuario_rol` = '20' WHERE `aplicativo_usuarios`.`usuario_correo` = 'flavia.aguilar@usalud.edu.mx';