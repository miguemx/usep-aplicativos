<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">

    <table class="table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
        <thead class="thead-inverse|thead-default">
            <tr>
                <th>Correo</th>
                <th>Hora de generación de QR</th>
                <th>Nombre del empleado(a)</th>
                <TH>Conteo de síntomas mayores</TH>
                <TH>Conteo de Síntomas menores</TH>
                <TH>Oxigenación</TH>
                <TH>Temperatura</TH>
            </tr>
        </thead>
        <tbody>
            <?php
            
            
                foreach ($arreglo as $datos) {
            ?>
                    <tr>
                        <td><?php echo $datos['seccion']  ?></td>
                        <td><?php echo $datos['materia']  ?></td>
                        <td> <?php echo $datos['dia1'] . " " . $datos['lunes'] . " " . $datos['lunesfin'] . " " . $datos['lunesa']       ?> </td>
                        <td> <?php echo $datos['dia2'] . " " . $datos['martes'] . " " . $datos['martesfin'] . " " . $datos['martesa']     ?> </td>
                        <td> <?php echo $datos['dia3'] . " " . $datos['miercoles'] . " " . $datos['miercolesfin'] . " " . $datos['miercolesa']  ?> </td>
                        <td> <?php echo $datos['dia4'] . " " . $datos['jueves'] . " " . $datos['juevesfin'] . " " . $datos['juevesa']     ?> </td>
                        <td> <?php echo $datos['dia5'] . " " . $datos['viernes'] . " " . $datos['viernesfin'] . " " . $datos['viernesa']   ?> </td>
                        <td> <?php echo $datos['dia6'] . " " . $datos['sabado'] . " " . $datos['sabadofin'] . " " . $datos['sabadoa']     ?> </td>
                    </tr>
            <?php
                }
            ?>
        </tbody>
    </table>
</div>


<?php echo $this->endSection() ?>