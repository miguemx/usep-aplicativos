<?php
namespace App\Models;
use CodeIgniter\Model;

class ClinicasModel extends Model {
    protected $table      = 'cat_clinicas_imss';
    protected $primaryKey = 'imss_id';

    protected $returnType    = 'App\Entities\ClinicaImss';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function getListado() {
        $clinicas = $this->orderBy('imss_localidad')->findAll();
        return $clinicas;
    }
    
}
