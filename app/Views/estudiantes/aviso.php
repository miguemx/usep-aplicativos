<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container">
    <div class="card">
        <img class="card-img-top" src="holder.js/100x180/" alt="">
        <div class="card-body">
            <h4 class="card-title">AVISO IMPORTANTE</h4>
            <p class="card-text">Se te informa que el horario de clases se encontrará disponible en esta sección a partir del día viernes 14 de enero del 2022, a las 17:00 horas.</p>
            <p class="card-text">De igual manera, recibirás en tu correo institucional las invitaciones (ligas de acceso) a cada una de las asignaturas que se muestran en tu horario; por ejemplo: si tienes 7 materias en tu horario, deberás tener 7 invitaciones en tu correo electrónico.</p>
            <p class="card-text">Cualquier duda nos encontramos a tus órdenes en escolar@usalud.edu.mx</p>
        </div>
    </div>

<?php echo $this->endSection() ?>