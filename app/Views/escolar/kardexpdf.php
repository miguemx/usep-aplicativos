<?php
$font_size = ' font-size: 6.5px ;';
$font_size_firma = 'font-size: 5px;';
?>
<style>
    * {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6.5px;
    }

    .table-main {
        text-align: center;
        border: 1px solid black;
        border-collapse: collapse;

        font-size: 6.5px;
    }

    .main {
        width: 80%;
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 6.5px;
    }

    .container {
        margin: auto;
        padding: auto;

        font-size: 6.5px;
    }

    .center {
        margin-left: auto;
        margin-right: auto;
    }

    * {
        box-sizing: border-box;
    }

    /* Create a two-column layout */
    .column {
        float: left;
        width: 50%;
        padding: 5px;
    }

    /* Clearfix (clear floats) */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }
</style>
<div style="font-family: Arial, Helvetica, sans-serif; padding: 30px; padding-bottom: 1px; padding-top: 1px;">
    <table width="100%">
        <tr>
            <td style="text-align: left"><img src="<?php echo base_url('img/logos/USEP.png');  ?>"></td>
            <td style="text-align: right"><img src="<?php echo base_url('img/logos/Gobierno.png');  ?>"></td>
            <td style="text-align: left">&nbsp;<br />&nbsp;<br />&nbsp;</td>
            <td style="text-align: right"></td>
        </tr>
    </table>
</div>
<div class="container" style="<?php echo $font_size ?>;">
    <div>
        <div class="row">
            <div class="col">
                <table class="table table-striped" style=" font-size: 6.5px; width: 80%; margin:auto">
                    <tr>
                        <td colspan="3"></td>
                        <td><strong>FOLIO: </strong><?php $fecha = explode("-", $documento->fechaExp);
                                                    echo $folio . "/" . $fecha[0]; ?></td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td><strong>CCT: </strong>21ESU0013X</td>
                    </tr>
                    <tr>
                        <td><strong>NOMBRE:</strong> </td>
                        <td><?php echo $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres; ?></td>
                        <td><strong>PROGRAMA</strong></td>
                        <td><?php echo $carrera->nombre; ?></td>
                    </tr>
                    <tr>
                        <td><strong>MATRÍCULA:</strong></td>
                        <td><?php echo $alumno->id; ?></td>
                        <td><strong>CLAVE DEL PROGRAMA:</strong></td>
                        <td><?php echo $carrera->clavePrograma ?></td>
                    </tr>
                    <tr>
                        <td><strong>ESTATUS:</strong></td>
                        <td><?php echo $alumno->status; ?></td>
                        <td><strong>PERIODO:</strong></td>
                        <td><?php echo $alumno->periodo ?></td>
                    </tr>

                    <tr>
                        <td><strong>FACULTAD / ESCULA:</strong></th>
                        <td><?php echo (($alumno->id == 1) ? 'ENFERMERIA Y OBSTETRICIA' : 'MÉDICO CIRUJANO'); ?></td>
                        <td><strong>SECCIÓN:</strong></td>
                        <td><?php echo $carrera->clave ?></td>
                    </tr>
                    <tr>
                        <td><strong>NIVEL:</strong></td>
                        <td>LICENCIATURA</td>
                    </tr>
                    <tr>
                        <td><strong>CAMPUS:</strong></td>
                        <td>PUEBLA</td>
                    </tr>
                </table>
            </div>
            <div class="col">

            </div>
        </div>

    </div>
    <?php
    $contador_filas = 1;
    $total_materias_apr = 0;
    $total_materias_rep = 0;
    $creditos = 0;
    $promedio_general = 0;
    $numPeriodosGeneral = 0;
    $sumGeneral = 0;
    $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    ?>
    <?php if (count($kardex) < 1) : ?>
        <div class="alert alert-info">
            El estudiante no cuenta con materias calificadas.
        </div>
    <?php else : ?>
        <?php foreach ($kardex as $periodo => $materias) : ?>
            <?php if ($contador_filas > 4) :
                $contador_filas = 0;
            ?>
                <div style="margin-top:400px;">
                    <div style="font-family: Arial, Helvetica, sans-serif; padding: 30px; padding-bottom: 1px; padding-top: 1px;">
                        <table width="100%">
                            <tr>
                                <td style="text-align: left"><img src="<?php echo base_url('img/logos/USEP.png');  ?>"></td>
                                <td style="text-align: right"><img src="<?php echo base_url('img/logos/Gobierno.png');  ?>"></td>
                                <td style="text-align: left">&nbsp;<br />&nbsp;<br />&nbsp;</td>
                                <td style="text-align: right"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($periodo != $alumno->periodo && $materias[0]->estado == 'FINALIZADO') : ?>
                <?php $numPeriodosGeneral++; ?>
                <div class="center" style="margin-top:20px">
                    <div>
                        <h5 style="margin-left: 80px;"> PERIODO: <?php echo $periodo . ' ' . $materias[0]->periodoNombre; ?></h5>
                    </div>
                    <table class="main" style="margin-left: auto;margin-right:auto;">
                        <thead class="table-main">
                            <tr>
                                <th>SEMESTRE</th>
                                <th>CLAVE</th>
                                <th>CREDITOS</th>
                                <th style=" text-align:left">ASIGNATURA</th>
                                <th>CALIFICACIÓN</th>
                                <th>TIPO DE EVALUACIÓN</th>
                                <th>SECCIÓN</th>
                            </tr>
                        </thead>
                        <tbody class="table-main">
                            <?php
                            $sum = 0;
                            $prom = 0;
                            $mat = 0;
                            $aprobadas = 0;
                            $reprobadas = 0;
                            ?>
                            <?php foreach ($materias as $materia) : ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo (($materia->semestre < 10) ? '0' . $materia->semestre : $materia->semestre) ?></td>
                                    <td style="text-align: center;"><?php echo $materia->materiaClave ?></td>
                                    <td style="text-align: center;"><?php echo (($materia->creditos < 10) ? '0' . $materia->creditos : $materia->creditos) ?></td>
                                    <td style=" text-align:left;  width:200px; min-width:200px; max-width:200px;"><?php echo $materia->materia ?></td>
                                    <td style="text-align: center;">
                                        <?php
                                        $calif = (($materia->extraordinario) ? $materia->extraordinario : $materia->ordinario);
                                        if ($calif < 6) $reprobadas++;
                                        else $aprobadas++;
                                        $sum += $calif;
                                        ?>
                                        <span class="view-normal-<?php echo $materia->grupo; ?>"><?php echo $calif; ?></span>
                                    </td>
                                    <td style="text-align: center;">
                                        <span class="view-normal-<?php echo $materia->grupo; ?>"><?php
                                                                                                    if ($materia->ordinario == '' && $materia->extraordinario == '') {
                                                                                                        echo '---';
                                                                                                    } elseif ($materia->extraordinario) {
                                                                                                        echo 'EXT';
                                                                                                    } else {
                                                                                                        echo 'ORD';
                                                                                                    }
                                                                                                    ?></span>

                                    </td>
                                    <td style="text-align: center;"><?php echo $materia->grupo ?> </td>
                                </tr>
                                <?php
                                $mat++;
                                (($materia->aprobada == 1) ? $total_materias_apr++ : $total_materias_rep++);
                                (($materia->aprobada == 1) ? $creditos += $materia->creditos : '');
                                ?>

                            <?php endforeach; ?>
                            <?php
                            $prom = $sum / $mat;
                            $sumGeneral += number_format($prom, 2, '.', ',');
                            ?>
                            <tr style="  border: 1px solid black; border-collapse: collapse; ">
                                <td colspan="7" style="text-align: center;">
                                    <strong>
                                        Núm. de asignaturas aprobadas: <?php echo $aprobadas; ?>&nbsp;&nbsp;&nbsp;
                                        Núm. de asignaturas reprobadas: <?php echo $reprobadas; ?>&nbsp;&nbsp;&nbsp;
                                        Promedio: <?php echo number_format($prom, 2, '.', ','); ?>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php $contador_filas++ ?>
            <?php elseif ($periodo == $alumno->periodo) : ?>
                <div style="display: block;">
                    <div style=" margin-left: 80px;">

                        <h5> CARGA EN PROCESO</h5>
                        <h5> PERIODO: <?php echo $periodo . ' ' . $materias[0]->periodoNombre; ?></h5>
                    </div>
                    <div style="width:380px; float:left; margin-left: 80px;">
                        <table style="width: 380px; font-size: 6.5px; border: 1px solid black; border-collapse: collapse; ">
                            <thead>
                                <tr style="  border: 1px solid black; border-collapse: collapse; ">
                                    <th style="margin:10px;">SEMESTRE</th>
                                    <th>CLAVE</th>
                                    <th>CREDITOS</th>
                                    <th style=" text-align:left">ASIGNATURA</th>
                                    <th>SECCIÓN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($materias as $materia) : ?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo (($materia->semestre < 10) ? '0' . $materia->semestre : $materia->semestre) ?></td>
                                        <td style="text-align: center;"><?php echo $materia->materiaClave ?></td>
                                        <td style="text-align: center;"><?php echo (($materia->creditos < 10) ? '0' . $materia->creditos : $materia->creditos) ?></td>
                                        <td style=" text-align:left;"><?php echo $materia->materia ?></td>
                                        <td style="text-align: center; "><?php echo $materia->grupo ?> </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div style="width:250px; float:left; margin-left:75px;">
                        <table style="width: 200px; font-size: 6.5px; border: 1px solid black; border-collapse: collapse; ">
                            <tbody>
                                <tr>
                                    <td>MATERIAS APROBADAS:</td>
                                    <td style="text-align: center; "><?php echo (($total_materias_apr == 0) ? '0' : (($total_materias_apr < 10) ? '0' . $total_materias_apr : $total_materias_apr)) ?></td>
                                </tr>
                                <tr>
                                    <td>MATERIAS REPROBADAS:</td>
                                    <td style="text-align: center;"><?php echo (($total_materias_rep == 0) ? '0' : (($total_materias_rep < 10) ? '0' . $total_materias_rep : $total_materias_rep)) ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL DE MATERIAS P.E:</td>
                                    <td style="text-align: center;"><?php echo (($alumno->carrera == 1) ? '56' : '65') ?></td>
                                </tr>
                                <tr>
                                    <td>PORCENTAJE DE AVANCE:</td>
                                    <td style="text-align: center;"><?php

                                                                    echo number_format(floatval(($total_materias_apr * 100) / (($alumno->carrera == 1) ? 56 : 56)), 1);

                                                                    ?>%</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>PROMEDIO:</td>
                                    <td style="text-align: center;"><?php echo (($sumGeneral > 0) ? ($sumGeneral / ($alumno->semestre - 1)) : 'N/A') ?></td>
                                </tr>
                                <tr>
                                    <td>CRÉDITOS OBTENIDOS:</td>
                                    <td style="text-align: center;"><?php echo $creditos ?></td>
                                </tr>
                                <tr>
                                    <td>CRÉDITOS TOTALES:</td>
                                    <td style="text-align: center;">
                                        <?php echo ($alumno->carrera == 1) ? '332' : '586' ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div style="padding-top: 10px;  background-image: url('<?php echo $qr ?>'); background-repeat: no-repeat; background-position: left bottom; background-size: 10%; margin-left:20px ">
                    <div class="center" style="text-align: left;  width:70%">
                        EL PRESENTE DOCUMENTO HA SIDO COTEJADO CON LAS ACTAS QUE OBRAN EN EL ARCHIVO ESCOLAR DE LA UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA SE EXTIENDE EL PRESENTE A SOLICITUD DE LA PERSONA INTERESADA Y PARA LOS FINES QUE A ELLA CONVENGAN.
                    </div>

                    <div style="text-align: center;  margin-top:40px;">
                        <strong>ATENTAMENTE:</strong><br />
                        <strong>&quot;Un pueblo sano, hace una nación vigorosa&quot;</strong><br />
                        H. Puebla de Z.; a <?php echo date('d') ?> de <?php echo $meses[date('n') - 1] ?> del <?php echo date('Y') ?> <br>
                    </div>
                    <div style=" height:100px; text-align: center; background-image: url('<?php echo base_url('img/credencial/firmarector.jpg') ?>'); background-repeat: no-repeat; background-position: center; background-size: 13%; ">
                        <p style="margin-top: 53px;">
                            <strong>Dr. José Hugo Eloy Meléndez Aguilar</strong><br />
                            Rector
                        </p>
                    </div>
                    <!-- <div style="text-align: left; background-color:red;">
                        <img src="<?php //echo $qr 
                                    ?>" width="90" />
                    </div> -->
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>