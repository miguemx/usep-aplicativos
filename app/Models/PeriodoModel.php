<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Alumno;

class PeriodoModel extends Model
{
    protected $table      = 'aca_periodos';
    protected $primaryKey = 'periodo_id';

    protected $returnType    = 'App\Entities\Periodo';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'periodo_id', 'periodo_nombre', 'periodo_inicio', 'periodo_fin', 'periodo_estado','periodo_const_inicio','periodo_const_fin','periodo_const_evaluaciones','periodo_const_vacaciones','periodo_const_administrativas','periodo_const_receso',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    public function lista()
    {
        $this->orderBy('periodo_inicio', 'ASC');
        return $this->findAll();
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if (array_key_exists('id', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['id'])) {
                $this->where('alumno_id', $filtros['id']);
            }
        }
        if (array_key_exists('apPaterno', $filtros)) { // -- filtrar por apPaterno
            if (strlen($filtros['apPaterno'])) {
                $this->where('alumno_ap_paterno', $filtros['apPaterno']);
            }
        }
        if (array_key_exists('apMaterno', $filtros)) { // -- filtrar por materno
            if (strlen($filtros['apMaterno'])) {
                $this->where('alumno_ap_materno', $filtros['apMaterno']);
            }
        }
        if (array_key_exists('nombre', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['nombre'])) {
                $this->where('alumno_nombres', $filtros['nombre']);
            }
        }

        $this->orderBy('alumno_ap_paterno', 'ASC');
        $this->orderBy('alumno_ap_materno', 'ASC');
        $this->orderBy('alumno_nombres', 'ASC');
    }

    /**
     * @return query contiene el id del periodo en estatus de inicio
     */
    public function getPeriodoEstatus($estatus)
    {
        return $this->where('periodo_estado', $estatus)->find();
        // return $query->getInsertID();
    }

    /**
     * @return query peridos disponibles para la programacion academica
     */
    public function getPeriodoProgramacionAcademica()
    {
        $query = $this->getPeriodoEstatus('INICIO');
        if ($query) {
            return $query[0]->id;
        } else {
            $query = $this->getPeriodoEstatus('INSCRIPCION');
            if ($query) {
                return $query[0]->id;
            } else {
                $query =  $this->getPeriodoEstatus('CURSO');
                if ($query) {
                    return $query[0]->id;
                } else return false;
            }
        }
    }

    /**
     * @param periodo ID del periodo
     * @return true si el periodo tiene permisos de asignacion
     * 
     */
    public function PermisoEdicionPeriodo($periodo)
    {
        $query = $this->find($periodo);
        switch ($query->periodo_estado) {
            case 'FINALIZADO':
                return false;
                break;
            case 'INICIO':
                return true;
                break;
            case 'INSCRIPCION':
                return true;
                break;
            case 'CURSO':
                return false;
                break;
            case 'CALIFICACIONES':
                return false;
                break;
        }
    }

    /**
     * @return query peridos disponibles para la consulta de horario de los docentes
     */
    public function getPeriodoHorarioDocente()
    {
        $query = $this->getPeriodoEstatus('CURSO');
        if ($query) {
            return $query[0]->id;
        } else {
            $query = $this->getPeriodoEstatus('CALIFICACIONES');
            if ($query) {
                return  $query[0]->id;
            } else {
                return false;
            }
        }
    }
    /**
     * @return this periodoen estatus de inicio para mostrar mensaje en vista de horario de estudiante
     */
    public function getPeriodohorario(){
        return $this->where('periodo_estado','CURSO')->find();
    }
    /**
     * @return this periodo periodo por id
     */
    public function getPeriodoById($id){
        return $this->where('periodo_id',$id)->first();
    }

    /**
     * @return consulta el arreglo de periodos que no se encuentren en estatus de Finalizado
     */
    public function getPeriodoSinFinalizar()
    {
         $this->where('periodo_estado !=','FINALIZADO');
         $this->orderBy('periodo_inicio', 'ASC');
         return $this->find();
    }

    /**
     * determina el periodo y toda su información con la fecha del sistema
     * @return consulta información del periodo
     */
    public function periodoFsys (){
        $hoy =  date('Y-m-d');
        $consulta =$this->where ('periodo_inicio <=',$hoy)
                        ->where ('periodo_fin >=',$hoy)
                        ->first();
                        //->getCompiledSelect();
        return $consulta;
    }


}
