<?php

namespace App\Controllers;

use App\Models\AspirantesGeneralesModel;
use App\Models\AlumnosModel;
use App\Models\UsuariosModel;
use App\Models\CarrerasModel;
use App\Models\CredencialModel;
use App\Models\AspiranteRespuestasModel;
use App\Models\GruposMateriasModel;
use App\Models\GrupoModel;
use App\Models\GrupoAlumnoModel;
use App\Models\AlumnosDocumentosFisicosModel;
use App\Models\HorarioAlumnosModel;

use App\Entities\Alumno;
use App\Entities\Usuario;
use App\Entities\Credencial;
use App\Entities\GrupoAlumno;
use App\Entities\AlumnoDocumentoFisico;
use App\Entities\PagoAlumno;
use App\Entities\HorarioAlumnos;
use \App\Libraries\Cifrado;
use App\Models\MateriaModel;
use App\Models\PagosAlumnosModel;
use App\Models\PagosConceptosModel;
use App\Models\RosModel;
use App\Models\AspirantesRespuestasModel;

class Inscripcion extends BaseController
{

    private $aspirantesModel;
    private $usuariosModel;
    private $alumnosModel;
    private $carrerasModel;
    private $aspirantesRespuestasModel;
    private $gruposAlumnosModel;
    private $pagosAlumnosModel;
    private $pagosConceptosModel;
    private $materiasModel;
    private $rosModel;
    private $aspiranteRespuestaModel;

    public function __construct()
    {
        $this->aspirantesModel = new AspirantesGeneralesModel();
        $this->alumnosModel = new AlumnosModel();
        $this->usuariosModel = new UsuariosModel();
        $this->carrerasModel = new CarrerasModel();
        $this->aspirantesRespuestasModel = new AspiranteRespuestasModel();
        $this->gruposAlumnosModel = new GrupoAlumnoModel();
        $this->pagosAlumnosModel = new PagosAlumnosModel();
        $this->pagosConceptosModel = new PagosConceptosModel();
        $this->materiasModel = new MateriaModel();
        $this->rosModel = new RosModel();
        $this->aspiranteRespuestaModel = new AspirantesRespuestasModel();
    }

    /**
     * renderiza el buscador para iniciar el proceso de inscripción
     */
    public function index()
    {
        $aspirantemodel = new AspirantesGeneralesModel();
        $data = $this->request->getGet();
        if (!count($data) || !array_key_exists('folio', $data)) {
            $data = ['folio' => '', 'carrera' => '', 'curp' => '', 'estatus' => '', 'nombre' => '', 'apPaterno' => '', 'apMaterno' => ''];
        }
        // $data['mensaje'] =(($this->sesion->getFlashdata('msjrechazo'))?$this->sesion->getFlashdata('msjrechazo'):false);
        $data['aceptado'] = '1';
        $data['inscrito'] = '';
        $data['carrera_list'] = $this->carrerasModel->findAll();
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : '';
        $data['aspirantes'] = $this->aspirantesModel->buscar($data);
        $data['pager'] = $this->aspirantesModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        $data['allowEdit'] = ($this->sesion->get('rol_id') == '3' || $this->sesion->get('rol_id') == '1') ? true : false;
        return view('inscripcion/lista', $data);
    }

    /**
     * renderiza el expediente del aspirante
     */
    public function aspirante()
    {
        $folio = $this->request->getPost('folio');
        $data['folio'] = $folio;
        $aspirante = $this->aspirantesModel->find($folio);
        $data['errores'] = null;
        if (!is_null($aspirante)) {
            if ($aspirante->aceptado == '1') {
                $estadosModel = new \App\Models\EstadosModel();
                $municipiosModel = new \App\Models\MunicipiosModel();
                $carrerasModel = new \App\Models\CarrerasModel();
                $data['aspirante'] = $aspirante;
                $data['respuestas'] = $this->aspirantesRespuestasModel->getCamposVisibles($folio);
                $respuestas = $this->aspirantesRespuestasModel->getCamposVisibles($folio);
                $respuestas = $this->aspirantesRespuestasModel->getAspiranteArray($folio);
                $data['respuestasAll'] = $this->aspirantesRespuestasModel->getAspiranteArray($folio);
                $data['archivos'] = $this->aspirantesRespuestasModel->getDocumentosAspirantes($folio);
                $data['estados'] = $estadosModel->findAll();
                $data['municipios'] = $municipiosModel->findAll();
                $data['carreras'] = $carrerasModel->findAll();
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('inscripcion/datos', $data);
    }

    /**
     * toma la peticion para inscribir y genera la vista del resultado, ya sea por error o exito
     */
    public function inscribir()
    {
        $estadosModel = new \App\Models\EstadosModel();
        $municipiosModel = new \App\Models\MunicipiosModel();
        $carrerasModel = new \App\Models\CarrerasModel();
        $data['errores'] = null;
        $data['estados'] = $estadosModel->findAll();
        $data['municipios'] = $municipiosModel->findAll();
        $data['carreras'] = $carrerasModel->findAll();
        $data['aspirante'] = $this->aspirantesModel->find($this->request->getPost('id'));
        $data['respuestas'] = $this->aspirantesRespuestasModel->getCamposVisibles($this->request->getPost('id'));
        $data['archivos'] = $this->aspirantesRespuestasModel->getDocumentosAspirantes($this->request->getPost('id'));
        $data['respuestasAll'] = $this->aspirantesRespuestasModel->getAspiranteArray($this->request->getPost('id'));
        $datos = $this->request->getPost();
        // $this->cifrar();
        $validacion = $this->valida($datos, 'alumno');
        //var_dump($validacion);
        //die();
        $aspiranterespuestas = $this->actualizaAspirantes($datos);
        $aspirantegeneral = $this->actualizaAspirantegral($datos);
        if ($validacion === true && $aspiranterespuestas === true && $aspirantegeneral === true) {
            $inscrito = $this->inscribe($datos);
            if ($inscrito === true) {
                //$url = base_url('Inscripcion/Documentacion') . '/' . $this->cifrar($datos['id']);
                $url = base_url('/Inscripcion/Documentacion') . '/' . $datos['id'];
                return redirect()->to($url);
            } else {
                $data['errores'] = implode('<br />', $inscrito);
            }
        } else {
            $data['errores'] = implode('<br />', $validacion);
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('inscripcion/datos', $data);
    }

    /**
     * realiza el registro de un nuevo estudiante
     * @param datos un array asociativo con los datos del estudiante para crear su entidad
     * @return true en caso de que se registre de forma correcta
     * @return array un arreglo con los errores generados durante la creación
     * @todo obtener dinámicamente el periodo
     */
    private function inscribe($datos)
    {
        $errores = [];
        $alumno = $this->alumnosModel->find($datos['id']);
        if (is_null($alumno)) {
            $usuario = new Usuario();
            $alumno = new Alumno($datos);
            $documentoFisico = new AlumnoDocumentoFisico();
            try {
                $usuario->correo = $alumno->id . '@usalud.edu.mx';
                $usuario->rol = '4';
                $alumno->periodo = '2022A'; // @TODO obtener dinamicamente el periodo
                $alumno->semestre = '1';
                $alumno->status = 'ACTIVO';
                $alumno->correo = $alumno->id . '@usalud.edu.mx';
                $this->usuariosModel->insert($usuario);
                $this->alumnosModel->insert($alumno);
                $this->creaCredencial($datos);
                $aspirante = $this->aspirantesModel->find($datos['id']);
                $aspirante->inscrito = '1';
                $this->aspirantesModel->save($aspirante);
                $documentoFisico->id = $alumno->id;
                $adf = new AlumnosDocumentosFisicosModel();
                $adf->insert($documentoFisico);
                log_message("notice", "ASPIRANTE INSCRITO: {matricula} - {usuario}", ["matricula" => $alumno->id, 'usuario' => $this->sesion->get('useremail')]);
                return true;
            } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
                return true;
            } catch (\mysqli_sql_exception $ex) {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
                $errores[] = 'Ocurrió una inconsistencia de datos. Por favor verifique con el administrador del sistema.';
            } catch (\Exception $ex) {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
                $errores[] = 'Ocurrió un error inesperado.';
            }
        } else {
            $errores[] = 'El folio ya corresponde a un estudiante registrado.';
        }
        return $errores;
    }
    
/**
 * Actualiza los datos de aspirantes modificados en la tabla de aspirantes respuestas
 */
    private function actualizaAspirantes($datos){
        $matricula = $datos['id'];
        try {
            $datos_aspirante = [
                $datos['53'],$datos['41'],$datos['estadocivil'],$datos['numerohijos'],$datos['numerodependientes'],$datos['padremadresoltero'],$datos['trabaja'],
                $datos['beca'],$datos['comunidadindigena'],$datos['lenguaindigena'],$datos['tiposangre'],$datos['padecimiento'],$datos['discapacidad'],$datos['cualdiscapacidad'],
                $datos['certificadomedico'],$datos['lentes'],$datos['protesis'],$datos['serviciosalud'],$datos['vacunacovid'],$datos['parentescocontacto'],
                $datos['nombrecontacto'],$datos['telefonocontacto'],$datos['correocontacto']
            ];
            $arr_numeros = ['53','41','4','5','6','7','54','26','12','56','27','28','29','57','30','31','32','33','58','42','43','44','45'];
            for ($i = 0; $i < count($arr_numeros); $i++) {
                $update = [
                    'respuesta_valor' => $datos_aspirante[$i]
                ];
                $this->aspiranteRespuestaModel->updateaspirantes($matricula,$update,$arr_numeros[$i]);             
            }
            return true;
        } catch (\Exception $ex) {
            $errores = 'No fue posible actualizar los datos del aspirante. ' .$ex->getMessage();
        }
        return $errores;
    }
/**
 * Actualiza los datos de aspirantes modificados ne la tabla de aspirantes generales
 */
    private function actualizaAspirantegral($datos){
        $matricula = $datos['id'];
        try {
            $datos_aspirante = [
                'aspirante_curp' => $datos['curp'],
                'aspirante_sexo' => $datos['sexo'],
                'aspirante_ap_paterno' => $datos['apPaterno'],
                'aspirante_ap_materno' => $datos['apMaterno'],
                'aspirante_nombre' => $datos['nombre'],
                'aspirante_correo' => $datos['correoPersonal'],
                'aspirante_calle' => $datos['calle'],
                'aspirante_numero' => $datos['numero'],
                'aspirante_num_int' => $datos['interior'],
                'aspirante_colonia' => $datos['colonia'],
                'aspirante_cp' => $datos['cp'],
                'aspirante_pais' => $datos['pais'],
                'aspirante_estado' => $datos['estado'],
                'aspirante_municipio' => $datos['municipio'],
                'aspirante_nac_fecha' => $datos['fechaNacimiento'],
                'aspirante_nac_pais' => $datos['paisNacimiento'],
                'aspirante_nac_estado' => $datos['estadoNacimiento'],
                'aspirante_nac_municipio' => $datos['municipioNacimiento'],
                'aspirante_telefono' =>$datos['telefono']
            ];
            $this->aspirantesModel->updateaspirante($matricula,$datos_aspirante);             
            return true;
        } catch (\Exception $ex) {
            $errores = 'No fue posible actualizar los datos del aspirante. ' .$ex->getMessage();
        }
        return $errores;
    }
    /**
     * renderiza la vista para visualizar los documentos y marcar los entregados
     * @param cid el ID del estudiante cifrado
     */
    public function documentacion($cid)
    {
        // $matricula = $this->descifrar($cid);
        $matricula = ($cid);
        if ($this->request->getPost('guardar') === 'g') {
            $this->guardaRelDocumental($matricula, $this->request->getPost());
        }
        $alumno = $this->alumnosModel->find($matricula);
        if (!is_null($alumno)) {
            $documentosFísicosModel = new AlumnosDocumentosFisicosModel();
            $data['reldocumental'] = $documentosFísicosModel->find($alumno->id);
            $data['alumno'] = $alumno;
            $data['carrera'] = $this->carrerasModel->find($alumno->carrera);
            $data['menu'] = $this->sesion->get('menu');
            return view('inscripcion/documentos', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * renderiza el documento a imprimir
     * @param matricula el ID o matricula del alumno
     * @param documento el nombre del documento a renderizar
     */
    public function documento($matricula, $documento)
    {
        $alumno = $this->alumnosModel->find($matricula);
        if (!is_null($alumno)) {
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'legal', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
                'margin_left' => 0, 'margin_right' => 0, 'margin_top' => 30, 'margin_bottom' => 0, 'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            $mpdf->SetHTMLHeader(view('inscripcion/docheader'));
            $mpdf->SetHTMLFooter(view('inscripcion/docfooter'));
            $data['alumno'] = $alumno;
            $data['carrera'] = $this->carrerasModel->find($alumno->carrera);
            $documentosFísicosModel = new AlumnosDocumentosFisicosModel();
            $data['reldocumental'] = $documentosFísicosModel->find($alumno->id);
            try {
                $doc = 'docconsentimiento';
                switch ($documento) {
                    case 'consentimiento':
                        $doc = 'docconsentimiento';
                        break;
                    case 'reglamento':
                        $doc = 'docreglamento';
                        break;
                    case 'responsiva':
                        $doc = 'docresponsiva';
                        break;
                    case 'adeudo':
                        $doc = 'docadeudo';
                        break;
                    case 'horario':
                        $doc = 'dochorario';
                        break;
                    default:
                        break;
                }

                if ($doc == 'dochorario') {
                    $horarioModel = new HorarioAlumnosModel();
                    $horario = $horarioModel->rejillaHorario($matricula);
                    $data['horario'] = $horario;
                }
                $html = view('inscripcion/' . $doc, $data);
                $mpdf->WriteHTML($html);
                $mpdf->Output($documento . '-' . $alumno->id . '.pdf', \Mpdf\Output\Destination::INLINE);
                die();
            } catch (\Exception $x) {
                log_message("error", "ERROR {exception}", ["exception" => $x]);
            }
            return true;
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * rechaza un aspirante para evitar su inscripción
     */
    public function rechazar()
    {
        $pantalla = $this->request->getPost('pantalla');
        $matricula = $this->request->getPost('id');
        $razon = $this->request->getPost('razon');
        $aspirante = $this->aspirantesModel->find($matricula);
        if (!is_null($aspirante)) {
            log_message("notice", "INICIO DE RECHAZO DE {matricula}", ["matricula" => $matricula]);
            try {
                if ($pantalla != '1') {
                    $credencialModel = new CredencialModel();
                    $credencialModel->delete($matricula, true);
                    $documentosModel = new AlumnosDocumentosFisicosModel();
                    $documentosModel->delete($matricula);
                    $this->gruposAlumnosModel->borrarAlumno( $matricula );
                    $alumno = $this->alumnosModel->find($matricula);
                    $correo = $alumno->correo;
                    $this->alumnosModel->delete($matricula, true);
                    $this->usuariosModel->delete($correo, true);
                }
                $aspirante->aceptado = '0';
                $aspirante->inscrito = '0';
                $this->aspirantesModel->save($aspirante);
                log_message("notice", "ASPIRANTE DESCARTADO POR {razon}: {matricula} - {usuario}", ["matricula" => $matricula, 'razon' => $razon, 'usuario' => $this->sesion->get('useremail')]);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha rechazado el aspirante: '.$matricula
                ];
            } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
                log_message("error", "ERROR {matricula}: {exception}: ", ["matricula" => $matricula, 'exception' => $ex]);
            }
            catch ( \mysqli_sql_exception $ex ) {
                log_message("error", "ERROR {matricula}: {exception}: ", ["matricula" => $matricula, 'exception' => $ex]);
            }
        }
        return redirect()->to('/Inscripcion');
    }

    /**
     * renderiza la vista para seleccionar el horario de un estudiante
     * @param matricula la matrícula del estudiante
     */
    public function seccion($matricula)
    {
        //$matricula = $this->$matricula;
        
        $grupoMateriasModel = new GruposMateriasModel();
        $alumno = $this->alumnosModel->find($matricula);
        /*echo "<pre>";
        var_dump($alumno);
        die();/**/
        if (!is_null($alumno)) {
            $gruposAlumno = $this->gruposAlumnosModel->historial($matricula);
            if (count($gruposAlumno) > 0) {
                return redirect()->to('/inscripcion/Horario/' . $matricula);
            }
            /*var_dump($matricula);
            var_dump($alumno->carrera);
            
            die();*/
            $grupos = $grupoMateriasModel->getBySemestrePeriodoCarrera('1', '2022A', $alumno->carrera, true);
            $data['grupos'] = [];
            $data['alumno'] = $alumno;
            $data['carrera'] = $this->carrerasModel->find($alumno->carrera);
            foreach ($grupos as $grupo) {
                $data['grupos'][$grupo->clave][] = $grupo;
            }
            $data['menu'] = $this->sesion->get('menu');
            return view('inscripcion/grupos', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * recibe por POST la petición para realizar la inscripción de un estudiante nuevo
     */
    public function altaSeccion()
    {
        $grupos = explode(',', $this->request->getPost('grupos'));
        $matricula = $this->request->getPost('id');
        foreach ($grupos as $grupo) {
            try {
                $grupoAlumno = new GrupoAlumno();
                $grupoAlumno->grupo = $grupo;
                $grupoAlumno->alumno = $matricula;
                $this->gruposAlumnosModel->insert($grupoAlumno);
                $grupoAlumno->id = $this->gruposAlumnosModel->lastId();

                $gruposModel = new GrupoModel();
                $gpo = $gruposModel->find($grupo);
                $gpo->ocupado++;
                $gruposModel->save($gpo);
            } catch (\mysqli_sql_exception $ex) {
                if (stripos($ex->getMessage(), 'Duplicate') !== false) {
                    log_message("warning", "INSCRIPCION DE  {matricula} DUPLICADA EN {grupo}", ["matricula" => $matricula, 'grupo' => $grupo]);
                }
            }
        }
        $url = '/inscripcion/Horario/' . $matricula;
        return redirect()->to($url);
    }

    /**
     * despliega el horario de un estudiante inscrito
     * @param matricula el Id o matricula del estudiante
     * @todo obtener el periodo de las banderas de base de datos
     */
    public function horario($matricula)
    {
        $alumno = $this->alumnosModel->find($matricula);
        if (!is_null($alumno)) {
            $horarioModel = new HorarioAlumnosModel();
            $horario = $horarioModel->rejillaHorario($matricula);
            /*echo "<pre>";
            var_dump ($horario);
            die();*/
            $data['alumno'] = $alumno;
            $data['horario'] = $horario;
            $data['carrera'] = $this->carrerasModel->find($alumno->carrera);
            $data['menu'] = $this->sesion->get('menu');
            return view('inscripcion/horario', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * registra una credencial para un estudiante nuevo
     * @param datos los datos del estudiante 
     * @return true en caso de crear correctamente la credencial
     * @return errores un array con los errores detectados
     * @todo obtener el periodo de la credencial desde la base de datos
     */
    private function creaCredencial($datos)
    {
        $source = WRITEPATH . '/admision/2022A/' . $datos['id'] . '/' . $datos['foto'];
        $destination = WRITEPATH . '/estudiantes/' . $datos['id'] . '/' . $datos['foto'];
        if (!is_dir(WRITEPATH . '/estudiantes/' . $datos['id'])) mkdir(WRITEPATH . '/estudiantes/' . $datos['id']);
        if (copy($source, $destination)) {
            $credencial = new Credencial();
            $credencialesModel = new CredencialModel();
            $credencial->alumno = $datos['id'];
            $credencial->foto = $datos['foto'];
            $credencial->vigencia = '12/2022'; // @TODO obtener del periodo en la bd
            $credencial->valida = '1';
            $credencialesModel->insert($credencial);
            return true;
        }
        return array('No se pudo crear el directorio de destino.');
    }

    /**
     * Realiza la actualización de la relación documental para generar los documentos
     * @param matricula el ID o matricula del alumno
     * @param datos un arreglo asociativo con los datos de los documentos
     */
    private function guardaRelDocumental($matricula, $datos)
    {
        $documentosFísicosModel = new AlumnosDocumentosFisicosModel();
        $relDocumental = $documentosFísicosModel->find($matricula);
        if (!is_null($relDocumental)) {
            try {
                $relDocumental->acta = (array_key_exists('acta', $datos) && $datos['acta'] == '1') ? $datos['acta'] : '0';
                $relDocumental->curp = (array_key_exists('curp', $datos) && $datos['curp'] == '1') ? $datos['curp'] : '0';
                $relDocumental->identificacion = (array_key_exists('identificacion', $datos) && $datos['identificacion'] == '1') ? $datos['identificacion'] : '0';
                $relDocumental->certificado = (array_key_exists('certificado', $datos) && $datos['certificado'] == '1') ? $datos['certificado'] : '0';
                $relDocumental->legalizado = (array_key_exists('legalizado', $datos) && $datos['legalizado'] == '1') ? $datos['legalizado'] : '0';
                $relDocumental->constancia = (array_key_exists('constancia', $datos) && $datos['constancia'] == '1') ? $datos['constancia'] : '0';
                $relDocumental->kardex = (array_key_exists('kardex', $datos) && $datos['kardex'] == '1') ? $datos['kardex'] : '0';
                $documentosFísicosModel->save($relDocumental);
            } catch (\Exception $ex) {
                log_message("error", "GUARDADO DE RELACION DOCUMENTAO {exception} ", ["exception" => $ex]);
            }
        } else {
            log_message("error", "NO SE PUDO ENCONTRAR LA RELACION DOCUMENTAL DE {matricula} ", ["matricula" => $matricula]);
        }
    }

    /**
     * obtiene la ruta para abrir un archivo y proyectarlo 
     * @param id_aspirante apunta a la carpeta propia del aspirtante
     * @param documento nombre del documento solicitado para desplegar
     * @return documento 
     */
    public function doc($id_aspirante, $respuesta)
    {
        $reg = $this->aspirantesRespuestasModel->find($respuesta);
        $documento = WRITEPATH . 'admision/2022A/' . $id_aspirante . '/' . $reg->valor;

        if (file_exists($documento)) {
            if ($file = fopen($documento, 'r')) {
                $partes_ruta = pathinfo($documento);
                $partes_ruta['extension'] = strtolower($partes_ruta['extension']);
                if ($partes_ruta['extension'] == 'pdf') header('Content-Type: application/pdf');
                if ($partes_ruta['extension'] == 'jpg') header('Content-Type: image/jpg');
                if ($partes_ruta['extension'] == 'jpeg') header('Content-Type: image/jpg');
                if ($partes_ruta['extension'] == 'png') header('Content-Type: image/png');
                // else 
                readfile($documento);
                exit;
            }
        } else {
            return 'Lo sentimos no hemos encontrado el archivo buscado';
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * despliga la vista para el registro de cuotas del estudiante
     */
    public function Cuotas($matricula)
    {
        // $matricula = $this->descifrar($matricula);
        $data['pagos'] = $this->pagosAlumnosModel->findByAlumno($matricula);
        $data['alumno'] = $this->alumnosModel->find($matricula);
        $data['menu'] = $this->sesion->get('menu');
        return view('inscripcion/cuotas', $data);
    }

    /**
     * renderiza el formulario para agregar pagos
     */
    public function agregarCuota($matricula)
    {
        // $matricula = $this->descifrar($matricula);
        $data = ['referencia' => '', 'concepto' => '', 'monto' => '', 'materia' => '', 'tipo' => ''];
        $data['alumno'] = $this->alumnosModel->find($matricula);
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        $data['conceptos'] = $this->pagosConceptosModel->findAll();
        $alumno = $this->alumnosModel->find($matricula);
        $carrera = $this->carrerasModel->find($alumno->carrera);
        $materias = $this->materiasModel->findByCarrera($carrera->id);
        $data['materias'] = $materias;
        return view('inscripcion/cuotasagregar', $data);
    }
    /**
     * realiza el guardado de la cuota de un estudiante y redirige a su panel de cuotas
     */
    public function guardaCuota($matricula)
    {
        // $matricula = $this->descifrar($matricula);
        $data = ['referencia' => '', 'concepto' => '', 'monto' => '', 'materia' => 'materia', 'tipo' => ''];
        $data['conceptos'] = $this->pagosConceptosModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        $pago = new PagoAlumno($this->request->getPost());
        // $res = $this->subePago($pago->referencia . '-' . $pago->materia,$matricula);
        $data['errores'] = [];
        // if (!is_string($res)) $data['errores'] = $res;
        if (!$pago->concepto) $data['errores'][] = 'Por favor selecciona un concepto de pago.';
        if (!$pago->referencia || !preg_match("/^[0-9]{20}$/", $pago->referencia)) $data['errores'][] = 'Por favor proporciona una referencia correcta.';

        if (count($data['errores']) == 0) {
            $pagoConcepto = $this->pagosConceptosModel->find($pago->concepto);
            $pago->comprobante = "SIN COMPROBANTE";
            $pago->conceptoTexto = $pagoConcepto->nombre;
            $resul = $this->pagosAlumnosModel->guardaPago($matricula, $pago);
            $pago->id = $this->pagosAlumnosModel->lastId();
            if ($resul === true) {
                $cotejamiento = $this->rosModel->coteja($pago->referencia, $pago);
                //if ($cotejamiento === true) return redirect()->to('/Inscripcion/Cuotas/'.$this->cifrar($matricula));
                if ($cotejamiento === true) return redirect()->to('/Inscripcion/Cuotas/'.$matricula);
                else $data['errores'][] = $cotejamiento;
            } else {
                $data['errores'][] = $resul;
            }
            try {
            } catch (\Exception $ex) {
                $data['errores'][] = 'Ocurrió un error al seleccionar el concepto.';
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            }
        }
        $alumno = $this->alumnosModel->find($matricula);
        $carrera = $this->carrerasModel->find($alumno->carrera);
        $materias = $this->materiasModel->findByCarrera($carrera->id);
        $data['materias'] = $materias;
        $data['alumno'] = $this->alumnosModel->find($matricula);
        $data['referencia'] = $this->request->getPost('referencia');
        $data['concepto'] = $this->request->getPost('concepto');
        $data['monto'] = $this->request->getPost('monto');
        $data['materia'] = $this->request->getPost('materia');
        $data['tipo'] = $this->request->getPost('tipo');
        return view('inscripcion/cuotasagregar', $data);
    }

    /**
     * procesa el archivo subido para el seguro facultativo, y lo guarda
     * @param referencia el numero de referencia proporcionado por el usuario para crear el archivo
     * @return dirFileName la ruta completa del archivo definitivo
     * @return errors un arreglo con los errores
     */
    private function subePago($referencia,$matricula)
    {
        $errors = [];
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $fileRules = ['comprobante' => 'uploaded[comprobante]|max_size[comprobante,512]|ext_in[comprobante,pdf]'];
        $fileRules_errors = [
            'comprobante' => ['uploaded' => 'Por favor sube tu comprobante de pago.', 'max_size' => 'El comprobante no debe tener un tamaño mayor a 512kb',  'ext_in' => 'El comprobante debe ser un archivo PDF.'],
        ];
        try {
            $esValido = $this->validate($fileRules, $fileRules_errors);
            if ($esValido) {
                $dir = WRITEPATH . 'estudiantes/' . $matricula;
                $fileName = $referencia . '-cuota.pdf';
                $file = $files['comprobante'];
                if (!is_dir($dir)) {
                    mkdir($dir, 0700);
                }
                $file->move($dir, $fileName);
                if ($file->hasMoved()) {
                    return $fileName;
                }
            } else {
                $mensajes = $validation->getErrors();
                $errors[] = $mensajes['comprobante'];
            }
        } catch (\Exception $ex) {
            $errors[] = 'Por favor sube un archivo correcto.';
        }
        return $errors;
    }
}
