<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<?php if ($mensaje) : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="text-center">

            <h3>Asignacion de aulas virtuales</h3>
        </div>
        <div class="mb-3">
            <label for="select_carrera" class="form-label">Carrera</label>
            <select class="form-select" name="select_carrera" id="select_carrera" <?php echo (($administrador) ? '' : 'disabled') ?> required>
                <option>Seleccione una opcion</option>
                <?php foreach ($carreras as $carrera) : ?>
                    <option value="<?php echo $carrera->id ?>" <?php echo (($rol ==  $carrera->id) ? 'selected' : '') ?>><?php echo $carrera->nombre ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
<div class="container-fluid ">
    <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="table-responsive">

            <table class="table table-borderless  mt-3 text-center ">
                <thead class="thead-default">
                    <tr>
                        <th scope="row">HORA</th>
                        <th>LUNES</th>
                        <th>MARTES</th>
                        <th>MIERCOLES</th>
                        <th>JUEVES</th>
                        <th>VIERNES</th>
                        <th>SABADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hora = 7;
                    ?>

                    <?php while ($hora < 21) : ?>

                        <tr>
                            <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                            <?php foreach ($dias as $dia) : ?>
                                <td style="background-color: #bdb994;">
                                <?php if ($bandera_edicion || $rol == 'administrador') : ?>
                                        <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo'); ?>" method="post">
                                            <input type="hidden" name="posicion" value="<?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|1|' . $dia . '|' . $periodo . '|' . $rol  ?>">
                                            <input type="hidden" name="bandera" value="AulaVirtual">
                                            <button type="submit" class="btn"><i class="bi bi-plus-circle" style="font-size: 2rem; color: grey;"></i></button>
                                        </form>
                                    <?php endif; ?>
                                    <!-- <form action="<?php // echo base_url('ProgramacionAcademica/listadoVirtual/'.(($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')).'|'. $dia . '|'.$periodo.'|'.$rol); 
                                                        ?>" method="post">
                                                <button type="submit" class="btn" name="bandera" value="AulaVirtual">Ver listado de grupos</button>
                                            </form> -->
                                    <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#listas_grupo" value="<?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|1|' . $dia . '|' . $periodo . '|' . $rol  ?>" onclick="buscar_listas(this)">Ver listado de grupos</button>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                        <?php $hora++; ?>
                    <?php endwhile; ?>


                </tbody>
            </table>
        </div>


    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="listas_grupo" tabindex="-1" role="dialog" aria-labelledby="grupos" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lista de aula</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="main_body">
                <table class="table" hidden id="main_table">
                    <thead>
                        <tr id="head_table">
                            <th>Materia</th>
                            <th>Docente</th>
                            <th>Seccion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="body_table">

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    var listas_grupo = document.getElementById('listas_grupo');

    listas_grupo.addEventListener('show.bs.modal', function(event) {
        // Button that triggered the modal
        let button = event.relatedTarget;
        // Extract info from data-bs-* attributes
        let recipient = button.getAttribute('data-bs-whatever');

        // Use above variables to manipulate the DOM
    });

    var buscar_listas = async (datos) => {
        let response = await fetch('<?php echo base_url("ProgramacionAcademica/getListadoHorario") ?>' + '/' + datos.value, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return null;
        });
        if (response) {
            // console.log(response);
            document.getElementById('main_table').hidden = false;
            let rol = '<?php echo $rol ?>';
            var body = document.getElementById('body_table');
            body.innerHTML = '';
            for (let index = 0; index < response.length; index++) {
                const element = response[index];
                var tr = document.createElement('tr');
                tr.id = response[index].grupo + '|' + response[index].periodo + '|' + response[index].h_inicio + '|' + response[index].h_fin + '|' + response[index].dia + '|' + response[index].ma_nombre;
                body.appendChild(tr);

                var td = document.createElement('td');
                td.innerHTML = response[index].ma_nombre.toUpperCase();
                tr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = (response[index].em_paterno + " " + response[index].em_materno + " " + response[index].em_nombre).toUpperCase();
                tr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = response[index].seccion;
                tr.appendChild(td);

                var td = document.createElement('td');
                if (response[index].carrera == rol || rol == 'administrador') {
                    var boton = document.createElement('buton');
                    boton.classList.add('btn', 'btn-secondary');
                    boton.setAttribute('onclick', 'eliminar(this)');
                    boton.value = response[index].grupo + '|' + response[index].periodo + '|' + response[index].h_inicio + '|' + response[index].h_fin + '|' + response[index].dia + '|' + response[index].ma_nombre;
                    boton.innerHTML = 'Eliminar';
                    td.appendChild(boton);
                }
                tr.appendChild(td);
            }
        } else {
            console.log('sin datos');
        }
    }
    var eliminar_grupo = async (grupo) => {
        var info
        let response = await fetch('<?php echo base_url("ProgramacionAcademica/popHorarioVirtual") ?>' + '/' + grupo.value, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return null;
        });
        console.log(response);
        if (response == 'exito') {
            var indicador = document.getElementById(grupo.value);
            console.log(indicador);
            indicador.innerHTML = '';
            indicador.hidden = true;
            var datos = grupo.value.split('|');
            console.log(datos);
            Swal.fire({
                'title': 'Realizado',
                'text': 'Se ha eliminado la materia ' + datos[5] + ' del Aula Virtual en la hora ' + datos[2] + '-' + datos[3] + ' del dia ' + datos[4],
                'icon': 'info',
                confirmButtonColor: '#900C3F',
                confirmButtonText: 'Aceptar',
            });
        }
    }

    function eliminar(grupo) {
        var datos = grupo.value.split('|');
        Swal.fire({
            'title': 'Confirmación',
            'html': 'Va a eliminar la materia <br>' + datos[5] + ' <br> del Aula Virtual en la hora ' + datos[2] + '-' + datos[3] + ' <br> del dia ' + datos[4] + ', ¿Desea continuar?',
            'icon': 'question',
            confirmButtonColor: '#900C3F',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            color: '#404040',
        }).then((result) => {
            if (result.value) {
                // document.getElementById(submit).submit();
                eliminar_grupo(grupo);

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#900C3F'
                });
            }
        });
    }
</script>

<?php echo $this->endSection() ?>