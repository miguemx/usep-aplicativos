
-- tabla de definicion
CREATE TABLE `usep_aplicativos`.`alumnos_constancias` ( `constancia_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `constancia_alumno` VARCHAR(9) NOT NULL , `constancia_periodo` VARCHAR(7) NOT NULL , `constancia_folio` INT NOT NULL COMMENT 'Reiniciar con cada año' , `constancia_tipo` ENUM('CALIFICACIONES','SIMPLE') NOT NULL , `constancia_fecha_exp` DATE NOT NULL COMMENT 'Actualizar esta fecha cada vez que se solicita una decarga' , `created_at` DATE NULL , `updated_at` DATE NULL , `deleted_at` DATE NULL , PRIMARY KEY (`constancia_id`)) ENGINE = InnoDB;

-- llaves foraneas
ALTER TABLE `alumnos_constancias` ADD FOREIGN KEY (constancia_alumno) REFERENCES alumnos_generales(alumno_id);
ALTER TABLE `alumnos_constancias` ADD FOREIGN KEY (constancia_periodo) REFERENCES aca_periodos(periodo_id);
