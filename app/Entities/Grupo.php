<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Grupo extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'idc' => null,
        'clave' => null,
        'folio' => null,
        'materia' => null,
        'docente' => null,
        'periodo' => null,
        'classroom' => null,

        'maximo' => null,
        'ocupado' => null,
        'acta' => null,
        'captura' => null,
        
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'grupo_id',
        'idc' => 'grupo_idc',
        'clave' => 'grupo_clave',
        'folio' => 'grupo_folio',
        'materia' => 'grupo_materia',
        'docente' => 'grupo_docente',
        'periodo' => 'grupo_periodo',
        'classroom' => 'grupo_classroom_id',
        

        'ocupado' => 'grupo_ocupado',
        'maximo' => 'grupo_max',
        'acta' => 'grupo_flag_acta',
        'captura'=>'grupo_fechacaptura',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
