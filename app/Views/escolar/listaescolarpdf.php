<?php

$font_size = ' font-size: 14px ;';
$font_size_firma = 'font-size: 5px;';

?>
<html lang='en'>

<head>
    <script src="JsBarcode.all.min.js"></script>
</head>
<style>
    body {
        height: 1650px;
        width: 1275px;
        font-family: Arial, Helvetica, sans-serif;
        background-image: url(<?php echo base_url('img/logos/Img5.jpg') ?>);
		background-size: cover;
		background-repeat: no-repeat;
        margin: 0;
        height: 100vh;

    }

    .barcode_img {
        padding-top: 190px;
        padding-left: 570px;
        height: 15px;
    }

    .datos_materia {
        padding-top: 0;
        padding-left: 75px;
        padding-right: 125px;
        padding-bottom: 0px;
        width: 100%;
        /* border: 3px solid slategrey; */
    }

    .tb-alumno th {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }

    .inf-col-1 {
        width: 175px;
        font-weight: bold;
    }

    .inf-col-2 {
        width: 115px
    }

    .inf-col-3 {
        font-weight: bold;
        width: 115px;
    }

    .inf-col-4 {
        width: 200px;
    }

    .tb-alumno {
        padding-top: 3px;
        /* border: 3px solid slateblue; */

    }

    .tb-alumno td {
        border-bottom: 1px solid black;
        padding: 0;
        margin: 0;
    }

    .col-no {
        width: 30px;
    }

    .col-no-hijo,
    .col-mat-hijo {
        text-align: center;
    }

    .col-mat {
        width: 60px;
    }

    .col-nom {
        width: 270px;
        text-align: center;
    }

    .col-calif {
        width: 140px;
        text-align: center;
    }

    .col-calif-num {
        text-align: center;
        padding-left: 5px;
    }

    .col-calif-let {
        text-align: center;
        width: 10px;
    }

    .col-tipo {
        text-align: center;
        width: 80px;
    }


    .tb-firma {
        position: absolute;
        top: 1563px;
        left: 95px;
        /*  border: 3px solid tomato; */
    }

    .firm-col-1 {
        width: 20px;
        text-align: center;
    }

    .firm-col-2 {
        width: 50px;
        text-align: center;
    }

    .firm-col-3 {
        width: 50px;
        text-align: center;
    }

    .firm-col-4 {
        width: 50px;
        text-align: center;
        padding-right: 100px;
    }

    .firm2-col-1 {
        width: 40px;
        padding-left: 10px;
        text-align: center;
    }

    .firm2-col-2 {
        width: 150px;
        text-align: center;
        /*background-color: sandybrown;*/
    }

    .firm2-col-3 {
        width: 140px;
        max-width: 140px;
        min-width: 140px;
        text-align: center;
        /* background-color:seagreen; */
    }

    .firm2-col-4 {
        width: 175px;
        max-width: 175px;
        /* background-color: royalblue; */
        text-align: right;
    }

    .firm2-col-5 {
        text-align: center;
        /*background-color: salmon;*/
    }
    .piedepagina{
        padding-top: 0;
        padding-left: 30px;
        padding-right: -600px;
        padding-bottom: 0px;
        width: 100%;
        font-size:17;
    }
    .tabla2{
        height:67%;
    }
    .piedepagina2{
        padding-top: 0;
        padding-left: 29px;
        padding-right: -510px;
        padding-bottom: 0px;
        width: 100%;
        font-size:15;
    }
    
</style>
    <div style='background-repeat: no-repeat; background-position: left bottom; background-size: 100%; height: 100%; width: 100%; <?php echo $font_size; ?>'>
    <?php   $contador = 0; 
            
    ?>
    
        <div class="titulos text-center" >
            <p style='padding-left: 250px; font-weight:bold; padding-top: 100px;' >Universidad de la Salud del Estado de Puebla</p>
            <p style='padding-left: 252px; font-weight:bold; padding-top: -18px; '>Dirección de Servicios Escolares y Titulación</p>
            <p style='padding-left: 290px; font-weight:bold; padding-top: -18px; '><?php echo $carrera ?></p>
        </div>
    
        <table class='datos_materia' style='padding-top: -20px; '>
            <tbody>
               
                <tr>
                    <td style="<?php echo $font_size ?>; font-weight: bold;"> Materia:</td>
                    <td style="<?php echo $font_size ?>"> <?php echo $nombremateria; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Sección:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $seccion; ?></td>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Periodo:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> "><?php echo $periodo; ?></td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">IDC:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $idc; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Docente:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?>"><?php echo $docente; ?></td>
                </tr>
                <tr style='padding-top: 7px;'>

                    <td></td>
                </tr>
            </tbody>
        </table>
        <div style="height: 629px; max-height: 629px;">
        <div class= "tabla2">
            <table style= "margin-left:172px; font-size:11; " class="">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($alumnos as $alumno): 
                        if($contador < 43):?>
                        <tr style=" font-size:12;">
                            <td style="width: 300px;" style="text-align:left"><?php echo $contador = $contador + 1; ?></td>
                            <td style="text-align:center; width: 150px;"><?php echo $alumno->gpoalumno ?></td>
                            <td style="width: 400px; text-align:left; "><?php echo $alumno->app_estudiante." ".$alumno->apm_estudiante." ".$alumno->nombre_estudiante ?></td>
                        </tr>
                        <?php endif ?>
                        <?php endforeach; ?>
                    
                </tbody>
            </table>
        </div>
        <table style='margin-top:70px' class='piedepagina'>
            <tbody>
                <tr>
                    <td > DSEyT</td>
                    <td style="'margin-left:300px';"> 1 de
                        <?php if(isset($nuevoalumno)){
                            echo  "2";
                        }else{
                            echo "1";
                        }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
    
    <?php if(isset($nuevoalumno)):?>
        <div class="titulos text-center" >
            <p style='padding-left: 250px; font-weight:bold; padding-top: 100px;' >Universidad de la Salud del Estado de Puebla</p>
            <p style='padding-left: 252px; font-weight:bold; padding-top: -18px; '>Dirección de Servicios Escolares y Titulación</p>
            <p style='padding-left: 290px; font-weight:bold; padding-top: -18px; '><?php echo $carrera ?></p>
        </div>
        <table class='datos_materia' style='padding-top: -20px; '>
            <tbody>
               
                <tr>
                    <td style="<?php echo $font_size ?>; font-weight: bold;"> Materia:</td>
                    <td style="<?php echo $font_size ?>"> <?php echo $nombremateria; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Sección:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $seccion; ?></td>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Periodo:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> "><?php echo $periodo; ?></td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">IDC:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $idc; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Docente:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?>"><?php echo $docente; ?></td>
                </tr>
                <tr style='padding-top: 7px;'>

                    <td></td>
                </tr>
            </tbody>
        </table>

        <div class= "tabla2">
            <table style= "margin: auto;  font-size:11; "; class="">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    <?php   
                      ?>
                        <?php 
                         foreach($nuevoalumno as $alumno): ?>
                            <tr>
                                <td style="width: 300px;" style="text-align:left"><?php echo $contador = $contador + 1; ?></td>
                                <td style="text-align:center; width: 150px;"><?php echo $alumno['id'] ?></td>
                                <td  style="width: 300px; text-align:left; "><?php echo $alumno['nombre']?></td>
                            </tr>
                        <?php endforeach; ?>
                </tbody>
            </table>
        </div> 
        <table style='margin-top:110px' class='piedepagina2'>
                    <tbody>
                        <tr>
                            <td > DSEyT</td>
                            <td > 2 de 2</td>
                        </tr>
                    </tbody>
            </table>
    <?php endif;?>
            
</body>
<script>
    JsBarcode(".barcode").init();
</script>

</html>