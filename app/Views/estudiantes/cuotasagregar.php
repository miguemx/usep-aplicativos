<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>&nbsp;</div>

    <div class="container">
        
        <?php if(isset($errores) && count($errores) > 0): ?>
            <div class="alert alert-danger">
                <?php foreach($errores as $error): ?>
                    <?php echo $error.'<br />'; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        
        
        <h5>Agregar una cuota</h5>
        <form name="agregarcuota" method="post" action="<?php echo base_url('Estudiantes/GuardaCuota'); ?>" enctype="multipart/form-data">
            <div class="col-sm-12">
                <button type="submit" name="pago" value="pago" class="btn btn-secondary">
                    Agregar
                </button>
            </div>
            <div class="col-sm-12">
                <label for="concepto" class="form-label">Concepto*</label>
                <select class="form-select" name="concepto" id="concepto" onchange="actualizaMonto(this)">
                    <option value="">-- Seleccione uno --</option>
                    <?php foreach( $conceptos as $concept ): ?>
                        <option value="<?php echo $concept->id; ?>" <?php if($concept->id==$concepto) echo 'selected="selected"'; ?>><?php echo $concept->nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-sm-12">
                <label for="monto" class="form-label">Monto*</label>
                <input type="text" class="form-control" id="monto" name="monto" readonly="readonly" value="<?php echo $monto; ?>" />
            </div>

            <div class="col-sm-12">
                <label for="referencia" class="form-label">Núm. de referencia*</label>
                <input type="text" class="form-control" id="referencia" name="referencia" value="<?php echo $referencia; ?>" />
            </div>

            <div class="col-sm-12">
                <label for="comprobante" class="form-label">Comprobante*</label>
                <input type="file" class="form-control" id="comprobante" name="comprobante" />
            </div>

            <div class="col-sm-12">
                <label for="tipo" class="form-label">Tipo de cuota*</label>
                <select class="form-select" name="tipo" id="tipo" onchange="muestraMaterias(this)">
                    <option value="">-- Seleccione una --</option>
                    <option value="EXTRAORDINARIO" <?php if($tipo=='EXTRAORDINARIO') echo 'selected="selected"'; ?>>EXAMEN EXTRAORDINARIO</option>
                    <option value="RECURSO" <?php if($tipo=='RECURSO') echo 'selected="selected"'; ?>>MATERIA A RECURSAR</option>
                    <option value="INSCRIPCION" <?php if($tipo=='INSCRIPCION') echo 'selected="selected"'; ?>>INSCRIPCIÓN (NUEVO INGRESO)</option>
                    <option value="REINSCRIPCION" <?php if($tipo=='REINSCRIPCION') echo 'selected="selected"'; ?>>REINSCRIPCIÓN</option>
                </select>
            </div>

            <div class="col-sm-12" id="divmateria" <?php if($tipo!='EXTRAORDINARIO' && $tipo!='RECURSO') echo 'style="display: none;"'; ?>>
                <label for="materia" class="form-label">Materia</label>
                <select class="form-select" name="materia" id="materia">
                    <option value="">-- Seleccione una --</option>
                    <?php foreach( $materias as $mat ): ?>
                        <option value="<?php echo $mat->clave; ?>" <?php if($mat->clave==$materia) echo 'selected="selected"'; ?>><?php echo $mat->clave.' - '.$mat->nombre; ?></option>
                    <?php endforeach; ?>
                </select>
                <div id="materiaHelp" class="form-text">Seleccionar solo en los casos en los que aplica.</div>
                
            </div>

            
        </form>
    </div>
   
</div>


<script type="text/javascript">

var conceptos = {};

<?php foreach( $conceptos as $concept ): ?>
    conceptos[ '<?php echo $concept->id ?>' ] = '<?php echo $concept->monto; ?>';
<?php endforeach; ?>

function actualizaMonto(control) {
    document.getElementById('monto').value = ( !isNaN(conceptos[control.value]) ) ? conceptos[control.value] : '' ;
}

function muestraMaterias(control) {
    let valor = control.value;
    if ( valor == 'EXTRAORDINARIO' || valor == 'RECURSO' ) {
        document.getElementById('divmateria').style.display = 'block';
    }
    else {
        document.getElementById('divmateria').style.display = 'none';
    }
}

</script>


<?php echo $this->endSection() ?>