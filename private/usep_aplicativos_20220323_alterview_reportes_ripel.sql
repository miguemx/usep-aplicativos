ALTER
 VIEW `view_reporte_ripel`
 AS (
    SELECT
        `a`.`ripel_matricula` AS `ripel_matricula`,
        `b`.`alumno_nombres` AS `alumno_nombres`,
        `b`.`alumno_ap_paterno` AS `alumno_ap_paterno`,
        `b`.`alumno_ap_materno` AS `alumno_ap_materno`,
    	`d`.`carrera_nombre` AS `carrera_nombre`,
        `c`.`usuario_correo` AS `usuario_correo`,
        `a`.`ripel_fecha` AS `ripel_fecha`,
        `a`.`ripel_ip` AS `ripel_ip`,
        `c`.`usuario_ultcnx` AS `usuario_ultcnx`
    FROM
        `usep_aplicativos`.`alumnos_ripel` `a`
    JOIN `usep_aplicativos`.`alumnos_generales` `b`
    JOIN `usep_aplicativos`.`aplicativo_usuarios` `c`
    JOIN `usep_aplicativos`.`prein_carreras` `d` on `d`.`carrera_id`  =`b`.`alumno_carrera`
    WHERE
        `a`.`ripel_matricula` = `b`.`alumno_id` AND `b`.`alumno_correo` = `c`.`usuario_correo`
);