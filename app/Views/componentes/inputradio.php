<?php 
if ( $disabled == true ) $disabled = 'disabled="disabled"';
?>
<div class="col-md-12 my-3"><!-- inicio del form group -->
    <div><label class="form-label"><?php echo $label; ?> <?php if($mandatory==true) echo '*'; ?></label></div>
    <input type="hidden" name="<?php echo $name; ?>" value="" />
    <?php foreach( $options as $text ): ?>
        <div class="form-check form-check-inline">
            <input class="form-check-input" 
                    type="radio" 
                    name="<?php echo $name; ?>" 
                    value="<?php echo $text ?>" 
                    id="<?php echo $name.'-'.$text; ?>" 
                    <?php if($value==$text) echo 'checked="checked"' ?>
                    <?php echo $disabled; ?>
                    />
            <label class="custom-control-label" for="<?php echo $name.'-'.$text; ?>"><?php echo $text; ?></label>
        </div>
    <?php endforeach; ?>
    
    <!-- descripcion del campo -->
    <?php if($description): ?>
        <div style="margin-bottom: 0.5rem !important; font-size: 13px;"><?php echo $description; ?></div>
    <?php endif; ?>

</div><!-- fin del form group -->