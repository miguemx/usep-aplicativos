<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div>
        &nbsp;
    </div>
    <form class="row g-3" method="post" action="<?php echo base_url('Estudiantes/SubirDatosCrendecial'); ?>" enctype="multipart/form-data">
        <?php if(isset($error)): ?>
            <div class="col-sm-12 alert alert-danger" role="alert"><?php echo $error; ?></div>
        <?php endif; ?>
        <?php if($paso==1): ?>
            <div class="col-sm-12 alert alert-secondary" role="alert">
                <strong>1. Vamos a comenzar a configurar tu credencial Institucional digital, para ello necesitamos tu fotografía</strong>
            </div>
            <div class="col-sm-12">
                <label for="foto" class="form-label">Selecciona una fotografía, que cumpla con las características que se describen abajo.</label>
                <input type="file" class="form-control" id="foto" name="foto" value="">
                <input type="hidden" name="documento" value="foto" />
                <div class="form-text">
                    <strong>Características para la fotografía:</strong>
                    <ul>
                        <li>Imagen en formato JPG</li>
                        <li>Resolución de imagen mayor a 400x400 px y menor a 2,000x2,000 px</li>
                        <li>Tamaño del archivo mayor a 100kb y menor a 512kb</li>
                        <li>Fotografía de frente</li>
                        <li>No utilizar selfies</li>
                        <li>Sin lentes, gorras ni objetos que cubran el rostro</li>
                        <li>La frente debe estar descubierta</li>
                        <li>Sin filtros</li>
                        <li>Sin fechas ni textos</li>
                        <li>En fondo blanco</li>
                        <li>La imagen debe abarcar hasta la parte de arriba del pecho</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" name="subir" class="btn btn-secondary bg-gradient">Subir Foto</button>
            </div>
        <?php endif; ?>
        <?php if($paso==2): ?>
            <div class="col-sm-12 alert alert-secondary" role="alert">
                <strong>2. Ahora necesitamos tu identificación oficial para validar tus datos.</strong>
            </div>
            <div class="col-sm-12">
                <label for="identificacion" class="form-label">Selecciona una identificación oficial con las características que se describen abajo.</label>
                <input type="file" class="form-control" id="identificacion" name="identificacion" value="">
                <input type="hidden" name="documento" value="identificacion" />
                <div class="form-text">
                    <strong>Características para la identificación:</strong>
                    <ul>
                        <li>Archivo en formato PDF</li>
                        <li>Tamaño del archivo mayor a 100kb y menor a 512kb</li>
                        <li>Anverso y reverso de la identificación en el mismo archivo</li>
                        <li>Se aceptará como identificación oficial: credencial para votar, licencia para conducir, cartilla de servicio
                         militar o pasaporte.</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" name="subir" class="btn btn-secondary bg-gradient">Subir Identificación</button>
            </div>
        <?php endif; ?>
        <?php if($paso==3): ?>
            <div class="col-sm-12 alert alert-success" role="alert">
                Has completado el proceso para la creación de tu credencial. 
                Por favor espera a que sea aprobada la documentación y podrás visualizarla en el apartado de 
                <a href="<?php echo base_url('Estudiantes/Credencial'); ?>" class="alert-link">Mi credencial</a>.
            </div>
        <?php endif; ?>
        
    </form>
</div>



<?php echo $this->endSection() ?>