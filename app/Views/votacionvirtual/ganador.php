<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
    <?php if (isset($mensaje)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $mensaje; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
    <?php endif; ?>
    <h1 style="display:flex; justify-content:center">Ganador del proceso</h1>
    <input style="text-align: center;"  class="form-control" name="ganadorproceso" type="text" value="<?php echo $ganador; ?>" disabled/>
    <br>
    <h1 style="display:flex; justify-content:center">Tabla de posiciones</h1>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Posición</th>
                    <th>Nombre</th>
                    <th>Voto</th>
                </tr>
            </thead>
            <tbody>
            <?php $contador = 0;
                foreach($candidatos as $candidato): ?>
                    <tr>
                        <td style="text-align:center"><?php echo $contador = $contador + 1; ?></td>
                        <td><?php echo $candidato->nombre ?></td>
                        <td><?php echo $candidato->voto ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <h1 style="display:flex; justify-content:center">Votos de abstención</h1>
    <div style="text-align: center;" class="table-responsive">
        <input style="text-align: center;" disabled value="<?php echo $abstenciones ?>"></input>
    </div>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/menu'); ?>">
        <div class="estadisticas" style="display: table-cell;  text-align: center;">
            <button type="submit" name='estadisticas' class="btn btn-secondary">Regresar al menú principal</button>
        </div>
    </form>
</div>
<?php echo $this->endSection() ?>