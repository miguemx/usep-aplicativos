<?php

namespace App\Models;

use CodeIgniter\Model;

class EvaluacionesTipoPreguntasModel extends Model
{
    protected $table = 'evaluacion_tipo_pregunta';
    protected $primaryKey = 'tipo_id';
    protected $returnType = 'App\Entities\EvaluacionesTipoPreguntas';
    protected $useSoftDeletes = false;
    protected $allowedFields = ['tipo_id', 'tipo_nombre', 'tipo_valores','tipo_clasificacion'];
    protected $db;
    protected $builder;

    protected $useTimestamps = false;


}
