ALTER TABLE `aca_materias` ADD `materia_seriacion` TEXT NULL AFTER `materia_clasificacion`;
ALTER TABLE `aca_materias` ADD `materia_semestre` INT NULL AFTER `materia_seriacion`;

CREATE TABLE `usep_aplicativos`.`aca_historial_actas` ( `historial_id` VARCHAR(10) NOT NULL , `historial_idc_grupo` INT(10) UNSIGNED NOT NULL , `historial_json_acta` TEXT NOT NULL , `historial_json_alumnos` TEXT NOT NULL , `historial_fecha_capturada` DATETIME NOT NULL , PRIMARY KEY (`historial_id`)) ENGINE = InnoDB;
ALTER TABLE `aca_historial_actas` ADD FOREIGN KEY (`historial_idc_grupo`) REFERENCES `aca_grupos`(`grupo_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `usep_aplicativos`.`aca_aulas` ( `aula_id` VARCHAR(20) NOT NULL , `aula_nombre` VARCHAR(50) NOT NULL , `aula_descripccion` TEXT NULL , `aula_cupo_mx` INT(10) NULL , PRIMARY KEY (`aula_id`)) ENGINE = InnoDB;
CREATE TABLE `usep_aplicativos`.`aca_grupos_aulas` ( `gpoaula_grupo` INT(10) UNSIGNED NOT NULL , `gpoaula_dia` VARCHAR(20) NOT NULL , `gpoaula_inicio` VARCHAR(20) NOT NULL , `gpoaula_fin` VARCHAR(20) NOT NULL , `gpoaula_aula` VARCHAR(20) NOT NULL ) ENGINE = InnoDB;

ALTER TABLE `aca_grupos_aulas` ADD FOREIGN KEY (`gpoaula_grupo`) REFERENCES `aca_grupos`(`grupo_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `aca_grupos_aulas` ADD FOREIGN KEY (`gpoaula_aula`) REFERENCES `aca_aulas`(`aula_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `aplicativo_empleados` ADD `empleado_flg_captura` INT(2) NOT NULL DEFAULT '0' AFTER `empleado_else_regidpass`; 