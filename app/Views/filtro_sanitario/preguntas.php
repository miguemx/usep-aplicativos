<?php echo $this->extend('plantillabase'); ?>
<?php echo $this->section('content') ?>
<style>
	* {
		padding: 0;
		margin: 0;
		box-sizing: border-box;
	}

	body {
		background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
		background-size: 794px 1100px;
		background-repeat: no-repeat;
		background-position: left;
		background-color: #dcdcdc;
		min-height: 100vh;
		min-width: 99vw;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 50px 5px;
		font-family: 'Open Sans', sans-serif;
		/*filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/
	}

	.contenedor-cuestionario {
		max-width: 1050px;
	}

	tbody {
		text-align: left;
	}
	.vacuna{
		border: 5px solid #000;
		border-color: red;
	}
	.trfechas{
		display:none;
	}
</style>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  	<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
	
</head>

<br>
<div class="container contenedor-cuestionario" style="background-color:#dcdcdc ;">
	<div class="card card-qr">
		<div class="card-header d-flex justify-content-center" style="background-color:#841138 ; color:antiquewhite;">
			CUESTIONARIO
		</div>
		<br>
		<div class="card-body">
            <div id="lcont" class="center mb-5" style="text-align: center;">
				Esta información es de carácter informativo. <br> No se usa para evaluar el riesgo de la persona de ser portadora de COVID-19
			</div>
            
			<form id="contenedor2" method="POST" action="<?php echo base_url('FiltroSanitario/Responde'); ?>" class="needs-validation"  >
			    <input type="hidden" name="tipo" id="tipo" value="<?php echo $tipo; ?>";>
				<input type="hidden" name="correo" id="correo" value="<?php echo $correo; ?>" ;>
				<?php 
				$sintomas = -10;
				?>
				<?php foreach ( $preguntas as $pregunta): ?>
					<?php
						$cambio = false;
						$letrero = '';
						if ( $pregunta->mayor != $sintomas ) {
							$sintomas = $pregunta->mayor;
							if ( $sintomas == 1 ) {
								$letrero = 'Síntomas mayores';
								$cambio = true;
							}
							else if ( $sintomas == 2 ) {
								$letrero = 'Síntomas menores';
								$cambio = true;
							}
						}
					?>

					<?php if ( $cambio ): ?>
						<h5><?php echo $letrero; ?></h5>
						<hr />
					<?php endif; ?>
                    <div class="mb-5 ml-3 mr-3">
                        <div>
                            <label for="exampleFormControlInput1" class="form-label"><?php echo $pregunta->pregunta; ?></label>
                        </div>
                        <?php
                        $opciones = explode( ',', $pregunta->opciones );
                        ?>

                        <?php foreach ( $opciones as $opcion ): ?>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" 
                                    type="radio" 
                                    name="<?php echo $pregunta->id; ?>" 
                                    value="<?php echo $opcion ?>" 
                                    id="<?php echo $pregunta->id.'-'.$opcion; ?>"
                                    required />
                                <label class="form-check-label" for="<?php echo $pregunta->id.'-'.$opcion; ?>"><?php echo $opcion; ?></label>
                                

                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
				
				<button class="btn btn-secondary" id="btn2"> Envia información </button>
			</form>
		</div>
	</div>
</div>


<?php echo $this->endSection() ?>