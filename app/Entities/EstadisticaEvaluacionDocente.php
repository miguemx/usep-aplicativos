<?php
namespace App\Entities;

use CodeIgniter\Entity;

class EstadisticaEvaluacionDocente extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id_respuesta'              => null,
        'matricula'                 => null,
        'grupo'                     => null,
        'pregunta_id'               => null,
        'respuesta'                 => null,
        'comentario'                => null,
        'formulario_id'             => null,
        'orden_pregunta'            => null,
        'enunciado_pregunta'        => null,
        'tipo_pregunta'             => null,
        'nombre_form'               => null,
        'orden_form'                => null,
        'instrucciones_form'        => null,
        'nombre_tipo_preguntanta'   => null,
        'valores_pregunta'          => null,
        'clasificacion_pregunta'    => null,
        'alumno_ap_pat'             => null,
        'alumno_ap_mat'             => null,
        'nombre_alumno'             => null,
        'estatus_alumno'            => null,
        'semestre_alumno'           => null,
        'grupo_alumno'              => null,
        'seccion'                   => null,
        'materia_id'                => null,
        'periodo'                   => null,
        'docente_id'                => null,
        'carrera'                   => null,
        'semestre_materia'          => null,
        'nombre_materia'            => null,
        'materia_id'                => null,
        'creditos'                  => null,
        'docente_id'                => null,
        'docente_correo'            => null,
        'docente_ap_paterno'        => null,
        'docente_ap_materno'        => null,
        'docente_nombre'            => null
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
    'id_respuesta'              => 'respuestas_id',
    'matricula'                 => 'respuestas_matricula',
    'grupo'                     => 'respuestas_grupo',
    'pregunta_id'               => 'respuestas_pregunta',
    'respuesta'                 => 'respuestas_valor',
    'comentario'                => 'respuestas_comentario',
    'formulario_id'             => 'preguntas_formulario',
    'orden_pregunta'            => 'preguntas_orden',
    'enunciado_pregunta'        => 'preguntas_titulo',
    'tipo_pregunta'             => 'preguntas_tipo',
    'clasificacion_pregunta'    => 'preguntas_clasificacion',
    'nombre_form'               => 'formulario_nombre',
    'orden_form'                => 'formulario_orden',
    'instrucciones_form'        => 'formulario_instrucciones',
    'evaluacion'                => 'evaluacion_id',
    'nombre_evaluacion'         => 'evaluacion_titulo',
    'nombre_tipo_preguntanta'   => 'tipo_nombre',
    'valores_pregunta'          => 'tipo_valores',
    'clasificacion_pregunta'    => 'tipo_clasificacion',
    'alumno_ap_pat'             => 'alumno_ap_paterno',
    'alumno_ap_mat'             => 'alumno_ap_materno',
    'nombre_alumno'             => 'alumno_nombres',
    'estatus_alumno'            => 'alumno_status',
    'semestre_alumno'           => 'alumno_semestre',
    'grupo_alumno'              => 'grupo_id',
    'seccion'                   => 'grupo_clave',
    'materia_id'                => 'grupo_materia',
    'periodo'                   => 'grupo_periodo',
    'docente_id'                => 'grupo_docente',
    'carrera'                   => 'materia_carrera',
    'semestre_materia'          => 'materia_semestre',
    'nombre_materia'            => 'materia_nombre',
    'materia_id'                => 'materia_clave',
    'creditos'                => 'materia_creditos',
    'docente_id'                => 'empleado_id',
    'docente_correo'            => 'empleado_correo',
    'docente_ap_paterno'        => 'empleado_ap_paterno',
    'docente_ap_materno'        => 'empleado_ap_materno',
    'docente_nombre'            => 'empleado_nombre'
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array'  => 'json-array'
    ];

}