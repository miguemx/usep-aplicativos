<?php
namespace App\Models;
use CodeIgniter\Model;

class AulasGruposDetallesModel extends Model {
    protected $table      = 'view_grupo_aulas';
    protected $primaryKey = 'gpoaula_grupo';

    protected $returnType    = 'App\Entities\AulasGruposDetalles';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function getbyperiodo($periodo)
    {
        return $this->where('gpoaula_periodo',$periodo)->where('gpoaula_dia','LUNES');
    }
    
}
