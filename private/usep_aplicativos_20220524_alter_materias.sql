ALTER TABLE `aca_materias` CHANGE `materia_horas_totales` `materia_horas_semanales` INT(10) NOT NULL DEFAULT '5';
UPDATE `aca_materias` SET `materia_horas_totales`='5' WHERE 1;


ALTER VIEW `view_grupos_materias` AS
SELECT
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_lunes` AS `grupo_lunes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_martes` AS `grupo_martes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_miercoles` AS `grupo_miercoles`,
    `usep_aplicativos`.`aca_grupos`.`grupo_jueves` AS `grupo_jueves`,
    `usep_aplicativos`.`aca_grupos`.`grupo_viernes` AS `grupo_viernes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_sabado` AS `grupo_sabado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_domingo` AS `grupo_domingo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_flag_acta` AS `grupo_flag_acta`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechacaptura` AS `grupo_fechacaptura`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechaimpresion` AS `grupo_fechaimpresion`,
    `usep_aplicativos`.`aca_grupos`.`grupo_historial_acta` AS `grupo_historial_acta`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`aca_materias`.`materia_clasificacion` AS `materia_clasificacion`,
    `usep_aplicativos`.`aca_materias`.`materia_seriacion` AS `materia_seriacion`,
    `usep_aplicativos`.`aca_materias`.`materia_semestre` AS `materia_semestre`,
    `usep_aplicativos`.`aca_materias`.`materia_horas_semanales` AS `materia_horas_semanales`,
    `usep_aplicativos`.`aca_periodos`.`periodo_id` AS `periodo_id`,
    `usep_aplicativos`.`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,
    `usep_aplicativos`.`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,
    `usep_aplicativos`.`aca_periodos`.`periodo_fin` AS `periodo_fin`,
    `usep_aplicativos`.`aca_periodos`.`periodo_estado` AS `periodo_estado`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` AS `empleado_id`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo` AS `empleado_correo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_horario` AS `empleado_horario`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_area` AS `empleado_area`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `empleado_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_apellido` AS `empleado_apellido`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `empleado_ap_materno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_categoria` AS `empleado_categoria`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_edad` AS `empleado_edad`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_sexo` AS `empleado_sexo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regid` AS `empleado_else_regid`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regidpass` AS `empleado_else_regidpass`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_flg_captura` AS `empleado_flg_captura`,
    `usep_aplicativos`.`aplicativo_empleados`.`created_at` AS `created_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`updated_at` AS `updated_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`deleted_at` AS `deleted_at`
FROM
    (
        (
            (
                `usep_aplicativos`.`aca_grupos`
            JOIN `usep_aplicativos`.`aca_materias` ON
                (
                    `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
                )
            )
        JOIN `usep_aplicativos`.`aca_periodos` ON
            (
                `usep_aplicativos`.`aca_periodos`.`periodo_id` = `usep_aplicativos`.`aca_grupos`.`grupo_periodo`
            )
        )
    LEFT JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` = `usep_aplicativos`.`aca_grupos`.`grupo_docente`
        )
    );

    ALTER TABLE `aca_grupos_aulas` CHANGE `gpoaula_id` `gpoaula_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;