<?php

namespace App\Models;

use CodeIgniter\Model;

class RipelModel extends Model
{
    protected $table = 'view_reporte_ripel';
    protected $primaryKey = 'ripel_matricula';

    protected $returnType = 'App\Entities\Ripel';
    protected $useSoftDeletes = false;
    protected $allowedFields = [
        'ripel_matricula',
        'alumno_nombres	',
        'alumno_ap_paterno',
        'alumno_ap_materno',
        'usuario_correo	',
        'ripel_fecha',
        'ripel_ip	',
        'usuario_ultcnx',
        'alumno_carrera',
    ];

    /**
     * funcion para regresar la lista completa de los alumnos que firmaron ripel
     */
    public function getAlumnosRipel()
    {
        return $this->findAll();
    }
}
