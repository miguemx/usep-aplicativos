<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\EstadisticaRegiones;

class EstadisticaRegionesModel extends Model
{
    protected $table="view_estadistica_regiones";
    protected $returnType='App\Entities\EstadisticaRegiones';
    protected $allowedFields=['n_alumnos','region_nombre','aspirante_carrera','aspirante_sexo'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
/**
 * muestra la estadística por regiones de puebla
 * @return consulta colección de registros agrupados por región
 */
    public function conteoxregiones(){
        //$consulta = $this->findAll();
        $consulta = $this->selectSum('n_alumnos')->select('region_nombre')->groupBy('region_nombre')->orderBy('region_nombre')->findAll();
        return $consulta;
    }

 /**
 * muestra la estadística por regiones agrupada por carrera y sexo 
 * @return consulta colección de registros de la vista estadistica_regiones
 */
public function conteoxcarrerasex(){
    $consulta = $this->orderBy('region_nombre')->findAll();
    return $consulta;
}

/**
 * muestra la cantidad de Alumnos de un argumento específico 
 * @param region nombre de la region
 * @param carrera id de la carrera
 * @param sexo género del grupo de estudiantes
 * @return consulta registro de la vista resultante de los argumentos otorgados
 */

public function conteoxargumento($carrera,$sexo,$region){
                $this->where('aspirante_carrera',$carrera)
                     ->where('aspirante_sexo',$sexo)
                     ->where('region_nombre',$region)
                     ->orderBy('region_nombre')
                     ->findFirst();
    return $cantidad;
}


}