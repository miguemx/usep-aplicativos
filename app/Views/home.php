<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .declaracion {
        display: flex;
        justify-content: space-around;
        align-items: center;
    }

    .input-error {
        border: 1px red solid !important;
    }

    .magnificar {
        transform: scale(2);
        padding: 10px;

    }
    .radio{
        margin: 5px;
    }
</style>
<div class="container-fluid">
    <div class="col-sm-12">&nbsp;</div>
    <!-- modal inicio -->
    <!-- The Modal -->
    <div id="myModal" class="modal hide fade in" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered " data-keyboard="false" data-backdrop="static">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Estimado Estudiante de la USEP</h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body text-center">
                    Te informamos que, el “REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA.”
                    Se encuentra disponible en: <br> <a id="link" target="_blank" href="https://usep.puebla.gob.mx/images/site/content/Transparencia/Reglamento%20Acad%C3%A9mico%20Int%2018ene2020.pdf">
                        https://usep.puebla.gob.mx/images/site/content/Transparencia/
                        Reglamento%20Acad%C3%A9mico%20Int
                        %2018ene2020.pdf.</a>
                    <br>
                    Para conservar tu calidad de estudiante y tener acceso a los servicios de “USEP Digital” es necesario recabar tu “conocimiento y aceptación del mismo”, activando la siguiente casilla:
                </div>
                <!-- Modal footer -->
                <form action="home/validacionripel" method="post" id="confirmacion">
                    <div class="modal-footer">
                        <div>
                            <div class="form-check declaracion">

                                <div class="radio">
                                    <input class="form-check-input magnificar" type="checkbox" id="check1" name="option1" value="something" required style=" width: 22px; height: 22px; border: 2px solid #840f31; top: 28px;">
                                </div>
                                <div style="padding-left: 15px;">
                                    <label class="form-check-label" id="label-check"> Declaro que he leído, he comprendido y acepto en su totalidad el Reglamento de Ingreso,
                                        Permanencia y Egreso de los aprendientes de la Universidad de la Salud del Estado de Puebla.</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" style="padding-bottom: 10px;">
                        <button type="button" class="btn btn-secondary" id="btn-acept-ripel">Aceptar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal fin -->
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">Tu espacio</div>
                <div class="card-body">
                    <?php if (isset($banner)) : ?>
                        <div class="alert alert-danger"><?php echo $banner; ?></div>
                    <?php else : ?>
                        <p>
                            Ya puedes contestar tu filtro sanitario desde 
                            <strong>
                                <a href="<?php echo base_url('FiltroSanitario'); ?>" target="_blank">aquí</a>
                            </strong>, o bien desde el enlace que aparece en la sección <strong>Enlaces rápidos.</strong>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">Enlaces rápidos</div>
                <div class="card-body">
                    <ul>
                        <li><a href="https://usep.puebla.gob.mx" target="_blank">Portal USEP</a></li>
                        <li><a href="<?php echo base_url('Bibliotecas'); ?>">Biblioteca Digital</a></li>
                        <li><a href="<?php echo base_url('FiltroSanitario'); ?>" target="_blank">Filtro sanitario</a></li>
                        <li><a href="<?php echo base_url('Bibliotecas/Acceso'); ?>" target="_blank">Acceso a Biblioteca</a></li>
                        <?php if ($evaluaciondocente): ?> 
                            <?php // echo json_encode($evaluaciondocente) ?>
                            <li><a href="<?php echo base_url('Evaluaciones/Contestar/'.$evaluaciondocente[0]->id); ?>" target="_blank">Contesta la "<?php echo $evaluaciondocente[0]->titulo?>"</a></li>
                        <?php endif;?>
                        <?php if (isset($votoelectronico)) : ?>
                            <li><a href="<?php echo base_url('votaciones/voto'); ?>">Voto electrónico</a></li>
                            <!--<li><a href="https://aplicativos.usep.mx/votaciones/voto">Voto electrónico</a></li> -->
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div>&nbsp;</div>
            <div class="card">
                <div class="card-header">Notificaciones</div>
                <div class="card-body">
                    <?php if (count($notificaciones)) : ?>
                        <?php foreach ($notificaciones as $notificacion) : ?>
                            <div>
                                <div style="font-size: 12px; color: #7d7d7d;">
                                    <?php echo $notificacion->created_at->getDay() . '/' . $notificacion->created_at->getMonth() . '/' . $notificacion->created_at->getYear(); ?> -
                                    <?php echo $notificacion->hora; ?>
                                </div>
                                <div><?php echo $notificacion->texto; ?></div>
                            </div>
                            <hr />
                        <?php endforeach; ?>
                    <?php else : ?>
                        <em>Por el momento, no cuentas con más notificaciones.</em>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($bandera_modal)) : ?>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $("#myModal").modal('show', {
                backdrop: 'static',
                keyboard: false
            });
            document.querySelector("#check1").focus();

        });
    </script>
    <script>
        window.addEventListener('click', function(e) {
            if (document.getElementById('btn-acept-ripel').contains(e.target) || document.getElementById('check1').contains(e.target) || document.getElementById('link').contains(e.target)) {
                // Clicked in box 
                //console.log("adentro");
            } else {
                location.reload();
            }
        });
    </script>
    <script>
        'use strict';
        const btn_guardar = document.querySelector('#btn-acept-ripel');
        let validar = () => {
            //let inputs_requeridos = document.querySelectorAll('#contenedor2 [required]');
            let checkacept = document.getElementById('check1');
            let error_empty = false;
            if (checkacept.checked == false) {
                checkacept.classList.add('input-error');
                error_empty = true;
            } else {
                checkacept.classList.remove('input-error');
            }
            return error_empty;
        };
        let obtener_datos = () => {
            let flg_validar = validar();
            if (!flg_validar) {
                document.getElementById('confirmacion').submit();
            }
        }
        btn_guardar.addEventListener('click', obtener_datos);
    </script>
<?php endif; ?>
<?php echo $this->endSection() ?>