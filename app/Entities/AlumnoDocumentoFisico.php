<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AlumnoDocumentoFisico extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                => null,
        'acta'              => null,
        'curp'              => null,
        'identificacion'    => null,
        'certificado'       => null,
        'legalizado'        => null,
        'constancia'        => null,
        'kardex'            => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'aludocfis_id',
        'acta'              => 'aludocfis_acta',
        'curp'              => 'aludocfis_curp',
        'identificacion'    => 'aludocfis_identificacion',
        'certificado'       => 'aludocfis_cert',
        'legalizado'        => 'aludocfis_cert_legal',
        'constancia'        => 'aludocfis_constancia',
        'kardex'            => 'aludocfis_kardex',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
