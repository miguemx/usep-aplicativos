DROP VIEW IF EXISTS `view_horario_kardex`;
CREATE
VIEW `view_horario_kardex` AS
select `aca_grupos_alumnos`.`gpoalumno_alumno` AS `gpoalumno_alumno`,`aca_grupos_alumnos`.`gpoalumno_grupo` AS `gpoalumno_grupo`,`aca_grupos_alumnos`.`gpoalumno_ordinario` AS `gpoalumno_ordinario`,
`aca_grupos_alumnos`.`gpoalumno_extraordinario` AS `gpoalumno_extraordinario`,`aca_grupos_alumnos`.`aprobada` AS `aprobada`,`aca_grupos`.`grupo_id` AS `grupo_id`,`aca_grupos`.`grupo_idc` AS `grupo_idc`,
`aca_grupos`.`grupo_clave` AS `grupo_clave`,`aca_grupos`.`grupo_folio` AS `grupo_folio`,`aca_grupos`.`grupo_materia` AS `grupo_materia`,`aca_grupos`.`grupo_docente` AS `grupo_docente`,`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
`aca_grupos`.`grupo_lunes` AS `grupo_lunes`,`aca_grupos`.`grupo_martes` AS `grupo_martes`,`aca_grupos`.`grupo_miercoles` AS `grupo_miercoles`,`aca_grupos`.`grupo_jueves` AS `grupo_jueves`,`aca_grupos`.`grupo_viernes` AS `grupo_viernes`,
`aca_grupos`.`grupo_sabado` AS `grupo_sabado`,`aca_grupos`.`grupo_domingo` AS `grupo_domingo`,`aca_grupos`.`grupo_max` AS `grupo_max`,`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,`aca_periodos`.`periodo_id` AS `periodo_id`,
`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,`aca_periodos`.`periodo_fin` AS `periodo_fin`,`aca_materias`.`materia_clave` AS `materia_clave`,
`aca_materias`.`materia_carrera` AS `materia_carrera`,`aca_materias`.`materia_nombre` AS `materia_nombre`,`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,`aca_materias`.`materia_creditos` AS `materia_creditos`,
`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,`aca_materias`.`materia_tipo` AS `materia_tipo`,`alumnos_generales`.`alumno_status` AS `status_alumno` from `aca_grupos_alumnos` 
join `alumnos_generales` on (`alumnos_generales`.`alumno_id` = `aca_grupos_alumnos`.`gpoalumno_alumno` )
join `aca_grupos` on(`aca_grupos_alumnos`.`gpoalumno_grupo` = `aca_grupos`.`grupo_id`) 
join `aca_periodos` on(`aca_grupos`.`grupo_periodo` = `aca_periodos`.`periodo_id`) 
join `aca_materias` on(`aca_grupos`.`grupo_materia` = `aca_materias`.`materia_clave`) WHERE alumnos_generales.alumno_status = 'ACTIVO'