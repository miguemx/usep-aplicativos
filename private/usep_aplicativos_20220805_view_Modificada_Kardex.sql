CREATE OR REPLACE VIEW view_horario_kardex AS
SELECT
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno` AS `gpoalumno_alumno`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` AS `gpoalumno_grupo`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_ordinario` AS `gpoalumno_ordinario`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_extraordinario` AS `gpoalumno_extraordinario`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`aprobada` AS `aprobada`,
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_periodos`.`periodo_id` AS `periodo_id`,
    `usep_aplicativos`.`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,
    `usep_aplicativos`.`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,
    `usep_aplicativos`.`aca_periodos`.`periodo_fin` AS `periodo_fin`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_status` AS `status_alumno`
FROM
    (
        (
            (
                (
                    `usep_aplicativos`.`aca_grupos_alumnos`
                JOIN `usep_aplicativos`.`alumnos_generales` ON
                    (
                        `usep_aplicativos`.`alumnos_generales`.`alumno_id` = `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno`
                    )
                )
            JOIN `usep_aplicativos`.`aca_grupos` ON
                (
                    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` = `usep_aplicativos`.`aca_grupos`.`grupo_id`
                )
            )
        JOIN `usep_aplicativos`.`aca_periodos` ON
            (
                `usep_aplicativos`.`aca_grupos`.`grupo_periodo` = `usep_aplicativos`.`aca_periodos`.`periodo_id`
            )
        )
    JOIN `usep_aplicativos`.`aca_materias` ON
        (
            `usep_aplicativos`.`aca_grupos`.`grupo_materia` = `usep_aplicativos`.`aca_materias`.`materia_clave`
        )
    )