<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AccesoBiblioteca extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'alumno' => null,
        'empleado' => null,
        'tipo' => null,
        'sexo' => null,
        'edad' => null,
        'entrada' => null,
        'salida' => null,
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'biblioacceso_id',
        'alumno'        => 'biblioacceso_alumno',
        'empleado'      => 'biblioacceso_empleado',
        'tipo'          => 'biblioacceso_tipo',
        'sexo'          => 'biblioacceso_sexo',
        'edad'          => 'biblioacceso_edad',
        'entrada'       => 'biblioacceso_entrada',
        'salida'        => 'biblioacceso_salida',
        'created_at'    => 'created_at',
        'updated_at'    => 'updated_at',
        'deleted_at'    => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
