<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <table class="table table-striped-columns
                    table-hover	
                    table-borderless
                    
                    align-middle vw-70">
        <thead class="table-light">
            <caption>EVDOC</caption>
            <tr>
                <!-- <th scope="col">idc</th> -->
                <th scope="col">Tipo</th>
                <th scope="col">PE</th>
                <th scope="col">Grupo</th>
                <th scope="col">GrupoS</th>
                <th scope="col">Asignatura</th>
                <th scope="col">Horas conducidas</th>
                <th scope="col">Horas por semana</th>
                <th scope="col">Tipo de contratación</th>
                <th scope="col">Docente</th>
                <th scope="col">Alumnos inscritos</th>
                <th scope="col">Participantes</th>
                <th scope="col">Significancia</th>
                <th scope="col">% participantes</th>
                <th scope="col">Calidad Recursos</th>
                <th scope="col">Pertinencia Aca</th>
                <th scope="col">Relación Aca</th>
                <th scope="col">Transmisión Conoc</th>
                <th scope="col">Promedio</th>
            </tr>
        </thead>
        <tbody class="table-group-divider">
            <?php foreach ($indicadores as $grupo) : ?>
                <tr>
                    <!-- <td><?php // echo $grupo['idc'] 
                                ?> </td> -->
                    <td><?php echo $grupo['tipo'] ?> </td>
                    <td><?php echo $grupo['PE'] ?> </td>
                    <td><?php echo $grupo['grupo'] ?> </td>
                    <td><?php echo $grupo['grupoS'] ?> </td>
                    <td><?php echo $grupo['asignatura'] ?> </td>
                    <td><?php echo $grupo['horas_conducidas'] ?> </td>
                    <td><?php echo $grupo['horas_semanales'] ?> </td>
                    <td><?php echo $grupo['tipo_contratacion'] ?> </td>
                    <td><?php echo $grupo['docente'] ?> </td>
                    <td><?php echo $grupo['alumnos_inscritos'] ?> </td>
                    <td><?php echo $grupo['alumnos_participantes'] ?> </td>
                    <td><?php echo $grupo['significancia'] ?> </td>
                    <td><?php echo $grupo['participacion'] ?> </td>
                    <td><?php echo $grupo['calidad_recursos'] ?> </td>
                    <td><?php echo $grupo['pertenencia_academica'] ?> </td>
                    <td><?php echo $grupo['relacion_academica'] ?> </td>
                    <td><?php echo $grupo['transmision_conocimientos'] ?></td>
                    <td><?php echo $grupo['promedio'] ?> </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <a name="btn_guardar" id="btn_guardar" class="btn btn-secondary" href="<?php echo base_url('EstadisticasEvaluacionDocente/gruardarEVDOC/'.$evaluacion) ?>" role="button">Guardar y descargar</a>
        </tfoot>
    </table>
</div>
<?php echo $this->endSection() ?>