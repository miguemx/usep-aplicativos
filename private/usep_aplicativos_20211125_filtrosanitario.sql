ALTER TABLE `filtro_sanitario` 
             ADD `filtro_temp` FLOAT(5) NULL AFTER `filtro_menorotro`,
             ADD `filtro_oxigen` FLOAT(5)  NULL AFTER `filtro_temp`;