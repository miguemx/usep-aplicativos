<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .global {
        height: 89vh;
        overflow-y: scroll;
    }

    .error {
        border: 2px solid red;
    }
    .evaluacion {
        /* padding-top: 4.2rem;
        padding-bottom: 4.2rem;
        font-family: 'Open Sans', sans-serif; */
        background-color: #dcdcdc;
        /* height: 100vh; */
        /* width: 100vw; */
        /* display: flex; */
        /* justify-content: center;
        align-items: center; */
        /* background: rgba(76, 175, 80, 0.8) */
        background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
        background-size: 794px 1100px;
        background-repeat: no-repeat;
        /* opacity: 0.3; */
        background-position: left;
    }
</style>
<div class="container-fluid evaluacion">
    <div class="row pt-3 d-flex justify-content-around">
        <div class="col-sm-3 mb-3 ">
            <div id="list-example" class="list-group">
                <?php foreach ($formularios as $formulario) : ?>
                    <h5>
                        <a class="list-group-item list-group-item-action" href="<?php echo '#list-' . $formulario->id . '-item' ?>"><?php echo $formulario->nombre ?></a>
                    </h5>
                <?php endforeach; ?>
            </div>
            <div class="bg-white rounded shadow my-3 p-3">
                <div class="datos text-center">
                    <div class="datos text-left">
                        <label style="margin: 0 10px">Materia:<h5 class="card-title"> <?php echo mb_strtoupper($grupo->materiaNombre);  ?></h5> </label>
                    </div>
                    <div class="datos text-left">
                        <label style="margin: 0 10px">Docente:<h5 class="card-title"> <?php echo mb_strtoupper($grupo->apPaterno . " " . $grupo->apMaterno . " " . $grupo->nombre_emp)  ?></h5> </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 mb-3 global">
            <div class="bg-white rounded shadow mb-4  ml-2 mr-2 p-3">
                <form action="<?php echo base_url('Evaluaciones/GuardarRespuestasEvaluacionDocente/' . $evaluacion->id . '/' . $grupo->grupo) ?>" method="post" id="form_guardar">
                    <div id="contenedor_preguntas">
                       
                        <?php foreach ($formularios as $formulario_cont) : ?>
                            <div class="card-header bg-white">
                                <h4 id="<?php echo 'list-' . $formulario_cont->id . '-item' ?>">
                                    <?php echo  $formulario_cont->nombre  ?>
                                </h4>
                            </div>
                            <?php foreach ($arr_preguntas as $pregunta_form => $pregunta_indicador) : ?>
                                <?php foreach ($pregunta_indicador as $pregunta) : ?>
                                    <?php if ($pregunta->formulario == $formulario_cont->id) : ?>
                                        <?php
                                        // echo "<br>". json_encode($pregunta) . "<br>";
                                        // $arr_input[] = $pregunta->id;
                                        $value = (array_key_exists($pregunta->id, $arr_respuestas)) ? $arr_respuestas[$pregunta->id]['valor'] : null;
                                        echo myInputEvaluacion($pregunta, $pregunta->objeto_tipo, $value, false, 'status', 'comment');
                                        ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="row">
                            <div class="col-md-6 m-4">
                                <button type="button" class="btn btn-secondary" onclick="selectores()">Guardar</button>
                                <a class="btn btn-secondary" href="<?php echo base_url('Evaluaciones/Contestar/'. $evaluacion->id )?>" role="button">Salir sin guardar</a>
                            </div>
                            <div class="col-md-6" style="font-size: 14px; margin-bottom: 10px;">Los campos marcados con asterisco (*) son obligatorios.</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 
<div class="container-fluid " hidden>
    <div class="row mt-3 d-flex justify-content-around">
        <div class="col-sm-3 mb-3">
            <div class="list-group" id="list-tab" role="tablist">

                <?php // foreach ($formularios as $formulario) : 
                ?>
                    <a class="list-group-item list-group-item-action" id="<?php // echo 'list-formulario-' . $formulario->id 
                                                                            ?>" data-bs-toggle="list" href="<?php // echo '#list-' . $formulario->id . '-item' 
                                                                                                            ?>" role="tab" aria-controls="<?php // echo 'list-' . $formulario->id . '-item' 
                                                                                                                                            ?>"><?php // echo $formulario->nombre 
                                                                                                                                                ?></a>
                <?php // endforeach; 
                ?>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="tab-content" id="nav-tabContent">
                <?php // foreach ($formularios as $formulario_cont) : 
                ?>
                    <form action="<?php // echo base_url('Evaluaciones/GuardarRespuestas/') 
                                    ?>" method="post">
                    </form>
                    <div class="tab-pane fade bg-white rounded shadow mb-4  ml-2 mr-2 p-3" id="<?php // echo 'list-' . $formulario_cont->id . '-item' 
                                                                                                ?>" role="tabpanel" aria-labelledby="<?php // echo 'list-formulario-' . $formulario_cont->id 
                                                                                                                                        ?>">
                        <div class="card-header bg-white">
                            <h5>
                                <?php // echo $formulario_cont->nombre 
                                ?>
                            </h5>
                        </div>
                        <?php // foreach ($arr_preguntas as $pregunta_form => $pregunta_indicador) : 
                        ?>
                            <?php // foreach ($pregunta_indicador as $pregunta) : 
                            ?>
                                <?php // if ($pregunta->formulario == $formulario_cont->id) : 
                                ?>

                                    <?php /*
                                    $value = (array_key_exists($pregunta->id, $arr_respuestas)) ? $arr_respuestas[$pregunta->id]['valor'] : null;
                                    echo myInputEvaluacion($pregunta, $pregunta->objeto_tipo, $value, false, 'status', 'comment');*/
                                    ?>

                                    <?php // // echo "[$pregunta->id] $pregunta->nombre $value  <br>" 
                                    ?>
                                <?php // endif; 
                                ?>
                            <?php // endforeach; 
                            ?>
                        <?php // endforeach; 
                        ?>
                    </div>
                <?php // endforeach; 
                ?>
            </div>

            <div class="col-md-12" style="font-size: 14px; margin-bottom: 10px;">Los campos marcados con asterisco (*) son obligatorios.</div>
        </div>
    </div>
</div> -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function selectores() {
        var errores_radios = validarRadios();
        var errores_basic = validarBasic();
        if (!errores_radios && !errores_basic) {
            // console.log(errores_radios + " " + errores_basic);
            Swal.fire({
                'title': 'Guardando...',
                'html': 'Procesando respuestas ',
                'icon': 'info',
                timer: 2000,
                timerProgressBar: true,
                confirmButtonColor: '#900C3F',
                confirmButtonText: 'Aceptar',
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    document.getElementById('form_guardar').submit();
                }
            })
            // setTimeout(() => {   document.getElementById('form_guardar').submit(); }, 2000);
            // document.getElementById('form_guardar').submit();

        } else {
            Swal.fire({
                'title': 'Datos incompletos',
                'text': 'Revise los campos resaltados en rojo y vuelva a intentarlo',
                'icon': 'info',
                confirmButtonColor: '#900C3F'
            });
        }
    }

    function validarRadios() {
        var contenedor = document.getElementsByClassName('mandatory_radio');
        var conteo = 0;
        for (let index = 0; index < contenedor.length; index++) {
            const element = contenedor[index].children;
            var bandera = false;
            for (let contador = 0; contador < element.length; contador++) {
                const hijo = element[contador].children;
                if (hijo[0].checked == true) {
                    bandera = true;
                }
            }
            if (!bandera) {
                contenedor[index].classList.add('error');
                conteo++;
            } else {
                contenedor[index].classList.remove('error');
            }
        }
        return (conteo > 0) ? true : false;
    }

    function validarBasic() {
        var contenedor = document.getElementsByClassName('mandatory_basic');
        var conteo = 0;
        for (let index = 0; index < contenedor.length; index++) {
            var input = (contenedor[index].children);

            if (input[0].value == '') {
                conteo++;
                contenedor[index].classList.add('error');
            } else {
                // console.log('con espacios');
                contenedor[index].classList.remove('error');
            }
        }
        return (conteo > 0) ? true : false;
    }
    // selectores();
</script>
<?php echo $this->endSection() ?>