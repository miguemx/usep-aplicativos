<?php

$font_size = ' font-size: 6.5px ;';
$font_size_firma = 'font-size: 5px;';

?>
<html lang='en'>

<head>
    <script src="JsBarcode.all.min.js"></script>
</head>
<style>
    body {
        height: 1650px;
        width: 1275px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .barcode_img {
        padding-top: 190px;
        padding-left: 570px;
        height: 15px;
    }

    .datos_materia {
        padding-top: 0;
        padding-left: 75px;
        padding-right: 125px;
        padding-bottom: 0px;
        width: 100%;
        /* border: 3px solid slategrey; */
    }

    .tb-alumno th {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }

    .inf-col-1 {
        width: 175px;
        font-weight: bold;
    }

    .inf-col-2 {
        width: 115px
    }

    .inf-col-3 {
        font-weight: bold;
        width: 115px;
    }

    .inf-col-4 {
        width: 150px;
    }

    .tb-alumno {
        padding-top: 3px;
        /* border: 3px solid slateblue; */

    }

    .tb-alumno td {
        border-bottom: 1px solid black;
        padding: 0;
        margin: 0;
    }

    .col-no {
        width: 30px;
    }

    .col-no-hijo,
    .col-mat-hijo {
        text-align: center;
    }

    .col-mat {
        width: 60px;
    }

    .col-nom {
        width: 270px;
        text-align: center;
    }

    .col-calif {
        width: 140px;
        text-align: center;
    }

    .col-calif-num {
        text-align: center;
        padding-left: 5px;
    }

    .col-calif-let {
        text-align: center;
        width: 10px;
    }

    .col-tipo {
        text-align: center;
        width: 80px;
    }


    .tb-firma {
        position: absolute;
        top: 1563px;
        left: 95px;
        /*  border: 3px solid tomato; */
    }

    .firm-col-1 {
        width: 20px;
        text-align: center;
    }

    .firm-col-2 {
        width: 50px;
        text-align: center;
    }

    .firm-col-3 {
        width: 50px;
        text-align: center;
    }

    .firm-col-4 {
        width: 50px;
        text-align: center;
        padding-right: 100px;
    }

    .firm2-col-1 {
        width: 40px;
        padding-left: 10px;
        text-align: center;
    }

    .firm2-col-2 {
        width: 150px;
        text-align: center;
        /*background-color: sandybrown;*/
    }

    .firm2-col-3 {
        width: 140px;
        max-width: 140px;
        min-width: 140px;
        text-align: center;
        /* background-color:seagreen; */
    }

    .firm2-col-4 {
        width: 175px;
        max-width: 175px;
        /* background-color: royalblue; */
        text-align: right;
    }

    .firm2-col-5 {
        text-align: center;
        /*background-color: salmon;*/
    }
</style>

<body>
    <div style='background-repeat: no-repeat; background-position: left bottom; background-size: 100%; height: 100%; width: 100%; <?php echo $font_size; ?>'>




        <!--<img src="data:image/jpg;base64,<?php //echo $codigo 
                                            ?>" height="20" />-->
        <img class="barcode_img" src="data:image/jpg;base64,<?php echo $codigo ?>" height="10" />
        <table class='datos_materia'>
            <tbody>
                <tr>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Folio:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> "><?php echo $folio ?></td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">IDC:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $idc; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Periodo:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> "><?php echo $periodo; ?></td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Sección:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $seccion; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Campus:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> ">Puebla</td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Programa:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?>"><?php echo $carrera; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-1' style="<?php echo $font_size ?> ">Facultad / Escuela:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> "><?php echo $facultad; ?></td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> ">Clave del programa:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> "><?php echo $claveprograma; ?></td>
                </tr>
                <tr>
                    <td class='inf-col-1' style="<?php echo $font_size ?> padding-top: 10px; ">Nivel:</td>
                    <td class='inf-col-2' style="<?php echo $font_size ?> padding-top: 10px; ">Licenciatura</td>
                    <td class='inf-col-3' style="<?php echo $font_size ?> padding-top: 10px; ">Clave materia:</td>
                    <td class='inf-col-4' style="<?php echo $font_size ?> padding-top: 10px; "><?php echo $clavemateria; ?></td>
                </tr>
                <tr style='padding-top: 7px;'>
                    <td></td>
                    <td></td>
                    <td style="<?php echo $font_size ?>; font-weight: bold;"> Materia:</td>
                    <td style="<?php echo $font_size ?>"> <?php echo $nombremateria; ?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <div style="height: 629px; max-height: 629px;">
            <table class='tb-alumno' style='width: 100%; border-color: red; padding-left: 70px; padding-right: 78px; padding-top: 12px;'>

                <thead>
                    <tr>
                        <th class='col-no' style="<?php echo $font_size ?>">NO.</th>
                        <th class='col-mat' style="<?php echo $font_size ?>">MATRÍCULA</th>
                        <th class='col-nom' style="<?php echo $font_size ?>">NOMBRE</th>
                        <th class='col-calif' colspan='2' style="<?php echo $font_size ?>">CALIFICACION</th>
                        <th class='col-tipo' style="<?php echo $font_size ?>">TIPO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $contador = 1;
                    $tipoCalificacion = null;
                    $calificacion = 0;
                    $numeroletra = "";
                    ?>
                    <?php
                    /* var_dump($lista);
                    echo "a";
                    die(); */
                    //echo $lista['id_alumno'];
                    //die();

                    foreach ($lista as $listas => $arr) :
                        for ($i = 0; $i < count($arr); $i++) {
                            ?> <tr>
                            <td class='col-no-hijo' style="<?php echo $font_size ?>" >  <?php echo $contador++ ?> </td> <?php

                            $j=0;
                            foreach ($arr[$i] as $key => $value) {
                                
                               // die();
                                switch ($j) {
                                    case 0:
                                        ?>
                                        <td class='col-no-hijo' style=" <?php echo $font_size ?>"> <?php echo $value  ?></td>
                                        <?php
                                        break;
                                    case 1:
                                        ?>
                                        <td class='col-no-hijo' style=" text-align:left; <?php echo $font_size; ?>"> <?php echo $value  ?></td>
                                        <?php
                                        break;
                                    case 2:
                                        ?>
                                        <td class='col-no-hijo' style="  <?php echo $font_size ?>"><?php echo $value ?></td>
                                        <?php
                                        break;
                                    case 3:
                                        ?>
                                        <td class='col-no-hijo' style=" <?php echo $font_size ?>"><?php echo $value ?></td>
                                        <?php
                                        break;
                                    case 4:
                                        ?>
                                        <td class='col-no-hijo' style="<?php echo $font_size ?>"><?php echo $value ?></td>
                                        <?php
                                        break;
                                    case 5:
                                        ?>
                                        <td class='col-no-hijo' style="<?php echo $font_size ?>"><?php echo $value ?></td>
                                        <?php
                                        break;

                                }
                                
                                $j++;
                            }
                            echo "</tr>";
                        }
                        //die();

                    endforeach; ?>
                </tbody>
            </table>
        </div>
        <table class='tb-firm' style="width: 100%; padding-top: 62px; padding-bottom: 0; padding-left: 60px;">
            <tr>
                <td class='firm-col-1' style="<?php echo $font_size_firma ?>">Docente:</td>
                <td class='firm-col-2' style="<?php echo $font_size_firma ?>"><?php echo $docentenombre; ?></td>
                <td class='firm-col-3' style="<?php echo $font_size_firma ?>">Dirección de Licenciatura</td>
                <td class='firm-col-4' style="<?php echo $font_size_firma ?>">Dirección de Servicios Escolares y Titulación</td>
            </tr>
        </table>
        <table class='tb-firma' style="width: 100%; margin-bottom: 0; padding-left: 70px; padding-right: 60px;">
            <tr style='height: 42px; padding-left: 15px; width: 100%;  '>
                <td class='firm2-col-1' style="<?php echo $font_size_firma ?>">ID:</td>
                <td class='firm2-col-2' style="<?php echo $font_size_firma ?>"><?php echo $docenteid; ?></td>
                <td class='firm2-col-3' style="<?php echo $font_size_firma ?>">Fecha de impresión: <?php echo $fechaimpresion; ?> </td>
                <td class='firm2-col-4' style="<?php echo $font_size_firma ?>">Fecha de Captura: <?php echo $fechacaptura; ?> </td>
                <td class='firm2-col-5' style="<?php echo $font_size_firma ?>">Página 1 de 1</td>
            </tr>
        </table>

    </div>
</body>
<script>
    JsBarcode(".barcode").init();
</script>

</html>