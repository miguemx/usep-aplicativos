create view as view_cal_alumnos(
    SELECT
    aca_grupos_alumnos.gpoalumno_alumno AS matricula,
    CONCAT(
        alumnos_generales.alumno_ap_paterno,
        " ",
        alumnos_generales.alumno_ap_materno,
        " ",
        alumnos_generales.alumno_nombres
    ) AS nombre,
    alumnos_generales.alumno_carrera,
    alumnos_generales.alumno_semestre,
    aca_grupos.grupo_clave,
    aca_grupos.grupo_materia,
    IF(
        aca_grupos_alumnos.`gpoalumno_extraordinario` IS NOT NULL,
        aca_grupos_alumnos.`gpoalumno_extraordinario`,
        aca_grupos_alumnos.`gpoalumno_ordinario`
    ) AS `calificacion`,
    aca_grupos_alumnos.`gpoalumno_grupo` AS `idc`,
    aca_grupos.`grupo_periodo` AS `periodo`,
    alumnos_generales.alumno_status AS alumno_status,
    aca_periodos.periodo_fin
    FROM
        `usep_aplicativos`.`aca_grupos_alumnos`
    INNER JOIN `usep_aplicativos`.`alumnos_generales` ON alumnos_generales.alumno_id = aca_grupos_alumnos.gpoalumno_alumno 
    INNER JOIN `usep_aplicativos`.`aca_grupos` on aca_grupos.grupo_id = aca_grupos_alumnos.gpoalumno_grupo
    INNER JOIN aca_periodos on aca_periodos.periodo_id = aca_grupos.grupo_periodo
    WHERE alumnos_generales.deleted_at IS NULL AND aca_periodos.periodo_id != '2022B' AND aca_grupos.grupo_materia NOT LIKE '%TUT'
    GROUP BY aca_grupos.`grupo_materia`
    ORDER BY `aca_grupos`.`grupo_materia` ASC;
);
-- misma vista que la anterior pero sin agrupar materia
create view view_cal_alumnos_v2 as (
SELECT
    a.gpoalumno_alumno AS matricula,
    CONCAT(
        b.alumno_ap_paterno,
        " ",
        b.alumno_ap_materno,
        " ",
        b.alumno_nombres
    ) AS nombre,
    b.alumno_carrera,
    b.alumno_semestre,
    c.grupo_clave,
    c.grupo_materia,
    IF(
        a.`gpoalumno_extraordinario` IS NOT NULL,
        a.`gpoalumno_extraordinario`,
        a.`gpoalumno_ordinario`
    ) AS `calificacion`,
    a.`gpoalumno_grupo` AS `idc`,
    c.`grupo_periodo` AS `periodo`,
    b.alumno_status AS alumno_status,
    d.periodo_fin
    FROM
        `usep_aplicativos`.`aca_grupos_alumnos` as a,
        `usep_aplicativos`.`alumnos_generales` as b,
        `usep_aplicativos`.`aca_grupos` as c,
        `usep_aplicativos`.`aca_periodos` as d
    WHERE 
    	a.gpoalumno_alumno=b.alumno_id AND
        a.gpoalumno_grupo=c.grupo_id AND
        c.grupo_periodo=d.periodo_id AND
    	b.deleted_at IS NULL AND
        d.periodo_id != '2022B' AND
        c.grupo_materia NOT LIKE '%TUT'


-- consulta para las calificaciones por periodo:
SELECT `matricula`, `nombre`, `alumno_carrera`, `alumno_semestre`, `grupo_clave`, `grupo_materia`, avg(`calificacion`), `idc`, `periodo`, `alumno_status`, `periodo_fin` FROM `view_cal_alumnos` GROUP BY `periodo`, `matricula`;