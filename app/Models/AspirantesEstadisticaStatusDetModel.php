<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesEstadisticaRegDet;

class AspirantesEstadisticaStatusDetModel extends Model
{
    protected $table="view_aspirantes_est_status_det";
    protected $returnType='App\Entities\AspirantesEstadisticaStatusDet';
    protected $allowedFields=['mujeres_enf','hombres_enf','mujeres_med','hombres_med','aspirante_estatus','aspirante_carrera','aspirante_sexo'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
/**
 * muestra la estadística por regiones de puebla
 * @return consulta colección de registros agrupados por región
 */
    public function estadisticadetstatus(){
        $consulta = $this->selectSum('mujeres_enf')
                         ->selectSum('hombres_enf')
                         ->selectSum('mujeres_med')
                         ->selectSum('hombres_med')
                         ->select('aspirante_estatus')
                         ->groupBy('aspirante_estatus')
                         ->orderBy('aspirante_estatus')
                         ->findAll();
                         
        //$consulta = $this->selectSum('n_alumnos')->select('region_nombre')->groupBy('region_nombre')->orderBy('region_nombre')->findAll();
        return $consulta;
    }
    public function status(){
        $consulta = $this->distinct('aspirante_estatus')
                         ->orderBy('aspirante_estatus')
                         ->find();
                         
        //$consulta = $this->selectSum('n_alumnos')->select('region_nombre')->groupBy('region_nombre')->orderBy('region_nombre')->findAll();
        return $consulta;
    }

 


}