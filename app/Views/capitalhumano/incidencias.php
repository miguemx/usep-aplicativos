<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    

<div class="container-fluid">
    <h2>Registro de Incidencias</h2>
    <form class="row g-3" method="post" action="<?php echo base_url('CapitalHumano/Incidencias'); ?>">
        <input name="empleado" type="hidden" value="<?php echo $empleadoid; ?>" />
        <div class="col-sm-6">
            <label for="fechaInicio" class="form-label">Fecha Inicial</label>
            <input type="date" class="form-control" id="fechaInicio" name="fechaInicio" value="<?php echo $inicio; ?>">
        </div>
        <div class="col-sm-6">
            <label for="fechaFinal" class="form-label">Fecha Final</label>
            <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="<?php echo $fin; ?>">
        </div>
        <div class="col-sm-2">
            <label for="id_empleado" class="form-label">Número de empleado</label>
            <input type="text" class="form-control" id="id_empleado" name="id_empleado";?>
        </div>
        <div class="col-12">
            <a href="<?php echo base_url('CapitalHumano/Personal'); ?>" class="btn btn-danger btn-sm">Regresar</a>
            <button type="submit" class="btn btn-secondary btn-sm">Buscar</button>
        </div>
    </form>
</div>
<div class="container-fluid" style="margin-top: 15px;">

    <?php if ( $error ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>


    <?php if ( !$error ): ?>
        <h5><?php echo $empleadoid.' - '.$nombre; ?></h5>
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                    <th>Incidencia</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($incidencias as $fecha=>$empleados): ?>
                    <tr>
                        <th><?php echo $fecha; ?></th>
                        <?php foreach ( $empleados as $id=>$empleado ): ?>
                            <td><?php echo $empleado['incidencias']['entrada']; ?></td>
                            <td><?php echo $empleado['incidencias']['salida']; ?></td>
                            <td>
                                <?php foreach($empleado['incidencias']['incidencias'] as $incidencia): ?>
                                <?php echo $incidencia.'<br />' ?>
                                <?php endforeach; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>    
    <?php endif; ?>

</div>

<?php echo $this->endSection() ?>
