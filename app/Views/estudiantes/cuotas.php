<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>&nbsp;</div>

    <?php if(isset($error)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php if(isset($exito)): ?>
        <div class="col-sm-12 alert alert-success" role="alert">
            <?php echo $exito; ?>
        </div>
    <?php endif; ?>

    <div class="container">
        
        <h5>Relación de cuotas reportadas</h5>
        <a href="<?php echo base_url('Estudiantes/AgregarCuota'); ?>" class="btn btn-secondary btn-sm">Agregar</a>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Concepto</th>
                        <th>Núm. de Referencia</th>
                        <th>Monto</th>
                        <th>Estatus</th>
                        <th>Observaciones</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php foreach( $pagos as $pago ): ?>
                        <tr>
                            <td><?php echo $pago->conceptoTexto; ?></td>
                            <td><?php echo $pago->referencia; ?></td>
                            <td><?php echo $pago->monto; ?></td>
                            <td><?php echo $pago->status; ?></td>
                            <td>
                                <?php echo $pago->observaciones; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
   
</div>

<?php echo $this->endSection() ?>