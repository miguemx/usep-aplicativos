<?php

namespace App\Models;

use CodeIgniter\Model;

class EvaluacionDocentePorcentajesModel extends Model
{
    protected $table = 'view_est_eval_doc_porcentajes';
    protected $primaryKey = 'empleado_id';
    protected $returnType = 'App\Entities\EvaluacionDocentePorcentaje';
    protected $useSoftDeletes = false;
    protected $allowedFields = [];
    protected $db;
    protected $builder;

    protected $useTimestamps = false;

    /**
     * muestra la estadística de respuestas de una evaluacion, ya catalogadas
     * @param evaluacion ID de evaluacion seleccionado
     * @return consulta colección de registros agrupados por región
     */
    public function estadisticadetstatus($evaluacion)
    {
        $consulta = $this
            ->selectSum('clasificacion1')
            ->selectSum('clasificacion2')
            ->selectSum('clasificacion3')
            ->selectSum('clasificacion4')
            ->select('respuestas_valor')
            ->select('materia_clave')
            ->select('materia_nombre')
            ->select('empleado_id')
            ->select('empleado_ap_paterno')
            ->select('empleado_ap_materno')
            ->select('empleado_nombre')
            ->select('gpoalumno_grupo')
            ->select('grupo_clave')
            ->select('evaluacion_id')
            ->groupBy('respuestas_valor')
            ->groupBy('gpoalumno_grupo')
            ->orderBy('gpoalumno_grupo')
            ->orderBy('respuestas_valor')
            ->where('evaluacion_id', $evaluacion)
            ->findAll();
        return $consulta;
    }
    /**
     * muestra la estadística de respuestas de una evaluacion, agrupadas por docente y materia
     * @param evaluacion ID de evaluacion seleccionado
     * @return consulta colección de registros agrupados por región
     */
    public function estxdocentemat($evaluacion)
    {
        $consulta = $this
            ->selectSum('clasificacion1')
            ->selectSum('clasificacion2')
            ->selectSum('clasificacion3')
            ->selectSum('clasificacion4')
            ->select('respuestas_valor')
            ->select('materia_clave')
            ->select('materia_nombre')
            ->select('empleado_id')
            ->select('empleado_ap_paterno')
            ->select('empleado_ap_materno')
            ->select('empleado_nombre')
            ->select('evaluacion_id')
            ->groupBy('empleado_id')
            ->groupBy('respuestas_valor')
            ->groupBy('materia_clave')
            ->orderBy('empleado_id')
            ->orderBy('materia_clave')
            ->orderBy('respuestas_valor')
            ->where('evaluacion_id', $evaluacion)
            ->findAll();
        return $consulta;
    }

    /**
     * devuelve los id de los docentes de una evaluación y nombre
     * @param evaluacion id de la evaluacion a consultar.
     * @return consulta colección de id's de los docentes
     */
    public function getDocentes($evaluacion)
    {
        $consulta = $this
            ->distinct('empleado_id')
            ->select('empleado_id')
            ->select('empleado_ap_paterno')
            ->select('empleado_ap_materno')
            ->select('empleado_nombre')
            ->where('evaluacion_id', $evaluacion)
            ->orderBy('empleado_id')
            ->findAll();
        return $consulta;
    }

    /**
     * devuelve las materias de un docente y de una evaluación es específico
     * @param evaluacion id de la evaluacion a consultar.
     * @param docente id del docente a consultar.
     * @return consulta colección de id's de los docentes
     */
    public function getMaterias($evaluacion, $docente)
    {
        $consulta = $this
            ->distinct('materia_clave')
            ->select('materia_clave')
            ->select('materia_nombre')
            ->where('evaluacion_id', $evaluacion)
            ->where('empleado_id', $docente)
            ->orderBy('empleado_id')
            ->findAll();
        return $consulta;
    }

    /**
     * muestra la estadística de respuestas de una evaluacion, agrupadas por docente y materia
     * @param evaluacion ID de evaluacion seleccionado
     * @param docente id del docente a consultar
     * @param materia id de la materia a consultar
     * @param respuesta clasificacion de las respuestas que dieron los alumnos al docente
     * @param grupo IDC del grupo seleccionado
     * @return consulta colección de registros agrupados por región
     */
    public function estxdocentematResp($evaluacion, $docente, $materia, $grupo = false/*,$respuesta*/)
    {
        if ($grupo) {
            $this->where('gpoalumno_grupo', $grupo);
        }
        $this
            ->selectSum('clasificacion1')
            ->selectSum('clasificacion2')
            ->selectSum('clasificacion3')
            ->selectSum('clasificacion4')
            ->select('respuestas_valor')
            ->select('materia_clave')
            ->select('materia_nombre')
            ->select('empleado_id')
            ->select('empleado_ap_paterno')
            ->select('empleado_ap_materno')
            ->select('empleado_nombre')
            ->select('evaluacion_id')
            ->groupBy('empleado_id')
            ->groupBy('respuestas_valor')
            ->groupBy('materia_clave')
            ->orderBy('empleado_id')
            ->orderBy('materia_clave')
            ->orderBy('respuestas_valor')
            ->where('evaluacion_id', $evaluacion)
            ->where('empleado_id', $docente)
            ->where('materia_clave', $materia);
        // ->where('respuestas_valor',$respuesta)

        $consulta = $this->findAll();
        return $consulta;
    }

    /**
     * devuelve las diferentes respuestas que ha tenido el docente
     * @param evaluacion id de la evaluacion a consultar.
     * @param docente id del docente a consultar.
     * @param materia id de la materia a consultar.
     * @return consulta colección de id's de los docentes
     */
    public function getRespuestas($evaluacion, $docente, $materia)
    {
        $consulta = $this
            ->distinct('respuestas_valor')
            ->select('respuestas_valor')
            ->select('materia_nombre')
            ->select('materia_clave')
            ->where('evaluacion_id', $evaluacion)
            ->where('empleado_id', $docente)
            ->where('materia_clave', $materia)
            ->orderBy('empleado_id')
            ->findAll();
        return $consulta;
    }
}
