<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Alumno extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'nombre' => null,
        'curp' => null,
        'sexo' => null,
        'correo' => null,
        'carrera' => null,
        'vigente' => null,
        'elseRegid' => null,
        'elseRegidpass' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'nombres' => null,
        'periodo' => null,
        'semestre' => null,
        'comentario' => null,
        'status' => null,

        'correoPersonal'        => null,
        'telefono'              => null,
        'calle'                 => null,
        'colonia'               => null,
        'numero'                => null,
        'interior'              => null,
        'cp'                    => null,
        'municipio'             => null,
        'estado'                => null,
        'pais'                  => null,
        'fechaNacimiento'       => null,
        'municipioNacimiento'   => null,
        'estadoNacimiento'      => null,
        'paisNacimiento'        => null,

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                    => 'alumno_id',
        'nombre'                => 'alumno_nombres',
        'curp'                  => 'alumno_curp',
        'sexo'                  => 'alumno_sexo',
        'correo'                => 'alumno_correo',
        'carrera'               => 'alumno_carrera',
        'vigente'               => 'alumno_vigente',
        'elseRegid'             => 'alumno_else_regid',
        'elseRegidpass'         => 'alumno_else_regpass',
        'apPaterno'             => 'alumno_ap_paterno',
        'apMaterno'             => 'alumno_ap_materno',
        'nombres'               => 'alumno_nombres',
        'periodo'               => 'alumno_periodo',
        'semestre'              => 'alumno_semestre',
        'comentario'            => 'alumno_comentario',
        'status'                => 'alumno_status',

        'correoPersonal'        => 'alumno_correo_personal',
        'telefono'              => 'alumno_telefono',
        'calle'                 => 'alumno_calle',
        'colonia'               => 'alumno_colonia',
        'numero'                => 'alumno_numero',
        'interior'              => 'alumno_num_int',
        'cp'                    => 'alumno_cp',
        'municipio'             => 'alumno_municipio',
        'estado'                => 'alumno_estado',
        'pais'                  => 'alumno_pais',
        'fechaNacimiento'       => 'alumno_nac_fecha',
        'municipioNacimiento'   => 'alumno_nac_municipio',
        'estadoNacimiento'      => 'alumno_nac_estado',
        'paisNacimiento'        => 'alumno_nac_pais',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
