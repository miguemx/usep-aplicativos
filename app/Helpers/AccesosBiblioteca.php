<?php
namespace App\Helpers;

use App\Models\AccesosBibliotecaModel;
use App\Models\AccesosBibliotecaViewModel;
use App\Entities\AccesoBiblioteca;


class AccesosBiblioteca {

    private $accesosBibliotecaModel;
    private $accesosBibliotecaViewModel;
    private $errors = [];
    private $lastError = '';

    /**
     * inicializa los modelos requeridos
     */
    public function __construct() {
        $this->accesosBibliotecaModel = new AccesosBibliotecaModel();
        $this->accesosBibliotecaViewModel = new AccesosBibliotecaViewModel();
    }

    /**
     * crea un registro de acceso para biblioteca
     * @param usuario el objeto Empleado o Alumno que desea realizar el registro
     * @param esAlumno true en caso de que sea alumno; false en caso contrario
     * @return id devuelve el ID del registro creado
     * @return false devuelve falso en caso de no poder generar el registro
     */
    public function crea( $usuario, $esAlumno ) {
        $folio = $this->accesosBibliotecaModel->vigente( $usuario->id, $esAlumno );
        if ( $folio === false ) {
            $folio = $this->inserta( $usuario, $esAlumno );
        }
        return $folio;
    }

    /**
     * registra en la base de datos un nuevo folio de acceso a biblioteca
     * @param usuario el objeto Empleado o Alumno que desea realizar el registro
     * @param esAlumno true en caso de que sea alumno; false en caso contrario
     * @return id devuelve el ID del registro creado
     * @return false devuelve falso en caso de no poder generar el registro
     */
    public function inserta( $usuario, $esAlumno ) {
        $acceso = new AccesoBiblioteca();
        if ( $esAlumno ) {
            $acceso->alumno = $usuario->id;
            $acceso->tipo = 'ESTUDIANTE';
        }
        else {
            $acceso->empleado = $usuario->id;
            if ( $usuario->docente == '1' )  $acceso->tipo = 'DOCENTE';
            else $acceso->tipo = 'ADMINISTRATIVO';
        } 
        $acceso->sexo = $usuario->sexo;
        $acceso->edad = \App\Helpers\Calculo::edadByCurp( $usuario->curp );

        try {
            $this->accesosBibliotecaModel->insert( $acceso );
            return $this->accesosBibliotecaModel->lastId();
        }
        catch(\Exception $ex) {
            log_message( 'error', 'ERROR: {ex}', [ 'ex'=>$ex ] );
            $this->errors[] = $ex->getMessage();
            $this->lastError = $ex->getMessage();
        }
        return false;
    }

    /**
     * busca un folio de acceso con base el ID del mismo
     * @param id el Id del folio de acceso
     * @return acceso los datos del acceso buscado
     * @return false en caso de que el folio no exista
     */
    public function busca($id) {
        $acceso = $this->accesosBibliotecaViewModel->find( $id );
        if ( !is_null($acceso) ) {
            return $acceso;
        }
        return false;
    }

    /**
     * registra la entrada o salida de un folio determinado
     * @param id el ID del folio de acceso
     * @return acceso el objeto de acceso
     * @return false en caso de no encontrar el folio o bien, que ya tenga registrada una entrada
     */
    public function registra( $id ) {
        $acceso = $this->accesosBibliotecaModel->find( $id );
        if ( !is_null($acceso) ) {
            try {
                if ( is_null($acceso->entrada) ) {
                    $acceso->entrada = date("Y-m-d H:i:s");
                    $acceso->salida = null;
                    $this->accesosBibliotecaModel->save( $acceso );
                    return $acceso;
                }
                else if ( is_null($acceso->salida) ) {
                    $acceso->salida = date( "Y-m-d H:i:s" );
                    $this->accesosBibliotecaModel->save( $acceso );
                    return $acceso;
                }
                else {
                    $this->errors[] = 'El código ya ha sido utilizado.';
                    $this->lastError = 'El código ya ha sido utilizado.';
                }
            }
            catch (\Exception $ex) {
                log_message( 'error', 'ERROR: {ex}', [ 'ex'=> $ex ] );
                $this->errors[] = $ex->getMessage();
                $this->lastError = $ex->getMessage();
            }
        }
        else {
            $this->errors[] = 'El código no existe.';
            $this->lastError = 'El código no existe.';
        }
        return false;
    }

    /**
     * registra la entrada de un folio determinado
     * @param id el ID del folio de acceso
     * @return acceso el objeto de acceso
     * @return false en caso de no encontrar el folio o bien, que ya tenga registrada una entrada
     */
    public function entrada( $id ) {
        $acceso = $this->accesosBibliotecaModel->find( $id );
        if ( !is_null($acceso) ) {
            try {
                if ( is_null($acceso->entrada) ) {
                    $acceso->entrada = date("Y-m-d H:i:s");
                    $acceso->salida = null;
                    $this->accesosBibliotecaModel->save( $acceso );
                    return $acceso;
                }
                else {
                    $this->errors[] = 'El código ya ha sido utilizado para acceder.';
                    $this->lastError = 'El código ya ha sido utilizado para acceder.';
                }
            }
            catch (\Exception $ex) {
                log_message( 'error', 'ERROR: {ex}', [ 'ex'=> $ex ] );
                $this->errors[] = $ex->getMessage();
                $this->lastError = $ex->getMessage();
            }
        }
        else {
            $this->errors[] = 'El código no existe.';
            $this->lastError = 'El código no existe.';
        }
        return false;
    }

    /**
     * devuelve la lista de los usuarios que registran solo entrada en biblioteca
     * @return aforo la lista con el aforo
     */
    public function aforo() {
        return $this->accesosBibliotecaViewModel->aforo();
    }

    /**
     * realiza una búsqueda de datos de accesos
     * @param filtros (opcional) arreglo con los filtros a enviar
     * @return registros los registros encntrados de acuerdo con los filtros
     */
    public function reporte( $filtros=array() ) {
        $filtros['entrada'] = 'SI';
        return $this->accesosBibliotecaViewModel->buscar( $filtros );
    }

    /**
     * devuelve el último error registrado
     * @return lastError la cadena con el ultimo error
     */
    public function getLastError() {
        return $this->lastError;
    }

    public function getError() {
        return $this->getLastError();
    }
    public function getMessage() {
        return $this->getLastError();
    }

    /**
     * ejecuta la consulta para marcar todos los entrantes con fecha de salida
     */
    public function cierra() {
        $this->accesosBibliotecaModel->cerrar();
    }

}