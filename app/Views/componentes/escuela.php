<?php 
if ( $disabled == true ) $disabled = 'disabled="disabled"';
?>

<div class="col-md-12 my-3"><!-- inicio del form group -->
    <div>
        <div class="card-header" style="padding: 10px 20px 0px 20px;">
            <?php echo $label; ?><?php if($mandatory)echo '*'; ?>
        </div>
        <!-- descripcion del campo -->
        <?php if($description): ?>
            <div style="margin-bottom: 0.5rem !important; font-size: 13px;"><?php echo $description; ?></div>
        <?php endif; ?>

        <div class="card-body" style="padding: 10px 20px 10px 20px;">
            <hr />
            <p>1. Busque su escuela.</p>
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                        <select class="form-control" id="cctestados" value="" onchange="getMunicipios(this)" <?php echo $disabled; ?>>
                            <option value="">Estado</option>
                            <?php foreach($estados as $estado): ?>
                                <option value="<?php echo $estado; ?>"><?php echo mb_strtoupper($estado); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                        <select class="form-control" id="cctmunicipios" value="" onchange="getEscuelas(this)" <?php echo $disabled; ?>>
                            <option value="">Municipio</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr />
            <p>2. Seleccione su escuela.</p>
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group input-group-outline my-3">
                        <select class="form-control" name="<?php echo $name; ?>" value="" id="cctescuelas" <?php echo $disabled; ?>>
                            <?php if($value): ?>
                                <option value="<?php echo $cct ?>"><?php echo $escuela; ?></option>
                            <?php else: ?>
                                <option value="">Escuela</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div>
                <?php if ( $disabled != 'disabled="disabled"' ): ?>
                    <a href="javascript:addescuela()" style="text-decoration: underline; font-size: 0.875em;">Mi escuela no aparece en el listado.</a>
                <?php endif; ?>
                </div>
            </div>
            <hr />
            <div class="row" id="addescuela" style="display: none;">
                <p>3. Si no encuentras tu escuela, puedes agregarla en este espacio.</p>
                <div id="s" class="form-text">Para agregar una nueva escuela, asegúrate que la has buscado por estado y municipio.</div>
                <div class="col-md-3">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Clave del Centro de Trabajo</label>
                        <input type="text" class="form-control" id="addcct" <?php echo $disabled; ?> />
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Nombre del centro educativo</label>
                        <input type="text" class="form-control" id="addcctnombre" <?php echo $disabled; ?> />
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-primary" onclick="guardaEscuela()" <?php echo $disabled; ?>>
                        Agregar escuela
                    </button>
                </div>
            </div>
        </div>

    </div>
</div><!-- fin del form group -->

<script>

    /**
     * obtiene desde ajax la lista de municipios dependiendo del estado seleccionado
     * @param obj el objeto que contiene el estado
     */
    async function getMunicipios(obj) {
        if( obj.value.length > 0 ) {
            let url = "<?php echo base_url().'/E/MpiosCCT/'; ?>" + obj.value;
            let data = {};
            resetSelect("cctmunicipios");
            let mpios = document.getElementById('cctmunicipios');
            let opt = document.createElement('option');
            opt.text = "Cargando municipios...";
            mpios.appendChild( opt );
            // Opciones por defecto estan marcadas con un *
            const response = await fetch(url, {
                method: 'GET', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'omit', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                // body: JSON.stringify(data) // body data type must match "Content-Type" header
            }).then( res=> res.json() ).catch( error => console.log(error) ).then( response => { return response; } );
            resetSelect("cctmunicipios");
            opt = document.createElement('option');
            opt.text = "Municipio";
            mpios.appendChild( opt );
            for( let i=0; i<response.length; i++) // parses JSON response into native JavaScript objects
            {
                opt = document.createElement('option');
                opt.text = response[i];
                opt.value = response[i];
                mpios.appendChild( opt );
            }
        }
    }

    function resetSelect(id) {
        let select = document.getElementById(id);
        while (select.firstChild) {
            select.removeChild(select.firstChild);
        }
    }

    /**
     * obtiene las escuelas
     */
    async function getEscuelas(obj) {
        let edo = document.getElementById("cctestados");
        if( obj.value.length > 0 ) {
            let url = "<?php echo base_url().'/E/cctbympio/'; ?>" + edo.value + "/" + obj.value;
            let data = {};
            resetSelect("cctescuelas");
            let esc = document.getElementById('cctescuelas');
            let opt = document.createElement('option');
            opt.text = "Cargando escuelas...";
            esc.appendChild( opt );
            // Opciones por defecto estan marcadas con un *
            const response = await fetch(url, {
                method: 'GET', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'omit', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                // body: JSON.stringify(data) // body data type must match "Content-Type" header
            }).then( res=> res.json() ).catch( error => console.log(error) ).then( response => { return response; } );
            resetSelect("cctescuelas");
            opt = document.createElement('option');
            opt.text = "Escuela";
            esc.appendChild( opt );
            for( let i=0; i<response.length; i++) // parses JSON response into native JavaScript objects
            {
                opt = document.createElement('option');
                opt.text = response[i].id + " - " + response[i].nombre + " (" + response[i].colonia + ")";
                opt.value = response[i].id;
                esc.appendChild( opt );
            }
        }
    }

    function addescuela() {
        let div = document.getElementById("addescuela");
        div.style.display = '';
    }

    async function guardaEscuela() {
        let url = "<?php echo base_url('E/AddEscuela'); ?>";
        let cct = document.getElementById("addcct");
        let cctnombre = document.getElementById("addcctnombre");
        let cctestados = document.getElementById("cctestados");
        let cctmunicipios = document.getElementById("cctmunicipios");
        let data = {
            cct: cct.value,
            nombre: cctnombre.value,
            estado: cctestados.value,
            municipio: cctmunicipios.value
        };
        // Opciones por defecto estan marcadas con un *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'omit', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                "X-Requested-With": "XMLHttpRequest"
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        }).then( res=> res.json() ).catch( error => console.log(error) ).then( response => { return response; } );
        if ( response.status == "ok" ) {
            let optionEscuela = document.createElement('option');
            optionEscuela.text = response.data.nombre;
            optionEscuela.value = response.data.cct;
            document.getElementById('cctescuelas').appendChild( optionEscuela );
            document.getElementById('cctescuelas').value = response.data.cct;            
        }
        else {
            alert ( response.data );
        }
    }

</script>