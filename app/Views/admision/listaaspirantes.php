<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .botonera {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }

    .footer {
        display: flex;
        justify-content: center;
    }
</style>

<?php if ($mensaje != '') : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>

<div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="row">
        <div class="mb-2">
            <form action="<?php echo base_url('ControlAdmision/ListadoAspirantes'); ?>" method="get" id="form-busqueda">
                <div class="row">
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">FOLIO</span>
                        <input name="folio" type="text" id="inp-folio" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $folio ?>">
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">CURP</span>
                        <input name="curp" type="text" id="inp-curp" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $curp ?>">
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                        <input name="nombre" type="text" id="inp-nombre" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $nombre ?>">
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">Paterno</span>
                        <input name="apPaterno" type="text" id="inp-paterno" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $apPaterno ?>">
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">Materno</span>
                        <input name="apMaterno" type="text" id="inp-materno" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $apMaterno ?>">
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">Carrera</span>
                        <select class="form-select" name="carrera" id="sel-carrera" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                            <option value="">Seleccione una opcion</option>
                            <?php foreach ($carrera_list as $carr) : ?>
                                <option value="<?php echo $carr->id ?>" <?php echo ($carr->id == $carrera) ? 'selected' : ''; ?>><?php echo $carr->nombre ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">Estatus</span>
                        <select class="form-select" name="estatus" id="sel-estatus" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                            <option value="">Seleccione una opcion</option>
                            <option value="EXITOSO" <?php echo ($estatus == 'EXITOSO') ? 'selected' : ''; ?>>EXITOSO</option>
                            <option value="OBSERVACIONES" <?php echo ($estatus == 'OBSERVACIONES') ? 'selected' : ''; ?>>OBSERVACIONES</option>
                            <option value="DECLINADO" <?php echo ($estatus == 'DECLINADO') ? 'selected' : ''; ?>>DECLINADO</option>
                            <option value="ESPERA" <?php echo ($estatus == 'ESPERA') ? 'selected' : ''; ?>>ESPERA</option>
                            <option value="REVISION" <?php echo ($estatus == 'REVISION') ? 'selected' : ''; ?>>REVISION</option>
                            <option value="REGISTRO" <?php echo ($estatus == 'REGISTRO') ? 'selected' : ''; ?>>REGISTRO</option>
                        </select>
                    </div>
                </div>
                <div class="text-center mt-1">
                    <button type="submit" class="btn btn-secondary">Buscar</button>
                    <button type="button" class="btn btn-secondary" onclick="limpiarbusqueda()">Limpiar busqueda</button>
                </div>
            </form>
        </div>
        <table class="table table-hover  table-fluid  text-center">
            <thead class="thead-default">
                <tr>
                    <th>FOLIO</th>
                    <th>CURP</th>
                    <th>NOMBRE COMPLETO</th>
                    <th>CARRERA</th>
                    <th>ESTATUS</th>
                    <th>FECHA DE CONCLUSIÓN O REVISIÓN</th>
                    <th class="text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($aspirantes  as $aspirante) : ?>
                    <tr>
                        <td><?php echo $aspirante->folio ?></td>
                        <td><?php echo $aspirante->curp ?></td>
                        <td><?php echo $aspirante->aspirante_ap_paterno . " " . $aspirante->aspirante_ap_materno . " " . $aspirante->aspirante_nombre ?></td>
                        <td><?php echo ($aspirante->carrera == 1) ? 'Licenciatura en Enfermería y Obstetricia' : 'Licenciatura en Médico Cirujano' ?></td>
                        <td><?php echo $aspirante->aspirante_estatus ?></td>
                        <td><?php echo date("Y-m-d", strtotime($aspirante->updated_at)); ?></td>
                        <td class=" btn_contenedor">
                            <?php if ($aspirante->aspirante_estatus == 'ESPERA'  || $aspirante->aspirante_estatus == 'REVISION') : ?>
                                <div class="botonera">
                                    <div>
                                        <form action="<?php echo base_url('ControlAdmision/BuscarAspirante'); ?>" method="post">
                                            <input type="hidden" name="aspirante-curp" value="<?php echo $aspirante->aspirante_curp ?>">
                                            <button type="submit" class="btn btn-secondary btn-sm">Editar</button>
                                        </form>
                                    </div>

                                    <div>
                                        <form action="<?php echo base_url('ControlAdmision/EtapasAspirantes'); ?>" method="post">
                                            <input type="hidden" name="aspirante-id" value="<?php echo $aspirante->aspirante_id ?>">
                                            <button type="submit" class="btn btn-secondary btn-sm">Revisar</button>
                                        </form>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="text-center">
                                    <?php if( $allowEdit ): ?>
                                        <div>
                                            <form action="<?php echo base_url('ControlAdmision/BuscarAspirante'); ?>" method="post">
                                                <input type="hidden" name="aspirante-curp" value="<?php echo $aspirante->aspirante_curp ?>">
                                                <button type="submit" class="btn btn-secondary btn-sm">Editar</button>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="footer">
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>
<script>
    function limpiarbusqueda() {
        document.getElementById('inp-folio').value = '';
        document.getElementById('inp-curp').value = '';
        document.getElementById('inp-nombre').value = '';
        document.getElementById('sel-carrera').value = '';
        document.getElementById('sel-estatus').value = '';
        let form = document.getElementById('form-busqueda');
        form.submit();
    }
</script>
<?php echo $this->endSection() ?>