<?php
namespace App\Entities;

use CodeIgniter\Entity;

class VotacionCandidatos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'correo' => null,
        'nombre'=> null,
        'voto'=> null,
        'ganador' => null,
        'proceso' => null
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'candidatos_id',
        'correo' => 'candidatos_correo',
        'nombre' => 'candidatos_nombre',
        'voto' => 'candidatos_voto',
        'ganador' => 'candidatos_ganador',
        'proceso' => 'candidatos_proceso'
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
