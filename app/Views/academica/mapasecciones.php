<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<?php if ($mensaje) : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<div class="container ">
    <?php //echo $rol . "<BR>" . $carrera ?>
    <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2 ">
        <div class="alert alert-info alert-dismissible fade show" role="alert" id="info_periodo" hidden>
            <div id="text_alert"></div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>


        <div class="row">
            <div class="col-sm">
                <div class="mb-3">
                    <label for="select_periodo" class="form-label">Periodo</label>
                    <select class="form-control" name="select_periodo" id="select_periodo" onchange="descripcion(this)  ">
                        <option value="0">Seleccione un periodo disponible</option>
                        <?php foreach ($periodos as $lista_periodo) : ?>
                            <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <script>
                function descripcion(contenido) {
                    if (contenido.value != '0') {
                        var periodos = <?php echo json_encode($periodos) ?>;
                        periodos.forEach(element => {
                            if (element.id == contenido.value) {
                                var alerta = document.getElementById('info_periodo');
                                var texto = document.getElementById('text_alert');
                                switch (element.estado) {
                                    case 'INICIO':
                                        texto.innerHTML = '';
                                        alerta.hidden = true;
                                        break;
                                    case 'INSCRIPCION':
                                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para la modificacion de horarios y cambios de docentes';
                                        alerta.hidden = false;
                                        break;
                                    case 'CURSO':
                                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                                        alerta.hidden = false;
                                        break;
                                    case 'CALIFICACIONES':
                                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                                        alerta.hidden = false;
                                        break;
                                    case 'FINALIZADO':
                                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                                        alerta.hidden = false;
                                        break;
                                }
                            }
                        });
                        document.getElementById('select_seccion').disabled = false;
                        getSecciones(contenido.value);
                    } else {
                        document.getElementById('select_seccion').disabled = true;
                    }
                }
                var getSecciones = async (periodo) => {
                    var seccion_select = '<?php echo ((isset($seccion)) ? $seccion : '') ?>';
                    var contenedor = document.getElementById('select_seccion');
                    contenedor.innerHTML = "";
                    let response = await fetch('<?php echo base_url("ProgramacionAcademica/getSeccionesPeriodo/$carrera") ?>/' + periodo, {
                        method: 'POST',
                    }).then(response => response.json()).then(result => {
                        console.log(result);
                        var option = document.createElement('option');
                        option.value = 0;
                        option.innerHTML = 'Seleccione una seccion disponible';
                        contenedor.appendChild(option);
                        result.forEach(element => {
                            var option = document.createElement('option');
                            option.value = element.clave;
                            option.innerHTML = element.clave;
                            if (element.clave == seccion_select) {
                                option.selected = true;
                            }
                            contenedor.appendChild(option);
                        });
                        boton(document.getElementById('select_seccion'))
                    }).catch(error => {
                        var option = document.createElement('option');
                        option.value = 0;
                        option.innerHTML = 'No hay secciones creadas para este periodo seleccionado';
                        contenedor.appendChild(option);
                    });
                }
            </script>
            <div class="col-sm">
                <div class="mb-3">
                    <label for="select_seccion" class="form-label">Sección</label>
                    <select class="form-select" name="select_seccion" id="select_seccion" onchange="boton(this)" disabled>
                    </select>
                </div>
            </div>
            <div class="col-m-1 ">
                <div class="mb-3 align-middle">
                    <button id="btn_buscar" type="button" class="btn btn-secondary" onclick="buscar()" disabled>Buscar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url('ProgramacionAcademica/HorarioSecciones'); ?>" method="post" id="form_busqueda">
</form>
<script>
    function boton(seccion) {
        if (seccion.value != 0)
            document.getElementById('btn_buscar').disabled = false;
        else document.getElementById('btn_buscar').disabled = true;
    }

    function buscar() {
        var form = document.getElementById('form_busqueda');
        form.innerHTML = '';
        var input = document.createElement('input');
        input.value = document.getElementById('select_periodo').value;
        input.name = 'periodo';
        input.hidden = true;
        form.appendChild(input);
        var input = document.createElement('input');
        input.value = document.getElementById('select_seccion').value;
        input.name = 'seccion';
        input.hidden = true;
        form.appendChild(input);
        form.submit();
    }
</script>
<?php if (isset($seccion)) : ?>
    <script>
        descripcion(document.getElementById('select_periodo'));
    </script>
    <div class="container-fluid">

        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
            <div class="text-center form-group mt-4">
                <?php // echo var_dump($permisos)."<br>"; ?>
                <label style="margin: 0 10px">SECCION:<h4 class="card-title"> <?php echo $seccion;  ?></h4> </label>
                <label style="margin: 0 10px">PERIODO:<h4 class="card-title"> <?php echo $periodo;  ?></h4> </label>

                <div class="contianer">
                    <table class="table table-striped-columns
                    table-hover	
                    table-borderless
                    
                    align-middle vw-70" >
                        <thead class="table-light">
                            <caption>Materias de la seccion <?php echo $seccion ?></caption>
                            <tr>
                                <th>MATERIA</th>
                                <th>DOCENTE</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            <?php foreach ($materias as $materia) : ?>
                                <tr class="text-left px-3">
                                    <td scope="row"><?php  echo mb_strtoupper($materia->materia." ".$materia->materiaNombre) ?></td>
                                    <td><?php  echo mb_strtoupper($materia->apPaterno." ".$materia->apMaterno." ".$materia->nombre_emp) ?></td>
                                    <td><?php echo mb_strtoupper($materia->id)  ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>


            </div>
            <table class="table table-borderless  mt-3 text-center">
                <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                    <tr>
                        <th scope="row">HORA</th>
                        <th>LUNES</th>
                        <th>MARTES</th>
                        <th>MIERCOLES</th>
                        <th>JUEVES</th>
                        <th>VIERNES</th>
                        <th>SABADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hora = 7;
                    ?>

                    <?php $bandera_row_dias = [0, 0, 0, 0, 0, 0, 0]; ?>
                    <?php while ($hora < 21) : ?>
                        <?php $index = 0; ?>
                        <tr>
                            <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                            <?php foreach ($dias as $dia) : ?>
                                <?php $bandera_coincidencia = false; ?>
                                <?php foreach ($grupos as $grupo) : ?>
                                    <?php if ($grupo->dia == $dia) : ?>
                                        <?php if ($grupo->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                            <?php
                                            $inicio = explode(':', $grupo->h_inicio);
                                            $fin = explode(':', $grupo->h_fin);
                                            $row_span = (intval($fin[0]) - intval($inicio[0]));
                                            ?>
                                            <td class="align-middle" id="<?php echo $grupo->grupo . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?>" rowspan="<?php echo $row_span + 1 ?>">
                                                <div <?php echo $grupo->h_inicio . " " . $grupo->h_fin ?>>
                                                    <?php if (($row_span + 1) > 1) {
                                                        $bandera_row_dias[$index] = ($row_span) + $bandera_row_dias[$index];
                                                    } ?>
                                                    <?php echo  $grupo->ma_nombre . '<br>' . (($grupo->docente == 1) ? 'Por asignar' : mb_strtoupper($grupo->em_paterno . ' ' . $grupo->em_materno . ' ' . $grupo->em_nombre)) . "<br>" . $grupo->au_nombre . "<br>" . $grupo->seccion . "<br>"  /* . $grupo->h_inicio . "-" . $grupo->h_fin . "<br>" . $grupo->dia */  ?>
                                                </div>
                                                <?php if ($grupo->carrera == $permisos['carrera'] || $permisos['rol'] == 'ADMINISTRADOR') : ?>
                                                    <?php if ($permisos['periodo']) : ?>
                                                        <div>
                                                            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#listas_grupo" name="btn_editar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre . "|$periodo"  ?>" onclick="getAulasDisponibles(this)"><i class="bi bi-pencil-square" style="font-size: 1.2rem; color: grey;"></i></button>
                                                            <button class="btn" name="btn_eliminar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre ?>" onclick="eliminar(this)"><i class="bi bi-trash container-fluid" style="font-size: 1.2rem; color: grey;"></i></button>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (!$bandera_coincidencia) : ?>
                                    <?php if ($bandera_row_dias[$index] != 0) : ?>
                                        <?php $bandera_row_dias[$index]--; ?>
                                    <?php else : ?>
                                        <td style="background-color: #bdb994;" class="align-middle">
                                            <?php if ($permisos['periodo']) : ?>
                                                <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#GruposDisponibles" value="<?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . "|$dia|$periodo" ?>" onclick="buscar_listas(this)"><i class="bi bi-plus-circle" style="font-size: 2rem; color: grey;"></i></button>
                                            <?php endif; ?>
                                        </td>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $index++; ?>
                            <?php endforeach; ?>
                        </tr>
                        <?php $hora++; ?>
                    <?php endwhile; ?>
                </tbody>
            </table>

        </div>
    </div>
    <div class="modal fade" id="listas_grupo" tabindex="-1" role="dialog" aria-labelledby="grupos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lista de aula</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="main_body">
                    <table class="table" id="main_table" hidden>
                        <thead>
                            <tr id="head_table">
                                <th>Aula</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>

                                    <div class="mb-3">
                                        <label for="aula_select" class="form-label">Seleccione un aula disponible</label>
                                        <select class="form-select" name="aula_select" id="aula_select" onchange="boton_guardar(this)">
                                        </select>
                                    </div>
                                </td>
                                <td id="botonera">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="mb-3">
                                        <label for="select_docente" class="form-label">Seleccione un docente disponible</label>
                                        <select class="form-select" name="select_docente" id="select_docente" onchange="boton_guardar_docente(this)">
                                        </select>
                                    </div>
                                </td>
                                <td id="botonera_docente">

                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="GruposDisponibles" tabindex="-1" role="dialog" aria-labelledby="grupos_disponibles" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Grupos disponibles</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="text-center m-3" id="cargando">

                    <div class="spinner-grow" style="width: 3rem; height: 3rem; color: rgb(132, 15, 49) !important;" role="status">
                        <span class="visually-hidden">Cargando...</span>

                    </div>
                </div>
                <div class="modal-body" id="main_body_materias" hidden>
                    <table class="table">
                        <thead>
                            <tr id="head_table">
                                <th>Materia</th>
                                <th>Seccion</th>
                                <th>Horas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="body_table_grupos">

                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="cambiarvista()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <form action="<?php echo base_url('ProgramacionAcademica/CambioAulas'); ?>" method="post" id="form-editar">
        <input type="hidden" name="bandera" value="<?php echo "HorarioSecciones/$seccion/$periodo";  ?>">
    </form>
    <form action="<?php echo base_url('ProgramacionAcademica/CambioDocenteGrupo'); ?>" method="post" id="form-editar-docente">
        <input type="hidden" name="bandera" value="<?php echo "HorarioSecciones/$seccion/$periodo";  ?>">
    </form>

    <form action="<?php echo base_url('ProgramacionAcademica/eliminarGrupoHora'); ?>" method="post" id="form-eliminar">
        <input type="hidden" name="bandera" value="<?php echo "HorarioSecciones/$seccion/$periodo";  ?>">
    </form>
    <form action="<?php echo base_url('ProgramacionAcademica/SaveGroup'); ?>" method="post" id="form-guardar">
        <input type="hidden" name="bandera" value="<?php echo "HorarioSecciones/$seccion/$periodo";  ?>">
    </form>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var getAulasDisponibles = async (datos) => {
            cambiarvista();
            var detalles = datos.value.split('|');
            document.getElementById('main_table').hidden = false;
            var select = document.getElementById('aula_select');
            select.innerHTML = '';
            var parametros = detalles[2] + '|' + detalles[3] + '|' + detalles[1] + '|' + detalles[5];
            let response = await fetch('<?php echo base_url("ProgramacionAcademica/getAulasDisponibles") ?>' + '/' + parametros, {
                method: 'POST',
            }).then(response => response.json()).then(result => {
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'Selecciona una opcion';
                select.appendChild(option);
                result.forEach(element => {
                    var option = document.createElement('option');
                    option.value = element.id_aula;
                    option.innerHTML = element.nombre_aula;
                    // console.log(element.id_aula);
                    select.appendChild(option);
                });
                var input = document.createElement('input');
                input.hidden = true;
                input.name = 'grupo_detalle';
                input.id = 'grupo_detalle';
                input.setAttribute('value', parametros + '|' + detalles[0] + '|' + detalles[4]);
                // input.value = ;
                document.getElementById('form-editar').appendChild(input);

            }).catch(error => {
                // return null;
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'No hay aulas disponibles en este horario';
                select.appendChild(option);
            });

            var select = document.getElementById('select_docente');
            select.innerHTML = '';
            let docentes = await fetch('<?php echo base_url("ProgramacionAcademica/getDocentesDisponibles") ?>' + '/' + detalles[2] + '/' + detalles[3] + '/' + detalles[1] + '/' + detalles[5], {
                method: 'POST',
            }).then(docentes => docentes.json()).then(result => {
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'Selecciona una opcion';
                select.appendChild(option);
                result.forEach(element => {
                    var option = document.createElement('option');
                    option.value = element.id;
                    option.innerHTML = (element.apPaterno + " " + element.apMaterno + " " + element.nombre).toUpperCase();
                    // console.log(element.id_aula);
                    select.appendChild(option);
                });
                var input = document.createElement('input');
                input.hidden = true;
                input.name = 'grupo_detalle';
                input.id = 'grupo_detalle';
                input.setAttribute('value', detalles[0]);
                document.getElementById('form-editar-docente').appendChild(input);

            }).catch(error => {
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'No hay mas docentes disponibles en este horario';
                select.appendChild(option);
            });

        }

        function boton_guardar(aula) {
            if (aula.value != 0) {
                var boton = document.createElement('button');
                var botonera = document.getElementById('botonera');
                botonera.innerHTML = '';
                boton.innerHTML = 'Guardar Cambios';
                boton.value = aula.value;
                boton.classList.add('btn', 'btn-secondary', 'mt-4');
                boton.setAttribute('onclick', 'editar(this)');
                botonera.appendChild(boton);
            } else {
                // alert('no es un aula ')
            }
        }

        function boton_guardar_docente(docente) {
            if (docente.value != 0) {
                var boton = document.createElement('button');
                var botonera = document.getElementById('botonera_docente');
                botonera.innerHTML = '';
                boton.innerHTML = 'Guardar Cambios';
                boton.value = docente.value;
                boton.classList.add('btn', 'btn-secondary', 'mt-4');
                boton.setAttribute('onclick', 'editar_docente(this)');
                botonera.appendChild(boton);
            } else {
                // alert('no es un aula ')
            }
        }

        function editar(aula) {
            var datos = document.getElementById('grupo_detalle').value;
            var contenedor = datos.split('|');
            mensajes(('Seguro que desea editar el aula de la materia ' + contenedor[5] + ' del dia ' + contenedor[2] + ' de la hora ' + contenedor[0] + '-' + contenedor[1] + ', ¿Desea continuar? '), 'form-editar', aula, 'grupo_aula');
        }

        function editar_docente(docente) {
            var datos = document.getElementById('grupo_detalle').value;
            var contenedor = datos.split('|');
            mensajes(('Seguro que desea editar el docente la materia ' + contenedor[5] + ' del dia ' + contenedor[2] + ' de la hora ' + contenedor[0] + '-' + contenedor[1] + ', ¿Desea continuar? '), 'form-editar-docente', docente, 'docente');
        }

        function eliminar(grupo) {
            var contenedor = grupo.value.split('|');
            mensajes(('¡ATENCIÓN ! Estas por eliminar la materia ' + contenedor[3] + ' del dia ' + contenedor[1] + ' en la hora ' + contenedor[2] + ', ¿Está seguro que deseas continuar? '), 'form-eliminar', grupo, 'grupo_detalles');
        }

        function mensajes(mensaje_1, submit, datos, name) {
            cambiarvista();
            Swal.fire({
                'title': 'Confirmación',
                'text': mensaje_1,
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    var form = document.getElementById(submit);
                    var input = document.createElement('input');
                    input.setAttribute('value', datos.value);
                    input.hidden = true;
                    input.name = name;
                    form.appendChild(input);
                    form.submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'No se realizó ningun movimiento',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }
        var buscar_listas = async (datos) => {
            cambiarvista();
            let response = await fetch('<?php echo base_url("ProgramacionAcademica/GrupoSecciones/$seccion/$periodo") ?>', {
                method: 'POST',
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                return null;
            });
            if (response) {
                var horario = datos.value.split('|');
                console.log(horario);
                console.log(response);
                let rol = <?php echo json_encode($permisos) ?>;
                var body = document.getElementById('body_table_grupos');
                body.innerHTML = '';
                for (let index = 0; index < response.length; index++) {
                    const element = response[index];
                    var tr = document.createElement('tr');
                    tr.id = response[index].grupo;
                    body.appendChild(tr);

                    var td = document.createElement('td');
                    td.innerHTML = response[index].materiaNombre.toUpperCase() + "<br><span style='font-size:9.5px;'>" + response[index].apPaterno.toUpperCase() + " " + response[index].apMaterno.toUpperCase() + " " + response[index].nombre_emp.toUpperCase() + "</span>";
                    tr.appendChild(td);


                    var td = document.createElement('td');
                    td.innerHTML = response[index].clave;
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    td.innerHTML = response[index].horas_asignadas + '/' + response[index].horas;
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    if (response[index].carrera == rol.carrera || rol.rol == 'ADMINISTRADOR') {
                        if (response[index].horas_asignadas < response[index].horas) {

                            var row = document.createElement('div');
                            row.classList.add('row');

                            var col1 = document.createElement('div');
                            col1.classList.add('col');

                            var select = document.createElement('select');
                            select.classList.add('form-select');
                            select.id = 'aula_seleccionada-' + response[index].grupo;
                            col1.appendChild(select);



                            let aulas = await fetch('<?php echo base_url("ProgramacionAcademica/getAulasDisponibles") ?>' + '/' + datos.value, {
                                method: 'POST',
                            }).then(aulas => aulas.json()).then(result => {

                                var option = document.createElement('option');
                                option.value = 1;
                                option.innerHTML = 'EN LINEA';
                                select.appendChild(option);
                                result.forEach(element => {
                                    var option = document.createElement('option');
                                    option.value = element.id_aula;
                                    option.innerHTML = element.nombre_aula;
                                    select.appendChild(option);
                                });
                                row.appendChild(col1);
                                var col2 = document.createElement('div');
                                col2.classList.add('col-2');

                                var boton = document.createElement('buton');
                                boton.classList.add('btn', 'btn-secondary');
                                boton.setAttribute('onclick', 'seleccionaaula(this)');
                                boton.value = response[index].materiaNombre + '|' + response[index].grupo + '|' + datos.value;
                                boton.innerHTML = 'Asignar';
                                col2.appendChild(boton);
                                row.appendChild(col2);
                            }).catch(error => {
                                var option = document.createElement('option');
                                option.value = 0;
                                option.innerHTML = 'No hay aulas disponibles en este horario';
                                select.appendChild(option);
                            });
                            td.appendChild(row);
                        }
                    }
                    tr.appendChild(td);
                }

            } else {
                console.log('sin datos');
            }
            document.getElementById('main_body_materias').hidden = false;
            document.getElementById('cargando').hidden = true;
        }
        var seleccionaaula = async (datos) => {
            cambiarvista();
            var contenedor = datos.value.split('|');
            var aula_select = document.getElementById('aula_seleccionada-' + contenedor[1]);
            console.log(datos.value);
            console.log(aula_select.options[aula_select.selectedIndex].text);
            var objeto = document.createElement('button');
            objeto.value = datos.value + '|' + aula_select.value;
            mensajes(('¡ATENCIÓN ! Estas por agregar la materia ' + (contenedor[0]).toUpperCase() + ' al dia ' + contenedor[4] + ' en un horario de ' + contenedor[2] + ' a ' + contenedor[3] + ' en el aula ' + aula_select.options[aula_select.selectedIndex].text + ', ¿Está seguro que deseas continuar? '), 'form-guardar', objeto, 'grupo_detalles');
        }

        function cambiarvista() {
            document.getElementById('main_body_materias').hidden = true;
            document.getElementById('cargando').hidden = false;
        }
    </script>
<?php endif; ?>
<?php echo $this->endSection() ?>