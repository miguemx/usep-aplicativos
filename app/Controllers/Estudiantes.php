<?php

namespace App\Controllers;

use App\Models\CredencialModel;
use App\Models\AlumnosModel;
use App\Models\CarrerasModel;
use App\Entities\Credencial;
use App\Models\ClinicasModel;
use App\Models\AlumnosSegurosModel;
use App\Entities\AlumnoSeguro;
use App\Entities\Constancia;
use App\Models\PeriodoModel;
use App\Models\KardexModel;
use App\Models\ConstanciasModel;
use App\Models\AlumnoGrupoHistorialModel;
use App\Models\GruposHorariosModel;
use App\Models\MateriaModel as MateriasModel;
use App\Models\RosModel;
use App\Models\HorarioAlumnosModel;

use App\Libraries\PagosEstudiantes;
use App\Helpers\Uploader;

use \Endroid\QrCode\Builder\Builder;
use \Endroid\QrCode\Encoding\Encoding;
use \Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use \Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use \Endroid\QrCode\Label\Font\NotoSans;
use \Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use \Endroid\QrCode\Writer\PngWriter;

class Estudiantes extends BaseController
{

    private $alumnosModel = null;
    private $carrerasModel = null;
    private $materiasModel = null;
    private $rosModel = null;
    private $horariosModel;
    private $periodoModel = null;
    private $constanciasModel;
    private $pagosEstudiantes = null;

    private $lastError;
    private $errors;

    /**
     * 
     */
    public function __construct()
    {
        $this->alumnosModel = new AlumnosModel();
        $this->carrerasModel = new CarrerasModel();
        $this->materiasModel = new MateriasModel();
        $this->rosModel = new RosModel();
        $this->horariosModel = new HorarioAlumnosModel();
        $this->periodoModel = new PeriodoModel();
        $this->constanciasModel = new ConstanciasModel();

        $this->lastError = '';
        $this->errors = [];
    }

    /**
     * renderiza el panel principal para los estudiantes
     */
    public function index()
    {
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        return view('home', $data);
    }

    /**
     * Muestra el diseño de credencial al estudiantes, y un enlace para la descarga en PDF
     */
    public function credencial()
    {
        $alumnosModel = new AlumnosModel();
        $data['error'] = 'Tus archivos todavía no han sido validados o no los has cargado.';
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find($this->sesion->get('id'));
        //var_dump($credencial);
        if (!is_null($credencial)) {
            if ($credencial->valida === '1') {

                unset($data['error']);
                $alumno = $alumnosModel->find($credencial->alumno);
                if ($alumno->status === "ACTIVO") {
                    $carrerasModel = new CarrerasModel();
                    $carrera = $carrerasModel->find($alumno->carrera);
                    $data['nombre'] =  $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres;
                    $data['matricula'] = $this->sesion->get('id');
                    $data['carrera'] = $carrera->nombre;
                    $data['vigencia'] = (is_null($credencial->vigencia)) ? '08/2020' : $credencial->vigencia;
                    $data['qrimage'] = $this->generaQr($this->sesion->get('id'));
                    $data['foto'] = base_url('Credenciales/Foto/' . $credencial->alumno);
                } else {
                    $data['error'] = 'Tus archivos todavía no han sido validados o no los has cargado.';
                }
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        $data['name'] = $this->sesion->get('nombre');
        //return redirect()->to('/login');
        return view('estudiantes/credencial', $data);
    }

    /**
     * Genera el PDF para poder imprimir la credencial, redirigiendo a un archivo PDF
     * @throws PageNotFoundException tira una página de 404 al no encontrar la credencial del estudiante
     */
    public function imprimeTuCredencial()
    {
        // Create an instance of the class:
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,]);
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find($this->sesion->get('id'));
        if (!is_null($credencial)) {
            try {
                $nombrefoto = $credencial->credencial_foto;
                $alumnogrupos = new AlumnoGrupoHistorialModel();
                $informacion = $alumnogrupos->gethistoria($this->sesion->get('id'));

                $foto = fopen(WRITEPATH . '/estudiantes/' . $credencial->alumno . '/' . $credencial->foto, 'r');
                $cadena = fread($foto, filesize(WRITEPATH . '/estudiantes/' . $credencial->alumno . '/' . $credencial->foto));
                $datos['foto'] =      "data:" . mime_content_type($foto) . ";base64," . base64_encode($cadena);
                $datos['qrimage'] =   $this->generaQr($this->sesion->get('id'));
                $datos['matricula'] = $this->sesion->get('id');
                $datos['carrera'] =   $informacion[0]->carrera_nombre;
                $datos['nombre'] =    $informacion[0]->alumno_nombre;
                $datos['vigencia'] =  $credencial->vigencia;
                $html = view('estudiantes/disenocredencialpdf', $datos);

                $mpdf->WriteHTML($html);
                $mpdf->Output($credencial->alumno . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
                fclose($foto);
            } catch (\Exception $x) {
                log_message("error", "ERROR {exception}", ["exception" => $x]);
            }
            //$mpdf->Output($credencial->alumno. '.pdf', \Mpdf\Output\Destination::FILE);
            die();
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * Muestra el diseño de la credencial en acceso público sin necesidad de sesión
     * @param matricula la matricula del estudiante
     * @return view la vista con solo el diseño de la credencial
     */
    public function identificacion($matricula = 0, $tipo = "web")
    {
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find($matricula);
        if (!is_null($credencial)) {
            if ($credencial->valida === '1') {
                $alumnosModel = new AlumnosModel();
                $alumno = $alumnosModel->find($credencial->alumno);
                $carrerasModel = new CarrerasModel();
                $carrera = $carrerasModel->find($alumno->carrera);
                $data['nombre'] =  $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres;
                $data['matricula'] = $matricula;
                $data['carrera'] = $carrera->nombre;
                $data['vigencia'] = $credencial->vigencia;
                $data['foto'] = base_url('Credenciales/Foto/' . $credencial->alumno);

                $data['qrimage'] = $this->generaQr($credencial->alumno);
                if ($tipo == "imprimible") {
                    return view('estudiantes/disenocredencialpdf', $data);
                } else {
                    return view('estudiantes/disenocredencialpdf', $data);
                }
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * imprime el formulario para la creacion de la credencial, buscando el pas
     * en el que se encuentra el estudiante
     */
    public function crearCredencial()
    {
        $data['paso'] = $this->getStatusCredencial();
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/cargacredencial', $data);
    }

    /**
     * Procesa los datos subidos por el estudiante para la creacion de su credencial
     * guardando los archivos y ajustando las banderas pertinentes
     */
    public function subirDatosCrendecial()
    {
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $tipo = $this->request->getPost('documento');
        if ($tipo == 'identificacion') {
            $fileRules = ['identificacion' => 'uploaded[identificacion]|ext_in[identificacion,pdf]|max_size[identificacion,2048]'];
            $fileRules_errors = ['identificacion' => ['uploaded' => 'Por favor sube una identificación correcta', 'ext_in' => 'La identificacion debe estar en un archivo PDF.', 'max_size' => 'El tamaño de archivo de la identificación no debe exceder los 2MB']];
        } else {
            $fileRules = ['foto' => 'uploaded[foto]|max_size[foto,512]|max_dims[foto,2000,2000]|mime_in[foto,image/jpg,image/jpeg]|ext_in[foto,jpg,jpeg]'];
            $fileRules_errors = [
                'foto' => ['uploaded' => 'Por favor sube una foto correcta', 'max_size' => 'La foto no debe tener un tamaño mayor de 2MB', 'mime_in' => 'Asegúrate que tu foto esté en un formato JPG o JPEG', 'ext_in' => 'La extensión del archivo de tu foto debe ser JPG o JPEG.'],
            ];
        }
        try {
            $esValido = $this->validate($fileRules, $fileRules_errors);
            if ($esValido) {
                $newName = $this->sesion->get('id') . '-' . $tipo . '.' . (($tipo == 'foto') ? 'jpg' : 'pdf');
                $file = $files[$tipo];
                $file->move(WRITEPATH . 'credenciales', $newName);
                if ($file->hasMoved()) {
                    $credencialModel = new CredencialModel();
                    $credencial = $credencialModel->find($this->sesion->get('id'));
                    if (is_null($credencial)) {
                        $credencial = new Credencial();
                        $credencial->alumno = $this->sesion->get('id');
                        $credencialModel->insert($credencial);
                    }
                    $credencial->$tipo = WRITEPATH . 'credenciales/' . $newName;
                    try {
                        $credencialModel->save($credencial);
                    } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
                        $data['error'] = "Ya has enviado tus datos previamente. Por favor verifica que estén correctos.";
                    } catch (\Exception $ex) {
                        $data['error'] = "Ha ocurrido un error inesperado. Por favor verifica que tu proceso esté en orden. De lo contrario, comunícate con Servicios Escolares.";
                    }
                }
            } else {
                $mensajes = $validation->getErrors();
                $data['error'] = $mensajes[$tipo];
            }
        } catch (\Exception $ex) {
            $data['error'] = 'Por favor sube un archivo correcto.';
        }
        $data['paso'] = $this->getStatusCredencial();
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/cargacredencial', $data);
    }

    /**
     * consulta todas las calificaciones de un estudiante y las muestra en la aplicacion
     */
    public function kardex()
    {
        $kardexModel = new KardexModel();
        $alumno = $this->alumnosModel->find($this->sesion->get('id'));
        if (!is_null($alumno)) {
            $periodos = $kardexModel->alumno($alumno);
            $data['kardex'] = $periodos;
            $data['menu'] = $this->sesion->get('menu');
            return view('estudiantes/kardex', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    public function kardexBecaManutencion()
    {
        $kardexModel = new KardexModel();
        $alumno = $this->alumnosModel->find($this->sesion->get('id'));
        if (!is_null($alumno) && $this->request->getPost('ll') == 'kxt') {
            $periodos = $kardexModel->alumno($alumno);
            $data['kardex'] = $periodos;
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            $horario = $kardexModel->horario($alumno->id);
            if (count($horario) > 0) {
                $data['fecha'] = date("d") . ' de ' . $this->getMes(date("m")) . ' del ' . date("Y");
                $data['folio'] = 'W' . $alumno->id . '/2021';
                $data['genero'] = ($alumno->sexo == 'M') ? 'la' : 'el';
                $data['sufijo'] = ($alumno->sexo == 'M') ? 'a' : 'o';
                $data['nombre'] = $alumno->nombres . ' ' . $alumno->apPaterno . ' ' . $alumno->apMaterno;
                $data['matricula'] = $alumno->id;
                $data['carrera'] = $carrera->nombre;
                $data['clavePrograma'] = $carrera->clavePrograma;
                $data['facultad'] = $carrera->facultad;
                $data['periodo'] = $horario[0]->periodo;
                // $data['qr'] = $this->generaQr( $constancia->id, 'E/c/' );
                $constanciaHtml = view('estudiantes/constanciakardexbeca', $data);
                $this->pdfConstancia($constanciaHtml, $alumno->id . '-KARDEX');
                /*  if ($url = ) {
                    return redirect()->to($url);
                    // return $constanciaHtml;
                } */
            }
            //var_dump($data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * consulta el horario de un estudiante
     */
    public function horario__()
    {
        $data['menu'] = $this->sesion->get('menu');
        $consultalumnos = new HorarioAlumnosModel();
        $arreglo = $consultalumnos->horarioAlumno($this->sesion->get('id'));
        //echo $this->sesion->get('id')."<br>";
        $idc_anterior = "";
        $captura = false;
        $contaidc = 1;
        $contacomp = 1;
        $arr_datos = [];
        $bandera = false;
        $dia1 = [];
        $dia2 = [];
        $dia3 = [];
        $dia4 = [];
        $dia5 = [];
        $dia6 = [];
        foreach ($arreglo as $horario) {
            $idc = $horario['gpoalumno_grupo'];
            if ($idc_anterior != $idc) {
                $bandera = false;
                if ($bandera === false) {
                    foreach ($arreglo as $comparacion) {
                        if ($comparacion['gpoalumno_grupo'] === $idc) {
                            $dia = $comparacion['gpoaula_dia'];
                            $inicio = $comparacion['gpoaula_inicio'];
                            $fin = $comparacion['gpoaula_fin'];
                            $aula = $comparacion['aula_nombre'];
                            if ($dia == "LUNES") {
                                $dia1[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            if ($dia == "MARTES") {
                                $dia2[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            if ($dia == "MIÉRCOLES") {
                                $dia3[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            if ($dia == "JUEVES") {
                                $dia4[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            if ($dia == "VIERNES") {
                                $dia5[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            if ($dia == "SÁBADO") {
                                $dia6[] = [
                                    'INICIO' => $inicio,
                                    'FIN' => $fin,
                                    'AULA' => $aula,
                                ];
                            }
                            $bandera = true;
                            $contacomp++;
                        }
                    }
                }
                $captura = false;
            } else {
                $captura = true;
            }
            if ($captura === false) {
                $arr_datos[] = [
                    "id" =>  $idc,
                    "seccion" => $horario['grupo_clave'],
                    "materia" => $horario['materia_nombre'],
                    "docente" => $horario['empleado_nombre'] . " " . $horario['empleado_ap_paterno'] . " " . $horario['empleado_ap_materno'],
                    "lunes" => $dia1,
                    "martes" => $dia2,
                    "miercoles" => $dia3,
                    "jueves" => $dia4,
                    "viernes" => $dia5,
                    "sabado" => $dia6,
                ];
                $dia1 = [];
                $dia2 = [];
                $dia3 = [];
                $dia4 = [];
                $dia5 = [];
                $dia6 = [];
            }
            $bandera = true;
            $contaidc++;
            $contacomp = 1;
            $idc_anterior = $idc;
        }
        $data['arreglo'] = $arr_datos;
        //echo json_encode($arr_datos);
        return view('estudiantes/horarioAlumnos', $data);
        //return view('estudiantes/aviso',$data);
    }

    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA EL TRATAMIENTO DEL SEGURO FACULTATIVO POR PARTE DEL ESTUDIANTE                             |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * renderiza la vista del registro al seguro y envia los mensajes respectivos a sus usuarios
     */
    public function seguro()
    {
        $alumnosSegurosModel = new AlumnosSegurosModel();
        $alumnoSeguro = $alumnosSegurosModel->find($this->sesion->get('id'));
        if (is_null($alumnoSeguro)) {
            $alumnoSeguro = $alumnosSegurosModel->crea($this->sesion->get('id'));
        }

        $pantalla = $alumnoSeguro->status;
        switch ($pantalla) {
            case 'ESPERA':
                return $this->seguroSeleccion($alumnoSeguro);
                break;
            case 'OBSERVADO':
            case 'SOLICITAR':
                return $this->seguroLlena($alumnoSeguro);
                break;
            case 'SOLICITADO':
            case 'COMPLETADO':
            case 'ENVIADO':
            case 'RECHAZADO':
                return $this->seguroResultado($alumnoSeguro);
                break;
            default:
                return $this->seguroSeleccion($alumnoSeguro);
                break;
        }
    }

    /**
     * genera la vista para la seleccion inicial del seguro facultativo
     * @param error null si no se desea reportar algun mensaje; string si se desea enviar un mensaje
     */
    private function seguroSeleccion($alumnoSeguro)
    {
        $data['menu'] = $this->sesion->get('menu');
        $data['name'] = $this->sesion->get('nombre');
        $alumnosSegurosModel = new AlumnosSegurosModel();
        if (!is_null($this->request->getPost('gf'))) {
            $seleccion = $this->request->getPost('seguro');
            if (is_null($seleccion)) {
                $data['error'] = 'Por favor selecciona alguna de las dos opciones';
            } else {
                $alumnoSeguro->status = ($seleccion == '1') ? 'SOLICITAR' : 'RECHAZADO';
                $alumnoSeguro->razon = $this->request->getPost('razon');
                $alumnoSeguro->razonSecundaria = $this->request->getPost('otrarazon');
                try {
                    $alumnosSegurosModel->save($alumnoSeguro);
                } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
                    // nada
                }

                if ($alumnoSeguro->status === 'SOLICITAR') {
                    return $this->seguroLlena($alumnoSeguro);
                } else {
                    $data['alumnoSeguro'] = $alumnoSeguro;
                    return view('estudiantes/seguroresultado', $data);
                }
            }
        }

        return view('estudiantes/seguroselecciona', $data);
    }

    /**
     * devuelve la vista del llenado de datos
     */
    private function seguroLlena($alumnoSeguro)
    {
        $data['menu'] = $this->sesion->get('menu');
        $data['name'] = $this->sesion->get('nombre');
        $data['alumnoSeguro'] = $alumnoSeguro;
        $alumnosModel = new AlumnosModel();
        $alumno = $alumnosModel->find($this->sesion->get('id'));
        if (!is_null($alumno)) {
            $carrerasModel = new CarrerasModel();
            $clinicasModel = new ClinicasModel();
            $alumnosSegurosModel = new AlumnosSegurosModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            $data['clinicas'] = $clinicasModel->getListado();
            $data['nombre'] = $alumno->nombres;
            $data['apPaterno'] = $alumno->apPaterno;
            $data['apMaterno'] = $alumno->apMaterno;
            $data['curp'] = $alumno->curp;
            $data['carrera'] = $carrera->nombre;
            if (!is_null($this->request->getPost('gfs'))) {
                $resGuardado = $this->guardaSeguro($alumno);
                if ($resGuardado === true) {
                    return view('estudiantes/seguroresultado', $data);
                } else {
                    $data['errors'] = $resGuardado;
                }
            }
            return view('estudiantes/seguro', $data);
        };
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    private function seguroResultado($alumnoSeguro)
    {
        $data['menu'] = $this->sesion->get('menu');
        $data['name'] = $this->sesion->get('nombre');
        $data['alumnoSeguro'] = $alumnoSeguro;
        return view('estudiantes/seguroresultado', $data);
    }

    /**
     * realiza el proceso de guardado del seguro
     * @param alumno el objeto con los datos generales del alumno
     * @return true en caso de que se realice correctamente el guardado
     * @return false en caso de que no se haya enviado informacion por parte del cliente
     * @return errors array con los errores detectados durante el guardado
     */
    private function guardaSeguro($alumno)
    {
        $errors = [];
        $subida = $this->request->getPost('gfs');
        if (!is_null($subida) && $subida == '1') {
            try {
                $alumnoSeguro = new AlumnoSeguro();
                $alumnoSeguro->matricula = $this->sesion->get('id');
                $alumnoSeguro->umf = $this->request->getPost('clinica');
                $alumnoSeguro->fecha = date("Y-m-d");

                $archivo = $this->subeImss();
                if (gettype($archivo) === 'string') {
                    $alumnoSeguro->archivo = $archivo;
                    $alumnoSeguro->status = 'SOLICITADO';
                    $alumnosSegurosModel = new AlumnosSegurosModel();
                    $alumnosSegurosModel->save($alumnoSeguro);
                    return true;
                } else {
                    return $archivo;
                }
            } catch (\mysqli_sql_exception $ex) {
                if (strstr($ex->getMessage(), 'CONSTRAINT')) {
                    $errors[] = "Por favor selecciona la clínica que te corresponde.";
                } else {
                    $errors[] = "No fue posible guardar la información.";
                }
            }
            return $errors;
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |   FINALIZA  METODOS PARA EL TRATAMIENTO DEL SEGURO FACULTATIVO POR PARTE DEL ESTUDIANTE                    |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * procesa el archivo subido para el seguro facultativo, y lo guarda
     * @return dirFileName la ruta completa del archivo definitivo
     * @return errors un arreglo con los errores
     */
    private function subeImss()
    {
        $errors = [];
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $fileRules = ['imss' => 'uploaded[imss]|max_size[imss,512]|ext_in[imss,pdf]'];
        $fileRules_errors = [
            'imss' => ['uploaded' => 'Por favor sube tu constancia de vigencia de derechos del IMSS.', 'max_size' => 'La constancia de vigencia de derechos no debe tener un tamaño mayor a 512kb',  'ext_in' => 'La constancia de vigencia de derechos debe ser un archivo PDF.'],
        ];
        try {
            $esValido = $this->validate($fileRules, $fileRules_errors);
            if ($esValido) {
                $dir = WRITEPATH . 'credenciales';
                $fileName = $this->sesion->get('id') . '-IMSS.pdf';
                $file = $files['imss'];
                if (file_exists($dir . '/' . $fileName)) unlink($dir . '/' . $fileName);
                $file->move($dir, $fileName);
                if ($file->hasMoved()) {
                    return $dir . '/' . $fileName;
                }
            } else {
                $mensajes = $validation->getErrors();
                $errors[] = $mensajes['imss'];
            }
        } catch (\Exception $ex) {
            $errors[] = 'Por favor sube un archivo correcto.';
        }
        return $errors;
    }

    /**
     * obtiene el status en el que se encuentra la credencial de un alumno
     */
    private function getStatusCredencial()
    {
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find($this->sesion->get('id'));
        $paso = 1;
        if (!is_null($credencial)) {
            $paso += (is_null($credencial->foto)) ? 0 : ((is_null($credencial->identificacion)) ? 1 : 2);
        }
        return $paso;
    }

    /**
     * Genera el codigo QR que irá en la parte baja de la credencial con base en la matricula del estudiante
     * @param id el identificador a validar
     * @param url la URL relativa a la que envia elQR
     * @return 
     */
    private function generaQr($id, $url = 'E/V/')
    {
        $infoQr = base_url($url . $id);
        //$infoQr = 'https://bit.ly/fsdrfgdf';
        $result = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($infoQr)
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->size(150)
            ->margin(1)
            ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->labelText('')
            ->labelFont(new NotoSans(20))
            ->labelAlignment(new LabelAlignmentCenter())
            ->build();
        return $result->getDataUri();
    }

    /**
     * separa el nombre del estudiante para completar sus datos como apellido paterno, materno y nombres
     * @param llave la cadena que deberá validar para poder asegurar que estamos accediendo de forma segura
     */
    public function separarNombres($llave)
    {
        if ($llave == '4c2c3df089fdbb10bf15f7da93f890a8f1c84861') {
            $alumnosModel = new AlumnosModel();
            $alumnos = $alumnosModel->findAll();
            foreach ($alumnos as $alumno) {
                try {
                    $datosNombre = preg_split("/\s/", $alumno->nombre, 3);
                    if (strlen($datosNombre[0]) < 3 || strlen($datosNombre[1]) < 3) {
                        throw new \Exception('Apellido compuesto');
                    }
                    echo $datosNombre[0] . ' - ' . $datosNombre[1] . ' - ' . $datosNombre[2] . '<br />';
                    $alumno->apPaterno = $datosNombre[0];
                    $alumno->apMaterno = $datosNombre[1];
                    $alumno->nombres = $datosNombre[2];
                    $alumnosModel->save($alumno);
                } catch (\Exception $ex) {
                    echo $alumno->nombre . ' could not be converted due to ' . $ex->getMessage() . '<br />';
                }
            }
        } else {
            echo '.';
        }
    }

    /**
     * muestra la lista de constancias
     */
    public function constancias()
    {
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/fueradeservicio', $data);
        die();
        return redirect()->to( base_url() );
        $constanciasModel = new ConstanciasModel();
        $res = $this->crearConstancia();
        if ($res !== null) {
            if ($res === true) {
                $data['exito'] = 'Constancia solicitada con éxito. Puedes ver tus constancias en el listado inferior';
            } else {
                $data['error'] = $res;
            }
        }

        $data['constancias'] = $constanciasModel->getEstudiante($this->sesion->get('id'));;
        $data['estudiante'] = $this->sesion->get('id');
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/constancias', $data);
    }
    

    /**
     * genera el PDF para la constancia solicitada y redirige al archivo generado
     */
    public function imprimeConstancia()
    {
        return redirect()->to( base_url() );
        $constanciasModel = new ConstanciasModel();
        $constancia = $constanciasModel->find($this->request->getPost('ceid'));
        if ($constancia !== null) {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->busca($constancia->alumno);
            if ($alumno != null) {
                $carrerasModel = new CarrerasModel();
                $carrera = $carrerasModel->find($alumno->carrera);
                $kardexModel = new KardexModel();
                $horario = $kardexModel->getMateriasActulales($alumno->id, $constancia->periodo);
                if (count($horario) > 0) {
                    $periodo = $this->periodoModel->find($constancia->periodo);
                    $data['fecha'] = date("d") . ' de ' . $this->getMes(date("m")) . ' del ' . date("Y");
                    $data['folio'] = 'W' . $constancia->id . '/' . substr($constancia->fechaExp, 0, 4);
                    $data['genero'] = ($alumno->sexo == 'M') ? 'la' : 'el';
                    $data['sufijo'] = ($alumno->sexo == 'M') ? 'a' : 'o';
                    $data['nombre'] = $alumno->nombres . ' ' . $alumno->apPaterno . ' ' . $alumno->apMaterno;
                    $data['matricula'] = $alumno->id;
                    $data['semestre'] = $this->getSemestre($alumno->semestre);
                    $data['carrera'] = $carrera->nombre;
                    $data['periodo'] = $horario[0]->periodo;
                    $data['periodoInicio'] = $periodo->inicio_c;
                    $data['periodoFin'] = $periodo->fin_c;
                    $data['periodoEvaluaciones'] = $periodo->evaluaciones;
                    $data['periodoVacaciones'] = $periodo->vacaciones;
                    $data['periodoAdministrativas'] = $periodo->administrativa;
                    $data['periodoRecesoEscolar'] = $periodo->receso;
                    $data['receso'] = ($periodo->receso) ? true : false;
                    $data['asignaturas'] = $horario;
                    $data['qr'] = $this->generaQr($constancia->id, 'E/c/');
                    $constanciaHtml = view('estudiantes/constancia', $data);
                    $this->pdfConstancia($constanciaHtml, $alumno->id . '' . $constancia->id);
                }
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }



    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |   INICIA  METODOS PARA EL TRATAMIENTO DE LAS CUOTAS DEL ESTUDIANTE                                         |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------
    /**
     * renderiza el listado de los pagos que ha realizado un estudiante
     */
    public function misCuotas()
    {
        $this->pagosEstudiantes = new PagosEstudiantes();
        $data['pagos'] = $this->pagosEstudiantes->alumno($this->sesion->get('id'));
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/cuotas', $data);
    }

    /**
     * renderiza el formulario para agregar pagos
     */
    public function agregarCuota()
    {
        $this->pagosEstudiantes = new PagosEstudiantes();
        $data = ['referencia' => '', 'concepto' => '', 'monto' => '', 'materia' => '', 'tipo' => ''];
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        $data['conceptos'] = $this->pagosEstudiantes->conceptos();
        $alumno = $this->alumnosModel->find($this->sesion->get('id'));
        $carrera = $this->carrerasModel->find($alumno->carrera);
        $materias = $this->materiasModel->findByCarrera($carrera->id);
        $data['materias'] = $materias;
        return view('estudiantes/cuotasagregar', $data);
    }

    /**
     * realiza el guardado de la cuota de un estudiante y redirige a su panel de cuotas
     */
    public function guardaCuota() {
        $this->pagosEstudiantes = new PagosEstudiantes();
        $uploader = new Uploader( $this->request );
        $data = ['referencia' => '', 'concepto' => '', 'monto' => '', 'materia' => 'materia', 'tipo' => ''];
        
        $dir = WRITEPATH . 'estudiantes/' . $this->sesion->get('id');
        $file = $uploader->subir( 'comprobante', $dir );
        if ( $file != false ) {
            if ( $this->pagosEstudiantes->registra( $this->sesion->get('id'), $file, $this->request->getPost() ) ) {
                $this->pagoEstudiantes->coteja( $this->request->getPost('referencia'), $this->pagoEstudiantes->pago );
                return redirect()->to('/Estudiantes/MisCuotas');
            }
            else {
                $data['errores'] = $this->pagosEstudiantes->getErrors();
            }
        }
        else {
            $data['errores'] = $uploader->getErrors();
        }
        $alumno = $this->alumnosModel->find($this->sesion->get('id'));
        $carrera = $this->carrerasModel->find($alumno->carrera);
        $materias = $this->materiasModel->findByCarrera($carrera->id);
        $data['conceptos'] = $this->pagosEstudiantes->conceptos();
        $data['materias'] = $materias;
        $data['referencia'] = $this->request->getPost('referencia');
        $data['concepto'] = $this->request->getPost('concepto');
        $data['monto'] = $this->request->getPost('monto');
        $data['materia'] = $this->request->getPost('materia');
        $data['tipo'] = $this->request->getPost('tipo');
        $data['name'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/cuotasagregar', $data);
    }

    /**
     * procesa el archivo subido para el seguro facultativo, y lo guarda
     * @param referencia el numero de referencia proporcionado por el usuario para crear el archivo
     * @return dirFileName la ruta completa del archivo definitivo
     * @return errors un arreglo con los errores
     */
    private function subePago($referencia)
    {
        $errors = [];
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $fileRules = ['comprobante' => 'uploaded[comprobante]|max_size[comprobante,512]|ext_in[comprobante,pdf]'];
        $fileRules_errors = [
            'comprobante' => ['uploaded' => 'Por favor sube tu comprobante de pago.', 'max_size' => 'El comprobante no debe tener un tamaño mayor a 512kb',  'ext_in' => 'El comprobante debe ser un archivo PDF.'],
        ];
        try {
            $esValido = $this->validate($fileRules, $fileRules_errors);
            if ($esValido) {
                $dir = WRITEPATH . 'estudiantes/' . $this->sesion->get('id');
                $fileName = $referencia . '-cuota.pdf';
                $file = $files['comprobante'];
                if (!is_dir($dir)) {
                    mkdir($dir, 0700);
                }
                $file->move($dir, $fileName);
                if ($file->hasMoved()) {
                    return $fileName;
                }
            } else {
                $mensajes = $validation->getErrors();
                $errors[] = $mensajes['comprobante'];
            }
        } catch (\Exception $ex) {
            $errors[] = 'Por favor sube un archivo correcto.';
        }
        return $errors;
    }
    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |   FINALIZA  METODOS PARA EL TRATAMIENTO LAS CUOTAS DEL ESTUDIANTE                                          |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * genera una constancia
     */
    private function crearConstancia()
    {
        $llave = $this->request->getPost('gce');
        if ($llave !== null && $llave === 'ok') {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find($this->sesion->get('id'));
            if ($alumno) {
                if ($alumno->status === "ACTIVO") {
                    if ($alumno->periodo !== null) {
                        $constanciasModel = new ConstanciasModel();
                        if ($constanciasModel->crea($alumno->id, $alumno->periodo, $this->request->getPost('tipo'))) {
                            return true;
                        } else {
                            return 'Ocurrió un error al generar la constancia.';
                        }
                    }
                    return 'No se ha podido generar la constancia. Puede deberse a que el estudiante no está registrado en alguna materia.';
                }
                return 'No se ha podido generar la constancia. El estudiante no está en estatus activo.';
            }
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
        return null;
    }

    /**
     * Genera el PDF de la constancia a partir del contenido
     * @param contenido el contenido en HTML de la constancia
     * @param nombre el nombre que se le debe dar al archivo
     * @return pdf el PDF generado
     */
    private function pdfConstancia($contenido, $nombre)
    {
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'letter', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 10,
            'margin_bottom' => 15,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $mpdf->AliasNbPages('[pagetotal]');
        $mpdf->SetHTMLHeader(view('estudiantes/constanciaheader'));
        $mpdf->SetHTMLFooter(view('estudiantes/constanciafooter'));
        $mpdf->WriteHTML($contenido);

        $mpdf->Output($nombre . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
        die();
    }

    /**
     * devuelve la cadena para la represetacion textual del semestre, de acuerdo al valor proporcionado
     * @param semestre el número del semestre al que está inscrito
     * @param periodo (opcional) el tipo de periodo (se puede llamar semestre, cuatrimestre, etc; por defecto semestre)
     * @param mayus (opcional) indica 1 para texto todo en mayúsculas, 2 para minúsculas, 3 para primera mayúscula, 4 para ucwords
     * @return texto el texto descriptivo del semestre/cuatrimestre
     */
    private function getSemestre($semestre, $periodo = 'semestre', $mayus = 1)
    {
        $texto = 'primer ';
        switch ($semestre) {
            case 1:
                break;
            case 2:
                $texto = 'segundo';
                break;
            case 3:
                $texto = 'tercer';
                break;
            case 4:
                $texto = 'cuarto';
                break;
            case 5:
                $texto = 'quinto';
                break;
            case 6:
                $texto = 'sexto';
                break;
            case 7:
                $texto = 'septimo';
                break;
            case 8:
                $texto = 'octavo';
                break;
            case 9:
                $texto = 'noveno';
                break;
            case 10:
                $texto = 'décimo';
                break;
            case 11:
                $texto = 'décimoprimer';
                break;
            case 12:
                $texto = 'décimosegundo';
                break;
            case 13:
                $texto = 'décimotercero';
                break;
            case 14:
                $texto = 'décimocuarto';
                break;
            case 15:
                $texto = 'décimoquinto';
                break;

            default:
                break;
        }
        $texto .= ' ' . $periodo;
        if ($mayus === 4) $texto = ucwords($texto);
        else if ($mayus === 3) $texto = ucfirst($texto);
        else if ($mayus === 2) $texto = mb_strtolower($texto);
        else $texto = mb_strtoupper($texto);
        return $texto;
    }
    public function desplegar()
    {
        return view('estudiantes/bloques');
    }
    private function conjuntobloques()
    {
        $alumno = $this->sesion->get('id');
    }
    public function agregarbloque()
    {
        $var = $this->request->getPost('idgrupo[]');
        var_dump($var);
    }

    /**
     * Valida la firma del reglamento de ingreso, permanencia y egreso.
     * @param arg1 próximamente
     * @return ret1 próximamente
     */
    private function firmaripel()
    {
        $data['menu'] = $this->sesion->get('menu');
        $data['menu'] = $this->sesion->get('id');
        $aulas = new AulasModel();
        $bitacoraAulasViewModel = new BitacoraAulasViewModel();
        $id_sesion = $this->sesion->get("id");
        $dia = $this->dia();
        $hora = date("H") . ":00";
        $data['sesion'] = $id_sesion;
        $data['dia'] = $dia;
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        return view('estudiantes/firmaRipel', $data);
    }

    /**
     * retorna la vista del horario
     */
    public function horario()
    {
        $periodo = $this->periodoModel->getPeriodohorario();
        if (count($periodo) == 0) {
            $periodo = null;
        }
        $alumno = $this->alumnosModel->find($this->sesion->get('id'));

        $alumnoperiodo = $this->periodoModel->find($alumno->periodo);

        if ($alumnoperiodo->estado == "CURSO") {
            $grupos = $this->horariosModel->getByPeriodo($alumno->id, $alumno->periodo);
            $data['periodo'] =  $alumno->periodo;
            $data['grupos'] =  $grupos;
            $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
            $data['menu'] = $this->sesion->get('menu');
            $data['mensajeperiodo'] = $periodo;
            return view('estudiantes/horario_actual', $data);
        } else {
            $data['menu'] = $this->sesion->get('menu');
            return view('estudiantes/nodisponible', $data);
        }
    }

    /**prueba para ver el horario dle alumno */
    public function algo($matricula)
    {

        $kardexModel = new KardexModel();
        $horario = $kardexModel->horario($matricula);
        echo json_encode($horario);
        die();
    }
    public function constanciasNoActivos()
    {
        $constanciasModel = new ConstanciasModel();
        $res = $this->crearConstancia();
        if ($res !== null) {
            if ($res === true) {
                $data['exito'] = 'Constancia solicitada con éxito. Puedes ver tus constancias en el listado inferior';
            } else {
                $data['error'] = $res;
            }
        }

        $data['constancias'] = $constanciasModel->getEstudiante($this->request->getPost('matricula'));
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/constancias', $data);
    }

    /**
     * Genera el registro del documento solicitado
     * @param matricula ID del alumno
     */
    public function DescargarDocumento($tipo)
    {
        $matricula = $this->sesion->get("id");
        $alumno = $this->alumnosModel->find($matricula);
        if ($alumno->status == 'ACTIVO') {
            $constancia = $this->constanciasModel->buscar(['alumno' => $matricula, 'tipo' => $tipo, 'periodo' => $alumno->periodo]);
            // echo json_encode($constancia);
            // die();
            if (!$constancia) {

                if ($this->constanciasModel->crea($alumno->id, $alumno->periodo, $tipo)) {
                    log_message("notice", " Se ha generado " . (($tipo == 'CONSTANCIA') ? 'una ' : 'un ') . "{tipo} para el alumno {matricula} POR EL USUARIO {usuario}", ["tipo" => $tipo, "matricula" => $matricula, "usuario" => $this->sesion->get('useremail')]);
                }
            } else {
                try {
                    $constancia[0]->fechaExp = date('Y-m-d');
                    $this->constanciasModel->update($constancia[0]->id, $constancia[0]);
                } catch (\Exception $ex) {
                    log_message("error", "No hay datos para actualizar $ex");
                }
            }
            $constancia = $this->constanciasModel->buscar(['alumno' => $matricula, 'tipo' => $tipo, 'periodo' => $alumno->periodo]);
            return redirect()->to('/E/d/' . $constancia[0]->id . "-" . crc32($constancia[0]->id));
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }
}
