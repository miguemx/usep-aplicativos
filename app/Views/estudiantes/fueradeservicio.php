<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .imagen {

        height: 50vh;
        width: 50vw;
        background-image: url(<?php echo base_url('img/conexion.png') ?>);
        background-size:auto;
        background-repeat: no-repeat;
        background-position: right;
        opacity: 0.5;
    }
</style>
<div class="container">
    <div class="bg-white  p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="row">
            <h1 class="display-3" >En este momento no está disponible el servicio de constancias de estudio. Si requieres tu documento de manera urgente, puedes visitar la Dirección de Servicios Escolares y Titulación.</h1>
            <div class="col imagen">
                
                <div class="col align-middle">
                    <!--<h1 class="display-3">Periodo en construccion</h1>
                    <p class="lead">Actualmente no hay un periodo disponible para la consulta de horario, espera a que este modulo este disponible, gracias</p>-->
                </div>
                
            </div>
    </div>

</div>
<?php echo $this->endSection() ?>
