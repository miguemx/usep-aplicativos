<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container ">
    <div class=" mt-5">
        <h2>Biblioteca digital</h2>
    </div>
    <hr />
    
    <?php if( session('rol_id') != '1027' ): ?>
    
    <div class=" contenedor ">
        <div class=" container  row">
            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://www.medicapanamericana.com/eureka%C2%A0" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Médica Panamericana</h2>
                <h4>Especialistas en el área de Ciencias de la salud durante todas sus etapas 
                        formativas desde hace más de 69 años. </h4>
                </article>
                <img src="<?php echo base_url('img/eureka3.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://search.ebscohost.com/login.aspx?authtype=ip,url,uid&custid=ns312737&groupid=main&user=usep&password=biblioteca*2023" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>EBSCO</h2>
                <h4>El proveedor líder de bases de datos de investigación, revistas electrónicas, suscripciones a revistas, 
                    libros electrónicos y servicio de descubrimiento para bibliotecas de todo tipo.</h4>
                </article>
                <img src="<?php echo base_url('img/ebsco-logo-color-screen.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://www.enferteca.com/" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Enferteca</h2>
                <h4>La Gran Biblioteca de Enfermería, basado en el mayor fondo editorial de habla hispana.</h4>
                </article>
                <img src="<?php echo base_url('img/enferteca.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="http://es.aclandanatomy.com/" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Acland</h2>
                <!--<h4>El VideoAtlas de Anatomía Humana de Acland contiene cerca de 330 videos de especímenes humanos reales con su apariencia y aspecto naturales.</h4>-->
                <h4>Para acceder al VideoAtlas de Anatomía Humana de Acland deberás otorgar las siguientes credenciales de acceso:<br></br> <br>Usuario: depuebla</br><br>Constraseña: trial01<br></h4>
                </article>
                <img src="<?php echo base_url('img/wolters.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://cienciasbasicas.lwwhealthlibrary.com/index.aspx" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Health Library</h2> 
                <h4>Is your portal to trusted Lippincott content including our most commonly required textbooks and multimedia assets for use in medical.</h4>
                </article>
                <img src="<?php echo base_url('img/wolters.png'); ?>" alt="">
            </div>
            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://ovides.ovidds.com/" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Ovid Español</h2>
                <h4>Nuestra nueva experiencia de lectura de Artículos ya está disponible para los clientes de Ovid. Nuestro objetivo es ayudar a los usuarios a acceder al contenido que necesitan lo más rápido posible, en una experiencia fácil de usar con acceso a herramientas que pueden mejorar aún más su investigación.</h4>
                </article>
                <img src="<?php echo base_url('img/wolters.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="http://ovidsp.ovid.com/ovidweb.cgi?T=JS&NEWS=n&CSC=Y&PAGE=browse&D=yrovft&ID=pue999&PASSWORD=cCXJzzx1" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Ovid Inglés</h2> 
                <h4>Our new journals reading experience is now available to Ovid customers. Our goal is to help users access the content they need as quickly as possible, in a user-friendly experience with access to tools that can further enhance their research.</h4>
                </article>
                <img src="<?php echo base_url('img/wolters.png'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://www.clinicalkey.com/student/" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Clinical Key</h2> 
                <h4><li>Colección de Libros que crece contínuamente. Hoy incluye 165 títulos de Medicna, que cubren más de 50 especialidades.</li>
                <li>Incluye +730 vídeos.</li> <li>Ofrece +65,000 imágenes de alta calidad en medicina.</li>
                </article>
                <img src="<?php echo base_url('img/elsevier_medicina.jpg'); ?>" alt="">
            </div>

            <div class="col-sm-3 container_foto ">
                <div class="ver_mas text-center">
                <span><a href="https://www.clinicalkey.com/student/nursing" target="_blank">
                    Acceder
                </a></span>
                </div>
                <article class="text-left">
                <h2>Nursing</h2> 
                <h4><li>Colección de contenidos que aumenta constantemente. Hoy incluye 68 libros de 35 especialidades de enfermería.</li> 
                    <li>Más de 5000 imágenes de alta calidad.</li>
                    <li>Incluye acceso a un área para docentes, con más de 600 recursos exclusivos para ellos.</li></h4>
                </article>
                <img src="<?php echo base_url('img/elsevier_enfermeria.jpg'); ?>" alt="">
            </div>
            
        </div>
    </div>

    <?php else: ?>
        <p>Estamos trabajando para ti.</p>
    <?php endif; ?>
    <hr />

</div>


</div>



<style type="text/css">
    .contenedor {
        height: 100%;
        padding: 0;
    }

    .container_foto {
        background-color: #d2d4d6;
        padding: 0;
        overflow: hidden;
        max-width: 350px;
        margin: 5px;
        height: 300px;
    }

    .container_foto article {
        padding: 10%;
        position: absolute;
        bottom: 0;
        z-index: 1;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }

    .container_foto h2 {
        color: #767d85;
        font-weight: 800;
        font-size: 25px;
        border-bottom: #767d85 solid 1px;
    }

    .container_foto h4 {
        font-weight: 300;
        color: #840f31;
        font-size: 13px;
    }

    .container_foto img {
        width: 80%;
        margin: 20px;
        top: 30;
        left: 0;
        opacity: 0.4;
        -webkit-transition: all 4s ease;
        -moz-transition: all 4s ease;
        -o-transition: all 4s ease;
        -ms-transition: all 4s ease;
        transition: all 4s ease;
    }

    .ver_mas {
        background-color: #840f31;
        position: absolute;
        width: 100%;
        height: 70px;
        bottom: 0;
        z-index: 1;
        opacity: 0;
        transform: translate(0px, 70px);
        -webkit-transform: translate(0px, 70px);
        -moz-transform: translate(0px, 70px);
        -o-transform: translate(0px, 70px);
        -ms-transform: translate(0px, 70px);
        -webkit-transition: all 0.2s ease-in-out;
        -moz-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        -ms-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
    }

    .ver_mas span > a {
        font-size: 28px;
        color: #fff;
        position: relative;
        margin: 0 auto;
        width: 100%;
        top: 13px;
        font-family: Arial, Helvetica, sans-serif;
        text-decoration: none;
        font-weight: bold;
    }


    /*hovers*/

    .container_foto:hover {
        cursor: pointer;
    }

    .container_foto:hover img {
        opacity: 0.1;
        transform: scale(1.5);
    }

    .container_foto:hover article {
        transform: translate(2px, -69px);
        -webkit-transform: translate(2px, -69px);
        -moz-transform: translate(2px, -69px);
        -o-transform: translate(2px, -69px);
        -ms-transform: translate(2px, -69px);
    }

    .container_foto:hover .ver_mas {
        transform: translate(0px, 0px);
        -webkit-transform: translate(0px, 0px);
        -moz-transform: translate(0px, 0px);
        -o-transform: translate(0px, 0px);
        -ms-transform: translate(0px, 0px);
        opacity: 1;
    }
</style>


<?php echo $this->endSection() ?>