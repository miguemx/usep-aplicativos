CREATE OR REPLACE VIEW view_est_eval_doc AS
SELECT
    `a`.`respuestas_id` AS `respuestas_id`,
    `a`.`respuestas_matricula` AS `respuestas_matricula`,
    `a`.`respuestas_grupo` AS `respuestas_grupo`,
    `a`.`respuestas_pregunta` AS `respuestas_pregunta`,
    `a`.`respuestas_valor` AS `respuestas_valor`,
    `a`.`respuestas_comentario` AS `respuestas_comentario`,
    `b`.`preguntas_formulario` AS `preguntas_formulario`,
    `b`.`preguntas_orden` AS `preguntas_orden`,
    `b`.`preguntas_titulo` AS `preguntas_titulo`,
    `b`.`preguntas_tipo` AS `preguntas_tipo`,
    `b`.`preguntas_clasificacion` AS `preguntas_clasificacion`,
    `c`.`formulario_nombre` AS `formulario_nombre`,
    `c`.`formulario_orden` AS `formulario_orden`,
    `c`.`formulario_instrucciones` AS `formulario_instrucciones`,
    `z`.`evaluacion_id` AS `evaluacion_id`,
    `z`.`evaluacion_titulo` AS `evaluacion_titulo`,
    `d`.`tipo_nombre` AS `tipo_nombre`,
    `d`.`tipo_valores` AS `tipo_valores`,
    `d`.`tipo_clasificacion` AS `tipo_clasificacion`,
    `e`.`alumno_ap_paterno` AS `alumno_ap_paterno`,
    `e`.`alumno_ap_materno` AS `alumno_ap_materno`,
    `e`.`alumno_nombres` AS `alumno_nombres`,
    `e`.`alumno_status` AS `alumno_status`,
    `e`.`alumno_semestre` AS `alumno_semestre`,
    `g`.`grupo_id` AS `grupo_id`,
    `g`.`grupo_clave` AS `grupo_clave`,
    `g`.`grupo_materia` AS `grupo_materia`,
    `g`.`grupo_periodo` AS `grupo_periodo`,
    `g`.`grupo_docente` AS `grupo_docente`,
    `h`.`materia_carrera` AS `materia_carrera`,
    `h`.`materia_semestre` AS `materia_semestre`,
    `h`.`materia_nombre` AS `materia_nombre`,
    `h`.`materia_clave` AS `materia_clave`,
    `h`.`materia_creditos` AS `materia_creditos`,
    `i`.`empleado_id` AS `empleado_id`,
    `i`.`empleado_correo` AS `empleado_correo`,
    `i`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `i`.`empleado_ap_materno` AS `empleado_ap_materno`,
    `i`.`empleado_nombre` AS `empleado_nombre`
FROM
    (
        (
            (
                (
                    (
                        (
                            (
                                (
                                    `usep_aplicativos`.`evaluacion_respuestas` `a`
                                JOIN `usep_aplicativos`.`evaluacion_preguntas` `b`
                                ON
                                    (
                                        `b`.`preguntas_id` = `a`.`respuestas_pregunta`
                                    )
                                )
                            JOIN `usep_aplicativos`.`evaluacion_formulario` `c`
                            ON
                                (
                                    `c`.`formulario_id` = `b`.`preguntas_formulario`
                                )
                            )
                        JOIN `usep_aplicativos`.`aplicativo_evaluacion` `z`
                        ON
                            (
                                `z`.`evaluacion_id` = `c`.`formulario_evaluacion`
                            )
                        )
                    JOIN `usep_aplicativos`.`evaluacion_tipo_pregunta` `d`
                    ON
                        (`d`.`tipo_id` = `b`.`preguntas_tipo`)
                    )
                JOIN `usep_aplicativos`.`alumnos_generales` `e`
                ON
                    (
                        `e`.`alumno_id` = `a`.`respuestas_matricula`
                    )
                )
            JOIN `usep_aplicativos`.`aca_grupos` `g`
            ON
                (
                    `g`.`grupo_id` = `a`.`respuestas_grupo`
                )
            )
        JOIN `usep_aplicativos`.`aca_materias` `h`
        ON
            (
                `h`.`materia_clave` = `g`.`grupo_materia`
            )
        )
    LEFT JOIN `usep_aplicativos`.`aplicativo_empleados` `i`
    ON
        (
            `i`.`empleado_id` = `g`.`grupo_docente`
        )
    )