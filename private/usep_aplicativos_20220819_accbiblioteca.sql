CREATE TABLE `usep_aplicativos`.`biblio_accesos` (
    `biblioacceso_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
    `biblioacceso_alumno` VARCHAR(9) NULL , 
    `biblioacceso_empleado` INT UNSIGNED NULL , 
    `biblioacceso_tipo` ENUM('ESTUDIANTE','ADMINISTRATIVO','DOCENTE') NOT NULL , 
    `biblioacceso_sexo` VARCHAR(1) NOT NULL , 
    `biblioacceso_edad` VARCHAR(2) NOT NULL , 
    `biblioacceso_entrada` DATETIME NULL , 
    `biblioacceso_salida` DATETIME NULL , 
    `created_at` DATETIME NULL , 
    `updated_at` DATETIME NULL , 
    `deleted_at` DATETIME NULL , 
    PRIMARY KEY (`biblioacceso_id`),
    FOREIGN KEY (`biblioacceso_alumno`) REFERENCES  alumnos_generales(alumno_id),
    FOREIGN KEY (`biblioacceso_empleado`) REFERENCES  aplicativo_empleados(empleado_id)
) ENGINE = InnoDB;

DROP VIEW IF EXISTS view_accesos_biblioteca;
CREATE VIEW view_accesos_biblioteca AS
SELECT 
    biblioacceso_id,
    biblioacceso_alumno,
    biblioacceso_empleado,
    biblioacceso_tipo,
    biblioacceso_sexo,
    biblioacceso_edad,
    biblioacceso_entrada,
    biblioacceso_salida,
    biblio_accesos.created_at as created_at,
    biblio_accesos.updated_at as updated_at,
    biblio_accesos.deleted_at as deleted_at,
    alumno_foto,
    alumno_id,
    alumno_correo ,
    alumno_nombres,
    alumno_ap_paterno,
    alumno_ap_materno,
    empleado_id ,
    empleado_correo ,
    empleado_nombre,
    empleado_ap_paterno,
    empleado_ap_materno,
    empleado_docente,
    carrera_nombre
FROM biblio_accesos
LEFT JOIN alumnos_generales ON biblioacceso_alumno=alumno_id
LEFT JOIN aplicativo_empleados ON biblioacceso_empleado=empleado_id
LEFT JOIN prein_carreras ON alumno_carrera=carrera_id
WHERE biblio_accesos.deleted_at is null;