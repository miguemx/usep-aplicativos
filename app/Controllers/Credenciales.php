<?php
namespace App\Controllers;

use App\Models\CredencialModel;
use App\Models\AlumnosModel;
use App\Models\CarrerasModel;

class Credenciales extends BaseController {

    public function index() {
        $data['credenciales'] = [];
        $credencialModel = new CredencialModel();
        $alumnosModel = new AlumnosModel();
        $carrerasModel = new CarrerasModel();
        $alumnos = $alumnosModel->where('alumno_status',"ACTIVO")->findAll();
        $i = 0;
        foreach ( $alumnos as $alumno ) {
            $credencial = $credencialModel->find( $alumno->id );
            if ( !is_null($credencial) ) {
                $carrera = $carrerasModel->find( $alumno->carrera );
                $data['credenciales'][$i]['matricula'] = $alumno->id;
                $data['credenciales'][$i]['nombre'] = $alumno->nombre;
                $data['credenciales'][$i]['carrera'] = $carrera->nombre;
                $data['credenciales'][$i]['foto'] = 'NO';
                $data['credenciales'][$i]['identificacion'] = 'NO';
                $data['credenciales'][$i]['valido'] = 'NO';
                $data['credenciales'][$i]['revisada'] = 'NO';
                $data['credenciales'][$i]['foto'] = ( is_null($credencial->foto) )? 'NO': 'SI';
                $data['credenciales'][$i]['identificacion'] = ( is_null($credencial->identificacion) )? 'NO': 'SI';
                $data['credenciales'][$i]['valido'] = ( $credencial->valida=='0' )? 'NO': 'SI';
                $data['credenciales'][$i]['revisada'] = ( $credencial->revisada=='0' )? 'NO': 'SI';
                $i++;
            }
            
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('credenciales/listado', $data);
    }

    public function validar($matricula) {
        $alumnosModel = new AlumnosModel();
        $carrerasModel = new CarrerasModel();
        $alumno = $alumnosModel->find($matricula);
        if ( !is_null($alumno) ) {
            $carrera = $carrerasModel->find( $alumno->carrera );
            $data['alumno']['matricula'] = $matricula;
            $data['alumno']['nombre'] = $alumno->nombre;
            $data['alumno']['carrera'] = $carrera->nombre;
            $data['alumno']['correo'] = $alumno->correo;
            $data['alumno']['foto'] = 'NO';
            $data['alumno']['identificacion'] = 'NO';
            $data['alumno']['valido'] = 'NO';
            $data['alumno']['revisada'] = 'NO';
            $credencialModel = new CredencialModel();
            $credencial = $credencialModel->find( $matricula );
            if ( !is_null($credencial) ) {
                $data['alumno']['foto'] = ( is_null($credencial->foto) )? 'NO': 'SI';
                $data['alumno']['identificacion'] = ( is_null($credencial->identificacion) )? 'NO': 'SI';
                $data['alumno']['valido'] = ( $credencial->valida=='0' )? 'NO': 'SI';
                $data['alumno']['revisada'] = ( $credencial->revisada=='0' )? 'NO': 'SI';
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('credenciales/validacion', $data);
    }

    public function doValidacion() {
        $datos = $this->request->getPost(); 
        $matricula = $this->request->getPost('matricula');
        if ( !is_null($matricula) ) {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find($matricula);
            $credencialModel = new CredencialModel();
            $credencial = $credencialModel->find( $matricula );
            if ( !is_null($credencial) ) {
                $credencial->revisada = '0';
                $credencial->valida = '0';
                $credencial->comentarioFoto = ( $datos['comentarioFoto'] )? $datos['comentarioFoto']: null;
                if( array_key_exists( 'revisada', $datos ) )  { 
                    $credencial->revisada = '1';
                }
                if ( $datos['foto'] == '1' && $datos['identificacion'] == '1' ) {
                    $credencial->valida = '1';
                    $credencial->comentarioFoto = NULL;
                    $this->notifica( $alumno->correo, 'Tu credencial está lista. puedes buscarla en el apartado <strong>Mi credencial</strong>' );
                }
                else {
                    if ( $datos['foto'] == '0' ) {
                        if ( file_exists($credencial->foto) )  unlink ( $credencial->foto );
                        $credencial->foto = NULL;
                        $this->notifica( $alumno->correo, 'La fotografía para tu credencial no es correcta. '.$datos['comentarioFoto'] );
                    }
                    if ( $datos['identificacion'] == '0' ) {
                        if ( file_exists($credencial->identificacion) )  unlink ( $credencial->identificacion );
                        $credencial->identificacion = NULL;
                        $this->notifica( $alumno->correo, 'La identificación que subiste para tu credencial no es correcta. '.$datos['comentarioFoto'] );
                    }
                }
                try {
                    $credencialModel->save( $credencial );
                }
                catch(\Exception $ex) {
                    
                }
            } 
            return redirect()->to('/Credenciales');
        }
    }

    /**
     * renderiza la foto de acuerdo a la matricula
     */
    public function foto($matricula) {
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find( $matricula );
        if ( !is_null($credencial) && !is_null($credencial->foto) ) {
            $foto = WRITEPATH.'/estudiantes/'.$credencial->alumno.'/'.$credencial->foto;
            return $this->response->setContentType('image/jpg')->download($foto, null);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * renderiza la foto de acuerdo a la matricula
     */
    public function identificacion($matricula) {
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find( $matricula );
        if ( !is_null($credencial) && !is_null($credencial->identificacion) ) {
            return $this->response->download($credencial->identificacion, null);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }


}