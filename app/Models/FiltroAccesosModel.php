<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroAccesosModel extends Model {
    protected $table      = 'filtro_accesos';
    protected $primaryKey = 'filtroacceso_id';

    protected $returnType    = 'App\Entities\FiltroAcceso';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'filtroacceso_folio','filtroacceso_temperatura','filtroacceso_oximetria',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

}