<div>
    <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
    <p>
        Bienvenido <strong><?php echo $nombre ?></strong>
    </p>
    <p>
        Se han generado comentarios sobre alguna o algunas de tus respuestas,
        verifica en la sección de Notificaciones de tu sesión en el portal de Admisión.
    </p>
    <?php foreach ($notificaciones as $notificacion) : ?>
        <p>
            <?php $linea = explode(':', $notificacion) ?>
            <strong><?php echo $linea[0] ?> : </strong> <?php echo $linea[1] ?>
        </p>
    <?php endforeach; ?>
    <p>
        Da clic
        <a href="<?php echo $enlace ?>">aquí</a>
        para dirigirte al portal de Aspirantes.
    </p>
    <p>
        Todo el staff de admisión te desea el mejor de los éxitos
    </p>
</div>