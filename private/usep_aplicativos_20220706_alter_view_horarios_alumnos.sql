drop view if exists`view_horario_alumnos`;

CREATE VIEW `view_horario_alumnos` AS
SELECT
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_id` AS `gpoalumno_id`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno` AS `gpoalumno_alumno`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` AS `gpoalumno_grupo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo` AS `empleado_correo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `empleado_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `empleado_ap_materno`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_dia` AS `gpoaula_dia`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_inicio` AS `gpoaula_inicio`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_fin` AS `gpoaula_fin`,
    `usep_aplicativos`.`aca_aulas`.`aula_id` AS `aula_id`,
    `usep_aplicativos`.`aca_aulas`.`aula_nombre` AS `aula_nombre`
FROM
    (
        (
            (
                (
                    (
                        `usep_aplicativos`.`aca_grupos_alumnos`
                    JOIN `usep_aplicativos`.`aca_grupos` ON
                        (
                            `usep_aplicativos`.`aca_grupos`.`grupo_id` = `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo`
                        )
                    )
                JOIN `usep_aplicativos`.`aca_grupos_aulas` ON
                    (
                        `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_grupo` = `usep_aplicativos`.`aca_grupos`.`grupo_id`
                    )
                )
            JOIN `usep_aplicativos`.`aca_aulas` ON
                (
                    `usep_aplicativos`.`aca_aulas`.`aula_id` = `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_aula`
                )
            )
        JOIN `usep_aplicativos`.`aplicativo_empleados` ON
            (
                `usep_aplicativos`.`aca_grupos`.`grupo_docente` = `usep_aplicativos`.`aplicativo_empleados`.`empleado_id`
            )
        )
    JOIN `usep_aplicativos`.`aca_materias` ON
        (
            `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
        )
    );