<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .input-error {
        border: 1px red solid !important;
    }

    .barra {
        max-height: 60vh;
        overflow-y: scroll;
    }
</style>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


<div class="container">
    <div class="bg-white rounded shadow p-5 mb-4 mt-1 ml-2 mr-2">
        <div class="row">
            <h4>Generador de Programacion Academica</h4>
        </div>

        <div class="row">
            <div class="col mt-2">
                <label for="input-clave-seccion">Clave de la Seccion</label>
                <div class="input-group ">
                    <select class="form-select" name="select_carrera" id="select-carrera" type="text" aria-label="Last name">
                        <option value="1">LEO</option>
                        <option value="2">MED</option>
                    </select>
                    <span class="input-group-text">-</span>
                    <input type="text" class="form-control" id="input-clave-seccion-grupo" name="clave_secciones_grupo" placeholder="101">
                </div>
                <small>El nombre que se le asignará a esta seccion</small>


            </div>
            <div class="col mt-2">
                <label for="input-num-secciones">Numero de Grupos</label>
                <input type="number" class="form-control" id="input-num-secciones" name="num_secciones">
                <small>El numero de materia que tendran los alumnos de esta sección</small>
            </div>


            <div class="col-2 mt-2">
                <label for="select-semestre">Semestre</label>
                <select class="form-select" name="select_semestre" id="select-semestre" type="text" aria-label="Last name">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>

            <div class="text-center mt-3">
                <button type="button" class="btn btn-secondary" onclick="crear_seccion()">Vista Previa</button>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <form action="<?php echo base_url('ProgramacionAcademica/CreateGrupos'); ?>" method="post" id="form-grupos">

        <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2" hidden id="main-data">
        </div>
    </form>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function crear_seccion() {
        let carrera = document.getElementById('select-carrera').value;
        let seccion_grupo = document.getElementById('input-clave-seccion-grupo').value;
        let num_secciones = document.getElementById('input-num-secciones').value;
        let semestre = document.getElementById('select-semestre').value;
        if (carrera && seccion_grupo && num_secciones && semestre) {
            //console.log('buscar materias');
            getMaterias(carrera, seccion_grupo, num_secciones, semestre);
        } else if (!num_secciones) {
            Swal.fire({
                'title': 'Atencion',
                'text': 'Asegurese de ingresar el numero de secciones que creara',
                'icon': 'info',
                confirmButtonColor: '#883232',


            });
        } else if (!seccion_grupo) {
            Swal.fire({
                'title': 'Atencion',
                'text': 'Asegurese de ingresar el nombre de la seccion',
                'icon': 'info',
                confirmButtonColor: '#883232',


            });
        }

    }
    var getMaterias = async (carrera, seccion_grupo, num_secciones, semestre) => {
        Swal.fire({
            'title': 'Atencion',
            'text': 'Asegurese de seleccionar una materia para cada grupo por crear',
            'icon': 'info',
            confirmButtonColor: '#883232',


        });
        // console.log('async');
        let response = await fetch('<?php echo base_url("ProgramacionAcademica/getMaterias") ?>' + '/' + carrera + '/' + semestre, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            console.error('Error:', error);
        });
        // console.log(response);
        if (response) {
            let contenedor = document.getElementById('main-data');
            contenedor.hidden = false;
            contenedor.innerHTML = '';

            let input_carrera = document.createElement('input');
            input_carrera.hidden = true;
            input_carrera.value = carrera;
            input_carrera.name = 'carrera';
            let input_semestre = document.createElement('input');
            input_semestre.hidden = true;
            input_semestre.value = semestre;
            input_semestre.name = 'semestre';
            let input_num_secciones = document.createElement('input');
            input_num_secciones.hidden = true;
            input_num_secciones.value = num_secciones;
            input_num_secciones.name = 'num_secciones';
            let input_seccion = document.createElement('input');
            input_seccion.hidden = true;
            input_seccion.value = seccion_grupo;
            input_seccion.name = 'grupo';
            contenedor.appendChild(input_carrera);
            contenedor.appendChild(input_semestre);
            contenedor.appendChild(input_num_secciones);
            contenedor.appendChild(input_seccion);
            for (let index = 1; index <= num_secciones; index++) {
                var div_row = document.createElement('div');
                div_row.classList.add('row', 'mt-1');
                contenedor.appendChild(div_row);
                var div_col = document.createElement('div');
                div_col.classList.add('col');
                div_row.appendChild(div_col);

                var selectList = document.createElement("select");
                selectList.id = "materias";
                selectList.name = "select_materia[]";
                selectList.classList.add('form-select');
                selectList.classList.add('mt-2');
                div_col.appendChild(selectList);
                var option = document.createElement('option');
                option.value = "";
                option.text = 'Seleccione un materia para asignar';
                selectList.appendChild(option);
                var option = document.createElement('option');
                option.value = "TUT";
                option.text = 'Hora de tutoria';
                selectList.appendChild(option);
                for (let y = 0; y < response.length; y++) {
                    let option = document.createElement('option');
                    option.value = response[y].clave;
                    option.text = response[y].nombre;
                    selectList.appendChild(option);
                }
               /*  var div_col = document.createElement('div');
                div_col.classList.add('col');
                div_row.appendChild(div_col);
                var selectList = document.createElement("select");
                selectList.id = "docentes";
                selectList.name = "select_materia[]";
                selectList.classList.add('form-select');
                selectList.classList.add('mt-2');
                div_col.appendChild(selectList); */

            }
            if (document.getElementById('main-data').clientHeight > 500) {
                document.getElementById('main-data').classList.add('barra');
            }
            let div = document.createElement('div');
            div.id = "botonera";
            div.classList.add('text-center', 'form-group', 'mt-3');
            let boton = document.createElement('button');
            boton.classList.add('btn', 'btn-secondary');
            boton.id = 'btn-crea-grupos';
            boton.innerHTML = "Crear Grupos";
            boton.type = 'button';
            boton.setAttribute("onclick", 'CrearGrupos()');
            div.appendChild(boton);
            contenedor.appendChild(div);

        }
    }
    /* var boton = document.getElementById('btn-crea-grupos');
    boton.addEventListener('click', CrearGrupos); */
    function CrearGrupos() {
        let flg_validar = false;
        let grupos = document.querySelectorAll('select[id="materias"]');
        for (let x = 0; x < grupos.length; x++) {
            if (grupos[x].value == '' /* || grupos[x].value != ' '  */ ) {
                // console.log(grupos[x].value);
                grupos[x].classList.add('input-error');
                flg_validar = true;
            } else {
                grupos[x].classList.remove('input-error');
            }

        }

        if (flg_validar) {
            Swal.fire({
                'title': 'ERROR',
                'text': 'Por favor revise los campos marcados en rojo ',
                'icon': 'error',
                confirmButtonColor: '#883232',


            });
        } else {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! No podrá realizar cambios posteriores una vez que continue ¿Está seguro que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('form-grupos').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }


        // alert("Estas por generar un catalogo de grupos,¿ estas seguro que deseas continuar?");

    }
</script>
<?php echo $this->endSection() ?>