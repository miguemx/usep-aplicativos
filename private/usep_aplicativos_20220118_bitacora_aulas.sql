
create table bitacora_aulas (
        num int(4) unsigned NOT NULL,
        id_docente int(10) unsigned NOT NULL,
        aula varchar(20)NOT NULL,
        aula_conf int(1)NOT NULL,
        f_pantalla int(1)NULL,
        f_cpu int(1)NULL,
        f_teclado int(1)NULL,
        f_mouse int(1)NULL,
        f_microfono int(1)NULL,
        f_camara int(1)NULL,
        f_control int(1)NULL,
        f_hubtab int(1)NULL,
        f_hubdisplay int(1)NULL,
        f_bocinas int(1)NULL,
        f_cables int(1)NULL,
        fecha timestamp NOT NULL
);
ALTER TABLE `bitacora_aulas` ADD FOREIGN KEY (`id_docente`) REFERENCES `aplicativo_empleados`(`empleado_id`);
ALTER TABLE `bitacora_aulas` CHANGE `num` `num` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`num`);

-- vista 

Create View view_bitacora_aulas as (
select
	a.gpoaula_aula,
    a.gpoaula_grupo,
    a.gpoaula_dia,
    a.gpoaula_inicio,
    a.gpoaula_fin,
    c.aula_nombre,
    b.grupo_docente,
    b.grupo_materia,
    b.grupo_periodo
from
	aca_grupos_aulas as a,
    aca_grupos as b,
    aca_aulas as c
WHERE
	a.gpoaula_grupo=b.grupo_id AND
    a.gpoaula_aula= c.aula_id);

INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'docente','*');