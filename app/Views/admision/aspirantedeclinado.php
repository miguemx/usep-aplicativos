<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<style>

.input-grupo.input-grupo-outline .formulario-control {
    background: none;
    border: 1px solid red;
    border-radius: 0.375rem;
    border-top-left-radius: 0.375rem !important;
    border-bottom-left-radius: 0.375rem !important;
    padding: 0.625rem 0.75rem
rem
 !important;
    line-height: 1.3 !important;
}

.div-select{

}

.seleccion{
    border: 1px red solid !important;
}

.clase1{
    display: none;
}

.input-error {
        border: 3px red solid !important;
        border-radius: 10px;
        border-top: 0px !important;
        border-left: 0px !important;
    }

</style>
<div class="container"> 
    <?php if(isset($msg)): ?>
        <div class="alert alert-secondary">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
              <div class="card card-plain">
                <div class="card-header">
                <h4 class="font-weight-bolder">Solicitud declinada</h4>
                  <p class="mb-0">
                      <?php echo $aspirante->nombre ?>, le informamos que, al revisar la validación de documentos, se han identificado las siguientes observaciones: .<br /><br />
                      <?php 
                        foreach ($notificaciones as $razones):
                            ?>
                    <p>
                        <?php $cabecera = explode(':', $razones->mensaje) ?>
                        <strong>
                            <?php echo $cabecera [0] ?>
                        </strong>: 
                        <?php echo $cabecera [1] ?>
                    </p>
                    <?php endforeach; ?>
                    <p>
                        Por lo anterior y conforme a lo establecido en la &quot;Convocatoria de Admisión 2022&quot;, su solicitud ha sido declinada.
                    </p>
                    <p>
                        Agradecemos el interés en formar parte de la comunidad USEP y le invitamos a permanecer pendiente de próximas convoctorias.
                    </p>
                </div>
                <div class="card-body">
                  
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-2 text-sm mx-auto" style="font-size: 20px !important;">
                  
                  </p>
              </div>

</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<?php echo $this->endSection() ?>