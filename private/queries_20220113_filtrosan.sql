CREATE view view_filtro_empleados AS

SELECT empleado_id as ID,
        empleado_ap_paterno as apPaterno,
        empleado_ap_materno as apMaterno,
        empleado_nombre as nombre,
        empleado_correo as correo,
        filtro_folio as folio, filtro_diabetes as diabetes, filtro_hipertension as hipertension, filtro_antecedentecard as cardiacos,
        filtro_enfermedadres as enfermedadRespiratoria, filtro_otropadecimiento as otroPadecimiento,filtro_fumador as esFumador, filtro_mayorfiebre as fiebre, 
        filtro_mayortos as tos,filtro_mayordifrespiratoria as dificultadRespiratoria, filtro_mayordolorcabeza as dolorCabeza, 
        filtro_menordolormuscular as dolorMuscular, filtro_menordolorarticular as dolorArticular, filtro_menorfatiga as fatiga,
         filtro_menorabdominal as dolorAbdominal, filtro_menoralteragusto as alteracionGusto,
        filtro_menoralteraolfato as alteracionOlfato, filtro_menornasal as escurrimientoNasal, filtro_menorconjuntivitis as conjuntivitis,
        filtro_menorescalofrios as escalofrios, filtro_menorviaje as estuvoViaje, filtro_menorcovid as contactoPersonaCovid,
        filtro_temp as temperatura, filtro_oxigen as oxigenacion, filtro_fecha as fecha
FROM `filtro_sanitario` 
INNER JOIN aplicativo_usuarios on filtro_correo=usuario_correo
INNER JOIN aplicativo_empleados on usuario_correo=empleado_correo  ;

-- La vista de estudiantes


CREATE view view_filtro_estudiantes AS

SELECT alumno_id as ID,
        alumno_ap_paterno as apPaterno,
        alumno_ap_materno as apMaterno,
        alumno_nombres as nombre,
        alumno_correo as correo,
        filtro_folio as folio, filtro_diabetes as diabetes, filtro_hipertension as hipertension, filtro_antecedentecard as cardiacos,
        filtro_enfermedadres as enfermedadRespiratoria, filtro_otropadecimiento as otroPadecimiento,filtro_fumador as esFumador, filtro_mayorfiebre as fiebre, 
        filtro_mayortos as tos,filtro_mayordifrespiratoria as dificultadRespiratoria, filtro_mayordolorcabeza as dolorCabeza, 
        filtro_menordolormuscular as dolorMuscular, filtro_menordolorarticular as dolorArticular, filtro_menorfatiga as fatiga,
         filtro_menorabdominal as dolorAbdominal, filtro_menoralteragusto as alteracionGusto,
        filtro_menoralteraolfato as alteracionOlfato, filtro_menornasal as escurrimientoNasal, filtro_menorconjuntivitis as conjuntivitis,
        filtro_menorescalofrios as escalofrios, filtro_menorviaje as estuvoViaje, filtro_menorcovid as contactoPersonaCovid,
        filtro_temp as temperatura, filtro_oxigen as oxigenacion, filtro_fecha as fecha
FROM `filtro_sanitario` 
INNER JOIN aplicativo_usuarios on filtro_correo=usuario_correo
INNER JOIN alumnos_generales on usuario_correo=alumno_correo 
WHERE alumnos_generales.deleted_at IS NULL


-- La vista de externos


CREATE view view_filtro_externos AS

SELECT 
        externo_correo as correo,
        externo_nombre as nombre,
        externo_dependencia as dependencia,
        externo_edad as rango_edad,
        externo_sexo as sexo,
        externo_categoria as categoria,
        filtro_folio as folio, filtro_diabetes as diabetes, filtro_hipertension as hipertension, filtro_antecedentecard as cardiacos,
        filtro_enfermedadres as enfermedadRespiratoria, filtro_otropadecimiento as otroPadecimiento,filtro_fumador as esFumador, filtro_mayorfiebre as fiebre, 
        filtro_mayortos as tos,filtro_mayordifrespiratoria as dificultadRespiratoria, filtro_mayordolorcabeza as dolorCabeza, 
        filtro_menordolormuscular as dolorMuscular, filtro_menordolorarticular as dolorArticular, filtro_menorfatiga as fatiga,
         filtro_menorabdominal as dolorAbdominal, filtro_menoralteragusto as alteracionGusto,
        filtro_menoralteraolfato as alteracionOlfato, filtro_menornasal as escurrimientoNasal, filtro_menorconjuntivitis as conjuntivitis,
        filtro_menorescalofrios as escalofrios, filtro_menorviaje as estuvoViaje, filtro_menorcovid as contactoPersonaCovid,
        filtro_temp as temperatura, filtro_oxigen as oxigenacion, filtro_fecha as fecha
FROM `filtro_sanitario` 
INNER JOIN filtro_usuarios_ext on filtro_correo=externo_correo


