<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
<?php if (isset($mensajecorreo)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $mensajecorreo; ?>
    </div>
<?php endif; ?>
        <?php if(isset($error)) :?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        <?php endif; ?>   
<?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
        <?php endif; ?>
        <?php if(isset($error)) :?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        <?php endif; ?>   

<?php if (isset($mensaje)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensaje; ?>
    </div>
<?php endif; ?>
<?php if (isset($mensajeexito)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensajeexito; ?>
    </div>
<?php endif; ?>
<?php if (isset($errorupdateproceso)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $errorupdateproceso; ?>
    </div>
<?php endif; ?>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/candidatos'); ?>">
        <h1>Registro de candidatos</h1>
        <br>
                <div class="idproceso">
                    <label for="selectproceso" class="form-label">Procesos disponibles</label>
                    <br>
                    <select id="selectproceso " class="custom-select" style="width: 80%;" name="selectproceso" required>
                        <option value = "">Selecciona procesos</option>
                        <?php foreach ($procesos as $arregloprocesos) : ?>
                            <option value = <?php echo $arregloprocesos->id ?> <?php if ($idproceso == $arregloprocesos->id) echo 'selected="selected"'?>><?php
                                echo $arregloprocesos->nombre
                            ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="correo">
                    <label for="correocandidato" class="form-label">Ingresa correo</label>
                    <input type="text" class="form-control" style="justify-content:center" id="correocandidato" name="correocandidato" required>
                </div>
                <div class="correo">
                    <label for="nombrecandidato" class="form-label"> Ingresa nombre</label>
                    <input type="text" class="form-control" style="justify-content:center" id="nombrecandidato" name="nombrecandidato"required>
                </div>
                <div class="guardar" style="display:flex; table-cell; justify-content:center">
                    <button type="submit" class="btn btn-secondary" name="btn1" value="1">Guardar y registrar otro candidato</button>&nbsp&nbsp&nbsp
                    <button  type="submit" class="btn btn-secondary" name="btn2" value="2">Guardar y finalizar registro de candidatos</button>
                </div>
    </form>
</div>
<?php echo $this->endSection() ?>