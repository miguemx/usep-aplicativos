<?php
namespace App\Models;
use CodeIgniter\Model;

class TiposDocumentosModel extends Model {
    protected $table      = 'prein_tipos_documentos';
    protected $primaryKey = 'tipodoc_id';

    protected $returnType    = 'App\Entities\TipoDocumento';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'tipodoc_id','tipodoc_nombre','tipodoc_requerido','tipodoc_formatos',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene todos los tipos de documentos vigentes para solicitar
     */
    public function getTiposVigentes() {
        $tiposVigentes = $this->where('tipodoc_requerido','1')->findAll();
        return $tiposVigentes;
    }
    
}
