<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }

    .formulario-cabecera,
    .formulario-datos {
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }

    .error {
        border: red solid 2px;
    }
</style>
<?php if (isset($mensaje)) : ?>
    <div class="alert <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
    </div>
<?php endif; ?>
<div class="container">

    <div class="card mt-1">

        <div class="card-body">
            <table class="table table-borderless  table-fluid">
                <thead class="thead-default">
                    <tr>
                        <th>ETAPA</th>
                        <th>PERIODO</th>
                        <th>FORMULARIO</th>
                        <th>TITULO DEL FORMULARIO</th>
                        <th>SECUENCIA</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $formularioinformacion[0]->etapa_nombre ?></td>
                        <td><?php echo $formularioinformacion[0]->etapa_periodo  ?></td>
                        <td><?php echo $formularioinformacion[0]->formulario_nombre  ?></td>
                        <td><?php echo $formularioinformacion[0]->formulario_titulo  ?></td>
                        <td><?php echo $formularioinformacion[0]->formulario_orden  ?></td>
                    </tr>
                </tbody>
            </table>
            <div class="formulario-datos text-center">
                <label> <h5 class="card-title text-uppercase">Instrucciones del formulario:</h5> <?php echo $formularioinformacion[0]->formulario_instrucciones  ?></label>

            </div>
            <?php $id_form = $formularioinformacion[0]->formulario_id; ?>
            
            <div class="lista-items mt-2">
                <?php if (isset($items_form)) : ?>
                    <table class="table table-hover  table-fluid">
                        <thead class="thead-default">
                            <tr>
                                <td colspan="8" class="text-center">
                                    Campos Creados
                                </td>
                            </tr>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Tipo de dato</th>
                                <th>Dependiente de</th>
                                <th>Secuencia</th>
                                <th>Obligatorio</th>
                                <th>Visible en Panel de revisión</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items_form as $lista) : ?>
                                <tr>
                                    <td><?php echo $lista->campo_nombre ?></td>
                                    <td><?php echo $lista->campo_descripcion ?></td>
                                    <td><?php echo $lista->tipocampo_nombre ?></td>
                                    <td><?php
                                        $bandera_dependiente = false;
                                        foreach ($items_form as $encontrar) {
                                            if ($lista->campo_dependiente == $encontrar->campo_id) {
                                                echo $encontrar->campo_nombre;
                                                $bandera_dependiente = true;
                                            }
                                        }
                                        if ($bandera_dependiente == false) {
                                            echo "No tiene dependencia";
                                        }
                                        ?></td>
                                    <td><?php echo $lista->campo_orden ?></td>
                                    <td><?php echo ($lista->campo_obligatorio == 1) ? "Obligatorio" : "No obligatorio"; ?></td>
                                    <td><?php echo ($lista->campo_visualizar == 1) ? 'Visible'  : ""; ?></td>
                                    <td>
                                        <div class="iconos-grupo">
                                            <div>
                                                <form action="editarimen" method="post" id="itemdeleted">
                                                    <input type="hidden" name="identidicadoritem" value="<?php echo $lista->campo_id ?>">
                                                    <input type="hidden" name="formulario-identificador" value="<?php echo $id_form ?>">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px" id="editar">
                                                        <i class="material-icons">mode_edit</i>
                                                    </button>
                                                </form>
                                            </div>
                                            <div>
                                                <form action="eliminaritem" method="post" onsubmit="return confirmaItem();">
                                                    <input type="hidden" name="identidicadoritem" value="<?php echo $lista->campo_id ?>">
                                                    <input type="hidden" name="formulario-identificador" value="<?php echo $id_form ?>">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px" id="eliminar">
                                                        <i class="material-icons">delete_forever</i>
                                                    </button>
                                                </form>
                                            </div>
                                    </td>
                                </tr>
                            <?php endforeach;  ?>
                        </tbody>
                    </table>


                <?php endif;  ?>
            </div>
            <form action="agregaritemformulario" method="post">
                <input type="hidden" name="formulario-identificador" value="<?php echo $id_form ?>">
                <div>
                    <h4 class="card-title">Selecciona que item agregar</h4>
                    <div>
                        <div class="mb-3">
                            <div>

                                <label for="etiqueta-nombre" class="form-label">Nombre del campo</label>
                                <input type="text" class="form-control" name="etiqueta-nombre" id="etiqueta-nombre" aria-describedby="helpId" placeholder="ej.Nombre">
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>
                            <br>
                            <div>
                                <div class="mb-3">
                                    <label for="help-text" class="form-label">Descripccion del Campo</label>
                                    <textarea rows="4" type="text" class="form-control" name="help-text" id="help-text" aria-describedby="helpId" placeholder="" require></textarea>
                                    <small id="helpId" class="form-text text-muted">Ingresa a una breve indicación de lo que deberán realizar en ese campo</small>
                                </div>
                            </div>
                            <br>
                            <div>

                                <label for="selector-tipo" class="form-label">Tipo de dato</label>
                                <select class="form-select" name="selector-tipo" id="selector-tipo" required onchange="add_select()">
                                    <option selected value="0">Selecciona una opccion</option>
                                    <?php foreach ($selecs_tipos as $opciones) : ?>
                                        <option value="<?php echo $opciones->id ?>"><?php echo $opciones->nombre ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>

                            <br>
                            <div>

                                <label for="inputs-relacionados" class="form-label">Depende de Otro Campo</label>
                                <select class="form-select" name="inputs-relacionados" id="inputs-relacionados">
                                    <option selected value="0">Selecciona una opccion</option>
                                    <?php if (isset($items_activos)) : ?>
                                        <?php foreach ($items_activos as $items) : ?>
                                            <?php //echo $items->campo 
                                            ?>
                                            <option value="<?php echo $items->campo ?>"><?php echo $items->nombre ?></option>
                                        <?php endforeach;  ?>
                                    <?php endif;  ?>

                                </select>

                            </div>
                            <br>
                            <div>
                                <label for="orden-item" class="form-label">Numero de Secuencia:</label> <br>
                                <input value="<?php echo  $conta_item ?>" class="form-control" type="number" name="orden-item" id="orden-item" required>
                                <small id="helpId" class="form-text text-muted">Ingrese el orden que tendra en el formulario este item </small>
                            </div>
                            <br>
                            <div class="form-check form-switch text-center">
                                <input class="form-check-input" type="checkbox" id="campo-obligatorio" name="campo-obligatorio" value="true">
                                <label class="form-check-label" for="campo-obligatorio">¿Este campo es obligatorio?</label>
                            </div>
                            <br>
                            <div class="form-check form-switch text-center">
                                <input class="form-check-input" type="checkbox" id="campo-visible" name="campo-visible" value="true">
                                <label class="form-check-label" for="campo-visible">¿Este campo será visible en el lateral de la revision documental?</label>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="card-footer text-muted text-center">
            <button type="submit" class="btn btn-secondary">Agregar Item</button>
            <button class="btn btn-secondary" type="button" id="cancelar">Regresar al menu</button>
        </div>
    </div>
    </form>
</div>
<form action="DiseñadordeModelo" method="post" id="home-fomr">
    <input type="hidden" name="home-back" value="true">
</form>
<script>
    'use strict';
    const btn_guardar = document.querySelector('#cancelar');
    let regresarhome = () => {
        document.getElementById('home-fomr').submit()
    }
    btn_guardar.addEventListener('click', regresarhome);
</script>

<script>
    function confirmaItem() {
        return confirm("¿Está seguro de que desea eliminar el item seleccionado?. \n Por favor confirme que desea continuar");
    }
</script>
<script>

</script>
<?php echo $this->endSection() ?>