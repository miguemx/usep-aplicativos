<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .main {
        padding-top: 20px;
    }

    .busqueda {
        margin: 0 20px;
    }

    .botonera {
        margin: 10px;
    }

    .titulo {
        margin-top: 10px;
    }

    .td2,
    .td3,
    .td4,
    .td5,
    .td6,
    .td7,
    .td8 {
        width: 250px;
    }

    .aula {
        font-size: 12px;

    }
</style>
    <div class="main">
        <form action="HorariosGenerales" method="post">

            <div class="container shadow py-2">
                <div class="row justify-content-center busqueda">
                    <label for="seccion" class="form-label titulo">Seccion a buscar</label>
                    <input type="text" class="form-control" name="seccion" id="seccion" aria-describedby="helpId">
                    <small id="helpId" class="form-text text-muted">Ingrese la seccion a consultar el horario</small>
                </div>
                <BR></BR>
                <div class="row justify-content-center busqueda">
                    <label for="aula-select" class="form-label titulo">Seleccione un aula</label>
                    <select class="form-select" name="aula-select" id="aula-select">
                        <option selected> </option>
                        <?php foreach ($aulas_totales as $aulas) :   ?>
                            <option value=" <?php echo $aulas->aula_id; ?>"><?php echo $aulas->nombre_aula; ?></option>
                        <?php endforeach;   ?>
                    </select>
                    <small id="helpId" class="form-text text-muted">Ingrese el salon del que se consultara el horario</small>
                </div>
                <div class="text-center botonera">
                    <button class="btn btn-secondary" type="submit">Buscar Registros</button>
                </div>
            </div>
        </form>
    </div>


<?php echo $this->endSection() ?>