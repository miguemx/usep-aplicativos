<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>


<br>
<div class="container contenedor-cuestionario" style="background-color:#dcdcdc ;">
	<div class="card card-qr">
		<div class="card-header d-flex justify-content-center" style="background-color:#841138 ; color:antiquewhite;">
			Validación de Código de Acceso para Filtro Sanitario
		</div>
		<br>
		<div class="card-body" style="text-align: center;">
            <?php if ( is_null($error) ): ?>
                <img src="<?php echo base_url('img/icons/correct.png') ?>" alt="OK" height="100" /><br />
                Acceso registrado. Por favor cierre esta pestaña.
            <?php else: ?>
                No se pudo actualizar el registro. El error ha sido reportado.
            <?php endif; ?>
		</div>
	</div>
</div>

<?php echo $this->endSection() ?>