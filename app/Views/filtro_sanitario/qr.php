<?php echo $this->extend('plantillabase'); ?>
<?php echo $this->section('content') ?>

<STyle>
  body {
    padding-top: 4.2rem;
    padding-bottom: 4.2rem;
    font-family: 'Open Sans', sans-serif;
    background-color: #dcdcdc;
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: center;
    align-items: center;
    background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
    background-size: 794px 1100px;
    background-repeat: no-repeat;
    background-position: left;
   /* filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/
  }

  .QR {
    display: flex;
    justify-content: center;

  }

  .card-qr {
    max-width: 40rem;
    border-radius: 50px;
    background-color: whitesmoke;
  }

  .contenedor-qr {
    border-radius: 50px;
    max-width: 40rem;
    background-color: #dcdcdc;
  }

  .card-header {
    background-color: #841138;
    color: antiquewhite;

  }
</STyle>
<div class="container contenedor-qr">
  <div class="card card-qr">
    <div class="card-header" style="border-top-left-radius: 50px; border-top-right-radius: 50px;">
      Código de Acceso
    </div>
    <br />
    <?php if(!is_null($qr)): ?>
      <div class="d-flex flex-column justify-content-center align-items-center">
        <br>
        <p>
          Presente este código QR en el Filtro Sanitario de acceso.
        </p>
      </div>
      <div class="card-body QR">
        <form id="codigoQr" method="POST">
          <div class="d-flex flex-column align-items-center">
            <img class="img-fluid" src="<?php echo $qr ?>" alt="qr de">
            <br>
            <p style="text-align: center;"><?php echo $texto ?></p>
            <?php if( $expirado ): ?>
              <div style="color: #f00;">
                Tu código ha expirado. Te invitamos a obtener uno nuevo 
                <a href="<?php echo base_url('FiltroSanitario'); ?>">aquí</a>
              </div>
            <?php endif; ?>
          </div>
        </form>
      </div>
    <?php else: ?>
      <div class="d-flex flex-column justify-content-center align-items-center">
        <br>
        <p>
          El código solicitado no fue encontrado.<br />
          Puedes obtener tu código QR
          <a href="<?php echo base_url('FiltroSanitario'); ?>">aquí</a>
        </p>
      </div>
    <?php endif; ?>

  </div>
</div>

<script>
  window.history.pushState(null, "", window.location.href);
  window.onpopstate = function () {
      window.history.pushState(null, "", window.location.href);
  };
</script>
<?php echo $this->endSection() ?>