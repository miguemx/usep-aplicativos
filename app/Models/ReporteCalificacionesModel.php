<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\ReporteCalificaciones;

class ReporteCalificacionesModel extends Model
{
    protected $table = "view_reporte_calificaciones";
    protected $returnType = 'App\Entities\ReporteCalificaciones';
    protected $allowedFields = [
        'gpoalumno_alumno', 'alumno_ap_paterno', 'alumno_ap_materno', 'alumno_nombres', 'calificacion', 'cal_letra', 'evaluacion', 'grupo_clave',
        'grupo_idc', 'grupo_periodo', 'materia_clave', 'materia_nombre', 'materia_semestre', 'carrera_nombre', 'empleado_nombre', 'empleado_id'
    ];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros = array())
    {
         $this->creaFiltros($filtros);
        // $alumnos = $this->getCompiledSelect();
        $alumnos = $this->find();
        return $alumnos;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['periodo'])) {
                $this->where('grupo_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('seccion', $filtros)) { // -- filtrar por apPaterno
            if (strlen($filtros['seccion'])) {
                $this->where('grupo_clave', $filtros['seccion']);
            }
        }
        if (array_key_exists('carrera', $filtros)) { // -- filtrar por materno
            if (strlen($filtros['carrera'])) {
                $this->where('carrera_nombre', $filtros['carrera']);
            }
        }
        $this->orderBy('grupo_periodo', 'ASC');
        $this->orderBy('materia_clave', 'ASC');
        $this->orderBy('grupo_clave', 'ASC');
        $this->orderBy('alumno_ap_paterno', 'ASC');
        $this->orderBy('alumno_ap_materno', 'ASC');
        $this->orderBy('alumno_nombres', 'ASC');
    }
}
