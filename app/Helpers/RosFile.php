<?php
namespace App\Helpers;

use App\Models\RosModel;
use App\Entities\Ros;
use App\Libraries\PagosEstudiantes;

class RosFile {

    public $totalArchivos = 0;
    private $file;

    private $rosProcesar;
    private $rosProcesados;
    private $fileList = [];

    private $rosModel;

    private $primeraLinea = 3;

    private $error = null;
    private $errors = [];

    /**
     * resive una ruta física de un archivo
     */
    public function __construct() {
        $this->rosProcesar = WRITEPATH.'/pagos/procesar/';
        $this->rosProcesados = WRITEPATH.'/pagos/completados/';
        $this->rosModel = new RosModel();
    }

    /**
     * abre el archivo de la instancia para procesarlo
     */
    public function procesa() {
        foreach ( $this->fileList as $filename ) {
            $path = $this->rosProcesar.$filename;
            try {
                $this->file = fopen( $path, 'r' );
                if ( $this->file ) {
                    $this->guarda();
                    fclose( $this->file );
                }
            }
            catch ( \Exception $ex ) {
                log_message( 'error', 'ERROR: {ex}', [ 'ex'=>$ex ] );
                $this->reportaError( "Ocurrió un error al procesar el archivo. ".$ex->getMessage() );
            }
        }
    }

    /**
     * obtiene la lista de los archivos para procesar subdos al server
     * y la coloca en la propiedad del objeto
     */
    public function getLista()
    {
        $this->fileList = [];
        $filesProcesar = scandir( $this->rosProcesar );
        foreach ($filesProcesar as $fileProcesar) {
            if (!is_dir($fileProcesar)) $this->fileList[] = $fileProcesar;
        }
        $this->totalArchivos = count( $this->fileList );
    }


    /**
     * lee el contenido del archivo para guardar los registros
     */
    public function guarda() {
        $i = 1;
        while( $linea = fgets( $this->file ) ) {
            if ( $i>=$this->primeraLinea ) {
                $ros = $this->getRosEntity( $linea, $i );
                if ( !is_null($ros) ) {
                    $refs = $this->rosModel->findByReferencia( $ros->referencia );
                    if ( count($refs) == 0 ) {
                        try {
                            $this->rosModel->insert( $ros );
                            $ros->id = $this->rosModel->lastId();
                            log_message( 'notice', "ROS: Registro guardado ref: {ref}, con ID: {id}", [ 'ref'=>$ros->referencia, 'id'=>$ros->id ] );
                        }
                        catch ( \Exception $ex ) {
                            $this->reportaerror( "Línea $i : No se puede insertar el registro: ".$ex->getMessage() );
                            log_message( 'error', "ERROR: {ex}", [ 'ex'=>$ex ] );
                        }
                    }
                }
            }
            $i++;
        }
    }

    /**
     * realiza el cotejamiento de una referencia reportada por el ROS
     * @param ref la cadena con la referencia
     */
    public function coteja() {

    }

    /**
     * obtiene un arreglo con los datos a guardar derivado de una linea del archivo ros;
     * la línea debe corresponder al formato separado por pipes
     * @param linea la linea del archivo ROS
     * @param numLinea el numero de linea (opcional)
     * @return ros una entidad App\Entities\Ros con los datos obtenidos
     * @return null si no se puede crear la entidad
     */
    public function getRosEntity( $linea, $numLinea=1 ) {
        $datos = explode( "|", $linea );
        $errores = 0;
        $datos[5] = str_replace( ",", "", $datos[5] );
        if ( !preg_match( "/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $datos[0] ) || !preg_match( "/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $datos[9] ) ) {
            $this->reportaError( "Errores en las fechas. Línea: ".$numLinea );
            $errores++;
        } 
        if ( !preg_match( "/^[0-9]{20}$/", $datos[7] ) ) {
            $errores++;
            $this->reportaError( "Error en la referencia. Línea: ".$numLinea );
        }
        if ( !preg_match( "/^[0-9]+(\.[0-9]{2})?$/", $datos[5] ) ) {
            $errores++;
            $this->reportaError( "Error en el monto. Línea: ".$numLinea );
        }
        if ( $errores === 0 ) {
            $fechaPago = \App\Helpers\Fechas::ddmmaaaa2Mysql( $datos[0] );
            $fechaOtorgado = \App\Helpers\Fechas::ddmmaaaa2Mysql( $datos[9] );
            $info = [
                'fechaPago' => $fechaPago, 'nombre' => $datos[1], 'concepto' => $datos[2], 'cuenta' => $datos[3], 'descCuenta' => $datos[4],
                'importe' => $datos[5], 'folioInteligente' => $datos[6], 'referencia' => $datos[7], 'estatusServicio' => $datos[8], 'fechaOtorgado' => $fechaOtorgado,
                'usuario' => $datos[10], 'unidadResponsable' => $datos[11], 'beneficiario' => $datos[12], 'cotejado' => '0', 
            ];
            $ros = new Ros( $info );
            return $ros;
        }
        return null;
    }

    /**
     * devuelve el ultimo error generado
     */
    public function getError() {
        return $this->error;
    }

    /**
     * devuelve un arreglo con las cadenas de todos los errores reportados
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * reporta un error a la instancia del objeto y lo agrega a la pila
     * @param error el error a reportar
     */
    private function reportaError( $error ) {
        $this->error = $error;
        $this->errors[] = $error;
    }

}