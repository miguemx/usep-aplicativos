<?php
namespace App\Models;
use CodeIgniter\Model;

class VotacionCandidatosModel extends Model {
    protected $table      = 'votacion_candidatos';
    protected $primaryKey = 'candidatos_id';

    protected $returnType    = 'App\Entities\VotacionCandidatos';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'candidatos_id','candidatos_correo','candidatos_nombre','candidatos_voto','candidatos_ganador','candidatos_proceso'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function buscacandidato($idcandidato,$proceso){
        $consulta = $this->where('candidatos_id', $idcandidato)->where('candidatos_proceso', $proceso)->findAll();
        return $consulta;
    }
    public function updatecandidato($idcandidato,$proceso,$voto){
        return $this->db
        ->table('votacion_candidatos')
        ->where(["candidatos_id" => $idcandidato, 'candidatos_proceso' => $proceso])
        ->set($voto)
        ->update();
    }
    public function consultacandidatos($proceso){
        $this->where('candidatos_proceso', $proceso);
        $this->orderBy( 'candidatos_voto', 'DESC' );
        $consulta = $this->findAll();
        return $consulta;
    }
    public function ganador($proceso){
        $this->where('candidatos_proceso', $proceso);
        $this->orderBy( 'candidatos_voto', 'ASC' );
        $consulta = $this->findAll();
        return $consulta;
    }
}