<?php

namespace App\Controllers;

use App\Entities\Evaluacion;
use App\Entities\EvaluacionesFormulario;
use App\Entities\EvaluacionesPreguntas;
use App\Libraries\Estadisticas;
use App\Models\EvaluacionesFormulariosModel;
use App\Models\EvaluacionesPreguntasModel;
use App\Models\EvaluacionesTipoPreguntasModel;
use App\Models\EvaluacionModel;
use App\Models\PeriodoModel;
use App\Models\PreguntaClasificacionModel;
use App\Models\RolesModel;
use App\Models\TipoEvaluacionesModel;
use App\Models\EstadisticaEvaluacionDocenteModel;
use App\Controllers\EstadisticasEvaluacionDocente;
use Google\Service\Appengine\Library;
use phpDocumentor\Reflection\Types\This;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Coordinate;

class EvaluacionDocenteController extends BaseController
{
    private $tipoEvaluacionesModel;
    private $evaluacionModel;
    private $periodosModel;
    private $fromularioModel;
    private $preguntasModel;
    private $tipoPreguntasModel;
    private $rolModel;
    private $clasificacionPreguntaModel;
    private $estadisticaEvaluacionDocente;
    private $EstadisticasEvaluacionDocenteController;

    public function __construct()
    {
        $this->evaluacionModel = new EvaluacionModel();
        $this->periodosModel = new PeriodoModel();
        $this->fromularioModel = new EvaluacionesFormulariosModel();
        $this->preguntasModel = new EvaluacionesPreguntasModel();
        $this->tipoPreguntasModel = new EvaluacionesTipoPreguntasModel();
        $this->tipoEvaluacionesModel = new TipoEvaluacionesModel();
        $this->rolModel = new RolesModel();
        $this->clasificacionPreguntaModel = new PreguntaClasificacionModel();
        $this->estadisticaEvaluacionDocente = new EstadisticaEvaluacionDocenteModel();
        $this->EstadisticasEvaluacionDocenteController = new EstadisticasEvaluacionDocente();
    }
    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA LA CREACION Y MODIFICACION DE FORMULARIOS PARA LA EVALUACION DOCENTE                       |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Despliega el menú principal para la creacion de evaluaciones docentes
     */
    public function Menu()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['eliminacion'] = ($this->sesion->getFlashdata('mensaje')) ? $this->sesion->getFlashdata('mensaje') : false;
        $data['tipos_evaluacion'] = $this->tipoEvaluacionesModel->findAll();
        $evaluaciones = $this->evaluacionModel->buscar(['tipo' => 1]);
        $data['evaluaciones'] = (($evaluaciones) ? $evaluaciones : false);
        $data['roles'] = $this->rolModel->getRolesEvaluaciones();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/menu', $data);
    }

    /**
     * Despliega la vista para la configuracion del ciclo de evaluacion
     */
    public function GenerarEvaluacion()
    {
        $data['creacion'] = true;
        $data['periodos'] = $this->periodosModel->getPeriodoSinFinalizar();
        $data['roles'] = $this->rolModel->getRolesEvaluaciones();
        $data['tipos_evaluacion'] = $this->tipoEvaluacionesModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configuracion', $data);
    }
    /**
     * Despliega la vista para la edicion de una evaluacion
     * @param evaluacion ID de evaluacion
     */
    public function ModificarEvaluacion($evaluacion)
    {
        $data['creacion'] = false;
        $data['periodos'] = $this->periodosModel->getPeriodoSinFinalizar();
        $data['roles'] = $this->rolModel->getRolesEvaluaciones();
        $data['tipos_evaluacion'] = $this->tipoEvaluacionesModel->findAll();
        $data['evaluacion'] = $this->evaluacionModel->find($evaluacion);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configuracion', $data);
    }

    /**
     *  crea la Evaluacion
     * @param titulo nombre que tendra la evaluacion
     * @param periodo ID del periodo disponible
     * @param tipo_evaluacion 
     * @param bandera donde retornara en caso que exita un error
     */
    public function CrearEvaluacion($titulo_evaluacion = false, $periodo = false, $tipo_evaluacion = false, $rol = false, $bandera = false)
    {

        $titulo_evaluacion =  (($titulo_evaluacion) ? $titulo_evaluacion : $this->request->getGet('titulo_evaluacion'));
        $periodo =  (($periodo) ? $periodo : $this->request->getGet('select_periodo'));
        $tipo_evaluacion =  (($tipo_evaluacion) ? $tipo_evaluacion : $this->request->getGet('select_tipo_evaluacion'));
        $bandera =  (($bandera) ? $bandera : $this->request->getGet('bandera'));
        $rol =  (($rol) ? $rol : $this->request->getGet('selct_rol'));
        $evaluacionEnt = new Evaluacion();
        $evaluacionEnt->periodo = $periodo;
        $evaluacionEnt->estatus = 'CERRADO';
        $evaluacionEnt->titulo = $titulo_evaluacion;
        $evaluacionEnt->tipo = $tipo_evaluacion;
        $evaluacionEnt->rol = $rol;
        $this->evaluacionModel->insert($evaluacionEnt);
        try {
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Se ha generado correctamente la evaluacion ' . $titulo_evaluacion . '.'
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/EvaluacionDocenteController/Menu');
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para generar la evaluacion.'
            ];
        }


        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/' . $bandera);
    }
    /**
     *  crea el formulario para la evalucion
     * @param evaluacion nombre que tendra el formulario
     * 
     */
    public function EditarEvaluacion($evaluacion)
    {

        $evaluacion_actual = $this->evaluacionModel->find($evaluacion);
        $evaluacion_comparativa = $this->evaluacionModel->find($evaluacion);
        $titulo_evaluacion =   $this->request->getGet('titulo_evaluacion_edicion');
        $periodo =  $this->request->getGet('select_periodo_edicion');
        $tipo_evaluacion = $this->request->getGet('select_tipo_evaluacion_edicion');
        $rol = $this->request->getGet('selct_rol_edicion');
        try {
            $evaluacion_actual->periodo = ($periodo);
            $evaluacion_actual->estatus = 'CERRADO';
            $evaluacion_actual->titulo = ($titulo_evaluacion);
            $evaluacion_actual->tipo = ($tipo_evaluacion);
            $evaluacion_actual->rol = ($rol);
            if ($evaluacion_actual != $evaluacion_comparativa) {
                $this->evaluacionModel->update($evaluacion, $evaluacion_actual);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha modificado correctamente la evaluacion ' . $titulo_evaluacion . '.'
                ];
            } else {
                $mensaje = [
                    'tipo' => 'alert-info',
                    'texto' => 'No ha habido ningun cambio por realizar ' . $titulo_evaluacion . '.'
                ];
            }
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/EvaluacionDocenteController/Menu');
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para editar la evaluacion ' . $titulo_evaluacion . '.'
            ];
        }

        // echo json_encode($evaluacion_actual)."<br>".json_encode($evaluacion_comparativa);
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/Menu');
    }

    /**
     * Recorre por todos los formularios y preguntas para eliminar la rama completa de los contenidos de dicha evaluacion
     *  
     */
    public function EliminarEvaluacion()
    {
        $evaluacion = $this->evaluacionModel->find($this->request->getPost());
        $contador_preguntas = 0;
        $contador_eliminadas = 0;
        $mensajes_denied = [];
        $mensajes_success = [];
        if (!$evaluacion) {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No se ha encontrado la evaluacion seleccionada para eliminar.'
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/EvaluacionDocenteController/Menu');
        } else {
            $formularios = $this->fromularioModel->getByEvaluacion($evaluacion[0]->id);
            if ($formularios) {
                foreach ($formularios as $formulario) {
                    $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
                    if ($preguntas) {
                        $contador_preguntas = count($preguntas);
                        $contador_eliminadas = 0;
                        foreach ($preguntas as $pregunta) {
                            if ($this->popPregunta($pregunta->id)) {
                                $contador_eliminadas++;
                                $log[] = 'Se ha eliminado la pregunta ' . $pregunta->id . ' del formulario [' . $formulario->id . ']';
                            }
                        }
                        if ($this->popFormulario($formulario->id)) {
                            $log[] = 'Se ha eliminado el formulario [' . $formulario->id . ']' . $formulario->nombre;
                            $mensajes_success[] = ['Se ha eliminado el formulario "' . $formulario->nombre . '" y ' . $contador_preguntas . '/' . $contador_eliminadas . ' preguntas que este contiene.'];
                        } else {
                            $mensajes_denied[] = ['No se ha podido eliminar el formulario "' . $formulario->nombre . '".'];
                        }
                    } else {
                        if ($this->popFormulario($formulario->id)) {
                            $log[] = 'Se ha eliminado el formulario [' . $formulario->id . ']' . $formulario->nombre;
                            $mensajes_success[] = ['Se ha eliminado el formulario "' . $formulario->nombre . '".'];
                        } else {
                            $mensajes_denied[] = ['No se ha podido eliminar el formulario "' . $formulario->nombre . '".'];
                        }
                    }
                }
            }
            if ($this->popEvaluacion($evaluacion[0]->id)) {
                $log[] = 'Se ha eliminado la evaluación [' . $evaluacion[0]->id . ']' . $evaluacion[0]->titulo;
                array_unshift($mensajes_success, ['Se ha eliminado la evaluación "' . $evaluacion[0]->titulo . '".']);
            } else {
                array_unshift($mensajes_denied, ['No se ha podido eliminar la evaluación "' . $evaluacion[0]->titulo . '".']);
            }
            // echo json_encode($mensajes_success)."<br>---------------------------<br>".json_encode($mensajes_denied);
            $this->sesion->setFlashdata('mensaje', [$mensajes_success, $mensajes_denied]);
            return redirect()->to('/EvaluacionDocenteController/Menu');
        }
        // log_message('info', 'Se ha realizado la modificacion del formulario' . $formulario_model->id . ' por el usuario ' . $this->sesion->get('id'));
    }

    /**
     * @param evaluacion ID de evaluacion 
     * @return true si logra eliminar la evaluacion 
     * @return false si no pudo eliminar
     */
    private function popEvaluacion($evaluacion)
    {
        try {
            $this->evaluacionModel->delete($evaluacion);
            return true;
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            return false;
        }
    }
    /**
     * @param formulario ID de formulario 
     * @return true si logra eliminar la formulario 
     * @return false si no pudo eliminar
     */
    private function popFormulario($formulario)
    {
        try {
            $this->fromularioModel->delete($formulario);
            return true;
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            return false;
        }
    }
    /**
     * @param pregunta ID de pregunta 
     * @return true si logra eliminar la pregunta 
     * @return false si no pudo eliminar
     */
    private function popPregunta($pregunta)
    {
        try {
            $this->preguntasModel->delete($pregunta);
            return true;
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            return false;
        }
    }

    /**
     * preceso para activar evaluacion
     */
    public function CambiarEstatus()
    {
        // echo json_encode($this->request->getPost());
        $evaluacion = ($this->request->getPost('evaluacion'));
        $estatus  = ($this->request->getPost('estado_actual'));
        $bandera_documento  = ($this->request->getPost('bandera_documento'));
        $bandera_status  = ($this->request->getPost('bandera_status'));
        $archivo = '';
        $archivo2 ='';
        $alert = 'alert-info';
        $texto = '';
        echo json_encode($this->request->getPost());
        // die();
        if ($bandera_documento == 'true') {
            if ($this->GenerartxtEvaluacion($evaluacion)) {
                $archivo = 'Se ha generado el archivo de datos de la evaluacion seleccionada';
            } else {
                $archivo = 'Ocurrio un error inesperado al momento de generar el archivo de datos de la evaluacion seleccionada';
            }
            if ($this->EstadisticasEvaluacionDocenteController->generartxtEvaluacionMateria($evaluacion)) {
                $archivo2 = 'Se ha generado el archivo de datos de la evaluacion seleccionada (Estadistica docente por materia)';
            } else {
                $archivo2 = 'Ocurrio un error inesperado al momento de generar el archivo de datos de la evaluacion seleccionada (Estadistica docente por materia)';
            }
        }


        if ($bandera_status == 'true') {
            $evaluacionModel = $this->evaluacionModel->find($evaluacion);
            try {
                $evaluacionModel->estatus = (($estatus === 'CERRADO') ? 'ACTIVO' : 'CERRADO');
                $this->evaluacionModel->update($evaluacion, $evaluacionModel);
                log_message('notice', 'Se ha {status} la evaluacion [{id_evaluacion}] {nombre} por el usuario {usuario} ', ['usuario' => $this->sesion->get('id'), 'status' => $evaluacionModel->estatus, 'id_evaluacion' => $evaluacion, 'nombre' => $evaluacionModel->titulo]);
                $alert = 'alert-success';
                $texto .= 'Se ha ' . (($estatus === 'CERRADO') ? 'activado' : 'cerrado') . ' la evaluacion ' . $evaluacionModel->titulo . '.<br>';
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);

                $alert = 'alert-danger';
                $texto .= 'Ha ocurrido un problema para ' . (($estatus === 'CERRADO') ? 'activar' : 'cerrar') . ' la evaluacion ' . $evaluacionModel->titulo . ' .<br>';
            }
        }
        $mensaje = [
            'tipo' => $alert,
            'texto' => $texto. $archivo. $archivo2 
        ];
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/Menu');
    }

    /**
     * Genera un archivo txt con los datos pertenecientes al cierre de la evaluacion docente seleccionada 
     * @param evaluacion
     */
    public function GenerartxtEvaluacion($evaluacion)
    {
        $evaluacion_model  = $this->evaluacionModel->find($evaluacion);
        $ruta = WRITEPATH . 'evaluacion/' . $evaluacion_model->periodo . '/';
        switch ($evaluacion_model->tipo) {
            case '1':
                $estadistica = new Estadisticas();
                $datos = $estadistica->generarEVDOC($evaluacion);
                try {
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $myfile = fopen($ruta . 'EVDOC_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt", "w");
                    fwrite($myfile, json_encode($datos));
                    fclose($myfile);
                    log_message("notice", "Se ha generado un reporte EVDOC de la evaluacion {id}{nombre} por el usuario {usuario}. Archivo de datos txt ({documento}) guardado con exito ", ['id' => $evaluacion, 'nombre' => $evaluacion_model->titulo, 'usuario' => $this->sesion->get('useremail'), 'documento' => 'EVDOC_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt"]);
                    return true;
                } catch (\Exception $ex) {
                    log_message("error", "ERROR al guardar el archivo de datos del documento {documento}.txt: {exception}: ", ['exception' => $ex, 'documento' => 'EVDOC_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt"]);
                    return false;
                }
                break;
        }
    }

    private function algo_e($evaluacion)
    {
        $estadistica = new Estadisticas();
        $datos = $estadistica->generarEVDOC($evaluacion);
        echo json_encode($datos);
    }



    /**
     * procesa los grupos pertenecientes al periodo de la evaluacion seleccionada para 
     * @param evaluacion ID de la evaluacion seleccionada   
     */
    private function BaseEVDOC($evaluacion)
    {
        $data['evaluacion'] = ($evaluacion);
        // $data['indicadores'] = $this->generarEVDOC($evaluacion);
        $ruta = WRITEPATH . 'periodo/2022A/';
        $data['indicadores'] = json_decode(file_get_contents($ruta . 'EVDOC_1_2022_10_19_08_10_45.txt'), true);
        $data['menu'] =  $this->sesion->get('menu');
        return view('evaluacion/evdoc', $data);
    }
    /**
     * despliega la vista de los formularios asiganados a una evaluacion
     * @param evaluacion ID del la evaluacion contenerdora
     */
    public function Formularios($evaluacion)
    {
        $data['creacion'] = true;
        $data['tipos_evaluacion'] = $this->tipoEvaluacionesModel->findAll();
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['evaluacion'] = $this->evaluacionModel->find($evaluacion);
        $data['formularios'] = $this->fromularioModel->getByEvaluacion($evaluacion);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/formularios', $data);
    }

    /**
     * Despliega la vista para configurar y crear un Formulario asignado a una evaluacion
     * @param evaluacion Id del proceso evalucion
     */
    public function AgragarFormulario($evaluacion)
    {
        $data['edicion'] = true;
        $data['tipos'] = $this->tipoEvaluacionesModel->findAll();
        $contador = $this->fromularioModel->getByEvaluacion($evaluacion);
        $data['contador_formularios'] = (($contador) ? $contador : 0);
        $data['evaluacion'] = $this->evaluacionModel->find($evaluacion);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configurarformulario', $data);
    }

    /**
     * Crea un formulario ligado a una evaluacion
     * @param evaluacion ID de evaluacion contenedora   
     */
    public function CrearFormulario($evaluacion)
    {
        $formularioEnt = new EvaluacionesFormulario();
        $evaluacion_contenedora = $this->evaluacionModel->find($evaluacion);
        try {
            $formularioEnt->nombre = $this->request->getGet('titulo_formulario');
            $formularioEnt->evaluacion = $evaluacion;
            $formularioEnt->orden = $this->request->getGet('order_formulario');
            $formularioEnt->instrucciones = (($this->request->getGet('instrucciones') != '') ? $this->request->getGet('instrucciones') : NULL);
            $this->fromularioModel->insert($formularioEnt);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Se ha generado correctamente el formulario ' . $formularioEnt->nombre . ' que pertenece a la evaluacion ' . $evaluacion_contenedora->titulo . "."
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/EvaluacionDocenteController/Formularios/' . $evaluacion);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para generar la evaluacion.'
            ];
        }
    }

    /**
     * Despliega la vista para editar el formulario
     * @param formulario Id del formulario
     */
    public function DetallesFormulario($formulario)
    {
        $data['edicion'] = false;
        $data['tipos'] = $this->tipoEvaluacionesModel->findAll();
        $data['formulario'] = $this->fromularioModel->find($formulario);
        $data['evaluacion'] = $this->evaluacionModel->find($data['formulario']->evaluacion);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configurarformulario', $data);
    }

    /**
     * Modifica un formulario existente
     * @param formulario ID
     */
    public function EditaFormulario($formulario)
    {
        $formulario_model = $this->fromularioModel->find($formulario);
        $formulario_actual = $this->fromularioModel->find($formulario);
        try {
            $formulario_model->nombre = $this->request->getGet('titulo_formulario_update');
            $formulario_model->orden = $this->request->getGet('order_formulario_update');
            $formulario_model->instrucciones = ((str_replace(' ', '', $this->request->getGet('instrucciones_update')) == '') ? NULL : $this->request->getGet('instrucciones_update'));
            if ($formulario_model != $formulario_actual) {
                $this->fromularioModel->update($formulario, $formulario_model);
                log_message('info', 'Se ha realizado la modificacion del formulario' . $formulario_model->id . ' por el usuario ' . $this->sesion->get('id'));
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha modificado el formulario ' . $formulario_model->nombre . '.'
                ];
            } else {
                $mensaje = [
                    'tipo' => 'alert-info',
                    'texto' => 'No ha habido ningun cambio por realizar.'
                ];
            }
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para modificar el formulario' . $formulario_actual->nombre . '.'
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/Formularios/' . $formulario_actual->evaluacion);
    }

    /**
     * Elimina un formulario y sus preguntas contenedoras
     * @param evaluacion ID de evaluacion contenedora
     */
    public function EliminarFormulario($evaluacion)
    {
        $formulario = $this->fromularioModel->find($this->request->getPost('formulario'));
        $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
        $total_preguntas = 0;
        $total_preguntas_eliminadas = 0;
        if ($preguntas) {
            $total_preguntas = count($preguntas);
            foreach ($preguntas as $pregunta) {
                try {
                    $this->preguntasModel->delete($pregunta->id);
                    $total_preguntas_eliminadas++;
                } catch (\Exception $ex) {
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                }
            }
        }
        try {
            $this->fromularioModel->delete($this->request->getPost('formulario'));
            log_message('info', 'Se ha realizado la eliminacion del formulario {formulario} con un total de {eliminadas}/{total} de preguntas que incluia, este proceso fue realizado por el usuario {usuario}', ['formulario' => $formulario->id, 'usuario' => $this->sesion->get('id'), 'eliminadas' => $total_preguntas_eliminadas, 'total' => $total_preguntas]);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Se ha realizado la eliminacion del formulario ' . $formulario->nombre . ' con un total de ' . $total_preguntas_eliminadas . '/' . $total_preguntas . ' de preguntas que incluia.'
            ];
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-alert',
                'texto' => 'Se ha generado un error al momento de eliminar el formulario ' . $formulario->nombre . ' se han eliminado un total de ' . $total_preguntas_eliminadas . '/' . $total_preguntas . '.'
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/Formularios/' . $evaluacion);
    }



    /**
     * Despliega la vista del contenido del formulario seleccionado
     * @param formulario ID formulario
     */
    public function ContenidoFormulario($formulario)
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['formulario'] = $this->fromularioModel->find($formulario);
        $data['preguntas'] = $this->preguntasModel->getByFormulario($formulario);
        $data['tipos'] = $this->tipoPreguntasModel->findAll();
        $data['clasificaciones'] = $this->clasificacionPreguntaModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/preguntas', $data);
    }

    /**
     * despliega la vista para la configuracion y asignacion de una pregunta en un formulario
     * @param formulario ID del formulario al que pertenece
     */
    public function AgregarPregunta($formulario)
    {
        $data['edicion'] = true;
        $data['formulario'] = $this->fromularioModel->find($formulario);
        $data['tipos'] = $this->tipoPreguntasModel->findAll();
        $contador = $this->preguntasModel->getByFormulario($formulario);
        $data['contador_pregunta'] = $contador;
        $data['clasificaciones'] = $this->clasificacionPreguntaModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configurarpregunta', $data);
    }

    /**
     * crear el registro de una pregunta en un formulario
     * @param formulario ID del formulario contenedor
     */
    public function CrearPregunta($formulario)
    {
        echo $formulario . "<br>" . json_encode($this->request->getGet());
        $formulario_contenedor = $this->fromularioModel->find($formulario);
        $preguntaEnt = new EvaluacionesPreguntas();
        try {
            $preguntaEnt->formulario = $formulario;
            $preguntaEnt->orden = $this->request->getGet('order_pregunta');
            $preguntaEnt->titulo = $this->request->getGet('titulo_pregunta');
            $preguntaEnt->tipo = $this->request->getGet('select_tipo');
            $preguntaEnt->clasificacion = $this->request->getGet('select_tipo_clasificacion');
            $this->preguntasModel->insert($preguntaEnt);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Se ha generado correctamente la pregunta ' . $preguntaEnt->titulo  . ' que pertenece al formulario ' . $formulario_contenedor->nombre . "."
            ];
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para generar la pregunta.'
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/ContenidoFormulario/' . $formulario);
    }


    /**
     * Retorna la vista para editar una pregunta 
     * @param pregunta ID de pregunta
     */
    public function DetallesPregunta($pregunta)
    {
        $data['edicion'] = false;
        $data['pregunta'] = $this->preguntasModel->find($pregunta);
        $data['formulario'] = $this->fromularioModel->find($data['pregunta']->formulario);
        $data['tipos'] = $this->tipoPreguntasModel->findAll();
        $data['clasificaciones'] = $this->clasificacionPreguntaModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/configurarpregunta', $data);
    }

    /**
     * Modifica la prgunta seleccionada
     * @param pregunta ID de pregunta
     */
    public function EditarPregunta($pregunta)
    {
        $pregunta_model = $this->preguntasModel->find($pregunta);
        $pregunta_actual = $this->preguntasModel->find($pregunta);
        try {
            $pregunta_model->titulo = ($this->request->getGet('titulo_pregunta_update'));
            $pregunta_model->orden = ($this->request->getGet('order_pregunta_update'));
            $pregunta_model->tipo = ($this->request->getGet('select_tipo_update'));
            $pregunta_model->clasificacion = ($this->request->getGet('select_tipo_clasificacion_update'));
            if ($pregunta_actual != $pregunta_model) {
                $this->preguntasModel->update($pregunta, $pregunta_model);
                log_message('info', 'Se ha realizado la modificacion de la pregunta ' . $pregunta_actual->id . ' por el usuario ' . $this->sesion->get('id'));
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha modificado la pregunta ' . $pregunta_model->titulo . '.'
                ];
            } else {
                $mensaje = [
                    'tipo' => 'alert-info',
                    'texto' => 'No ha habido ningun cambio por realizar.'
                ];
            }
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para editar la pregunta ' . $pregunta_actual->titulo . '.'
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/ContenidoFormulario/' . $pregunta_model->formulario);
    }


    /**
     * Funcion para eliminar la pregunta seleccionada
     * @param formulario ID del formulario contenedor
     */
    public function EliminarPregunta($formulario)
    {
        $pregunta_id = $this->request->getPost('pregunta');
        echo json_encode($pregunta_id);
        try {
            $pregunta = $this->preguntasModel->find($pregunta_id);
            $this->preguntasModel->delete($pregunta_id);
            log_message('info', 'Se ha eliminado la pregunta ' . $pregunta->id . ' ' . $pregunta->titulo . ' por el usuario ' . $this->sesion->get('id'));
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Se ha eliminado la pregunta ' . $pregunta->titulo . '.'
            ];
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'ha ocurrido un problema para eliminar la pregunta ' . $pregunta->titulo . '.'
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/EvaluacionDocenteController/ContenidoFormulario/' . $formulario);
    }

    /**
     * Genera el reporte en excel de las respuestas de evaluacion docente
     * @param evaluacion Id de evaluacion contenedora
     */
    public function reporteRespuestasExcel($evaluacion){
        $respuestas = $this->estadisticaEvaluacionDocente->respuestasExcel($evaluacion);
        $excel = new Spreadsheet();
        $hojaActiva = $excel->getActiveSheet()->setTitle('RespuestasEvaluacion');
        $hojaActiva->setCellValue('A1', 'PERIODO');
        $hojaActiva->setCellValue('B1', 'ID EMPLEADO');
        $hojaActiva->setCellValue('C1', 'NOMBRE');
        $hojaActiva->setCellValue('D1', 'APELLIDO PATERNO');
        $hojaActiva->setCellValue('E1', 'APELLIDO MATERNO');
        $hojaActiva->setCellValue('F1', 'CARRERA');
        $hojaActiva->setCellValue('G1', 'SEMESTRE');
        $hojaActiva->setCellValue('H1', 'CLAVE MATERIA');
        $hojaActiva->setCellValue('I1', 'NOMBRE MATERIA');
        $hojaActiva->setCellValue('J1', 'GRUPO');
        $hojaActiva->setCellValue('K1', 'GRUPO CLAVE');
        $hojaActiva->setCellValue('L1', 'PREGUNTAS');
        $hojaActiva->setCellValue('M1', 'RESPUESTAS');

        $row = 2;
        foreach ($respuestas as $respuesta) {
            $hojaActiva
                ->setCellValue('A' . $row,  $respuesta->grupo_periodo)
                ->setCellValue('B' . $row,  $respuesta->empleado_id)
                ->setCellValue('C' . $row,  $respuesta->empleado_nombre)
                ->setCellValue('D' . $row,  $respuesta->empleado_ap_paterno)
                ->setCellValue('E' . $row,  $respuesta->empleado_ap_materno)
                ->setCellValue('F' . $row,  $respuesta->numero_materia == 1 ? 'LIC. EN ENFERMERÍA Y OBSTETRICIA' : 'LIC. EN MEDICO CIRUJANO')
                ->setCellValue('G' . $row,  $respuesta->materia_semestre)
                ->setCellValue('H' . $row,  $respuesta->materia_clave)
                ->setCellValue('I' . $row,  $respuesta->materia_nombre)
                ->setCellValue('J' . $row,  $respuesta->respuestas_grupo)
                ->setCellValue('K' . $row,  $respuesta->grupo_clave)
                ->setCellValue('L' . $row,  $respuesta->preguntas_titulo)
                ->setCellValue('M' . $row,  $respuesta->respuestas_valor);
            $row++;
        }
        // redirect output to client browser
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporteRespuestas.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($excel, 'Xls');
        $writer->save('php://output');
        exit;
    }
}
