<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div class="bg-white rounded shadow p-3 mt-2">
        <h4 class="mb-2">Asignación de horario</h4>
        <table class="table table-striped table-sm">
            <tr>
                <th>Matrícula:</th>
                <td><?php echo $alumno->id; ?></td>
            </tr>
            <tr>
                <th>Nombre:</th>
                <td><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></td>
            </tr>
            <tr>
                <th>Correo:</th>
                <td><?php echo $alumno->correo; ?></td>
            </tr>
            <tr>
                <th>Contraseña:</th>
                <td><?php echo substr( $alumno->curp, 0, 10).'*'; ?></td>
            </tr>
            <tr>
                <th>Carrera:</th>
                <td><?php echo $carrera->nombre; ?></td>
            </tr>
        </table>
        <div>
        (<em>La cuenta de correo electrónico estará disponible de 48 a 72 horas posterior a la hora de inscripción</em>)
        </div>
        <div class="row">
            <?php foreach ( $grupos as $seccion=>$materias ): ?>
                <form class="mb-3 col-sm-3" method="post" action="<?php echo base_url('Inscripcion/AltaSeccion') ?>" onsubmit="return confirma('<?php echo $seccion ?>');">
                    <div class="">
                        <div class="card">
                            <div class="card-header">
                                <?php echo $seccion; ?>
                            </div>
                            <div class="card-body">
                                <?php 
                                $acaGrupos = [];
                                $disponible = 0;
                                foreach ( $materias as $materia ) {
                                    $disponible = $materia->max - $materia->ocupado;
                                    $acaGrupos[] = $materia->grupo;
                                }
                                ?>
                                Espacios disponibles: <?php echo $disponible; ?>
                                
                            </div>
                            <div class="card-footer">
                                <input type="hidden" name="id" value="<?php echo $alumno->id; ?>" />
                                <input type="hidden" name="grupos" value="<?php echo implode( ',', $acaGrupos ); ?>" />
                                <button type="submit" class="btn btn-secondary btn-sm" <?php if( $disponible < 1 ) echo 'disabled="disabled"'; ?>>
                                    Asignar sección
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<script type="text/javascript"> 
    function confirma(seccion) {
        if ( confirm("¿Está seguro que desea asignar este alumno a la seccion "+seccion+"?\n\nEsta acción no se puede deshacer.") ) {
            return true;
        }
        return false;
    }
</script>


<?php echo $this->endSection() ?>