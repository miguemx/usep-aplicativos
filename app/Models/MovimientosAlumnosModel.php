<?php
namespace App\Models;
use CodeIgniter\Model;

class MovimientosAlumnosModel extends Model {
    protected $table      = 'aca_movimientos_alumnos';
    protected $primaryKey = 'movimiento_id';

    protected $returnType    = 'App\Entities\MovimientosAlumnos';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'movimiento_id','movimiento_matricula','movimiento_autor','movimiento_oficio',
        'movimiento_comentarios','movimiento_tipo','movimiento_fecha','movimiento_idc',
        
    ];


    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;


}