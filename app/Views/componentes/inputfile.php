<?php
if( isset($props) ) {
    if ( $props['status'] == 'OBSERVACIONES' && $props['comentario']==null ) {
        $disabled = true;
    }
}

if ( $disabled == true ) $disabled = 'disabled="disabled"';
?>
<div class="col-md-12 my-3"><!-- inicio del form group -->
    <div >
        <div class="card-header" style="padding: 10px 20px 0px 20px;">
            <?php echo $label; ?><?php if($mandatory)echo '*'; ?>
        </div>
        <div class="card-body" style="padding: 10px 20px 10px 20px;">
            <div class="row">
                <div class="col-sm-12">
                    <?php if($value): ?>
                        <?php if($props['comentario']==null): ?>
                            <img src="<?php echo base_url('img/icons').'/fileok.png'; ?>" alt="Archivo correcto" /><br />
                            Tu archivo se ha cargado correctamente.
                        <?php else: ?>
                            <img src="<?php echo base_url('img/icons').'/file-alert.png'; ?>" alt="Archivo con comentarios" /><br />
                            <?php echo nl2br($props['comentario']); ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="<?php echo base_url('img/icons').'/fileempty.png'; ?>" alt="Archivo correcto" />
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php if(!$disabled): ?>
                    <label class="form-label">Seleccione para <strong><?php if($value) echo 'reemplazar'; else echo 'agregar'; ?></strong></label>
                    <?php endif; ?>
                    <div class="input-group input-group-outline input-group-sm" <?php if($description) echo 'style="margin-bottom: 0px !important;"'; ?>>    
                        <input type="file" 
                                    class="form-control <?php if($value) echo 'file-value'; ?>" 
                                    name="<?php echo $name; ?>" 
                                    id="<?php echo $name; ?>"
                                    <?php echo $disabled; ?>
                                />
                    </div>
                </div>
            </div>
            
            
            <!-- descripcion del campo -->
            <?php if($description): ?>
                <div style="margin-bottom: 0.5rem !important; font-size: 13px;"><?php echo $description; ?></div>
            <?php endif; ?>
        </div>
    </div>
</div><!-- fin del form group -->