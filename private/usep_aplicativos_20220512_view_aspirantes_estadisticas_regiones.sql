create view view_aspirantes_estadistica_regiones as(
    SELECT DISTINCT
        `a`.`region_nombre` AS `region_nombre`,
        SUM(
            IF(
                `a`.`aspirante_sexo` = 'M' AND `a`.`aspirante_carrera` = '1',
                `a`.`n_alumnos`,
                0
            )
        ) AS `n_alumnos_m_enf`,
        SUM(
            IF(
                `a`.`aspirante_sexo` = 'H' AND `a`.`aspirante_carrera` = '1',
                `a`.`n_alumnos`,
                0
            )
        ) AS `n_alumnos_h_enf`,
        SUM(
            IF(
                `a`.`aspirante_sexo` = 'M' AND `a`.`aspirante_carrera` = '2',
                `a`.`n_alumnos`,
                0
            )
        ) AS `n_alumnos_m_med`,
        SUM(
            IF(
                `a`.`aspirante_sexo` = 'H' AND `a`.`aspirante_carrera` = '2',
                `a`.`n_alumnos`,
                0
            )
        ) AS `n_alumnos_h_med`
    FROM
        `usep_aplicativos`.`view_estadistica_regiones` `a`
    GROUP BY
        `a`.`region_nombre`
    ORDER BY
        `a`.`region_nombre`
)