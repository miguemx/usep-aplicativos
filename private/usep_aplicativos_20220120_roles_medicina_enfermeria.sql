INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'DIRECTOR MEDICINA', 'default');
INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'DIRECTOR ENFERMERIA', 'default');



UPDATE aplicativo_usuarios SET usuario_rol = 9 WHERE usuario_correo = 'hector.lopez@usep.mx';

UPDATE aplicativo_usuarios SET usuario_rol = 10 WHERE usuario_correo = 'maribel.perez@usep.mx';

INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'renderfiles','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'docente','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'escolar','listaescolar');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'escolar','consultatablalista');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (9,'escolar','generalistapdf');



INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'renderfiles','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'docente','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'escolar','listaescolar');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'escolar','consultatablalista');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (10,'escolar','generalistapdf');

INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'renderfiles','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'docente','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'escolar','listaescolar');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'escolar','consultatablalista');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (11,'escolar','generalistapdf');

INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'renderfiles','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'docente','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'escolar','listaescolar');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'escolar','consultatablalista');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (12,'escolar','generalistapdf');