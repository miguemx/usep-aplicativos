<?php
namespace App\Controllers;

use App\Models\AlumnosSegurosModel;
use App\Models\DocumentosAspiranteModel;

class RenderFiles extends BaseController {

    public function seguro($matricula) {
        $segurosModel = new AlumnosSegurosModel();
        $seguro = $segurosModel->find( $matricula );
        if ( !is_null($seguro) ) {
            if ( file_exists($seguro->archivo) ) {
                if ( $file = fopen( $seguro->archivo, 'r' ) ) {
                    
                        $outputname = $this->sesion->get('id').'-'.time();
                        $partes_ruta = pathinfo($seguro->archivo);
                        // header('Content-Description: File Transfer');
                        header('Content-Type: application/pdf');
                        // header('Content-Disposition: attachment; filename="'.$outputname.'.'.$partes_ruta['extension'].'"');
                        // header('Expires: 0');
                        // header('Cache-Control: must-revalidate');
                        // header('Pragma: public');
                        // header('Content-Length: ' . filesize($seguro->archivo));
                        readfile($seguro->archivo);
                        exit;
                }
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    public function aspirantes( $id ) {
        $docAspiranteModel = new DocumentosAspiranteModel();
        $aspirante = $docAspiranteModel->find( $id );
        if ( !is_null($aspirante) ) {
            if ( file_exists($aspirante->ruta) ) {
                if ( $file = fopen( $aspirante->ruta, 'r' ) ) {                    
                    $partes_ruta = pathinfo($aspirante->ruta); 
                    $partes_ruta['extension'] = strtolower( $partes_ruta['extension'] );
                    if ( $partes_ruta['extension'] == 'pdf' ) header('Content-Type: application/pdf');
                    if ( $partes_ruta['extension'] == 'jpg' ) header('Content-Type: image/jpg');
                    if ( $partes_ruta['extension'] == 'jpeg' ) header('Content-Type: image/jpg');
                    // else 
                    readfile($aspirante->ruta);
                    exit;
                }
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

}