<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>

<style>
body {
  background-image:url('<?php echo base_url('img/patio.jpg') ?>');
  background-position:center;
  background-size:cover;
  
  -webkit-font-smoothing: antialiased;
  font: normal 14px Roboto,arial,sans-serif;

  /*
  filter: grayscale(100%) !important; 
  */
}

h3 {
    color: #fff;
}
</style>
<div class="container-fluid">
   <div>
      &nbsp;
   </div>
   
   <?php if(isset($error)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            El estudiante no está registrado o vigente en la institución
        </div>
   <?php else: ?>
      <div class="container">
        <h3>Universidad de la Salud del Estado de Puebla.</h3>
        <div class="col-sm-12 alert alert-success" role="alert">
            El estudiante ha sido encontrado en los registros de la institución. Por favor asegúrese que la credencial mostrada coincide con la que se despliega abajo.
        </div>
        <?php echo $this->include('estudiantes/disenocredencial'); ?>
      </div>
      
      
   <?php endif; ?>

   
</div>


<?php echo $this->endSection() ?>