create view `view_reporte_calificaciones` as (
select
	gpoalumno_alumno,
	alumno_ap_paterno,
	alumno_ap_materno,
	alumno_nombres,
	if (gpoalumno_extraordinario is NULL,gpoalumno_ordinario,gpoalumno_extraordinario) as calificacion,
	CASE gpoalumno_ordinario
		WHEN '10'
		THEN 'DIEZ'
		WHEN '9'
		THEN 'NUEVE'
		WHEN '8'
		THEN 'OCHO'
		WHEN '7'
		THEN 'SIETE'
		WHEN '6'
		THEN 'SEIS'
		WHEN '5'
		THEN CASE gpoalumno_extraordinario
			WHEN '10'
			THEN 'DIEZ'
			WHEN '9'
			THEN 'NUEVE'
			WHEN '8'
			THEN 'OCHO'
			WHEN '7'
			THEN 'SIETE'
			WHEN '6'
			THEN 'SEIS'
			WHEN '5'
			THEN 'CINCO'
			WHEN NULL
			THEN 'SIN CALIFICACIÓN'
		WHEN NULL
		THEN 'SIN CALIFICACIÓN'
		END
	END AS cal_letra,
	if (gpoalumno_extraordinario is NULL,"ORDINARIO","EXTRAORDINARIO") as evaluacion,
	grupo_clave,
	grupo_idc,
	grupo_periodo,
	materia_clave,
	materia_nombre,
	materia_semestre,
	carrera_nombre,
	empleado_nombre,
	empleado_id
from
	view_calificaciones_edit
where
	grupo_periodo ="2022B" and
	deleted_at is null
order by 
	carrera_nombre,
	grupo_clave,
	grupo_idc,
	alumno_ap_paterno,
	alumno_ap_materno,
	alumno_nombres
)