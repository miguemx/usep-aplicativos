<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .main-content {
        display: flex;
        flex-direction: row;
    }

    .sidebar-menu {
        height: 100hv;
        /* background-color: red; */
    }

    .sidebar-menu li {
        text-decoration: none;
        list-style: none;
    }

    a {
        text-decoration: none;
        color: black;
    }

    a:hover {
        text-decoration: none;
        color: #840f31;
    }
</style>
<div class=" container-fluid main-content mt-2">
    <div class="sidebar-menu me-2 w-25 bd-sidebar">
        <div class="card rounded-2">
            <a class="list-group-item list-group-item-action btn-home " onclick="datahome()">
                <h4>Datos Generales</h4>
            </a>

            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Etapa Actual</div>
                    <ul>
                        <?php echo strtoupper($datos[0]->etapa_nombre) ?>
                    </ul>
                </div>
            </li>


            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Etapas </div>
                </div>
            </li>
            <div class="accordion" id="accordionExample">
                <?php $etapa_anterior = '';
                $contador = 1; ?>
                <?php foreach ($etapas_formularios as $listado) : ?>
                    <?php if ($etapa_anterior != $listado->etapa_id) : ?>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="<?php echo "heading" . $contador ?>">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="<?php echo "#collapse" . $contador ?>" aria-expanded="false" aria-controls="<?php echo "collapse" . $contador ?>">
                                    <?php echo strtoupper($listado->etapa_nombre) ?>
                                    <?php $titulo = $listado->etapa_nombre ?>
                                </button>
                            </h2>
                        </div>
                        <div id="<?php echo "collapse" . $contador ?>" class="accordion-collapse collapse" aria-labelledby="<?php echo "heading" . $contador ?>" data-bs-parent="#accordionExample">
                            <div class="list-group" id="myList" role="tablist">
                                <?php foreach ($etapas_formularios as $items) : ?>
                                    <?php if ($items->etapa_nombre == $titulo) : ?>
                                        <a class="list-group-item list-group-item-action item_formulario" data-bs-toggle="list" href="<?php echo "#data" . $items->formulario_id ?>" role="tab"><?php echo $items->formulario_nombre ?></a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php $etapa_anterior = $listado->etapa_id;
                    $contador++; ?>
                <?php endforeach; ?>

            </div>
        </div>
        <div class="card rounded-2 mt-1">
            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Archivos </div>
                    <?php $etapa_anterior = "" ?>
                    <?php foreach ($respuestas as $archivos) : ?>

                        <?php if ($etapa_anterior != $archivos->nombreEtapa) : ?>
                            <div>
                                <?php echo strtoupper($archivos->nombreEtapa) ?>
                            </div>
                            <?php $etapa_anterior = $archivos->nombreEtapa ?>
                        <?php endif; ?>
                        <?php ?>
                        <ul>
                            <a href="<?php echo base_url() . "ControlAdmision/revisionDocumental/" . $archivos->aspirante . "/" . $archivos->valor;   ?>" target="_blank" rel="noopener noreferrer"><?php echo $archivos->campo_nombre ?></a>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </li>
        </div>
    </div>
    <div class="datos-menu  w-75">
        <div class="card rounded-2">
            <div class="card-body">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="dataHome" role="tabpanel">
                            <div class="col-md-12">
                                <h4>Datos del Aspirante</h4>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <label for="validationServer01" class="form-label">Nombre Completo</label>
                                    <input type="text" class="form-control" id="validationServer01" value="<?php echo $datos[0]->aspirante_ap_paterno . " " . $datos[0]->aspirante_ap_materno . " " . $datos[0]->aspirante_nombre; ?>" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label for="validationServerUsername" class="form-label">Correo</label>
                                    <div class="input-group has-validation">
                                        <input type="text" class="form-control " id="validationServerUsername" value="<?php echo $datos[0]->aspirante_correo ?>" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="validationServerUsername" class="form-label">Carrera</label>
                                    <div class="input-group has-validation">
                                        <input type="text" class="form-control " id="validationServerUsername" value="<?php echo ($datos[0]->aspirante_curp == 1) ? 'Licenciatura en Enfermería y Obstetricia' : 'Licenciatura en Médico Cirujano'; ?>" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="validationServer03" class="form-label">CURP</label>
                                    <input type="text" class="form-control " id="validationServer03" value="<?php echo $datos[0]->aspirante_curp ?>" disabled>
                                </div>
                                <div class="col-md-6">
                                    <label for="validationServer05" class="form-label">Telefono</label>
                                    <input type="text" class="form-control " id="validationServer05" value="<?php echo $datos[0]->aspirante_telefono ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($etapas_formularios as $contenido) : ?>
                            <div class="tab-pane form_content" id="<?php echo "data".$contenido->formulario_id ?>" role="tabpanel"> <?php echo $contenido->formulario_id." => ".$contenido->formulario_nombre ?> profile Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi eligendi, quaerat quam sapiente incidunt ex molestiae mollitia doloribus doloremque non nihil cum animi dolor autem praesentium facilis ad repellat nemo.</div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
   'use strict';
    const collapse_list = document.getElementsByClassName('accordion-button');
    
    let desactivar = () => {
        let formularios = document.getElementsByClassName('item_formulario');
        for (let index = 0; index < formularios.length; index++) {
           formularios[index].classList.remove('active');
        }
    }
    for (let array = 0; array < collapse_list.length; array++) {
        collapse_list[array].addEventListener('click', desactivar);
    }
    let datahome = () =>{
        let formularios = document.getElementsByClassName('item_formulario');
        for (let index = 0; index < formularios.length; index++) {
            formularios[index].classList.remove('active');
        }
        let datos = document.getElementsByClassName('form_content');
        for (let index = 0; index < datos.length; index++) {
            datos[index].classList.remove('active');
            
        }
        document.getElementById('dataHome').classList.add('active');
    }
    
</script>
<?php echo $this->endSection() ?>