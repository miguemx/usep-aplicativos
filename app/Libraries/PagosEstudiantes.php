<?php
namespace App\Libraries;

use App\Models\RosModel;
use App\Models\PagosAlumnosModel;
use App\Models\PagosConceptosModel;
use App\Models\KardexModel;
use App\Entities\PagoAlumno;

class PagosEstudiantes {

    public $pago; // la entidad de pago actual

    private $pagosAlumnosModel;
    private $pagosConceptosModel;
    private $rosModel;
    
    private $errors = [];
    private $lastError = false;

    public function __construct() {
        $this->pagosAlumnosModel = new PagosAlumnosModel();
        $this->pagosConceptosModel = new PagosConceptosModel();
        $this->rosModel = new RosModel();
    }

    /**
     * devuelve el último error reportado
     */
    public function getError() {
        return $this->lastError;
    }

    /**
     * devuelve todos los errores reportados
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * devuelve todos los pagos registrados de una matrícula determinada
     * @param matricula la matricula del alumno a buscar sus pagos
     * @return array con los pagos encontrados, o vacío si no hay pagos registrados
     */
    public function alumno( $matricula ) {
        return $this->pagosAlumnosModel->findByAlumno( $matricula );
    }

    /**
     * devuelve los conceptos de pago registrados 
     * @param todos false por defecto para devolver los activos; true para devolver todos
     * @return conceptos un arreglo con los conceptos encontrados, o vacío si no existen
     */
    public function conceptos( $todos=false ) {
        if ( $todos ) $this->pogosConceptosModel->withDeleted();
        $conceptos = $this->pagosConceptosModel->findAll();
        return $conceptos;
    }

    /**
     * guarda un pago de un estudiante
     * @param matricula la matrícula del estudiante
     * @param archivo el nombre del archivo del comprobante
     * @param datos un arreglo con los datos del pago [ concepto, referencia, status]
     * @return true si el registro se crea correctamente
     * @return false si el registro no se puede crear correctamente
     */
    public function registra( $matricula, $archivo, $datos ) {
        $this->pago = new PagoAlumno( $datos );
        if (!$this->pago->tipo) $this->addError( 'Por favor selecciona el tipo de cuota.' );
        if (!$this->pago->concepto) $this->addError( 'Por favor selecciona un concepto de pago.' );
        if (!$this->pago->referencia || !preg_match("/^[0-9]{20}$/", $this->pago->referencia)) $this->addError( 'Por favor proporciona una referencia correcta.' );
        if ( count($this->errors) == 0 ) {
            try {
                if ( !$this->pagosAlumnosModel->referenciaExistente( $this->pago->referencia ) ) {
                    $pagoConcepto = $this->pagosConceptosModel->find($this->pago->concepto);
                    $this->pago->comprobante = $archivo;
                    $this->pago->conceptoTexto = $pagoConcepto->nombre;
                    $this->pago->alumno = $matricula;
                    $this->pago->monto = $pagoConcepto->monto;
                    $this->pago->fecha = date("Y-m-d");
                    $this->pago->status = 'EN PROCESO';
                    if ( $this->pagosAlumnosModel->insert($this->pago) ) {
                        $this->pago->id = $this->pagosAlumnosModel->lastId();
                        return true;
                    }
                    $this->addError( 'No se pudo registrar el pago. Puede ser que la referencia ya esté registrada.' );
                }
                else {
                    $this->addError( 'La referencia ya ha sido registrada.' );
                }
            } catch (\Exception $ex) {
                $this->addError( 'Ocurrió un error al seleccionar el concepto.' );
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            }
        }
        return false;
    }

    /**
     * reporta un mensaje de error
     * @param error el mensaje de error en una cadena
     */
    private function addError( $error ) {
        $this->errors[] = $error;
        $this->lastError = $error;
    }

    /**
     * busca cotejar los elementos guardados con los registros de los estudiantes
     * @param referencia la referancia para buscar en los pagos del ROS
     */
    public function coteja($referencia) {
        $pagoAlumno = $this->pagosAlumnosModel->refPorCotejar( $referencia );
        if ( !is_null($pagoAlumno) ) {
            $ros = $this->rosModel->findReferencia( $pagoAlumno->referencia );
            if ( !is_null($ros) ) {
                if ( $ros->cotejado == '0' ) {
                    try {
                        $pagoAlumno->status = 'ACEPTADO';
                        $ros->cotejado = '1';
                        $ros->matricula = $pagoAlumno->alumno;
                        $this->rosModel->save( $ros );
                        $this->pagosAlumnosModel->save( $pagoAlumno );
                        log_message( 'notice', "ROS: Pago cotejado con alumno $pagoAlumno->alumno y referencia $referencia." );
                        return true;
                    }
                    catch ( \Exception $ex ) {
                        log_message( 'error', "ERROR: Al cotejar - {$ex}", [ 'ex' => $ex ] );
                        $this->addError( "No fue posible actualizar la referencia de cotejamiento: ref $referencia (".date("YmdHis").')' );
                    }
                }
                else {
                    $this->addError( "La referencia ya ha sido utilizada: ref $referencia" );
                }
            }
            else {
                $this->addError( "El pago aún no ha sido reportado por el ROS: ref $referencia" );
            }
        }
        else {
            $this->addError( "El pago a cotejar no fue reportado: ref $referencia" );
        }
        return false;
    }

    /**
     * derivado de un pago, habilita una materia para extraordinario o recurso
     * @param pagoAlumno el objeto del pago del alumno
     */
    private function activaMateria($pagoAlumno) {
        echo "Activar materia \n";
        if ( $pagoAlumno->tipo == 'EXTRAORDINARIO' ) {
            $kardex = new KardexModel();
            $gpoalumno = $kardex->materiaCursando( $pagoAlumno->alumno, $pagoAlumno->materia );
            if ( !is_null($gpoalumno) ) {
                $sql = "UPDATE aca_grupos_alumnos SET gpoalumno_flg_pago='1' WHERE gpoalumno_alumno = ? AND gpoalumno_grupo = ? ";
                $this->db->query($sql, [$gpoalumno->gpoalumno_alumno, $gpoalumno->gpoalumno_grupo]);
                echo "ACTIVADA -- \n";
            }
        }
    }

    /**
     * derivado de un pago, habilita una materia para extraordinario o recurso
     * @param pagoAlumno el objeto del pago del alumno
     */
    private function activaReinscripcion($pagoAlumno) {
        echo "VALIDAR INSCRIPCION ($pagoAlumno->alumno) -----> ";
        if ( $pagoAlumno->tipo == 'REINSCRIPCION' ) {
            echo "TIPO DE PAGO CORRECTO -----> ";
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find( $pagoAlumno->alumno );
            if ( !is_null($alumno) ) {
                if ( $alumno->status != 'ACTIVO' ) {
                    $alumno->status = 'ACTIVO';
                    $alumnosModel->update( $alumno->id, $alumno );
                    echo "ALUMNO $alumno->id REINSCTITO -- ";
                }
            }
        }
        echo "\n";
    }

}