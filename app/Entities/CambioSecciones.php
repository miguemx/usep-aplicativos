<?php
namespace App\Entities;

use CodeIgniter\Entity;

class CambioSecciones extends Entity {
    protected $attributes = [
        'matricula' => null,
        'procedencia' => null,
        'final' => null,
        'oficio' => null,
        'observaciones' => null,
        'usuario' => null,
    ];

    protected $datamap = [
        'matricula' => 'cambios_matricula',
        'procedencia' => 'cambios_seccion_procedencia',
        'final' => 'cambios_seccion_final',
        'oficio' => 'cambios_oficio',
        'observaciones' => 'cambios_observaciones',
        'usuario' => 'cambios_usuario',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}