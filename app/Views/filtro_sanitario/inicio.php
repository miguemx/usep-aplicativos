<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>
<STyle>
    body {
        padding: 40px 5px;
        font-family: 'Open Sans', sans-serif;
        background-color: #dcdcdc;
        height: 100vh;
        width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
        background-size: 794px 1100px;
        background-repeat: no-repeat;
        background-position: left;

       /* filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/

    }

    .QR {
        display: flex;
        justify-content: center;

    }

    .card-qr {
        max-width: 40rem;
        border-radius: 20px;
        background-color: whitesmoke;
    }

    .contenedor-qr {
        border-radius: 50px;
        max-width: 40rem;
        background-color: #dcdcdc;
    }

    .logo-cabecera {
        background-image: url(<?php echo base_url('img/Logo_USEP_negativo.png') ?>);
        background-color: #841138;
        color: antiquewhite;
        text-align: center;
        border-top-left-radius: 20px !important;
        border-top-right-radius: 20px !important;
        padding: 20px;
        min-height: 100px;
        background-repeat: no-repeat;
        background-size: 200px;
        background-position: center;
        position: relative;
    }

    .correo {
        min-height: 100px;
        vertical-align: middle;
    }
</STyle>
<div class="container contenedor-qr">
    <div class="card card-qr">
        <div class="card-header logo-cabecera">
        </div>
        <br>
        <div class="card-body QR">
            <form class="row g-3" method="post" action="<?php echo base_url('FiltroSanitario/usuario'); ?>">
                <div class="correo">
                    <label for="id_correo" class="form-label">Ingresa tu correo</label>
                    <input type="email" class="form-control" id="id_correo" name="id_correo" value="<?php echo $correo; ?>" required>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="accion" id="flexRadioDefault1" value="new" />
                    <label class="form-check-label" for="flexRadioDefault1">
                        Contestar un nuevo cuestionario
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="accion" id="flexRadioDefault2" value="qr" checked="checked" />
                    <label class="form-check-label" for="flexRadioDefault2">
                        Recuperar mi código QR
                    </label>
                </div>

                <div class="busqueda" style="display:flex; justify-content:center">
                    <button type="submit" class="btn btn-secondary">Buscar</button>
                </div>
                <br>
            </form>
        </div>

    </div>
</div>

<?php echo $this->endSection() ?>