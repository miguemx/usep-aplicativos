<?php
namespace App\Entities;

use CodeIgniter\Entity;

class InsertarFiltroSanitario extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'folio' => null,
        'correointerno' => null,
        'correoexterno'=> null,
        'vacuna' => null,
        'diabetes'=> null,
        'hipertension'=> null,
        'antecedentecard'=> null,
        'enfermedadres'=> null,
        'otropadecimiento'=> null,
        'fumador'=> null,
        'mayorfiebre'=> null,
        'mayortos'=> null,
        'mayordifrespiratoria'=> null,
        'mayordolorcabeza'=> null,
        'menordolormuscular'=> null,
        'menordolorarticular'=> null,
        'menorfatiga'=> null,
        'menoralimentos'=> null,
        'menorabdominal'=> null,
        'menoralteragusto'=> null,
        'menoralteraolfato'=> null,
        'menornasal'=> null,
        'menorconjuntivitis'=> null,
        'menorescalofrios'=> null,
        'menorviaje'=> null,
        'menorcovid'=> null,
        'fecha12' => null,
        'fecha13'=> null,
        'fecha14'=> null,
        'menorotro'=> null,
        'temp'=> null,
        'oxigen'=> null,
        'fechafiltro'=> null,
        'tipotos'=>null
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'folio' => 'filtro_folio',
        'correointerno' => 'filtro_correo',
        'correoexterno'=> 'filtro_correo_externo',
        'diabetes'=> 'filtro_diabetes',
        'hipertension'=> 'filtro_hipertension',
        'antecedentecard'=> 'filtro_antecedentecard',
        'enfermedadres'=> 'filtro_enfermedadres',
        'otropadecimiento'=> 'filtro_otropadecimiento',
        'fumador'=> 'filtro_fumador',
        'mayorfiebre'=> 'filtro_mayorfiebre',
        'mayortos'=> 'filtro_mayortos',
        'mayordifrespiratoria'=> 'filtro_mayordifrespiratoria',
        'mayordolorcabeza'=> 'filtro_mayordolorcabeza',
        'menordolormuscular'=> 'filtro_menordolormuscular',
        'menordolorarticular'=> 'filtro_menordolorarticular',
        'menorfatiga'=> 'filtro_menorfatiga',
        'menoralimentos'=> 'filtro_menoralimentos',
        'menorabdominal'=> 'filtro_menorabdominal',
        'menoralteragusto'=> 'filtro_menoralteragusto',
        'menoralteraolfato'=> 'filtro_menoralteraolfato',
        'menornasal'=>  'filtro_menornasal',
        'menorconjuntivitis'=> 'filtro_menorconjuntivitis',
        'menorescalofrios'=> 'filtro_menorescalofrios',
        'menorviaje'=> 'filtro_menorviaje',
        'menorcovid'=> 'filtro_menorcovid',
        'menorotro'=> 'filtro_menorotro',
        'temp'=> 'filtro_temp',
        'oxigen'=> 'filtro_oxigen',
        'fechafiltro' => 'filtro_fecha',
        'vacuna' => 'filtro_vacuna',
        'fecha12'=> 'filtro_fechacontactocovid',
        'fecha13'=> 'filtro_fechapruebanegativo',
        'fecha14'=> 'filtro_fechapruebapositiva',
        'tipotos'=> 'filtro_tipotos'
    ];
    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}