CREATE VIEW view_horario_docente as
SELECT
   aca_grupos.grupo_id,
   aca_grupos.grupo_clave,
   aca_grupos.grupo_materia,
   aca_grupos.grupo_docente,
   aplicativo_empleados.empleado_correo,
   aplicativo_empleados.empleado_nombre,
   aplicativo_empleados.empleado_apellido,
   aplicativo_empleados.empleado_ap_paterno,
   aplicativo_empleados.empleado_ap_materno,
   aca_grupos.grupo_periodo,
   aca_materias.materia_nombre,
   aca_grupos_aulas.gpoaula_dia,
   aca_grupos_aulas.gpoaula_inicio,
   aca_grupos_aulas.gpoaula_fin,
   aca_aulas.aula_id,
   aca_aulas.aula_nombre
FROM
    aca_grupos
INNER JOIN aca_materias ON aca_materias.materia_clave = aca_grupos.grupo_materia
INNER JOIN aca_grupos_aulas ON aca_grupos.grupo_id = aca_grupos_aulas.gpoaula_grupo
INNER JOIN aca_aulas on aca_aulas.aula_id = aca_grupos_aulas.gpoaula_aula
INNER JOIN aplicativo_empleados on aca_grupos.grupo_docente = aplicativo_empleados.empleado_id  
ORDER BY `aca_grupos`.`grupo_id` ASC;

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '1', 'docente', '*'), (NULL, '5', 'docente', 'horario');