<?php
namespace App\Models;
use CodeIgniter\Model;

class CarrerasModel extends Model {
    protected $table      = 'prein_carreras';
    protected $primaryKey = 'carrera_id';

    protected $returnType    = 'App\Entities\Carrera';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'carrera_id','carrera_nombre','carrera_numero','carrera_clave','carrera_clave_programa','carrera_facultad'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene todas las carreras activas
     * @todo modificar la tabla para aceptar soft deletes o bien, devolver mediante una bandera las carreras activas
     */
    public function activas() {
        $carreras = $this->findAll();
        return $carreras;
    }
    
}
