-- creación del nuevo rol
INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES ('13', 'FIRMARIPEL', 'alumnos');

-- creación de la nueva columna para validar el rippel
ALTER TABLE `alumnos_generales` ADD `firmaripel` INT(1) NULL AFTER `alumno_status`;

-- se updatea el rol de los alumnos
update `aplicativo_usuarios` set usuario_rol =13 where usuario_rol =4;

-- creación de la tabla que guarda los datos del alumno que firma el ripel
create table alumnos_ripel(
ripel_matricula varchar(9),
ripel_ip varchar(11),
ripel_fecha timestamp
);
-- creación del permiso 
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '13', 'login', '*'), (NULL, '13', 'home', '*');

-- creación de la columna en aplicativos usuarios

ALTER TABLE `aplicativo_usuarios` ADD `usuario_ultcnx` TIMESTAMP NULL AFTER `usuario_nombre`;

-- creación de la vista para el reporte ripel
create view view_reporte_ripel as (
SELECT
 a.ripel_matricula,
 b.alumno_nombres,
 b.alumno_ap_paterno,
 b.alumno_ap_materno,
 c.usuario_correo,
 a.ripel_fecha,
 a.ripel_ip,
 c.usuario_ultcnx
FROM
	`alumnos_ripel` as a,
    `alumnos_generales` as b,
    `aplicativo_usuarios` as c
WHERE 
	a.ripel_matricula=b.alumno_id AND
    b.alumno_correo=c.usuario_correo)