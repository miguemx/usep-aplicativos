<?php
namespace App\Entities;

use CodeIgniter\Entity;

class EstadisticaRegiones extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'cantidad' => null,
        'region' => null,
        'carrera' => null,
        'sexo' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
        'cantidad' => 'n_alumnos',
        'region' => 'region_nombre',
        'carrera' => 'aspirante_carrera',
        'sexo' => 'aspirante_sexo',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}