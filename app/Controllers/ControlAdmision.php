<?php

namespace App\Controllers;

use App\Entities\AspirantesCampos;
use App\Entities\AspirantesConfiguracion;
use App\Entities\AspirantesEtapas;
use App\Entities\AspirantesFormularios;
use App\Entities\AspirantesGenerales;
use App\Entities\AspirantesNotificaciones;
use App\Entities\AspirantesRespuestas;
use App\Libraries\RespuestaValidacion;
use App\Models\AspiranteRespuestasModel;
use App\Models\AspirantesCamposModel;
use App\Models\AspirantesComentariosModel;
use App\Models\AspirantesConfiguracionModel;
use App\Models\AspirantesEtapasModel;
use App\Models\AspirantesFormulariosModel;
use App\Models\AspirantesGeneralesModel;
use App\Models\AspirantesModel;
use App\Models\AspirantesNotificacionesModel;
use App\Models\AspirantesRespuestasModel;
use App\Models\AspirantesTiposCampoModel;
use App\Models\CarrerasModel;
use App\Models\CCTSModel;
use App\Models\EstadosModel;
use App\Models\MunicipiosModel;
use App\Models\PeriodoModel;

class ControlAdmision extends BaseController
{
    private $configuracionModelo;
    private $etapasModel;
    private $formulariosModel;
    private $etapaEnttitie;
    private $camposModel;
    private $respuestasModel;
    private $notificacionesModel;
    private $aspirantesModel; // apunta al modelo de la vista respuesta
    private $comentariosModel;
    private $carrerasModel;
    private $respuestaTModel; //apunta al modelo de la tabla respuestas

    public function __construct()
    {
        $this->configuracionModelo = new AspirantesConfiguracionModel();
        $this->etapasModel = new AspirantesEtapasModel();
        $this->formulariosModel = new AspirantesFormulariosModel();
        $this->etapaEnttitie = new AspirantesEtapas();
        $this->camposModel = new AspirantesCamposModel();
        $this->respuestasModel = new AspiranteRespuestasModel();
        $this->notificacionesModel = new AspirantesNotificacionesModel();
        $this->aspirantesModel = new AspirantesGeneralesModel;
        $this->comentariosModel = new AspirantesComentariosModel();
        $this->carrerasModel = new CarrerasModel();
        $this->respuestaTModel = new AspirantesRespuestasModel();
    }

    /**
     * rederiza la vista principal con los datos de las etapas creadas 
     */
    public function DiseñadordeModelo($mensaje = null)
    {

        if ($this->sesion->get('rol') == 'SERVICIOS ESCOLARES' || $this->sesion->get('rol') == 'ADMINISTRADOR') {

            if ($mensaje) {
                $data['mensaje'] = $mensaje;
            }
            if ($this->request->getPost('home-back')) {
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha cancelado la operacion. '
                ];
                $data['mensaje'] = $mensaje;
            }

            $data['configuraciones'] =  ($this->configuracionModelo->findAll()) ? $this->configuracionModelo->findAll() : null;
            $data['formularios'] = $this->formulariosModel->buscarFormularios();
            $data['etapasexistentes'] = $this->etapasModel->buscaretapas();
            $data['menu'] = $this->sesion->get('menu');
            $data['etapas'] = true;
            return view('admision/generadordeadmision', $data);
        } else {
            return redirect()->to(base_url());
        }
    }

    /**
     * modifica las configuraciones exitentes en la lista
     */
    public function updateConfiguracion()
    {

        $configuracion = $this->request->getPost();
        $confi_model = $this->configuracionModelo->find($configuracion['datos']);
        $this->response->setHeader('Content-Type', 'application/json');
        //log_message('error', $configuracion);

        try {
            $confi_model->configuracion_status = ($confi_model->configuracion_status == 1) ? 0 : 1;
            $this->configuracionModelo->update($configuracion, $confi_model);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Configuracion ' . $confi_model->configuracion_nombre . ' actualizada con exito. '
            ];
            echo json_encode($mensaje);
        } catch (\Exception $ex) {
            //log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No fue posible actualizar la Configuracion ' . $confi_model->configuracion_nombre
            ];
            return $this->DiseñadordeModelo($mensaje);
            echo json_encode($mensaje);
        }

        //echo json_encode($configuracion);
    }

    public function agregarConfiguracion()
    {
        $configuracion = new AspirantesConfiguracion();
        $configuracion->nombre = $this->request->getPost('name-config');
        $configuracion->status = ($this->request->getPost('status-config')) ? 1 : 0;
        try {
            $this->configuracionModelo->insert($configuracion);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Configuracion agregada con exito. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No fue posible agregar la Configuracion. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        }
        //var_dump($this->request->getPost());
        //return $this->DiseñadordeModelo($mensaje);
    }

    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA LA CREACION Y MODIFICACION DE ETAPAS DEL PROCESO DE ADMISION                               |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * @param periodos devuelte a la vista los periodos existentes
     * @param formulario devuelve los formularios existentes
     * 
     */
    public function agregaretapa()
    {
        $periodomodel = new PeriodoModel();
        $data['conta_etapas'] = count($this->etapasModel->findAll()) + 1;
        $data['periodos'] = $periodomodel->lista();
        $data['menu'] = $this->sesion->get('menu');
        $data['configuradoretapa'] = true;
        return view('admision/etapas', $data);
    }

    /**
     * recibe datos de la vista para generar etapa
     * al finalizar regresa a la primera vista para la administracion de etapas
     */
    public function generaretapa()
    {
        try {
            $this->etapaEnttitie = new AspirantesEtapas();
            $this->etapaEnttitie->periodo = $this->request->getPost('select-periodo');
            $this->etapaEnttitie->nombre = $this->request->getPost('nombre-etapa');
            $this->etapaEnttitie->orden = $this->request->getPost('orden-etapa');
            $this->etapaEnttitie->formulario = ($this->request->getPost('form-vinculado') == true) ? '1' : '0';
            $this->etapaEnttitie->responsable =  ($this->request->getPost('responsabilidad-rol') == true) ? '1' : '0';
            $this->etapasModel->insert($this->etapaEnttitie);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Etapa agregada con exito. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No fue posible agregar la etapa. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        }
    }
    /**
     * retorna la vista para el panel de modificacion de etapas
     */
    public function modificaretapa()
    {
        $etapa_id = $this->request->getPost('etapa-identificador');

        $datosetapa = $this->etapasModel->find($etapa_id);
        $data['identificacdor'] = $etapa_id;
        $data['periodo_selected'] =  $datosetapa->etapa_periodo;
        $data['nombre_selected'] = $datosetapa->nombre;
        $data['secuencia_selected'] = $datosetapa->orden;
        $data['responsable_selected'] = ($datosetapa->responsable == 1) ? true : null;
        $data['vinculado_selected'] = ($datosetapa->formulario == 1) ? true : null;
        $data['bandera_updateetapa'] = true;
        $periodomodel = new PeriodoModel();
        $data['periodos'] = $periodomodel->lista();
        $data['menu'] = $this->sesion->get('menu');
        $data['configuradoretapa'] = true;
        return view('admision/etapas', $data);
    }

    /**
     * recibe los datos de una etapa existente y modificac los campos 
     */
    public function modificaretapaexistente()
    {

        $etapamodel = new AspirantesEtapasModel();
        $etapa_valores = $etapamodel->findAll($this->request->getPost('etapa-identificador'));
        $etapa_id = $this->request->getPost('etapa-identificador');
        if ($etapa_id) {
            try {
                $etapa_valores['etapa_periodo'] = $this->request->getPost('select-periodo');
                $etapa_valores['etapa_nombre'] = $this->request->getPost('nombre-etapa');
                $etapa_valores['etapa_orden'] = $this->request->getPost('orden-etapa');
                $etapa_valores['etapa_formulario'] = ($this->request->getPost('form-vinculado') == true) ? '1' : '0';
                $etapa_valores['etapa_responsable'] = ($this->request->getPost('responsabilidad-rol') == true) ? '1' : '0';
                $etapamodel->update($etapa_id, $etapa_valores);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Etapa Modificada con exito. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'No fue posible Modificar la etapa seleccionada. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            }
        } else {
            echo "sin valores";
        }
    }
    public function eliminaretapa()
    {
        if ($this->request->getPost('etapa-identificador')) {
            $etapamodel = new AspirantesEtapasModel();
            try {

                //buscar todos los fomularios que pertenezcan a la estapa
                $formularios_activos = $this->formulariosModel->getFromEtapa($this->request->getPost('etapa-identificador'));
                foreach ($formularios_activos as $arr_datos) {
                    $this->formulariosModel->delete($arr_datos->formulario);
                    $campos_formulario = $this->camposModel->camposformularios($arr_datos->formulario);
                    foreach ($campos_formulario as $arr_campos) {
                        $this->camposModel->delete($arr_campos->campo);
                    }
                }
                $etapamodel->delete($this->request->getPost('etapa-identificador'));
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Etapa eliminada con exito. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                //$data['error_etapa'] = 'No fue posible Modificar la etapa seleccionada. ';
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'No fue posible eliminar la etapa seleccionada. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            }
        }
    }

    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA LA CREACION Y MODIFICACION DE FORMULARIOS PARA LAS ETAPAS DEL PROCESO DE ADMISION          |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * retorna la vista para generacion del formulario
     */
    public function agregarFormulario()
    {

        $etapasmodel = new AspirantesEtapasModel();
        $detallesetapa = $etapasmodel->find($this->request->getPost('etapa-identificador'));
        if ($detallesetapa) {

            $data['etapa_detalles'] =  $detallesetapa->etapa_id;
            $data['etapa_periodo'] =  $detallesetapa->etapa_periodo;
            $data['etapa_nombre'] = $detallesetapa->etapa_nombre;
            $data['etapa_orden'] = $detallesetapa->etapa_orden;
        }
        $aux = $this->formulariosModel->getFromEtapa($this->request->getPost('etapa-identificador'));
        $index = 0;
        foreach ($aux as $contador) {
            $index++;
        }
        $data['num_form'] = $index + 1;
        $data['menu'] = $this->sesion->get('menu');
        $data['configuracionformulario'] = true;
        return view('admision/formularios', $data);
    }
    /**
     * genera el registro de un nuevo formulario
     */
    public function configurarformulario()
    {
        $formulariomodel = new AspirantesFormulariosModel();
        $formularioent = new AspirantesFormularios();
        try {
            $formularioent->etapa =         $this->request->getPost('formulario-identificador');
            $formularioent->nombre =        $this->request->getPost('nombre-formulario');
            $formularioent->titulo =        $this->request->getPost('titulo-formulario');
            $formularioent->orden =         $this->request->getPost('orden-formulario');
            $formularioent->instrucciones = $this->request->getPost('help-text');
            $formulariomodel->insert($formularioent);
            //$data['exito_formulario'] = 'Formulario creado con exito.';
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Formulario ' . $formularioent->nombre . ' creado con exito. '
            ];
            return  $this->DiseñadordeModelo($mensaje);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);

            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No fue posible crear el formulario para la etapa seleccionada. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        }
    }

    /**
     * accede a la informacion del formulario seleccionado
     */
    public function detalleformulario($mensaje = null)
    {
        if ($mensaje) {
            $data['mensaje'] = $mensaje;
        }
        $formulariomodel = new AspirantesFormulariosModel();
        $inpusformulario = new AspirantesCamposModel();

        $datos = $inpusformulario->camposformularios(trim($this->request->getPost('formulario-identificador')));
        if ($datos) {
            $data['items_activos'] = $datos;
        } else {
            $data['items_activos'] = null;
        }
        $aux =  1;
        $camposcompletos = $inpusformulario->camposdetalles($this->request->getPost('formulario-identificador'));
        if ($camposcompletos) {
            $data['items_form'] = $camposcompletos;
            $aux = count($camposcompletos) + 1;
        }
        $data['conta_item'] = $aux;
        $tiposcampos = new AspirantesTiposCampoModel();
        $tipos_select = $tiposcampos->findAll();
        $data['selecs_tipos'] =  $tipos_select;
        $data['formularioinformacion'] = $formulariomodel->formulariocompleto($this->request->getPost('formulario-identificador'));
        $data['menu'] = $this->sesion->get('menu');
        $data['detallesformulario'] = true;
        return view('admision/items', $data);
    }

    /**
     * regresa al modo de creacion del formulario
     * 
     */
    public function editarformulario()
    {
        $formulariomodel = new AspirantesFormulariosModel();
        $informacion = $formulariomodel->formulariocompleto($this->request->getPost('formulario-identificador'));
        if ($informacion) {
            foreach ($informacion as $dato) {
                $data['nombre_form'] =  $dato->formulario_nombre;
                $data['titulo_form'] =  $dato->formulario_titulo;
                $data['numero_form'] = $dato->formulario_orden;
                $data['instruccion_form'] = $dato->formulario_instrucciones;
                $data['bandera_edit'] = true;
            }
        }
        $etapasmodel = new AspirantesEtapasModel();
        $id_etapa = $this->request->getPost('etapa-identificador');
        $detallesetapa = $etapasmodel->findAll($id_etapa);
        if ($detallesetapa) {
            foreach ($detallesetapa as $detalles) {
                $data['etapa_detalles'] =  $detalles->etapa_id;
                $data['etapa_periodo'] =  $detalles->etapa_periodo;
                $data['etapa_nombre'] = $detalles->etapa_nombre;
                $data['etapa_orden'] = $detalles->etapa_orden;
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        $data['id_formulario'] = $this->request->getPost('formulario-identificador');
        $data['configuracionformulario'] = true;
        return view('admision/formularios', $data);
    }


    /**
     * eliminar formulario 
     */
    public function eliminaformulario()
    {
        if ($this->request->getPost('formulario-identificador')) {
            $formulariomodel = new AspirantesFormulariosModel();
            try {
                $formulariomodel->delete($this->request->getPost('formulario-identificador'));
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Formulario eliminado con exito. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                //$data['error_etapa'] = 'No fue posible Modificar la etapa seleccionada. ';
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'No fue posible eliminar el formulario seleccionada. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            }
        }
    }
    public function updateformulario()
    {
        $nombre = $this->request->getPost('nombre-formulario');
        $id_formulario = $this->request->getPost('formulario-id');
        /* echo $nombre."<br>".$id_formulario;
        die(); */
        $titulo = $this->request->getPost('titulo-formulario');
        $orden = $this->request->getPost('orden-formulario');
        $help = $this->request->getPost('help-text');
        if ($id_formulario) {
            $formulariomodel = new AspirantesFormulariosModel();
            $formulario = $formulariomodel->find(trim($id_formulario));
            if ($formulario) {
                try {
                    $formulario->formulario_nombre =        $nombre;
                    $formulario->formulario_titulo =        $titulo;
                    $formulario->formulario_orden =         $orden;
                    $formulario->formulario_instrucciones =  $help;
                    $formulariomodel->update($id_formulario, $formulario);
                    $mensaje = [
                        'tipo' => 'alert-success',
                        'texto' => 'Formulario editado con exito. '
                    ];
                    return $this->DiseñadordeModelo($mensaje);
                } catch (\Exception $ex) {
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                    $mensaje = [
                        'tipo' => 'alert-danger',
                        'texto' => 'No fue posible editar los datos del formulario.'
                    ];
                    return $this->DiseñadordeModelo($mensaje);
                }
            } else {
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'No fue posible editar los datos del formulario. '
                ];
                return $this->DiseñadordeModelo($mensaje);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No fue posible editar los datos del formulario. '
            ];
            return $this->DiseñadordeModelo($mensaje);
        }
    }
    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA LA CREACION Y MODIFICACION DE CAMPOS PARA LOS FORMULARIOS DEL PROCESO DE ADMISION          |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Funcion para añadir item al formulario seleccionado
     */
    public function agregaritemformulario()
    {
        $campomodelo = new AspirantesCamposModel();
        $campoent = new AspirantesCampos();
        try {

            $campoent->formulario = $this->request->getPost('formulario-identificador');
            $campoent->nombre = $this->request->getPost('etiqueta-nombre');
            $campoent->descripcion = $this->request->getPost('help-text');
            $campoent->tipo = $this->request->getPost('selector-tipo');
            $campoent->dependiente = ($this->request->getPost('inputs-relacionados') == 0) ? null : $this->request->getPost('inputs-relacionados');
            $campoent->orden = $this->request->getPost('orden-item');
            $campoent->obligatorio = ($this->request->getPost('campo-obligatorio') == true) ? 1 : null;
            $campoent->visible = ($this->request->getPost('campo-visible') == true) ? 1 : 0;
            $campomodelo->insert($campoent);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Item agregado con exito. '
            ];
            return $this->detalleformulario($mensaje);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'Item no pudo ser agregado. '
            ];
            return $this->detalleformulario($mensaje);
        }
    }

    /**
     * elimina un item seleccionado del formulario
     * @param id_item id correspondiente al item a eliminar
     */
    public function eliminaritem()
    {
        $id_item =  $this->request->getPost('identidicadoritem');
        $itemmodel = new AspirantesCamposModel();

        try {
            $itemmodel->delete($id_item);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'Item eliminado con exito. '
            ];
            return $this->detalleformulario($mensaje);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'Item no pudo ser eliminado. '
            ];
            return $this->detalleformulario($mensaje);
        }
    }
    /**
     * retorna a la vista para edicion del item con los datos correspondientes para su edicion
     * @param id_formulario con el id respectivo al formulario al que pertenece el item
     * @param id_item con el id del item a modificar
     */
    public function editarimen()
    {
        $formulariomodel = new AspirantesFormulariosModel();
        $inpusformulario = new AspirantesCamposModel();
        $id_formulario = $this->request->getPost('formulario-identificador');
        $id_item = $this->request->getPost('identidicadoritem');
        $informacion = $inpusformulario->find($id_item);
        if ($informacion) {
            $data['nombre'] =  $informacion->campo_nombre;
            $data['descripcion'] =  $informacion->campo_descripcion;
            $data['selectedtipo'] =  $informacion->campo_tipo;
            $data['selectedcampo'] =  $informacion->campo_dependiente;
            $data['secuencia'] =  $informacion->campo_orden;
            $data['obligatorio'] =  $informacion->campo_obligatorio;
            $data['visible'] =  $informacion->visible;
        }

        $datos = $inpusformulario->camposformularios($id_formulario);
        if ($datos) {
            $data['items_activos'] = $datos;
        } else {
            $data['items_activos'] = null;
        }
        $camposcompletos = $inpusformulario->camposdetalles($id_formulario);
        if ($camposcompletos) {
            $data['items_form'] = $camposcompletos;
        }
        $tiposcampos = new AspirantesTiposCampoModel();
        $tipos_select = $tiposcampos->findAll();
        $data['selecs_tipos'] =  $tipos_select;
        $data['id_form'] =  $id_formulario;
        $data['id_item'] =  $id_item;
        $data['formularioinformacion'] = $formulariomodel->formulariocompleto($id_formulario);
        $data['menu'] = $this->sesion->get('menu');
        $data['detallesformulario'] = true;
        return view('admision/editaritem', $data);
    }

    /**
     * funcion para editar el item seleccionado de un formulario
     * @param id_item 
     * @return mensaje que contiene el respectivo mensaje resultado del proeso 
     * retorna la vista del formulario padre 
     */
    public function moficicaritem()
    {
        $id_item = $this->request->getPost('identidicadoritem');
        $itemmodel = new AspirantesCamposModel();
        $orden_modificado =  $this->request->getPost('orden-item');
        $item = $itemmodel->find($id_item);
        if ($item) {
            try {
                // $item->formulario = $this->request->getPost('formulario-identificador');
                $item->nombre = $this->request->getPost('etiqueta-nombre');
                $item->descripcion = $this->request->getPost('help-text');
                $item->tipo = $this->request->getPost('selector-tipo');
                $item->dependiente = ($this->request->getPost('inputs-relacionados') == 0) ? null : $this->request->getPost('inputs-relacionados');
                $item->orden = $orden_modificado;
                $item->obligatorio = ($this->request->getPost('campo-obligatorio') == true) ? 1 : null;
                $item->visible = ($this->request->getPost('campo-visible') == true) ? 1 : 0;
                $itemmodel->update($id_item, $item);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Item modificado con exito. '
                ];
                return $this->detalleformulario($mensaje);
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'Item no pudo ser modificado. '
                ];
                return $this->detalleformulario($mensaje);
            }
        }
    }

    // --------------------------------------------------------------------------------------------------------------
    // |                                                                                                            |
    // |    METODOS PARA LA MODIFICACION DE DATOS DE LOS ASPIRANTES POR PARTE DE SERVICIOS ESCOLARES                |
    // |                                                                                                            |
    // --------------------------------------------------------------------------------------------------------------
    /**
     * retorna la vista principal del menu
     */
    public function EdicionAspirantes()
    {
        $data = $this->buscaGenerales(0);
        $data['menu'] = $this->sesion->get('menu');
        $data['aspirante'] = null;
        return view('admision/edicionaspirantes', $data);
    }
    /**
     * retorna la vista principal del menu
     */
    public function ListadoAspirantes()
    {
        
            $aspirantemodel = new AspirantesGeneralesModel();
            $data = $this->request->getGet();
            if (!count($data) || !array_key_exists('folio', $data)) {
                $data = ['folio' => '', 'carrera' => '', 'curp' => '', 'estatus' => '', 'nombre' => '', 'apPaterno' => '', 'apMaterno' => ''];
            }
            $data['carrera_list'] = $this->carrerasModel->findAll();
            $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : '';
            $data['aspirantes'] = $aspirantemodel->buscar($data);
            $data['pager'] = $aspirantemodel->pager;
            $data['menu'] = $this->sesion->get('menu');
            $data['allowEdit'] = ($this->sesion->get('rol_id') == '3' || $this->sesion->get('rol_id') == '1') ? true : false;
            return view('admision/listaaspirantes', $data);
        
    }



    /**
     * retorna la vista principal del menu //alter con lista de aspirantes
     */
    public function BuscarAspirante()
    {
        $curp_aspirante = $this->request->getPost('aspirante-curp');
        if ($curp_aspirante) {
            $aspitantemodel = new AspirantesGeneralesModel();
            $datos = $aspitantemodel->findByCurp($curp_aspirante);
            if ($datos) {
                $info = $this->buscaGenerales($datos->aspirante_id);
                if ($info) {
                    $data  = $info;
                }
                $data['banderaaspirante'] = true;
                $carrerasmodel = new CarrerasModel();
                $data['carreras'] = $carrerasmodel->findAll();
                $etapasmodel = new AspirantesEtapasModel();
                $data['etapas'] = $etapasmodel->findAll();
                $estadosmodel = new EstadosModel();
                $data['estados'] = $estadosmodel->findAll();
                $data['menu'] = $this->sesion->get('menu');
                $data['bandera_rol'] = false;
                if ($this->sesion->get('rol') == 'SERVICIOS ESCOLARES' || $this->sesion->get('rol') == 'ADMINISTRADOR') {
                    $data['bandera_rol'] = true;
                }
                return view('admision/edicionaspirantes', $data);
            } else {
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'No se ha encontrado al aspirante buscado. '
                ];
                $data['mensaje'] = $mensaje;
                return view('admision/listaaspirantes', $data);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No se ha encontrado al aspirante buscado. '
            ];
            $data['mensaje'] = $mensaje;
            return view('admision/listaaspirantes', $data);
        }
    }

    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaGenerales($id_aspirante)
    {
        $datos = [
            'id' => '', 'carrera' => '', 'etapa' => '', 'apPaterno' => '', 'apMaterno' => '', 'nombre' => '', 'curp' => '', 'correo' => '',
            'telefono' => '', 'calle' => '', 'numero' => '', 'numeroInterior' => '', 'cp' => '', 'colonia' => '', 'municipio' => '',
            'estado' => '', 'pais' => '', 'nacimientoFecha' => '', 'nacimientoMunicipio' => '', 'nacimientoEstado' => '', 'nacimientoPais' => '', 'enviado' => '0',
            'sexo' => '', 'estatus' => '',
        ];
        $aspirantesModel = new AspirantesGeneralesModel();
        // echo($id_aspirante);
        $aspirante = $aspirantesModel->where('aspirante_id', $id_aspirante)->find();
        if (!is_null($aspirante)) {
            //var_dump($aspirante);
            foreach ($aspirante as $info_asp) {
                # code...
                $datos = [
                    'id'                    => $info_asp->folio,
                    'carrera'               => $info_asp->carrera,
                    'etapa'                 => ((is_null($info_asp->etapa)) ? '' : $info_asp->etapa),
                    'apPaterno'             => $info_asp->apPaterno,
                    'apMaterno'             => $info_asp->apMaterno,
                    'nombre'                => $info_asp->nombre,
                    'curp'                  => $info_asp->curp,
                    'correo'                => $info_asp->correo,
                    'telefono'              => $info_asp->telefono,
                    'sexo'                  => $info_asp->sexo,
                    'estatus'               => $info_asp->estatus,
                    'calle'                 => ((is_null($info_asp->calle)) ? '' : $info_asp->calle),
                    'numero'                => ((is_null($info_asp->numero)) ? '' : $info_asp->numero),
                    'numeroInterior'        => ((is_null($info_asp->num_int)) ? '' : $info_asp->num_int),
                    'cp'                    => ((is_null($info_asp->cp)) ? '' : $info_asp->cp),
                    'colonia'               => ((is_null($info_asp->colonia)) ? '' : $info_asp->colonia),
                    'municipio'             => ((is_null($info_asp->municipio)) ? '' : $info_asp->municipio),
                    'estado'                => ((is_null($info_asp->estado)) ? '' : $info_asp->estado),
                    'pais'                  => ((is_null($info_asp->pais)) ? '' : $info_asp->pais),
                    'nacimientoFecha'       => ((is_null($info_asp->nac_fecha)) ? '' : $info_asp->nac_fecha),
                    'nacimientoMunicipio'   => ((is_null($info_asp->nac_municipio)) ? '' : $info_asp->nac_municipio),
                    'nacimientoEstado'      => ((is_null($info_asp->nac_estado)) ? '' : $info_asp->nac_estado),
                    'nacimientoPais'        => ((is_null($info_asp->nac_pais)) ? '' : $info_asp->nac_pais),
                ];
            }
        }
        return $datos;
    }

    /**
     * funcion que busca los municipios y los retorna en tipo JSON
     * @param estado obtiene el estado seleccionado desde la vista
     * @return municipios de acuerdo al estado seleccionado
     */
    public function getmunicipioscct()
    {
        $municipiosmodel = new MunicipiosModel();
        $estado = $this->request->getPost('estado');
        $municipios = $municipiosmodel->getByEstadosmunicipio($estado);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($municipios);
    }
    /**
     * funcion que busca los municipios y los retorna en tipo JSON
     * @param estado obtiene el estado seleccionado desde la vista
     * @return municipios de acuerdo al estado seleccionado
     */
    public function getmunicipios_nac_cct()
    {
        $municipiosmodel = new MunicipiosModel();
        $estado = $this->request->getPost('estado_nac');
        $municipios = $municipiosmodel->getByEstadosmunicipio($estado);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($municipios);
    }

    /**
     * funcion que devuelve un JSON de escuelas correspondientes al municipio
     * @param municipio correspondiente al id del municipio 
     * @return ccts retorna en tipo json los registros correspondientes al municipio
     */
    public function getccts()
    {
        $cctsmodel = new CCTSModel();
        $municipio_id = $this->request->getPost('municipio');
        $municipiosmodel = new MunicipiosModel();
        $municipio = $municipiosmodel->find($municipio_id);
        $municipio_nombre = strtoupper($municipio->nombre);
        $ccts = $cctsmodel->getcctsbymunicipio($municipio_nombre);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($ccts);
    }

    /**
     * Modificacion de los datos del aspirante
     */
    public function ModificarAspirante()
    {
        $municipiomodel = new MunicipiosModel();
        $estadosmodel = new EstadosModel();
        $aspirante =  $this->request->getPost();
        if ($aspirante) {
            $aspirantemodel = new AspirantesGeneralesModel();
            $aspirante_datos = $aspirantemodel->find($aspirante['id_aspirante']);
            if ($aspirante_datos) {
                /* var_dump($aspirante);
                die(); */
                $aspirante_datos->estatus =     ($aspirante_datos->estatus == 'REGISTRO') ? $aspirante_datos->estatus :  'OBSERVACIONES';
                /* echo $aspirante['etapa']." ".$aspirante_datos->etapa . " ". $aspirante_datos->estatus;
                die(); */
                $aspirante_datos->etapa =       (is_null($aspirante['etapa']) || $aspirante['etapa'] == '') ? $aspirante_datos->etapa :  $aspirante['etapa'];
                $aspirante_datos->nombre =      (is_null($aspirante['aspirante-nombre'])) ? $aspirante_datos->nombre :  $aspirante['aspirante-nombre'];
                $aspirante_datos->apPaterno =   (is_null($aspirante['aspirante-appaterno'])) ? $aspirante_datos->appaterno :  $aspirante['aspirante-appaterno'];
                $aspirante_datos->apMaterno =   (is_null($aspirante['aspirante-apmaterno'])) ? $aspirante_datos->apMaterno :  $aspirante['aspirante-apmaterno'];
                $aspirante_datos->correo =      (is_null($aspirante['aspirante-correo'])) ? $aspirante_datos->correo :  $aspirante['aspirante-correo'];
                $aspirante_datos->telefono =    (is_null($aspirante['aspirante-telefono'])) ? $aspirante_datos->telefono :  $aspirante['aspirante-telefono'];
                $aspirante_datos->calle =       (is_null($aspirante['aspirante-calle'])) ? $aspirante_datos->calle :  $aspirante['aspirante-calle'];
                $aspirante_datos->numero =      (is_null($aspirante['aspirante-numero'])) ? $aspirante_datos->numero :  $aspirante['aspirante-numero'];
                $aspirante_datos->num_int =     (is_null($aspirante['aspirante-numeroint'])) ? $aspirante_datos->num_int :  $aspirante['aspirante-numeroint'];
                $aspirante_datos->cp =          (is_null($aspirante['aspirante-cp'])) ? $aspirante_datos->cp :  $aspirante['aspirante-cp'];
                $aspirante_datos->pais =        (is_null($aspirante['pais'])) ? $aspirante_datos->pais :  $aspirante['pais'];
                $aspirante_datos->estatus =    (!isset($aspirante['switch_status'])) ? $aspirante_datos->estatus : 'REVISION';

                /*     echo $aspirante_datos->estatus."<br>";
                echo  (!isset($aspirante['switch_status']))?$aspirante_datos->estatus:'ESPERA';
                echo  "<br>";
die(); */


                $estado = $estadosmodel->find($aspirante['estado']);
                $aspirante_datos->estado =      (is_null($estado)) ? $aspirante_datos->estado : $estado->id;

                if (isset($aspirante['municipio'])) {
                    $municipio = $municipiomodel->find($aspirante['municipio']);
                    $aspirante_datos->municipio =   (is_null($municipio)) ? $aspirante_datos->municipio : $municipio->id;
                } else {
                    $aspirante_datos->municipio =  $aspirante_datos->municipio;
                }
                $aspirante_datos->sexo =        (is_null($aspirante['aspirante-sexo'])) ? $aspirante_datos->sexo : $aspirante['aspirante-sexo'];
                $aspirante_datos->nac_fecha =   (is_null($aspirante['aspirante-nac'])) ? $aspirante_datos->nac_fecha : $aspirante['aspirante-nac'];
                if (isset($aspirante['municipio_nac'])) {
                    $municipionac = $municipiomodel->find($aspirante['municipio_nac']);
                    $aspirante_datos->nac_municipio = (is_null($aspirante['municipio_nac'])) ? $aspirante_datos->nac_municipio :  $municipionac->id;
                } else {
                    $aspirante_datos->nac_municipio = $aspirante_datos->nac_municipio;
                }
                $estadonac = $estadosmodel->find($aspirante['estado_nac']);
                $aspirante_datos->nac_estado =  (is_null($estadonac)) ? $aspirante_datos->nac_estado :  $estadonac->id;
                $aspirante_datos->nac_pais =    (is_null($aspirante['pais_nac'])) ? $aspirante_datos->nac_pais : $aspirante['pais_nac'];
                try {
                    $aspirantemodel->update($aspirante['id_aspirante'], $aspirante_datos);
                    $mensaje = [
                        'tipo' => 'alert-success',
                        'texto' => 'El aspirante se modificó con éxito.'
                    ];
                    log_message('info', 'se ha modificado al aspirante con el id: ' . $aspirante_datos->aspirante_id . ' modificado por:' . $this->sesion->get('id'), []);
                } catch (\Exception $ex) {
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                    $mensaje = [
                        'tipo' => 'alert-danger',
                        'texto' => 'El aspirante no pudo ser modificado. '
                    ];
                }
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'No se pudo encontrar datos del aspirante. '
            ];
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        // return $this->ListadoAspirantes($mensaje);
        return redirect()->to('/ControlAdmision/ListadoAspirantes/');
    }

    /**
     * elimina al aspirante seleccionado
     * @param folio corresponde al folio unico del aspirante
     */
    public function EliminarAspirante()
    {
        $folio = $this->request->getPost('bandera-eliminar');
        try {
            $this->aspirantesModel->delete($folio);
            $mensaje = [
                'tipo' => 'alert-success',
                'texto' => 'El aspirante con folio ' . $folio . ' se eliminó con éxito.'
            ];
            log_message('info', 'Se ha eliminado al aspirante con el id: ' . $folio . ' eliminado por:' . $this->sesion->get('correo'), []);
        } catch (\Exception $ex) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => 'El aspirante con folio ' . $folio . ' no pudo ser eliminado. '
            ];
        }

        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/ControlAdmision/ListadoAspirantes/');
    }
    /**
     * @param id_tipo identificador de la tabla de tipos permitidos
     * @return result los datos para la generacion del select en la vista de items
     */
    public function gettipodatos()
    {
        $id_tipo = $this->request->getPost('selector-tipo');
        $tiposmodel = new AspirantesTiposCampoModel();
        $result = $tiposmodel->find($id_tipo);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($result->tipocampo_clasificacion);
    }

    /**
     * despliega la vista de las etapas en las que se ecuentran los aspirantes
     * @param id_aspirante folio del aspirante en revision 
     * @param mensaje si obtiene un mensaje de proceso este sera enviado a la vista principal
     */
    public function EtapasAspirantes($id_aspirante = null, $mensajes = null, $menu = null)
    {
        $data['mensaje'] = '';
        if ($id_aspirante === null) {
            $id_aspirante = $this->request->getPost('aspirante-id');
        }
        $aspirante_respuestas = new AspiranteRespuestasModel();
        $aspirante_datos = $aspirante_respuestas->getDocumentosAspirantes($id_aspirante);
        $mensaje_estatus = '';
        $aspirante_etapa = new AspirantesGeneralesModel();
        $aspirante = $aspirante_etapa->find($id_aspirante);
        if ($aspirante) {
            if ($aspirante->aspirante_estatus == 'REVISION') {
                $mensaje_estatus = [
                    'tipo' => 'alert-info',
                    'texto' => 'El aspirante se ancuentra actualmente en revision por otro usuario . '
                ];
                $data['mensaje'] = $mensaje_estatus;
            }
            if ($aspirante->aspirante_estatus == 'ESPERA') {
                $aspirante->aspirante_estatus = 'REVISION';
                $aspirante_etapa->update($id_aspirante, $aspirante);
            }
        }
        $bandera = $this->sesion->getFlashdata('mensaje');
        if ($bandera) {

            switch ($bandera) {
                case '1':
                    $data['mensaje'] = [
                        'tipo' => 'alert-success',
                        'texto' => 'Se ha guardado los comentarios ingresados .'
                    ];
                    break;
                case '2':
                    $data['mensaje'] = [
                        'tipo' => 'alert-danger',
                        'texto' => 'Ha habido un problema para guardar los comentarios.'
                    ];
                    break;
                case '3':
                    $data['mensaje']  = [
                        'tipo' => 'alert-info',
                        'texto' => 'No hay comentarios que agregar.'
                    ];
                    break;
            }
        }
        // $data['mensaje'] = ($mensajes) ? $mensajes : '';
        $etpasmodel = new AspirantesEtapasModel();
        $camposModel = new AspirantesCamposModel();
        $tiposCamposModel = new AspirantesTiposCampoModel();
        $respModel = new AspiranteRespuestasModel();
        $data['campos_completos'] = $camposModel->getAllOrderByFormulario();
        $data['tiposCampos'] = [];
        $tiposCampo = $tiposCamposModel->findAll();
        foreach ($tiposCampo as $tipoCampo) {
            $data['tiposCampos'][$tipoCampo->id] = $tipoCampo;
        }
        $resp = $respModel->getAspiranteRespuestas($id_aspirante);
        $respuestas = [];
        $data['bandera_comentarios'] = false;
        foreach ($resp as $respuesta) {
            $respuestas[$respuesta->campo] = $respuesta->valor;
            if ($respuesta->comentario != null) $data['bandera_comentarios'] = true;
        }
        $etapas = $this->etapasModel->getEtapasDocumentales();
        $data['apirante'] = $aspirante;
        $data['etapas_documentales'] = ($etapas) ? $etapas : '';
        $data['respuestas_bss'] =  $respuestas;
        $data['etapas_formularios'] = $etpasmodel->getEtapasFormularios();
        $data['etapas_totales'] = $etpasmodel->findAll();
        $data['respuestas'] = $aspirante_datos;
        if ($menu) {
            $data['menu'] = $menu;
        } else {

            $data['menu'] = $this->sesion->get('menu');
        }
        $data['campos_visibles'] = $this->respuestasModel->getCamposVisibles($id_aspirante);
        // var_dump($data['campos_visibles']);
        $arr_documentos = [];
        foreach ($aspirante_datos as $documentos) {
            if ($documentos->clasificacion == 'ARCHIVO' && $documentos->visualizar == 1) {
                $arr_documentos[] = base_url('Admision/renderImg/' . $id_aspirante . '/' . $documentos->valor);
            }
        }
        if ($arr_documentos) {

            $data['campos_documentos'] = $arr_documentos;
        }
        // var_dump($resp);
        return view('admision/aspiranteetapas', $data);
    }

    /**
     * despliega la lista de documentos a revisar de un aspirante
     */

    public function revisionDocumental()
    {
        $id_aspirante = $this->request->getPost('aspirante-id');
        $aspirante_respuestas = new AspiranteRespuestasModel();
        $aspirante_datos = $aspirante_respuestas->getDocumentosAspirantes($id_aspirante);
        $aspirante_etapa = new AspirantesGeneralesModel();
        $etpasmodel = new AspirantesEtapasModel();
        $data['contadores'] = "";
        $data['etapas_formularios'] = $etpasmodel->getEtapasFormularios();
        $data['etapas_totales'] = $etpasmodel->findAll();
        $data['respuestas'] = $aspirante_datos;
        $data['datos'] = $aspirante_etapa->listaEtapas($id_aspirante);
        $data['menu'] = $this->sesion->get('menu');
        return view('Admision/revisionDocumental', $data);
    }


    /**
     * obtiene la ruta para abrir un archivo y proyectarlo 
     * @param id_aspirante apunta a la carpeta propia del aspirtante
     * @param documento nombre del documento solicitado para desplegar
     * @return documento 
     */
    public function documentoPreview($id_aspirante, $documento)
    {

        $documento = WRITEPATH . 'admision/2022A/' . $id_aspirante . '/' . $documento;
        //var_dump($documento);
        if (file_exists($documento)) {
            if ($file = fopen($documento, 'r')) {
                $partes_ruta = pathinfo($documento);
                $partes_ruta['extension'] = strtolower($partes_ruta['extension']);
                if ($partes_ruta['extension'] == 'pdf') header('Content-Type: application/pdf');
                if ($partes_ruta['extension'] == 'jpg') header('Content-Type: image/jpg');
                if ($partes_ruta['extension'] == 'jpeg') header('Content-Type: image/jpg');
                if ($partes_ruta['extension'] == 'png') header('Content-Type: image/png');
                // else 
                readfile($documento);
                exit;
            }
        } else {
            return 'Lo sentimos no hemos encontrado el archivo buscado';
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }
    /**
     * Obtiene los comentarios escritos y los asigna como notificaciones 
     * @param comentarios_doc arreglo de comentarios correspondientes a los documentos
     * @param id_aspirante folio perteneciente al aspirante en revision 
     * @param documento lista de respuestas de tipo documento
     * @return mensaje devuelve el mensaje dependiendo si ha logrado agregar los comentarios o no
     */
    public function guardarComentarios()
    {
        $mensaje = '';
        $bandera = '';
        $id_aspirante = $this->request->getPost('id_aspirante');
        $comentarios_doc = $this->request->getPost('coment_doc');
        $documento =  $this->respuestasModel->getDocumentosAspirantes($id_aspirante);
        $aspirante_respuesta = new AspirantesRespuestasModel();
        if ($comentarios_doc) {
            $aux = 0;
            foreach ($documento as $key) {
                /* if ($comentarios_doc[$aux] != '') { */
                try {
                    $respuesta = $aspirante_respuesta->find($key->respuesta_id);
                    // echo " $comentarios_doc[$aux] != $respuesta->respuesta_comentario";
                    if ($comentarios_doc[$aux] != $respuesta->respuesta_comentario) {
                        $respuesta->respuesta_comentario = ($comentarios_doc[$aux] == '') ? null : $comentarios_doc[$aux];
                        $aspirante_respuesta->update($key->respuesta_id, $respuesta);
                    }
                    $mensaje = [
                        'tipo' => 'alert-success',
                        'texto' => 'Se ha guardado los comentarios ingresados .'
                    ];
                    $bandera = 1;
                    //echo "$key->respuesta_id::$respuesta->respuesta_comentario ";
                } catch (\Exception $ex) {
                    // var_dump($respuesta);
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                    $mensaje = [
                        'tipo' => 'alert-danger',
                        'texto' => 'Ha habido un problema para guardar los comentarios.'
                    ];
                    $bandera = 2;
                }
                /*    }*/
                $aux++;
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => 'No hay comentarios que agregar.'
            ];
            $bandera = 3;
        }
        log_message('notice', 'Aspirante actualizado: ' . $id_aspirante . ' por ' . $this->sesion->get('useremail'));
        $this->sesion->setFlashdata('mensaje', $bandera);
        return redirect()->to('/ControlAdmision/EtapasAspirantes/' . $id_aspirante);
    }


    /**
     * Crea las alertas de los campos con comentarios
     * @param id_aspirante folio del aspirante correspondiente
     * @param respuestas arreglo de todas las respuestas del aspirante
     */

    public function generarAlertas()
    {
        $alerta_movimiento = '';
        $id_aspirante = $this->request->getPost('folio');
        $etapa = $this->request->getPost('etapas_disponibles');
        $notificacionent = new AspirantesNotificaciones();
        //$this->aspirantesModel->find($id_aspirante);
        $arr_mensajes = [];
        $respuestas = $this->respuestasModel->getAspiranteRespuestas($id_aspirante);
        //---------------
        $aspirante = $this->aspirantesModel->find($id_aspirante);
        $aspirante_comparar = $this->aspirantesModel->find($id_aspirante);
        $aspirante->etapa = $etapa;
        $aspirante->estatus = 'OBSERVACIONES';
        if ($aspirante_comparar != $aspirante) {
            try {
                $this->aspirantesModel->update($id_aspirante, $aspirante);
                log_message('notice', 'Actualizacion de estatus a OBSERVACIONES del aspirante: ' . $id_aspirante . ' por ' . $this->sesion->get('useremail'));
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            }
        }
        //---------------
        if ($respuestas) {
            $notificaiones = $this->notificacionesModel->getNotificacionesAspirante($id_aspirante);
            if ($notificaiones) {
                foreach ($notificaiones as $notificacion) {
                    $this->notificacionesModel->delete($notificacion->id);
                }
            }
            $contador = 0;
            foreach ($respuestas as $respuesta) {
                // si la respuesta cuenta con comentarios genera la notificacion 
                if ($respuesta->comentario && $respuesta->comentario != ' ') {
                    // echo $contador++."<br>";

                    $formulario = $this->camposModel->find($respuesta->campo);
                    $arr_mensajes[] = strtoupper($respuesta->campo_nombre) . " : " . $respuesta->comentario;
                    $mensaje =  strtoupper($respuesta->campo_nombre) . " : " . $respuesta->comentario;
                    $notificacionent->aspirante = $id_aspirante;
                    $notificacionent->mensaje = $mensaje;
                    $notificacionent->formulario = $formulario->formulario;
                    try {

                       
                        $this->notificacionesModel->insert($notificacionent);

                        $alerta_movimiento = [
                            'tipo' => 'alert-success',
                            'texto' => 'Se han generado las alertas para el aspirante en su seccion de notificaciones.'
                        ];
                        log_message('notice', 'Envío de notificaciones para: ' . $id_aspirante . ' por ' . $this->sesion->get('useremail'));
                    } catch (\Exception $ex) {
                        $bandera = 2;
                        log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                        $alerta_movimiento = [
                            'tipo' => 'alert-danger',
                            'texto' => 'Se ha generado un error que no permitió la generación de la alerta para el aspirante.'
                        ];
                    }
                }
            }
        }
        /* if (count($arr_mensajes) != 0) {
            $this->correoNotificaciones($aspirante, $arr_mensajes);
        } */
        //return  $this->ListadoAspirantes($bandera);

        $this->sesion->setFlashdata('datos', $alerta_movimiento);
        // var_dump($alerta_movimiento);
        //die();
        return redirect()->to('/ControlAdmision/ListadoAspirantes/');
    }

    /**
     * envia el correo para notificar los comentarios en su proceso de revision
     * @param aspirante el objeto de tipo AspiranteEitity con los datos del registro generado
     * @return void
     */
    private function correoNotificaciones($aspirante, $arr_mensajes)
    {
        $destinatario = $aspirante->correo;
        $data['notificaciones'] = $arr_mensajes;
        $data['nombre'] = $aspirante->nombre;
        $data['enlace'] = base_url('Admision');
        $mensaje = view('admision/correonotificacion', $data);
        $asunto = 'Notificaciones sobre tu proceso de admisión de la Universidad de la Salud.';
        $correoDe = 'admision@usalud.edu.mx';
        $nombreDe = 'Admisión - Universidad de la Salud';
        $this->correo($destinatario, $mensaje, $asunto, $correoDe, $nombreDe);
    }

    /**
     * funcion para terminar proceso de revision del aspirante 
     * @param folio folio del aspirante en revision
     * @param 
     * 
     */
    public function terminarProceso()
    {
        $mensaje = [
            'tipo' => 'alert-danger',
            'texto' => 'No se ha encontrado el aspirante.'
        ];
        $estatus_anterior = '';
        $folio = $this->request->getPost('folio');
        $bandera = $this->request->getPost('bandera');
        $aspirante = $this->aspirantesModel->find($folio);
       
        if ($aspirante) {
            try {
                $estatus_anterior = $aspirante->estatus;
                $aspirante->estatus = strtoupper($bandera);
                $aspirante->etapa = null;
                $this->aspirantesModel->update($folio, $aspirante);
                if($aspirante->estatus == 'EXITOSO'){
                    $this->deleteComentarios($folio);
                }
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se ha actualizado el estatus del aspirante ' . $aspirante->curp . ' de ' . $estatus_anterior . ' a ' . $aspirante->estatus . ','
                ];
                log_message('notice', 'Se ha actualizado el estatus del aspirante ' . $aspirante->folio . ' de ' . $estatus_anterior . ' a ' . $aspirante->estatus . ' por el usuario ' . $this->sesion->get('useremail'));
            } catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);

                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => 'Se ha generado un error que no permitió la actualizacioon del estatus del aspirante.'
                ];
            }
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to(base_url() . '/ControlAdmision/ListadoAspirantes');
    }

    /**
     * borra los comentarios hechos en los archivos del aspirante
     * @param aspirante id del aspirante
     */
    public function deleteComentarios($aspirante)
    {
        $respuestas =  $this->respuestaTModel->getRespuestas( $aspirante);
        foreach ($respuestas as $respuesta) {
            if($respuesta->comentario){
                $respuesta->comentario = null;
                $this->respuestaTModel->update($respuesta->respuesta,$respuesta);
            }
        }
    }

    /**
     * retorna arreglo de comentarios en formato json
     * @param id_comentario 
     * @param 
     */
    public function getComentarios($id_comentario = null)
    {
        if ($id_comentario) {
            $comentarios = $this->comentariosModel->getComentarios($id_comentario);
            if ($comentarios) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($comentarios);
            } else {
                return null;
            }
        }
    }
    /**
     * actualiza los datos del campo correspondiente
     * @param respuesta corresponde al id de la respuesta
     * @param valor corresponde al valor que se actualizara en la tabla
     * @return esatus si la actualizacion fue correcta o no
     */
    public function UpdateCampo($respuesta, $valor)
    {
        $dato_respuesta  = $this->respuestaTModel->find($respuesta);
        if ($dato_respuesta) {
            $campo = $this->camposModel->find($dato_respuesta->campo);
            $validador = new RespuestaValidacion();
            // echo json_encode($validador);
            if ($validador->do($campo, $valor)) {

                try {
                    $dato_respuesta->valor = (($valor) ? $valor : $dato_respuesta->valor);

                    $this->respuestaTModel->update($respuesta, $dato_respuesta);
                    log_message('info', 'se ha modificado al aspirante con el id: ' . $dato_respuesta->dato_respuesta . ' modificado por:' . $this->sesion->get('id'), []);
                    $this->response->setHeader('Content-Type', 'application/json');
                    echo json_encode('guardado');
                } catch (\Exception $ex) {
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                    // return false;
                    return null;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * renderiza toda la información contestada por los aspirantes que han validado su correo
     * @todo agregar el periodo
     */
    public function informacionGeneral()
    {
        $informacionAspirantes = new \App\Libraries\InformacionAspirantes();
        $filename = 'Bettdecke_admision2022A_' . date("YmdHis") . '.csv';
        $this->response->setHeader('Content-type', 'application/csv');
        $this->response->setHeader('Content-Disposition', 'attachment;filename=' . $filename);
        $info = $informacionAspirantes->full();
        $rows = count($info);
        $cols = count($info[0]);
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                if (array_key_exists($j, $info[$i])) {
                    echo utf8_decode(str_replace(',', '.', strip_tags($info[$i][$j]))) . ',';
                } else {
                    echo ',';
                }
            }
            echo "\n";
        }
    }

    /**
     * despliega vista para el cambio de estatus del aspirante
     */
}
