<?php

namespace App\Entities;

use CodeIgniter\Entity;

class FiltroExternos extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'dependencia' => null,
        'rango_edad' => null,

        'nombre' => null,

        'correo' => null,
        'sexo' => null,
        'folio' => null,
        'diabetes' => null,
        'hipertension' => null,
        'cardiacos' => null,
        'enfermedadRespiratoria' => null,
        'otroPadecimiento' => null,
        'esFumador' => null,
        'fiebre' => null,
        'tos' => null,
        'dificultadRespiratoria' => null,
        'dolorCabeza' => null,
        'dolorMuscular' => null,
        'dolorArticular' => null,
        'fatiga' => null,
        'dolorAbdominal' => null,
        'alteracionGusto' => null,
        'alteracionOlfato' => null,
        'escurrimientoNasal' => null,
        'conjuntivitis' => null,
        'escalofrios' => null,
        'estuvoViaje' => null,
        'contactoPersonaCovid' => null,
        'temperatura' => null,
        'oxigenacion' => null,
        'fecha'       => null,
        'vacuna' => null,
        'tipotos' => null

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'dependencia' => 'dependencia',
        'rango_edad' => 'rango_edad',
        'nombre' => 'nombre',
        'correo' => 'correo',
        'sexo' => 'sexo',
        'folio' => 'folio',
        'diabetes' => 'diabetes',
        'hipertension' => 'hipertension',
        'cardiacos' => 'cardiacos',
        'enfermedadRespiratoria' => 'enfermedadRespiratoria',
        'otroPadecimiento' => 'otroPadecimiento',
        'esFumador' => 'esFumador',
        'fiebre' => 'fiebre',
        'tos' => 'tos',
        'dificultadRespiratoria' => 'dificultadRespiratoria',
        'dolorCabeza' => 'dolorCabeza',
        'dolorMuscular' => 'dolorMuscular',
        'dolorArticular' => 'dolorArticular',
        'fatiga' => 'fatiga',
        'dolorAbdominal' => 'dolorAbdominal',
        'alteracionGusto' => 'alteracionGusto',
        'alteracionOlfato' => 'alteracionOlfato',
        'escurrimientoNasal' => 'escurrimientoNasal',
        'conjuntivitis' => 'conjuntivitis',
        'escalofrios' => 'escalofrios',
        'estuvoViaje' => 'estuvoViaje',
        'contactoPersonaCovid' => 'contactoPersonaCovid',
        'temperatura' => 'temperatura',
        'oxigenacion' => 'oxigenacion',
        'fecha' =>         'fecha',
        'vacuna' => 'vacuna',
        'tipotos' => 'tipo_tos'
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
