CREATE TABLE `usep_aplicativos`.`filtro_preguntas` (
    `filtropreg_id` INT UNSIGNED NOT NULL  AUTO_INCREMENT , 
    `filtropreg_pregunta` VARCHAR(1000) NOT NULL  , 
    `filtropreg_opciones` VARCHAR(1000) NOT NULL , 
    `filtropreg_activa` VARCHAR(1) NOT NULL , 
    `created_at` DATE NOT NULL , 
    `updated_at` DATE NOT NULL , 
    `deleted_at` DATE NOT NULL , 
    PRIMARY KEY (`filtropreg_id`)
    
) ENGINE = InnoDB;



-- ----------------------------------------------------------------------------------------------------------
-- VALIDAR EL COLLATE DE LA TABLA ANTES DE SU CREACIÓN
-- ----------------------------------------------------------------------------------------------------------
CREATE TABLE `filtro_folios` (
    `filtrofolio_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
    `filtrofolio_usuario` VARCHAR(60) NULL , 
    `filtrofolio_usuario_ext` VARCHAR(60) NULL , 
    `created_at` DATE NOT NULL , 
    `updated_at` DATE NOT NULL , 
    `deleted_at` DATE NOT NULL , 
    PRIMARY KEY (`filtrofolio_id`),
    FOREIGN KEY fk_foliousuariofl(`filtrofolio_usuario`) REFERENCES  aplicativo_usuarios (`usuario_correo`),
    FOREIGN KEY fk_foliousuarioflext(`filtrofolio_usuario_ext`) REFERENCES  filtro_usuarios_ext (`externo_correo`)
)COLLATE=utf8mb4_general_ci;


CREATE TABLE `usep_aplicativos`.`filtro_respuestas` (
    `filtroresp_id` INT UNSIGNED NOT NULL  AUTO_INCREMENT, 
    `filtroresp_pregunta` INT UNSIGNED NOT NULL  , 
    `filtroresp_folio` INT UNSIGNED NOT NULL  , 
    `filtroresp_respuesta` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`filtroresp_id`),
    FOREIGN KEY (`filtroresp_pregunta`) REFERENCES filtro_preguntas(filtropreg_id),
    FOREIGN KEY (`filtroresp_folio`) REFERENCES filtro_folios(filtrofolio_id)
);



ALTER TABLE `filtro_preguntas` CHANGE `created_at` `created_at` DATE NULL, CHANGE `updated_at` `updated_at` DATE NULL, CHANGE `deleted_at` `deleted_at` DATE NULL;
ALTER TABLE `filtro_folios` CHANGE `created_at` `created_at` DATE NULL, CHANGE `updated_at` `updated_at` DATE NULL, CHANGE `deleted_at` `deleted_at` DATE NULL;


INSERT INTO `filtro_preguntas` (`filtropreg_id`, `filtropreg_pregunta`, `filtropreg_opciones`, `filtropreg_activa`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'INFORME DE VACUNACIÓN SARS-COV-2 (COVID-19)', '1a. dosis,2a. dosis,3a dosis,Refuerzo,Prefiero no contestar', '1', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Fiebre mayor o igual a 38°C', 'SI,NO', '1', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dificultad para respirar (disnea)', 'SI,NO', '1', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Tos', 'SI,NO', '1', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dolor de cabeza (cefalea)', 'SI,NO', '1', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dolor en articulaciones (artralgias)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dolor muscular (mialgias)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dolor de garganta (odinofagia)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Escalofríos', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Dolor en pecho (torácico)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Escurrimiento nasal (rinorrea)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Conjuntuvitis', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Incrementa la profundidad y frecuencia respiratoria (polipnea)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Pérdida de la capacidad de oler (anosmia)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24'), (NULL, 'Pérdida del sentido del gusto (disgeusia)', 'SI,NO', '0', '2022-05-24', '2022-05-24', '2022-05-24');

ALTER TABLE `filtro_preguntas` CHANGE `filtropreg_activa` `filtropreg_mayor` VARCHAR(1) NOT NULL;

UPDATE `filtro_preguntas` SET deleted_at=NULL;

ALTER TABLE `filtro_folios` CHANGE `created_at` `created_at` DATETIME NULL, CHANGE `updated_at` `updated_at` DATETIME NULL, CHANGE `deleted_at` `deleted_at` DATETIME NULL;


ALTER TABLE `filtro_usuarios_ext` CHANGE `update_at` `updated_at` DATE NULL DEFAULT NULL;

CREATE TABLE `usep_aplicativos`.`filtro_accesos` (
    `filtroacceso_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
    `filtroacceso_folio` INT UNSIGNED NOT NULL , 
    `filtroacceso_temperatura` INT NULL , 
    `filtroacceso_oximetria` INT NULL , 
    `created_at` DATETIME NULL , 
    `updated_at` DATETIME NULL , 
    `deleted_at` DATETIME NULL , 
    PRIMARY KEY (`filtroacceso_id`),
    FOREIGN KEY (`filtroacceso_folio`) REFERENCES filtro_folios(filtrofolio_id)
) ENGINE = InnoDB;


ALTER TABLE `filtro_preguntas` CHANGE `filtropreg_mayor` `filtropreg_mayor` TINYINT(1) NOT NULL COMMENT '-1 para primeras preguntas, 1 para sintomas mayores, 2 para sintomas menores';
UPDATE `filtro_preguntas` SET filtropreg_mayor=2 WHERE filtropreg_mayor=0;