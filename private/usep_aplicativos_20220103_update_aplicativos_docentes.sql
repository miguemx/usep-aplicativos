INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES 
('100040','marisol.lopez@usep.mx','1','Marisol','López Rivera','López','Rivera',CURRENT_DATE,CURRENT_DATE),
('100041','kirsten.vargas@usep.mx','1','Kirsten','Leal Vargas','Leal','Vargas',CURRENT_DATE,CURRENT_DATE),
('100042','manuel.garcia@usep.mx','1','José Manuel','García Páez','García','Páez',CURRENT_DATE,CURRENT_DATE),
('100044','elizabeth.rodriguez@usep.mx','1','Elizabeth Valentina ','Rodríguez Rosas','Rodríguez','Rosas',CURRENT_DATE,CURRENT_DATE)
;

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES 
('100045','noe.velazquez@usep.mx','1','Noe','Velázquez Márquez','Velázquez','Márquez',CURRENT_DATE,CURRENT_DATE),
('100046','jorge.almazan@usep.mx','1','Jorge Fabian','Almazán Torres','Almazán','Torres',CURRENT_DATE,CURRENT_DATE),
('100047','sagrario.lobato@usep.mx','1','Sagrario','Lobato Huerta','Lobato','Huerta',CURRENT_DATE,CURRENT_DATE),
('100050','estefania.martin@usep.mx','1','Estefanía','Martin Zayago','Martin','Zayago',CURRENT_DATE,CURRENT_DATE),
('100052','laura.cristina@usep.mx','1','Laura Cristina','Huelgas Saavedra','Huelgas','Saavedra',CURRENT_DATE,CURRENT_DATE),
('100054','lorena.arroyo@usep.mx','1','Erika Lorena','Arroyo Ríos','Arroyo','Ríos',CURRENT_DATE,CURRENT_DATE),
('100055','maria.villatoro@usep.mx','1','María Del Socorro','Villatoro García','Villatoro','García',CURRENT_DATE,CURRENT_DATE)
;

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('100056','juan.moneda@usep.mx','1','Juan Víctor','Moneda Rovira','Moneda','Rovira',CURRENT_DATE,CURRENT_DATE),
('100061','carlos.luna@usep.mx','1','Carlos Alberto','Luna Ayaquica','Luna','Ayaquica',CURRENT_DATE,CURRENT_DATE),
('100063','adriana.romero@usep.mx','1','Adriana Paulina','Romero López','Romero','López',CURRENT_DATE,CURRENT_DATE),
('100064','miguel.coral@usep.mx','1','Miguel Ángel','Coral García','Coral','García',CURRENT_DATE,CURRENT_DATE),
('100082','angelica.ortiz@usep.mx','1','Angelica María','Ortiz Bueno','Ortiz','Bueno',CURRENT_DATE,CURRENT_DATE),
('100051','victor.sanchez@usep.mx','1','Víctor Hugo','Sánchez Chimeu','Sánchez','Chimeu',CURRENT_DATE,CURRENT_DATE),
('100076','juan.baltazar@usep.mx','1','Juan Pablo','Baltazar Colio','Baltazar','Colio',CURRENT_DATE,CURRENT_DATE),
('100078','gabriela.gomez@usep.mx','1','Gabriela Ollyini','Gómez Rivera','Gómez','Rivera',CURRENT_DATE,CURRENT_DATE)
;

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES 
('100080','yessica.cosme@usep.mx','1','Yessica Ibelith','Cosme Herrera','Cosme','Herrera',CURRENT_DATE,CURRENT_DATE),
('100081','jorge.rodriguez@usep.mx','1','Jorge Alberto','Rodríguez Ventura','Rodríguez','Ventura',CURRENT_DATE,CURRENT_DATE),
('200024','mariana.miguel@usep.mx','1','Mariana Lee','Miguel Sardaneta','Miguel','Sardaneta',CURRENT_DATE,CURRENT_DATE),
('200025','zeniff.gomez@usep.mx','1','Zeniff ','Gómez Arcive','Gómez','Arcive',CURRENT_DATE,CURRENT_DATE),
('200026','ariel.gutierrez@usep.mx','1','Ariel Farit','Gutiérrez Alexander','Gutiérrez','Alexander',CURRENT_DATE,CURRENT_DATE),
('200027','patricia.martinez@usep.mx','1','Patricia Esmeralda','Martínez Hernández','Martínez','Hernández',CURRENT_DATE,CURRENT_DATE),
('200028','carlos.gonzalez@usep.mx','1','Carlos Arturo','González Castañeda','González','Castañeda',CURRENT_DATE,CURRENT_DATE),
('200029','bianey.zamira@usep.mx','1','Bianey Zamira','Espinoza Villalobos','Espinoza','Villalobos',CURRENT_DATE,CURRENT_DATE),
('200030','aurea.vera@usep.mx','1','Aurea ','Vera Loaiza','Vera','Loaiza',CURRENT_DATE,CURRENT_DATE);


INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200031','erika.maldonado@usep.mx','1','Erika Yaredi','Maldonado Romero','Maldonado','Romero',CURRENT_DATE,CURRENT_DATE);

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200033','jesua.bueno@usep.mx','1','Jesua Roberto','Bueno Gasca','Bueno','Gasca',CURRENT_DATE,CURRENT_DATE),
('200034','norma.hernandez@usep.mx','1','Norma Araceli','Hernández Corona','Hernández','Corona',CURRENT_DATE,CURRENT_DATE),
('200035','alma.alcaide@usep.mx','1','Alma Rosa','Alcaide López','Alcaide','López',CURRENT_DATE,CURRENT_DATE),
('200036','fernando.navarro@usep.mx','1','Fernando ','Navarro Tovar','Navarro','Tovar',CURRENT_DATE,CURRENT_DATE);

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200037','larissa.montiel@usep.mx','1','Larissa ','Montiel Sánchez','Montiel','Sánchez',CURRENT_DATE,CURRENT_DATE),
('200038','daniela.cisneros@usep.mx ','1','Daniela ','Cisneros Sánchez','Cisneros','Sánchez',CURRENT_DATE,CURRENT_DATE),
('200039','manuel.gil@usep.mx','1','Manuel ','Gil Vargas','Gil','Vargas',CURRENT_DATE,CURRENT_DATE),
('200040','helios.herrera@usep.mx','1','Helios ','Herrera Aranda','Herrera','Aranda',CURRENT_DATE,CURRENT_DATE),
('200041','elizabeth.garcia@usep.mx','1','Elizabeth ','García Salazar','García','Salazar',CURRENT_DATE,CURRENT_DATE);

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200042','maria.lugo@usep.mx','1','María Laura','Lugo León','Lugo','León',CURRENT_DATE,CURRENT_DATE),
('200043','alfonso.ponce@usep.mx','1','Alfonso Francisco','Ponce Reyes','Ponce','Reyes',CURRENT_DATE,CURRENT_DATE),
('200044','ricardo.martinez@usep.mx','1','Ricardo ','Martínez Zavala','Martínez','Zavala',CURRENT_DATE,CURRENT_DATE),
('200045','mariela.rodriguez@usep.mx','1','Mariela De Jesús','Rodríguez Hernández','Rodríguez','Hernández',CURRENT_DATE,CURRENT_DATE);


INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200047','karen.abonce@usep.mx','1','Karen Selene','Abonce Villagomez','Abonce','Villagomez',CURRENT_DATE,CURRENT_DATE),
('200055','miguel.olmedo@usep.mx','1','Miguel Ángel','Olmedo Suárez','Olmedo','Suárez',CURRENT_DATE,CURRENT_DATE);

INSERT INTO `aplicativo_usuarios` (`usuario_correo`, `usuario_rol`, `usuario_nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES ('adan.zambrano@usep.mx', '5', 'Adán Zambrano Saucedo\r\n', '2022-01-03', '2022-01-03', NULL);

INSERT INTO `aplicativo_empleados`(`empleado_id`, `empleado_correo`, `empleado_horario`,`empleado_nombre`, `empleado_apellido`, `empleado_ap_paterno`, `empleado_ap_materno`, `created_at`, `updated_at`) 
VALUES ('200032','adan.zambrano@usep.mx','1','Adán ','Zambrano Saucedo','Zambrano','Saucedo',CURRENT_DATE,CURRENT_DATE);


UPDATE `aplicativo_empleados` SET `empleado_correo` = 'sergio.benavides@usep.mx', `empleado_nombre` = 'Sergio Adán\r\n', `empleado_apellido` = 'Benavides Suarez\r\n', `empleado_ap_paterno` = 'Benavides', `empleado_ap_materno` = 'Suarez', `created_at` = '2022-01-03', `deleted_at` = NULL WHERE `aplicativo_empleados`.`empleado_id` = 100043;



