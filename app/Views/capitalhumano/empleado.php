<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    

<div class="container" style="margin-top: 15px;">

    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <?php if ( isset($exito) ): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $exito; ?>
        </div>
    <?php endif; ?>

    <form method="post" action="<?php echo base_url('CapitalHumano/Guardar') ?>">
        
        <div class="" style="text-align:right;">
            <a href="<?php echo base_url('CapitalHumano/Personal'); ?>" class="btn btn-danger btn-sm">Regresar</a>
            <button class=" btn btn-secondary btn-sm" type="submit" name="app" value="empleado">Guardar</button>
        </div>

        <div class="mb-3">
            <label for="id" class="form-label">ID</label>
            <input type="text" class="form-control form-control-sm" id="id" name="id" value="<?php echo $empleado->id ?>" <?php if($empleado->id) echo 'readonly="readonly"'; ?> />
        </div>

        <div class="mb-3">
            <label for="id" class="form-label">Correo Institucional</label>
            <input type="text" class="form-control form-control-sm" id="correo" name="correo" value="<?php echo $empleado->correo ?>" <?php if(isset($edit)) echo 'disabled="disabled"' ?> />
        </div>
        
        <div class="mb-3">
            <label for="id" class="form-label">Apellido Paterno</label>
            <input type="text" class="form-control form-control-sm" id="apPaterno" name="apPaterno" value="<?php echo $empleado->apPaterno ?>" />
        </div>

        <div class="mb-3">
            <label for="id" class="form-label">Apellido Materno</label>
            <input type="text" class="form-control form-control-sm" id="apMaterno" name="apMaterno" value="<?php echo $empleado->apMaterno ?>" />
        </div>

        <div class="mb-3">
            <label for="id" class="form-label">Nombre(s)</label>
            <input type="text" class="form-control form-control-sm" id="nombre" name="nombre" value="<?php echo $empleado->nombre ?>" />
        </div>

        <div class="mb-3">
            <label for="id" class="form-label">Horario</label>
            <select class="form-select" name="horario">
                <option value="">Seleccione uno</option>
                <?php foreach( $horarios as $horario ): ?>
                    <option value="<?php echo $horario->id; ?>" <?php if($empleado->horario==$horario->id) echo 'selected="selected"'; ?>><?php echo $horario->nombre; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        
    </form>
    

<?php echo $this->endSection() ?>
