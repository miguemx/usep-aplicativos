CREATE TABLE `usep_aplicativos`.`aspirantes_notificaciones`(
    `notificaciones_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `notificaciones_aspirante` VARCHAR(9) NOT NULL,
    `notificaciones_mensaje` TEXT NOT NULL,
    `notificaciones_leido` INT NULL,
    `created_at` DATETIME NULL,
    `updated_at` DATETIME NULL,
    `deleted_at` DATETIME NULL,
    PRIMARY KEY(`notificaciones_id`),
    FOREIGN KEY(`notificaciones_aspirante`) REFERENCES `aspirantes_generales`(`aspirante_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = INNODB;