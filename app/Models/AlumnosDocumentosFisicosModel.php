<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AlumnosDocumentosFisicosModel extends Model {
    protected $table      = 'alumnos_documentos_fisicos';
    protected $primaryKey = 'aludocfis_id';

    protected $returnType    = 'App\Entities\AlumnoDocumentoFisico';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'aludocfis_id','aludocfis_acta','aludocfis_curp','aludocfis_identificacion',
        'aludocfis_cert','aludocfis_cert_legal','aludocfis_constancia','aludocfis_kardex'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

}