Drop view view_listagrupos_2022b;
CREATE
 VIEW `view_listagrupos` AS
SELECT
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno` AS `gpoalumno_alumno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno` AS `Ap_paterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno` AS `Ap_materno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres` AS `Alumno_nombres`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` AS `gpoalumno_grupo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `docente_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `docente_ap_materno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `docente_nombre`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`alumnos_generales`.`created_at` AS `created_at`,
    `usep_aplicativos`.`alumnos_generales`.`deleted_at` AS `deleted_at`,
    `usep_aplicativos`.`alumnos_generales`.`updated_at` AS `updated_at`
FROM
    (
        (
            (
                (
                    (
                        `usep_aplicativos`.`aca_grupos_alumnos`
                    JOIN `usep_aplicativos`.`aca_grupos` ON
                        (
                            `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` = `usep_aplicativos`.`aca_grupos`.`grupo_id`
                        )
                    )
                JOIN `usep_aplicativos`.`aca_periodos` ON
                    (
                        `usep_aplicativos`.`aca_grupos`.`grupo_periodo` = `usep_aplicativos`.`aca_periodos`.`periodo_id`
                    )
                )
            JOIN `usep_aplicativos`.`aca_materias` ON
                (
                    `usep_aplicativos`.`aca_grupos`.`grupo_materia` = `usep_aplicativos`.`aca_materias`.`materia_clave`
                )
            )
        JOIN `usep_aplicativos`.`alumnos_generales` ON
            (
                `usep_aplicativos`.`alumnos_generales`.`alumno_id` = `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno`
            )
        )
    LEFT JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` = `usep_aplicativos`.`aca_grupos`.`grupo_docente`
        )
    )
where `usep_aplicativos`.`alumnos_generales`.`deleted_at`is Null
ORDER BY
    `usep_aplicativos`.`aca_materias`.`materia_carrera`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_id`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres`