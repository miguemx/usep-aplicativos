<?php echo $this->extend('plantillabase'); ?>
<?php echo $this->section('content') ?>
<style>
	* {
		padding: 0;
		margin: 0;
		box-sizing: border-box;
	}

	body {
		background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
		background-size: 794px 1100px;
		background-repeat: no-repeat;
		background-position: left;
		background-color: #dcdcdc;
		min-height: 100vh;
		min-width: 99vw;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 50px 5px;
		font-family: 'Open Sans', sans-serif;
		/*filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/
	}

	.contenedor-cuestionario {
		max-width: 1050px;
	}

	tbody {
		text-align: left;
	}
	.vacuna{
		border: 5px solid #000;
		border-color: red;
	}
	.trfechas{
		display:none;
	}
</style>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  	<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
	<script>
		$(function() {
		var $j = jQuery.noConflict();
		//$j("#datepicker").datepicker();
		$("#date12").datepicker({dateFormat:'yy/mm/dd'});
		$("#date13").datepicker({dateFormat:'yy/mm/dd'});
		$("#date14").datepicker({dateFormat:'yy/mm/dd'});
		});
	</script>
</head>
<script>
	function mostrarelementos(valor,id,idfecha) {
		if(valor == "1"){
			document.getElementById(id).style.display = "block";
		}
		if(valor == "0"){
			document.getElementById(id).style.display = "none";
        }
		valor=null;
		id=null;
		idfecha=null;
	}  

	function ValidarFechas(idfechas)
   {
        var fechainicial = document.getElementById("fechaactual").value;
        var fechafinal = document.getElementById(idfechas).value;
		console.log(fechainicial);
		console.log(fechafinal);
        if(fechafinal > fechainicial)
        {
        alert("La fecha no puede ser mayor a la actual");
		document.getElementById(idfechas).value = null;
		}
   } 
</script>
<script>
	function mostrar(dato) {
		if (dato == "1") {
			document.getElementById("tipotos").style.display = "block";

		}
		if (dato == "0") {
			document.getElementById("tipotos").style.display = "none";

		}
	}
</script>
<br>
<div class="container contenedor-cuestionario" style="background-color:#dcdcdc ;">
	<div class="card card-qr">
		<div class="card-header d-flex justify-content-center" style="background-color:#841138 ; color:antiquewhite;">
			CUESTIONARIO
		</div>
		<br>
		<div class="card-body">
			<form id="contenedor2" method="POST" action="<?php echo base_url('FiltroSanitario/registroInformacion'); ?>" style="text-align:center">
			    <input type="hidden" name="fechaactual" id="fechaactual" value="<?php echo $fechaactual?>";>
				<input type="hidden" name="varcorr" id="varcorr" value="<?php echo $varcorr; ?>" ;>
				<input type="hidden" name="correo" id="correo" value="<?php echo $correo; ?>" ;>
				<p id="lcont">Esta información es de caráctes informatívo <br> No se usa para evaluar el riesgo de la persona de ser portadora de COVID-19</p>
				<br>
				<div class="vacuna">
					<h2>
						Informe de vacunación SARS-CoV-2 (COVID-19)
					</h2>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="vacuna" id="idvacuna1" value="1"title="Seleccione una opción" <?php if ($vacuna==1) {?> checked <?php } ?> required>
						<label class="form-check-label" for="idvacuna1">1ra dosis</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="vacuna" id="idvacuna2" value="2" title="Seleccione una opción" <?php if ($vacuna==2) {?> checked <?php } ?> required>
						<label class="form-check-label" for="idvacuna2">2da dosis</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="vacuna" id="idvacuna3" value="3" title="Seleccione una opción" <?php if ($vacuna==3) {?> checked <?php } ?> required>
						<label class="form-check-label" for="idvacuna3">Refuerzo</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="vacuna" id="idvacuna4" value="4" title="Seleccione una opción" <?php if ($vacuna==4) {?> checked <?php } ?> required>
						<label class="form-check-label" for="idvacuna4">Prefiero no contestar</label>
					</div>
				</div>
				<div class="container">
					<br>
					<h3>Ha sido diagnósticado por un médico con:<br><br></h3>
					<table class="table table-borderless">
						<tbody>
							<tr>
								<th>Diabetes mellitus </th>
								<th style="text-align: center;">
									<div class="required">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="diagnostico1" id="diagnostico" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico1==1) {?> checked <?php } ?> required>
											<label class="form-check-label" for="diagnostico">SI</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="diagnostico1" id="diagnostico1NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico1==0) {?> checked <?php } ?>required>
											<label class="form-check-label" for="diagnostico1NO">NO</label>
										</div>
									</th>
								</tr>
								<tr>
									<th>Hipertensión arterial</th>
									<th style="text-align: center;">
								<div class="required" required>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico2" id="diagnostico2" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico2==1) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico2">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico2" id="diagnostico2NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico2==0) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico2NO">NO</label>
									</div>
								</div>
							</th>
						</tr>
						<tr>
							<th>Antecedentes cardiovasculares</th>
							<th style="text-align: center;">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="diagnostico3" id="diagnostico3" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico3==1) {?> checked <?php } ?> required>
									<label class="form-check-label" for="diagnostico3">SI</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="diagnostico3" id="diagnostico3NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico3==0) {?> checked <?php } ?> required>
									<label class="form-check-label" for="diagnostico3NO">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Enfermedad respiratoria crónica</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico4" id="diagnostico4" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico4==1) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico4">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico4" id="diagnostico4NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico4==0) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico4NO">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Otro padecimiento</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico5" id="diagnostico5" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico5==1) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico5">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico5" id="diagnostico5NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico5==0) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico5NO">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Es Usted fumador</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico6" id="diagnostico6" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico6==1) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico6">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="diagnostico6" id="diagnostico6NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($diagnostico6==0) {?> checked <?php } ?> required>
										<label class="form-check-label" for="diagnostico6NO">NO</label>
									</div>
								</th>
							</tr>
						</tbody>
					</table>
					<br><br>
					<h1>Signos y síntomas mayores<br><br></h1>
					<h3>En los últimos 10 días ha presentado:<br><br></h3>
					<table class="table table-borderless">
						<tbody>
							<tr>
								<th>Fiebre </th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor1" id="mayor1" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="mayor1">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor1" id="mayor1NO" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="mayor1NO">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Tos</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor2" id="mayor2" value="1" onchange="mostrar(this.value);" title="Seleccione una opción" required>
										<label class="form-check-label" for="mayor2">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor2" id="M2" value="0" onchange="mostrar(this.value);"  title="Seleccione una opción" required>
										<label class="form-check-label" for="M2">NO</label>
									</div>
								</th>
							</tr>
							<tr name="tipotos" id="tipotos" style="display: none;">
								<th> Selecciona el tipo de tos </th>
								<th style="text-align: center; margin-right: 800px">
									<div class="form-check form-check-inline">
										<select class="custom-select" aria-label="Default select example" name="selecttipotos">
											<option value="">Selecciona una opción</option>
											<option value="Tos seca">Tos seca</option>
											<option value="Tos productiva">Tos productiva</option>
										</select>
									</div>
								</th>
							</tr>
							<tr>
								<th>Dificultad para respirar o falta de aire</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor3" id="mayor3" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="mayor3">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor3" id="M3" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="M3">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Dolor de cabeza</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor4" id="mayor4" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="mayor4">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="mayor4" id="M4" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="M4">NO</label>
									</div>
								</th>
							</tr>

						</tbody>
					</table>
					<br><br>
					<h3>Signos y síntomas menores que pueden acompañar a los mayores<br><br></h3>
					<table class="table table-borderless">
						<tbody>
							<tr>
								<th>Dolores musculares </th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor1" id="menor1" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor1">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor1" id="N1" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N1">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Dolor a nivel de articulaciones</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor2" id="menor2" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor2">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor2" id="N2" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N2">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Sensación de fatiga y debilidad muscular</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor3" id="menor3" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor3">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor3" id="N3" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N3">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Dolor al tragar alimentos o liquidos</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor4" id="menor4" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor4">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor4" id="N4" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N4">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Dolor abdominal o diarrea</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor5" id="menor5" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor5">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor5" id="N5" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N5">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Alteraciones del gusto</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor6" id="menor6" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor6">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor6" id="N6" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="N6">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Alteraciones del olfato</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor7" id="menor7" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor7">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor7" id="n7" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="n7">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Escurrimiento nasal</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor8" id="menor8" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor8">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor8" id="n8" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="n8">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Conjuntivitis</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor9" id="menor9" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor9">SI</label>
									</div>


									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor9" id="n9" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="n9">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>Escalofríos</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor10" id="menor10" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor10">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor10" id="n10" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="n10">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>¿Ha estado de viaje (nacional o en el extranjero)?</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor11" id="menor11" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="menor11">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor11" id="n11" value="0" pattern="^@?(\w){1,15}$" title="Seleccione una opción" required>
										<label class="form-check-label" for="n11">NO</label>
									</div>
								</th>
							</tr>
							<tr>
								<th>¿Ha sido diagnosticado por un médico con SARS Cov-2 (COVID 19)?</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor12" id="menor12" value="1" title="Seleccione una opción" onchange="mostrarelementos(this.value,'fecha12','date12');" required>
										<label class="form-check-label" for="menor12">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor12" id="n12" value="0" title="Seleccione una opción" onchange="mostrarelementos(this.value,'fecha12','date12');" required>
										<label class="form-check-label" for="n12">NO</label>
									</div>
								</th>
							</tr>
							<tr class="trfechas" id="fecha12">
								<th>Fecha de su confirmación por prueba de antígenos o PCR</th>
								<th style="text-align: center;" >
								<div >
								<input type="text" id="date12" name="date12" onchange="ValidarFechas(this.id)">
								</div>
								</th>
							</tr>
							<tr>
								<th>¿Cursa con más de 14 días de cuarentena?</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor13" id="menor13" value="1" onchange="mostrarelementos(this.value,'fecha13','date13');" required>
										<label class="form-check-label" for="menor13">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor13" id="n13" value="0" onchange="mostrarelementos(this.value,'fecha13','date13');" required>
										<label class="form-check-label" for="n13">NO</label>
									</div>
								</th>
							</tr>
							<tr class="trfechas" id="fecha13">
								<th>Ingrese la fecha de Prueba de antígeno NEGATIVA</th>
								<th style="text-align: center;" >
									<div >
										<input type="text" id="date13" name="date13" onchange="ValidarFechas(this.id)">
									</div>
								</th>
							</tr>
							<tr>
								<th>¿Ha tenido contacto con personas sospechosas o diagnosticada con COVID-19?</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor14" id="menor14" value="1" onchange="mostrarelementos(this.value,'fecha14','date14');" required>
										<label class="form-check-label" for="menor14">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor14" id="n14" value="0" onchange="mostrarelementos(this.value,'fecha14','date14');" required>
										<label class="form-check-label" for="n14">NO</label>
									</div>
								</th>
							</tr>
							<tr class="trfechas" id="fecha14">
								<th>Fecha de su confirmación por prueba de antígenos o PCR</th>
								<th style="text-align: center;" >
									<div >
										<input type="text" id="date14" name="date14" onchange="ValidarFechas(this.id)" >
									</div>
								</th>
							</tr>
							<tr>
								<th>Otro</th>
								<th style="text-align: center;">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor15" id="menor15" value="1" required>
										<label class="form-check-label" for="menor15">SI</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="menor15" id="n15" value="0" required>
										<label class="form-check-label" for="n15">NO</label>
									</div>
								</th>
							</tr>
						</tbody>
					</table>
				</div>
				<button class="btn btn-secondary" id="btn2"> Envia información </button>
			</form>
		</div>
	</div>
</div>


<?php echo $this->endSection() ?>