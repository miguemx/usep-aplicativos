<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<style>
    .error {
        border: red 2px solid;
    }

    datalist {
        width: 50000px;
        height: 80px;
    }

    .icono-1:hover {
        background-color: #4dff4d;
        /* Green */
        color: white;
        /* border: 2px solid red; */
    }

    .boton-agregar {
        background-color: #ceccb5;
    }
</style>
<?php if ($mensaje) : ?>
    <script>
        <?php
        switch ($mensaje['tipo']) {
            case 'alert-warning':
                $icon = 'error';
                $titulo = 'Error inesperado';
                break;
            case 'alert-info':
                $titulo = 'Información del proceso';
                $icon = 'info';
                break;
            case 'alert-success':
                $titulo = 'Proceso exitoso';
                $icon = 'success';
                break;
        }
        ?>

        function mensajes_info() {
            console.log('mensaje');
            Swal.fire({
                'title': '<?php echo $titulo ?>',
                'text': "<?php echo $mensaje['texto']; ?>",
                'icon': '<?php echo $icon ?>',
                confirmButtonColor: '#840f31'
            });
        }
        mensajes_info();
    </script>
<?php endif; ?>
<div class="container ">
    <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
        <form action="<?php echo base_url('ProgramacionAcademica/MapaAulas'); ?>" method="post" id="buscador">
            <div class="alert fade show alert-info" role="alert">
                <div id="text_periodo"></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="select_periodo" class="form-label">Periodo</label>
                        <select class="form-select" name="select_periodo" id="select_periodo" onchange="getDescripccion(this)">
                            <option value="0">Seleccione un periodo</option>
                            <?php foreach ($periodos as $lista_periodo) : ?>
                                <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                            <?php endforeach; ?>
                        </select>
                        <small></small>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="aulas_select" class="form-label">Seleccione el Aula</label>
                        <select class="form-select" name="aulas_select" id="aulas_select">
                            <option value="0">Seleccione una opcion</option>
                            <?php foreach ($aulas as $aula) : ?>
                                <option value="<?php echo $aula->id_aula ?>" <?php echo (($aula_clave == $aula->id_aula) ? 'selected' : '') ?>><?php echo $aula->nombre_aula ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="carrera_select" class="form-label">Carrera</label>
                        <select class="form-select" name="carrera_select" id="carrera_select" <?php echo (($administrador) ? '' : 'disabled') ?>>
                            <?php foreach ($carreras as $carrera) : ?>
                                <option value="<?php echo $carrera->id ?>" <?php echo (($rol ==  $carrera->id) ? 'selected' : '') ?>><?php echo $carrera->nombre ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="text-center mt-3">
                    <button type="button" class="btn btn-secondary" onclick="buscar()">Buscar</button>
                </div>
            </div>


        </form>
    </div>

</div>
<?php if ($horario) : ?>
    <div class="container-fluid">
        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
            <div class="text-center form-group mt-4">
                <h4><?php echo $aula_nombre  ?></h4>
                <div><a href="<?php echo base_url('ProgramacionAcademica/MapaAulasCsv').'/'.$aulaId.'/'.$periodo ?>" class="btn btn-secondary btn-sm" target="_blank">Descargar</a></div>
            </div>
            <table class="table table-borderless  mt-3 text-center">
                <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                    <tr>
                        <th scope="row">HORA</th>
                        <th>LUNES</th>
                        <th>MARTES</th>
                        <th>MIERCOLES</th>
                        <th>JUEVES</th>
                        <th>VIERNES</th>
                        <th>SABADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hora = 7;
                    ?>

                    <?php while ($hora < 21) : ?>

                        <tr>
                            <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                            <?php foreach ($dias as $dia) : ?>
                                <?php $bandera_coincidencia = false; ?>
                                <?php foreach ($grupos as $grupo) : ?>
                                    <?php if ($grupo->dia == $dia) : ?>
                                        <?php if ($grupo->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                            <td id="<?php echo $grupo->grupo . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?>">
                                                <div>
                                                    <?php echo  $grupo->ma_nombre . '<br>' . (($grupo->docente == 1) ? 'Por asignar' : ($grupo->em_paterno . ' ' . $grupo->em_materno . ' ' . $grupo->em_nombre)) . "<br>" . $grupo->seccion ?>
                                                </div>
                                                <?php if ($grupo->carrera == $rol || $rol == 'administrador') : ?>
                                                    <?php if ($bandera_edicion) : ?>
                                                        <div>
                                                            <button class="btn" name="btn_editar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre . "|$dia"  ?>" onclick="editar(this)"><i class="bi bi-pencil-square" style="font-size: 1.2rem; color: grey;"></i></button>
                                                            <button class="btn" name="btn_eliminar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre ?>" onclick="eliminar(this)"><i class="bi bi-trash container-fluid" style="font-size: 1.2rem; color: grey;"></i></button>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (!$bandera_coincidencia) : ?>
                                    <td style="background-color: #bdb994;">
                                        <?php if ($bandera_edicion) : ?>
                                            <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo/' . $periodo); ?>" method="post">
                                                <input type="hidden" name="posicion" value="<?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $aula_clave . '|' . $dia . '|' . $periodo . '|' . $rol ?>">
                                                <button type="submit" class="btn" name="bandera" value="<?php echo $bandera ?>"><i class="bi bi-plus-circle" style="font-size: 2rem; color: grey;"></i></button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                        <?php $hora++; ?>

                    <?php endwhile; ?>


                </tbody>
            </table>

        </div>
    </div>

    <form action="<?php echo base_url('ProgramacionAcademica/eliminarGrupoHora'); ?>" method="post" id="form-eliminar">

    </form>
    <form action="<?php echo base_url('ProgramacionAcademica/editarGrupoHora'); ?>" method="post" id="form-editar">
        <input type="hidden" name="bandera" value="<?php echo "MapaAulas/$aula_clave";  ?>">
    </form>
<?php endif; ?>
<script>
    function buscar() {
        var selectores = document.querySelectorAll('select');
        let errores = false;
        selectores.forEach(element => {
            // console.log(element.value);
            if (element.value == 0) {
                element.classList.add('error');
                errores = true;
            } else {
                element.classList.remove('error');
            }

        });
        if (!errores) {
            document.getElementById('buscador').submit();
        } else {
            alert('seleccione un periodo y un aula para continuar');
        }
    }

    function editar(grupo) {
        var contenedor = grupo.value.split('|');
        mensajes(('Seguro que desea editar la ' + contenedor[3] + ' del dia ' + contenedor[1] + ' en la hora ' + contenedor[2] + ', ¿Desea continuar? '), 'form-editar', grupo);
    }

    function eliminar(grupo) {
        var contenedor = grupo.value.split('|');
        mensajes(('¡ATENCIÓN ! Estas por eliminar la materia ' + contenedor[3] + ' del dia ' + contenedor[1] + ' en la hora ' + contenedor[2] + ', ¿Está seguro que deseas continuar? '), 'form-eliminar', grupo);
    }

    function mensajes(mensaje_1, submit, grupo) {
        Swal.fire({
            'title': 'Confirmación',
            'text': mensaje_1,
            'icon': 'question',
            confirmButtonColor: '#31BA31E3',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                var form = document.getElementById(submit); //.submit();
                var input = document.createElement('input');
                input.setAttribute('value', grupo.value);
                input.hidden = true;
                input.name = 'grupo_detalles';
                form.appendChild(input);
                form.submit();

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#31BA31E3'
                });
            }
        });
    }

    function getDescripccion(periodo) {
        let periodos = <?php echo json_encode($periodos) ?>;
        // console.log(periodo);
        // console.log(periodos);
        periodos.forEach(element => {
            if (element.id == periodo.value) {
                let texto = document.getElementById('text_periodo');
                switch (element.estado) {
                    case 'INICIO':
                        texto.innerHTML = 'El periodo seleccionado esta habilitado para realizar cambios de horarios y grupos';
                        break;
                    case 'INSCRIPCION':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para la modificacion de horarios y cambios de docentes';
                        break;
                    case 'CURSO':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'CALIFICACIONES':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'FINALIZADO':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                }
            }
        });
    }
    let periodo_selected = document.getElementById('select_periodo');
    getDescripccion(periodo_selected);
</script>
<?php echo $this->endSection() ?>