<!--
=========================================================
* Material Dashboard 2 - v3.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('templates'); ?>assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url('favicon.ico'); ?>">
  <title>
    Registro de Aspirantes :: Universidad de la Salud
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/material-dashboard.css?v=3.0.0" rel="stylesheet" />
  <style>
    .mayus {
      text-transform: uppercase;
    }
  </style>
</head>

<body class="">
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <?php echo $this->include('admision/menu'); ?>
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
              <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center" style="background-image: url('<?php echo base_url('img/fondos/patio_noche.jpg'); ?>'); background-size: cover;">
              </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5" style="padding-top: 70px;">
              <div class="card card-plain">
                <div class="card-header">
                  <h4 class="font-weight-bolder">Registro</h4>
                  <p class="mb-0">Para dar inicio a su proceso de admisión, capture el siguiente formulario de datos generales:</p>
                </div>
                <div class="card-body">
                  <?php if( isset($error) ): ?>
                    <div style="color: #f00; margin-bottom: 15px;">
                      Por favor verifica los siguientes errores:<br /><br />
                      <?php echo $error; ?>
                    </div>
                  <?php endif; ?>
                  <form role="form" method="post" action="<?php echo base_url('Admision/Registro') ?>">
                  <div class="input-group input-group-static mb-3">
                      <label class="form-label">Clave Única de Registro de Población (CURP)</label>
                      <input type="text" class="form-control mayus" name="curp" value="<?php echo $curp; ?>" />
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label class="form-label">Primer Apellido</label>
                      <input type="text" class="form-control mayus" name="apPaterno" value="<?php echo $apPaterno; ?>" />
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label class="form-label">Segundo Apellido</label>
                      <input type="text" class="form-control mayus" name="apMaterno" value="<?php echo $apMaterno; ?>" />
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label class="form-label">Nombre(s)</label>
                      <input type="text" class="form-control mayus" name="nombre" value="<?php echo $nombre; ?>" />
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label class="form-label">Correo electrónico vigente</label>
                      <input type="text" class="form-control" name="correo" value="<?php echo $correo; ?>" />
                    </div>

                    <div class="input-group input-group-static mb-3">
                      <select class="form-control" name="carrera" value="">
                        <option value="">Opción de ingreso</option>
                        <?php foreach($carreras as $opcion): ?>
                          <option value="<?php echo $opcion->id; ?>" <?php if($opcion->id==$carrera) echo 'selected="selected"'; ?> ><?php echo mb_strtoupper($opcion->nombre); ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>

                    <div class="form-check form-check-info text-start ps-0">
                      <input class="form-check-input" type="checkbox" value="1" id="flexCheckDefault" name="terminos" />
                      <label class="form-check-label" for="flexCheckDefault">
                        Estoy de acuerdo con los <a href="https://usep.puebla.gob.mx/images/site/content/Servicios%20escolares/Convocatoria%202022%20-%202%20mayo%20subsanada.pdf" target="_blank" class="text-dark font-weight-bolder">términos y condiciones</a> de la convocatoria de admisión 2022.
                      </label>
                    </div>
                    <div class="form-check form-check-info text-start ps-0">
                      <input class="form-check-input" type="checkbox" value="1" id="flexCheckDefault" name="aviso" />
                      <label class="form-check-label" for="flexCheckDefault">
                        He leido y acepto el <a href="https://usep.puebla.gob.mx/index.php/web/transparencia" target="_blank" class="text-dark font-weight-bolder">aviso de privacidad.</a><!--Mario dijo-->
                      </label>
                    </div>
                    <div class="text-center">
                        <div class="g-recaptcha" data-sitekey="6LdsjLEeAAAAAK9jUl4wCsIuGV_kgpAh39I3e0XW" data-theme="light" data-callback="prepare" id="captcha_login"></div>
                    </div>
                    
                    <div class="text-center">
                      <input type="hidden" name="aspreg" value="1" />
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0" id="btnReg" disabled="disabled" >Crear registro</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-2 text-sm mx-auto">
                    Si ya cuenta con sus datos de registro, puede ingresar al Portal de Aspirantes
                    <a href="<?php echo base_url('Admision/Inicia'); ?>" class="text-primary text-gradient font-weight-bold" stile="font-size: 25px">aquí</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/bootstrap.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard//assets/js/material-dashboard.min.js?v=3.0.0"></script>



  <script type="text/javascript">
    var onloadCallback = function() {
        /* grecaptcha.render('captcha_login', {
            'site_key' : '6LdsjLEeAAAAAK9jUl4wCsIuGV_kgpAh39I3e0XW'
        }); */
    };

    function prepare() {
        document.getElementById('btnReg').disabled = false;
    }

    let cforms = document.getElementsByClassName('form-control');
    for ( let i=0; i<cforms.length; i++ ) {
      if ( cforms[i].value != "" ) {
        cforms[i].parentElement.classList.add('is-filled');
      }
    }
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

</body>

</html>