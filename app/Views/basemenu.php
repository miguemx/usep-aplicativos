<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-4 fixed-start ms-3 bg-gradient-primary" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="#">
        <img src="<?php echo base_url('img/logo_USEP_negativo.png'); ?>" class="navbar-brand-img h-100" alt="main_logo" style="max-height: 2.9rem;">
        <!-- <span class="ms-1 font-weight-bold text-white">Universidad de la Salud del Estado de Puebla</span> -->
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main" style="height: auto;">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white " href="<?php echo base_url('Admision'); ?>" style="margin: 0px;">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Inicio</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="<?php echo base_url('Admision/DatosGenerales'); ?>" style="margin: 0px;">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10" <?php if( $aspirante->municipio != null ) echo 'style="color: #0f0;"'; else echo 'style="color: #676767;"'; ?>>person</i>
            </div>
            <span class="nav-link-text ms-1">Datos Generales</span>
          </a>
        </li>

        <!-- etapas definidas por el configurador -->
        <?php foreach( $etapas as $etapa ): ?>
          <li class="nav-item">
          <a class="nav-link text-white " href="<?php if($etapa->completado==false && $etapa->actual==false) echo 'javascript:void()'; else echo base_url('Admision/Etapa').'/'.$etapa->etapa; ?>" style="margin: 0px;">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <?php 
              $iconstyle = '';
              if( $etapa->completado == false && $etapa->actual == false ) $iconstyle = 'color: #676767;';
              else if ($etapa->actual == true) $iconstyle = 'color: #fff;';
              else $iconstyle = 'color: #0f0;';
              ?>
              <i class="material-icons opacity-10" style="<?php echo $iconstyle; ?>">
                <?php 
                if( $etapa->completado == false && $etapa->actual == false ) echo 'disabled_by_default';
                else if ($etapa->actual == true) echo 'edit_note';
                else echo 'check_circle';
                ?>
              </i>
            </div>
            <span class="nav-link-text ms-1"><?php echo $etapa->nombre; ?></span>
          </a>
        </li>
        <?php endforeach; ?>

        <!-- <?php // if( $aspirante->estatus == 'EXITOSO' ): ?>
          <li class="nav-item">
            <a class="nav-link text-white " href="<?php echo base_url('Admision/aspiranteaceptado'); ?>" style="margin: 0px;">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">receipt_long</i>
              </div>
              <span class="nav-link-text ms-1">Ficha de examen</span>
            </a>
          </li>
        <?php // endif; ?>

        <?php // if( $aspirante->estatus == 'DECLINADO' ): ?>
          <li class="nav-item">
            <a class="nav-link text-white " href="<?php echo base_url('Admision/aspirantedeclinado'); ?>" style="margin: 0px;">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">sports_score</i>
              </div>
              <span class="nav-link-text ms-1">Proceso concluido</span>
            </a>
          </li>
        <?php // endif; ?> -->


        <li class="nav-item">
          <a class="nav-link text-white" href="<?php echo base_url('Admision/Salir'); ?>" style="margin: 0px;">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">logout</i>
            </div>
            <span class="nav-link-text ms-1">Cerrar sesión</span>
          </a>
        </li>
      </ul>
    </div>
    <!-- <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3"> 
        <a class="btn bg-gradient-secondary mt-4 w-100" href="<?php echo base_url('Admision/Salir'); ?>" type="button">Cerrar sesión</a>
      </div>
    </div> -->
  </aside>