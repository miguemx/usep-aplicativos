<?php
namespace App\Helpers;

class Calculo {

    /**
     * calcula la edad con base en una curp entregada
     * @param curp la curp
     * @return edad la edad en numero entero
     */
    public static function edadByCurp($curp) {
        $edad = 0;
        if( preg_match( "/^[A-Z]{4}[0-9]{6}/", $curp ) ) {
            $anioCurp = (int)substr( $curp, 4, 2);
            $anioCurp = ( $anioCurp<50 )? $anioCurp+2000: $anioCurp+1900;
            $actual = date("Y-m-d");
            $fnacimiento = $anioCurp.'-'.substr( $curp, 6,2).'-'.substr( $curp, 8, 2 );
            $dateDifference = abs(strtotime($actual) - strtotime($fnacimiento));
            $edad  = floor($dateDifference / (365 * 60 * 60 * 24));
        }
        return $edad;
    }

}