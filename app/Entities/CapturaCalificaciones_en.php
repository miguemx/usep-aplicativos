<?php

namespace App\Entities;

use CodeIgniter\Entity;

class CapturaCalificaciones_en extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'gpoalumno' => null,
        'gpo_grupo' => null,
        'gpo_ordinario' => null,
        'gpo_extraordinario' => null,
        'pgo_comentario' => null,
        'aprobada' => null,
        'pgo_flg' => null,
        'grupo_id' => null,
        'grupoidc' => null,
        'grupo_clave' => null,
        'grupo_folio' => null,
        'grupo_materia' => null,
        'grupo_docente' => null,
        'grupo_periodo' => null,
        'grupo_max' => null,
        'grupo_ocupado' => null,
        'gpoflag_acta' => null,
        'materia_clave' => null,
        'materia_carrera' => null,
        'materia_nombre' => null,
        'materia_creditos' => null,
        'materia_obligatoria' => null,
        'materia_clasificacion' => null,
        'materia_seriacion' => null,
        'materia_semestre' => null,
        'carrera_id' => null,
        'carrera_nombre' => null,
        'carrera_numero' => null,
        'carrera_clave' => null,
        'clave_programa' => null,
        'carrera_facultad' => null,
        'periodo_id' => null,
        'periodo_nombre' => null,
        'periodo_inicio' => null,
        'periodo_fin' => null,
        'id_alumno' => null,
        'correo' => null,
        'nombre' => null,
        'nombres' => null,
        'apellido_p' => null,
        'apellido_m' => null,
        'curp' => null,
        'sexo' => null,
        'carrera' => null,
        'vigente' => null,
        'empleado_nombre' => null,
        'empleado_apellido' => null,
        'fecha_captura' => null,
        'fecha_acta' =>         null,
        'fecha_impresion' =>null,

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id_alumno' =>          "alumno_id",
        'correo' =>             "alumno_correo",
        'nombre' =>             "alumno_nombre",
        'nombres' =>            "alumno_nombres",
        'apellido_p' =>         "alumno_ap_paterno",
        'apellido_m' =>         "alumno_ap_materno",
        'curp' =>               "alumno_curp",
        'sexo' =>               "alumno_sexo",
        'carrera' =>            "alumno_carrera",
        'vigente' =>            "alumno_vigente",
        'gpoalumno' =>          "gpoalumno_alumno",
        'gpo_grupo' =>          "gpoalumno_grupo",
        'gpo_ordinario' =>      "gpoalumno_ordinario",
        'gpo_extraordinario' => "gpoalumno_extraordinario",
        'gpo_aprobada' =>       "aprobada",
        'gpo_pago' =>           "gpoalumno_flg_pago",
        'grupo_id' =>           "grupo_id",
        'grupoidc' =>           "grupo_idc",
        'grupo_clave' =>        "grupo_clave",
        'grupo_folio' =>        "grupo_folio",
        'carrera' =>            "carrera_nombre",
        'carrera_id' =>         "carrera_id",
        'grupo_materia' =>      "grupo_materia",
        'materia_nombre' =>     "materia_nombre",
        'grupo_docente' =>      "grupo_docente",
        'empleado_nombre' =>    "empleado_nombre",
        'empleado_apellido' =>  "empleado_apellido",
        'empleado_correo' =>    "empleado_correo",
        'grupo_periodo' =>      "grupo_periodo",
        'periodo_nombre' =>     "periodo_nombre",
        'periodo_inicio' =>     "periodo_inicio",
        'periodo_fin' =>        "periodo_fin",
        'grupo_max' =>          "grupo_max",
        'grupo_ocupado' =>      "grupo_ocupado",
        'clave_programa' =>     "carrera_clave_programa",
        'facultad' =>           "carrera_facultad",
        'carrera_nombre' =>     "carrera_nombre",
        'empleado_nombre' =>    "empleado_nombre",
        'empleado_apellido' =>  "empleado_apellido",
        'fecha_captura' =>      "grupo_fechacaptura",
        'fecha_acta' =>         "grupo_flag_acta",
        'fecha_impresion' =>    "grupo_fechaimpresion",
        'materia_seriacion' =>    "materia_seriacion",
        'materia_semestre' =>    "materia_semestre",
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
