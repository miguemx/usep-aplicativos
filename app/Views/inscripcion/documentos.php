<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <div class="bg-white rounded shadow p-2 mt-2 mb-4">
        <div class="col-sm-12">
            <h4 class="mb-2">Relación documental</h4>
            <table class="table table-striped table-sm">
                <tr>
                    <th>Matrícula:</th>
                    <td><?php echo $alumno->id; ?></td>
                </tr>
                <tr>
                    <th>Nombre:</th>
                    <td><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></td>
                </tr>
                <tr>
                    <th>Correo:</th>
                    <td><?php echo $alumno->correo; ?></td>
                </tr>
                <tr>
                    <th>Contraseña:</th>
                    <td><?php echo substr( $alumno->curp, 0, 10).'*'; ?></td>
                </tr>
                <tr>
                    <th>Carrera:</th>
                    <td><?php echo $carrera->nombre; ?></td>
                </tr>
            </table>
                
            <!--<form method="post" action="<?php// echo base_url('Inscripcion/Documentacion').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>" class="row g3"> @TODO1 habilitar esta linea en cuanto se revise la libreria de cifrado-->
            <form method="post" action="<?php echo base_url('Inscripcion/Documentacion').'/'.$alumno->id; ?>" class="row g3">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Entrega</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Extracto de Acta de nacimiento:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="acta"
                                            value="1"
                                            <?php if($reldocumental->acta=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>CURP:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="curp"
                                            value="1"
                                            <?php if($reldocumental->curp=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Identificación oficial con fotografía:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="identificacion"
                                            value="1"
                                            <?php if($reldocumental->identificacion=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Certificado de Estudios Concluidos:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="certificado"
                                            value="1"
                                            <?php if($reldocumental->certificado=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Certificado de Estudios Concluidos legalizado:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="legalizado"
                                            value="1"
                                            <?php if($reldocumental->legalizado=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Constancia de estudios Terminados:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="constancia"
                                            value="1"
                                            <?php if($reldocumental->constancia=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Kardex Legalizado:</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" 
                                            type="checkbox" role="switch" 
                                            id="flexSwitchCheckChecked" 
                                            name="kardex"
                                            value="1"
                                            <?php if($reldocumental->kardex=='1') echo 'checked="checked"' ?> />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <div>&nbsp;</div>
                <div class="d-flex flex-row-reverse">
                    <input type="hidden" name="guardar" value="g" />
                    <!--<a class="btn btn-secondary" href="<?php //echo base_url('Inscripcion/Seccion').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>">@TODO1 habilitar esta linea en cuanto se revise la libreria de cifrado-->
                    <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Seccion').'/'.$alumno->id; ?>">
                        Proceder al Horario
                    </a>
                    <div>&nbsp;</div>
                    <button class="btn btn-secondary" type="submit">
                        Guardar
                    </button>
                    <div>&nbsp;</div>
                    <!--<a class="btn btn-secondary" href="<?php //echo base_url('Inscripcion/Cuotas').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>">@TODO1 habilitar esta linea en cuanto se revise la libreria de cifrado-->
                    <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Cuotas').'/'.$alumno->id; ?>">
                        Cuotas
                    </a>
                    <div>&nbsp;</div>
                    <button class="btn btn-danger" type="button" onclick="rechazar()">
                        Rechazar
                    </button>
                </div>
                
            </form>

            <form method="post" action="<?php echo base_url('Inscripcion/Rechazar'); ?>" class="row g3" onsubmit="return confirmarechazo()" id="rechazo" style="display: none;">
                <div class="d-flex mt-3 mb-3" >
                    <label class="flex-fill">Razón del rechazo: </label>
                    <input type="text" name="razon" id="razon" value="" class="form-control flex-fill" />
                    <input type="hidden" name="id" value="<?php echo $alumno->id; ?>"  />
                    <input type="hidden" name="pantalla" value="2"  />
                    <button class="btn btn-danger flex-fill" type="submit">
                        Confirmar aspirante rechazado.
                    </button>
                </div>
            </form>

            <h4>Imprmibles</h4>
            <div class="d-flex">
                <input type="hidden" name="guardar" value="g" />
                <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Documento').'/'.$alumno->id.'/reglamento'; ?>" target="_blank">
                    Aceptación del Reglamento
                </a>
                <div>&nbsp;</div>
                <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Documento').'/'.$alumno->id.'/consentimiento'; ?>" target="_blank">
                    Consentimiento de uso de datos personales
                </a>
                <?php if( $reldocumental->acta=='1' && $reldocumental->curp=='1' && $reldocumental->identificacion=='1' && ( $reldocumental->certificado=='1' || $reldocumental->legalizado=='1' || $reldocumental->kardex=='1') ): ?>
                    <div>&nbsp;</div>
                    <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Documento').'/'.$alumno->id.'/responsiva'; ?>" target="_blank">
                        Carta responsiva
                    </a>
                <?php else: ?>
                    <div>&nbsp;</div>
                    <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Documento').'/'.$alumno->id.'/adeudo'; ?>" target="_blank">
                        Carta de adeudo
                    </a>
                <?php endif; ?>
                <div>&nbsp;</div>
            </div>

        </div>
    </div>
</div>

<script>
    function rechazar() {
        document.getElementById('rechazo').style.display = 'block';
    }
    function confirma() {
        if ( confirm("¿Confirma que desea inscribir a este aspirante?\nA partir de este momento se convertirá en estudiante y se generará su acceso a las aplicaciones y correo electrónico.") ) {
            return true;
        }
        return false;
    }
    function confirmarechazo() {
        if ( confirm("¿Confirma que desea rechazar este aspirante?\nNo podrá volver a inscribirlo ni lo verá en la lista de aceptados.\n\nEsta acción no se puede deshacer.") ) {
            let razon  = document.getElementById('razon').value;
            if( razon.length > 10 ) {
                return true;
            }
            alert( "Por favor escriba la razón del rechazo" );
        }
        return false;
    }
</script>



<?php echo $this->endSection() ?>