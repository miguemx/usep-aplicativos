CREATE OR REPLACE VIEW view_filtro_empleados AS 
SELECT
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` AS `ID`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `apPaterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `apMaterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo` AS `correo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_sexo` AS `sexo`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_vacuna` AS `vacuna`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_folio` AS `folio`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_diabetes` AS `diabetes`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_hipertension` AS `hipertension`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_antecedentecard` AS `cardiacos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_enfermedadres` AS `enfermedadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_otropadecimiento` AS `otroPadecimiento`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fumador` AS `esFumador`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayorfiebre` AS `fiebre`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayortos` AS `tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordifrespiratoria` AS `dificultadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordolorcabeza` AS `dolorCabeza`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolormuscular` AS `dolorMuscular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolorarticular` AS `dolorArticular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorfatiga` AS `fatiga`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorabdominal` AS `dolorAbdominal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteragusto` AS `alteracionGusto`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteraolfato` AS `alteracionOlfato`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menornasal` AS `escurrimientoNasal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorconjuntivitis` AS `conjuntivitis`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorescalofrios` AS `escalofrios`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorviaje` AS `estuvoViaje`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorcovid` AS `contactoPersonaCovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_temp` AS `temperatura`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_oxigen` AS `oxigenacion`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebapositiva` AS `fecha_prueba_positiva`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebanegativo` AS `fecha_prueba_negativo`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechacontactocovid` AS `fecha_contactocovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_tipotos` AS `tipo_tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fecha` AS `fecha`
FROM
    (
        (
            `usep_aplicativos`.`filtro_sanitario`
        JOIN `usep_aplicativos`.`aplicativo_usuarios` ON
            (
                `usep_aplicativos`.`filtro_sanitario`.`filtro_correo` = `usep_aplicativos`.`aplicativo_usuarios`.`usuario_correo`
            )
        )
    JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_usuarios`.`usuario_correo` = `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo`
        )
    );


CREATE OR REPLACE VIEW view_filtro_estudiantes AS 
SELECT
    `usep_aplicativos`.`alumnos_generales`.`alumno_id` AS `ID`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno` AS `apPaterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno` AS `apMaterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres` AS `nombre`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_correo` AS `correo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_sexo` AS `sexo`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_vacuna` AS `vacuna`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_folio` AS `folio`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_diabetes` AS `diabetes`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_hipertension` AS `hipertension`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_antecedentecard` AS `cardiacos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_enfermedadres` AS `enfermedadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_otropadecimiento` AS `otroPadecimiento`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fumador` AS `esFumador`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayorfiebre` AS `fiebre`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayortos` AS `tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordifrespiratoria` AS `dificultadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordolorcabeza` AS `dolorCabeza`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolormuscular` AS `dolorMuscular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolorarticular` AS `dolorArticular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorfatiga` AS `fatiga`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorabdominal` AS `dolorAbdominal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteragusto` AS `alteracionGusto`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteraolfato` AS `alteracionOlfato`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menornasal` AS `escurrimientoNasal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorconjuntivitis` AS `conjuntivitis`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorescalofrios` AS `escalofrios`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorviaje` AS `estuvoViaje`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorcovid` AS `contactoPersonaCovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_temp` AS `temperatura`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_oxigen` AS `oxigenacion`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebapositiva` AS `fecha_prueba_positiva`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebanegativo` AS `fecha_prueba_negativo`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechacontactocovid` AS `fecha_contactocovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_tipotos` AS `tipo_tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fecha` AS `fecha`
FROM
    (
        (
            `usep_aplicativos`.`filtro_sanitario`
        JOIN `usep_aplicativos`.`aplicativo_usuarios` ON
            (
                `usep_aplicativos`.`filtro_sanitario`.`filtro_correo` = `usep_aplicativos`.`aplicativo_usuarios`.`usuario_correo`
            )
        )
    JOIN `usep_aplicativos`.`alumnos_generales` ON
        (
            `usep_aplicativos`.`aplicativo_usuarios`.`usuario_correo` = `usep_aplicativos`.`alumnos_generales`.`alumno_correo`
        )
    );

CREATE OR REPLACE VIEW view_filtro_externos AS 
SELECT
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_correo` AS `correo`,
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_nombre` AS `nombre`,
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_dependencia` AS `dependencia`,
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_edad` AS `rango_edad`,
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_sexo` AS `sexo`,
    `usep_aplicativos`.`filtro_usuarios_ext`.`externo_categoria` AS `categoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_vacuna` AS `vacuna`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_folio` AS `folio`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_diabetes` AS `diabetes`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_hipertension` AS `hipertension`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_antecedentecard` AS `cardiacos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_enfermedadres` AS `enfermedadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_otropadecimiento` AS `otroPadecimiento`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fumador` AS `esFumador`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayorfiebre` AS `fiebre`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayortos` AS `tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordifrespiratoria` AS `dificultadRespiratoria`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_mayordolorcabeza` AS `dolorCabeza`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolormuscular` AS `dolorMuscular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menordolorarticular` AS `dolorArticular`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorfatiga` AS `fatiga`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorabdominal` AS `dolorAbdominal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteragusto` AS `alteracionGusto`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menoralteraolfato` AS `alteracionOlfato`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menornasal` AS `escurrimientoNasal`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorconjuntivitis` AS `conjuntivitis`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorescalofrios` AS `escalofrios`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorviaje` AS `estuvoViaje`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_menorcovid` AS `contactoPersonaCovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_temp` AS `temperatura`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_oxigen` AS `oxigenacion`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebapositiva` AS `fecha_prueba_positiva`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechapruebanegativo` AS `fecha_prueba_negativo`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fechacontactocovid` AS `fecha_contactocovid`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_tipotos` AS `tipo_tos`,
    `usep_aplicativos`.`filtro_sanitario`.`filtro_fecha` AS `fecha`
FROM
    (
        `usep_aplicativos`.`filtro_sanitario`
    JOIN `usep_aplicativos`.`filtro_usuarios_ext` ON
        (
            `usep_aplicativos`.`filtro_sanitario`.`filtro_correo_externo` = `usep_aplicativos`.`filtro_usuarios_ext`.`externo_correo`
        )
    );