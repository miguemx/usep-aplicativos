-- --------------------------------------------------
-- eliminación de registros de aca_grupos_alumnos  --
-- --------------------------------------------------
DELETE
FROM aca_grupos_alumnos
WHERE gpoalumno_grupo in (select grupo_id from aca_grupos where grupo_periodo ='2021A');
-- -------------------------------------- --
-- eliminación de registros de aca_grupos --
-- -------------------------------------- --
DELETE FROM aca_grupos WHERE grupo_periodo = '2021A' AND NOT grupo_materia = 'MC-TUT';

-- ----------------------------
-- encuadre de grupos 2021A  --
-- ----------------------------

INSERT INTO aca_grupos (grupo_id,grupo_idc,grupo_clave,grupo_materia,grupo_docente,grupo_periodo) 
SELECT idc,idc,seccion,id_materia,id_docente,periodo FROM docentesid_idc_2021a;
INSERT INTO aca_grupos (grupo_id,grupo_idc,grupo_clave,grupo_materia,grupo_docente,grupo_periodo) values (2101112301,2101112301,'secc01','LEO-01','1','2021A');
INSERT INTO aca_grupos (grupo_id,grupo_idc,grupo_clave,grupo_materia,grupo_docente,grupo_periodo) values (2101113308,2101113308,'secc01','LEO-01','1','2021A');
-- ----------------------------------------- --
-- encuadre de la matrícula de alumnos 2021A --
-- ----------------------------------------- --

INSERT INTO `aca_grupos_alumnos`(`gpoalumno_alumno`, `gpoalumno_grupo`,`aprobada`, `gpoalumno_flg_pago`) select `matricula`,`idc`,'0','0' from listas_se;