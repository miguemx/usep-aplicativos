
<div style="font-family: Arial, Helvetica, sans-serif; padding: 30px; padding-bottom: 1px; padding-top: 1px;">
    <table  width="100%">
        <tr>
            <td style="text-align: left" width="30%"><img src="<?php echo base_url('img/logos/USEP.png'); ?>"></td>
            <td style="text-align: center; font-family: Arial, Helvetica, sans-serif;" valign="bottom"><strong>KARDEX</strong></td>
            <td style="text-align: right" width="30%"><img src="<?php echo base_url('img/logos/Gobierno.png'); ?>"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: right; font-family: Arial, Helvetica, sans-serif;" valign="top">
                &nbsp;<br />
                <strong>Folio:</strong> <?php echo $folio; ?> <br />
                <strong>CCT:</strong> 21ESU0013X
            </td>
        </tr>
    </table>
</div>
<div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-image: url('<?php echo base_url('img/fondozaca.png') ?>'); background-repeat: no-repeat; background-position: left bottom; background-size: 60%; padding: 30px; padding-top: 5px;">
    <table width="100%" style="font-size: 10px; font-family: Arial, Helvetica, sans-serif;">
        <tr>
            <td width="17%"><strong>NOMBRE:</strong></td><td width="33%"><?php echo $nombre; ?></td>
            <td width="17%"><strong>PROGRAMA:</strong></td><td width="33%"><?php echo mb_strtoupper( $carrera ); ?></td>
        </tr>
        <tr>
            <td><strong>MATRÍCULA:</strong></td><td><?php echo $matricula; ?></td>
            <td><strong>CLAVE DEL PROGRAMA:</strong></td><td><?php echo $clavePrograma; ?></td>
        </tr>
        <tr>
            <td><strong>FACULTAD / ESCUELA:</strong></td><td><?php echo $facultad; ?></td>
            <td><strong>PERIODO:</strong></td><td><?php echo '2021A'; ?></td>
        </tr>
        <tr>
            <td><strong>NIVEL:</strong></td><td><?php echo 'LICENCIATURA'; ?></td>
            <td><strong>SEMESTRE:</strong></td><td><?php echo '3'; ?></td>
        </tr>
        <tr>
            <td><strong>CAMPUS:</strong></td><td><?php echo 'PUEBLA'; ?></td>
            <td><strong>MODALIDAD:</strong></td><td><?php echo 'ESCOLARIZADA'; ?></td>
        </tr>
        
    </table>
    
    <p>
        &nbsp;
    </p>

    <div style="margin-left: 1px; margin-right: 1px;">
        <?php
        $par = 1;
        $totMaterias = 0;
        $sumCalif = 0;
        $totAprobadas = 0;
        $totReprobadas = 0;
        ?>
        <?php foreach( $kardex as $periodo=>$materias ): ?>
            <div style="float: <?php if($par%2==0) echo 'right'; else echo 'left' ?>; width: 47%; font-size: 10px;">
                PERIODO <?php echo $periodo.' ('.$materias[0]->periodoNombre.')'; ?>
                <table width="100%" style="border: 1px solid #000;">
                    <tr>
                        <th style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">CLAVE</th>
                        <th style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">ASIGNATURA</th>
                        <th style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">CALIFICACIÓN</th>
                        <th style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">TIPO DE EVALUACIÓN</th>
                        <th style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">IDC</th>
                    </tr>
                    <?php foreach( $materias as $materia ): ?>
                        <tr>
                            <td style="text-align: center; font-size: 6px; font-family: Arial, Helvetica, sans-serif;"><?php echo $materia->materiaClave; ?></td>
                            <td style="font-size: 6px; font-family: Arial, Helvetica, sans-serif;"><?php echo $materia->materia; ?></td>
                            <td style="text-align: center; font-size: 6px; font-family: Arial, Helvetica, sans-serif;">
                                <?php
                                    $calif = 0;
                                    if ( $materia->ordinario ) $calif = $materia->ordinario;
                                    else $calif = $materia->extraordinario;
                                    $totMaterias++;
                                    $sumCalif += $calif;
                                    if ( $calif > 5 ) $totAprobadas++;
                                    else $totReprobadas++;
                                    echo $calif;
                                ?>
                            </td>
                            <td style="text-align: center; font-size: 6px; font-family: Arial, Helvetica, sans-serif;">
                                <?php 
                                    if ( $materia->ordinario ) echo 'ORD'; 
                                    else echo 'EXT';
                                ?>
                            </td>
                            <td style="text-align: center; font-size: 6px; font-family: Arial, Helvetica, sans-serif;"><?php echo $materia->idc; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    
                </table>
            </div>
            <?php $par++; ?>
        <?php endforeach; ?>

    </div>

    <div style="float: left; padding: 5px; border: 1px solid #353535; width: 18%; margin-top: 15px; margin-left: 10px;">
        <table style="font-size: 8px; font-family: Arial, Helvetica, sans-serif;">
            <tr><td>MATERIAS APROBADAS</td><td style="text-align: right;"><?php echo $totAprobadas; ?></td></tr>
            <tr><td>MATERIAS REPROBADAS</td><td style="text-align: right;"><?php echo $totReprobadas; ?></td></tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr><td>PROMEDIO</td><td style="text-align: right;"><?php echo number_format( $sumCalif/$totMaterias, 2, '.', ',' ); ?></td></tr>
        </table>
    </div>

    <div style="clear: both;">&nbsp;</div>
    <div style="font-size: 10px; text-align: center; margin-top: 10px; margin-bottom: 10px;">
        EL PRESENTE DOCUMENTO CUENTA CON VALIDEZ OFICIAL ÚNICAMENTE PARA LA GESTIÓN DE LA BECA DE MANUTENCIÓN DEL GOBIERNO DEL ESTADO DE PUEBLA 2021
    </div>
    
    <div style="text-align: center;">
        <strong>ATENTAMENTE:</strong><br />
        <strong>&quot;Un pueblo sano, hace una nación vigorosa&quot;</strong><br />
        H. Puebla de Z.; a <?php echo $fecha; ?>
    </div>
    <div style="text-align: center; background-image: url('<?php echo base_url('img/credencial/firmarector.jpg') ?>'); background-repeat: no-repeat; background-position: center; background-size: 13%; ">
        <p style="margin-top: 50px;">
            <strong>Dr. José Hugo Eloy Meléndez Aguilar</strong><br />
            Rector
        </p>
    </div>
    <div style="text-align: right; padding-right: 70px; ">
        <img src="<?php echo base_url('img/sellos/sep-ses-usep.png');?>" width="160" />
    </div>
</div>
