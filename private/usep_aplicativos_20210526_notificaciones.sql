create table aplicativo_notificaciones(
    notificacion_id INT  UNSIGNED  AUTO_INCREMENT NOT NULL PRIMARY KEY,
    notificacion_creador VARCHAR(60),
    notificacion_destinatario VARCHAR(60) NOT NULL,
    notificacion_texto TEXT NOT NULL,
    notificacion_hora TIME NOT NULL,
    notificacion_status ENUM('NUEVA','LEIDA') NOT NULL DEFAULT 'NUEVA',
    created_at DATE,
    updated_at DATE,
    deleted_at DATE,
    FOREIGN KEY (notificacion_creador) REFERENCES aplicativo_usuarios(usuario_correo),
    FOREIGN KEY (notificacion_destinatario) REFERENCES aplicativo_usuarios(usuario_correo)
);