<?php
namespace App\Entities;

use CodeIgniter\Entity;

class BitacoraAulasView extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'aula_id' => NULL,
        'grupo' => NULL,
        'dia' => NULL,
        'h_inicio' => NULL,
        'h_fin' => NULL,
        'aula' => NULL,
        'docente' => NULL,
        'materia' => NULL,
        'periodo' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'aula_id' =>'gpoaula_aula',
        'grupo' => 'gpoaula_grupo',
        'dia' => 'gpoaula_dia',
        'h_inicio' => 'gpoaula_inicio',
        'h_fin' => 'gpoaula_fin',
        'aula' => 'aula_nombre',
        'docente' => 'grupo_docente',
        'materia' => 'grupo_materia',
        'periodo' => 'grupo_periodo',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}