<?php

namespace App\Models;

use CodeIgniter\Model;

class ListaEscolarModel extends Model
{
    protected $table      = 'view_listagrupos';
    protected $primaryKey = 'gpoalumno_alumno';

    protected $returnType    = 'App\Entities\ListaEscolar';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }


    public function buscar($filtros = array())
    {
        $pager = \Config\Services::pager();
        $this->creaFiltros($filtros);
        $alumnos = $this->paginate(50);
        return $alumnos;
    }
    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {

        if (array_key_exists('rol', $filtros)) { // -- filtrar por periodo

            if (($filtros['rol']) == 'enfermeria') {
                $this->where('materia_carrera', 1);
                if (array_key_exists('id', $filtros)) { // -- filtrar por ID
                    if (strlen($filtros['id'])) {
                        $this->where('grupo_id', $filtros['id']);
                    }
                }
                if (array_key_exists('seccion', $filtros)) { // -- filtrar por seccion
                    if (strlen($filtros['seccion'])) {
                        $this->like('grupo_clave', trim($filtros['seccion']), 'both');
                    }
                }
                if (array_key_exists('iddocente', $filtros)) { // -- filtrar por id de docente
                    if (strlen($filtros['iddocente'])) {
                        $this->like('grupo_docente', trim($filtros['iddocente']), 'both');
                    }
                }
                if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
                    if (strlen($filtros['periodo'])) {
                        $this->like('grupo_periodo', trim($filtros['periodo']), 'both');
                    }
                }
            }
            if (($filtros['rol']) == 'medicina') {
                $this->where('materia_carrera', 2);
                if (array_key_exists('id', $filtros)) { // -- filtrar por ID
                    if (strlen($filtros['id'])) {
                        $this->where('grupo_id', $filtros['id']);
                    }
                }
                if (array_key_exists('seccion', $filtros)) { // -- filtrar por seccion
                    if (strlen($filtros['seccion'])) {
                        $this->like('grupo_clave', trim($filtros['seccion']), 'both');
                    }
                }
                if (array_key_exists('iddocente', $filtros)) { // -- filtrar por id de docente
                    if (strlen($filtros['iddocente'])) {
                        $this->like('grupo_docente', trim($filtros['iddocente']), 'both');
                    }
                }
                if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
                    if (strlen($filtros['periodo'])) {
                        $this->like('grupo_periodo', trim($filtros['periodo']), 'both');
                    }
                }
            }
            if (($filtros['rol']) == 'todos') {
                if (array_key_exists('id', $filtros)) { // -- filtrar por ID
                    if (strlen($filtros['id'])) {
                        $this->where('grupo_id', $filtros['id']);
                    }
                }
                if (array_key_exists('seccion', $filtros)) { // -- filtrar por seccion
                    if (strlen($filtros['seccion'])) {
                        $this->like('grupo_clave', trim($filtros['seccion']), 'both');
                    }
                }
                if (array_key_exists('iddocente', $filtros)) { // -- filtrar por id de docente
                    if (strlen($filtros['iddocente'])) {
                        $this->like('grupo_docente', trim($filtros['iddocente']), 'both');
                    }
                }
                if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
                    if (strlen($filtros['periodo'])) {
                        $this->like('grupo_periodo', trim($filtros['periodo']), 'both');
                    }
                }
            }
            if (($filtros['rol']) == 'docente') {
                $this->like('grupo_docente', trim($filtros['iddocente']), 'both');
            }
        }
        $this->groupBy('grupo_id');
        $this->orderBy('grupo_periodo','ASC');
        $this->orderBy('Ap_paterno', 'ASC');
        $this->orderBy('Ap_materno', 'ASC');
        $this->orderBy('Alumno_nombres', 'ASC');
    }
    public function buscargrupo($id)
    {
        $this->where('grupo_id', $id);
        $this->orderBy('Ap_paterno', 'ASC');
        $this->orderBy('Ap_materno', 'ASC');
        $this->orderBy('Alumno_nombres', 'ASC');
        return $this->find();
    }

    /**
     * @param periodo ID del periodo
     * @return registros conjunto de registros de un periodo seleccionado
     */
    public function getByPeriodo($periodo, $seccion = false)
    {
        $this->where('grupo_periodo', $periodo);
        if ($seccion) {
            $this->like('grupo_clave', $seccion, 'both');
        }
        $this->orderBy('grupo_id', 'ASC');
        $this->orderBy('grupo_materia', 'ASC');
        $this->orderBy('grupo_clave', 'ASC');
        $this->orderBy('Ap_paterno', 'ASC');
        $this->orderBy('Ap_materno', 'ASC');
        $this->orderBy('Alumno_nombres', 'ASC');
        $registros = $this->find();
        return $registros;
    }
}
