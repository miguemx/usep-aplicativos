<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .formulario-cabecera,
    .formulario-datos {
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }

    .contenedor {
        border: white solid 1px;
        border-radius: 10px;
    }
</style>
<br>
<?php if (isset($mensaje)) : ?>
    <div class="alert <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
    </div>
<?php endif; ?>

<?php if (isset($etapas)) : ?>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Etapas de Admision</h3>
            </div>
            <div class="container-fluid">
                <div class="p-5 bg-light">
                    Configuracion General

                    <table class="table table-sm  table-fluid text-center">
                        <thead class="thead-default">
                            <tr>
                                <th>Nombre</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                            <form action="editarConfiguracion" method="post" id="form-configuracion">
                            </form>
                            <?php if (isset($configuraciones)) : ?>
                                <?php foreach ($configuraciones as $conf_detalle) : ?>
                                    <tr>
                                        <td>
                                            <?php echo $conf_detalle->nombre ?>
                                        </td>
                                        <td>

                                            <div class="form-check form-switch">
                                                <input <?php echo ($conf_detalle->status == 1) ? "checked" : ""; ?> class="form-check-input" type="checkbox" id="conf_activas" name="conf_activas" value="<?php echo $conf_detalle->id ?>" onchange="cambiarEstado(this)">
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <form action="agregarConfiguracion" method="post" id="add-config">


                                <!-- Modal -->
                                <div class="modal fade" id="agregar_configuracion" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Agregar</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container-fluid">
                                                    <div class="mb-3">
                                                        <label for="name-config" class="form-label">Nombre</label>
                                                        <input type="text" class="form-control" name="name-config" id="name-config" aria-describedby="help-name" required>
                                                        <small id="help-name" class="form-text text-muted">Agrega un nombre que sera el que identificacra la configuracion</small>
                                                    </div>
                                                    <div class="form-check form-switch">
                                                        <input class="form-check-input" type="checkbox" id="conf-status" name="status-config">
                                                        <label for="conf-status" class="form-label">Estatus Inicial</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                <button type="button" class="btn btn-secondary" id="btn-agregar">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <tr>
                                <td colspan="3">
                                    <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#agregar_configuracion">Agregar Configuracion</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
            <?php if (!isset($etapasexistentes)) : ?>
                <div class="card-body">
                    <div class="text-center">
                        <h4>No hay Etapas Registradas</h4>
                        <form action="agregaretapa" method="post">
                            <div>
                                <br>
                                <button class="btn btn-secondary" type="submit">Agregar nueva etapa</button>
                            </div>
                            <td class="text-center"></td>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (isset($etapasexistentes)) : ?>
                <div class="card-body">
                    <table class="table  text-center">
                        <thead class="thead-default">
                            <tr>
                                <th>Periodo Etapa</th>
                                <th>Nombre de Etapa</th>
                                <th>Etapa Orden</th>
                                <th>Controles de Formularios </th>
                                <th>Controles de Etapas</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">

                            <?php foreach ($etapasexistentes as $etapa_actuales) : ?>
                                <tr>
                                    <td class="row1"><?php echo $etapa_actuales->etapa_periodo ?></td>
                                    <td class="row2"><?php echo $etapa_actuales->etapa_nombre ?></td>
                                    <td class="row3"><?php echo $etapa_actuales->etapa_orden ?></td>
                                    <td class="row4">
                                        <table class="table table-hover table-fluid table-borderless">
                                            <tbody class="text-center">

                                                <?php if ($etapa_actuales->etapa_formulario == 0) : ?>
                                                    <?php echo 'No tiene Formulario vinculado'; ?>
                                                <?php else : ?>
                                                    <?php if (isset($formularios)) : ?>
                                                        <?php foreach ($formularios as $formetapa) : ?>
                                                            <?php if ($formetapa->formulario_etapa ==  $etapa_actuales->etapa_id) : ?>
                                                                <tr>
                                                                    <td>
                                                                        <div>
                                                                            <?php echo $formetapa->formulario_nombre ?>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div>
                                                                            <?php echo $formetapa->formulario_orden ?>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="iconos-grupo">
                                                                            <div>
                                                                                <form action="detalleformulario" method="post">
                                                                                    <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                                                        <i class="material-icons">table_view</i>
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                            <div>

                                                                                <form action="editarformulario" method="post">
                                                                                    <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                                                        <i class="material-icons">mode_edit</i>
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                            <div>
                                                                                <form action="eliminaformulario" method="post" onsubmit="return confirmaFormulario();">
                                                                                    <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                                                        <i class="material-icons">delete_forever</i>
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <tr>
                                                        <td colspan="3" >
                                                            <form action="agregarFormulario" method="post">
                                                                <button class="btn btn-secondary" type="submit">Agregar nuevo Formulario</button>
                                                                <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                            </form>

                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="text-center row5">
                                        <div class="iconos-grupo">
                                            <div>
                                                <form action="modificaretapa" method="post">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                        <i class="material-icons">mode_edit</i>
                                                    </button>
                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                </form>
                                            </div>
                                            <div>
                                                <form action="eliminaretapa" method="post" onsubmit="return confirmaReset();">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                        <i class="material-icons">delete_forever</i>
                                                    </button>
                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <form action="agregaretapa" method="post">
                                    <td class="text-center" colspan="5"><button class="btn btn-secondary" type="submit">Agregar nueva etapa</button></td>
                                </form>
                            </tr>

                        </tbody>
                    </table>

                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<script>
    function confirmaReset() {
        return confirm("Al confirmar esta ventana no podrá recuperar los datos de formularios generados en esta etapa. \n Por favor confirme que desea continuar");
    }

    function confirmaFormulario() {
        return confirm("¿Está seguro de que desea eliminar el formulario seleccionado?. \n Por favor confirme que desea continuar");
    }

    

    let button_agregar = document.getElementById("btn-agregar");
    button_agregar.addEventListener("click", function() {
        let name = document.getElementById('name-config').value;
        if (name != '') {
            document.getElementById('add-config').submit();
        } else {
            alert("Nombre Vacio");
        }
    });

    function actualizar(item) {
        let configuracion = item.value;
        console.log(configuracion);


    }
    var cambiarEstado = async (config) => {
        //console.log(config);
        let datos = config.value;
        const formData = new FormData();
        formData.append('datos', config.value);
        let response = await fetch('<?php echo base_url("ControlAdmision/updateConfiguracion"); ?>', {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            console.error('Error:', error);
        });

        alert(response.texto);
    }
</script>
<?php echo $this->endSection() ?>