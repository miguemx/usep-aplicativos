<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
    <?php if (isset($mensajefinaliza)) : ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $mensajefinaliza; ?>
            </div>
            <?php endif; ?>
    <?php if (isset($validafinalizar)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $validafinalizar; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)) :?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensaje)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensaje; ?>
    </div>
    <?php endif; ?>
    <?php if(isset($errorfinalizaregistro)) :?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorfinalizaregistro; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensajeexito)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensajeexito; ?>
    </div>
    <?php endif; ?>
    <?php if(isset($mensajecorreo)) :?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajecorreo; ?>
        </div>
    <?php endif; ?>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/padron'); ?>">
        <h1>Registro de padrón</h1>
        <div class="row" style="padding-top: 10px;">
            <div class="mb-1 row col-sm-4" >
                <label for="matricula">Estudiantes</label>
                <div class="col-sm-10" style= 'text-align:"center"'>
                    <input type='radio' id="estudiante" name="checkfiltro" value="0" onchange="this.form.submit();" onclick="muestratabla(this.id)"<?php if($check1 == "0") echo 'checked'; ?>/>
                </div>
            </div>
            <div class="mb-1 row col-sm-4">
                <label for="matricula">Empleados</label>
                <div class="col-sm-10">
                    <input type='radio' id="empleado" name="checkfiltro" onclick="muestratabla(this.id)" value="1" onchange="this.form.submit();" <?php if($check2 == 1) echo 'checked';; ?>/>
                </div>
            </div>
            <div class="mb-1 row col-sm-4">
                <label for="matricula">Registro individual</label>
                <div class="col-sm-10">
                    <input type='radio' id="externo" name="checkfiltro" onclick="muestracampos(this.id)" value="2" />
                </div>
            </div>
        </div>
        <div id="correo" style="display:none">
            <label for="correoproceso" class="form-label">Ingresa correo</label>
            <input type="text" class="form-control" style="justify-content:center" id="correoproceso" name="correoproceso">
        </div>
        <div id="idproceso" >
                        <label for="selectproceso" class="form-label">Procesos</label>
                        <br>
                        <select id="selectproceso " class="custom-select" style="width: 80%;" name="selectproceso" required>
                            <option value = "">Selecciona procesos</option>
                            <?php foreach ($procesos as $arregloprocesos) : ?>
                                <option value = <?php echo $arregloprocesos->id ?>><?php
                                        echo $arregloprocesos->nombre
                                        ?></option>
                            <?php endforeach; ?>
                        </select>
        </div>
        <br>
        <br>
        <div class="guardar" style="display: table-cell;  text-align: center;">
            <button type="submit" name='btnguardar'  value="1" class="btn btn-secondary" >Guardar</button>
            <button type="submit" name='btnguardafinaliza'  value="1" class="btn btn-secondary" >Guardar y finalizar</button>
        </div>
        <div class="guardar" style="display:flex; justify-content:center">
        </div>
        <div class="table-responsive tabla" id='tabla' >
            <table name='calif_arr' class="table table-light text-center">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Correo</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody <?php if (isset($banderaalumno)) {echo "hidden";} ?>><?php
                    $contador = 0;
                    foreach ($padronalumnos as $padronalumno) : ?>
                    <?php $contador = $contador + 1; ?>
                        <tr>
                            <td><?php echo $contador?></td>
                            <td><?php
                            echo $padronalumno->correo; ?>
                            </td>
                            <td class="text-left" disabled><?php echo $padronalumno->alumno_ap_paterno . " " . $padronalumno->alumno_ap_materno . " " . $padronalumno->nombres ?></td>
                        </tr>
                            <?php endforeach; ?>
                </tbody>
                <tbody <?php if (isset($banderaempleados)) {echo "hidden";} ?>><?php
                    $contador = 0;
                    foreach ($padronempleados as $padronempleado) : ?>
                    <?php $contador = $contador + 1; ?>
                        <tr>
                            <td><?php echo $contador?></td>
                            <td><?php
                            echo $padronempleado->correo; ?>
                            </td>
                            <td class="text-left" disabled><?php echo $padronempleado->apPaterno. " " .$padronempleado->apMaterno. " " .$padronempleado->nombre  ?></td>
                        </tr>
                            <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
<script>
    function muestratabla(id){
        console.log(id);
        let checkid = document.getElementById(id);
        if ( checkid.checked === true ) {
			document.getElementByclass("tabla").style.display = "block";
        }
    }
</script>
<script>
    function muestracampos(id){
        console.log(id);
        let checkid = document.getElementById(id);
        if ( checkid.checked === true ) {
			document.getElementById("correo").style.display = "block";
			document.getElementById("tabla").style.display = "none";
        }else{
			document.getElementById("correo").style.display = "none";
        }
    }
</script>

<?php echo $this->endSection() ?>