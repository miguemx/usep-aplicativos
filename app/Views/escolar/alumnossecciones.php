<?php

use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Php;

echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .container {
        padding-top: 20px;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .input-error {
        border: 1px red solid !important;
    }
</style>
<?php if ($mensaje) : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="container" style="padding-bottom: 20px; height:80%;">
            <?php if (isset($busqueda)) : ?>
                <form action="<?php echo base_url('Escolar/alumnoSeccionActual') ?>" method="post">
                    <div class="mb-3">
                        <label for="matricula" class="form-label">MATRICULA</label>
                        <input type="text" class="form-control" name="matricula" id="matricula" aria-describedby="helpId">
                        <small id="helpId" class="form-text text-muted">Ingrese la matricula del alumno para buscar su seccion</small>
                    </div>
                    <div class="text-center">
                        <button type='submit' class='btn btn-secondary'>Buscar</button>
                    </div>
                </form>
            <?php elseif (isset($secciones)) : ?>
                <form action="<?php echo base_url('Escolar/cambioSeccion') ?>" method="post">
                    <div class="card-body">
                        <input type="hidden" name="matricula" value="<?php echo $alumno->id ?>">
                        <div class="datos text-center">
                            <div class="datos text-center">
                                <label style="margin: 0 10px">MATRICULA:<h4 class="card-title"> <?php echo $alumno->id;  ?></h4> </label>
                                <label style="margin: 0 10px">NOMBRE:<h4 class="card-title"> <?php echo $alumno->apPaterno . " " . $alumno->apMaterno . " " . $alumno->nombre;  ?></h4> </label>
                                <label style="margin: 0 10px">PERIODO :<h4 class="card-title"> <?php echo $alumno->periodo;  ?></h4> </label>
                            </div>
                        </div>
                        <h4 class="card-title"> </h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class=" text-center">SECCION</th>
                                    <th class=" text-center">CLAVE MATERIA</th>
                                    <th>MATERIA</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($materias_actuales as $valores => $datos) : ?>
                                    <tr>
                                        <td class='text-center'><?php echo $datos->seccion ?></td>
                                        <td class='text-center'><?php echo $datos->grupo_materia ?></td>
                                        <td><?php echo $datos->materia_nombre ?></td>
                                    </tr>
                                <?php endforeach;  ?>

                            </tbody>
                        </table>
                        <div class="mb-3">
                            <label for="clave_seccion" class="form-label">Cambio de Sección</label>
                            <!-- <input type="text" class="form-control" name="clave_seccion" id="clave_seccion" aria-describedby="helpId" placeholder=""> -->

                            <select class="form-control" name="clave_seccion" id="clave_seccion">
                                <option>Selecciona una seccion disponible</option>
                                <?php foreach ($secciones as $seccion) : ?>
                                    <option value="<?php echo $seccion->clave ?>"><?php echo $seccion->clave ?></option>
                                <?php endforeach; ?>
                            </select>
                            <small id="helpId" class="form-text text-muted">Ingrese la sección a la que desea mover al alumno para verificar las materias que podrán ser asignados</small>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-secondary" type="submit">Buscar Sección</button>
                    </div>
                </form>
            <?php elseif (isset($materias_cambio_seccion)) : ?>
                <div>
                    <div>
                        <form action="<?php echo base_url('Escolar/reasignacionAlumno/' . $alumno->id . "/" . $alumno->periodo."/".$seccion_siguiente) ?>" method="post" id="form-cambioseccion">

                            <div>
                                <div class="datos text-center">
                                    <div class="datos text-center">
                                        <label style="margin: 0 10px">MATRICULA:<h4 class="card-title"> <?php echo $alumno->id;  ?></h4> </label>
                                        <label style="margin: 0 10px">NOMBRE:<h4 class="card-title"> <?php echo $alumno->apPaterno . " " . $alumno->apMaterno . " " . $alumno->nombre;  ?></h4> </label>
                                        <label style="margin: 0 10px">PERIODO :<h4 class="card-title"> <?php echo $alumno->periodo;  ?></h4> </label>
                                    </div>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>SECCION</th>
                                            <th>CLAVE MATERIA</th>
                                            <th>MATERIA</th>
                                            <th>ACEPTAR</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <tbody>
                                        <?php foreach ($materias_cambio_seccion as $valores => $datos) : ?>
                                            <tr>
                                                <td><?php echo $datos->grupo_clave ?></td>
                                                <td><?php echo $datos->grupo_materia ?></td>
                                                <td><?php echo $datos->materia_nombre ?></td>
                                                <td class='text-center'>
                                                    <div class='form-check form-switch'>
                                                        <?php if ($datos->permiso) : ?>
                                                            <input checked class='form-check-input' type='checkbox' id='id_materia' name='id_materia[]' value='<?php echo $datos->grupo_id ?>'>
                                                        <?php endif; ?>
                                                    </div>
                                                </td>
                                            </tr>

                                        <?php endforeach; ?>

                                    </tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-secondary" id="btn-confimacion" onclick="confirmacion()">Cambiar seccion</button>
                            </div>
                        </form>
                    </div>


                </div>
            <?php endif; ?>
        </div>


    </div>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        'use strict';

        function confirmacion() {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! Los cambios que se realizaran tendrán efecto de inmediato y no se podrán cancelar ¿Está seguro que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',

            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('form-cambioseccion').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }

        function regresar() {
            document.getElementById('buscar_seccion').submit();
            // console.log('hola');
        }
    </script>

    <?php echo $this->endSection() ?>