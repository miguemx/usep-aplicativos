<?php

namespace App\Entities;

use CodeIgniter\Entity;

class EvaluacionesFormulario extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'nombre' => null,
        'evaluacion' => null,
        'orden' => null,
        'instrucciones' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'id' => 'formulario_id',
        'nombre' => 'formulario_nombre',
        'evaluacion' => 'formulario_evaluacion',
        'orden' => 'formulario_orden',
        'instrucciones' => 'formulario_instrucciones',

        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
