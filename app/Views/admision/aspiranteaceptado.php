<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<style>

.input-grupo.input-grupo-outline .formulario-control {
    background: none;
    border: 1px solid red;
    border-radius: 0.375rem;
    border-top-left-radius: 0.375rem !important;
    border-bottom-left-radius: 0.375rem !important;
    padding: 0.625rem 0.75rem
rem
 !important;
    line-height: 1.3 !important;
}

.div-select{

}

.seleccion{
    border: 1px red solid !important;
}

.clase1{
    display: none;
}

.input-error {
        border: 3px red solid !important;
        border-radius: 10px;
        border-top: 0px !important;
        border-left: 0px !important;
    }

</style>
<div class="container"> 
    <?php if(isset($msg)): ?>
        <div class="alert alert-secondary">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
              <div class="card card-plain">
                <div class="card-header">
                  <h4 class="font-weight-bolder">Solicitud Exitosa</h4>
                  <p class="mb-0">
                  <?php echo $aspirante->nombre ?>, su validación de documentos ha concluido satisfactoriamente, por lo que se habilitó el apartado de descarga de &quot;Ficha de Examen&quot;. Esta actividad deberá realizarla del <strong>lunes 20 al jueves 30 de junio del 2022</strong><br /><br />
                      
                  </p>
                </div>
                <div class="card-body">
                  
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <?php if ($band_tiempo){ ?>
                  <p class="mb-2 text-sm mx-auto" style="font-size: 20px !important;">
                  Puede descargar su ficha para el examen de admisión dando clic
                    <a href="<?php echo "Admision/fichaExamen"; ?>" class="text-primary text-gradient font-weight-bold">aquí</a>
                  </p> <?php } ?>
              </div>

</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<?php echo $this->endSection() ?>