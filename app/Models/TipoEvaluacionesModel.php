<?php

namespace App\Models;

use CodeIgniter\Model;

class TipoEvaluacionesModel extends Model
{
    protected $table = 'aplicativo_tipo_evaluacion';
    protected $primaryKey = 'tipo_id';
    protected $returnType = 'App\Entities\TipoEvaluaciones';
    protected $useSoftDeletes = false;
    protected $allowedFields = ['tipo_id', 'tipo_nombre'];
    protected $db;
    protected $builder;

    protected $useTimestamps = false;

}
