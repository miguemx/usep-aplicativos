<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<?php
$completadas = 0;
foreach ($etapas as $etapa) {
  if ($etapa->completado == true) $completadas++;
}
?>

<h4>Inicio</h4>
<hr />
<div class="container">
  <?php if (isset($msg)) : ?>
    <div class="alert alert-secondary">
      <?php echo $msg; ?>
    </div>
  <?php endif; ?>

  <div class="row mb-4">
    <?php if ($aspirante->estatus != 'DECLINADO' && $aspirante->estatus != 'EXITOSO') :  ?>
      <div class="col-lg-8 col-md-6 mb-md-0 mb-4">

        <div class="card mb-4">
          <div class="card-body">
            En el panel izquierdo, encontrará los apartados disponibles, mismos que deberá capturar para continuar con su proceso.
          </div>
        </div>
        <div class="card">
          <div class="card-header pb-0">
            <div class="row">
              <div class="col-lg-6 col">
                <h6>Mis notificaciones y anuncios</h6>
              </div>
              <?php if ($aspirante->estatus == "EXITOSO" || $aspirante->estatus == "DECLINADO" || $aspirante->estatus == "OBSERVACIONES") : ?>
                <p>
                  <strong>
                    Solicitud con <?php echo mb_strtolower($aspirante->estatus); ?>
                  </strong>
                </p>
              <?php endif; ?>
              <?php if ($aspirante->estatus == "OBSERVACIONES") : ?>
                <img src="<?php echo base_url().'/img/icons/file-alert.png'; ?>" style="width: 50px;" />
                <p>
                  Es importante que revise el apartado de &quot;Carga de documentos&quot; debido a que cuenta con observaciones. 
                  Recuerde que debe subsanarlas ántes de la fecha de cierre de registro y que sólo cuenta con una oportunidad de corrección.
                </p>
              <?php endif; ?>
                  
                <p style="display: none;">
                      <strong>Cierre de registro.</strong><br />
                      Le informamos que la etapa de &quot;Registro de personas aspirantes y carga de documentos&quot; concluye el <strong>30 de mayo de 2022</strong>, 
                      por lo que le invitamos a concluir el mismo a la brevedad. En caso contrario, conforme a lo establecido en la 
                      &quot;Convocatoria de admisión&quot; su solicitud será <strong>declinada</strong>
                </p>

                

            </div>
          </div>
          <div class="card-body px-0 pb-2">
              <?php if ($notificaciones != '') : ?>
                  <?php
                  $contador = 0;
                  foreach ($notificaciones as $notificacion_lista) {
                    $contador = ($notificacion_lista->leido == null) ? $contador + 1 : $contador;
                  } ?>
                  <p class="text-sm mb-0 ml-2" style="margin-left: 10px;">
                    <i class="fa fa-check text-info" aria-hidden="true"></i>
                    <span class="font-weight-bold ms-1"><?php echo $contador . " notificaciones nueva(s)" ?></span>
                  </p>

                <?php endif; ?>
            <div class="table-responsive">
              <table class="table align-items-center mb-0">
                <thead>
                  <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Notificación</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Fecha <i class="fa-solid fa-angles-right"></i></th>

                  </tr>
                </thead>
                <tbody>
                  <?php if ($notificaciones != '') : ?>

                    <?php foreach ($notificaciones as $notificacion_lista) : ?>
                      <tr>
                        <td style="white-space: normal;">
                          <div class="d-flex px-2 py-1">
                            <?php $cabecera = explode(':', $notificacion_lista->mensaje) ?>

                            <div class="mb-0 text-sm"> <?php echo ($notificacion_lista->leido == null) ? "<i class='fa fa-check text-info' aria-hidden='true'></i>" : '';  ?><strong><?php echo " " . $cabecera[0] . ":" ?></strong> <?php echo $cabecera[1] ?></div>
                          </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                          <span class="text-xs font-weight-bold"> <?php $fecha = explode(' ', $notificacion_lista->updated_at);
                                                                  echo $fecha[0]; ?></span>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>

                  <!-- <tr>
            <td>
              <div class="d-flex px-2 py-1">
                <div class="mb-0 text-sm">Por favor vuelve a subir tu acta de nacimiento. Documento ilegible</div>
              </div>
            </td>
            <td class="align-middle text-center text-sm">
              <span class="text-xs font-weight-bold"> 2022-04-01</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="d-flex px-2 py-1">
                <div class="mb-0 text-sm">La CURP debe ser de este año</div>
              </div>
            </td>
            <td class="align-middle text-center text-sm">
              <span class="text-xs font-weight-bold"> 2022-04-01</span>
            </td>
          </tr> -->

                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

      <!-- ######################## progreso ################################## -->
      <div class="col-lg-4 col-md-6">
        <div class="card h-100">
          <div class="card-header pb-0">
            <h6>Progreso actual</h6>
            <p class="text-sm">
              <i class="fa fa-arrow-up text-success" aria-hidden="true"></i>
              <span class="font-weight-bold"><?php echo $completadas; ?> / <?php echo count($etapas); ?> </span> etapas concluidas
            </p>
          </div>
          <div class="card-body p-3">
            <div class="timeline timeline-one-side">

              <?php foreach ($etapas as $etapa) : ?>
                <div class="timeline-block mb-3">
                  <span class="timeline-step">
                    <i class="material-icons text-gradient <?php if ($etapa->completado == true) echo 'text-success';
                                                            else if ($etapa->actual == true) echo 'text-primary';
                                                            else echo 'text-dark'; ?>">circle</i>
                  </span>
                  <div class="timeline-content">
                    <h6 class="text-dark text-sm font-weight-bold mb-0"><?php echo $etapa->nombre ?></h6>
                    <p class="text-secondary text-xs mt-1 mb-0">
                      <?php
                      if ($etapa->completado == true) echo 'Etapa concluida. ';
                      else if ($etapa->actual == true) echo 'Etapa actual.';
                      else echo 'Etapa pendiente.';
                      ?>
                    </p>
                  </div>
                </div>
              <?php endforeach; ?>


            </div>
          </div>
        </div>
      </div>
      <!-- ######################## END progreso ################################## -->
    <?php elseif ($aspirante->estatus == 'EXITOSO') : ?>

      <div class="card card-plain">
        <div class="card-header">
        <img src="<?php echo base_url().'/img/icons/correct.png'; ?>" width="50" />
          <h4 class="font-weight-bolder">Solicitud Exitosa</h4>
          <!--<p class="mb-0">
            <?php //echo $aspirante->nombre ?>, su validación de documentos ha concluido satisfactoriamente, por lo que se habilitará el apartado de descarga de &quot;Ficha de Examen&quot;. Esta actividad deberá realizarla del <strong>lunes 20 al jueves 30 de junio del 2022</strong><br /><br />

          </p>!-->
          <p class="mb-0">
            <?php echo $aspirante->nombre ?>, le informamos que la ficha de examen se encontrará disponible en este "Portal de Aspirantes" a partir de las <b>20:00 horas del día 20 de junio y hasta las 23:59 horas del 30 de junio del 2022.</b></strong><br /><br />
</strong><br /><br />

          </p>
        </div>
        <div class="card-footer text-center pt-0 px-lg-2 px-1">
          <?php if ($band_tiempo) { ?>
            <p class="mb-2 text-sm mx-auto" style="font-size: 20px !important;">
              Puede descargar su ficha para el examen de admisión dando clic
              <a href="<?php echo base_url().'/Admision/fichaExamen'; ?>" class="text-primary text-gradient font-weight-bold">aquí</a>
            </p> <?php } ?>
        </div>
      </div>
          <?php if ($aspirante->aspirante_aceptado=="1") { ?>
            <div class="card">
              <div class="card-body">
                 <div class="card-plain text-justify pt-0 px-lg-2 px-1">
                 <p class="mb-0" align="right">
                 Puebla, Pue., a 18 de julio de 2022
                </p>
                <p class="mb-0" align="right">
                <h2 class="text-center">CARTA DE ACEPTACIÓN</h2>
                </p>
                 <p class="mb-0">
                  <h3><?php echo $aspirante->nombre.' '.$aspirante->apPaterno.' '.$aspirante->apMaterno.' ';?>.</h3>
                </p>
              <p class="mb-0">
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Me es grato saludarle y al mismo tiempo informarle que, gracias a su esfuerzo y dedicación, el puntaje obtenido
                 en el proceso de admisión 2022 ha sido suficiente para
                obtener un lugar dentro de la “Universidad de la Salud” del Estado de Puebla.
              </p>
              <p class="mb-0 text-center">
                <!--<h3 class="text-center">Puntaje:<b> <?php// echo $aspirante->aspirante_puntaje; ?></b> puntos.</h3>-->
                <h3 class="text-center">Puntaje:<b> <?php echo number_format((float)$aspirante->aspirante_puntaje, 3, '.', ''); ?></b> puntos.</h3>
              </p>
              <p class="mb-0">            
              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Ahora, podrá formar parte de la comunidad USEP y realizará sus estudios dentro del área de la salud, misma que requiere de total entrega, vocación, esfuerzo, honestidad,
                nobleza y sentido humanista desde el primer día de clases.
              </p>
              <p class="mb-0">
              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp En USEP potencializamos la formación de nuestros estudiantes al brindarles la más alta calidad en docentes, laboratorios, simuladores, instrumentos de enseñanza - aprendizaje,
                convenios colaborativos, becas y recursos tecnológicos, seguidos del puntual acompañamiento de toda la planta administrativa que conformamos la Universidad.
                Con ello garantizamos que nuestros estudiantes adquieran conocimientos y competencias que les permitan enfrentar los nuevos retos que en materia de salud exige nuestra sociedad. 
              </p>
              
                <p class="mb-0">
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <b>Es importante consultar el “Comunicado de inscripción” para conocer los requisitos y el procedimiento que deberá cumplir para formalizar su estatus de estudiante. </b>
                </p>
                <p class="mb-0">
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <b>La CITA programada, para formalizar su inscripción, en las instalaciones de nuestra universidad es el día 
                  <?php
                  $fechainsc=strtotime($aspirante->aspirante_inscripcion_fecha); 
                  $diassemana = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                  echo $diassemana[date('w',$fechainsc)]." ".date('d',$fechainsc)." de ".$meses[date('n',$fechainsc)-1]. " del ".date('Y',$fechainsc).",";?>
                   a las <?php echo date('H:i',strtotime($aspirante->aspirante_inscripcion_hora)); ?> horas. Le solicitamos ser puntual.</b>
                </p>
                <p class="mb-0 text-center">
                <b>¡Felicidades y éxito en esta nueva etapa!</b>
                </p>
                <p class="mb-0 text-center">
                <b>Atentamente <br>
                  “Un pueblo sano, hace una nación vigorosa”</b>
                </p>

                <p class="mb-0 text-center"><b>C. José Hugo Eloy Meléndez Aguilar</b></p>
                <p class="mb-0 text-center"><b>Rector</b></p>
              </b>
            </div>
              </div>
            </div>
           
            <?php } elseif ($aspirante->aspirante_aceptado=="0"){?>
              <div class="card">
              <div class="card-body">
                 <div class="card-plain text-justify pt-0 px-lg-2 px-1">
                  
                 <p class="mb-0" align="right">
                 Puebla, Pue., a 18 de julio de 2022
                </p>
                <p class="mb-0" align="right">
                <h2 class="text-center">RESULTADO DEL PROCESO DE ADMISIÓN 2022</h2>
                </p>
                 <p class="mb-0">
                 <h3><?php echo $aspirante->nombre.' '.$aspirante->apPaterno.' '.$aspirante->apMaterno.' ';?>.</h3>
                </p>
                <p class="mb-0">
                  Le informamos que, su puntaje obtenido derivado del proceso de admisión 2022 es:
                </p>
                <p class="mb-0">
                  Puntaje: <b> <?php echo $aspirante->aspirante_puntaje; ?></b> puntos.
                </p>
                <p class="mb-0">
                  Mismo que no ha sido suficiente para obtener un lugar dentro de los ofertados en la “Universidad de la Salud” del Estado de Puebla.
                  Reconocemos su esfuerzo y agradecemos su interés en formar parte de la USEP.
                </p>
                </div>
              </div>
            </div>
            <?php } ?>

        
    <?php elseif ($aspirante->estatus == 'DECLINADO') : ?>
      <div class="card card-plain">
        <div class="card-header">
          <img src="<?php echo base_url().'/img/icons/wrong.png'; ?>" width="50" />
          <h4 class="font-weight-bolder">Solicitud declinada</h4>
          <p class="mb-0">
            <?php echo $aspirante->nombre ?>, le informamos que, al revisar la validación de documentos, se han identificado las siguientes observaciones: .<br /><br />
            <ul>
            <?php foreach ($observaciones as $observacion) : ?>
              <li>
                <?php echo '<strong>'.$observacion->nombreCampo.':</strong> '.preg_replace("/\n/",'. ',$observacion->comentario); ?>
              </li>
            <?php endforeach; ?>
            </ul>
        <p>
          Por lo anterior y conforme a lo establecido en la &quot;Convocatoria de Admisión 2022&quot;, su solicitud ha sido declinada.
        </p>
        <p>
          Agradecemos el interés en formar parte de la comunidad USEP y le invitamos a permanecer pendiente de próximas convoctorias.
        </p>
        </div>
        <div class="card-body">

        </div>
        <div class="card-footer text-center pt-0 px-lg-2 px-1">
          <p class="mb-2 text-sm mx-auto" style="font-size: 20px !important;">

          </p>
        </div>

      </div>
    <?php endif; ?>



  </div>
</div>




<?php echo $this->endSection() ?>