<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    
<div class="container-fluid">
    <?php if(isset($error)): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php if(isset($exito)): ?>
        <div class="alert alert-success" role="alert">
            Archivo procesado con éxito.
        </div>
    <?php endif; ?>

    <form class="row g-3" method="post" action="<?php echo base_url('CapitalHumano/SubirRegistros'); ?>" enctype="multipart/form-data">
        <div class="col-sm-12">
            <label for="fechaInicio" class="form-label">Seleccione el archivo con los registros</label>
            <input type="file" class="form-control" id="archivo" name="archivo" >
        </div>
        
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Subir y Procesar</button>
        </div>
        
    </form>
</div>


<?php echo $this->endSection() ?>
