<?php 

namespace App\Libraries;

use \Google\Client;
use \Google\Service\Classroom;
use \Google\Service\Classroom\Course;
use \Google\Service\Classroom\Teacher;
use Google\Service\Classroom\Student;
use \Google\Service\Exception;


class GoogleClassroomLibrary {

    private $client = null;
    private $clientSecretFile = '';
    private $tokenPath = '';
    private $errors = [];
    private $lastError = '';

    /**
     * construye la instancia del manejador de Google Classroom.
     * Ajusta las propiedades de los archivos de permisos y credenciales
     * e inicializa el 
     */
    public function __construct() {
        $this->clientSecretFile = ROOTPATH.'private/client_secret_webmaster.json';
        $this->tokenPath = WRITEPATH.'/token.json';
        $this->setClient();
    }

    /**
     * devuelve el ultimo mensaje de error generado
     */
    public function getLastError() {
        return $this->lastError;
    }

    /**
     * realiza la conexión del cliente con la api solicitada
     */
    private function setClient()
    {
        $this->client = new Client();
        $this->client->setApplicationName('Google Classroom API PHP Quickstart');
        $this->addScopes();
        $this->client->setAuthConfig( $this->clientSecretFile );
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');
        try {
            if (file_exists($this->tokenPath)) {
                $accessToken = json_decode(file_get_contents($this->tokenPath), true);
                $this->client->setAccessToken($accessToken);
            }
            if ($this->client->isAccessTokenExpired()) { // If there is no previous token or it's expired.
                if ($this->client->getRefreshToken()) { // Refresh the token if possible, else fetch a new one.
                    $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
                } 
                else {
                    $authUrl = $this->client->createAuthUrl();
                    printf("Open the following link in your browser:\n%s\n", $authUrl);
                    print 'Enter verification code: ';
                    $authCode = trim(fgets(STDIN));
                    // Exchange authorization code for an access token.
                    $accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
                    $this->client->setAccessToken($accessToken);

                    if (array_key_exists('error', $accessToken)) {
                        throw new Exception(join(', ', $accessToken));
                    }
                }
                if (!file_exists(dirname($this->tokenPath))) { // Save the token to a file.
                    mkdir(dirname($this->tokenPath), 0700, true);
                }
                file_put_contents($this->tokenPath, json_encode($this->client->getAccessToken()));
            }
        }
        catch ( Exception $ex ) {
            $this->errors[] = $e->getMessage();
            $this->lastError = $e->getMessage();
            log_message('error', "ERROR: {ex}", [ 'ex'=>$e ]);
            $this->client = null;
        }
    }

    /**
     * agrega los permisos de la api
     */
    private function addScopes() {
        if ( !is_null($this->client) ) {
            $this->client->addScope('https://www.googleapis.com/auth/classroom.profile.photos');
            $this->client->addScope('https://www.googleapis.com/auth/classroom.courses');
            $this->client->addScope('https://www.googleapis.com/auth/classroom.profile.emails');
            $this->client->addScope('https://www.googleapis.com/auth/classroom.rosters');
        }
    }

    /**
     * obtiene un arreglo con los datos de los cursos encontrados
     * devuelve un arreglo vacío si no se encuentra algún curso
     * @return cursos un arreglo con los datos de los cursos
     */
    public function lista() {
        $cursos = [];
        try{
            $service = new Classroom($this->client);
            $optParams = array( 'pageSize' => 1000 );
            $results = $service->courses->listCourses($optParams);

            if ( count( $results->getCourses() ) > 0) {
                foreach ($results->getCourses() as $course) {
                    $cursos[] = [ 'id'=>$course->getId(), 'nombre'=>$course->getName(), 'erollmentCode'=>$course->getEnrollmentCode(), 'status'=>$course->courseState ];
                }
            }
        }
        catch(Exception $e) {
            $this->errors[] = $e->getMessage();
            $this->lastError = $e->getMessage();
            log_message('error', "ERROR: {ex}", [ 'ex'=>$e ]);
        }
        return $cursos;
    }

    /**
     * crea un curso
     * @param materia el nombre de la materia como aparece en Classroom
     * @param seccion la sección que pertenece
     * @param descripcion la descripcion que aparece en google classroom
     * @param aula el Aula en el que se imparte el curso
     * @param correosMaestros un arreglo con los correos de los maestros [ 'correo1', 'correo2', ... ]
     */
    public function crea( $materia, $seccion, $descripcion, $aula, $correosMaestros ) {
        try {
            $service = new Classroom($this->client);
            $course = new Course([
                'name' => $materia,
                'section' => $seccion,
                'descriptionHeading' => '',
                'description' => $descripcion,
                'room' => $aula,
                'ownerId' => 'me',
                'courseState' => 'ACTIVE'
            ]);
            $course = $service->courses->create($course);
            foreach ( $correosMaestros as $maestro ) {
                $this->agregaMaestro( $course->id, $maestro );
            }
            // $this->enrollAsStudent( $course->id, $enrollmentCode, 'miguel.silva@usalud.edu.mx' );
            // $this->enrollAsStudent( $course->id, $enrollmentCode, 'emiliano.alonso@usalud.edu.mx' );
            log_message( 'notice', "Curso creado con $course->id de la seccion $seccion." );
            return $course;
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            $this->lastError = $e->getMessage();
            log_message('error', "ERROR: {ex}", [ 'ex'=>$e ]);
        }
        return false;
    }

    /**
     * actualiza un curso ya creado a través de su curseId
     * @param courseId el ID del curso como está en classroom
     * @param state la cadena de state como está en https://developers.google.com/classroom/reference/rest/v1/courses#coursestate
     * @return course el objeto Course de Classroom
     * @return false en caso de que no se pueda actualzar
     */
    function updateState($courseId, $state) {
        try {
            $service = new Classroom($this->client);
            $course = $service->courses->get($courseId);
            $course->courseState = $state;
            $course = $service->courses->update($courseId, $course);
            return $course;
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            $this->lastError = $e->getMessage();
            log_message('error', "ERROR: (Curso {curso}) {ex}", [ 'ex'=>$e, 'curso'=>$courseId ]);
        }
        return false;
        
    }

    /**
     * agrega un maestro a un curso a través de su dirección de correo
     * @param courseId el Id del curso de google classroom
     * @param teacherEmail el correo del maestro
     */
    public function agregaMaestro($courseId, $teacherEmail) {
        $service = new Classroom($this->client);
        $teacher = new Teacher([ 'userId' => $teacherEmail ]);
        try {    
            $teacher = $service->courses_teachers->create($courseId, $teacher);
        } catch (Exception $e) {
            if ($e->getCode() == 409) {
                log_message( "warning", "WARNING: Docente $teacherEmail ya existe en el grupo $courseId." );
            } 
            else {
                throw $e;
            }
        }
        return $teacher;
    }

    /**
     * agrega un estudiante a un curso de classroom mediante su correo electrónico a través del id de google
     * @param courseId el Id del curso en classroom
     * @param enrollmentCode el codigo de enrollment de classroom
     * @param correo el correo usep del estudiante
     */
    function agregaEstudiante($courseId,$enrollmentCode,$correo) {
        $service = new Classroom($this->client);
        $student = new Student([ 'userId' => $correo ]);
        try {    
            $params = [ 'enrollmentCode' => $enrollmentCode ];
            $student = $service->courses_students->create($courseId, $student, $params);
        } catch (Exception $e) {
            if ($e->getCode() == 409) {
                log_message( "warning", "WARNING: Estudiante $correo ya existe en el grupo $courseId." );
            } else {
                throw $e;
            }
        }
        return $student;
    }

    /**
     * archiva un curso en específico
     * @param courseId el ID del curso como está en classroom
     */
    public function archiva($courseId) {
        $this->updateState($courseId, 'ARCHIVED');
    }

    /**
     * archiva todos los cursos del usuario actual 
     */
    public function archivaTodos() {
        $cursos = $this->lista();
        $i = 0;
        foreach ( $cursos as $curso ) {
            if ( $curso['status'] != 'ARCHIVED' ) {
                $res = $this->archiva( $curso['id'] );
                if ( $res === false ) {
                    $this->errors[] = " - ERROR al archivar el curso ".$curso['id'].". Verifique el log.";
                }
                $i++;
            }
            if ( $i%20 == 0 ) sleep(5);
        }
    }

}