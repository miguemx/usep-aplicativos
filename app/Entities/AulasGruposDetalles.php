<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AulasGruposDetalles extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'grupo' => NULL,
        'aula' => NULL,
        'nombre' => NULL,
        'dia' => NULL,
        'inicio' => NULL,
        'fin' => NULL,
        'periodo' => NULL,
        'clave' => NULL,
        'materia' => NULL,
        'nombre_materia' => NULL,
        'docente' => NULL,
        'nombre_docente' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
        'grupo' => 'gpoaula_grupo',
        'aula' => 'aula_id',
        'nombre' => 'aula_nombre',
        'dia' => 'gpoaula_dia',
        'inicio' => 'gpoaula_inicio',
        'fin' => 'gpoaula_fin',
        'periodo' => 'gpoaula_periodo',
        'clave' => 'grupo_clave',
        'materia' => 'grupo_materia',
        'nombre_materia' => 'materia_nombre',
        'docente' => 'grupo_docente',
        'nombre_docente' => 'nombre_docente',
    ];
    
    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}