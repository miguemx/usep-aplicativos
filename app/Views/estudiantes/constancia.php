<div style="font-family: Arial, Helvetica, sans-serif; padding: 30px; padding-bottom: 1px; padding-top: 1px;">
    <table width="100%">
        <tr>
            <td style="text-align: left"><img src="<?php echo base_url('img/logos/USEP.png');  ?>"></td>
            <td style="text-align: right"><img src="<?php echo base_url('img/logos/Gobierno.png');  ?>"></td>
            <td style="text-align: left">&nbsp;<br />&nbsp;<br />&nbsp;</td>
            <td style="text-align: right"></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <div style="font-weight: bold; margin-top: 10px; font-family: Arial, Helvetica, sans-serif;"><br />ASUNTO: CONSTANCIA DE ESTUDIOS</div>
                <div style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">A QUIEN CORRESPONDA.</div>
            </td>
            <td style="text-align: right; font-family: Arial, Helvetica, sans-serif;" valign="top"><strong>Folio:</strong> <?php echo $folio; ?></td>
        </tr>
    </table>
</div>
<div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-image: url('<?php echo base_url('img/fondozaca.png') ?>'); background-repeat: no-repeat; background-position: left bottom; background-size: 70%; padding: 30px; padding-top: 5px;">
    <div>
        La Universidad de la Salud del Estado de Puebla, con dirección en Av. Reforma No. 722,
        Col. Centro, Puebla, Pue., Clave 21MSU0084Z, por medio de la presente:
    </div>
    <p style="text-align: center; font-weight: bold;">
        HACE CONSTAR QUE:
    </p>
    <div>
        <?php echo ucwords($genero); ?> C. <strong> <?php echo ucwords(mb_strtolower($nombre)); ?></strong> con número de matrícula <strong><?php echo $matricula; ?></strong>
        es estudiante de la <strong><?php echo $carrera ?></strong>, inscrit<?php echo $sufijo; ?> en el periodo escolar <strong><?php echo $periodo; ?></strong> que comprende
        del <?php echo "$periodoInicio al $periodoFin"; ?>.
        <br>
        <br>El cual contempla:

    </div>
    <div>
        <ul>
            <li>Periodo de evaluaciones: <?php echo $periodoEvaluaciones; ?></li>
            <li>Periodo vacacional: <?php echo $periodoVacaciones; ?></li>
            <li>Periodo de actividades académico - administrativas: <?php echo $periodoAdministrativas; ?></li>
            <?php if ($receso) : ?>
                <li>Periodo de receso escolar: <?php echo $periodoRecesoEscolar; ?></li>
            <?php endif; ?>
        </ul>
    </div>
    <p>
        <?php echo ucwords($genero); ?> estudiante se encuentra cursando actualmente el <?php echo $semestre; ?> con las siguientes asignaturas:
    </p>
    <div style="border: 1px solid #000; margin-left: 30px; margin-right: 30px;">
        <table width="100%">
            <tr>
                <th style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;">CLAVE</th>
                <th style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;">CREDITOS</th>
                <th style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;">ASIGNATURA</th>
                <th style="font-size: 11px; font-family: Arial, Helvetica, sans-serif;">IDC</th>
            </tr>
            <?php foreach ($asignaturas as $asignatura) : ?>
                <tr>
                    <td style="text-align: center; font-size: 10px; font-family: Arial, Helvetica, sans-serif;"><?php echo $asignatura->materiaClave; ?></td>
                    <td style="text-align: center; font-size: 10px; font-family: Arial, Helvetica, sans-serif;"><?php echo $asignatura->materia_creditos; ?></td>
                    <td style="font-size: 10px; font-family: Arial, Helvetica, sans-serif;"> <?php echo $asignatura->materia; ?></td>
                    <td style="text-align: center; font-size: 10px; font-family: Arial, Helvetica, sans-serif;"><?php echo $asignatura->idc; ?></td>
                </tr>
            <?php endforeach; ?>

        </table>
    </div>
    <p>
        Se extiende la presente constancia a solicitud de la persona interesada y para los fines que a ella convenga.
    </p>

    <div style="text-align: center;">
        <strong>ATENTAMENTE:</strong><br />
        <strong>&quot;Un pueblo sano, hace una nación vigorosa&quot;</strong><br />
        H. Puebla de Z.; a <?php echo $fecha; ?>
    </div>
    <div style="text-align: center; background-image: url('<?php echo base_url('img/credencial/firmarector.jpg') ?>'); background-repeat: no-repeat; background-position: center; background-size: 13%; ">
        <p style="margin-top: 50px;">
            <strong>Dr. José Hugo Eloy Meléndez Aguilar</strong><br />
            Rector
        </p>
    </div>
    <div style="text-align: right; margin-top: -40px;">
        <img src="<?php echo $qr ?>" width="90" />
    </div>
</div>