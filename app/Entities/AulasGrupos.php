<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AulasGrupos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => NULL,
        'grupo' => NULL,
        'dia' => NULL,
        'inicio' => NULL,
        'fin' => NULL,
        'aula' => NULL,
        'periodo' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
        'id' => 'gpoaula_id',
        'grupo' => 'gpoaula_grupo',
        'dia' => 'gpoaula_dia',
        'inicio' => 'gpoaula_inicio',
        'fin' => 'gpoaula_fin',
        'aula' => 'gpoaula_aula',
        'periodo' => 'gpoaula_periodo',
       
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}