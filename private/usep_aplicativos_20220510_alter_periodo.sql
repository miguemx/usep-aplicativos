ALTER TABLE `aca_periodos` ADD `periodo_estado` ENUM('INICIO','INSCRIPCION','CURSO','CALIFICACIONES','FINALIZADO') NOT NULL AFTER `periodo_fin`;
UPDATE `aca_periodos` SET `periodo_estado` = 'FINALIZADO' WHERE `aca_periodos`.`periodo_id` = '2020A';
UPDATE `aca_periodos` SET `periodo_estado` = 'FINALIZADO' WHERE `aca_periodos`.`periodo_id` = '2021A';
UPDATE `aca_periodos` SET `periodo_estado` = 'FINALIZADO' WHERE `aca_periodos`.`periodo_id` = '2021B';
UPDATE `aca_periodos` SET `periodo_estado` = 'CURSO' WHERE `aca_periodos`.`periodo_id` = '2022B';