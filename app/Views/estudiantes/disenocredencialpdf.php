<div style="width: 321px; height: 208px; background: url(<?php echo base_url('img/credencial/Fondo-01-small.png'); ?>); background-repeat: no-repeat; background-size: auto; font-family: Arial, Helvetica, sans-serif !important; font-size: 10px;">
    <table style="width: 321px">
        <tr>
            <td width="110" valign="top" style="padding-top: 5;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="height: 100px; padding-left: 7px;" >
                            <img src="<?php echo $foto ?>" style="vertical-align: middle; max-height: 90px; max-width: 90px;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #fff; margin-top: 5px; padding: 5px 7px; font-size: 7px; font-family: Arial, Helvetica, sans-serif">Vigencia: <?php echo $vigencia; ?></td>
                    </tr>
                    <tr>
                        <td style="color: #fff; padding: 0px 7px; font-size: 7px; font-family: Arial, Helvetica, sans-serif">
                            <img src="<?php echo $qrimage; ?>" alt="QR" style="max-height: 50px;" />
                        </td>
                    </tr>
                </table>
            </td>

            <td valign="top" style="font-family: Arial, Helvetica, sans-serif">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif">
                    <tr>
                        <td style="padding-top: 5px; height: 50px;">
                            <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; font-size: 12px; padding-top: 1px; ">
                            Estudiante
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888;  font-weight: bold; line-height: 1; width: 100px; height: 50px; ">
                            <?php echo $nombre; ?> 
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #8a0933; font-weight: bold; line-height: 1; padding-right: 5px; margin-top: 5px; font-size: 12px; height: 30px; ">
                            <?php echo $carrera; ?> 
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; line-height: 1; text-align: right; font-size: 10px; padding-right: 5px; padding-top: 5px;">
                            Matrícula
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; font-weight: bold; line-height: 1; text-align: right; font-size: 11px; padding-right: 5px">
                            <?php echo $matricula; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</div>



<div style="width: 321px; height: 208px; background: url(<?php echo base_url('img/credencial/Fondo-02-small.png'); ?>); background-repeat: no-repeat; background-size: auto; font-family: Arial, Helvetica, sans-serif !important; font-size: 10px;">
    <table style="width: 321px;font-family: Arial, Helvetica, sans-serif " cellpadding="0" cellspacing="0">
        <tr>
            <td width="25%" style="text-align: center; height: 43px;">
                &nbsp;
            </td>
            <td width="25%" style="text-align: center;">
                <img src="<?php echo base_url('img/credencial/Logo_Puebla-01.png'); ?>" style="max-height: 25px;" />
            </td>
            <td width="25%" style="text-align: center;">
                <img src="<?php echo base_url('img/credencial/Logo_SEP-01.png'); ?>" style="max-height: 25px;" />
            </td>
            <td width="25%" style="text-align: center;">
                <img src="<?php echo base_url('img/credencial/Logo_USEP-01-corto.png'); ?>" style="max-height: 25px;" />
            </td>
        </tr>
        <tr>
            <td rowspan="2" colspan="2" valign="bottom" style="font-size: 9px; color: #fff; padding-left: 9px; padding-bottom: 5px;">
                Este documento digital <br />cuenta con
                validez oficial, <br />ya que se
                genera a partir de <br />los datos
                concentrados en los archivos que
                obran en la institución. la misma, tiene
                un código QR que permite la validación
                electrónica.<br />
                &nbsp;&nbsp;&nbsp;&nbsp;Contacto: escolar@usalud.edu.mx
            </td>
            <td colspan="2" style="text-align: right; height: 70px; text-align: center; color: #8a0933; font-size: 8px;">
                <img src="<?php echo base_url('img/credencial/Area_Firma_Estudiante-01.png'); ?>" style="max-width: 150px; max-height: 40px;" />
                <strong>Firma del estudiante</strong><br />
                <?php echo $nombre; ?>
            </td>
        </tr>
        <tr>
            
            <td colspan="2" style="text-align: right; height: 70px; padding-right: 5px; padding-left: 30px; text-align: center; color: #8a0933; font-size: 8px; ">
                <img src="<?php echo base_url('img/credencial/firmarector.jpg'); ?>" style="max-width: 150px; max-height: 35px;" /><br />
                <strong>Dr. Hugo Eloy Meléndez Aguilar</strong><br />
                RECTOR
            </td>
        </tr>
    </table>
    
</div>

