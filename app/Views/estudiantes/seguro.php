<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div>
        <h2>Seguro Facultativo</h2>
        <hr />
    </div>
    <?php if(isset($errors)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            Por favor verifica los siguientes errores:
            <ul>
            <?php foreach( $errors as $error ): ?>
                <li><?php echo $error; ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if(isset($alumnoSeguro->comentarios)): ?>
        <div class="col-sm-12 alert alert-warning" role="alert">
            
                <?php echo $alumnoSeguro->comentarios; ?>
            
        </div>
    <?php endif; ?>
    
    <form method="post" action="<?php echo base_url('Estudiantes/Seguro/Datos') ?>" enctype="multipart/form-data">
        <a href="#">Instructivo para la afiliación al seguro facultativo.</a>
        <div class="alert alert-primary">
            1. Verifica tus datos, en caso de requerir alguna corrección en tu nombre o CURP, te pedimos 
            escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a> antes de continuar con tu proceso de afiliación al seguro facultativo.
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label for="apPaterno" class="form-label">Apellido paterno</label>
                <input type="text" class="form-control" id="apPaterno" value="<?php echo $apPaterno ?>" disabled="disabled" />
            </div>
            <div class="col-sm-12">
                <label for="apPaterno" class="form-label">Apellido materno</label>
                <input type="text" class="form-control" id="apMaterno" value="<?php echo $apMaterno ?>" disabled="disabled" />
            </div>
            <div class="col-sm-12">
                <label for="apPaterno" class="form-label">Nombre(s)</label>
                <input type="text" class="form-control" id="nombre" value="<?php echo $nombre ?>" disabled="disabled" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label for="apPaterno" class="form-label">CURP</label>
                <input type="text" class="form-control" id="nombre" value="<?php echo $curp ?>" disabled="disabled" />
            </div>
            <div class="col-sm-9">
                <label for="apPaterno" class="form-label">Licenciatura</label>
                <input type="text" class="form-control" id="nombre" value="<?php echo $carrera ?>" disabled="disabled" />
            </div>
        </div>
        
        <div>&nbsp;</div><div>&nbsp;</div>
        <div class="alert alert-primary">
            2. Ingresa la información solicitada
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Selecciona tu clínica IMSS más cercana a tu domicilio</label>
            <select class="form-select" aria-label="Default select example" name="clinica">
                <option value="1">-- Ubica por localidad y número de clínica --</option>
                <?php foreach( $clinicas as $clinica ): ?>
                    <option value="<?php echo $clinica->id ?>" <?php if($alumnoSeguro->umf==$clinica->id) echo 'selected="selected"'  ?> ><?php echo $clinica->localidad.' - '.$clinica->nombre; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div>&nbsp;</div><div>&nbsp;</div>
        <div class="alert alert-primary">
            3. Adjunta la información solicitada
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Sube tu constancia de vigencia de derechos de IMSS</label>
            <input type="file" class="form-control" name="imss" />
        </div>
        <div>&nbsp;</div><div>&nbsp;</div>
        <button class="btn btn-secondary" type="submit" name="gfs" value="1">
            Enviar
        </button>

    </form>

   
</div>

<?php echo $this->endSection() ?>