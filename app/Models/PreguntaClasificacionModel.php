<?php

namespace App\Models;

use CodeIgniter\Model;

class PreguntaClasificacionModel extends Model
{
    protected $table = 'preguntas_clasificacion';
    protected $primaryKey = 'clasificacion_id';
    protected $returnType = 'App\Entities\PreguntaClasificacion';
    protected $useSoftDeletes = false;
    protected $allowedFields = ['clasificacion_id', 'clasificacion_nombre'];
    protected $db;
    protected $builder;

    protected $useTimestamps = false;

}
