<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }

    .formulario-cabecera,
    .formulario-datos {
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }
</style>
<br>

<?php if (isset($configuracionformulario)) : ?>
    <?php if (isset($etapa_detalles)) : ?>

        <div class="container">
            <?php if (isset($bandera_edit)) : ?>
                <form action="updateformulario" method="post">
                    <input type="hidden" name="formulario-id" value="<?php echo $id_formulario; ?>">
                <?php else : ?>
                    <form action="configurarformulario" method="post">
                    <?php endif; ?>
                    <div class="card">
                        <div class="card-header">
                            <h3>Configurador de Formulario</h3>
                        </div>
                        <div class="card-body">
                            <div class="datos text-center">
                                <label>
                                    Periodo:
                                    <h5 class="card-title"><?php echo $etapa_periodo ?></h5>
                                </label>
                                <label>
                                    Nombre:
                                    <h5 class="card-title"><?php echo $etapa_nombre ?></h5>
                                </label>
                                <label>
                                    Numero de Secuiencia:
                                    <h5 class="card-title"><?php echo $etapa_orden ?></h5>
                                </label>
                                <input type="hidden" name="formulario-identificador" value="<?php echo $etapa_detalles ?>">
                            </div>
                            <br>
                            <div>
                                <div class="mb-3">
                                    <div>
                                        <label for="nombre-formulario" class="form-label">Nombre del Formulario:</label>
                                        <?php if (isset($nombre_form)) : ?>
                                            <input type="text" class="form-control" name="nombre-formulario" id="nombre-formulario" aria-describedby="helpId" placeholder="ej.Datos iniciales" required value="<?php echo $nombre_form; ?>">
                                        <?php else : ?>
                                            <input type="text" class="form-control" name="nombre-formulario" id="nombre-formulario" aria-describedby="helpId" placeholder="ej.Datos iniciales" required>
                                        <?php endif; ?>
                                        <small id="helpId" class="form-text text-muted">Ingrese el nombre que se le asignara al formulario</small>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="titulo-formulario" class="form-label">Titulo del Formulario:</label>
                                        <?php if (isset($titulo_form)) : ?>
                                            <input value="<?php echo $titulo_form; ?>" type="text" class="form-control" name="titulo-formulario" id="titulo-formulario" aria-describedby="helpId" required>
                                        <?php else : ?>
                                            <input type="text" class="form-control" name="titulo-formulario" id="titulo-formulario" aria-describedby="helpId" required>
                                        <?php endif; ?>
                                        <small id="helpId" class="form-text text-muted">Ingrese el Titulo del Formulario que se le mostrara al aspirante</small>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="orden-formulario" class="form-label">Numero de Secuencia:</label> <br>
                                        <?php if (isset($numero_form)) : ?>
                                            <input value="<?php echo $numero_form ?>" class="form-control" type="number" name="orden-formulario" id="orden-formulario" required>
                                        <?php else : ?>
                                            <input value="<?php echo $num_form ?>" class="form-control" type="number" name="orden-formulario" id="orden-formulario" required>
                                        <?php endif; ?>
                                        <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                                    </div>
                                    <br>
                                    <div>
                                        <label for="comment">Instrucciones del Formulario:</label>
                                        <?php if (isset($instruccion_form)) : ?>
                                            <textarea class="form-control" rows="5" id="comment" name="help-text" required><?php echo $instruccion_form ?></textarea>
                                        <?php else : ?>
                                            <textarea class="form-control" rows="5" id="comment" name="help-text" required></textarea>
                                        <?php endif; ?>
                                        <small id="helpId" class="form-text text-muted">Ingresa a una breve indicación de lo que deberán realizar en este formulario</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-muted text-center">
                            <?php if (isset($bandera_edit)) : ?>
                                <button type="submit" class="btn btn-secondary">Editar Formulario</button>
                            <?php else : ?>
                                <button type="submit" class="btn btn-secondary">Crear Formulario</button>
                            <?php endif; ?>
                            <button class="btn btn-secondary" type="button" id="cancelar">Cancelar</button>
                        </div>
                    </div>
                    </form>
        </div>
        <form action="DiseñadordeModelo" method="post" id="home-fomr">
            <input type="hidden" name="home-back" value="true">
        </form>
        <script>
            'use strict';
            const btn_guardar = document.querySelector('#cancelar');
            let regresarhome = () => {
                document.getElementById('home-fomr').submit()
            }
            btn_guardar.addEventListener('click', regresarhome);
        </script>
    <?php else : ?>
        <h3>
            No se encontro la etapa
        </h3>
    <?php endif; ?>
<?php endif; ?>




<?php echo $this->endSection() ?>