<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Horario;

class HorariosModel extends Model {
    protected $table      = 'aplicativo_horarios';
    protected $primaryKey = 'horario_id';

    protected $returnType    = 'App\Entities\Horario';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'horario_id','horario_nombre','horario_prorroga','horario_limite','horario_salida_anticipada',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * devuelve todos los horarios registrados, ordenados por nombre
     */
    public function todos() {
        $this->orderBy('horario_nombre','ASC');
        return $this->findAll();
    }
}