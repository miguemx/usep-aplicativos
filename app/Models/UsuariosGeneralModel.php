<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class UsuariosGeneralModel extends Model {
    protected $table      = 'view_usuarios_generales';
    protected $primaryKey = 'id';

    protected $returnType    = 'App\Entities\UsuarioGeneral';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $this->creaFiltros( $filtros );
        $usuarios = $this->paginate(25);
        
        return $usuarios;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['id']) ) {
                $this->where( 'id', $filtros['id'] );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'apPaterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'apMaterno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'nombre', trim($filtros['nombre']), 'both' ); 
            }
        }
        
        $this->orderBy('apPaterno','ASC');
        $this->orderBy('apMaterno','ASC');
        $this->orderBy('nombre','ASC');

    }

}