CREATE VIEW view_pagos_alumnos AS
SELECT alumno_id, alumno_ap_paterno, alumno_ap_materno, alumno_nombres, carrera_nombre, materia_nombre, alumnos_pagos.* 
FROM alumnos_pagos
INNER JOIN alumnos_generales on pago_alumno=alumno_id
INNER JOIN prein_carreras on alumno_carrera=carrera_id
LEFT JOIN aca_materias on pago_materia=materia_clave
ORDER BY alumno_ap_paterno, alumno_ap_materno, alumno_nombres