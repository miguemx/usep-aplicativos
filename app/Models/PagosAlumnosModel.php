<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\PagoAlumno;

class PagosAlumnosModel extends Model {
    protected $table      = 'alumnos_pagos';
    protected $primaryKey = 'pago_id';

    protected $returnType    = 'App\Entities\PagoAlumno';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'pago_alumno','pago_concepto','pago_referencia','pago_status','pago_monto','pago_concepto_texto','pago_fecha','pago_comprobante',
        'pago_tipo','pago_observaciones', 'pago_materia',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros );
        $pagos = $this->paginate(50);
        return $pagos;
    }

    /**
     * busca los pagos de un alumno determinado
     * @param matricula la matricula del alumno
     * @return array con los objetos de los pagos de alumnos, o vacio en caso de no existir
     */
    public function findByAlumno($matricula) {
        $pagos = $this->where('pago_alumno',$matricula)->findAll();
        return $pagos;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('matricula', $filtros) ) { // -- filtrar alumno
            if ( strlen($filtros['matricula']) ) {
                $this->where( 'pago_alumno', $filtros['matricula'] );
            }
        }
        if ( array_key_exists('referencia', $filtros) ) { // -- filtrar por referencia
            if ( strlen($filtros['referencia']) ) {
                $this->like( 'pago_referencia', trim($filtros['referencia']), 'both' ); 
            }
        }
        if ( array_key_exists('status', $filtros) ) { // -- filtrar por status
            if ( strlen($filtros['status']) ) {
                $this->where( 'pago_status', $filtros['status'] );
            }
        }
        if ( array_key_exists('tipo', $filtros) ) { // -- filtrar por tipo
            if ( strlen($filtros['tipo']) ) {
                $this->where( 'pago_tipo', $filtros['tipo'] );
            }
        }
        if ( array_key_exists('fecha', $filtros) ) { // -- filtrar por tipo
            if ( preg_match( "/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/",$filtros['fecha'] ) ) {
                $this->where( 'pago_fecha', $filtros['fecha'] );
            }
        }

        $this->orderBy('pago_fecha','DESC');
        $this->orderBy('pago_status','ASC');
        $this->orderBy('pago_referencia','ASC');
    }

    /**
     * inserta un pago para un alumno en especifico
     * @param matricula el ID del alumno a guardar
     * @param pago el objeto PagoAlumno a insertar
     * @return true en caso de que el pago se inserta de forma adecuada
     * @param error una cadena con el error
     */
    public function guardapago( $matricula, $pago ) {
        
        
    }

    /**
     * busca un pago de un alumno por la referencia
     * @param referencia el numero de referencia a buscar
     */
    public function findByReferencia( $referencia ) {
        $this->where('pago_referencia',$referencia);
        return $this->findAll();
    }

    /**
     * busca un registro por cotejar de acuerdo con la referencia
     * @param referencia el numero de referencia para buscar
     * @return array un arreglo con los datos de los pagos encontrados
     */
    public function refPorCotejar( $referencia ) {
        $this->where( 'pago_referencia', $referencia );
        $this->where( 'pago_status', 'EN PROCESO' );
        return $this->first();
    }

    /**
     * busca un pago de un alumno por la referencia
     * @param referencia el numero de referencia a buscar
     * @param true si la referencia existe sin estado rechazado
     * @param false si la referencia no existe o existe una con estado rechazado
     */
    public function referenciaExistente( $referencia ) {
        $this->where('pago_referencia',$referencia);
        $refs = $this->findAll(); 
        $buscar = false;
        foreach ( $refs as $ref ) {
            if ( $ref->status != 'RECHAZADO' ) {
                $buscar = true;
                break;
            }
        }
        return $buscar;
    }

    /**
     * devuelve un pago mediante su referencia. el pago debe se debe encontrar en espera
     * @param referencia el numero de referencia
     * @return PagoAlumno la entidad del pago encontrado
     * @return null en caso de no existir, o bien que sea mas de uno
     */
    public function getEnEsperaByReferencia( $referencia ) {
        $this->where('pago_referencia',$referencia);
        $this->where('pago_status','EN PROCESO');
        $result = $this->findAll(); 
        if ( count($result) == 1 ) {
            return $result[0];
        }
        return null;
    }

    /**
     * @deprecated se debera utilizar la funcion del RosModel con el mismo nombre
     * verifica si algun alumno ha registrado la referencia pasada para validarla
     * @param referencia el numero de referencia a verificar
     */
    public function coteja($referencia) {
        // buscar primero si la referencia está registrada por algùn alumno
        $this->where( 'pago_referencia', $referencia );
        $this->where( 'pago_status', 'EN PROCESO' );
        $alumnoPago = $this->first();
        if ( !is_null($alumnoPago) ) { // si se encuentra entonces se procede a buscar en el ROS
            $rosModel = new RosModel();
            $refs = $rosModel->findByReferencia( $referencia, 'OTORGADO' );
            $importe = 0;
            foreach ( $refs as $ref ) {
                $importe += $ref->importe;
            }
            if ( $alumnoPago->monto <= $importe ) {
                try {
                    foreach ( $refs as $ref ) {
                        $ref->cotejado = '1';
                        $ref->matricula = $alumnoPago->matricula;
                        $rosModel->update( $ref->id, $ref );
                    }
                    $alumnoPago->status = 'ACEPTADO';
                    $this->update( $alumnoPago->id, $alumnoPago );
                }
                catch (\Exception $ex) {
                    log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                }
            }
        }
    }
}