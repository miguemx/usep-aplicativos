<style type="text/css">
    *, div, p {
        font-size: 1.2rem;
        font-family: Arial, Helvetica, sans-serif;
    }
    p {
        text-align: justify;
        margin: 25px 35px;
    }
</style>


<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta de conocimiento y aceptación del reglamento de ingreso, permanencia y egreso de los aprendientes de la “Universidad de la Salud” del Estado de Puebla. 
</p>
<p>
    Por mi propio derecho, el (la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente 
    inscrito(a) en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id; ?></u></strong>, y en 
    cumplimiento al Artículo 11 y 14 del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD DE LA 
    SALUD DEL ESTADO DE PUEBLA, bajo protesta de decir verdad, declaro haber leído, tener conocimiento, entender y aceptar las normas 
    del Reglamento de Ingreso, Permanencia y Egreso de los Aprendientes de la Universidad de la Salud del Estado de Puebla; y al mismo 
    tiempo me comprometo a cumplir con cada una de las cláusulas establecidas en el mismo y acoplando de conformidad, así como con los 
    procedimientos, instrucciones y acuerdos que emitan las autoridades competentes de la “Universidad de la Salud”.
</p>
<p >
    Así mismo, acepto las responsabilidades, derechos y obligaciones establecidos para las (los) estudiantes/aprendientes en la 
    normatividad interna de cada una de las Licenciaturas.
</p>
<!--<div style="padding: 0 25%; background-repeat: no-repeat; background-image: '<?php //echo base_url('img/inscripcion/marcadseyt.jpg')?>'; background-position: 94% 0%;">-->
<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('<?php echo base_url('img/inscripcion/marcadseyt.jpg')?>'); background-position: 94% 0%;">
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px;">
    <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>


<div>
    <img src="<?php echo base_url('/img/inscripcion/footerdocs.jpg') ?>" />
</div>

<hr style="border 1px dotted #444;" />

<div>
    <img src="<?php echo base_url('/img/inscripcion/headerdocs.jpg') ?>" />
</div>
<p style="text-align: center; font-weight: bold; text-align: center; font-size: 0.7rem;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta de conocimiento y aceptación del reglamento de ingreso, permanencia y egreso de los aprendientes de la “Universidad de la Salud” del Estado de Puebla.
</p>
<p style="text-align: justify; margin: 5px 20px; font-size: 0.7rem;">
Por mi propio derecho, el (la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente 
    inscrito(a) en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id; ?></u></strong>, y en 
    cumplimiento al Artículo 11 y 14 del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD DE LA 
    SALUD DEL ESTADO DE PUEBLA, bajo protesta de decir verdad, declaro haber leído, tener conocimiento, entender y aceptar las normas 
    del Reglamento de Ingreso, Permanencia y Egreso de los Aprendientes de la Universidad de la Salud del Estado de Puebla; y al mismo 
    tiempo me comprometo a cumplir con cada una de las cláusulas establecidas en el mismo y acoplando de conformidad, así como con los 
    procedimientos, instrucciones y acuerdos que emitan las autoridades competentes de la “Universidad de la Salud”.
</p>
<p  style="text-align: justify; margin: 5px 20px;  font-size: 0.7rem;">
Así mismo, acepto las responsabilidades, derechos y obligaciones establecidos para las (los) estudiantes/aprendientes en la 
    normatividad interna de cada una de las Licenciaturas.
</p>
<div style="padding: 0 25%";>
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px; font-size: 0.7rem;">
    <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>
