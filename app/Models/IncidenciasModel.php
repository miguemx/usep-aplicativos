<?php

namespace App\Models;
use CodeIgniter\Model;

class IncidenciasModel extends Model {
    
    protected $table      = 'bio_registros';
    protected $primaryKey = 'registro_id';

    protected $returnType    = 'App\Entities\Persona';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'registro_id','registro_persona','registro_num','registro_fecha','registro_tipo',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function registros($inicio, $fin) {
        $res = $this->db->query( "SELECT * FROM bio_personas" );
        $personas = $res->getResult();
        foreach ( $personas as $persona ) {
            $this->where( 'registro_fecha >= ', $inicio.' 00:00:00' );
            $this->where( 'registro_fecha <= ', $fin.' 23:59:59' );
            $this->where( 'registro_persona', $persona->persona_id );
            $this->orderBy( 'registro_fecha ASC' );

            $registrosDia = $this->findAll();
            $registrosNum = count( $registrosDia );

            if ( $registrosNum < 1 ) {
                echo "PERSONA FALTÓ: ".$persona->persona_id.' '.$persona->persona_nombre.' '.$persona->persona_apellido;
                echo '<hr />';
            }
            else if ( $registrosNum < 2 ) {
                echo "PERSONA NO REGISTRÓ ENTRADA O SALIDA: ".$registrosDia[0]->registro_fecha.' - '.$persona->persona_id.' '.$persona->persona_nombre.' '.$persona->persona_apellido;
                echo '<hr />';
            }
            else {
                
            }

            

        }
        return null;
    }

    /**
     * genera un listado con todas las incidencias encontradas en el rango de fechas indicado
     * @param empleados un arreglo con todos los App\Entities\Empleados encontrados en la BD
     * @param fechaInicio la fecha en la que se inicia la busqueda
     * @param fechaFin la fecha en la que se finaliza la búsqueda
     * @return null devuelve un valor null si la fecha de inicio es mayor que la fin
     * @return listado devuelve el arreglo con las incidencias encontradas; si no existen, el arreglo irá vacío
     */
    public function listar($empleados, $fechaInicio, $fechaFin) { 
        $timeInicio=strtotime($fechaInicio);
        $timeFin=strtotime($fechaFin);
        if ( $timeInicio > $timeFin ) {
            return null;    
        }
        $listado = array();
        for($i=$timeInicio; $i<=$timeFin; $i+=DAY){
            $fecha = date( "Y-m-d", $i );
            $diaSemana = date("w", $i);
            foreach ( $empleados as $empleado ) {
                $horario = $this->obtenerHorario( $empleado ); 
                $incidencia = $this->buscaIncidencia($fecha, $horario);
                if ( !is_null($incidencia) ) {
                    $listado[$fecha][$empleado->id]['nombre'] = $empleado->nombre;
                    $listado[$fecha][$empleado->id]['apellido'] = $empleado->apellido;
                    $listado[$fecha][$empleado->id]['incidencias'] = $incidencia;
                }
            }
        }
        return $listado;
    }

    /**
     * busca una las incidencias en una fecha determinada, de acuerdo al horario especificado de la persona
     * @param fecha cadena con la fecha a buscar incidencia, en formato YYYY-mm-dd
     * @param horario arreglo con las llaves como dias de la semana (0 para domingo) y los horarios de entrada y salida
     * @return null si no encuentra incidencias de acuerdo al horario
     * @return 
     */
    private function buscaIncidencia($fecha, $horario) {
        $sql = " SELECT * FROM view_empleados_registros WHERE empleado_id=? AND registro_fecha>=? AND registro_fecha <=? ORDER BY registro_fecha ASC";
        $query = $this->db->query( $sql, [ $horario['id'], $fecha.' 00:00:00', $fecha.' 23:59:59' ] );
        $rows = $query->getResult(); // buscar los registros de la persona
        $time = strtotime($fecha); // convertir la fecha para comparar
        $dia = date("w", $time); // obtener el dia de la semana para buscar el horario correspondiente
        if( !array_key_exists( $dia, $horario['dias'] ) ) return null; // si el dia no esta en su horario, no se buscan incidencias
        $timeValido = strtotime($fecha.' '.$horario['dias'][$dia]['entrada']) + $horario['prorroga'] * MINUTE; // obtener cuando no es retardo
        $timeLimite = strtotime($fecha.' '.$horario['dias'][$dia]['entrada']) + $horario['limite'] * MINUTE; // obtener cuando es un retardo
        $timeSalidaValido = strtotime($fecha.' '.$horario['dias'][$dia]['salida']) - $horario['prorroga'] * MINUTE; // obtener la salida mas temprana
        $primerRegistro = $ultiRegistro = null;
        $incidenciaEntrada = 2; // 0 para checada en tiempo, 1 para checada tarde, 2 para checada de falta
        $incidenciaSalida = 1; // 0 para salir en tiempo, 1 para salir mas anticipadamente
        foreach ( $rows as $row ) {
            $timeChecada = strtotime($row->registro_fecha);
            if ( !$primerRegistro ) $primerRegistro = $row->registro_fecha;
            $ultiRegistro = $row->registro_fecha;
            // verificar las entradas
            if ( $timeChecada <= $timeValido ) {
                $incidenciaEntrada = 0;
            }
            else if ($timeChecada <= $timeLimite) {
                $incidenciaEntrada = 1;
            }
            // verificar las salidas
            if ( $timeChecada >=  $timeSalidaValido ) {
                $incidenciaSalida = 0; break;
            }
        }

        if ( $incidenciaEntrada === 0 && $incidenciaSalida === 0 ) {  // si no se encuentra ninguna incidencia de entrada y salida
            return null;
        }
        $incidencia = [ 'entrada'=>$primerRegistro, 'salida'=>$ultiRegistro, 'incidencias'=>[] ];
        if ( count($rows) < 1) {
            $incidencia = [ 'entrada'=>$primerRegistro, 'salida'=>$ultiRegistro, 'incidencias'=>['SIN REGISTROS'] ];
        }
        else {
            if ( $incidenciaEntrada == 1 ) $incidencia['incidencias'][] = "RETARDO";
            else if ( $incidenciaEntrada == 2 ) $incidencia['incidencias'][] = "LLEGADA TARDE";
            if ( $incidenciaSalida == 1 ) $incidencia['incidencias'][] = "SALIDA ANTICIPADA";
        }
        return( $incidencia );
    }


    /**
     * obtiene todo el horario registrado para un empleado en especifico
     * @param empleado el objeto del empleado
     * @return horario un arreglo vacio si no tiene horario, o con los datos de su horario
     */
    public function obtenerHorario($empleado) {
        $horario = [];

        $query = $this->db->query( "SELECT * FROM view_horario_empleados WHERE empleado_id=? ORDER BY diashorario_dia ASC", [$empleado->id] );
        $result = $query->getResult();

        if ( count($result) ) {
            $horario['id'] = $empleado->id;
            
            $dias = [];
            foreach ( $result as $rowhorario ) {
                $horario['prorroga'] = $rowhorario->horario_prorroga;
                $horario['limite'] = $rowhorario->horario_limite;
                $horario['salida'] = $rowhorario->horario_salida_anticipada;
                $dias[ $rowhorario->diashorario_dia ] = [ 'entrada'=>$rowhorario->diashorario_entrada, 'salida' => $rowhorario->diashorario_salida ];
            }
            $horario['dias'] = $dias;
        }

        return $horario;
    }


}
