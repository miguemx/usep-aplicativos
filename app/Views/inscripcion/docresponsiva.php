<style type="text/css">
    *, div, p {
        font-size: 1.08rem;
        font-family: Arial, Helvetica, sans-serif;
    }
    p {
        text-align: justify;
        margin: 20px 35px;
    }
    ol {
        margin: 0px;
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta responsiva
</p>
<p>
    El (la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?></u></strong> en mi carácter de estudiante/aprendiente 
    inscrito(a) en la <strong><u><?php echo $carrera->nombre ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id ?></u></strong>, 
    identificándome con los documentos establecidos en el comunicado de inscripción:
</p>
<p style="padding-left:15px;">
    A) Identificación oficial, <br />
    B) Acta de Nacimiento, <br />
    C) CURP y <br />
    D) Certificado de Estudios del Nivel Medio Superior,
</p>
<p>
    manifiesto bajo protesta de decir verdad que: AVALO Y ME RESPONSABILIZO DE LA VALIDEZ OFICIAL de los documentos que presento, 
    mismos que obtuve mediante los procedimientos legales respectivos; por lo que acepto que sean validados y formen parte del 
    archivo de esta Universidad de la Salud, en cumplimiento al Artículo 11 inciso d) del REGLAMENTO DE INGRESO, PERMANENCIA Y 
    EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA.
</p>
<p>
    Así mismo, declaro que conozco las consecuencias penales a las que se hacen acreedoras las personas que realizan prácticas 
    indebidas de documentos apócrifos y que de acuerdo con el Artículo 41 inciso a, del Reglamento anteriormente citado, ACEPTO que la 
    “UNIVERSIDAD DE LA SALUD” DEL ESTADO DE PUEBLA, en su caso, ejecute la baja institucional definitiva y ejerza las acciones legales pertinentes.
</p>
<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('<?php echo base_url('img/inscripcion/marcadseyt.jpg')?>'); background-position: 94% 0%;">
<!--<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('<?php //echo base_url('img/inscripcion/marcadseyt.jpg');?>; background-position: 94% 0%;";>-->
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px;">
    <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de agosto del 2022.
    </div>
</div>


<div>
    <img src="<?php echo base_url('/img/inscripcion/footerdocs.jpg') ?>" />
</div>

<hr style="border 1px dotted #444;" />

<div>
    <img src="<?php echo base_url('/img/inscripcion/headerdocs.jpg') ?>" />
</div>
<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px; font-size: 0.7rem;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta responsiva
</p>
<p style="font-size: 0.65rem; margin: 5px 35px;">
    El (la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?></strong></u> en mi carácter de estudiante/aprendiente 
    inscrito(a) en la <strong><u><?php echo $carrera->nombre ?></strong></u>, con número de matrícula: <strong><u><?php echo $alumno->id ?></strong></u>, 
    identificándome con los documentos establecidos en el comunicado de inscripción:
</p>
<p style="font-size: 0.65rem; margin: 5px 35px;padding-left:15px;" >
    A) Identificación oficial, <br />
    B) Acta de Nacimiento, <br />
    C) CURP y <br />
    D) Certificado de Estudios del Nivel Medio Superior,
</p>
<p style="font-size: 0.65rem; margin: 5px 35px;">
    manifiesto bajo protesta de decir verdad que: AVALO Y ME RESPONSABILIZO DE LA VALIDEZ OFICIAL de los documentos que presento, 
    mismos que obtuve mediante los procedimientos legales respectivos; por lo que acepto que sean validados y formen parte del 
    archivo de esta Universidad de la Salud, en cumplimiento al Artículo 11 inciso d) del REGLAMENTO DE INGRESO, PERMANENCIA Y 
    EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA.
</p>
<p style="font-size: 0.65rem; margin: 5px 35px;">
    Así mismo, declaro que conozco las consecuencias penales a las que se hacen acreedoras las personas que realizan prácticas 
    indebidas de documentos apócrifos y que de acuerdo con el Artículo 41 inciso a, del Reglamento anteriormente citado, ACEPTO que la 
    “UNIVERSIDAD DE LA SALUD” DEL ESTADO DE PUEBLA, en su caso, ejecute la baja institucional definitiva y ejerza las acciones legales pertinentes.
</p>
<div style="padding: 0 25%";>
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 30px; font-size: 0.65rem;">
    <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de agosto del 2022.
    </div>
</div>
