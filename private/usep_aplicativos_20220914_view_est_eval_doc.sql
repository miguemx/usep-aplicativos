CREATE view view_est_eval_doc as (
SELECT
	a.respuestas_id,
    a.respuestas_matricula,
    a.respuestas_grupo,
    a.respuestas_pregunta,
    a.respuestas_valor,
    a.respuestas_comentario,
    b.preguntas_formulario,
    b.preguntas_orden,
    b.preguntas_titulo,
    b.preguntas_tipo,
    b.preguntas_clasificacion,
    c.formulario_nombre,
    c.formulario_orden,
    c.formulario_instrucciones,
    z.evaluacion_id,
    z.evaluacion_titulo,
    d.tipo_nombre,
    d.tipo_valores,
    d.tipo_clasificacion,
    e.alumno_ap_paterno,
    e.alumno_ap_materno,
    e.alumno_nombres,
    e.alumno_status,
    e.alumno_semestre,
    f.gpoalumno_grupo,
    g.grupo_clave,
    g.grupo_materia,
    g.grupo_periodo,
    g.grupo_docente,
    h.materia_carrera,
    h.materia_semestre,
    h.materia_nombre,
    h.materia_clave,
    i.empleado_id,
    i.empleado_correo,
    i.empleado_ap_paterno,
    i.empleado_ap_materno,
    i.empleado_nombre
FROM 
	`evaluacion_respuestas` as a
join `evaluacion_preguntas` as b on b.preguntas_id=a.respuestas_pregunta
join `evaluacion_formulario` as c on c.formulario_id = b.preguntas_formulario
join `aplicativo_evaluacion` as z on z.evaluacion_id=c.formulario_evaluacion
join `evaluacion_tipo_pregunta` d on d.tipo_id = c.formulario_evaluacion
join `alumnos_generales` as e on e.alumno_id=a.respuestas_matricula
join `aca_grupos_alumnos` as f on f.gpoalumno_alumno=a.respuestas_matricula and f.gpoalumno_grupo=a.respuestas_grupo
join `aca_grupos` as g on g.grupo_id=f.gpoalumno_grupo
join `aca_materias` as h on h.materia_clave = g.grupo_materia
left join `aplicativo_empleados` as i on i.empleado_id=g.grupo_docente);