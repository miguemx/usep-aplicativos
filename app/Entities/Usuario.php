<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Usuario extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'correo' => null,
        'rol' => null,
        'nombre' => null,
        'ultcnx' =>null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'correo' => 'usuario_correo',
        'rol' => 'usuario_rol',
        'nombre' => 'usuario_nombre',
        'ultcnx' => 'usuario_ultcnx',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
