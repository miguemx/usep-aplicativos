drop view if exists view_aspirantes_est_status_det;


create view view_aspirantes_est_status_det as(
select
	if (aspirante_carrera ="1" and aspirante_sexo="M",count(aspirante_id),0) as mujeres_enf,
    if (aspirante_carrera ="1" and aspirante_sexo="H",count(aspirante_id),0) as hombres_enf,
    if (aspirante_carrera ="2" and aspirante_sexo="M",count(aspirante_id),0) as mujeres_med,
    if (aspirante_carrera ="2" and aspirante_sexo="H",count(aspirante_id),0) as hombres_med,
    aspirante_estatus,
    aspirante_carrera,
    aspirante_sexo
FROM
	aspirantes_generales
WHERE
	aspirante_validado="1"
    and deleted_at IS NULL
group by
	aspirante_estatus,
    aspirante_carrera,
    aspirante_sexo
order by aspirante_estatus);