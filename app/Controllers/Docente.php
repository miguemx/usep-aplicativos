<?php

namespace App\Controllers;

use App\Entities\HorarioDocente;
use App\Models\GrupoModel;
use App\Models\HorarioDocentesModel;
use App\Models\BitacoraAulasModel;
use App\Entities\BitacoraAulas;
use App\Models\BitacoraAulasViewModel;
use App\Entities\BitacoraViewAulas;
use App\Models\AulasModel;
use App\Models\AsistenciasDiariasModel;
use App\Entities\AsistenciasDiarias;
use App\Models\PeriodoModel;
use App\Models\CapturaCalificacionesModel;
use App\Models\AsistenciasAlumnosModel;
use App\Models\EmpleadosModel;


class Docente extends BaseController
{
    private $periodoModel;
    private $HorarioDocentesModel;
    private $CapturaCalificacionesModel;
    private $AsistenciasDiarias;
    private $AsistenciasAlumnos;
    private $empleados;
    public function __construct()
    {
        $this->periodoModel = new PeriodoModel();
        $this->HorarioDocentesModel = new HorarioDocentesModel();
        $this->CapturaCalificacionesModel = new CapturaCalificacionesModel();
        $this->AsistenciasDiarias = new AsistenciasDiariasModel();
        $this->AsistenciasAlumnos = new AsistenciasAlumnosModel();
        $this->empleados = new EmpleadosModel();
    }
    public function Horario_old()
    {
        $gruposmodel = new HorarioDocentesModel();
        $horario =  $gruposmodel->buscarGrupos($this->sesion->get("id"), '2022B');
        //echo  $this->sesion->get("id");
        //var_dump($gruposmodel->buscarGrupos(100050,'2022B'));
        $lunes = [];
        $martes = [];
        $miercoles = [];
        $jueves = [];
        $viernes = [];
        $sabado = [];
        $materia_anterior = "";
        $bandera = false;
        $arr_datos = [];
        foreach ($horario as $materia) {
            $id = $materia->id;
            //echo $materia->grupo_id." [".$materia->grupo_materia."][ ".$materia->gpoaula_dia."]<br>";
            if ($materia_anterior != $id) {
                // echo $materia->id."<br>";

                $bandera = false;
                if ($bandera === false) {
                    foreach ($horario as $comparacion) {
                        if ($comparacion->id === $id) {
                            $dia = $comparacion->dia;
                            switch ($dia) {
                                case 'LUNES':
                                    $lunes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'MARTES':
                                    $martes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'MIÉRCOLES':
                                    $miercoles[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'JUEVES':
                                    $jueves[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'VIERNES':
                                    $viernes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'SÁBADO':
                                    $sabado[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;

                                    $bandera = true;
                            }
                        }
                    }
                }
                $captura = false;
            } else {
                $captura = true;
                //echo "__[$materia->id]<br>";
            }
            if ($captura === false) {
                $arr_datos[] = [
                    "id" =>  $id,
                    "seccion" => $materia->seccion,
                    "materia" => $materia->materia_nombre,
                    "lunes" => $lunes,
                    "martes" => $martes,
                    "miercoles" => $miercoles,
                    "jueves" => $jueves,
                    "viernes" => $viernes,
                    "sabado" => $sabado,

                ];
                $lunes = [];
                $martes = [];
                $miercoles = [];
                $jueves = [];
                $viernes = [];
                $sabado = [];
            }
            $materia_anterior = $id;
            $bandera = true;
        }
        $data['arreglo'] = $arr_datos;
        $data['menu'] = $this->sesion->get('menu');
        //echo json_encode($arr_datos);
        return view('docentes/horario', $data);
    }

    public function dia()
    {
        $dia = date("l");
        if ($dia == "Monday") {
            return ("LUNES");
        }
        if ($dia == "Tuesday") {
            return ("MARTES");
        }
        if ($dia == "Wednesday") {
            return ("MIÉRCOLES");
        }
        if ($dia == "Thursday") {
            return ("JUEVES");
        }
        if ($dia == "Friday") {
            return ("VIERNES");
        }
        if ($dia == "Saturday") {
            return ("SÁBADO");
        }
        if ($dia == "Sunday") {
            return ("DOMINGO");
        }
    }

    public function bitacoraAulas()
    {
        $aulas = new AulasModel();
        $bitacoraAulasViewModel = new BitacoraAulasViewModel();
        $aulasall = $aulas->notlike('aula_nombre', 'EN LINEA')->findAll();
        $id_sesion = $this->sesion->get("id");
        $dia = $this->dia();
        $hora = date("H") . ":00";
        $bitacoraDatos = $bitacoraAulasViewModel->buscaAula($id_sesion, $hora, $dia, "2022B");
        if (count($bitacoraDatos) == 0) {
            $data['sinhorario'] = 1;
        } else {
            foreach ($bitacoraDatos as  $datos) {
                $aula_id = $datos['gpoaula_aula'];
                $grupo = $datos['gpoaula_grupo'];
                $dia = $datos['gpoaula_dia'];
                $hInicio = $datos['gpoaula_inicio'];
                $hFin = $datos['gpoaula_fin'];
                $aula = $datos['aula_nombre'];
                $docente = $datos['grupo_docente'];
                $materia = $datos['grupo_materia'];
                $periodo = $datos['grupo_periodo'];
            }
            $data['menu'] = $this->sesion->get('menu');
            $data['sesion'] = $id_sesion;
            $data['dia'] = $dia;
            $data['hora'] = $hora;
            $data['aula_id'] = $aula_id;
            $data['grupo'] = $grupo;
            $data['dia'] = $dia;
            $data['hInicio'] = $hInicio;
            $data['$hFin'] = $hFin;
            $data['aula'] = $aula;
            $data['docente'] = $docente;
            $data['materia'] = $materia;
            $data['periodo'] = $periodo;
            $data['conhorario'] = 1;
            $data['selectaula'] = $aulasall;
            $data['idaula'] = $aula_id;
        }
        return view('docentes/bitacora_aulas', $data);
    }

    public function bitacoraInserta()
    {
        $insertbitacora = new BitacoraAulasModel();
        $insertregbitacora = new BitacoraAulas();
        if ($this->request->getPost('idaula') != null) {
            $idaula = $this->request->getPost('idaula');
            echo "id_aula" . $idaula;
        } else {
            $idaula = $this->request->getPost('aulaname');
            echo "aula_name" . $idaula;
        }
        if ($this->request->getPost('aulavalida') == NULL) {
            $insertregbitacora->aula_bandera = "0";
        } else {
            $insertregbitacora->aula_bandera = $this->request->getPost('aulavalida');
        }
        $insertregbitacora->docente = $this->sesion->get("id");
        $insertregbitacora->aula_id = $idaula;
        $insertregbitacora->pantalla = $this->request->getPost('pantalla');
        $insertregbitacora->cpu = $this->request->getPost('cpu');
        $insertregbitacora->teclado = $this->request->getPost('teclado');
        $insertregbitacora->mouse = $this->request->getPost('mouse');
        $insertregbitacora->microfono = $this->request->getPost('microfono');
        $insertregbitacora->camara = $this->request->getPost('camara');
        $insertregbitacora->control = $this->request->getPost('control');
        $insertregbitacora->hubtab = $this->request->getPost('hubdetab');
        $insertregbitacora->hubdisplay = $this->request->getPost('hubdedisplay');
        $insertregbitacora->bocinas = $this->request->getPost('bocinas');
        $insertregbitacora->cables = $this->request->getPost('cables');
        $insertregbitacora->f_actual = date("Y-m-d H:i:s");

        //$insertbitacora->insert($insertregbitacora);/*
        try {
            $insertbitacora->insert($insertregbitacora);
            $data['acept'] = 'Guardado exitoso ';
        } catch (\Exception $ex) {
            $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
        }
        return view('docentes/bitacora_aulas', $data);
    }

    public function BuscarHorarios()
    {
        $aulasmodel = new AulasModel();
        /* foreach($aulasmodel->getAulas() as $valores){
            echo "$valores->id_aula<br>";
        } */
        $data['aulas_totales'] = $aulasmodel->getAulas();
        $data['menu'] = $this->sesion->get('menu');
        return view('horariosgenerales/selecciondehorario', $data);
    }

    public function HorariosGenerales()
    {
        $seccion = $this->request->getPost('seccion');
        $aula = trim($this->request->getPost('aula-select'));
        $horariosmodel = new HorarioDocentesModel();

        if ($seccion != null) {

            return $this->generadorHorario($horariosmodel->buscarSecciones($seccion));
            die();
        } else    if ($aula != null) {
            // echo $aula;
            return $this->generadorHorario($horariosmodel->buscarAula($aula));
            die();
            // $this->horarioSalon($aula);
        }
    }
    public function generadorHorario($horario)
    {
        $lunes = [];
        $martes = [];
        $miercoles = [];
        $jueves = [];
        $viernes = [];
        $sabado = [];
        $materia_anterior = "";
        $bandera = false;
        $arr_datos = [];
        foreach ($horario as $materia) {
            $id = $materia->id;
            if ($materia_anterior != $id) {
                $bandera = false;
                if ($bandera === false) {
                    foreach ($horario as $comparacion) {
                        if ($comparacion->id === $id) {
                            $dia = $comparacion->dia;
                            switch ($dia) {
                                case 'LUNES':
                                    $lunes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'MARTES':
                                    $martes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'MIÉRCOLES':
                                    $miercoles[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'JUEVES':
                                    $jueves[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'VIERNES':
                                    $viernes[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;
                                case 'SÁBADO':
                                    $sabado[] = [
                                        'inicio' => $comparacion->inicio,
                                        'fin' => $comparacion->fin,
                                        'aula' => $comparacion->aula_nombre,
                                    ];
                                    break;

                                    $bandera = true;
                            }
                        }
                    }
                }
                $captura = false;
            } else {
                $captura = true;
                //echo "__[$materia->id]<br>";
            }
            if ($captura === false) {
                $arr_datos[] = [
                    "id" =>  $id,
                    "seccion" => $materia->seccion,
                    "materia" => $materia->materia_nombre,
                    "lunes" => $lunes,
                    "martes" => $martes,
                    "miercoles" => $miercoles,
                    "jueves" => $jueves,
                    "viernes" => $viernes,
                    "sabado" => $sabado,

                ];
                $lunes = [];
                $martes = [];
                $miercoles = [];
                $jueves = [];
                $viernes = [];
                $sabado = [];
            }
            $materia_anterior = $id;
            $bandera = true;
        }
        $data['arreglo'] = $arr_datos;
        $data['menu'] = $this->sesion->get('menu');
        return view('docentes/horarioseccion', $data);
    }
    /**
     * retorna la vista del horario
     */
    public function Horario()
    {
        $periodo = $this->periodoModel->getPeriodoHorarioDocente();
        if ($periodo) {
            //$grupos =  $this->horariosModel->buscarGrupos($this->sesion->get('id'), $periodo);
            $grupos =  $this->HorarioDocentesModel->buscarGrupos($this->sesion->get('id'), $periodo);
            // var_dump($this->sesion->get('id'));
            // echo "<br>";
            // var_dump($periodo);
            // echo "<br>";
            // var_dump($grupos);
            $data['grupos'] =  $grupos;
            $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
            $data['menu'] = $this->sesion->get('menu');
            return view('docentes/horario_actual', $data);
        } else {
            $data['menu'] = $this->sesion->get('menu');
            return view('docentes/nodisponible', $data);
            // return redirect()->to('/Home');
        }
    }
    /**
     * Función para mostrar la vista de asistencia diaria para docentes
     */
    public function asistencia(){
        $data['grupo'] = "";
        $data['materia'] = "";
        $data['aula'] = "";
        $data['alumno'] = [];
        $data['idgrupo'] = "";
        $data['menu'] = $this->sesion->get('menu');
        if($this->sesion->getFlashdata('datos')){
          $mensaje = $this->sesion->getFlashdata('datos');  
        }
        else{
            $mensaje = NULL;
        }
        $data['mensaje'] = $mensaje;
        return view('docentes/asistencia',$data);
    }
    /**
     * Función en la cual consulta el grupo del docente dependiendo del id de docente, periodo, día y hora actual
     */
    public function consultahorario(){
        $periodo = $this->getPeriodo();
        $iddocente = $this->sesion->get('id');
        $hoy = date("H").":00";
        $dia = date("N");
        $hora = date("H").":00:00";
        $fecha = date("Y-m-d");
        $fechayhora = $fecha.' '.$hora;
        $diaconvertido = $this->conviertedias($dia);
        $horario = $this->HorarioDocentesModel->buscahorario($iddocente,$periodo,$diaconvertido,$hoy);
        $asistencias = $this->AsistenciasAlumnos->buscaasistencias($fechayhora,$iddocente);
        if($horario == NULL){
            $mensaje = "No hay grupos asignados en esta hora";
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Docente/asistencia/');
        }
        elseif($asistencias != null){
            $mensaje = "La captura de asistencias de este horario ya se encuentra registrada";
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Docente/asistencia/');
        }
        else{
            $idgrupo = $horario[0]->grupo_id;
            $alumnos = $this->CapturaCalificacionesModel->buscaidcgrupo($idgrupo);
            $data['idgrupo'] = $idgrupo;
            $data['grupo'] = $horario[0]->grupo_clave;
            $data['materia'] = $horario[0]->materia_nombre;
            $data['aula'] = $horario[0]->aula_nombre;
            $data['alumno'] = $alumnos;
            $data['menu'] = $this->sesion->get('menu');
            return view('docentes/asistencia',$data);
        }
    }
    private function getPeriodo(){
        $periodo = $this->periodoModel->getPeriodoEstatus("CURSO");
        $id = null;
        foreach($periodo as $periodos){
            $id = $periodos->periodo_id;
        }
        return $id;
    }
    /**
     * Funcion para convertir el nombre del día
     * @param dia número de día
     * @return diaconvertido nombre de día
     */
    private function conviertedias($dia){
        switch($dia){
            case 1:
                $diaconvertido = 'LUNES';
                break;
            case 2:
                $diaconvertido = 'MARTES';
                break;
            case 3:
                $diaconvertido = 'MIERCOLES';
                break;
            case 4:
                $diaconvertido = 'JUEVES';
                break;
            case 5:
                $diaconvertido = 'VIERNES';
                break;
            case 6:
                $diaconvertido = 'SABADO';
                break;
            case 7:
                $diaconvertido = 'DOMINGO';
                break;
        }
        return $diaconvertido;
    }
    /**
     * Función para guardar en base de datos el conjunto de alumnos que se capturaron en el pase de lista diaria
     */
    public function guardaasistencias(){
        $data['grupo'] = "";
        $data['materia'] = "";
        $data['aula'] = "";
        $data['alumno'] = [];
        $data['idgrupo'] = "";
        $data['mensajeok'] = null;
        $data['mensajeerror'] = null;
        $iddocente = $this->sesion->get('id');
        $alumnos = $this->request->getPost('idalumno');
        $idgrupo = $this->request->getPost('idgrupo');
        $aula = $this->request->getPost('aula');
        $asistencia = $this->request->getPost('asistencia');
        $hora = date("H").":00:00";
        $hoy = date("Y-m-d");
        $horaydia = $hoy.' '.$hora;
        $AsistenciasDiarias_enti = new AsistenciasDiarias();
        try {
            for ($i = 0; $i < count($alumnos); $i++) {
              $AsistenciasDiarias_enti->id_grupo = $idgrupo;
              $AsistenciasDiarias_enti->matricula = $alumnos[$i];
              $AsistenciasDiarias_enti->docente = $iddocente;
              $AsistenciasDiarias_enti->fecha = $horaydia;
              $AsistenciasDiarias_enti->nombre_aula = $aula;
              $AsistenciasDiarias_enti->tipo_asistencia = $asistencia[$i];
              $this->AsistenciasDiarias->insert($AsistenciasDiarias_enti);             
            }
            $data['mensajeok'] = "SE GUARDO CORRECTAMENTE LAS ASISTENCIAS DE LOS ALUMNOS";
            log_message("notice", "Se capturó las asistencias del grupo ".$idgrupo." Docente: ".$iddocente);
          }
          catch(\Exception $ex) {
            $data['mensajeerror'] = "No se pudo guardar las asistencias".$ex;
          }
          $data['menu'] = $this->sesion->get('menu');
        return view('docentes/asistencia',$data);

    }
    /**
     * Funcion para desplegar la vista de modificación de asistencias o capturas extemporaneas
     */
    public function actualizacion_capturaEx(){
        $rol = $this->sesion->get('rol');
        $dias['primerdia']= null;
        $fechaactual = null;
        //$iddocente = $this->sesion->get('id');
        $iddocente = $this->request->getPost('docente');
        $periodo = $this->getPeriodo();
        $idgrupo = $this->request->getPost('idgrupo');
        $hora = $this->request->getPost('hora');
        $nuevahora = $hora;
        if(strlen($nuevahora) < 5){
            $nuevahora = '0'.$hora;
        }
        $nombre_materia = $this->request->getPost('nombre_materia');
        $aula = $this->request->getPost('aula');
        $fecha = $this->request->getPost('fecha');
        $seccion = $this->request->getPost('seccion');
        $dia = $this->request->getPost('dia');
        $btnModificacion = $this->request->getPost('modificación');
        $btnCaptura_ex = $this->request->getPost('captura_ex');
        $fechayhora = $fecha.' '.$hora;
        $horario = $this->HorarioDocentesModel->buscahorarioAsistencias($iddocente,$periodo,$dia,$nuevahora,$idgrupo);
        if($btnModificacion == '1'){
            if(count($horario) == 0){ //valida si existen valores con la fecha y hora capturada
                $mensaje = "No se encuentran grupos en la fecha y/u hora indicada";
                $this->sesion->setFlashdata('datos', $mensaje);
                return redirect()->to('/Docente/horarioGrupos/');
            }
            else{
                $asistencias = $this->AsistenciasAlumnos->buscaasistenciasxgrupo($fechayhora,$iddocente,$idgrupo);
                if(count($asistencias) == 0){ //valida si existen datos en la tabla de asistencias; redirecciona a la pantalla en donde captura asistencia extemporanea
                    $mensaje = "No se encuentran registros guardados, realiza una captura extemporanea";
                    $this->sesion->setFlashdata('datos', $mensaje);
                    return redirect()->to('/Docente/horarioGrupos/');
                }
                else{
                    if($rol === 'GENERAL'){
                        $dias = $this->getdiasxmes();
                        $fechaactual = date("Y-m-d");
                    }
                    $data['idgrupo'] = $idgrupo;
                    $data['grupo'] = $seccion;
                    $data['materia'] = $nombre_materia;
                    $data['aula'] = $aula;
                    $data['alumno'] = $asistencias;
                    $data['fechayhora'] = $fechayhora;
                    $data['fechamin'] = $dias['primerdia'];;
                    $data['fechamax'] = $fechaactual;
                    $data['fecha'] = $fecha;
                    $data['hora'] = $hora;
                    $data['docente'] = $iddocente;
                    $data['menu'] = $this->sesion->get('menu');
                    return view('docentes/modificacionasistencias',$data);
                }
            }
            return view('docentes/modificacionasistencias',$data);
        }
        elseif($btnCaptura_ex == '1'){
            if(count($horario) == 0){ //valida si existen valores con la fecha y hora capturada
                $mensaje = "No se encuentran grupos asignados con la fecha y hora seleccionada";
                $this->sesion->setFlashdata('datos', $mensaje);
                return redirect()->to('/Docente/horarioGrupos/');
            }
            else{
                $asistencias = $this->AsistenciasAlumnos->buscaasistenciasxgrupo($fechayhora,$iddocente,$idgrupo);
                if(count($asistencias) == 0){ //valida si existen datos en la tabla de asistencias; redirecciona a la pantalla en donde captura asistencia extemporanea
                    $data['fecha'] = $fecha;
                    $data['fechayhora'] = $fechayhora;
                    $data['hora'] = $hora;
                    $data['idgrupo'] = $idgrupo;
                    $data['grupo'] = $horario[0]->grupo_clave;
                    $data['materia'] = $horario[0]->materia_nombre;
                    $data['aula'] = $horario[0]->aula_nombre;
                    $alumnos = $this->CapturaCalificacionesModel->buscaidcgrupo($idgrupo);
                    $data['alumno'] = $alumnos;
                    $data['docente'] = $iddocente;
                $data['menu'] = $this->sesion->get('menu');
                    return view('docentes/asistenciasextemporaneas',$data);
                }
                if(count($asistencias) != 0){ //valida si existen datos en la tabla de asistencias; redirecciona a la pantalla en donde captura asistencia extemporanea
                    $mensaje = "Ya se ha capturado las asistencias del día seleccionado, realiza la actualización de asistencias";
                    $this->sesion->setFlashdata('datos', $mensaje);
                    return redirect()->to('/Docente/horarioGrupos/');
                }
            }
            //return view('docentes/modificacionasistencias',$data);
        }
    }
    /**
     * Función que solicita actualización y redirecciona la pantalla de modificación de asistencias
     */
    public function guardaactualizacion(){
        $docente = $this->request->getpost('docente');
        $fechayhora = $this->request->getpost('fechayhora');
        $alumnos = $this->request->getPost('idalumno');
        $asistencias = $this->request->getPost('asistencia');
        $actualización = $this->actualizaasistencias($alumnos,$fechayhora,$asistencias);
        if($actualización['mensajeok']){
            $mensaje = $actualización['mensajeok'];
            $this->sesion->setFlashdata('mensajeok', $mensaje);
        }
        else{
            $mensaje = $actualización['mensajeerror'];
            $this->sesion->setFlashdata('mensajenok', $mensaje);
        }
        $this->sesion->setFlashdata('iddocente', $docente);
        return redirect()->to('/Docente/horarioGrupos/');
    }
    /**
     * funcion que actualiza en base de datos las asistencias
     */
    private function actualizaasistencias($alumnos,$fechayhora,$asistencias){
        try {
            for ($i = 0; $i < count($alumnos); $i++) {
                $actualiza = [
                    'asistencia_tipo' => $asistencias[$i]
                ];
                $this->AsistenciasDiarias->updateasistencias($alumnos[$i],$fechayhora,$actualiza); 
                log_message("notice", "Se actualizó la asistencia del alumno ".$alumnos[$i].' Docente que actualizó: '.$this->sesion->get('id'));
              }
            $data['mensajeok'] = "Se actualizó correctamente el estatus de asistencia";
        } catch (\Exception $ex) {
            $data['mensajeerror'] = 'No fue posible actualizar el estatus de asistencia. ' . $ex->getMessage();
        }
        return $data;

    }
    /**
     * Funcion que guarda las asistencias extemporaneas en base de datos y redirecciona a la pantalla
     */
    public function guardaasistenciasExtemporaneas(){
        //$iddocente = $this->sesion->get('id');
        $iddocente = $this->request->getPost('docente');
        $alumnos = $this->request->getPost('idalumno');
        $idgrupo = $this->request->getPost('idgrupo');
        $aula = $this->request->getPost('aula');
        $asistencia = $this->request->getPost('asistencia');
        $horaydia = $this->request->getpost('fechayhora');
        $AsistenciasDiarias_enti = new AsistenciasDiarias();
        try {
            for ($i = 0; $i < count($alumnos); $i++) {
              $AsistenciasDiarias_enti->id_grupo = $idgrupo;
              $AsistenciasDiarias_enti->matricula = $alumnos[$i];
              $AsistenciasDiarias_enti->docente = $iddocente;
              $AsistenciasDiarias_enti->fecha = $horaydia;
              $AsistenciasDiarias_enti->nombre_aula = $aula;
              $AsistenciasDiarias_enti->tipo_asistencia = $asistencia[$i];
              $this->AsistenciasDiarias->insert($AsistenciasDiarias_enti);          
            }
            $mensaje = "SE GUARDO CORRECTAMENTE LAS ASISTENCIAS EXTEMPORANEAS DE LOS ALUMNOS";
            $this->sesion->setFlashdata('mensajeok', $mensaje);
            log_message("notice", "Se capturó las asistencias del grupo ".$idgrupo." Docente que capturo asistencia extemporanea: ".$this->sesion->get('id'));
          }
          catch(\Exception $ex) {
            $mensaje = "No se pudo guardar las asistencias";
            $this->sesion->setFlashdata('mensajeerror', $mensaje);
          }
            $this->sesion->setFlashdata('iddocente', $iddocente);
            return redirect()->to('/Docente/horarioGrupos/');
    }
    /**
     * Funcion para desplegar la vista de filtros de modificación de asistencias de día
     */
    public function asistenciasextemporaneas()
    {
        $periodo = $this->getPeriodo();
        $iddocente = $this->sesion->get('id');
        $data = $this->request->getPost();
        if (!count($data) || !array_key_exists('grupo', $data)) {
            $data = ['grupo' => '','materia' => '','dias' => ''];
        }
        $materias = $this->HorarioDocentesModel->buscamaterias($iddocente,$periodo);
        $datosdocente = $this->HorarioDocentesModel->buscar($data,$iddocente,$periodo);
        $data['arrmaterias'] = $materias;
        $data['datosdocente'] = $datosdocente;
        $data['pager'] = $this->HorarioDocentesModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        if($this->sesion->getFlashdata('datos')){
            $data['error'] = $mensaje = $this->sesion->getFlashdata('datos');  
        }
            else{
                $mensaje = NULL;
        }
        return view('docentes/busquedaasistenciasextemporanea', $data);
    }
    /**
     * Función para desplegar la vista con el horario del docente para realizar la modificación o captura extemporanea de asistencias
     */
    public function horarioGrupos()
    {
        $iddocente = $this->request->getpost('docente');
        $rol = $this->sesion->get('rol');
        $dias['primerdia']= null;
        $fechaactual = null;
        $periodo = $this->periodoModel->getPeriodoHorarioDocente();
        if ($periodo) {
            if($rol === 'GENERAL'){
                $dias = $this->getdiasxmes();
                $fechaactual = date("Y-m-d");
            }
            if($iddocente != null){
                $grupos =  $this->HorarioDocentesModel->buscarGrupos($iddocente, $periodo);    
            }
            elseif($this->sesion->getFlashdata('iddocente')){
                $iddocente = $this->sesion->getFlashdata('iddocente');
                $grupos =  $this->HorarioDocentesModel->buscarGrupos($iddocente, $periodo);    
            }else{
                $iddocente = $this->sesion->get('id');
                $grupos =  $this->HorarioDocentesModel->buscarGrupos($iddocente, $periodo);    
            }
            $data['grupos'] =  $grupos;
            $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
            $data['fechamin'] = $dias['primerdia'];
            $data['fechamax'] = $fechaactual;
            $data['docente'] = $iddocente;
            $data['menu'] = $this->sesion->get('menu');
            if($this->sesion->getFlashdata('datos')){
                $data['error'] = $this->sesion->getFlashdata('datos');  
            }
                else{
                    $data['error'] = NULL;
            }
            if($this->sesion->getFlashdata('mensajeok')){
                $data['mensajeok'] = $this->sesion->getFlashdata('mensajeok');
            }
                else{
                    $data['mensajeok'] = NULL;
            }
            if($this->sesion->getFlashdata('mensajenok')){
                $data['mensajeerror'] = $this->sesion->getFlashdata('mensajenok');  
            }
                else{
                    $data['mensajeerror'] = NULL;
            }
            return view('docentes/modificacion_captura', $data);
        } else {
            $data['menu'] = $this->sesion->get('menu');
            return view('docentes/nodisponible', $data);
        }
    }
    /**
     * Función para desplegar la vista de los reportes de asistencias dependiendo del rol del usuario
     */
    public function resportesAsistencias(){
        $periodo = $this->getPeriodo();
        $rol = $this->sesion->get('rol');
        $data['rol'] = null;
        $data['docente'] = null;
        $data['grupo'] = null;
        if ($rol === 'GENERAL') {
            $iddocente = $this->sesion->get('id');
            $data['rol'] = 1;
            $data['docente'] = $iddocente;
            $docentes = $this->HorarioDocentesModel->docentesAll($periodo);
            $data['grupos'] = $this->HorarioDocentesModel->buscamaterias($iddocente,$periodo);
        }
        if($rol =='DIRECTOR MEDICINA' OR $rol =='SUBDIRECCION MEDICINA' OR $rol =='JEFATURA ACADEMICA' ){
            $docentes = $this->HorarioDocentesModel->buscadocentes('MED',$periodo);
            $data['jefatura'] = 1;
        }
        elseif($rol =='DIRECTOR ENFERMERIA' OR $rol =='SUBDIRECCION ENFERMERIA' OR $rol =='JEFATURA ENFERMERIA'){
            $docentes = $this->HorarioDocentesModel->buscadocentes('LEO',$periodo);
            $data['jefatura'] = 1;
            //$grupo = $this->HorarioDocentesModel->materiasxcarrera('LEO',$periodo);
        }
        elseif ($rol == 'ADMINISTRADOR' OR $rol == 'SECRETARIOS') {
            $docentes = $this->HorarioDocentesModel->docentesAll($periodo);
            $data['jefatura'] = 1;
        }
        $dias = $this->getdiasxmes();
        $data['fechainicio'] = null;
        $data['fechafinal'] = null;
        //$data['grupo'] = null;
        $data['docentes'] = $docentes;
        $data['asistencias'] = [];
        $data['fechamin'] = $dias['primerdia'];
        $data['fechamax'] = $dias['ultimodia'];
        $data['menu'] = $this->sesion->get('menu');
        if($this->sesion->getFlashdata('datos')){
            $mensaje = $this->sesion->getFlashdata('datos');  
          }
          else{
              $mensaje = NULL;
          }
          $data['mensaje'] = $mensaje;
        return view('docentes/reporteasistencias',$data);
    }
    /**
     * Obtiene las materias correspondientes de un docente en especifico 
     */
    public function getmateriasxdocente($iddocente)
    {
        $periodo = $this->getPeriodo();
        if ($iddocente) {
            $campos = $this->HorarioDocentesModel->materiasxcarrera($iddocente,$periodo);

            if ($campos) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($campos);
            } else {
                return null;
            }
        }
    }
    /**
     * Realiza la busqueda de los registros de asistencias con base a los parámetros que el usuario colocó
     */
    public function buscaAsistenciasReporte(){
        $periodo = $this->getPeriodo();
        $rol = $this->sesion->get('rol');
        if ($rol === 'GENERAL') {
            $iddocente = $this->sesion->get('id');
            $data['rol'] = 1;
            $data['docente'] = $iddocente;
            $docentes = $this->HorarioDocentesModel->docentesAll($periodo);
            $data['grupos'] = $this->HorarioDocentesModel->buscamaterias($iddocente,$periodo);
        }
        if($rol =='DIRECTOR MEDICINA' OR $rol =='SUBDIRECCION MEDICINA' OR $rol =='JEFATURA ACADEMICA' ){
            $docentes = $this->HorarioDocentesModel->buscadocentes('MED',$periodo);
            $data['jefatura'] = 1;
        }
        elseif($rol =='DIRECTOR ENFERMERIA' OR $rol =='SUBDIRECCION ENFERMERIA' OR $rol =='JEFATURA ENFERMERIA'){
            $docentes = $this->HorarioDocentesModel->buscadocentes('LEO',$periodo);
            $data['jefatura'] = 1;
        }
        elseif ($rol == 'ADMINISTRADOR' OR $rol == 'SECRETARIOS') {
            $docentes = $this->HorarioDocentesModel->docentesAll($periodo);
            $data['jefatura'] = 1;
        }
        $fechainicio = $this->request->getPost('fechainicio');
        $fechafin = $this->request->getPost('fechafin');
        $docente = $this->request->getPost('selectdocente');
        $grupo = $this->request->getPost('selectgrupo');
        $asistencias = $this->AsistenciasAlumnos->asistenciasReporte($fechainicio,$fechafin,$docente,$grupo);
        if(count($asistencias) == 0){
            $mensaje = "No hay registro de asistencias capturadas del grupo asignado";
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Docente/resportesAsistencias/');
        }
            $alumnos = $this->grupos($fechainicio,$fechafin,$docente,$grupo);
            $dias = $this->getdiasxmes();
            $listaasistencias = $this->generalista($alumnos,$asistencias,$fechainicio,$fechafin);
            $fechas = $this->getfechas($asistencias,$fechainicio,$fechafin)['fechas'];
            $data['fechainicio'] = $fechainicio;
            $data['fechafinal'] = $fechafin;
            $data['grupo'] = $grupo;
            $data['docente'] = $docente;
            $data['docentes'] = $docentes;
            $data['fechamin'] = $dias['primerdia'];
            $data['fechamax'] = $dias['ultimodia'];
            $data['tabla'] = $listaasistencias;
            $data['asistencias'] = $this->generalista($alumnos,$asistencias,$fechainicio,$fechafin);
            $data['catalogofechas'] = $fechas;
            $data['menu'] = $this->sesion->get('menu');
            return view('docentes/reporteasistencias',$data);
    }
    /**
     * retorna los grupos por medio del docente
     */
    public function grupos($fechainicio,$fechafin,$docente,$grupo){
        $grupos = $this->AsistenciasAlumnos->grupo($fechainicio,$fechafin,$docente,$grupo);
        return $grupos;
    }

    /**
     * renderiza la vista con el reporte de asistencias
     * @param alumnos arreglo de los alumnos por grupo
     * @param asistencias arreglo con las asistencias del grupo
     * @param fechainicio fecha de inicio del periodo a consultar
     * @param fechafin fecha final del periodo a consultar
     * 
     */
    private function generalista($alumnos,$asistencias,$fechainicio,$fechafin){
        $arr_datos = NULL;
        foreach($alumnos as $alumno){
            $asistenciaValor= [];
            $inasistencia = [];
            $retardo = [];
            $faltajustificada = [];
            $tipoasistencias = [];
            $arregloasistencias = [];
            $fecha_arr = $this->getfechas($asistencias,$fechainicio,$fechafin)['arreglofechas'];
            foreach ($asistencias as $asist) {
                if ($alumno->alumno_id == $asist->alumno_id){
                    if($asist->asistencia_tipo == 'ASISTENCIA'){
                        $asistenciaValor[] = $asist->asistencia_tipo;
                    }
                    elseif($asist->asistencia_tipo == 'INASISTENCIA'){
                        $inasistencia[] = $asist->asistencia_tipo;
                    }
                    elseif($asist->asistencia_tipo == 'RETARDO'){
                        $retardo[] = $asist->asistencia_tipo;
                    }
                    elseif($asist->asistencia_tipo == 'FALTA JUSTIFICADA'){
                        $faltajustificada[] = $asist->asistencia_tipo;
                    }
                    $fecha[$asist->asistencia_fecha] = array($asist->asistencia_fecha);
                    $tipoasistencias[] = $asist->asistencia_tipo;
                }
            }
            $arregloasistencias = $this->generaAsistencia($fecha_arr,$fecha,$tipoasistencias);
            $catalogo = ['ASISTENCIA','INASISTENCIA','RETARDO','FALTA JUSTIFICADA'];
            $catalogodias = [];
            $int_asistencias = $this->getCountArray($catalogo, array_count_values($asistenciaValor));
            $int_inasistencias = $this->getCountArray($catalogo, array_count_values($inasistencia));
            $int_retardo = $this->getCountArray($catalogo, array_count_values($retardo));
            $int_faltajustificada = $this->getCountArray($catalogo, array_count_values($faltajustificada));
            $total = $int_asistencias+$int_inasistencias+$int_retardo+$int_faltajustificada;
            $arr_datos []= [
                'matricula' => $alumno->alumno_id,
                'nombre' => $alumno->alumno_ap_paterno.' '.$alumno->alumno_ap_materno.' '.$alumno->alumno_nombres,
                'asistencias' => $int_asistencias,
                'inasistencias' => $int_inasistencias,
                'retardo' => $int_retardo,
                'faltajustificada' => $int_faltajustificada,
                'total' => $total,
                'tipoasistencia' => [$arregloasistencias]
            ];
        }
        return $arr_datos;
    }
    private function getCountArray($asistencias, $arr_contadores)
    {
        $contadores = 0;
        foreach ($asistencias as $asistencia) {
            if (array_key_exists($asistencia, $arr_contadores)) {
                $contadores = $arr_contadores[$asistencia];
            }
        }
        return $contadores;
    }
    private function getdiasxmes(){
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        $dias['ultimodia'] = date('Y-m-d', mktime(0,0,0, $month, $day, $year));
        $dias['primerdia'] = date('Y-m-d', mktime(0,0,0, $month, 1, $year));
        return $dias;
    }
    private function getfechas($asistencias,$fechainicio,$fechafin){
        $fechas = [];
        $arregloasistencias = [];
        $arr_fechas = [];
        $diasmateria = [];
        $arr_nuevo=[];
        $arreglo_fechas =[];
        $matricula = $asistencias[0]->alumno_id;
        $diassemana = array("DOMINGO","LUNES","(MARTES)","MIERCOLES","JUEVES","VIERNES","SABADO");
        $fecha_completa = [];
        foreach($asistencias as $asistencia){
            if($matricula == $asistencia->alumno_id){
                $fechas[] = substr($asistencia->asistencia_fecha, 0, -9);
                $fecha_completa[] = $asistencia->asistencia_fecha;
            }
        }
        $diasmes = $this->generadiasmes($fechainicio,$fechafin);
        //$diasmateria = array_unique($arr_nuevo);
        foreach($diasmes as $diames => $value){
            $fechaconvertida = strtotime($value[0]);
            $diaacomparar = $diassemana[date('w',$fechaconvertida)];
            $contador = 0;
            foreach($fechas as $fecha){
                if($fecha == $value[0]){
                    $arr_fechas[] = $fecha_completa[$contador].' '.$diaacomparar;
                    $arreglo_fechas[] = $fecha_completa[$contador];
                }
                $contador++;
            }
         }
        $datos['arreglofechas'] = $arreglo_fechas;
        $datos['fechas'] = $arr_fechas;
        return $datos;
    }
    private function generadiasmes(string $fechainicio, string $fechafin = null) {
        $datos = [];
        if (!$fechafin) {
            $fechafin = date('Y-m-d');
        }
        $fechainicio = strtotime($fechainicio);
        $fechafin = strtotime($fechafin);
        do {
            $periodo = date('Y-m-d', $fechainicio); // para agrupar por periodo AÑO-MES
            $datos[$periodo][] = date('Y-m-d', $fechainicio);
            $fechainicio = strtotime("+ 1 day", $fechainicio);
        } while($fechainicio <= $fechafin);
        return $datos;
    }
    /**
     * genera el arreglo a utilizar para generar el reporte de asistencias a partir de un rango de fechas
     * @param fecha_arr arreglo que contiene el rango de fechas solocitadas para el reporte 
     * @param fecha arreglo del conjunto de fechas en base de datos
     * @param tipoasistencias arreglo que contiene los 4 tipos de asistencias
     * @return arregloasistencias arreglo con los datos para el reporte de asistencias
     */

    private function generaAsistencia($fecha_arr,$fecha,$tipoasistencias){
        $arregloasistencias =[];
        foreach($fecha_arr as $fecha_ar){
            if(array_key_exists($fecha_ar,$fecha)){
                $contador = 0;
                foreach($fecha as $fecha_ex => $fecha_exist){
                    foreach($fecha_exist as $fecha1){
                        if($fecha_ar == $fecha1){
                            if(isset($tipoasistencias[$contador])){
                                $arregloasistencias[] = $tipoasistencias[$contador];
                            }
                            else{
                                $arregloasistencias[] = '-';
                            }
                        }
                        $contador++;
                    }
                }
            }
            else{
                $arregloasistencias[] = '-';
            }
        }
        return $arregloasistencias;
    }
}
