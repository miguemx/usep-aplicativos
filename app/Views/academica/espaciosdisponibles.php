<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<style>
    .error {
        border: red 2px solid;
    }

    datalist {
        width: 50000px;
        height: 80px;
    }

    .icono-1:hover {
        background-color: #4dff4d;
        /* Green */
        color: white;
        /* border: 2px solid red; */
    }

    .boton-agregar {
        background-color: #ceccb5;
    }
</style>
<div class="container ">
    <div class="bg-white rounded shadow p-1 m-4 ">
        <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        Buscar espacios disponibles
                    </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        <form action="<?php echo base_url('ProgramacionAcademica/EspaciosDisponibles'); ?>" method="post" id="EspaciosDisponibles">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="select_periodo" class="form-label">Periodo</label>
                                        <select class="form-select" name="select_periodo" id="select_periodo" onchange="getDescripccion(this)">
                                            <option value="0">Seleccione un periodo</option>
                                            <?php foreach ($periodos as $lista_periodo) : ?>
                                                <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo_selected == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <small></small>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="select_dia" class="form-label">Dia</label>
                                        <select class="form-select form-select-sm" name="select_dia" id="select_dia">
                                            <option selected value="0">Seleccione un dia disponible</option>
                                            <?php foreach ($dias as $dia) : ?>
                                                <option value="<?php echo $dia ?>" <?php echo (($dia_selected == $dia) ? 'selected' : ''); ?>><?php echo $dia ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="text-center mt-3">
                                    <button type="button" class="btn btn-secondary" onclick="buscar()">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function buscar() {
        var periodo = document.getElementById('select_periodo');
        var dia = document.getElementById('select_dia');
        if (periodo.value !== '0' && dia.value !== '0') {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
            Toast.fire({
                icon: 'success',
                title: 'Buscando.'
            })
            document.getElementById('EspaciosDisponibles').submit();

        } else {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'info',
                title: 'Selecciona un día y un periodo valido para realizar la búsqueda.'
            })
        }
    }
</script>

<style>
    .contenedor_tabla {
        width: 80hw;
        overflow-x: scroll;
    }

    .container_table {
        height: 79vh;
    }

    .bloque {
        border: #5c5c63 1px solid;
        border-radius: 5px;
    }
</style>
<div class="overflow-auto">
    <div class="container-fluid container_table">
        <table class="table table-borderless   text-center table-hover">
            <thead class="thead-default" style="position: sticky;top: -1px; background-color:whitesmoke;">
                <tr>
                    <div class="row">

                        <?php $bandera_row_dias = []; ?>
                        <!-- <th >HORA</th> -->
                        <th data-field="hora" data-filter-control="input" data-sortable="true">HORA</th>
                        <?php foreach ($aulas as $aula_th) : ?>
                            <th class="col"><?php echo $aula_th->nombre_corto ?></th>
                            <?php $bandera_row_dias[] = 0; ?>
                        <?php endforeach; ?>
                    </div>
                </tr>
            </thead>
            <tbody>
                <?php $hora = 7; ?>
                <?php $operacion = 1; ?>
                <?php while ($hora < 21) : ?>
                    <?php $index = 0; ?>
                    <tr>
                        <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                        <?php foreach ($aulas as $aula) : ?>
                            <?php $bandera_coincidencia = false; ?>
                            <?php if ($horas_ocupadas != false) : ?>
                                <?php foreach ($horas_ocupadas as $hora_actual) : ?>
                                    <?php if ($hora_actual->aula == $aula->id_aula) : ?>
                                        <?php if ($hora_actual->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) :
                                            $inicio = explode(':', $hora_actual->h_inicio);
                                            $fin = explode(':', $hora_actual->h_fin);
                                            $row_span = (intval($fin[0]) - intval($inicio[0]));
                                        ?>
                                            <td class="align-middle bloque" rowspan="<?php echo $row_span + 1 ?>">
                                                <div <?php echo $hora_actual->grupo ?>>


                                                    <?php if (($row_span + 1) > 1) {
                                                        $bandera_row_dias[$index] = ($row_span) + $bandera_row_dias[$index];
                                                    } ?>
                                                    <?php echo  $hora_actual->ma_nombre . '<br>' . (($hora_actual->docente == 1) ? 'Por asignar' : mb_strtoupper($hora_actual->em_paterno . ' ' . $hora_actual->em_materno . ' ' . $hora_actual->em_nombre)) . "<br>" . $hora_actual->au_nombre . "<br>" . $hora_actual->seccion
                                                    ?>
                                                </div>
                                            </td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif ?>

                            <?php if (!$bandera_coincidencia) : ?>
                                <?php //if ($operacion == 109 ){ echo $bandera_row_dias[$index] . "<br> " . json_encode($bandera_row_dias); }  
                                ?>
                                <?php if ($bandera_row_dias[$index] > 0) : ?>
                                    <?php $bandera_row_dias[$index]--; ?>
                                <?php else : ?>
                                    <td style="background-color: #bdb994;" class="align-middle">

                                        <?php // echo " $aula->nombre_corto <br>" . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) 
                                        ?>
                                    </td>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $operacion++; ?>
                            <?php $index++; ?>
                        <?php endforeach; ?>

                    </tr>
                    <?php $hora++; ?>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>

</div>
<script></script>
<?php echo $this->endSection() ?>