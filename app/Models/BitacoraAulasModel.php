<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\BitacoraAulas;
class BitacoraAulasModel extends Model
{
    protected $table="bitacora_aulas";
    protected $primarykey=" ";
    protected $returntype="App/Entities/BitacoraAulas";
    protected $allowedFields=['num','id_docente','aula','aula_conf','f_pantalla','f_cpu','f_teclado','f_mouse','f_microfono','f_camara','f_control',
                                'f_hubtab','f_hubdisplay','f_bocinas','f_cables','fecha'];
    protected $db;
    protected $builder;
    
    
}

?>