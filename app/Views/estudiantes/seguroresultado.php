<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>
        <h2>Seguro Facultativo</h2>
        <hr />
    </div>
    

    <?php if( $alumnoSeguro->status == 'RECHAZADO' ): ?>

        <div class="alert alert-danger">
            <p>
                Has decidido rechazar el beneficio del seguro facultativo que te ofrece la USEP.
            </p>
            <p>Si tienes alguna duda, favor de escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a>.</p>
        </div>

    <?php else: ?>

        <div class="alert alert-success">
            <p>
                <strong>Has completado el registro para el seguro facutativo que te otorga la USEP.</strong>
            </p>
            <p>
                La documentación proporcionada será sujeta a validación.
                Si ésta es correcta, se solicitará tu afiliación al IMSS. 
            </p>
            <p>
                En un lapso máximo de 8 días hábiles deberás consultar nuevamente 
                tu constancia de vigencia de derechos en la cual deberás contar con un estatus de Activo por parte de la Universidad de la Salud
                y podrás acudir a tu clínica para el alta correspodiente o visitar el siguiente enlace:
            </p>
            <p>
                <a href="https://serviciosdigitales.imss.gob.mx/portal-ciudadano-web-externo/derechohabientes/tramite/registro" target="_blank">
                Alta en clínica o UMF con CURP
                </a>
            </p>
            <p>Si tienes alguna duda, favor de escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a>.</p>
        </div>
    
    <?php endif; ?>
   
</div>

<?php echo $this->endSection() ?>