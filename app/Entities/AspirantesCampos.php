<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesCampos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'campo' => null,
        'formulario' => null,
        'tipo' => null,
        'dependiente' => null,
        'nombre' => null,
        'descripcion' => null,
        'orden' => null,
        'obligatorio' => null,
        'visible' => null,
        'creado_al' => null,
        'actualizado_al' => null,
        'borrado_al' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'campo' => 'campo_id',
        'formulario' => 'campo_formulario',
        'tipo' => 'campo_tipo',
        'dependiente' => 'campo_dependiente',
        'nombre' => 'campo_nombre',
        'descripcion' => 'campo_descripcion',
        'orden' => 'campo_orden',
        'obligatorio' => 'campo_obligatorio',
        'visible' => 'campo_visualizar',
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}