<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div>
        <img src="<?php echo base_url('img/logobn.jpeg'); ?>" alt="USEP" width="150" />
    </div>
    <div>
        <h3 style="text-align: center;">
            Universidad de la Salud del Estado de Puebla<br />
            Dirección de Servicios Escolares y Titulación<br />
            
        </h3>
        <h4 style="text-align: center;">Horario de clase</h4>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>SECCION</td>
                        <td>ASIGNATURA</td>
                        <td>Lunes</td>
                        <td>Martes</td>
                        <td>Miércoles</td>
                        <td>Jueves</td>
                        <td>Viernes</td>
                        <td>Sábado</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $horario as $grupo ): ?>
                        <tr>
                            <td><?php echo $grupo->seccion; ?></td>
                            <td><?php echo $grupo->materiaClave.'<br />'.$grupo->materia; ?></td>
                            <td><?php echo $grupo->lunes; ?></td>
                            <td><?php echo $grupo->martes; ?></td>
                            <td><?php echo $grupo->miercoles; ?></td>
                            <td><?php echo $grupo->jueves; ?></td>
                            <td><?php echo $grupo->viernes; ?></td>
                            <td><?php echo $grupo->sabado; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div>



<?php echo $this->endSection() ?>