<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .error {
        border: red 2px solid;
    }
</style>
<div class="container">
    <?php if ($mensaje) : ?>
        <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
            <?php echo $mensaje['texto']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="datos text-center">
            <div class="datos text-center">
                <label style="margin: 0 10px">Título:<h4 class="card-title"> <?php echo $evaluacion->titulo;  ?></h4> </label>
                <label style="margin: 0 10px">Periodo:<h4 class="card-title"> <?php echo $evaluacion->periodo  ?></h4> </label>
                <label style="margin: 0 10px">Tipo de evaluación :<h4 class="card-title"> <?php
                foreach($tipos_evaluacion as $tipo) {
                    if($tipo->id ==$evaluacion->tipo ) echo $tipo->nombre;
                }
                 ?></h4> </label>
                <label style="margin: 0 10px">Estado :<h4 class="card-title"> <?php echo $evaluacion->estatus;  ?></h4> </label>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="table-responsive-xl">
            <table class="table table-striped-columns
            table-hover	
            table-borderless
            
            align-middle">
                <thead class="table-light">
                    <caption>Formularios</caption>
                    <tr>
                        <th>#</th>
                        <th>Título</th>
                        <th>Instrucciones</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    <?php if ($formularios) : ?>
                    <?php // echo json_encode($formularios) ?>
                        <?php foreach ($formularios as $formulario) : ?>
                            <tr>
                                <td scope="row"><?php echo $formulario->orden ?></td>
                                <td><?php echo $formulario->nombre ?></td>
                                <td><?php echo (($formulario->instrucciones)?$formulario->instrucciones:'Sin instrucciones') ?></td>
                                <td class="text-right">
                                    <div class="d-flex justify-content-end">
                                        <div>
                                            <a class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/ContenidoFormulario/' . $formulario->id) ?>">Ver</a>

                                        </div>
                                        <div>
                                            <div class="dropdown">
                                                <button class="btn btn-default btn-sm " type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                                                        <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                                    </svg>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="<?php echo base_url('EvaluacionDocenteController/DetallesFormulario/' . $formulario->id) ?>">Editar</a></li>
                                                    <li>
                                                        <button type="button" class="dropdown-item" onclick="validar(this)" value="<?php echo  $formulario->id . '|' . $formulario->nombre ?>">Eliminar</button>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td class="text-center" colspan="4">No hay formularios pertenecientes a esta evaluación</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <div class="m-2">
                        <form action="<?php echo base_url('EvaluacionDocenteController/AgragarFormulario/' . $evaluacion->id) ?>" method="post">
                            <button type="submit" class="btn btn-secondary">Agregar Formulario</button>
                        </form>
                        <a class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Menu/') ?>" role="button">Regresar al menú</a>
                    </div>
                </tfoot>
            </table>
        </div>

    </div>
</div>
<form action="<?php echo base_url('EvaluacionDocenteController/EliminarFormulario/' . $evaluacion->id) ?>" method="post" id="form_eliminar">

</form>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function validar(datos) {
        console.log(datos.value);
        var pregunta = datos.value.split('|');
        Swal.fire({
            'title': 'Confirmación',
            'html': '¿Seguro que desea eliminar la pregunta "' + pregunta[1] + '"?',
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': 'Se ha eliminado la pregunta "' + pregunta[1] + '"?',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                var form = document.getElementById('form_eliminar');
                var input = document.createElement('input');
                input.value = pregunta[0];
                input.name = 'formulario';
                input.hidden = true;
                form.appendChild(input);
                form.submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>