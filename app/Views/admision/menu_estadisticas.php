<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
        height: 400px;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #ebebeb;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    .container {
        max-height: 90vh;
        overflow-y: scroll;
    }
    #datatable,#datatable1 {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable,#datatable1 caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable,,#datatable1 th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable,#datatable1 td,
  #datatable,#datatable1 th,
  #datatable,#datatable1 caption {
    padding: 0.5em;
  }

  #datatable,#datatable1 thead tr,
  #datatable,#datatable1 tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<div class="container ">
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
        <div class="row ">
            <h1 class="text-center my-2">Datos estadísticos</h1>
            <div class="accordion accordion-flush mt-2" id="menu_estadisticcas">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            DATOS GENERALES DE ASPIRANTE
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="" aria-labelledby="flush-headingOne" data-bs-parent="#menu_estadisticcas">
                        <div class="accordion-body">
                            <form action="<?php echo base_url('EstadisticasAdmision/estadisticaregiones'); ?>" method="post">
                                <button class="list-group-item list-group-item-action border-0 " type="submit" value='ingreso' >Estadísticas por región</button>
                            </form>
                            <form action="<?php echo base_url('EstadisticasAdmision/estadisticavalidadodet'); ?>" method="post">
                                <button class="list-group-item list-group-item-action border-0" type="submit" name='revalidacion' value='revalida'>Estadísticas de aspirantes con folio y género</button> 
                            </form>
                        </div>
                    </div><!--
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            ESTATUS DE REGISTRO
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#menu_estadisticcas">
                        <div class="accordion-body">

                            <form action="<?php// echo base_url('EstadisticasAdmision/estatusregistro'); ?>" method="post">
                            </form>
                             <button type="submit" name='status' value='Validacion de correo' class="list-group-item list-group-item-action border-0 " onclick="ValidacionCorreo()">En validación de correo</button> 
                            <button type="submit" name='status' value='Captura de informacion' class="list-group-item list-group-item-action border-0 " onclick="graficar(this)">En captura de información</button>
                            <button type="submit" name='status' value='Carga de documentos' class="list-group-item list-group-item-action border-0 " onclick="graficar(this)">En carga de documentos</button>
                            <button type="submit" name='status' value='Revisión documental' class="list-group-item list-group-item-action border-0 " onclick="graficaotros(this)">En revision documental</button>
                            <button type="submit" name='status' value='Observaciones' class="list-group-item list-group-item-action border-0 " onclick="graficaotros(this)">Con observaciones</button>
                            <button type="submit" name='status' value='Registro validado' class="list-group-item list-group-item-action border-0 " onclick="graficaotros(this)">Aspirantes Aceptados</button>
                            <button type="submit" name='status' value='Registro declinado' class="list-group-item list-group-item-action border-0 " onclick="graficaotros(this)">Aspirantes Declinados</button>
                            <button type="submit" name='status' value='total' class="list-group-item list-group-item-action border-0 " onclick="graficatotal()">Total</button>
                        </div>
                    </div>
                </div>-->
                <div class="accordion-item">
                    <h2 class="accordion-header" id="">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                            POR INDICADOR
                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="" aria-labelledby="flush-headingThree" data-bs-parent="#menu_estadisticcas">
                        <div class="accordion-body">
                            <div class="card-body">
                                <div class="mb-3">
                                    <label for="select-tipocampo" class="form-label">Seleccione el tipo de campo</label>
                                    <select class="form-select" name="select-tipocampo" id="select-tipocampo" onchange="getPreguntas()">
                                        <option selected>Seleccione una opcion disponible</option>
                                        <?php foreach ($campos as $campo) : ?>
                                            <option value="<?php echo $campo->id ?>"><?php echo $campo->nombre ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <small id="help-tipo-campo" class="form-text text-muted">Selecciona un tipo de dato para que se desplieguen las opciones de la seleccion de preguntas</small>

                                </div>
                                <div class="mb-3 select-pregunta">
                                    <label for="selec-pregunta" class="form-label">Selecciona la pregunta que se requiera graficar</label>
                                    <select class="form-select" name="selec-pregunta" id="selec-pregunta">
                                    </select>
                                    <small id="help-campo" class="form-text text-muted">Si no hay ningun elemento en este selector por favor selecciona un tipo de campo diferente y vuelve a intentar</small>

                                </div>
                                <div class="text-center mt-1">
                                    <button type="button" class="btn btn-secondary" onclick="getDatosGrafico()">Buscar estadistica</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                            ESPECIFICOS
                        </button>
                    </h2>
                    <div id="flush-collapseFour" class="" aria-labelledby="flush-headingFour" data-bs-parent="#menu_estadisticcas">
                        <div class="accordion-body">
                            <button class="list-group-item list-group-item-action border-0 " type="button" onclick="getPromedio()">Promedio</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2" hidden id="controles">
        <input type="hidden" name="bandera" id="bandera_busqueda">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="select-carrera" class="form-label">Seleccione carrer</label>
                        <select class="form-select" name="select-carrera" id="select-carrera">
                            <option value="1">Medicina</option>
                            <option value="2">Enfermeria</option>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="select-sexo" class="form-label">Seleccione el sexo</label>
                        <select class="form-select" name="select-sexo" id="select-sexo">
                            <option value="H">Hombres</option>
                            <option value="M">Mujeres</option>
                            <option value="X">Todos</option>
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" onclick="ConsultaDatos()">Buscar</button>
                </div>
            </div>

        </div>
    </div>
   
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2">
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="container"></div>
                </figure>
            </div>
            <div class="card-body " id="grafica-2" hidden>
                <figure class="highcharts-figure">
                    <div id="container_2"></div>
                </figure>
            </div>
        <table id="datatable">
<!--             <thead>
            <tr>
                <th>Categoría</th>
                <th>Hombres LEO</th>
                <th>Mujeres LEO</th>
                <th>Hombres MED</th>
                <th>Mujeres MED</th>
            </tr>
            </thead>
            <tbody id="mytable">
            </tbody> -->
        </table>
        <table id="datatable1">
<!--             <thead>
            <tr>
                <th>MEDIA</th>
                <th>MEDIANA</th>
                <th>MODA</th>
            </tr>
            </thead>
            <tbody id="estadistica_promedio">
            </tbody> -->
        </table>
        </div>

</div>
<script>
    var getPreguntas = async () => {
        
        document.getElementById('grafica-2').hidden = true;
        let tipo_campo = document.getElementById('select-tipocampo').value;
        const formData = new FormData();
        formData.append('tipo_campo', tipo_campo);
        document.getElementById('selec-pregunta').innerHTML = '';
        console.log(tipo_campo);
        let response = await fetch('<?php echo base_url("EstadisticasAdmision/getCamposCorrespondientes") ?>' + '/' + tipo_campo, {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return false;
        });
        if (response) {
            document.getElementById('selec-pregunta').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'Seleccione una opcion';
            document.getElementById('selec-pregunta').appendChild(option);
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].campo;
                option.innerHTML = response[i].nombre;
                document.getElementById('selec-pregunta').appendChild(option);
            }
        } else {
            document.getElementById('selec-pregunta').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'No hay opciones disponibles';
            document.getElementById('selec-pregunta').appendChild(option);
        }
    }

    let getDatosGrafico = async () => {
        document.getElementById('grafica-2').hidden = true;
        let campo = document.getElementById('selec-pregunta').value;
        if (campo != '') {
            const formData = new FormData();
            formData.append('id_campo', campo);
            let response = await fetch('<?php echo base_url("EstadisticasAdmision/getArrayEstadisticos") ?>' + '/' + campo, {
                method: 'POST',
                body: formData,
            }).then(response => response.json()).then(result => {
                console.log(result);
        document.getElementById('grafica-2').hidden = true;
                graficador_sub(result);
                return result; 
            }).catch(error => {
                return false;
            });
            var table = document.getElementById('datatable');
            table.innerHTML = "";
            var tablaprom = document.getElementById('datatable1');
            tablaprom.innerHTML = "";
            let thead = document.createElement('thead');
            let trcabecera = document.createElement('tr');
            tablaprom.appendChild(thead);
            thead.appendChild(trcabecera);
            let th1 = document.createElement('th');
            let th2 = document.createElement('th');
            let th3 = document.createElement('th');
            let th4 = document.createElement('th');
            let th5 = document.createElement('th');
            th1.innerHTML = "Categoría";
            th2.innerHTML = "Hombres LEO";
            th3.innerHTML = "Mujeres LEO";
            th4.innerHTML = "Hombres MED";
            th5.innerHTML = "Mujeres MED";
            trcabecera.appendChild(th1);
            trcabecera.appendChild(th2);
            trcabecera.appendChild(th3);
            trcabecera.appendChild(th4);
            trcabecera.appendChild(th5);
            let tbody = document.createElement('tbody');
            tablaprom.appendChild(tbody);
            //var tabla = document.getElementById('mytable');
            //tabla.innerHTML = "";

            for(let x = 0;x < response.cabecera.length;x++){
                let tr = document.createElement('tr');
                tr.innerHTML = response.cabecera[x];
                console.log(response.cabecera[x]);
                tbody.appendChild(tr);
                
                var td = document.createElement('td');
                td.innerHTML = response.hombres_leo[x];
                tr.appendChild(td);
                
                var td = document.createElement('td');
                td.innerHTML = response.mujeres_leo[x];
                tr.appendChild(td);
                
                var td = document.createElement('td');
                td.innerHTML = response.hombres_med[x];
                tr.appendChild(td);
                
                var td = document.createElement('td');
                td.innerHTML = response.mujeres_med[x];
                tr.appendChild(td);
            }
        } else {
            document.getElementById('container').innerHTML = '';
            alert("No se econtraron resultados de esta busqueda");
        }
    }

    function getPromedio() {
        grafircarPromedio();
        document.getElementById('grafica-2').hidden = false;
         grafircarPromedioTendencia();
    }
    let grafircarPromedio = async () => {
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/getPromedio") ?>' + '/CALIFICACION', {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
            graficador_sub(result);
            return result;
        }).catch(error => {
            return false;
        });
    }
    let grafircarPromedioTendencia = async () => {
        let arr_promedio2 = await fetch('<?php echo base_url("EstadisticasAdmision/algo") ?>' + '/CALIFICACION', {
            method: 'POST',
        }).then(arr_promedio2 => arr_promedio2.json()).then(result => {
            graficador_promedio(result);
            return result;
        }).catch(error => {
            return false;
        });
        console.log(arr_promedio2);
        
        var tabla = document.getElementById('datatable');
            tabla.innerHTML = "";
        var tablaprom = document.getElementById('datatable1');
            tablaprom.innerHTML = "";
            let thead = document.createElement('thead');
            let trcabecera = document.createElement('tr');
            tablaprom.appendChild(thead);
            thead.appendChild(trcabecera);
            let th1 = document.createElement('th');
            let th2 = document.createElement('th');
            let th3 = document.createElement('th');
            th1.innerHTML = "MEDIA";
            th2.innerHTML = "MEDIANA";
            th3.innerHTML = "MODA";
            trcabecera.appendChild(th1);
            trcabecera.appendChild(th2);
            trcabecera.appendChild(th3);
            let tbody = document.createElement('tbody');
            tablaprom.appendChild(tbody);
        let conteocalificaciones = 0; 
        let conteounitario = 0;
        let modallave = arr_promedio2.moda.llave;
        let modanum = arr_promedio2.moda.valor;
        for(let x = 0;x < arr_promedio2.conteos.length-1;x++){
                conteounitario = conteounitario + (arr_promedio2.category[x]*arr_promedio2.conteos[x]);
                conteocalificaciones = conteocalificaciones+arr_promedio2.conteos[x];
        }
        conteounitario = conteounitario + arr_promedio2.otros*6.9;
        var td1 = document.createElement('td');
        let media = parseFloat(conteounitario/conteocalificaciones);
        td1.innerHTML = media.toFixed(2);
        tbody.appendChild(td1);
        let mediana = arr_promedio2.mediana;
        var td2 = document.createElement('td');
        td2.innerHTML = mediana;
        tbody.appendChild(td2);
        var td3 = document.createElement('td');
        td3.innerHTML = "Promedio: "+ modallave+"  ("+modanum  +")" ;
        tbody.appendChild(td3);
    }

    function graficador_sub(params) {
        //console.log(params);
        let variable = params.hombres_leo;
        Highcharts.chart('container', {

            chart: {
                type: 'column'
            },

            title: {
                text: 'ASPIRANTES CON ESTATUS DE '+params.titulo.toUpperCase(),
            },

            xAxis: {
                categories: params.cabecera
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: ''
                }
            },

            tooltip: {
                formatter: function() {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal;
                }
            },

            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Hombres LEO',
                data: params.hombres_leo,
                color: '#051F03',

                stack: 'Enfermería y Obstetricia'
            }, {
                name: 'Mujeres LEO',
                data: params.mujeres_leo,
                color: '#B37D0B',
                stack: 'Enfermería y Obstetricia'
            }, {
                name: 'Hombres MED',
                data: params.hombres_med,
                color: '#5C5151',
                stack: 'Médico Cirujano'
            }, {
                name: 'Mujeres MED',
                data: params.mujeres_med,
                color: '#630505',
                stack: 'Médico Cirujano'
            }]
        });

    }

    function graficador(promedio) {
        let suma = 0;
        for (let index = 0; index < promedio.valores.length; index++) {
            suma += promedio.valores[index];
        }
        const chart = Highcharts.chart('container', {
            chart: {
                inverted: false,
                polar: false
            },
            title: {
                text: 'USEP Estadisticos'
            },
            subtitle: {
                text: promedio.titulo
            },
            xAxis: {
                className: 'Campos',
                categories: promedio.cabecera,
            },
            yAxis: {
                title: {
                    text: promedio.lateral,
                }
            },
            series: [{
                name: 'Numero de Aspirantes',
                type: 'column',
                colorByPoint: true,
                data: promedio.valores,
                showInLegend: false
            }],
            credits: {
                enabled: false
            },

        });
    }

    function graficador_promedio(datos) {
        console.log(datos.mediana);
        Highcharts.chart('container_2', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Calificaciones'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: datos.category,
                reversed: true
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Calificaciones',
                data: datos.conteos,
                color: '#840f31'
            }]
        });
    }

    let graficar = async (datos) => {
        
        document.getElementById('grafica-2').hidden = true;
        nombre = datos.value;
        console.log(nombre);
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/busquedaporetapa") ?>' + '/' + nombre, {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
        document.getElementById('grafica-2').hidden = true;
            // graficador(result);
            graficador_sub(result);
            console.log(result);
            return result;
        }).catch(error => {
            // console.log('resultado malo');
            return false;
        });
    }
    let graficaotros = async (datos) => {
        if (datos.value == 'Revisión documental') {
            nombre = "revision";
        }
        if (datos.value == 'Observaciones') {
            nombre = "observacion";
        }
        if (datos.value == 'Registro validado') {
            nombre = "aceptado";
        }
        if (datos.value == 'Registro declinado') {
            nombre = "declinado";
        }
        if (datos.value == 'total') {
            nombre = "total";
        }
         console.log(nombre);
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/") ?>' + '/' + nombre, {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
        document.getElementById('grafica-2').hidden = true;
            // graficador(result);
            ///console.log(result);
            console.log(result);
            graficador_sub(result);
            return result;
        }).catch(error => {
            // console.log('resultado malo');
            return false;
        });
    }
    let graficatotal = async () => {
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/total") ?>' , {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
            //console.log(result);
            graficador_sub(result);
            return result;
        }).catch(error => {
            // console.log('resultado malo');
            return false;
        });
    }
    function control(bandera) {
        document.getElementById('grafica-2').hidden = true;
        document.getElementById('controles').hidden = false;
        document.getElementById('bandera_busqueda').value = bandera.value;
        console.log(document.getElementById('bandera_busqueda').value);
    }
    let ConsultaDatos = async () => {
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/estadisticavalidadodet") ?>', {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
            
        document.getElementById('grafica-2').hidden = true;
            // graficador(result);
            graficador_sub(result);
            // console.log(result);
            // return result;
        }).catch(error => {
            // console.log('resultado malo');
            return false;
        });
    }

    function validacion() {

    }
    let ValidacionCorreo = async () => {
        let arr_promedio = await fetch('<?php echo base_url("EstadisticasAdmision/getAspirrantesValidados") ?>', {
            method: 'POST',
        }).then(arr_promedio => arr_promedio.json()).then(result => {
            
        document.getElementById('grafica-2').hidden = true;
            // graficador(result);
            // return result;
            graficador_sub(result);
            //console.log(result);
             return result;
        }).catch(error => {
            // console.log('resultado malo');
            return false;
        });
    }
</script>
<?php echo $this->endSection() ?>