<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .td2,.td3,.td4,.td5,.td6,.td7,.td8{
        width: 250px;
    }
    .aula{
        font-size: 12px;
        
    }

</style>

<div class="table-responsive" style="padding-left: 20px; padding-right: 20px;">
<br>
    <table class="table table-centered table-nowrap mb-0 rounded" >
        <thead class="thead-light">
            <tr>
                <th>SECCION</th>
                <th>ASIGNATURA</th>
                <th class="text-center">LUNES</th>
                <TH class="text-center">MARTES</TH>
                <TH class="text-center">MIERCOLES</TH>
                <TH class="text-center">JUEVES</TH>
                <TH class="text-center">VIERNES</TH>
                <TH class="text-center">SABADO</TH>
            </tr>
        </thead>
        <tbody>
            <?php
            /* echo "<pre>";
             var_dump( $arreglo);
             echo "</pre>"; */
            $arr_horario = [];
            $banderaiteracion = 0;
            $texto = "";
            if (isset($arreglo)) {
                foreach ($arreglo as $datos) {
            ?>
                    <tr>
                        <td><?php echo $datos['seccion']  ?></td>
                        <td class="td1"><?php echo $datos['materia']  ?></td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['lunes'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['martes'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['miercoles'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['jueves'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['viernes'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                        <td class="td3 text-center"> <?php
                        foreach( $datos['sabado'] as $valores){
                            echo $valores['inicio'] . " - " . $valores['fin'] . "<br> <div class= 'aula'> " . $valores['aula']    ."</div>";  
                        }
                         ?> </td>
                    </tr>
            <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>


<?php echo $this->endSection() ?>