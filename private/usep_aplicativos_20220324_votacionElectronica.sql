INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'VOTO ELECTRONICO', 'default');

DROP TABLE IF EXISTS `votacion_procesos`;
CREATE TABLE `votacion_procesos` (
  `proceso_id` int(10) NOT NULL AUTO_INCREMENT,
  `proceso_nombre` varchar(120) DEFAULT NULL,
  `proceso_inicio` datetime DEFAULT NULL,
  `proceso_fin` datetime DEFAULT NULL,
  `proceso_terminado` int(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`proceso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `votacion_candidatos` (
  `candidatos_id` int(10) NOT NULL AUTO_INCREMENT,
  `candidatos_correo` varchar(60) NOT NULL,
  `candidatos_nombre` varchar(120) DEFAULT NULL,
  `candidatos_voto` int(5) DEFAULT NULL,
  `candidatos_ganador` int(1) DEFAULT NULL,
  `candidatos_proceso` int(10) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`candidatos_id`),
  KEY `candidatos_proceso` (`candidatos_proceso`),
  CONSTRAINT `votacion_candidatos_ibfk_1` FOREIGN KEY (`candidatos_proceso`) REFERENCES `votacion_procesos` (`proceso_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `votacion_padron`;
CREATE TABLE `votacion_padron` (
  `padron_id` int(10) NOT NULL AUTO_INCREMENT,
  `padron_correo` varchar(60) NOT NULL,
  `padron_proceso` int(10) DEFAULT NULL,
  `padron_voto_abstencion` int(1) DEFAULT NULL,
  `padron_tipovotante` int(2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`padron_id`),
  KEY `padron_proceso` (`padron_proceso`),
  CONSTRAINT `votacion_padron_ibfk_1` FOREIGN KEY (`padron_proceso`) REFERENCES `votacion_procesos` (`proceso_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8027 DEFAULT CHARSET=utf8mb4;

CREATE VIEW view_votaciones AS 
	SELECT
    `usep_aplicativos`.`votacion_procesos`.`proceso_id` AS `proceso_id`,
    `usep_aplicativos`.`votacion_procesos`.`proceso_nombre` AS `proceso_nombre`,
    `usep_aplicativos`.`votacion_procesos`.`proceso_inicio` AS `proceso_inicio`,
    `usep_aplicativos`.`votacion_procesos`.`proceso_fin` AS `proceso_fin`,
    `usep_aplicativos`.`votacion_candidatos`.`candidatos_id` AS `candidatos_id`,
    `usep_aplicativos`.`votacion_candidatos`.`candidatos_correo` AS `candidatos_correo`,
    `usep_aplicativos`.`votacion_candidatos`.`candidatos_nombre` AS `candidatos_nombre`,
    `usep_aplicativos`.`votacion_candidatos`.`candidatos_voto` AS `candidatos_voto`,
    `usep_aplicativos`.`votacion_candidatos`.`candidatos_ganador` AS `candidatos_ganador`
FROM
    (
        `usep_aplicativos`.`votacion_procesos`
    JOIN `usep_aplicativos`.`votacion_candidatos` ON
        (
            `usep_aplicativos`.`votacion_procesos`.`proceso_id` = `usep_aplicativos`.`votacion_candidatos`.`candidatos_proceso`
        )
    )