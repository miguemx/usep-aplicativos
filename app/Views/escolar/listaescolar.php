<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>
<div class="container-fluid">
    <form class="row g-3" method="post">

        <!-- <div class="col-12">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div> -->

    </form>
</div>
<div class="container-fluid" style="margin-top: 15px;">

    <?php if (isset($error)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <form method="Post" action="<?php echo base_url('Escolar/listaescolar') ?>">

        <div class="row" style="padding-top: 10px;" <?php if (isset($bandera)) {
                                                        echo "hidden";
                                                    } ?>>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-2 col-form-label">Id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="id" value="<?php echo $id; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Sección</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="seccion" value="<?php echo $seccion; ?>" />
                </div>
            </div>
            <div class="mb-3 col-sm-4" style="text-align: right;">
                <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
                <?php if ($rol_bandera) : ?>
                    <button class="btn btn-secondary btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#descargar_lista">Descargar Listas</button>
                <?php endif; ?>
            </div>
            <input type="hidden" name="rol" value="<?php echo $rol ?>" hidden>

        </div>
        <div class="row" <?php if (isset($bandera)) {
                                echo "hidden";
                            } ?>>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Id docente</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="iddocente" value="<?php echo $iddocente; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Periodo</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="periodo" value="<?php echo $periodo; ?>" />
                </div>
            </div>
        </div>

    </form>
</div>

<div class="table-responsive">
    <div>
        <table class="table table-striped table-bordered">
            <thead class="  " style="background-color: #84112c  ; color:white; border: 1px solid black">
                <tr>
                    <th style="width: 80px; ">No.</th>
                    <th style="width: 120px; ">IDC</th>
                    <th style="width: 120px; ">PERIODO</th>
                    <th style="width: 120px; ">SECCIÓN</th>
                    <th style="width: 300px; ">MATERIA</th>
                    <th style="width: 300px; ">NOMBRE DOCENTE</th>
                    <th style="width: 100px; ">VER LISTA</th>
                    <th style="width: 100px; ">DESCARGAR</th>
                </tr>
            </thead>
            <tbody>

                <?php $contador = 0; ?>
                <?php foreach ($alumnos as $alumno) : ?>
                    <tr>
                        <td><?php echo $contador = $contador + 1; ?></td>
                        <td><?php echo $alumno->idc_grupo ?></td>
                        <td><?php echo $alumno->periodo_grupo ?></td>
                        <td><?php echo $alumno->clave_grupo ?></td>
                        <td><?php echo $alumno->nombre_materia ?></td>
                        <td><?php echo $alumno->nombre_docente . " " . $alumno->app_docente . " " . $alumno->apm_docente ?></td>
                        <td>
                            <form action="<?php echo base_url('escolar/consultatablalista'); ?>" method="post">
                                <input name="matricula" type="hidden" value="<?php echo $alumno->idc_grupo; ?>" />
                                <button type="submit" name="kx" value="1" class="btn btn-secondary btn-sm">
                                    Consultar
                                </button>
                            </form>
                        </td>
                        <td style="max-width: 230px;">
                            <div class="d-flex justify-content-between">
                                <div class="col">
                                    <form action="<?php echo base_url('escolar/generalistapdf'); ?>" method="post">
                                        <input name="matricula" type="hidden" value="<?php echo $alumno->idc_grupo; ?>" />
                                        <button type="submit" name="kx" value="1" class="btn btn-secondary btn-sm">
                                            Descargar PDF
                                        </button>
                                    </form>
                                </div>
                                <div class="col">
                                    <a href="<?php echo base_url('escolar/consultatablalistaExcel/' . "$alumno->idc_grupo/$alumno->clave_grupo" . "_" . str_replace(" ", "_", $alumno->nombre_materia)); ?>" target="_blank" rel="noopener noreferrer" class="btn btn-secondary btn-sm">Descargar excel</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <div>
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>
</div>

<!-- Modal Body -->
<!-- if you want to close by clicking outside the modal, delete the last endpoint:data-bs-backdrop and data-bs-keyboard -->
<div class="modal fade" id="descargar_lista" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" role="dialog" aria-labelledby="modalTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitleId">Seleccion de periodo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('Escolar/ListasGrupo/') ?>" method="post" id="form-descarga">
                    <div class="mb-3">
                        <label for="periodo" class="form-label">Periodo</label>
                        <select class="form-select form-select-sm" name="periodo" id="periodo">
                            <option value="0" selected>Selecciona un periodo</option>
                            <?php foreach ($periodos_disponibles as $periodo_list) : ?>
                                <option value="<?php echo $periodo_list->id ?>"><?php echo $periodo_list->id ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="bandera_concatenar">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Concatenar nombres de alumnos?</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="descargar()">Descargar</button>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function confirmacion() {
        var bandera = document.getElementById('flexSwitchCheckDefault').checked;
        var mensaje = 'A continuacion se descargará el paquete de listas del periodo seleccionado con los nombres de alumnos dividido por apellidos, ¿Desea continuar?' ;
        if(bandera == true){
             mensaje = 'A continuacion se descargará el paquete de listas del periodo seleccionado con los nombres de alumnos en una sola columna, ¿Desea continuar?' ;
        }
        Swal.fire({
            'title': 'Confirmación',
            'html': mensaje,
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',

        }).then((result) => {
            if (result.value) {
                Swal.fire({

                    'title': 'Descarga en proceso',
                    'html': '¡Atención! Esta descarga puede demorar un poco, <strong> no cierres la ventana </strong> y espera a que finalice el proceso.',
                    'icon': 'success',
                    confirmButtonColor: '#840f31'

                });
                document.getElementById('form-descarga').submit();

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });
    }

    function descargar() {
        let periodo = document.getElementById('periodo').value;
        
        if (periodo != '0') {
            confirmacion();
        }
    }
</script>
<?php echo $this->endSection() ?>