INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'SUBDIRECCION EVALUACION', 'evaluacion');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '4', 'evaluaciones', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '1', 'estadisticasevaluaciondocente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '1', 'evaluaciondocentecontroller', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'estadisticasevaluaciondocente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'evaluaciondocentecontroller', '*');



CREATE TABLE `aplicativo_evaluacion` (
  `evaluacion_id` int(11) UNSIGNED NOT NULL,
  `evaluacion_titulo` varchar(100) NOT NULL,
  `evaluacion_tipo` int(10) UNSIGNED NOT NULL,
  `evaluacion_periodo` varchar(7) NOT NULL,
  `evaluacion_rol` int(10) UNSIGNED NOT NULL,
  `evaluacion_estatus` enum('ACTIVO','CERRADO') NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aplicativo_evaluacion`
--

INSERT INTO `aplicativo_evaluacion` (`evaluacion_id`, `evaluacion_titulo`, `evaluacion_tipo`, `evaluacion_periodo`, `evaluacion_rol`, `evaluacion_estatus`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Evaluación docente', 1, '2022A', 4, 'ACTIVO', '2022-09-14', '2022-09-14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aplicativo_tipo_evaluacion`
--

CREATE TABLE `aplicativo_tipo_evaluacion` (
  `tipo_id` int(10) UNSIGNED NOT NULL,
  `tipo_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aplicativo_tipo_evaluacion`
--

INSERT INTO `aplicativo_tipo_evaluacion` (`tipo_id`, `tipo_nombre`) VALUES
(1, 'EVALUACION DOCENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion_formulario`
--

CREATE TABLE `evaluacion_formulario` (
  `formulario_id` int(10) UNSIGNED NOT NULL,
  `formulario_nombre` varchar(100) NOT NULL,
  `formulario_evaluacion` int(10) UNSIGNED NOT NULL,
  `formulario_orden` int(10) UNSIGNED NOT NULL,
  `formulario_instrucciones` text DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `evaluacion_formulario`
--

INSERT INTO `evaluacion_formulario` (`formulario_id`, `formulario_nombre`, `formulario_evaluacion`, `formulario_orden`, `formulario_instrucciones`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'EVALUACION DOCENTE', 1, 1, NULL, '2022-09-14', '2022-09-14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion_preguntas`
--

CREATE TABLE `evaluacion_preguntas` (
  `preguntas_id` int(10) UNSIGNED NOT NULL,
  `preguntas_formulario` int(10) UNSIGNED NOT NULL,
  `preguntas_orden` int(10) UNSIGNED NOT NULL,
  `preguntas_titulo` text NOT NULL,
  `preguntas_tipo` int(10) UNSIGNED NOT NULL,
  `preguntas_clasificacion` int(10) UNSIGNED NOT NULL,
  `preguntas_obligatorio` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `evaluacion_preguntas`
--

INSERT INTO `evaluacion_preguntas` (`preguntas_id`, `preguntas_formulario`, `preguntas_orden`, `preguntas_titulo`, `preguntas_tipo`, `preguntas_clasificacion`, `preguntas_obligatorio`, `created_at`, `updated_at`, `deleted_at`) VALUES

(1, 1, 1, '1. En cuanto a la METODOLOGÍA y las actividades de aprendizaje, la persona docente me brindó explicaciones claras y precisas; con medios y técnicas innovadoras o herramientas tecnológicas, que facilitaron y apoyaron mi aprendizaje, de manera', 4, 4, 1, '2022-09-14', '2022-09-14', NULL),
(2, 1, 2, '2. La persona docente me apoyó en construir HERRAMIENTAS CONCEPTUALES para: resolver casos clínicos, proyectos o problemas reales, prácticas en laboratorios o talleres (en caso de que aplique), visitar (o acercarme a las actividades de) empresas, hospitales, laboratorios u organizaciones, o interactuar con personas que trabajan aplicando los temas de la clase, de manera', 4, 4, 1, '2022-09-14', '2022-09-14', NULL),
(3, 1, 3, '3. La interacción con la persona docente en la ASESORÍA RECIBIDA durante el proceso de aprendizaje, me apoyó para resolver dudas o mostró disponibilidad en medios y horarios previamente acordados o bien: existió un ambiente de aprendizaje respetuoso y abierto, de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(4, 1, 4, '4. El SISTEMA DE EVALUACIÓN de la persona docente consideró un conjunto de herramientas que me dieron retroalimentación sobre mis fortalezas y debilidades en el curso con base en las reglas y criterios establecidos oportunamente, de manera', 4, 2, 1, '2022-09-14', '2022-09-14', NULL),
(5, 1, 5, '5. En cuanto al nivel de reto intelectual, la persona docente me MOTIVÓ O EXIGIÓ a dar el mayor esfuerzo y cumplir con calidad en las actividades académicas para beneficio de mi aprendizaje y mi crecimiento personal, de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(6, 1, 6, '6. En cuanto al ROL como guía en el aprendizaje, la persona docente me inspiró o demostró compromiso con mi aprendizaje, desarrollo y crecimiento integral, de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(7, 1, 7, '7. En cuanto al DOMINIO de la materia, la persona docente mostró conocimientos o experiencia, de manera', 4, 4, 1, '2022-09-14', '2022-09-14', NULL),
(8, 1, 8, '8. La FACILIDAD o ACCESO a los materiales, actividades y evaluaciones del curso en la plataforma fueron', 4, 1, 1, '2022-09-14', '2022-09-14', NULL),
(9, 1, 9, '9. La UTILIDAD de los contenidos didácticos para mi aprendizaje con base en la información de la plataforma del curso fue', 4, 1, 1, '2022-09-14', '2022-09-14', NULL),
(10, 1, 10, '10. La CALIDAD DE LOS MATERIALES seleccionados por la persona docente para promover mi aprendizaje fueron', 4, 1, 1, '2022-09-14', '2022-09-14', NULL),
(11, 1, 11, '11. La RELEVANCIA de los conocimientos adquiridos para mi aprendizaje fueron', 4, 2, 1, '2022-09-14', '2022-09-14', NULL),
(12, 1, 12, '12. El nivel de SATISFACCIÓN del proceso de enseñanza aprendizaje obtenido a través de la orientación de la persona docente fue', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(13, 1, 13, '13. La INFORMACIÓN que presentó la persona docente en las sesiones sincrónicas contribuyó en mi aprendizaje de manera', 4, 4, 1, '2022-09-14', '2022-09-14', NULL),
(14, 1, 14, '14. La persona docente promovió mi PARTICIPACIÓN ACTIVA durante las sesiones sincrónicas de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(15, 1, 15, '15. Las INTERACCIONES que promovió la persona docente con mis compañeras y compañeros me ayudaron a fortalecer mis CONOCIMIENTOS Y HABILIDADES de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(16, 1, 16, '16. Las INTERACCIONES que promovió la persona docente con mis compañeras y compañeros ayudaron a mejorar mi habilidad de COMPAÑERISMO Y RESPETO de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(17, 1, 17, '17. Las INTERACCIONES que promovió la persona docente con mis compañeras y compañeros ayudaron a mejorar mi habilidad de TRABAJO EN EQUIPO de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(18, 1, 18, '18. Las ACTIVIDADES COLABORATIVAS que promovió la persona docente me brindaron oportunidades para aplicar mis conocimientos y habilidades de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(19, 1, 19, '19. Soy CAPAZ de demostrar el conocimiento, habilidades, actitudes y valores adquiridos en el curso de manera', 4, 2, 1, '2022-09-14', '2022-09-14', NULL),
(20, 1, 20, '20. La PARTICIPACIÓN en los proyectos, solución de problemas, casos clínicos, estudios de caso, etc., que promovió la persona docente me preparan para la vida profesional de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(21, 1, 21, '21. El SEGUIMIENTO que di al proceso de mi propio aprendizaje contribuyó en mi rendimiento académico de manera', 4, 3, 1, '2022-09-14', '2022-09-14', NULL),
(22, 1, 22, '22. Considero que mi experiencia con el docente en la asignatura impactó en mi formación, de manera: (pregunta abierta).', 1, 3, 1, '2022-09-14', '2022-09-14', NULL),
(23, 1, 23, '23. ¿Recomendarías a la/el docente?, SI/NO ¿Por qué? (coloca una respuesta amplia)', 1, 3, 1, '2022-09-14', '2022-09-14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion_respuestas`
--

CREATE TABLE `evaluacion_respuestas` (
  `respuestas_id` int(11) NOT NULL,
  `respuestas_matricula` int(9) NOT NULL,
  `respuestas_grupo` int(10) UNSIGNED NOT NULL,
  `respuestas_pregunta` int(10) UNSIGNED NOT NULL,
  `respuestas_valor` varchar(250) NOT NULL,
  `respuestas_comentario` text DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion_tipo_pregunta`
--

CREATE TABLE `evaluacion_tipo_pregunta` (
  `tipo_id` int(10) UNSIGNED NOT NULL,
  `tipo_nombre` varchar(50) NOT NULL,
  `tipo_valores` text NOT NULL,
  `tipo_clasificacion` enum('BASIC','COLECCION','ARCHIVO','CATALOGO','CALIFICACION') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `evaluacion_tipo_pregunta`
--

INSERT INTO `evaluacion_tipo_pregunta` (`tipo_id`, `tipo_nombre`, `tipo_valores`, `tipo_clasificacion`) VALUES
(1, 'TEXTO', '/^[\\w\\sáéíóúñüÁÉÍÓÚÑÜ,\\.]+$/', 'BASIC'),
(2, 'NUMERO', '/^[0-9]+$/', 'BASIC'),
(3, 'FECHA', '/^[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}$/', 'BASIC'),
(4, 'RANGO', 'NULO,DEFICIENTE,REGULAR,BUENO,EXCELENTE', 'COLECCION');


CREATE TABLE `preguntas_clasificacion` (
  `clasificacion_id` int(10) UNSIGNED NOT NULL,
  `clasificacion_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_clasificacion`
--

INSERT INTO `preguntas_clasificacion` (`clasificacion_id`, `clasificacion_nombre`) VALUES
(1, 'CALIDAD RECURSOS'),
(2, 'PERTENENCIA ACADÉMICA'),
(3, 'RELACIÓN ACADÉMICA'),
(4, 'TRANSMISIÓN CONOCIMIENTOS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `preguntas_clasificacion`
--
ALTER TABLE `preguntas_clasificacion`
  ADD PRIMARY KEY (`clasificacion_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `preguntas_clasificacion`
--
ALTER TABLE `preguntas_clasificacion`
  MODIFY `clasificacion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aplicativo_evaluacion`
--
ALTER TABLE `aplicativo_evaluacion`
  ADD PRIMARY KEY (`evaluacion_id`),
  ADD KEY `evaluacion_periodo` (`evaluacion_periodo`),
  ADD KEY `evaluacion_tipo` (`evaluacion_tipo`),
  ADD KEY `evaluacion_rol` (`evaluacion_rol`);

--
-- Indices de la tabla `aplicativo_tipo_evaluacion`
--
ALTER TABLE `aplicativo_tipo_evaluacion`
  ADD PRIMARY KEY (`tipo_id`);

--
-- Indices de la tabla `evaluacion_formulario`
--
ALTER TABLE `evaluacion_formulario`
  ADD PRIMARY KEY (`formulario_id`),
  ADD KEY `formulario_evaluacion` (`formulario_evaluacion`);

--
-- Indices de la tabla `evaluacion_preguntas`
--
ALTER TABLE `evaluacion_preguntas`
  ADD PRIMARY KEY (`preguntas_id`),
  ADD KEY `preguntas_formulario` (`preguntas_formulario`),
  ADD KEY `preguntas_tipo` (`preguntas_tipo`),
  ADD KEY `preguntas_clasificacion` (`preguntas_clasificacion`);

--
-- Indices de la tabla `evaluacion_respuestas`
--
ALTER TABLE `evaluacion_respuestas`
  ADD PRIMARY KEY (`respuestas_id`),
  ADD KEY `respuestas_grupo` (`respuestas_grupo`),
  ADD KEY `respuestas_pregunta` (`respuestas_pregunta`);

--
-- Indices de la tabla `evaluacion_tipo_pregunta`
--
ALTER TABLE `evaluacion_tipo_pregunta`
  ADD PRIMARY KEY (`tipo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aplicativo_evaluacion`
--
ALTER TABLE `aplicativo_evaluacion`
  MODIFY `evaluacion_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `aplicativo_tipo_evaluacion`
--
ALTER TABLE `aplicativo_tipo_evaluacion`
  MODIFY `tipo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `evaluacion_formulario`
--
ALTER TABLE `evaluacion_formulario`
  MODIFY `formulario_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `evaluacion_preguntas`
--
ALTER TABLE `evaluacion_preguntas`
  MODIFY `preguntas_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `evaluacion_respuestas`
--
ALTER TABLE `evaluacion_respuestas`
  MODIFY `respuestas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2741;

--
-- AUTO_INCREMENT de la tabla `evaluacion_tipo_pregunta`
--
ALTER TABLE `evaluacion_tipo_pregunta`
  MODIFY `tipo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aplicativo_evaluacion`
--
ALTER TABLE `aplicativo_evaluacion`
  ADD CONSTRAINT `aplicativo_evaluacion_ibfk_1` FOREIGN KEY (`evaluacion_periodo`) REFERENCES `aca_periodos` (`periodo_id`),
  ADD CONSTRAINT `aplicativo_evaluacion_ibfk_2` FOREIGN KEY (`evaluacion_tipo`) REFERENCES `aplicativo_tipo_evaluacion` (`tipo_id`),
  ADD CONSTRAINT `aplicativo_evaluacion_ibfk_3` FOREIGN KEY (`evaluacion_rol`) REFERENCES `aplicativo_roles` (`rol_id`);

--
-- Filtros para la tabla `evaluacion_formulario`
--
ALTER TABLE `evaluacion_formulario`
  ADD CONSTRAINT `evaluacion_formulario_ibfk_1` FOREIGN KEY (`formulario_evaluacion`) REFERENCES `aplicativo_evaluacion` (`evaluacion_id`);

--
-- Filtros para la tabla `evaluacion_preguntas`
--
ALTER TABLE `evaluacion_preguntas`
  ADD CONSTRAINT `evaluacion_preguntas_ibfk_1` FOREIGN KEY (`preguntas_formulario`) REFERENCES `evaluacion_formulario` (`formulario_id`),
  ADD CONSTRAINT `evaluacion_preguntas_ibfk_2` FOREIGN KEY (`preguntas_tipo`) REFERENCES `evaluacion_tipo_pregunta` (`tipo_id`) ,
  ADD CONSTRAINT `evaluacion_preguntas_ibfk_3` FOREIGN KEY (`preguntas_clasificacion`) REFERENCES `preguntas_clasificacion` (`clasificacion_id`);

--
-- Filtros para la tabla `evaluacion_respuestas`
--
ALTER TABLE `evaluacion_respuestas`
  ADD CONSTRAINT `evaluacion_respuestas_ibfk_1` FOREIGN KEY (`respuestas_grupo`) REFERENCES `aca_grupos` (`grupo_id`),
  ADD CONSTRAINT `evaluacion_respuestas_ibfk_2` FOREIGN KEY (`respuestas_pregunta`) REFERENCES `evaluacion_preguntas` (`preguntas_id`);
