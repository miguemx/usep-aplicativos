<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <h2>Reportes</h2>
    <form method="get" action="<?php echo base_url('Biblioteca/ReporteAccesos'); ?>">
        <div class="card">
            <div class="card-body row">
                <div class="col-sm-6">
                    <div class="input-group">
                        <div class="input-group-text">Fecha incial</div>
                        <input type="date" name="inicio" class="form-control" value="<?php echo $inicio; ?>" id="inicio" />
                    </div>
                    
                </div>
                <div class="col-sm-6 text-right">    
                    <div class="input-group">
                        <div class="input-group-text">Fecha final</div>
                        <input type="date" name="fin" class="form-control" value="<?php echo $fin; ?>" id="fin" />
                    </div>
                </div>
                <div class="col-sm-12 text-right mt-2">    
                    <a href="<?php echo base_url('Biblioteca/ReporteAccesos'); ?>" class="btn btn-secondary btn-sm">Ver todos</a>
                    <button type="button" class="btn btn-secondary btn-sm" onclick="descarga()">
                        Descargar
                    </button>
                    <button type="submit" class="btn btn-secondary btn-sm">
                        Filtrar
                    </button>
                </div>
            </div>
        </div>
    </form>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Hora de entrada</th>    
                    <th>ID o matrícula</th>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Carrera</th>
                    <th>Sexo</th>
                    <th>Edad</th>
                    <th>Hora de salida</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach( $registros as $registro ): ?>
                    <tr>
                        <td><?php echo $registro->entrada; ?></td>
                        <td><?php echo ( ( is_null($registro->alumno) )? $registro->empleado: $registro->alumno  ); ?></td>
                        <td><?php echo $registro->tipo; ?></td>
                        <td>
                            <?php 
                            if( is_null($registro->alumno) ){
                                echo $registro->empleadoNombre.' '.$registro->empleadoApPaterno.' '.$registro->empleadoApMaterno;
                            }
                            else {
                                echo $registro->alumnoNombre.' '.$registro->alumnoApPaterno.' '.$registro->alumnoApMaterno;
                            }
                            ?>
                        </td>
                        <td><?php echo $registro->carrera; ?></td>
                        <td><?php echo $registro->sexo; ?></td>
                        <td><?php echo $registro->edad; ?></td>
                        <td><?php echo $registro->salida; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div id="forms" style="display: none;"></div>
</div>

<script type="text/javascript">
    function descarga() {
        let formdescarga = document.createElement("form");
        formdescarga.setAttribute('method', "post");
        formdescarga.setAttribute('action', "<?php echo base_url('Biblioteca/ReporteAccesosDescarga') ?>");
        formdescarga.setAttribute('target', "_blank");

        let inicio = document.createElement("input");
        inicio.setAttribute('type', "text");
        inicio.setAttribute('name', "inicio");
        inicio.setAttribute('value', document.getElementById("inicio").value );

        let fin = document.createElement("input");
        fin.setAttribute('type', "text");
        fin.setAttribute('name', "fin");
        fin.setAttribute('value', document.getElementById("fin").value );

        formdescarga.appendChild(inicio);
        formdescarga.appendChild(fin);

        document.getElementById("forms").appendChild(formdescarga);
        formdescarga.submit();
    }
</script>

<?php echo $this->endSection() ?>