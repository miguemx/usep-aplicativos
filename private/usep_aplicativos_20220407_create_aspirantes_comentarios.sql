CREATE TABLE `usep_aplicativos`.`aspirantes_comentarios`(
    `comentarios_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `comentarios_texto` TEXT NOT NULL,
    `comentarios_campo` INT(10) UNSIGNED NOT NULL,
    `created_at` DATE NULL,
    `updated_at` DATE NULL,
    `deleted_at` DATE NULL,
    PRIMARY KEY(`comentarios_id`)
) ENGINE = INNODB;


ALTER TABLE
    `aspirantes_comentarios` ADD FOREIGN KEY(`comentarios_campo`) REFERENCES `aspirantes_campos`(`campo_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO `aspirantes_comentarios`( `comentarios_texto`, `comentarios_campo`, `created_at`, `updated_at`) VALUES 
('El Extracto de Acta de Nacimiento no es legible (borrosa)','1',CURRENT_DATE,CURRENT_DATE),
('El Extracto de Acta de Nacimiento corresponde a otra persona','1',CURRENT_DATE,CURRENT_DATE),
('El Extracto de Acta de Nacimiento no está digitalizada de manera correcta (es una fotografía)','1',CURRENT_DATE,CURRENT_DATE),
('El Extracto de Acta de Nacimiento se encuentra cortada (escaneo incompleto)','1',CURRENT_DATE,CURRENT_DATE),
('Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria','1',CURRENT_DATE,CURRENT_DATE),

('La Clave Única de Registro de Población no es legible (borrosa).','47',CURRENT_DATE,CURRENT_DATE),
('La Clave Única de Registro de Población no se encuentra actualizada al año 2022.','47',CURRENT_DATE,CURRENT_DATE),
('La Clave Única de Registro de Población corresponde a otra persona.','47',CURRENT_DATE,CURRENT_DATE),
('La Clave Única de Registro de Población no está digitalizada de manera correcta (no es visible la fecha de emisión del documento).','47',CURRENT_DATE,CURRENT_DATE),
('La Clave Única de Registro de Población se encuentra cortada (escaneo incompleto).','47',CURRENT_DATE,CURRENT_DATE),
('Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.','47',CURRENT_DATE,CURRENT_DATE),

('El documento cuenta con promedio inferior a 8.0 (ocho punto cero).','50',CURRENT_DATE,CURRENT_DATE),
('El documento cuenta con adeudo de materias.','50',CURRENT_DATE,CURRENT_DATE),
('El documento no es legible (borroso).','50',CURRENT_DATE,CURRENT_DATE),
('El documento corresponde a otra persona.','50',CURRENT_DATE,CURRENT_DATE),
('El certificado es de estudios parciales, debe enviar Certificado de Estudios concluidos de nivel medio superior.','50',CURRENT_DATE,CURRENT_DATE),
('El documento cuenta con adeudo de materias.','50',CURRENT_DATE,CURRENT_DATE),
('El Certificado de Estudios no corresponde al nivel medio superior.','50',CURRENT_DATE,CURRENT_DATE),
('Debe adjuntar en este mismo archivo el Transcript y la traducción simple.','50',CURRENT_DATE,CURRENT_DATE),
('La Constancia de Estudios no fue expedida en el año 2022.','50',CURRENT_DATE,CURRENT_DATE),
('La Constancia de Estudios no cuenta con desglose de materias y calificaciones.','50',CURRENT_DATE,CURRENT_DATE),
('El desglose de materias debe de ser desde el primer semestre - año y hasta el quinto semestre - segundo año según corresponda al plan de estudios de la escuela de procedencia.','50',CURRENT_DATE,CURRENT_DATE),
('El documento proporcionado no está digitalizado de manera correcta (es una fotografía).','50',CURRENT_DATE,CURRENT_DATE),
('El documento se encuentra cortado (escaneo incompleto).','50',CURRENT_DATE,CURRENT_DATE),
('El escaneo del documento debe ser por ambos lados.','50',CURRENT_DATE,CURRENT_DATE),
('Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.','50',CURRENT_DATE,CURRENT_DATE),

('La identificación no es legible (borrosa).','48',CURRENT_DATE,CURRENT_DATE),
('La identificación no cuenta con sello y firma de la institución.','48',CURRENT_DATE,CURRENT_DATE),
('La identificación corresponde a otra persona.','48',CURRENT_DATE,CURRENT_DATE),
('El documento no corresponde a una identificación oficial estipulada en la convocatoria de admisión 2022.','48',CURRENT_DATE,CURRENT_DATE),
('El documento proporcionado no está digitalizado de manera correcta (es una fotografía).','48',CURRENT_DATE,CURRENT_DATE),
('El documento se encuentra cortado (escaneo incompleto).','48',CURRENT_DATE,CURRENT_DATE),
('El escaneo del documento debe ser por ambos lados.','48',CURRENT_DATE,CURRENT_DATE),
('Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.','48',CURRENT_DATE,CURRENT_DATE),

('La fotografía es borrosa.','52',CURRENT_DATE,CURRENT_DATE),
('La fotografía es una selfie.','52',CURRENT_DATE,CURRENT_DATE),
('La fotografía no debe tener filtros, textos ni fechas.','52',CURRENT_DATE,CURRENT_DATE),
('La fotografía debe abarcar del pecho hacia arriba.','52',CURRENT_DATE,CURRENT_DATE),
('La fotografía corresponde a otra persona.','52',CURRENT_DATE,CURRENT_DATE),
('El fondo de la fotografía debe ser blanco y liso.','52',CURRENT_DATE,CURRENT_DATE),
('El rostro se debe encontrar descubierto (no lentes, no fleco, de frente y preferentemente mirando a la cámara)','52',CURRENT_DATE,CURRENT_DATE),
('La dimensión de la fotografía no es la solicitada.','52',CURRENT_DATE,CURRENT_DATE),
('La fotografía se encuentra cortada.','52',CURRENT_DATE,CURRENT_DATE),
('Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.','52',CURRENT_DATE,CURRENT_DATE);