<?php
namespace App\Models;
use CodeIgniter\Model;

class UsuariosModel extends Model {
    protected $table      = 'aplicativo_usuarios';
    protected $primaryKey = 'usuario_correo';

    protected $returnType    = 'App\Entities\Usuario';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'usuario_correo','usuario_rol','usuario_nombre','usuario_ultcnx','deleted_at',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene todos los datos de sesion para un usuario
     * @param correo el correo electronico a buscar
     * @return datos un arreglo con los datos encontrados en las llaves, con el nombre de las propiedades de las entidades
     * @return null devuelve valor nulo cuando no encuentra al usuario
     */
    public function getSessionData($correo) {
        $usuario = $this->find( $correo );
        if ( !is_null($usuario) ) {
            $rolesModel = new RolesModel();
            //$empleadosm = new EmpleadosModel();
            $rol = $rolesModel->find( $usuario->rol );
            //$rol = $rolesModel->find( $usuario->rol );
            if ( !is_null($rol) ) {
                $generales = $this->getUSerData( $correo );
                return [
                    'id' => $generales['id'],
                    'nombre' => $generales['nombre'],
                    'id_rol' => $rol->rol_id,
                    'rol_id' => $rol->rol_id,
                    'rol' => $rol->nombre,
                    'menu' => $rol->menu,
                    'correo' => $correo
                ];
            }
        }
        return null;
    }

    /**
     * obtiene los datos generales de un usuario; toma como base el correo
     * ya que esta es la llave primaria de la tabla de usuarios
     * @param correo el correo del usuario a buscar
     * @return generales un array sin datos
     * @return generales arreglo con los datos del usuario
     */
    public function getUserData ( $correo )  {
        $generales = [ 'id' => '0', 'nombre' => '', 'correo'=>'' ];
        $alumnosModel = new AlumnosModel();
        $empleadosModel = new EmpleadosModel();
        $alumno = $alumnosModel->findByEmail( $correo );
        if ( !is_null($alumno) ) {
            $generales = [ 'id'=>$alumno->id, 'nombre'=>$alumno->nombres.' '.$alumno->apPaterno.' '.$alumno->apMaterno, 'correo'=>$correo ];
        }
        else {
            $empleado = $empleadosModel->findByEmail( $correo );
            if ( !is_null($empleado) ) {
                $generales = [ 'id'=>$empleado->id, 'nombre'=>$empleado->nombre, 'correo'=>$correo ];
            }
        }
        return $generales;
    }

    /**
     * encuentra el usuario correspondiente al @param correo
     */
	public function findByEmail($correo) {
        $usuario = $this->where('usuario_correo',$correo)->first();
        return $usuario;
    }

    /**
     * encuentra el usuario correspondiente al correo y devuelve todos los datos de la tabla
     * @param correo
     */
	public function findByEmail2($correo) {
        $usuario = $this->where('usuario_correo',$correo)->find();
        return $usuario;
    }
    public function consultacorreo($correo){
        $consulta =  $this->where('usuario_correo',$correo)->find();
        return $consulta;
    }
}