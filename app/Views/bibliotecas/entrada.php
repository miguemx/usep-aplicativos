<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <div class="text-center mt-5">
        <h2>Bibliotecas - Registro de acceso</h2>
        <form class="row gy-2 gx-3 align-items-center">
            <div class="col-sm-3">
                <label class="visually-hidden" for="autoSizingInputGroup">ID o matrícula</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-text">ID o Matrícula</div>
                    <input type="text" class="form-control textfiltro" id="id" />
                </div>
            </div>
            <div class="col-sm-3">
                <label class="visually-hidden" for="autoSizingInputGroup">ID o matrícula</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-text">Nombre</div>
                    <input type="text" class="form-control textfiltro" id="nombre" />
                </div>
            </div>
            <div class="col-sm-3">
                <label class="visually-hidden" for="autoSizingInputGroup">ID o matrícula</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-text">Apellido Paterno</div>
                    <input type="text" class="form-control textfiltro" id="apPaterno" />
                </div>
            </div>
            <div class="col-sm-3">
                <label class="visually-hidden" for="autoSizingInputGroup">ID o matrícula</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-text">Apellido Materno</div>
                    <input type="text" class="form-control textfiltro" id="apMaterno" />
                </div>
            </div>
            <div class="col-sm-12 ">
                <div class="text-right">
                    <a href="<?php echo base_url('Biblioteca/Aforo'); ?>" class="btn btn-sm btn-danger">Regresar</a>
                    <button class="btn btn-sm btn-secondary" id="btnbuscar" type="button">
                        Buscar
                    </button>
                </div>
            </div>
        </form>

        <form class="form">
        <div class="col-sm-12 mt-5 table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Carrera</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="listuser">
                </tbody>
                <thead>
                    <tr>
                        <td colspan="5" id="wait" style="display:none;">Buscando...</td>
                    </tr>
                </thead>
            </table>
        </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const busca = async () => {
        removeChildren( "listuser" );
        document.getElementById("wait").style.display = '';
        let url = "<?php echo base_url('Api/Usuarios/Buscar') ?>";
        let statusCode = 200;
        url = appendParameters( url );
        
        const resp = await fetch(url, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'omit', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            // body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        .then( res => {
            if( res.status == 200 ) {
                return  res.json();
            }
            return { 'status':'error', 'code':res.status, 'message': res.statusText };
        })
        .catch( error => { return { 'status': 'error', 'code':500, 'message': 'Error en el servidor.', 'data': error }; } )
        .then( response => { return response; } );
        document.getElementById("wait").style.display = 'none';
        if ( resp.status == 'ok' ) {
            creaTabla( resp );
        }
        else {
            alert( "Ocurrió un error al buscar. \n\n" + resp.message );
        }
    }

    const appendParameters = ( url ) => {
        url = url + "?";
        if ( document.getElementById('id').value ) {
            url += "&id=" + document.getElementById('id').value;
        }
        if ( document.getElementById('nombre').value ) {
            url += "&nombre=" + document.getElementById('nombre').value;
        }
        if ( document.getElementById('apPaterno').value ) {
            url += "&apPaterno=" + document.getElementById('apPaterno').value;
        }
        if ( document.getElementById('apMaterno').value ) {
            url += "&apMaterno=" + document.getElementById('apMaterno').value;
        }
        return url;
    }

    /**
     * crea los renglones de la tabla
     * @param data los datos obtenidos de la response
     */
    const creaTabla = (data) => {
        removeChildren( "listuser" );
        data.results.forEach(
            function(row) {
                let tr = document.createElement("tr");
                let tdId = document.createElement("td");
                let tdNombre = document.createElement("td");
                let tdTipo = document.createElement("td");
                let tdCarrera = document.createElement("td");
                let tdBoton = document.createElement("td");

                tdId.innerText = row.id;
                tdNombre.innerText = creaNombre(row);
                tdTipo.innerText = ( row.rolid == 4 )?'ESTUDANTE':'DOCENTE / ADMINISTRATIVO';
                tdCarrera.innerText = ( isNaN( row.carrera ) )? row.carrera: ' ';
                
                tr.append(tdId);
                tr.append(tdNombre);
                tr.append(tdTipo);
                tr.append(tdCarrera);
                tr.append( creaBoton(row) );

                document.getElementById('listuser').append( tr );
            }
        );
    }

    function creaNombre(row) {
        let nombre = '';
        nombre += ( row.nombre == null )? '': row.nombre + ' ';
        nombre += ( row.apPaterno == null )? '': row.apPaterno + ' ';
        nombre += ( row.apMaterno == null )? '': row.apMaterno + ' ';
        return nombre;
    }

    function creaBoton(row) {
        let boton = document.createElement("button");
        boton.type = "button";
        boton.innerText = "Registrar";
        boton.classList.add ("btn");
        boton.classList.add ("btn-sm");
        boton.classList.add ("btn-outline-secondary");
        boton.classList.add ("m-2");
        boton.onclick = function() { registraEntrada(row) };
        return boton;
    }

    function removeChildren(id) {
        let elem = document.getElementById(id);
        let first = elem.firstElementChild;
        while (first) {
            first.remove();
            first = elem.firstElementChild;
        }
    }

    async function registraEntrada(row) {
        let url = "<?php echo base_url('Api/AccesoBiblioteca/Nuevo') ?>";
        let data = {
            id: row.id, rolid: row.rolid
        };
        
        const resp = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'omit', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        .then( res => {
            if( res.status == 201 ) {
                return  res.json();
            }
            else if ( res.status == 400 ) {
                console.log( res.status );
                return { 'status':'error', 'code':res.status, 'message': 'El código ya ha sido utilizado para acceder.' };
            }
            return { 'status':'error', 'code':res.status, 'message': res.statusText };
        })
        .catch( error => { return { 'status': 'error', 'code':500, 'message': 'Error en el servidor.', 'data': error }; } )
        .then( response => { return response; } );
        if ( resp.status == 'ok' ) {
            alert( 'Se ha registrado el acceso.' );
            document.location.href = "<?php echo base_url('Biblioteca/Aforo'); ?>";
        }
        else {
            alert( "Ocurrió un error al buscar. \n\n" + resp.message );
        }
    }

    document.getElementById('btnbuscar').onclick = busca;
    textfiltros = document.getElementsByClassName("textfiltro");
    var filtros = Array.from( textfiltros );
    filtros.forEach( function(elem){
        elem.onkeyup = function(key) {
            if(key.keyCode == 13) {
                busca();
            }
        };
    } );
    
</script>

<?php echo $this->endSection() ?>