<?php

namespace App\Controllers;

use App\Models\AlumnosModel;
use App\Models\CarrerasModel;
use App\Models\KardexModel;
use App\Models\MateriaModel;

class Calificaciones extends BaseController
{

    private $alumnosModel = null;
    private $kardexModel = null;
    private $carrerasModel = null;
    private $materiasModel;

    private $errorMsg = null;
    private $exitoMsg = null;

    public function __construct()
    {
        $this->alumnosModel = new AlumnosModel();
        $this->kardexModel = new KardexModel();
        $this->carrerasModel = new CarrerasModel();
        $this->materiasModel = new MateriaModel();;
    }

    public function kardex()
    {
        $valida = $this->request->getPost('kx');
        if ($valida === '1') {
            $alumno = $this->alumnosModel->busca($this->request->getPost('matricula'));
            if (!is_null($alumno)) {
                $this->corregir();
                $kardex = $this->kardexModel->alumno($alumno);
                $carrera = $this->carrerasModel->find($alumno->carrera);
                $data['kardex'] = $kardex;
                $data['alumno'] = $alumno;
                $data['carrera'] = $carrera;
                $data['error'] = $this->errorMsg;
                $data['exito'] = $this->exitoMsg;
                $data['menu'] = $this->sesion->get('menu');
                return view('escolar/kardex', $data);
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * ejecuta una correccion de calificacion de acuerdo a los valores que se envian en el formulario
     */
    private function corregir()
    {
        $ban = $this->request->getPost('crx');
        if ($ban == 'esc') {
            $matricula = $this->request->getPost('matricula');
            $calificacion = $this->request->getPost('calificacion');
            $grupo = $this->request->getPost('grupo');
            $tipo = $this->request->getPost('tipo');
            $nota = $this->request->getPost('nota');
            if ($calificacion > 10) {
                $this->errorMsg = 'La calificación no puede ser mayor que 10.';
                return false;
            }
            if (strlen($nota) < 6) {
                $this->errorMsg = 'Debe escribir una razón del cambio de calificación.';
                return false;
            }

            if ($this->kardexModel->actualiza($matricula, $grupo, $calificacion, $tipo, $nota)) {
                $this->exitoMsg = 'Calificación actualizada con éxito.';
            } else {
                $this->errorMsg = 'Ocurrió un problema al actualizar la calificación.';
            }
        }
    }
}
