<?php
namespace App\Models;
use CodeIgniter\Model;

class NotificacionesModel extends Model {
    protected $table      = 'aplicativo_notificaciones';
    protected $primaryKey = 'notificacion_id';

    protected $returnType    = 'App\Entities\Notificacion';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'notificacion_creador','notificacion_destinatario','notificacion_texto','notificacion_hora','notificacion_status', 
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene las ultimas notificaciones por un usuario determinado
     * @param correo el correo del usuario 
     * @param limit el numero de notificaciones a devolver (opcional por defecto 20)
     * @return notificaciones las notificaciones encontradas
     */
    public function forUser($correo, $limit=20) {
        $this->where( 'notificacion_destinatario', $correo );
        $this->orderBy( 'created_at', 'DESC' );
        $this->orderBy( 'notificacion_hora', 'DESC' );
        $notificaciones = $this->findAll( $limit );
        return $notificaciones;
    }
}