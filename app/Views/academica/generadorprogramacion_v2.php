<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .input-error {
        border: 1px red solid !important;
    }

    .barra {
        max-height: 60vh;
        overflow-y: scroll;
    }
</style>
<div class="container mt-3">
    <div class="bg-white rounded shadow p-3 mb-2 mt-1 ml-2 mr-2">
        <div class="row">
            <h4>Generador de Programacion Academica</h4>
        </div>

        <div class="row">
            <div class="col-2 mt-3">
                <label for="select_periodo" class="form-label">Periodos disponibles</label>
                <select class="form-select" name="select_periodo" id="select_periodo" >
                    <option value="0">Seleccione un periodo</option>
                    <?php foreach ($periodos as $lista_periodo) : ?>
                        <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                    <?php endforeach; ?>
                </select>
                <small id="text_periodo">Seleccione un periodo disponible de la lista </small>
            </div>
            <div class="col-1 mt-3">
                <label for="select-semestre">Semestre</label>
                <select class="form-select" name="select_semestre" id="select-semestre" type="text" aria-label="Last name">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
            <div class="col-2 mt-3">
                <label for="input-num-secciones">Cantidad de secciones</label>
                <input type="number" class="form-control" id="input-num-secciones" name="num_secciones" min="1">
                <small>Indique la cantidad de secciones que se crearan en este periodo para el semestre seleccionado</small>
            </div>
            
            <div class="col mt-3">
                <label for="select-carrera">Carrera de las Secciones</label>
                <select class="form-select" name="select_carrera" id="select-carrera" <?php echo (($rol == '10' || $rol == '9') ? 'disabled' : '') ?>>
                    <option value="1" <?php echo (($rol == '10') ? 'selected' : '') ?>>Licenciatura en Enfermería y Obstetricia</option>
                    <option value="2" <?php echo (($rol == '9') ? 'selected' : '') ?>>Licenciatura en Médico Cirujano</option>
                </select>
            </div>
            
            <div class="form-check form-switch col-2 mt-5 mx-1">
                <input class="form-check-input" type="checkbox" id="tut-check" name="tutorias" checked>
                <label class="form-check-label" for="tut-check">Incluir hora de tutorias?</label>
            </div>
        </div>
        <div class="row mt-3 text-center">
            <div class="col text-center">
                <button type="button" class="btn btn-secondary" onclick="CrearGrupos()">Vista Previa</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <form action="<?php echo base_url('ProgramacionAcademica/CreateGrupos'); ?>" method="post" id="form-grupos">
        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 " hidden id="main-data">
        </div>
    </form>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function CrearGrupos() {
        var num_secciones = document.getElementById('input-num-secciones');
        var periodo = document.getElementById('select_periodo');
        var carrera = document.getElementById('select-carrera').value;
        var semestre = document.getElementById('select-semestre').value;
        var tutoria = document.getElementById('tut-check').checked;
        let mensaje = '';
        // let bandera = true;
        // console.log(periodo);
        if (periodo.value == 0) {
            mensaje = mensaje + 'Seleccione un periodo disponible.<br>';
            periodo.classList.add('input-error');
        }  else periodo.classList.remove('input-error');

        if (!num_secciones.value || num_secciones.value <= 0) {
           mensaje = mensaje+'Asegurese de ingresar el numero de secciones que creara.<br>';
            num_secciones.classList.add('input-error');
            
        } else num_secciones.classList.remove('input-error');
        
        if(periodo.value != 0 && num_secciones.value){
            getMaterias(carrera, semestre, num_secciones.value, tutoria,periodo.value);
        }else{
            Swal.fire({
                'title': 'Atencion',
                'html': mensaje,
                'icon': 'info',
                confirmButtonColor: '#883232',

            });
        }
    }
    var getMaterias = async (carrera, semestre, num_secciones, tutoria,periodo) => {
        let contenedor = document.getElementById('main-data');
        contenedor.hidden = false;
        contenedor.innerHTML = '';
        let input_carrera = document.createElement('input');
        input_carrera.hidden = true;
        input_carrera.setAttribute('value', carrera);
        input_carrera.name = 'carrera';
        let input_semestre = document.createElement('input');
        input_semestre.hidden = true;
        input_semestre.setAttribute('value', semestre);
        input_semestre.name = 'semestre';
        let periodo_select = document.createElement('input');
        periodo_select.hidden = true;
        periodo_select.setAttribute('value', periodo);
        periodo_select.name = 'periodo';
        let input_num_secciones = document.createElement('input');
        input_num_secciones.hidden = true;
        input_num_secciones.setAttribute('value', num_secciones);
        input_num_secciones.name = 'num_secciones';
        let tutorias = document.createElement('input');
        tutorias.hidden = true;
        tutorias.setAttribute('value', (tutorias) ? 'si' : 'no');
        tutorias.name = 'tutorias';
        contenedor.appendChild(input_carrera);
        contenedor.appendChild(input_semestre);
        contenedor.appendChild(input_num_secciones);
        contenedor.appendChild(tutorias);
        contenedor.appendChild(periodo_select);
        let secuencia = 0;
        let grupos_creados = await fetch('<?php echo base_url("ProgramacionAcademica/getGruposExistentes") ?>' + '/' + carrera + '/' + semestre+ '/' + periodo, {
            method: 'POST',
        }).then(grupos_creados => grupos_creados.json()).then(result => {
            // return result;
            console.log(result);
            alert('Se han encontrado grupos creados para este periodo con el  semestre seleccionado');

            for (let index = 0; index < result.secciones.length; index++) {
                // console.log(result.secciones[index].clave);

                var main = document.createElement('div');
                main.classList.add('mt-2', 'mb-2', 'card-body');
                contenedor.appendChild(main);
                var titulo = document.createElement('h3');
                titulo.innerHTML = 'Seccion: ' + result.secciones[index].clave;
                main.appendChild(titulo);
                for (let yindex = 0; yindex < result.grupos.length; yindex++) {

                    if (result.secciones[index].clave == result.grupos[yindex].clave) {

                        // console.log('('+result.grupos[yindex].clave + ') '+result.grupos[yindex].materia+' - ' + result.grupos[yindex].materiaNombre);

                        var div_row = document.createElement('div');
                        div_row.id = (result.grupos[yindex].grupo);
                        div_row.classList.add('row', 'd-flex', 'justify-content-center');
                        main.appendChild(div_row);

                        var div_col2 = document.createElement('div');
                        div_col2.classList.add('col-4', 'mt-1');
                        div_row.appendChild(div_col2);

                        var input = document.createElement('input');
                        input.classList.add('form-control');
                        input.disabled = true;
                        input.value = result.grupos[yindex].materia + ' - ' + result.grupos[yindex].materiaNombre;
                        div_col2.appendChild(input); /* */

                        var div_col2 = document.createElement('div');
                        div_col2.classList.add('col-4', 'mt-1');
                        div_row.appendChild(div_col2);

                        var input = document.createElement('input');
                        input.classList.add('form-control');
                        input.disabled = true;
                        input.value = (result.grupos[yindex].apPaterno + ' ' + result.grupos[yindex].apMaterno + ' ' + result.grupos[yindex].nombre_emp).toUpperCase();
                        div_col2.appendChild(input);

                        var div_col3 = document.createElement('div');
                        div_col3.classList.add('col-2', 'mt-1', 'd-flex', 'justify-content-around');
                        div_row.appendChild(div_col3);

                        var botonera = document.createElement('button');
                        botonera.classList.add('btn', 'btn-secondary');
                        botonera.type = 'button';
                        botonera.value = (result.grupos[yindex].grupo + '|' + result.grupos[yindex].materia + '|' + result.grupos[yindex].materiaNombre) + '|' + result.secciones[index].clave;
                        botonera.setAttribute('onclick', 'eliminar_existente(this)');
                        botonera.innerHTML = 'Eliminar';
                        div_col3.appendChild(botonera);
                    }
                }

                secuencia = index + 1;
            }
        }).catch(error => {
            return null;
        });
        // console.log(grupos_creados);

        let response = await fetch('<?php echo base_url("ProgramacionAcademica/getMaterias") ?>' + '/' + carrera + '/' + semestre, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return null;
        });
        if (response) {

            let index = parseFloat((secuencia == 0) ? 1 : secuencia + 1);
            // console.log(num_secciones);
            // console.log(((secuencia == 0) ? 1 : secuencia));
            // console.log(parseFloat(num_secciones) + parseFloat(secuencia));

            for (index; index <= (parseFloat(num_secciones) + parseFloat(secuencia)); index++) {
                var main = document.createElement('div');
                main.classList.add('mt-2', 'mb-2', 'card-body');
                contenedor.appendChild(main);
                var titulo = document.createElement('h3');
                main.appendChild(titulo);
                let x = 0;
                for (let y = 0; y < response.length; y++) {
                    titulo.innerHTML = 'Seccion: ' + ((carrera == 1) ? ('LEO-' + semestre + ((index < 10) ? ('0' + index) : index)) : ('MED-' + semestre + ((index < 10) ? ('0' + index) : index)));
                    var div_row = document.createElement('div');
                    div_row.id = ('contenedor-' + index + '-' + y);
                    div_row.classList.add('row', 'd-flex', 'justify-content-center');
                    main.appendChild(div_row);



                    var div_col2 = document.createElement('div');
                    div_col2.classList.add('col-4', 'mt-1');
                    div_row.appendChild(div_col2);

                    var input_clave = document.createElement('input');
                    input_clave.name = 'seccion[]';
                    input_clave.value = ((carrera == 1) ? ('LEO-' + semestre + ((index < 10) ? ('0' + index) : index)) : ('MED-' + semestre + ((index < 10) ? ('0' + index) : index)));
                    input_clave.hidden = true;
                    div_col2.appendChild(input_clave);

                    var input = document.createElement('input');
                    input.classList.add('form-control');
                    input.setAttribute('readonly', 'readonly');
                    input.name = 'materia[]';
                    input.value = response[y].clave + ' ' + response[y].nombre;
                    div_col2.appendChild(input);

                    var div_col2 = document.createElement('div');
                    div_col2.classList.add('col-4', 'mt-1');
                    div_row.appendChild(div_col2);

                    var select = document.createElement('select');
                    select.name = 'select_docente[]';
                    select.classList.add('form-select');
                    div_col2.appendChild(select);

                    let contador = <?php echo count($docentes); ?>;
                    let docentes = <?php echo json_encode($docentes) ?>;
                    var option = document.createElement('option');
                    option.value = 1;
                    option.innerHTML = "Sin asignar";
                    select.appendChild(option);
                    for (let index = 0; index < contador; index++) {
                        var option = document.createElement('option');
                        option.value = docentes[index].id;
                        option.innerHTML = (docentes[index].apPaterno + " " + docentes[index].apMaterno + " " + docentes[index].nombre).toUpperCase();
                        select.appendChild(option);
                    }
                    // console.log(docentes);

                    var div_col3 = document.createElement('div');
                    div_col3.classList.add('col-2', 'mt-1', 'd-flex', 'justify-content-around');
                    div_row.appendChild(div_col3);

                    var botonera = document.createElement('button');
                    botonera.classList.add('btn', 'btn-secondary');
                    botonera.type = 'button';
                    botonera.value = ('contenedor-' + index + '-' + y);
                    botonera.setAttribute('onclick', 'eliminar(this)');
                    botonera.innerHTML = 'Eliminar';
                    div_col3.appendChild(botonera);
                    x = y + 1;
                }
                var tut = document.getElementById('tut-check').checked;
                if (tut == true) {
                    // console.log(tut);
                    var div_row = document.createElement('div');
                    div_row.id = ('contenedor-' + index + '-' + x);
                    // div_row.id = ('contenedor-' + index + '-TUT');
                    div_row.classList.add('row', 'd-flex', 'justify-content-center');
                    main.appendChild(div_row);

                    var div_col2 = document.createElement('div');
                    div_col2.classList.add('col-4', 'mt-1');
                    div_row.appendChild(div_col2);

                    var input_clave = document.createElement('input');
                    input_clave.name = 'seccion[]';
                    input_clave.value = ((carrera == 1) ? ('LEO-' + semestre + ((index < 10) ? ('0' + index) : index)) : ('MED-' + semestre + ((index < 10) ? ('0' + index) : index)));
                    input_clave.hidden = true;
                    div_col2.appendChild(input_clave);

                    var input = document.createElement('input');
                    input.classList.add('form-control');
                    input.setAttribute('readonly', 'readonly');
                    input.name = 'materia[]';
                    input.value = ((carrera == 1) ? ('LEO-TUT Hora de tutoria') : ('MC-TUT Hora de tutoria'));
                    div_col2.appendChild(input);

                    var div_col2 = document.createElement('div');
                    div_col2.classList.add('col-4', 'mt-1');
                    div_row.appendChild(div_col2);

                    var select = document.createElement('select');
                    select.name = 'select_docente[]';
                    select.classList.add('form-select');
                    div_col2.appendChild(select);

                    let contador = <?php echo count($docentes); ?>;
                    let docentes = <?php echo json_encode($docentes) ?>;
                    var option = document.createElement('option');
                    option.value = 1;
                    option.innerHTML = "Sin asignar";
                    select.appendChild(option);
                    for (let index = 0; index < contador; index++) {
                        var option = document.createElement('option');
                        option.value = docentes[index].id;
                        option.innerHTML = (docentes[index].apPaterno + " " + docentes[index].apMaterno + " " + docentes[index].nombre).toUpperCase();
                        select.appendChild(option);
                    }

                    var div_col3 = document.createElement('div');
                    div_col3.classList.add('col-2', 'mt-1', 'd-flex', 'justify-content-around');
                    div_row.appendChild(div_col3);

                    var botonera = document.createElement('button');
                    botonera.classList.add('btn', 'btn-secondary');
                    botonera.type = 'button';
                    botonera.value = ('contenedor-' + index + '-' + x);
                    botonera.setAttribute('onclick', 'eliminar(this)');
                    botonera.innerHTML = 'Eliminar';
                    div_col3.appendChild(botonera);
                }
            }
            // var tut = document.getElementById('tut-check').checked;
            //     console.log(tut);
            if (document.getElementById('main-data').clientHeight > 500) {
                document.getElementById('main-data').classList.add('barra');
            }
            let div = document.createElement('div');
            div.id = "botonera";
            div.classList.add('text-center', 'form-group', 'mt-3');
            let boton = document.createElement('button');
            boton.classList.add('btn', 'btn-secondary');
            boton.name ='periodo';
            boton.value=periodo;
            boton.id = 'btn-crea-grupos';
            boton.innerHTML = "Crear Secciones";
            boton.type = 'button';
            boton.setAttribute("onclick", 'Genera()');
            div.appendChild(boton);
            contenedor.appendChild(div);

        } else {
            alert('No hay materias disponibles para este semestre seleccionado');
        }
    }

    function eliminar(algo) {
        var confirmado = confirm('Si eliminara este grupo,¿Desesas continuar?');
        if (confirmado == true) {
            var grupo = document.getElementById(algo.value);
            grupo.hidden = true;
            grupo.innerHTML = null;
        }
    }

    function Genera() {
        document.getElementById('form-grupos').submit();

    }
    var eliminar_existente = async (item) => {
        let datos = item.value.split('|');
        Swal.fire({
            'title': 'Confirmación',
            'html': '¿Seguro que desea eliminar el grupo de <br>' + datos[2] + ' de la seccion ' + datos[3] + '?, <br> <h3>Esta operacion es irreversible </h3>',
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {


                confirmacion(datos);
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });

    }
    var confirmacion = async (datos) => {
        let response = await fetch('<?php echo base_url("ProgramacionAcademica/eliminarGrupo") ?>' + '/' + datos[0], {
            method: 'POST',
        }).then(response => response.json()).then(valores => {
            return valores;
        }).catch(error => {
            return null;
        });
        console.log(response);
        if (response) {
            if (response == 'realizado') {
                let contenedor = document.getElementById(datos[0]);
                contenedor.innerHTML = '';
                Swal.fire({
                    'title': 'Eliminado',
                    'html': 'Se realizo satisfactoriamente la eliminacion del grupo <br>' + datos[2],
                    'icon': 'info',
                    confirmButtonColor: '#840f31',
                    timer: 3000
                });
                // console.log(contenedor);

            } else if (response == 'error') {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'Se produjo un error, recarga y vuelve a intentarlo nuevamente',
                    'icon': 'info',
                    confirmButtonColor: '#840f31',
                    timer: 3000
                });
            } else if (response == 'horario') {
                Swal.fire({
                    'title': 'Cancelado',
                    'html': 'No se puede eliminar debido a que el grupo <br>' + datos[2] + ' de la seccion ' + datos[3] + ' ya tiene horario asignado',
                    'icon': 'info',
                    confirmButtonColor: '#840f31',
                    timer: 3000
                });
            } else if (response == 'periodo') {
                Swal.fire({
                    'title': 'Cancelado',
                    'html': 'No se puede eliminar debido a que el periodo actual ya no permite hacer modifcaciones',
                    'icon': 'info',
                    confirmButtonColor: '#840f31',
                    timer: 3000
                });
            }
        }
    }
</script>
<?php echo $this->endSection() ?>