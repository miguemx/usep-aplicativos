<?php
namespace App\Helpers;

use CodeIgniter\Validation\FileRules;

/**
 * realiza subidas de archivo al servidor
 */
class Uploader extends FileRules {

    private $validacion;
    private $files;
    private $fileRules;
    private $fileRules_errors;
    protected $request;

    private $errors = [];
    private $lastError = false;

    /**
     * para subir archivos es necesario obtener el arreglo de los archivos subidos al servidor
     * @param files un arreglo con los archivos subidos
     */
    public function __construct( $request ) {
        $this->request = $request;
        $this->files = $this->request->getFiles();
        $this->validacion =  \Config\Services::validation();
        $this->setRules();
    }

    /**
     * devuelve un arreglo con todos los errores reportados
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * devuelve el último error reportado
     * o false si no existe algun error
     */
    public function getError() {
        return $this->lastError;
    }

    /**
     * sube un archivo al servidor y lo mueve a la carpeta indicada
     * @param input el nombre del input file y de las reglas de validacion (deben ser el mismo)
     * @param dir el directorio al que se desea subir el archivo
     * @param filename el nombre con el que se va a guardar el archivo (opcional, se puede obtener desde el origen)
     * @return string con el nombre del archivo si el archivo se sube sin problema
     * @return false si el archivo no puede ser subido
     */
    public function subir( $input, $dir, $filename=null ) {
        if ($this->validar($input)) {
            try {
                $file = $this->files[$input];
                if ( is_null($filename) ) {
                    $filename = $file->getClientName();
                }
                if (!is_dir($dir)) {
                    mkdir($dir, 0700);
                }
                $res = $file->move($dir, $filename);
                if ($file->hasMoved()) {
                    return $file->getName();
                }
                $this->addError( 'El archivo subido no se pudo vincular. ' );
            }
            catch ( \Exception $ex ) {
                log_message( 'error', 'ERROR: {ex}', [ 'ex'=>$ex ] );
                $this->addError( 'No se pudo subir el archivo. ' );
            }
        }
        return false;
    }

    /**
     * valida un archivo de acuerdo con los requerimientos definidos por el nombre
     * @param nombre el nombre del control de inicio para validar el archivo
     * @return true en caso de que el archivo respete las reglas definidas
     * @return false en caso de que el archivo incumpla alguna regla
     */
    private function validar($nombre) {
        $numErrores = 0;
        $esValido = $this->uploaded('',$nombre);
        if ( $esValido ) {
            if ( array_key_exists('max_size', $this->fileRules[$nombre] ) ) {
                if ( !$this->max_size( '', $nombre.','.$this->fileRules[ $nombre ][ 'max_size' ] ) ) {
                    $this->addError( $this->fileRules_errors[ $nombre ]['max_size'] );
                    $numErrores++;
                }
            }
            if ( array_key_exists('is_image', $this->fileRules[$nombre] ) ) {
                if ( !$this->is_image( '', $nombre ) ) {
                    $this->addError( $this->fileRules_errors[ $nombre ]['is_image'] );
                    $numErrores++;
                }
            }
            if ( array_key_exists('mime_in', $this->fileRules[$nombre] ) ) {
                if ( !$this->mime_in( '', $nombre.','.$this->fileRules[ $nombre ][ 'mime_in' ] ) ) {
                    $this->addError( $this->fileRules_errors[ $nombre ]['mime_in'] );
                    $numErrores++;
                }
            }
            if ( array_key_exists('ext_in', $this->fileRules[$nombre] ) ) {
                if ( !$this->ext_in( '', $nombre.','.$this->fileRules[ $nombre ][ 'ext_in' ] ) ) {
                    $this->addError( $this->fileRules_errors[ $nombre ]['ext_in'] );
                    $numErrores++;
                }
            }
            if ( array_key_exists('max_dims', $this->fileRules[$nombre] ) ) {
                if ( !$this->max_dims( '', $nombre.','.$this->fileRules[ $nombre ][ 'max_dims' ] ) ) {
                    $this->addError( $this->fileRules_errors[ $nombre ]['max_dims'] );
                    $numErrores++;
                }
            }
            
        }
        else {
            $this->addError( 'Por favor sube un archivo correcto.' );
            $numErrores++;
        }
        if ( $numErrores === 0 ) return true;
        else return false;
    }

    /**
     * agrega un error a la pila de errores del objeto
     * @param error una cadena con el error reportado
     */
    private function addError( $error ) {
        $this->lastError = $error;
        $this->errors[] = $error;
    }

    /**
     * pone todas las reglas de archivos en los arreglos correspondientes definiendo los valores fijos
     * de los arreglos destinados para tal efecto y utilizados en el servicio de validación
     */
    private function setRules() {
        $this->fileRules = [
            'comprobante' => [
                'max_size' => '512', 'ext_in' => 'pdf',
            ],
            'pagosros' => [
                'max_size' => '1024', 'ext_in' => 'txt',
            ],
        ];
        $this->fileRules_errors = [
            'comprobante' => [
                'max_size' => 'El comprobante no debe tener un tamaño mayor a 512kb',  
                'ext_in' => 'El comprobante debe ser un archivo PDF.'
            ],
            'pagosros' => [
                'max_size' => 'El comprobante no debe tener un tamaño mayor a 1MB',  
                'ext_in' => 'El comprobante debe ser un archivo de texto.'
            ],
        ];
    }
    
}