<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesEtapas;
class AspirantesEtapasModel extends Model
{
    protected $table="aspirantes_etapas";
    protected $primaryKey="etapa_id";
    protected $returnType='App\Entities\AspirantesEtapas';
    protected $useSoftDeletes = true;
    protected $allowedFields=['etapa_id','etapa_periodo','etapa_nombre','etapa_orden','etapa_formulario','created_at','updated_at','deleted_at','etapa_responsable'];
    protected $db;
    protected $builder;
    protected $useTimestamps = true;
    
    /**
     * Regresa el total de etapas activas existentes
     */
    public function buscaretapas()
    {
        $this->orderBy('etapa_orden','ASC');
        return $this->findAll();
    }

    /**
     * obtiene la etapa de acuerdo al id
     * @param etapa_id ID de la etapa 
     */
    public function buscaretapasporid($etapa_id)
    {
        return $this
        ->where("etapa_id",$etapa_id)
        ->find();
    }
    /**
     * obtiene las etapas del proceso de admisión de determinado periodo
     * @param periodo el ID o clave del periodo a obtener el proceso
     * @return etapas las etapas que tiene el periodo
     */
    public function getPeriodo($periodo) {
        $this->where( 'etapa_periodo', $periodo );
        $this->orderBy('etapa_orden','ASC');
        $etapas = $this->findAll();
        return $etapas;
    }

    /**
     * @return lista contiene la lista de las etapas con formularios existentes para el periodo por iniciar
     * 
     */
    public function getEtapasFormularios()
    {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_formularios', 'etapa_id=formulario_etapa', 'inner' );
        $this->builder->where('aspirantes_etapas.deleted_at IS NULL');
        $this->builder->where('aspirantes_formularios.deleted_at IS NULL');
        $this->builder->orderBy('etapa_id','ASC');
        $lista = $this->builder->get();
        return $lista->getResult();
    }

    /**
     * 
     * @return lista listado de etapas que contengan en uno de sus formularios contengan por lo menos algun 
     * item de tipo archivo
     */
    public function getEtapasDocumentales()
    {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_formularios', 'etapa_id=formulario_etapa', 'inner' );
        $this->builder->join( 'aspirantes_campos', 'campo_formulario=formulario_id', 'inner' );
        $this->builder->join( 'aspirantes_tipos_campo', 'tipocampo_id=campo_tipo', 'inner' );
        
        $this->builder->where('aspirantes_etapas.deleted_at IS NULL');
        $this->builder->where('aspirantes_formularios.deleted_at IS NULL');
        $this->builder->where('aspirantes_campos.deleted_at IS NULL');
        
        $this->builder->where(' aspirantes_campos.campo_tipo', '4');
        
        
        $this->builder->orderBy('etapa_id','ASC');
        
       
        $lista = $this->builder->get();
        return $lista->getResult();
    }
}

?>