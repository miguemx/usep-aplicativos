<?php

namespace App\Controllers;

use App\Entities\VotacionProcesos;
use App\Models\VotacionProcesosModel;
use App\Entities\VotacionCandidatos;
use App\Models\VotacionCandidatosModel;
use App\Entities\VotacionPadron;
use App\Models\VotacionPadronModel;
use App\Entities\Votacion;
use App\Models\VotacionesModel;
use App\Models\AlumnosModel;
use App\Models\EmpleadosModel;
use App\Models\UsuariosModel;

class Votaciones extends BaseController
{
    public function menu(){
        $data['menu'] = $this->sesion->get('menu');
        return view('votacionvirtual/menu',$data);
    }
    public function procesos(){
        $insertaprocesoModel = new VotacionProcesosModel();
        $ban = null;
        $insertaproceso = new VotacionProcesos();
        $insertaprocesoModel = new VotacionProcesosModel();
        $busquedaprocesos = $insertaprocesoModel->findAll();
        $nombreproceso = $this->request->getPost('nombreproceso');
        $fechainicio = $this->request->getPost('fechainicio');
        $fechafin = $this->request->getPost('fechafinal');
        $fechainiciotime = $this->request->getPost('fechainiciotime');
        $fechafintime = $this->request->getPost('fechafintime');
        $data['procesos'] = $insertaprocesoModel->buscaprocesos3();

        foreach ($busquedaprocesos as $procesos){
            if($procesos->nombre === $nombreproceso & $procesos->inicio === $fechainicio & $procesos->fin === $fechafin){
                $ban= 1;
                $data['mensajeerror'] = "Ya se encuentra registrado el proceso.";
            }
        }
        if($ban == null){
            $insertaproceso->inicio =  $fechainicio." ".$fechainiciotime;
            $insertaproceso->fin = $fechafin." ".$fechafintime;
            $insertaproceso->nombre = $nombreproceso;
            $insertaproceso->terminado = 0;
        }
        if($nombreproceso != "" & $fechainicio != "" & $fechafin != ""){
            try{
                $insertaprocesoModel->insert($insertaproceso);
                $data['mensaje'] = "Se realizó el registro correctamente";
            } catch (\Exception $ex) {
                $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
            }
        }
        $data['menu'] = $this->sesion->get('menu');
         return view('votacionvirtual/proceso', $data);
    }
    public function candidatos(){
        $candidato = new VotacionCandidatos();
        $insertacandidatos = new VotacionCandidatosModel();
        $ban = null;
        $votacionprocesoModel = new VotacionProcesosModel();
        $proceso = $votacionprocesoModel->buscaprocesos();
        $data['procesos'] = $proceso;
        $idproceso = $this->request->getPost('selectproceso');
        $correo = $this->request->getPost('correocandidato');
        $nombre = $this->request->getPost('nombrecandidato');
        $busquedacandidatos = $insertacandidatos->findAll();
        $botonfinal = $this->request->getpost('btn2');
        $data['menu'] = $this->sesion->get('menu');
        $data['idproceso'] = $idproceso;
        foreach ($busquedacandidatos as $candidatos){
            if($candidatos->correo === $correo & $candidatos->proceso === $idproceso){
                $ban= 1;
                $data['mensajeerror'] = "Ya se encuentra registrado el candidato al proceso seleccionado.";
                $correo = null;
                $nombre = null; 
            }
        }
        if($idproceso != "" & $correo != "" & $nombre != ""){
            $usuarios = new UsuariosModel();
            $consultacorreo = $usuarios->consultacorreo($correo);
            if($consultacorreo == null){
            $data['mensajecorreo'] = "El correo no se encuentra registrado en el sistema.";
            }
            elseif($ban == null){
                $candidato->proceso =  $idproceso;
                $candidato->correo = $correo;
                $candidato->nombre = $nombre;
                try{
                    $insertacandidatos->insert($candidato);
                    $data['mensaje'] = "Se realizó el registro correctamente";
                } catch (\Exception $ex) {
                    $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
                } 
                if($botonfinal == 2){
                    try {
                        $candidatoupdate = [
                            'proceso_terminado' => "1"
                        ];
                        $votacionprocesoModel->updateproceso1($idproceso,$candidatoupdate);
                        $data['mensajeexito'] = "Se finalizó correctamente el registro de candidatos";
                        $data['procesos'] = [];
                    } catch (\Exception $ex) {
                        $data['errorupdateproceso'] = 'No fue posible actualizar el estatus del proceso. ' . $ex->getMessage();
                    }
                    
                }/* else{
                    
                    return view("votacionvirtual/candidatos", $data);
                }  */
            }
        }
        if($ban == null && $botonfinal == 2){
            return $this->padron();
            //return view("votacionvirtual/padron", $data);
        }else{
            return view("votacionvirtual/candidatos", $data);
        }
    }
    public function padron(){
        $alumnosmodel = new AlumnosModel(); 
        $empleadosmodel = new EmpleadosModel(); 
        $votacionprocesoModel = new VotacionProcesosModel();
        $padron = new VotacionPadron();
        $insertapadron = new VotacionPadronModel();
        $proceso = $votacionprocesoModel->buscaprocesos1();
        $ban = null;
        $data['procesos'] = $proceso;
        $data['padronalumnos'] = [];
        $data['padronempleados'] = [];
        $data['check1'] = '';
        $data['check2'] = '';
        $idproceso = $this->request->getPost('selectproceso');
        $checkfiltro = $this->request->getPost('checkfiltro');
        $guardar = $this->request->getPost('btnguardar');
        $idalumno = $this->request->getPost('idempleado');
        $finaliza = $this->request->getPost('btnguardafinaliza');
        $busquedapadron = null;
        $validafinalizar = $this->validafinalizarproceso($idproceso);
        if($finaliza != null){
            if($validafinalizar == null){
                $data['mensajefinaliza'] = "No se encuentran candidatos registrados para finalizar el registro del padrón.";
            }
        }
        if($checkfiltro === '0'){
            $proceso = $votacionprocesoModel->buscaprocesos1();
            $alumnos = $alumnosmodel->findAll();
            $data['pager'] = $alumnosmodel->pager;
            $data['padronalumnos'] = $alumnos;
            $data['banderaempleados'] = 1;
            $data['check1'] = 0;
            if($guardar === "1" || $finaliza ==="1"){
                $busquedapadron = $insertapadron->buscapadron(0,$idproceso);
                $padron->proceso =  $idproceso;
                $padron->tipopadron =  0;
                if(($busquedapadron != null)){
                    $data['mensajeerror'] = "Ya se encuentran el registro de alumnos votantes al proceso seleccionado.";
                }else{
                        foreach($alumnos as $alumno){
                            $padron->correo = $alumno->correo;
                            try{
                                $insertapadron->insert($padron);
                                $data['mensaje'] = "Se realizó el registro correctamente";
                            } catch (\Exception $ex) {
                                $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
                            }
                        }
                        if($finaliza == "1"){
                            try {
                                $finalizavotantes = [
                                    'proceso_terminado' => 2
                                ];
                                $votacionprocesoModel->cambiaetapaproceso($idproceso,$finalizavotantes);
                                $data['mensajeexito'] = "Se finalizó la etapa de registro de padrón correctamente";
                            } catch (\Exception $ex) {
                                $data['errorfinalizaregistro'] = 'No fue posible finalizar la etapa de registro de padrón. ' . $ex->getMessage();
                            }
                        }
                $data['procesos'] = $votacionprocesoModel->buscaprocesos1();
                }
            }
        }elseif($checkfiltro === '1'){
            $proceso = $votacionprocesoModel->buscaprocesos1();
            $empleados = $empleadosmodel->findAll();
            $data['pager'] = $empleadosmodel->pager;
            $data['padronempleados'] = $empleados;
            $data['banderaalumno'] = 1;
            $data['check2'] = 1;
            if($guardar === "1" || $finaliza ==="1"){
                $busquedapadron = $insertapadron->buscapadron(1,$idproceso);
                $padron->proceso =  $idproceso;
                $padron->tipopadron =  1;
                if($busquedapadron != null){
                    $data['mensajeerror'] = "Ya se encuentran el registro de administrativos votantes al proceso seleccionado.";
                }else{
                        foreach($empleados as $empleado){
                            $padron->correo = $empleado->correo;
                            try{
                                $insertapadron->insert($padron);
                                $data['mensaje'] = "Se realizó el registro correctamente";
                            } catch (\Exception $ex) {
                                $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
                            }
                        } 
                    if($finaliza == "1"){
                        try {
                            $finalizavotantes = [
                                'proceso_terminado' => 2
                            ];
                            $votacionprocesoModel->cambiaetapaproceso($idproceso,$finalizavotantes);
                            $data['mensajeexito'] = "Se finalizó la etapa de registro de padrón correctamente";
                        } catch (\Exception $ex) {
                            $data['errorfinalizaregistro'] = 'No fue posible finalizar la etapa de registro de padrón. ' . $ex->getMessage();
                        }
                    }
                $data['procesos'] = $votacionprocesoModel->buscaprocesos1();
                }
            }
        }elseif($checkfiltro === '2'){
            $proceso = $votacionprocesoModel->buscaprocesos1();
            $busquedapadron = $insertapadron->findAll();
            $usuarios = new UsuariosModel();
            $correo = $this->request->getPost('correoproceso');
            $data['banderatabla'] = 1;
             foreach ($busquedapadron as $padrones){
                if($padrones->correo === $correo & $padrones->proceso === $idproceso){
                    $ban= 1;
                    $data['mensajeerror'] = "Ya se encuentra registrado el votante registrado.";
                }
            }
            $consultacorreo = $usuarios->consultacorreo($correo);
            if($consultacorreo == null){
            $data['mensajecorreo'] = "El correo no se encuentra registrado en el sistema.";
            }
            elseif($idproceso != "" & $correo != ""){
                if($ban == null){
                    $padron->proceso =  $idproceso;
                    $padron->correo = $correo;
                    $padron->tipopadron =  2;
                    try{
                        $insertapadron->insert($padron);
                        $data['mensaje'] = "Se realizó el registro correctamente";
                    } catch (\Exception $ex) {
                        $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
                    }
                }
                if($finaliza == "1"){
                    try {
                        $finalizavotantes = [
                            'proceso_terminado' => 2
                        ];
                        $votacionprocesoModel->cambiaetapaproceso($idproceso,$finalizavotantes);
                        $data['mensajeexito'] = "Se finalizó la etapa de registro de padrón correctamente";
                    } catch (\Exception $ex) {
                        $data['errorfinalizaregistro'] = 'No fue posible finalizar la etapa de registro de padrón. ' . $ex->getMessage();
                    }
                }
                $data['procesos'] = $votacionprocesoModel->buscaprocesos1();

            }
        }
        $data['menu'] = $this->sesion->get('menu');
        return view("votacionvirtual/registropadron", $data);
    }
    public function voto(){
        $padron = new VotacionPadronModel();
        $votacionprocesoModel = new VotacionProcesosModel();
        $candidatosModel = new VotacionesModel();
        $correoaplicativos = $this->sesion->get('useremail');
        $proceso = $votacionprocesoModel->buscaprocesos2();
        $idproceso = $this->request->getPost('selectproceso');
        $candidatos = [];
        $votos = null;
        $correo2 = null;
        $proceso2 = null;
        if(isset($mensajeexito)){
                $data['mensajeexito'] = "Su voto ha sido registrado correctamente";
        }
        //*****Buscar en tabla padron para validar si ya se ha votado ********
        $votopadron = $padron->buscavoto($idproceso,$correoaplicativos);
        if($idproceso != null ){
            if($votopadron != null){
                foreach($votopadron as $voto){
                    $votos = $voto->voto_abstencion;
                }
                if($votos === null){
                    $candidatos = $candidatosModel->buscaprocesos($idproceso);
                    $correo2 = $correoaplicativos;
                    $proceso2 = $idproceso;
                }else{
                    $data['votorealizado'] = "Su voto ya se encuentra registrado";
                }
            }else{
            $data['noregistro'] = "No se encuentra registrado al proceso seleccionado";
            }
        }
        $data['arrprocesos'] = $idproceso;
        $data['correo'] = $correoaplicativos;
        $data['procesos'] = $proceso;
        $data['candidatos'] = $candidatos;
        $data['correo2'] = $correo2;
        $data['proceso2'] = $proceso2;
        $data['menu'] = $this->sesion->get('menu');
        return view("votacionvirtual/padron", $data);
    }
    public function registrovoto(){
        $padronModel = new VotacionPadronModel();
        $candidatosModel = new VotacionCandidatosModel();
        $votacionprocesoModel = new VotacionProcesosModel();
        $procesoarr = $votacionprocesoModel->buscaprocesos2();
        $idcandidato = $this->request->getPost('idcandidato');
        $correo = $this->sesion->get('useremail');
        $proceso = $this->request->getPost('proceso');
        $voto = $this->request->getPost('voto');
        if($proceso == null){
            $data['seleccionaproceso'] = "Debes seleccionar un proceso.";
        }
        elseif($voto === "1"){
            $candidatos = $candidatosModel->buscacandidato($idcandidato,$proceso );
            foreach($candidatos as $candidato){
                $numvotos = $candidato->voto;
            }
            try {
                $candidatoupdate = [
                    'candidatos_voto' =>$numvotos+1
                ];
                $candidatosModel->updatecandidato($idcandidato,$proceso,$candidatoupdate);
                $votoupdate = [
                    'padron_voto_abstencion' => $voto
                ];
                $padronModel->updatePadron($correo,$proceso,$votoupdate);
                $data['mensajeexito'] = "Su voto ha sido registrado correctamente";
            } catch (\Exception $ex) {
                $data['error'] = 'No fue posible guardar el voto. ' . $ex->getMessage();
            }
        }elseif($voto === "0"){
            try {
                $votoupdate = [
                    'padron_voto_abstencion' => $voto
                ];
                $padronModel->updatePadron($correo,$proceso,$votoupdate);
                $data['mensajeexito'] = "Su voto ha sido registrado correctamente";
            } catch (\Exception $ex) {
                $data['error'] = 'No fue posible guardar el voto. ' . $ex->getMessage();
            }
        }
        $padron = $padronModel->buscavotos($proceso);
        $votantes = 0;
        $votos = 0;
        foreach($padron as $padrones){
            $votantes = $votantes + 1;
            if($padrones->voto_abstencion != null){
                $votos = $votos + 1;
            }
        }
        if($votos === $votantes){
            $this->finprocesovoto($proceso);
        }
        $data['arrprocesos'] = "";
        $data['correo'] = $correo;
        $data['procesos'] = $procesoarr;
        $data['candidatos'] = [];
        $data['correo2'] = "";
        $data['proceso2'] = "";
        $data['menu'] = $this->sesion->get('menu');
        return view("votacionvirtual/padron", $data);
    }
    public function estadisticas(){
        $votacionprocesoModel = new VotacionProcesosModel();
        $candidatosModel = new VotacionesModel();
        $idproceso = null;
        $candidatos = null;
        $procesos = null;
        $procesos = $votacionprocesoModel->findAll();
        $idproceso = $this->request->getPost('selectproceso');
        $candidatos = $candidatosModel->buscaprocesos($idproceso);
        $data['procesoid'] = $idproceso;
        $data['candidatos'] = $candidatos;
        $data['procesos'] = $procesos;
        $data['menu'] = $this->sesion->get('menu');
        return view("votacionvirtual/estadisticas",$data);
    }
     public function finproceso(){
        $votacionprocesoModel = new VotacionProcesosModel();
        $idproceso = $this->request->getPost('selectproceso');
        try{
            $finproceso = [
                //'proceso_terminado' => 2
                'proceso_terminado' => 3
            ];
            $votacionprocesoModel->finalizaproceso($idproceso,$finproceso);
            $data['mensajefinalizado'] = "Se finalizó el proceso correctamente";
        } catch (\Exception $ex) {
            $data['errorfinalizado'] = 'No fue posible finalizar el proceso seleccionado. ' . $ex->getMessage();
        }
        $data['procesos'] = $votacionprocesoModel->buscaprocesos();
        return view('votacionvirtual/proceso', $data);
    }
    public function validafinalizarproceso($idproceso){
        $candidatosModel = new VotacionCandidatosModel();
        $consulta = $candidatosModel->consultacandidatos($idproceso); 
        return $consulta;
    }
    public function ganador(){
        $padronModel = new VotacionPadronModel();
        $votacionprocesoModel = new VotacionProcesosModel();
        $idproceso =  $this->request->getPost('idproceso');
        $valor = $this->finalizarprocesoautomatico($idproceso);
        $candidatos = [];
        $ganador = null;
        $validarfinproceso = $votacionprocesoModel->validafinprocesos($idproceso);
        if($valor === 1 || $validarfinproceso != null){
            $data['mensaje'] = "Ha finalizado el proceso correctamente";
            $candidatos = $this->validafinalizarproceso($idproceso);
            $data['candidatos'] = $candidatos;
        }elseif($valor === 2){
            $data['mensajeerror'] = "Aún no finaliza el tiempo del proceso seleccionado, no se puede mostrar la tabla de posiciones";
            $data['candidatos'] = $candidatos;
        }
        $abstenciones =$padronModel->buscavotosabstencion($idproceso);
        $conteo = 0;
        foreach($abstenciones as $abstencion){
            $conteo = $conteo + 1;
        }
        $candidatosModel = new VotacionCandidatosModel();
        $retornacandidatos  = $candidatosModel->ganador($idproceso);
        foreach($retornacandidatos as $candidato){
            $ganador = $candidato->nombre;
        }
        $data['menu'] = $this->sesion->get('menu');
        $data['abstenciones'] = $conteo;
        $data['ganador'] = $ganador;
        return view('votacionvirtual/ganador', $data);
    }
    private function finalizarprocesoautomatico($idproceso){
        $fechaHora = strtotime(date("Y-m-d H:i:s"));
        $fechafin = null;
        $proceso[] = null ;
        $bandera = null;
        $votacionprocesoModel = new VotacionProcesosModel();
        $proceso = $votacionprocesoModel->busca($idproceso);
        $fechafin = strtotime($proceso[0]->fin);
        if($fechafin < $fechaHora){
            try{
                $finproceso = [
                    //'proceso_terminado' => 2
                    'proceso_terminado' => 3
                ];
                $votacionprocesoModel->finalizaproceso($idproceso,$finproceso);
                $data['mensajefinalizado'] = "Se finalizó el proceso correctamente";
            } catch (\Exception $ex) {
                $data['errorfinalizado'] = 'No fue posible finalizar el proceso seleccionado. ' . $ex->getMessage();
            }
            $bandera = 1; 
            //echo "se ha finalizado el proceso correctamente";
        }
        else{
            $bandera = 2; 
            //echo "Aún no finaliza el tiempo del proceso seleccionado, no se puede mostrar la tabla de posiciones";
        }
        return $bandera;
    }
    public function finprocesovoto($idproceso){
        $votacionprocesoModel = new VotacionProcesosModel();
        //$idproceso = $this->request->getPost('selectproceso');
        try{
            $finproceso = [
                //'proceso_terminado' => 2
                'proceso_terminado' => 3
            ];
            $votacionprocesoModel->finalizaproceso($idproceso,$finproceso);
            //$data['mensajefinalizado'] = "Se finalizó el proceso correctamente";
        } catch (\Exception $ex) {
            $data['errorfinalizado'] = 'No fue posible finalizar el proceso seleccionado. ' . $ex->getMessage();
        }
        $data['procesos'] = $votacionprocesoModel->buscaprocesos();
        return view('votacionvirtual/proceso', $data);
    }
    public function gestionprocesos(){
        $votacionprocesoModel = new VotacionProcesosModel();
        $busquedaradio = $this->request->getPost('busquedaradio');
        $proceso = [];
        $btnidproceso = $this->request->getPost('btnidproceso');
        $btnguardar = $this->request->getPost('guardar');
        if($busquedaradio === '0'){
            $proceso = $votacionprocesoModel->buscaprocesosetapas($busquedaradio);
        }
        elseif($busquedaradio === '1'){
            $proceso = $votacionprocesoModel->buscaprocesosetapas($busquedaradio);
        }
        elseif($busquedaradio === '2'){
            $proceso = $votacionprocesoModel->buscaprocesosetapas($busquedaradio);
        }
        elseif($busquedaradio === '3'){
            $proceso = $votacionprocesoModel->buscaprocesosetapas($busquedaradio);
        }
        elseif($busquedaradio === '4'){
            $proceso = $votacionprocesoModel->findAll();
        }
        $data['idproceso2'] = $this->request->getPost('idproceso2');
        $data['nombreproceso'] = "";
        $data['fechainicio'] = "";
        $data['fechainiciotime'] = "";
        $data['fechafinal'] = "";
        $data['fechafintime'] = "";
        $data['estatus'] = "";
        if($btnidproceso != ""){
            $arreglo = $this->buscadatosproceso($btnidproceso);
            $fechainicio=substr($arreglo['inicio'] , 0, -9);
            $fechainiciotime=substr($arreglo['inicio'] , 11);
            $fechafin=substr($arreglo['fin'] , 0, -9);
            $fechafintime=substr($arreglo['fin'] , 11);
            $data['nombreproceso'] = $arreglo['nombre'];
            $data['estatus'] = $arreglo['terminado'];
            $data['fechainicio'] = $fechainicio;
            $data['fechainiciotime'] = $fechainiciotime;
            $data['fechafinal'] = $fechafin;
            $data['fechafintime'] = $fechafintime;
            $data['idproceso2'] = $btnidproceso;
        }
        if($btnguardar === '1'){
            $idproceso = $this->request->getPost('idproceso2');
            $fechainicio = $this->request->getPost('fechainicio');
            $fechainiciotime = $this->request->getPost('fechainiciotime');
            $fechafin = $this->request->getPost('fechafinal');
            $fechafintime = $this->request->getPost('fechafintime');
            $nombreproceso = $this->request->getPost('nombreproceso');
            $status = $this->request->getPost('estatus');
            $arreglo = [
                'id' => $idproceso, 'inicio' => $fechainicio." ".$fechainiciotime, 'fin' => $fechafin." ".$fechafintime, 'nombre' => $nombreproceso, 'terminado' => $status
            ];
            $mensaje = $this->actualizaproceso($idproceso,$arreglo);
            if(isset($mensaje['exito'])){
                $data['actualizacorrecto'] = $mensaje['exito'];
            }
            if(isset($mensaje['error'])){
                $data['noactualiza'] = $mensaje['error'];
            }
        }
        $valor = $this->request->getPost('btnidproceso');
        $data['procesos'] = $proceso;
        $data['busquedaradio'] = $busquedaradio;
        $data['menu'] = $this->sesion->get('menu');
        return view('votacionvirtual/gestionprocesos',$data);
    }
    private function buscadatosproceso($idproceso){
        $votacionprocesoModel = new VotacionProcesosModel();
        $datosproceso = $votacionprocesoModel->busca($idproceso);
        $arreglodatos = [];
        foreach($datosproceso as $proceso){
            $arreglodatos = [
                'id' => $proceso->id, 'inicio' => $proceso->inicio, 'fin' => $proceso->fin, 'nombre' => $proceso->nombre, 'terminado' => $proceso->terminado
            ];
        }
        return $arreglodatos;
    }
    private function actualizaproceso($idproceso,$arreglo){
        $votacionprocesoModel = new VotacionProcesosModel();
        try {
            $actualiza = [
                'proceso_nombre' => $arreglo['nombre'],
                'proceso_inicio' => $arreglo['inicio'],
                'proceso_fin' => $arreglo['fin'],
                'proceso_terminado' => $arreglo['terminado']
            ];
            $votacionprocesoModel->updateproceso1($idproceso,$actualiza);
            $data['exito'] = "Se actualizó correctamente los datos del proceso";
        } catch (\Exception $ex) {
            $data['error'] = 'No fue posible actualizar los datos del proceso. ' . $ex->getMessage();
        }
        return $data;
    }
}