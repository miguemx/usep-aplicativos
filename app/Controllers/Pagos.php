<?php
namespace App\Controllers;

use App\Models\RosModel;
use App\Models\PagosAlumnosModel;
use App\Entities\Ros;
use App\Helpers\Uploader;

class Pagos extends BaseController {

    private $pagosdir = '';
    private $rosModel = null;
    private $pagosAlumnosModel = null;

    public function __construct() {
        $this->pagosdir =  WRITEPATH.'pagos/procesar';
        $this->pagosprocesados =  WRITEPATH.'pagos/completados';

        $this->rosModel = new RosModel();
        $this->pagosAlumnosModel = new PagosAlumnosModel();
    }

    public function index() {
        return $this->listado();
    }

    public function listado() {
        $data['matricula'] = ( $this->request->getGet('matricula') )? $this->request->getGet('matricula'): '';
        $data['referencia'] = ( $this->request->getGet('referencia') )? $this->request->getGet('referencia'): '';
        $data['status'] = ( $this->request->getGet('status') )? $this->request->getGet('status'): '';
        $data['tipo'] = ( $this->request->getGet('tipo') )? $this->request->getGet('tipo'): '';
        $data['fecha'] = ( $this->request->getGet('fecha') )? $this->request->getGet('fecha'): '';
        $pagosAlumnos = $this->pagosAlumnosModel->buscar( $data );
        $data['pagos'] = $pagosAlumnos;
        $data['pager'] = $this->pagosAlumnosModel->pager;
        $data['name'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
        return view( 'pagos/listado', $data );
    }

    /**
     * renderiza la vista para la carga de archivos del ROS
     */
    public function cargaRos() {
        
        if ( $this->request->getPost('smb') === 'fros' ) {
            $uploader = new Uploader( $this->request );
            $archivo = $uploader->subir( 'pagosros', $this->pagosdir );
            if ( is_string($archivo) ) {
                $data['mensaje'] = 'Archivo subido con éxito. Será procesado en breve.';
            }
            else {
                $data['errors'] = $uploader->getErrors();
            }
        }
        $data['name'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
        return view( 'pagos/cargaros', $data );
    }
    

}