<?php
namespace App\Models;
use CodeIgniter\Model;

class CambioSeccionesModel extends Model {
    protected $table      = 'aca_cambios_secciones';
    protected $primaryKey = 'cambios_matricula';

    protected $returnType    = 'App\Entities\CambioSecciones';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'cambios_matricula','cambios_seccion_procedencia','cambios_seccion_final','cambios_oficio','cambios_observaciones','cambios_usuario',
    ];


    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

   
}