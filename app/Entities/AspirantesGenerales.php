<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesGenerales extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'folio' => null,
        'carrera' => null,
        'etapa' => null,
        'validado' => null,
        'curp' => null,
        'nombre' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'correo' => null,
        'telefono' => null,
        'calle' => null,
        'colonia' => null,
        'numero' => null,
        'num_int' => null,
        'cp' => null,
        'municipio' => null,
        'estado' => null,
        'pais' => null,
        'sexo' => null,
        'nac_fecha' => null,
        'nac_municipio' => null,
        'nac_estado' => null,
        'nac_pais' => null,
        'revalidar' => null,
        'estatus' => null,
        'dia' => null,
        'hora'=> null,
        'ficha'=> null,
        'aceptado' => null,
        'fechaInscripcion' => null,
        'horaInscripcion' => null,
        'inscrito' => null,
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
   ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'folio' => 'aspirante_id',
        'carrera' => 'aspirante_carrera',
        'etapa' => 'aspirante_etapa',
        'validado' => 'aspirante_validado',
        'curp' => 'aspirante_curp',
        'nombre' => 'aspirante_nombre',
        'apPaterno' => 'aspirante_ap_paterno',
        'apMaterno' => 'aspirante_ap_materno',
        'correo' => 'aspirante_correo',
        'telefono' => 'aspirante_telefono',
        'calle' => 'aspirante_calle',
        'colonia' => 'aspirante_colonia',
        'numero' => 'aspirante_numero',
        'num_int' => 'aspirante_num_int',
        'cp' => 'aspirante_cp',
        'municipio' => 'aspirante_municipio',
        'estado' => 'aspirante_estado',
        'pais' => 'aspirante_pais',
        'sexo' => 'aspirante_sexo',
        'nac_fecha' => 'aspirante_nac_fecha',
        'nac_municipio' => 'aspirante_nac_municipio',
        'nac_estado' => 'aspirante_nac_estado',
        'nac_pais' => 'aspirante_nac_pais',
        'revalidar' => 'aspirante_revalida',
        'estatus' => 'aspirante_estatus',
        'dia'=>'aspirante_dia',
        'hora'=>'aspirante_hora',
        'ficha'=>'aspirante_ficha',
        'aceptado' => 'aspirante_aceptado',
        'fechaInscripcion' => 'aspirante_inscripcion_fecha',
        'horaInscripcion' => 'aspirante_inscripcion_hora',
        'inscrito' => 'aspirante_inscrito',
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}