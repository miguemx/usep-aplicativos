<?php

namespace App\Entities;

use CodeIgniter\Entity;

class HorarioDocente extends Entity
{
    protected $attributes = [
        'id' => null,
        'seccion' => null,
        'materia' => null,
        'id_docente' => null,
        'correo' => null,
        'nombre' => null,
        'apellidos' => null,
        'ap_paterno' => null,
        'ap_materno' => null,
        'periodo' => null,
        'materia_nombre' => null,
        'dia' => null,
        'inicio' => null,
        'fin' => null,
        'aula' => null,
        'aula_nombre' => null,
    ];
    protected $datamap = [
        'id' => 'grupo_id',
        'seccion' => 'grupo_clave',
        'materia' => 'grupo_materia',
        'id_docente' => 'grupo_docente',
        'correo' => 'empleado_correo',
        'nombre' => 'empleado_nombre',
        'apellidos' => 'empleado_apellido',
        'ap_paterno' => 'empleado_ap_paterno',
        'ap_materno' => 'empleado_ap_materno',
        'periodo' => 'grupo_periodo',
        'materia_nombre' => 'materia_nombre',
        'dia' => 'gpoaula_dia',
        'inicio' => 'gpoaula_inicio',
        'fin' => 'gpoaula_fin',
        'aula' => 'aula_id',
        'aula_nombre' => 'aula_nombre',
    ];
}
