<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@100&display=swap" rel="stylesheet">
<div class="container">
    <div class="bg-white rounded shadow p-2 mt-2 mb-4">
        <div class="col-sm-12">
            <h4 class="mb-2">Horario de clase</h4>
            <table class="table table-striped table-sm">
                <tr>
                    <th>Matrícula:</th>
                    <td><?php echo $alumno->id; ?></td>
                </tr>
                <tr>
                    <th>Nombre:</th>
                    <td><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></td>
                </tr>
                <tr>
                    <th>Correo:</th>
                    <td><?php echo $alumno->correo; ?></td>
                </tr>
                <tr>
                    <th>Contraseña:</th>
                    <td><?php echo substr( $alumno->curp, 0, 10); ?></td>
                </tr>
                <tr>
                    <th>Carrera:</th>
                    <td><?php echo $carrera->nombre; ?></td>
                </tr>
            </table>
            
            <table class="table table-borered" style="font-size: 12px; font-family: Roboto+Mono">
                <thead>
                    <tr>
                        <th>Sección</th>    
                        <th>Materia / Docente</th>

                        <th width="12%" style="text-align: center;">Lunes</th>
                        <th width="12%" style="text-align: center;">Martes</th>
                        <th width="12%" style="text-align: center;">Miércoles</th>
                        <th width="12%" style="text-align: center;">Jueves</th>
                        <th width="12%" style="text-align: center;">Viernes</th>
                        <th width="12%" style="text-align: center;">Sábado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $horario as $materia ): ?>
                        <tr>
                            <td><?php echo $materia['seccion']; ?></td>
                            <td>
                                <?php echo $materia['materia']; ?><br />
                                <em><?php echo mb_strtoupper($materia['docente']); ?></em>
                            </td>
                            <?php if( array_key_exists('LUNES', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['LUNES'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                            <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>

                            <?php if( array_key_exists('MARTES', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['MARTES'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                             <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>

                            <?php if( array_key_exists('MIERCOLES', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['MIERCOLES'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                             <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>

                            <?php if( array_key_exists('JUEVES', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['JUEVES'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                             <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>

                            <?php if( array_key_exists('VIERNES', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['VIERNES'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                             <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>

                            <?php if( array_key_exists('SABADO', $materia) ): ?>
                                <td>
                                    <?php foreach( $materia['SABADO'] as $hora=>$aula ): ?>
                                        <div style="text-align: center;">
                                            <?php echo $hora; ?><br />
                                             <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            <?php else: ?>
                                <td>&nbsp;</td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
            <div class="d-flex flex-row-reverse">            
                <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion'); ?>">
                    Finalizar
                </a>
            </div>
            
            <h4>Imprmibles</h4>
            <div class="d-flex">
                <input type="hidden" name="guardar" value="g" />
                <a class="btn btn-secondary" href="<?php echo base_url('Inscripcion/Documento').'/'.$alumno->id.'/horario'; ?>" target="_blank">
                    Horario
                </a>
                <div>&nbsp;</div>
            </div>
            

        </div>
    </div>
</div>

<?php echo $this->endSection() ?>