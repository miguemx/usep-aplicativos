<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Empleado extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'correo' => null,
        'horario' => null,
        'area' => null,
        'nombre' => null,
        'apellido' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'elseRegid' => null,
        'elseRegidpass' => null,
        'docente' => null,
        'sexo' => null,
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'         => 'empleado_id',
        'correo'     => 'empleado_correo',
        'horario'    => 'empleado_horario',
        'area'       => 'empleado_area',
        'nombre'     => 'empleado_nombre',
        'apellido'   => 'empleado_apellido',
        'apPaterno'   => 'empleado_ap_paterno',
        'apMaterno'   => 'empleado_ap_materno',
        'elseRegid'     => 'empleado_else_regid',
        'elseRegidpass' => 'empleado_else_regidpass',
        'docente' => 'empleado_docente',
        'sexo' => 'empleado_sexo',
    ];
    // protected $datamap = [
    //     'persona_id' => 'id'         ,
    //     'persona_horario' => 'horario'    ,
    //     'persona_area' => 'area'       ,
    //     'persona_nombre' =>'nombre'     , 
    //     'persona_apellido' =>'apellido'   
    // ];

    // protected $casts = [
    //     'options' => 'array',
    //             'options_object' => 'json',
    //             'options_array' => 'json-array'
    // ];

}
