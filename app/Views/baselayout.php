<!--
=========================================================
* Material Dashboard 2 - v3.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('templates'); ?>/material-dashboard/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url('favicon.ico'); ?>">
  <title>
    Universidad de la Salud
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/material-dashboard.css" rel="stylesheet" />

</head>

<body class="g-sidenav-show  bg-gray-200" onload="fload();">
  <?php echo $this->include('basemenu'); ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Top Navbar -->
    <?php echo $this->include('basetopnavbar'); ?>
    <!-- End Top Navbar -->
    <div class="container-fluid py-4">
      <div class="row min-vh-80 h-100">
        <!-- Inicia el contenido principal de la seccion -->
        <div class="col-12">
          <?php $this->renderSection('content'); ?>
        </div>
        <!-- Termina el contenido principal de la seccion -->
    </div>
    <footer class="footer pt-5">
      <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6 mb-lg-0 mb-4">
            <div class="copyright text-center text-sm text-muted text-lg-start">
              © &quot;Universidad de la Salud&quot; del Estado de Puebla
            </div>
          </div>
          <div class="col-lg-6">
            <!-- <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="#" class="nav-link text-muted" target="_blank">Sitio Web</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link text-muted" target="_blank">Correo electrónico</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link text-muted" target="_blank">Clases</a>
              </li>
            </ul> -->
          </div>
        </div>
      </div>
    </footer>
  </div>
  </main>
  
  <!--   Core JS Files   -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/bootstrap.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }

    let cforms = document.getElementsByClassName('form-control');
    for ( let i=0; i<cforms.length; i++ ) {
      if ( cforms[i].value != "" ) {
        cforms[i].parentElement.classList.add('is-filled');
      }
    }
    
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/material-dashboard.min.js"></script>
</body>

</html>