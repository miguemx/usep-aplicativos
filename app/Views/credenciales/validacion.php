<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <form action="<?php echo base_url('Credenciales/doValidacion'); ?>" method="post">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>Matrícula</th>
                    <td><?php echo $alumno['matricula'] ?></td>
                    <td rowspan="4">
                        <div class="form-floating">
                            <textarea class="form-control" id="floatingTextarea" name="comentarioFoto" style="height: 100px"></textarea>
                            <label for="floatingTextarea">Comentarios</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="revisada" name="revisada" checked="checked">
                                <label class="form-check-label" for="revisada">Revisada</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-secondary btn-sm">Guardar</button>
                        <button type="button" class="btn btn-secondary btn-sm" onclick="javascript:window.history.back();">Cancelar</button>
                        <input type="hidden" name="matricula" value="<?php echo $alumno['matricula']; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <td><?php echo $alumno['nombre'] ?></td>
                </tr>
                <tr>
                    <th>Carrera</th>
                    <td><?php echo $alumno['carrera'] ?></td>
                </tr>
                <tr>
                    <th>Correo</th>
                    <td><?php echo $alumno['correo'] ?></td>
                </tr>
                <tr>
                    <th>Identificación</th>
                    <td>
                        <?php if ($alumno['identificacion'] == 'SI'): ?>
                            <a href="<?php echo base_url('Credenciales/identificacion').'/'.$alumno['matricula']; ?>">Ver Identificación</a>
                        <?php else: ?>
                            NO
                        <?php endif; ?>
                    </td>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="identificacion" id="identificacion" value="1">
                            <label class="form-check-label" for="identificacion">
                                Correcto
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="identificacion" id="identificacion1" value="0" checked >
                            <label class="form-check-label" for="identificacion1">
                                Incorrecto
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Foto </th>
                    <td>
                        <?php if ($alumno['foto'] == 'SI'): ?>
                            <img src="<?php echo base_url('Credenciales/foto').'/'.$alumno['matricula']; ?>" alt="" />
                        <?php else: ?>
                            NO
                        <?php endif; ?>
                    </td>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foto" id="foto" value="1">
                            <label class="form-check-label" for="foto">
                                Correcto
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foto" id="foto1" value="0" checked>
                            <label class="form-check-label" for="foto1">
                                Incorrecto
                            </label>
                        </div> 
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>



<?php echo $this->endSection() ?>