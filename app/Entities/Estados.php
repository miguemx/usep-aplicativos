<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Estados extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'clave' => null,
        'nombre' => null,
        'abrev' => null,
        'activo' => null,
   ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id_estado' => 'id',
        'clave_estado' => 'clave',
        'nombre' => 'nombre',
        'abreviatura' => 'abrev',
        'reg_activo' => 'activo',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}