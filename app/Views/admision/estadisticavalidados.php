<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<div class="container ">
<div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
  <div class="card-header">
    <h1 style="display:flex; justify-content:center">Estadística de Aspirantes con Folio</h1>
  </div>
  <div class="card-body">
    <figure class="highcharts-figure">
      <div id="container"></div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Cantidad de Aspirantes</th>
            <th>Estado del folio</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($estadistica as $estadisticas) :?>
            <tr>
              <td><?php echo $estadisticas->aspirante_id?></td>
              <td><?php if ($estadisticas->aspirante_validado =='0') {echo 'Aspirantes sin folio';} else {echo 'Aspirantes con folio';}?></td>
            </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </figure>
  </div>
</div>
</div>    
<div class="container ">
  <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="card-header">
      <h1 style="display:flex; justify-content:center">Estadística de Aspirantes por Estatus de Registro</h1>
    </div>
    <div class="card-body">
      <figure class="highcharts-figure">
          <div id="container_status"></div>
            <table id="datatable">
              <thead>
                <tr>
                  <th>Cantidad de Aspirantes</th>
                  <th>Estatus</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($estadistica_estatus as $est_det) :?>
                  <tr>
                    <td><?php echo $est_det->aspirante_id?></td>
                    <td><?php echo $est_det->aspirante_estatus?></td>
                  </tr>
                <?php endforeach;?>
              </tbody>
            </table>
      </figure>
    </div>
  </div>
</div>

<script>
    Highcharts.chart('container', {
    /*data: {
      table: 'datatable' // descomentar esta sección si y solo si se desea que la fuente de datos de la gráfica sea la tabla de esta vista
    },*/
    /*chart: {
      type: 'column'
    },*/
    title: {
      text: 'Gráfica en tiempo real'
    },
    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Cantidad de Aspirantes'
      }
    },
    xAxis: {
      allowDecimals: false,
      categories: [
        <?php foreach ($estadistica as $estadisticas) :?>
          <?php if ($estadisticas->aspirante_validado =='0') {echo "'".'Aspirantes sin folio'."',";} else {echo "'".'Aspirantes con folio'."',";}?>
        <?php endforeach; ?>
      ],
      title: {
        text: 'Estatus de validación de correo electrónico'
      }
    },
    series: [{
    type: 'column',
    colorByPoint: true,
    data: [
       <?php foreach ($estadistica as $estadisticas) :?>
           <?php echo $estadisticas->aspirante_id.","?>
         <?php endforeach; ?>
    ],
    showInLegend: false
  }],
    legend: {
        enabled: false
    },/*
    colorByPoint: {
        enabled: true
    }*/
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          this.point.y + ' ' + this.point.name.toUpperCase();
      }
    }
    
  }
  );

  Highcharts.chart('container_status', {
    /*data: {
      table: 'datatable' // descomentar esta sección si y solo si se desea que la fuente de datos de la gráfica sea la tabla de esta vista
    },*/
    /*chart: {
      type: 'column'
    },*/
    title: {
      text: 'Gráfica en tiempo real el estatus del aspirante'
    },
    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Cantidad de Aspirantes'
      }
    },
    xAxis: {
      allowDecimals: false,
      categories: [
        <?php foreach ($estadistica_estatus as $estadisticas) :?>
          <?php echo "'".$estadisticas->aspirante_estatus."',";?>
        <?php endforeach; ?>
      ],
      title: {
        text: 'Estatus del aspirante'
      }
    },
    series: [{
    type: 'column',
    colorByPoint: true,
    data: [
       <?php foreach ($estadistica_estatus as $estadisticas) :?>
           <?php echo $estadisticas->aspirante_id.","?>
         <?php endforeach; ?>
    ],
    showInLegend: false
  }],
    legend: {
        enabled: false
    },/*
    colorByPoint: {
        enabled: true
    }*/
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          this.point.y + ' ' + this.point.name.toUpperCase();
      }
    }
    
  }
  );
  
  let creditos = document.getElementsByClassName('highcharts-credits');
    creditos[0].childNodes[0].data = '';
    creditos[1].childNodes[0].data = '';
</script>
<?php echo $this->endSection() ?>