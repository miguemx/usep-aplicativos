DROP TABLE IF EXISTS `filtro_reportes_docentes`;
CREATE TABLE `filtro_reportes_docentes` (
  `reportes_id` int(5) NOT NULL AUTO_INCREMENT,
  `reportes_correo` varchar(25) NOT NULL,
  `reportes_matricula` varchar(25) DEFAULT '',
  `reportes_fechacontagio` date DEFAULT NULL,
  PRIMARY KEY (`reportes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

ALTER TABLE filtro_sanitario ADD COLUMN filtro_tipotos VARCHAR(20) AFTER filtro_mayortos;