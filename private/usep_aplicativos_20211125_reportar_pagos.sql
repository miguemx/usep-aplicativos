CREATE TABLE IF NOT EXISTS `pagos_conceptos` (
  `concepto_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `concepto_nombre` VARCHAR(120) NOT NULL,
  `concepto_descripcion` VARCHAR(500) NULL,
  `concepto_monto` DOUBLE NULL,
  `created_at` DATE NULL,
  `updated_at` DATE NULL,
  `deleted_at` DATE NULL,
  PRIMARY KEY (`concepto_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `alumnos_pagos` (
  `pago_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pago_alumno` VARCHAR(9) NOT NULL,
  `pago_concepto` INT UNSIGNED NOT NULL,
  `pago_referencia` VARCHAR(20) NOT NULL,
  `pago_status` ENUM('EN PROCESO', 'ACEPTADO', 'RECHAZADO') NOT NULL,
  `pago_monto` DOUBLE NOT NULL,
  `pago_concepto_texto` VARCHAR(120) NOT NULL,
  `pago_fecha` DATE NOT NULL,
  `pago_comprobante` VARCHAR(100) NOT NULL COMMENT 'Ruta relativa al directorio Writable donde se almacena el archivo PDF del comprobante\n',
  `pago_observaciones` TEXT NULL,
  `created_at` DATE NULL,
  `updated_at` DATE NULL,
  `deleted_at` DATE NULL,
  PRIMARY KEY (`pago_id`),
  INDEX `pagosestudiantes_idx` (`pago_alumno` ASC) ,
  INDEX `pagosconceptos_idx` (`pago_concepto` ASC) ,
  CONSTRAINT `pagosestudiantes`
    FOREIGN KEY (`pago_alumno`)
    REFERENCES `alumnos_generales` (`alumno_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `pagosconceptos`
    FOREIGN KEY (`pago_concepto`)
    REFERENCES `pagos_conceptos` (`concepto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE `usep_aplicativos`.`pagos_ros` ( 
  `ros_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
  `ros_fecha_pago` DATE NOT NULL , 
  `ros_nombre` VARCHAR(125) NOT NULL , 
  `ros_concepto` VARCHAR(500) NOT NULL , 
  `ros_cuenta` VARCHAR(10) NOT NULL , 
  `ros_desc_cuenta` VARCHAR(500) NOT NULL , 
  `ros_importe` DOUBLE NOT NULL , 
  `ros_folio_inteligente` VARCHAR(12) NOT NULL , 
  `ros_referencia` VARCHAR(20) NOT NULL , 
  `ros_estatus_servicio` VARCHAR(20) NOT NULL , 
  `ros_fecha_otorgado` DATE NOT NULL , 
  `ros_usuario` VARCHAR(10) NOT NULL , 
  `ros_unidad_responsable` VARCHAR(80) NOT NULL , 
  `ros_beneficiario` VARCHAR(159) NOT NULL , 
  `ros_cotejado` VARCHAR(1) NOT NULL , 
  `ros_matricula` VARCHAR(9) NOT NULL , 
  `created_at` DATE NOT NULL , `updated_at` DATE NOT NULL , `deleted_at` DATE NOT NULL , 
  PRIMARY KEY (`ros_id`)
  ) ENGINE = InnoDB;