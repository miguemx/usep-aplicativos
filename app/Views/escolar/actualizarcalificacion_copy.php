<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    
<?php if($bandera_captura == ""){
                        $desplegar = 'style="display:none"';
                        $busqueda= "";
                        $text = "Buscar";
                    }else{
                        $desplegar = "";
                        $busqueda= 'style="display:none"';
                        $text = "Guardar";
                    } ?>
<style>
    .main{
        margin-top: 35px; 
        display:flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
    }
</style>
<div class="container-fluid">
    <form class="row g-3" method="post">
        
        <!-- <div class="col-12">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div> -->
        
    </form>
</div>
<div class="container-fluid row col-md-8 offset-md-2 main">

    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php if ( isset($exito) ): ?>
        <div class="alert alert-success" role="alert">
        <?php echo $exito; ?>
        </div>
    <?php endif; ?>

    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                Actualización de calificaciones
            </div>
            <div class="card-body">
                <form class="form" method="post" action="<?php echo base_url('Escolar/actualizaCalificaciones'); ?>" enctype="multipart/form-data">
                    <div class="col-sm-12"  <?php echo $busqueda ?>>
                        <label for="matricula" class="form-label">Matrícula</label>
                        <input type="text" class="form-control" id="matricula" name="matricula" value="<?php echo $matricula; ?>"/>
                    </div>
                    <div class="col-sm-12">
                        <label for="nombre" class="form-label">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" disabled="disabled" value="<?php echo $nombre; ?>" />
                        <input type="hidden" class="form-control" id="confirma" name="confirma" value="<?php echo $matricula; ?>" />
                        <input type="hidden" class="form-control" id="nombre_id" name="nombre_id" value="<?php echo $nombre; ?>" />
                    </div>

                    <div  <?php echo $desplegar ?>>
                        <div class="col-sm-12"  >
                            <label for="nofolio" class="form-label">Folio de Oficio</label>
                            <input type="text" class="form-control" id="nofolio" name="nofolio" value="<?php echo $nofolio; ?>" />
                        </div>
                        <div class="col-sm-12"  >
                            <label for="seccion" class="form-label">Sección - Materia</label>
                            <select class="form-select" name="seccion">
                                <option>Selecciona sección - materia</option>
                                <?php foreach($secciones as $seccion): ?>
                                    <option value="<?php echo $seccion->gpo_grupo ?>" <?php if($seccion===$seccion->gpo_grupo) echo 'selected="selected"'; ?>>
                                        <?php echo $seccion->grupo_clave.' - '.$seccion->materia_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
    
                        <div class="col-sm-12" >
                            <label for="calificacion" class="form-label">Calificación</label>
                            <input type="text" class="form-control" id="calificacion" name="calificacion" value="<?php echo $calificacion; ?>" />
                        </div>
    
                        <div class="col-sm-12"  >
                            <label for="tipo" class="form-label">Tipo</label>
                            <select class="form-select" name="tipo">
                                <option value="ordinario" <?php if($tipo==='ordinario') echo 'selected="selected"'; ?>>Ordinario</option>
                                <option value="extraordinario" <?php if($tipo==='extraordinario') echo 'selected="selected"'; ?>>Extraordinario</option>
                            </select>
                        </div>
                        <div class="col-sm-12" >
                            <label for="comentario" class="form-label">Comentarios</label>
                            <input type="text" class="form-control" id="comentario" name="comentario" value="<?php echo $comentario; ?>" />
                        </div>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>MATRICULA</th>
                                    <th>NOMBRE</th>
                                    <th>FOLIO</th>
                                    <th>SECCION - MATERIA</th>
                                    <th>CALIFICACION</th>
                                    <th>TIPO</th>
                                    <th>COMENTARIOS</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                //foreach($arregloCalificaciones as $arreglo): 
                                    //var_dump($arreglo); die();
                               
                               foreach($arregloCalificaciones as $llave => $valor):?>
                                   
                                        <td><?php echo $arreglo[$i]; ?></td>
                              
                               <?php endforeach; ?>
                            </tbody>
                        </table>
                    <div>
                   
                    <div class="col-sm-12 text-center" style="padding-top: 20px;" >
                    <button type="submit" name="btnagrega" value="1" class="btn btn-secondary">
                            AGREGAR
                        </button>
                        <button type="submit" name="btnagrega" value="2" class="btn btn-secondary">
                            <?php echo $text; ?>
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    
</div>


<?php echo $this->endSection() ?>
