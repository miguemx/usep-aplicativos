<?php

namespace App\Models;
use CodeIgniter\Model;

class RegistrosModel extends Model {
    
    protected $table      = 'aplicativo_registros_biometrico';
    protected $primaryKey = 'registro_id';

    protected $returnType    = 'App\Entities\Registro';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'registro_id','registro_persona','registro_num','registro_fecha','registro_tipo',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
}
