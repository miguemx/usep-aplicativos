CREATE TABLE `usep_aplicativos`.`cat_clinicas_imss` ( `imss_id` VARCHAR(3) NOT NULL COMMENT 'ID de la clinica' , `imss_nombre` VARCHAR(60) NOT NULL COMMENT 'Nombre comun' , `imss_entidad` VARCHAR(60) NOT NULL , `imss_municipio` VARCHAR(60) NOT NULL , `imss_localidad` VARCHAR(60) NOT NULL ) ENGINE = InnoDB;

CREATE TABLE `usep_aplicativos`.`alumnos_seguro` ( `seguro_matricula` VARCHAR(9) NOT NULL , `seguro_reg_patrimonial` VARCHAR(11) NOT NULL , `seguro_ssn` VARCHAR(11) NOT NULL , `seguro_salario` VARCHAR(6) NOT NULL , `seguro_generico_6` VARCHAR(6) NOT NULL , `seguro_tipo_contratacion` VARCHAR(1) NOT NULL , `seguro_tipo_salario` VARCHAR(1) NOT NULL , `seguro_semana` VARCHAR(1) NOT NULL , `seguro_fecha` DATE NOT NULL , `seguro_umf` INT NOT NULL , `seguro_generico_2` VARCHAR(2) NOT NULL , `seguro_tipo_movimiento` VARCHAR(2) NOT NULL , `seguro_guia` VARCHAR(5) NOT NULL , `seguro_generico_1` VARCHAR(1) NOT NULL , `seguro_formato` VARCHAR(1) NOT NULL , PRIMARY KEY (`seguro_matricula`)) ENGINE = InnoDB;

ALTER TABLE `cat_clinicas_imss` ADD PRIMARY KEY(`imss_id`);


ALTER TABLE `alumnos_seguro` CHANGE `seguro_umf` `seguro_umf` VARCHAR(3) NOT NULL;

ALTER TABLE alumnos_seguro ADD CONSTRAINT fk_alumnosimss FOREIGN KEY (`seguro_umf`) REFERENCES cat_clinicas_imss(imss_id);

ALTER TABLE `alumnos_seguro` ADD `seguro_archivo` VARCHAR(250) NULL AFTER `seguro_formato`;


ALTER TABLE `alumnos_generales` ADD `alumno_nombres` VARCHAR(80) NOT NULL AFTER `alumno_nombre`, ADD `alumno_ap_paterno` VARCHAR(80) NOT NULL AFTER `alumno_nombres`, ADD `alumno_ap_materno` VARCHAR(80) NOT NULL AFTER `alumno_ap_paterno`;