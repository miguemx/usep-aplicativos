SELECT *  
FROM `aca_grupos` 
INNER JOIN aca_grupos_alumnos ON gpoalumno_grupo = grupo_id
INNER JOIN aca_materias ON grupo_materia = materia_clave
INNER JOIN alumnos_generales ON gpoalumno_alumno = alumno_id
WHERE `grupo_periodo` LIKE '2020A'  

ORDER BY `aca_grupos`.`grupo_clave` ASC, grupo_materia ASC, alumno_ap_paterno ASC, alumno_ap_materno ASC, alumno_nombres ASC