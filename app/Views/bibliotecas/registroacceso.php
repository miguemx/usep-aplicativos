<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container ">
    <div class="text-center mt-5">
        <h2>Acceso a biblioteca</h2>
        <h5>
            
        </h5>
        <?php if( isset($tipo) ): ?>
            <div class="alert alert-success">
                <div>
                    <?php if( $tipo == 'entrada' ): ?>
                        <img src="<?php echo base_url('img/icons/access_in.png'); ?>" width="100" alt="Ok" />
                    <?php else: ?>
                        <img src="<?php echo base_url('img/icons/access_out.png'); ?>" width="100" alt="Ok" />
                    <?php endif; ?>
                </div>
                Acceso de <strong><?php echo $tipo; ?></strong> correcto.
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?php if( $datos['foto'] !== null ): ?>
                        <img src="<?php echo $datos['foto']; ?>" width="200" alt="Foto" />
                    <?php else: ?>
                        <img src="<?php echo base_url('img/icons/user_gray_big.png'); ?>" width="200" alt="USER" />
                    <?php endif; ?>
                </div>
                <div class="col-sm-8">
                    <table class="table table-striped">
                        <tr>
                            <th>ID o Matrícula</th>
                            <td><?php echo $datos['id'] ?></td>
                        </tr>
                        <tr>
                            <th>Nombre</th>
                            <td><?php echo $datos['nombre'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        <?php else: ?>
            <div class="alert alert-danger">
                <div>
                    <img src="<?php echo base_url('img/icons/wrong.png'); ?>" width="100" alt="ERROR" />
                </div>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>

        <div>
            Puede cerrar esta pestaña.
        </div>
    </div>
</div>

<?php echo $this->endSection() ?>