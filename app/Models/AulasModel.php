<?php

namespace App\Models;

use CodeIgniter\Model;

class AulasModel extends Model
{
    protected $table      = 'aca_aulas';
    protected $primaryKey = 'aula_id';

    protected $returnType    = 'App\Entities\Aulas';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function getAulas()
    {
        $this->where('aula_id !=', '1');
        $this->where('aula_id !=', 'SM');
        $this->where('aula_id !=', 'VES');
        $this->orderBy('aula_nombre', 'ASC');
        return $this->findAll();
    }

    /**
     * retorna el listado de aulas ordenado por ID
     * @param bandera (true)  visibles
     * @param bandera (false)  disponibles
     */
    public function getAulasId($bandera = false)
    {
        $this->where('aula_id !=', '1');
        // var_dump($bandera);
        if ($bandera) {
            $this->where('aula_visible', '1');
        } else {
            $this->where('aula_disponible', '1');
        }

        $this->orderBy('aula_id', 'ASC');
        return $this->findAll();
    }
    /**
     * obtiene un conjunto de espacios físicos de la la tabla, pero descarta los id que se encuentren en el array "descarta"
     */
    public function getSelAulas()
    {
        $descarta = array('1', '105', '106', '107', '108', '109', '2', '205', '205-B', 'AUD', 'PREO', 'SM', 'VES');
        $this->whereNotIn('aula_id', $descarta);
        $this->orderBy('aula_id', 'ASC');
        return $this->findAll();
    }

    /**
     * return listado de aulas 
     */
    public function aulasMapa()
    {
        $this->where('aula_id !=', '1');
        $this->where('aula_id !=', 'SM');
        $this->where('aula_id !=', 'VES');
        $this->orderBy('aula_id', 'ASC');
        return $this->findAll();
    }

    /**
     * retorna un arreglo de allas filtradas 
     */
    public function getAulasFiltradas($filtros = array())
    {
        return $this->whereIn('aula_id', $filtros)->find();
    }
}
