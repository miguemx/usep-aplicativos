<?php
namespace App\Controllers;


use App\Models\GrupoModel;
use App\Models\AlumnosModel;
use App\Models\GrupoAlumnoModel;
use App\Models\EmpleadosModel;

use App\Entities\Alumno;
use App\Entities\Usuario;
use App\Entities\GrupoAlumno;
use App\Entities\Empleado;
use App\Entities\Grupo;

class InscripcionReinscripcion extends BaseController {

    public function MuestraBloques() {
        $bloquesGrupoModel = new GrupoModel();
        $bloquesAlumnosModel = new AlumnosModel();
        $bloquesAlumnosGruposModel = new GrupoAlumnoModel();
        $bloquesEmpleadosModel =new EmpleadosModel();
        $idAlumno=$this->sesion->get('id');
        $semAlumno=$this->sesion->get('semestre');
        $alumnosIDCS=$this->asignaAlumnoIDC($idAlumno);
        $BGM=$this->BloquesGrupoModel->FormaBloques($alumnosIDCS);
         return view( 'alumnos/calificaciones', $variables );
       
    }
}