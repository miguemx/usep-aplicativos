<?php
namespace App\Models;
use CodeIgniter\Model;

class AspiranteDatosModel extends Model {
    protected $table      = 'view_aspirantes_full';
    protected $primaryKey = 'aspirante_id';

    protected $returnType    = 'App\Entities\AspiranteFull';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca un aspirante mediante su direccion de correo
     * @param correo el correo que se va a buscar
     * @return AspiranteFull entidad de Alumno con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByEmail($correo) {
        $aspirante = $this->where('aspirante_correo',$correo)->first();
        return $aspirante;
    }

    /**
     * busca un aspirante mediante su CURP
     * @param correo el correo que se va a buscar
     * @return AspiranteFull entidad de Alumno con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByCurp($curp) {
        $aspirante = $this->where('aspirante_curp',$correo)->first();
        return $aspirante;
    }

    /**
     * devuelve todos los registros de aspirantes de acuerdo al criterio
     * @param filtros los filtros de la consulta
     * @param page la pagina que desea devolver
     * @param numero el numero de registros a devolver
     * @return aspirantes la lista de los aspirantes seleccionados
     */
    public function getAll($filtros = null, $page=1, $numero=10) {
        $limit = 500;

        if ( array_key_exists('folio', $filtros ) && $filtros['folio'] !== '' ) {
            $this->where( 'aspirante_id', $filtros['folio'] );
        }
        if ( array_key_exists('curp', $filtros ) && $filtros['curp'] !== '' ) {
            $this->where( 'aspirante_curp', $filtros['curp'] );
        }
        if ( array_key_exists('apPaterno', $filtros ) && $filtros['apPaterno'] !== '' ) {
            $this->like( 'aspirante_ap_paterno', $filtros['apPaterno'] );
        }
        if ( array_key_exists('apMaterno', $filtros ) && $filtros['apMaterno'] !== '' ) {
            $this->like( 'aspirante_ap_materno', $filtros['apMaterno'] );
        }
        if ( array_key_exists('nombre', $filtros ) && $filtros['nombre'] !== '' ) {
            $this->like( 'aspirante_nombre', $filtros['nombre'] );
        }

        $builder = $this->where('aspirante_aceptado','1')->orderBy('aspirante_ap_paterno','ASC')->orderBy('aspirante_ap_materno','ASC');
        $aspirantes = ($limit>0)? $builder->findAll($limit): $builder->findAll();
        return $aspirantes;
    }

    /**
     * devuelve un arreglo formateado con los datos estadisticos del proceso
     */
    public function estadisticos() {
        $estados = $this->getEstados();
        $builder = $this->table( $this->table );
        $builder->select( '*' );
        $builder->orderBy( 'creacion', 'ASC' );
        $query = $builder->get();
        $rows = $query->getResult();
        $datos = [
            'total' => count( $rows ),
            'hombres' => 0, 'mujeres' => 0,
            'captura'=>0, 'documentos'=>0, 'revisados'=>0, 'exitosos'=>0, 'espera'=>0, 'rechazados'=>0,
            'estados' => $estados,
            'genero' => [
                'med' => [ 'hombres'=>0, 'mujeres'=>0  ],
                'enf' => [ 'hombres'=>0, 'mujeres'=>0  ],
            ],
            'proceso' => [
                'med' => [ 'h' => [ 'c'=>0, 'd'=>0, 'x'=>0, 'e'=>0, 'r'=>0 ], 'm' => [ 'c'=>0, 'd'=>0, 'x'=>0, 'e'=>0, 'r'=>0 ] ],
                'enf' => [ 'h' => [ 'c'=>0, 'd'=>0, 'x'=>0, 'e'=>0, 'r'=>0 ], 'm' => [ 'c'=>0, 'd'=>0, 'x'=>0, 'e'=>0, 'r'=>0 ] ]
            ]
        ];
        foreach( $rows as $row ) {
            $sexo = substr( $row->aspirante_curp, 10, 1 ); if($sexo!='M' && $sexo !='H') echo "$sexo<br />";
            if ( $sexo == 'M' ) $datos['mujeres'] ++;
            else $datos['hombres']++;
            if ( !$row->aspirante_estado ) $datos['estados']['sin-respuesta'] ++;
            else $datos['estados'][$row->aspirante_estado] ++;
            if ( $row->proceso_validacion == '1' && $row->proceso_captura == '0' ) {  // quienes estan capturando datos
                if( $sexo=='M' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['m']['c']++;
                if( $sexo=='H' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['h']['c']++;
                if( $sexo=='M' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['m']['c']++;
                if( $sexo=='H' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['h']['c']++;
            }
            if ( $row->proceso_captura == '1' && $row->proceso_documentos == '0' ) {  // quienes estan cargando documentos
                if( $sexo=='M' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['m']['d']++;
                if( $sexo=='H' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['h']['d']++;
                if( $sexo=='M' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['m']['d']++;
                if( $sexo=='H' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['h']['d']++;
            }
            if ( $row->proceso_exito == '1')  {  // quienes estan aceptados
                if( $sexo=='M' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['m']['e']++;
                if( $sexo=='H' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['h']['e']++;
                if( $sexo=='M' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['m']['e']++;
                if( $sexo=='H' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['h']['e']++;
            }
            if ( $row->proceso_rechazado == '1')  {  // quienes estan declinados
                if( $sexo=='M' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['m']['r']++;
                if( $sexo=='H' && $row->aspirante_carrera=='2' ) $datos['proceso']['med']['h']['r']++;
                if( $sexo=='M' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['m']['r']++;
                if( $sexo=='H' && $row->aspirante_carrera=='1' ) $datos['proceso']['enf']['h']['r']++;
            }

            if( $sexo=='M' && $row->aspirante_carrera=='2' ) $datos['genero']['med']['mujeres']++;
            if( $sexo=='H' && $row->aspirante_carrera=='2' ) $datos['genero']['med']['hombres']++;
            if( $sexo=='M' && $row->aspirante_carrera=='1' ) $datos['genero']['enf']['mujeres']++;
            if( $sexo=='H' && $row->aspirante_carrera=='1' ) $datos['genero']['enf']['hombres']++;
        }
        
        $datos['proceso']['med']['m']['x'] = $datos['genero']['med']['mujeres']-$datos['proceso']['med']['m']['c']-$datos['proceso']['med']['m']['d']-$datos['proceso']['med']['m']['e']-$datos['proceso']['med']['m']['r'];
        $datos['proceso']['med']['h']['x'] = $datos['genero']['med']['hombres']-$datos['proceso']['med']['h']['c']-$datos['proceso']['med']['h']['d']-$datos['proceso']['med']['h']['e']-$datos['proceso']['med']['h']['r'];
        $datos['proceso']['enf']['m']['x'] = $datos['genero']['enf']['mujeres']-$datos['proceso']['enf']['m']['c']-$datos['proceso']['enf']['m']['d']-$datos['proceso']['enf']['m']['e']-$datos['proceso']['enf']['m']['r'];
        $datos['proceso']['enf']['h']['x'] = $datos['genero']['enf']['hombres']-$datos['proceso']['enf']['h']['c']-$datos['proceso']['enf']['h']['d']-$datos['proceso']['enf']['h']['e']-$datos['proceso']['enf']['h']['r'];

        if ( $datos['proceso']['enf']['m']['x'] < 0  ) {
            $datos['proceso']['enf']['m']['d'] = $datos['proceso']['enf']['m']['d'] + $datos['proceso']['enf']['m']['x'];
            $datos['proceso']['enf']['m']['x'] = 0;
        }

        if ( $datos['proceso']['med']['m']['x'] < 0  ) {
            $datos['proceso']['med']['m']['d'] = $datos['proceso']['med']['m']['d'] + $datos['proceso']['med']['m']['x'];
            $datos['proceso']['med']['m']['x'] = 0;
        }

        return $datos;
    }

    /**
     * obtiene todos los estados de que se han recibido solicitudes
     */
    private function getEstados() {
        $builder = $this->table( $this->table );
        $builder->select( 'aspirante_estado' );
        $builder->distinct();
        $builder->orderBy('aspirante_estado','ASC');
        $query = $builder->get();
        $rows = $query->getResult();
        $estados = [];
        foreach( $rows as $row ) {
            if ( $row->aspirante_estado ) {
                $estados[$row->aspirante_estado] = 0;
            }
        }
        $estados['sin-respuesta'] = 0;
        return $estados;
    }

    /**
     * devuelve la sabana de informacion de todos los aspirantes
     */
    public function sabana() {
        $builder = $this->table( $this->table );
        $builder->orderBy( 'creacion', 'ASC' );
        $query = $builder->get();
        $rows = $query->getResultArray();
        return $rows;
    }

    /**
     * public function obtiene exitodos
     */
    public function exitosos() {
        $this->where( 'proceso_exito','1' );
        return $this->findAll();
    }

    /**
     * asigna un horario al aspirante designado
     * @param aspiranteId el ID o folio o matricula del aspirante
     * @param fecha la fecha de examen
     * @param hora la hora de examen
     * @return res true si se pudo aisgnar, false si no se pudo asignar
     */
    public function asignaHorario($aspiranteId, $fecha, $hora) {
        $sql = "UPDATE prein_aspirantes SET aspirante_examen_fecha='$fecha', aspirante_examen_hora='$hora' WHERE aspirante_id='$aspiranteId'";
        try {
            $query = $this->db->query( $sql );
            if ( $this->db->affectedRows() ) {
                return true;
            }
        }
        catch ( \Exception $ex ) {
            echo " ERROR: ".$ex->getMessage();
        }
        return false;
    }
}