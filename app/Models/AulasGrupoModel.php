<?php

namespace App\Models;

use CodeIgniter\Model;

class AulasGrupoModel extends Model
{
    protected $table      = 'aca_grupos_aulas';
    protected $primaryKey = 'gpoaula_id';

    protected $returnType    = 'App\Entities\AulasGrupos';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'gpoaula_grupo',
        'gpoaula_dia',
        'gpoaula_inicio',
        'gpoaula_fin',
        'gpoaula_aula',
        'gpoaula_periodo',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function getbyperiodo($periodo)
    {
        return $this->where('gpoaula_periodo', $periodo)->findAll();
    }
    public function gruposaulas($periodo, $dia)
    {
        $this->builder = $this->db->table($this->table);
        $this->builder->join('aca_aulas', 'gpoaula_aula=aula_id', 'inner');
        $this->builder->join('aca_grupos', 'gpoaula_grupo=grupo_id', 'inner');
        $this->builder->join('aca_materias', 'materia_clave=grupo_materia', 'inner');
        $this->builder->join('aplicativo_empleados', 'empleado_id=grupo_docente', 'inner');
        $this->builder->where('gpoaula_periodo', $periodo);
        $this->builder->where('gpoaula_dia', $dia);
        $query = $this->builder->get();
        return $query->getResult();
    }

    /**
     * @return query registro del grupo en la hora y dia seleccionado
     * @param grupo id del grupo
     * @param dia 
     */
    public function getGrupoHoraDia($grupo, $dia, $hora)
    {
        return $this->where('gpoaula_grupo', $grupo)->where('gpoaula_dia', $dia)->where('gpoaula_inicio', $hora)->find();
    }
    /**
     * @return query registro de un id determinado
     * @param id id del registro
     */
    public function getbyid1($id)
    {
        return $this->where('gpoaula_id', $id)->findFirst();
    }

    /**
     * regresa un registro considerando el dia, hora, lugar, descartando el id del registro de la tabla
     * @return query registro del grupo en la hora y dia seleccionado
     * @param grupo idc del grupo
     * @param dia 
     * @param aula 
     * @param id llave del registro a descartar 
     */
    public function getGrupoHoraDiaLugar($aula, $dia, $hora,$id,$periodo)
    {
        return $this->where('gpoaula_aula', $aula)
                    ->where('gpoaula_dia', $dia)
                    ->where('gpoaula_inicio', $hora)
                    ->where('gpoaula_id !=',$id)
                    ->where('gpoaula_periodo',$periodo)
                    ->find();
    }
    

    /**
     * retorna todos los horarios de un grupo en especifico
     * @param grupo ID del grupo
     */
    public function getByGrupo($grupo)
    {
        return $this->where('gpoaula_grupo', $grupo)->find();
    }
}
