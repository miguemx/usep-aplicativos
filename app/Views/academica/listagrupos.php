<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>
<div class="container ">
    <div class="bg-white rounded shadow p-2 mt-4 ml-2 mr-2">
        <div class="mb-3 d-flex justify-content-around">
            <label class="m-2">Periodo:<h4 class="card-title"> <?php echo $periodo ?></h4> </label>
            <label class="m-2">Aula:<h4 class="card-title"> <?php echo ($aula == 1) ? 'Aula virtual' : $aula ?></h4> </label>
            <label class="m-2">Dia:<h4 class="card-title"> <?php echo $dia  ?></h4> </label>
            <label class="m-2">Horario:<h4 class="card-title"> <?php echo $inicio . "-" . $fin  ?></h4> </label>
        </div>
    </div>
</div>
<div class="container-fluid">
    <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo'); ?>" method="post">

        <input type="hidden" name="select_periodo" value="<?php echo $periodo ?>">
        <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
        <div class="bg-white rounded shadow p-2 mt-4 ml-2 mr-2">
            <input type="hidden" name="posicion" value="<?php echo "$inicio|$fin|$aula|$dia|$periodo|$carrera" ?>">
            <div class="row">
                <div class="input-group mb-3 col">
                    <span class="input-group-text">Materia nombre</span>
                    <input name="mat_nombre" type="text" class="form-control" value="<?php echo $mat_nombre ?>">
                </div>
                <div class="input-group mb-3 col">
                    <span class="input-group-text">Materia clave</span>
                    <input name="mat_calve" type="text" class="form-control" value="<?php echo $mat_calve ?>">
                </div>
                <div class="input-group mb-3 col">
                    <span class="input-group-text">Seccion</span>
                    <input name="seccion" type="text" class="form-control" value="<?php echo $seccion ?>">
                </div>

            </div>
            <div class="row">
                <div class="input-group mb-3 col">
                    <span class="input-group-text">Docente nombre</span>
                    <input name="nombre" type="text" class="form-control" value="<?php echo $nombre ?>">
                    <span class="input-group-text">Primer apellido</span>
                    <input name="apPaterno" type="text" class="form-control" value="<?php echo $apPaterno ?>">
                    <span class="input-group-text">Segundo apellido</span>
                    <input name="apMaterno" type="text" class="form-control" value="<?php echo $apMaterno ?>">
                </div>
            </div>
            <div class="row">

                <div class="mb-3 col">
                    <div class="text-center">
                        <button type="submit" class="btn btn-secondary">Filtrar</button>
                        <button type="button" class="btn btn-secondary" onclick="buscar()">Buscar todos</button>
                        <button type="button" class="btn btn-secondary" onclick="cancelar()">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <div class="bg-white rounded shadow p-2 mt-4 ml-2 mr-2">
        <table class="table table-hover">
            <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                <tr>
                    <th>Materia</th>
                    <th>Seccion</th>
                    <th>Docente</th>
                    <th>Total de horas</th>
                    <th class="text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($grupos_periodo as $grupo) : ?>
                    <tr>
                        <td>

                            <?php echo "" . $grupo->materia . " " . $grupo->materiaNombre . " " . $grupo->periodo ?>
                        </td>
                        <td>
                            <?php echo $grupo->clave ?>
                        </td>
                        <td>
                            <?php echo mb_strtoupper($grupo->apPaterno . " " . $grupo->apMaterno . " " . $grupo->nombre_emp) ?>
                        </td>
                        <td>
                            <?php if (array_key_exists($grupo->grupo, $grupos_contador)) : ?>
                                <?php echo  $grupos_contador[$grupo->grupo] . "/" . $grupo->horas ?>
                            <?php else : ?>
                                <?php echo  "0/" . $grupo->horas ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <?php if (array_key_exists($grupo->grupo, $grupos_contador)) : ?>
                                <?php if ($grupos_contador[$grupo->grupo] < $grupo->horas) : ?>
                                    <button type="button" class="btn btn-secondary" onclick="asignar(this)" value="<?php echo $grupo->grupo . "|" . $aula . "|" . $inicio . "-" . $fin . "|" . $grupo->materiaNombre . "|" . $grupo->clave . "|" . $dia . "|" . $grupo->docente ?>">Asignar</button>
                                <?php endif; ?>
                            <?php else : ?>
                                <button type="button" class="btn btn-secondary" onclick="asignar(this)" value="<?php echo $grupo->grupo . "|" . $aula . "|" . $inicio . "-" . $fin . "|" . $grupo->materiaNombre . "|" . $grupo->clave . "|" . $dia . "|" . $grupo->docente ?>">Asignar</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-secondary" value="<?php echo $grupo->grupo . "|0|0|" . $grupo->materiaNombre . "|$aula|$carrera" ?>" onclick="editar(this)">Editar</button>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo'); ?>" method="post" id="buscar">
    <input type="hidden" name="posicion" value="<?php echo "$inicio|$fin|$aula|$dia|$periodo|$carrera" ?>">
    <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
    <input type="hidden" name="select_periodo" value="<?php echo $periodo ?>">
</form>
<form action="<?php echo base_url('ProgramacionAcademica/editarGrupoHora/'); ?>" method="post" id="form-editar">
    <input type="hidden" name="posicion" value="<?php echo "$inicio|$fin|$aula|$dia|$periodo|$carrera" ?>">
    <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
</form>
<form action="<?php echo base_url('ProgramacionAcademica/asignarHoraMateria'); ?>" method="post" id="form-asignar">
    <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
    <input type="hidden" name="select_periodo" value="<?php echo $periodo ?>">
</form>
<form action="<?php echo base_url('ProgramacionAcademica/' . (($bandera == 'AulaVirtual') ? 'AulaVirtual' : (($bandera == 'Home') ? "Home/$aula" : (($bandera == 'MapaAulas') ? "MapaAulas/$aula" : (($bandera == 'HorarioDocente') ? "HorarioDocente/" : 'Menu'))))); ?>" method="post" id="form-cancelar">
    <input type="hidden" name="select_periodo" value="<?php echo $periodo ?>">
</form>


<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function cancelar() {
        document.getElementById('form-cancelar').submit();
    }

    function buscar() {
        document.getElementById('buscar').submit();
    }

    function editar(grupo) {
        var contenedor = grupo.value.split('|');
        console.log(grupo.value);
        mensajes(('Seguro que desea editar el grupo ' + contenedor[3] + ', si sale de esta pantalla regresará al menu principal ¿Desea continuar? '), 'form-editar', grupo.value);
    }

    function asignar(idc) {
        var contenedor = idc.value.split('|');
        console.log(idc.value);
        mensajes(('Seguro que desea asignar el grupo ' + contenedor[3] + ' <br> de la seccion ' + contenedor[4] + ' al aula ' + ((contenedor[1] == 1) ? 'Aula Virtual' : contenedor[1]) + ' en el horario de ' + contenedor[2] + ' los dias ' + contenedor[5] + ' </br> ¿Desea continuar? '), 'form-asignar', idc.value);
    }

    function mensajes(mensaje_1, submit, datos) {
        Swal.fire({
            'title': 'Confirmación',
            'html': mensaje_1,
            'icon': 'question',
            confirmButtonColor: '#31BA31E3',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                console.log(datos)
                var form = document.getElementById(submit);
                var input = document.createElement('input');
                input.setAttribute('value', datos);
                input.hidden = true;
                input.name = 'grupo_detalles';
                form.appendChild(input);
                form.submit();

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#31BA31E3'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>