<?php

namespace App\Entities;

use CodeIgniter\Entity;

class FiltroReportes extends Entity
{
    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'=>null,
        'correo' => null,
        'matricula' => null,
        'fecha' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'reportes_id',
        'correo' => 'reportes_correo',
        'matricula' => 'reportes_matricula',
        'fecha' => 'reportes_fechacontagio',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
