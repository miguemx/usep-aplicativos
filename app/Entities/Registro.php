<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Registro extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'persona' => null,
        'num' => null,
        'fecha' => null,
        'tipo' => null,
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'         => 'registro_id',
        'persona'    => 'registro_persona',
        'num'       => 'registro_num',
        'fecha'     => 'registro_fecha',
        'tipo'   => 'registro_tipo',
    ];
    // protected $datamap = [
    //     'persona_id' => 'id'         ,
    //     'persona_horario' => 'horario'    ,
    //     'persona_area' => 'area'       ,
    //     'persona_nombre' =>'nombre'     , 
    //     'persona_apellido' =>'apellido'   
    // ];

    // protected $casts = [
    //     'options' => 'array',
    //             'options_object' => 'json',
    //             'options_array' => 'json-array'
    // ];

}
