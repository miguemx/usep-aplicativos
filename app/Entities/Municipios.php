<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Municipios extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'clave' => null,
        'nombre' => null,
        'abrev' => null,
        'activo' => null,
        'estado' => null,
   ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id_municipio' => 'id',
        'id_estado' => 'estado_id',
        'clave_municipio' => 'clave',
        'nombre_municipio' => 'nombre',
        'reg_activo' => 'activo',

        'estado' => 'id_estado',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];
}