<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .error {
        border: red 2px solid;
    }
</style>
<div class="container">
    <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="datos text-center">
            <div class="datos text-center">
                <label style="margin: 0 10px">Nombre:<h4 class="card-title"> <?php echo $formulario->nombre;  ?></h4> </label>
                <label style="margin: 0 10px">Orden:<h4 class="card-title"> <?php echo $formulario->orden  ?></h4> </label>
                <label style="margin: 0 10px">Instrucciones :<h4 class="card-title"> <?php echo (($formulario->instrucciones) ? $formulario->instrucciones : '&nbsp');  ?></h4> </label>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <?php if ($edicion) : ?>
        <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <form action="<?php echo base_url('EvaluacionDocenteController/CrearPregunta/' . $formulario->id) ?>" method="get" id="form_evaluacion">
                <div class="mb-3">
                    <label for="titulo_pregunta" class="form-label">Texto de la pregunta</label>
                    <input type="text" class="form-control" name="titulo_pregunta" id="titulo_pregunta" aria-describedby="help_titulo" placeholder="¿Hace uso de elemento audiovisuales para dar su clase?">
                </div>
                <div class="mb-3">
                    <label for="order_pregunta" class="form-label">Orden</label>
                    <input type="number" class="form-control" name="order_pregunta" id="order_pregunta" aria-describedby="ordertext" min=1 max=20 value="<?php echo count($contador_pregunta) + 1 ?>">
                    <small id="ordertext" class="form-text text-muted">Seleccione el orde que tendrá este formulario</small>
                </div>
                <div class="mb-3">
                    <label for="select_tipo" class="form-label">Tipo de Pregunta</label>
                    <select class="form-select " name="select_tipo" id="select_tipo">
                        <option value="0" selected>Seleccione una opccion disponible</option>
                        <?php foreach ($tipos as $tipo) : ?>
                            <option value="<?php echo $tipo->id ?>"><?php echo $tipo->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="select_tipo_clasificacion" class="form-label">Clasificación</label>
                    <select class="form-select " name="select_tipo_clasificacion" id="select_tipo_clasificacion">
                        <option value="0" selected>Seleccione una opccion disponible</option>
                        <?php foreach ($clasificaciones as $clasificacion) : ?>
                            <option value="<?php echo $clasificacion->id ?>"><?php echo $clasificacion->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>


                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="verificar('form_evaluacion','¿Seguro que desea generar una pregunta para  el formulario ' + '<?php echo $formulario->nombre;  ?>' + ' ?','Se generará una nueva pregunta para el formulario')">Guardar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/ContenidoFormulario/' . $formulario->id) ?>" role="button">Cancelar</a>

                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        </div>
    <?php else : ?>
        <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <form action="<?php echo base_url('EvaluacionDocenteController/EditarPregunta/' . $pregunta->id) ?>" method="get" id="form_edicion">
                <div class="mb-3">
                    <label for="titulo_pregunta_update" class="form-label">Texto de la pregunta</label>
                    <input type="text" class="form-control" name="titulo_pregunta_update" id="titulo_pregunta_update" aria-describedby="help_titulo" placeholder="¿Hace uso de elemento audiovisuales para dar su clase?" value="<?php echo $pregunta->titulo ?>">
                </div>
                <div class="mb-3">
                    <label for="order_pregunta_update" class="form-label">Orden</label>
                    <input type="number" class="form-control" name="order_pregunta_update" id="order_pregunta_update" aria-describedby="ordertext" min=1 max=20 value="<?php echo $pregunta->orden ?>">
                    <small id="ordertext" class="form-text text-muted">Seleccione el orde que tendrá este formulario</small>
                </div>
                <div class="mb-3">
                    <label for="select_tipo_update" class="form-label">Tipo de Pregunta</label>
                    <select class="form-select " name="select_tipo_update" id="select_tipo_update">
                        <option value="0" selected>Seleccione una opccion disponible</option>
                        <?php foreach ($tipos as $tipo) : ?>
                            <option value="<?php echo $tipo->id ?>" <?php echo (($tipo->id == $pregunta->tipo) ? 'selected' : '') ?>><?php echo $tipo->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="select_tipo_clasificacion_update" class="form-label">Clasificación</label>
                    <select class="form-select " name="select_tipo_clasificacion_update" id="select_tipo_clasificacion_update">
                        <option value="0" selected>Seleccione una opccion disponible</option>
                        <?php foreach ($clasificaciones as $clasificacion) : ?>
                            <option value="<?php echo $clasificacion->id ?>"<?php echo (($clasificacion->id == $pregunta->clasificacion) ? 'selected' : '') ?>><?php echo $clasificacion->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="mensaje('form_edicion','¿Seguro que desea editar la pregunta para  el formulario ' + '<?php echo $formulario->nombre;  ?>' + ' ?','Se modificado la pregunta para el formulario')">Guardar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/ContenidoFormulario/' . $formulario->id) ?>" role="button">Cancelar</a>

                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        </div>
    <?php endif; ?>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function verificar(submit, texto, confirmar) {
        var titulo = document.getElementById('titulo_pregunta');
        var orden = document.getElementById('order_pregunta');
        var tipo = document.getElementById('select_tipo');
        var calsificacion = document.getElementById('select_tipo_clasificacion');
        var bandera = false;
        if (titulo.value == '') {
            titulo.classList.add('error');
            bandera = true
        } else {
            titulo.classList.remove('error');
        }

        if (orden.value <= 0) {
            orden.classList.add('error');
            bandera = true
        } else {
            orden.classList.remove('error');
        }
        if (calsificacion.value == '0') {
            calsificacion.classList.add('error');
            bandera = true
        } else {
            calsificacion.classList.remove('error');
        }

        if (tipo.value == '0') {
            tipo.classList.add('error');
            bandera = true
        } else {
            tipo.classList.remove('error');
        }
        if (!bandera) {
            mensaje(submit, texto, confirmar);
        } else {
            Swal.fire({
                'title': 'Error',
                'text': 'Verifica los elementos marcados en rojo y vuelve a intentarlo',
                'icon': 'info',
                confirmButtonColor: '#840f31'
            });
        }

    }

    function mensaje(submit, texto, confirmar) {
        Swal.fire({
            'title': 'Confirmación',
            'html': texto,
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': confirmar,
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                document.getElementById(submit).submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>