<?php
namespace App\Models;
use CodeIgniter\Model;

class CapturaCalificacionesModel extends Model {
    protected $table      = 'view_calificaciones_edit';
    protected $primaryKey = 'alumno_id';

    protected $returnType    = 'App\Entities\CapturaCalificaciones_en';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }
    public function buscaPeriodos(){
        $consulta = $this->distinct()
                         ->findAll();
        return $consulta;
    }
    public function buscarMaterias($valores) {
        $consulta =$this->distinct('materia_nombre')
                        ->where('carrera_id',trim($valores))
                        //->where('grupo_periodo',$periodo)
                        ->findAll();
        return $consulta;
    }
    public function buscarGrupos($valores,$periodo) {

        $consulta = $this->distinct()
                         ->where('materia_nombre',trim($valores))
                         ->where('grupo_periodo',$periodo)->findAll();

        return $consulta;
    }
    //validar funcion para depurar
    public function buscarAlumnos($grupos,$periodo,$iddocente,$clavemateria) {
        var_dump($grupos."-".$periodo."_".$iddocente."_".$clavemateria);
        $consulta = $this->where('gpoalumno_grupo',trim($grupos))->where('grupo_periodo',$periodo)->where('grupo_docente',$iddocente)
        ->where('materia_clave',$clavemateria)->orderBy('alumno_ap_paterno','Asc')->orderBy('alumno_ap_materno','Asc')->orderBy('alumno_nombres','Asc')->findAll();
        var_dump($consulta);
        return $consulta;
    }
    public function buscaidcgrupo($idcgrupo){
        $consulta = $this->where('gpoalumno_grupo',$idcgrupo)->orderBy('alumno_ap_paterno','Asc')->orderBy('alumno_ap_materno','Asc')->orderBy('alumno_nombres','Asc')->findAll();
        return $consulta;
    }
    public function buscamateria($alumno){
        $consulta = $this->where('gpoalumno_alumno',$alumno)->orderBy('grupo_clave','Asc')->find();
        return $consulta;
    }
    public function buscagrupoJson($idcgrupo,$fecha){
        $consulta = $this->where('gpoalumno_grupo',$idcgrupo)->where( 'gpoalumno_fecha_modificacion',$fecha)->find();
        return $consulta;
    }
    public function buscamateriacursadas($alumno){
        $consulta = $this->where('gpoalumno_alumno',$alumno)->findAll();
        return $consulta;
    }
    //funcion para buscar alumnos en captura y actualizacion de calificaciones
    public function buscarAlumno($grupos,$periodo) {
        //var_dump($grupos."-".$periodo."_".$iddocente."_".$clavemateria);
        $consulta = $this->where('gpoalumno_grupo',trim($grupos))->where('grupo_periodo',$periodo)->orderBy('alumno_ap_paterno','Asc')->orderBy('alumno_ap_materno','Asc')->orderBy('alumno_nombres','Asc')->findAll();
        return $consulta;
    }
    /**
     * devuelve el conteo de alumnos por sección junto con la sección a la cual pertecen
     * @param periodo id del periodo a revisar
     * @param seccion sección a revisar
     * @return consulta arreglo con el número de alumnos, sección y periodo
     */
    public function n_alumnos_secciones($periodo,$seccion){
        $consulta = $this->select('COUNT(distinct gpoalumno_alumno) as n_alumnos')
                         ->select('grupo_clave')
                         ->select('grupo_periodo')
                         ->where('grupo_periodo',$periodo)
                         ->where('grupo_clave',$seccion)
                         ->groupBy('grupo_periodo')
                         ->groupBy('grupo_clave')
                         ->orderBy('grupo_clave')
                         ->first();
        return $consulta;
    }
    /**
     * devuelve el conteo de alumnos por Grupo junto con el grupo al cual pertecen siempre y cuando no sean tutorias
     * @param periodo id del periodo a revisar
     * @return consulta arreglo con el número de alumnos, sección y periodo
     */
    public function n_alumnos_grupos($periodo){
        $consulta = $this->select('COUNT(distinct grupo_id) as n_grupos')
                         ->select('gpoalumno_alumno')
                         ->where('grupo_periodo',$periodo)
                         ->where('materia_creditos > 0')
                         ->groupBy('gpoalumno_alumno')
                         ->orderBy('gpoalumno_alumno')
                         ->findAll();
        return $consulta;
    }

}