<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container">
    <div class="mt-2">
        <?php if ($mensaje) : ?>
            <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
                <?php echo $mensaje['texto']; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif; ?>
        <?php if ($eliminacion) : ?>
            <?php  ?>
            <?php if ($eliminacion[0]) : ?>
                <div class="alert alert-dismissible fade show alert-success" role="alert">
                    <?php foreach ($eliminacion[0] as $correctos) : ?>
                        <?php foreach ($correctos as $mensaje) : ?>
                            <?php echo ($mensaje); ?><br>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php endif; ?>
            <?php if ($eliminacion[1]) : ?>
                <div class="alert alert-dismissible fade show alert-danger" role="alert">
                    <?php foreach ($eliminacion[1] as $incorrecto) : ?>
                        <?php foreach ($incorrecto as $mensaje) : ?>
                            <?php echo $mensaje; ?><br>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">

        <div class="table-responsive-xl">
            <table class="table table-striped-columns table-hover table-borderless align-middle">
                <thead class="table-light">
                    <caption>Evaluacion Docente</caption>
                    <tr>
                        <th>Titulo</th>
                        <th>Periodo</th>
                        <th>Estatus</th>
                        <th>Tipo</th>
                        <th>Evaluado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    <?php if ($evaluaciones) : ?>
                        <?php foreach ($evaluaciones as $evaluacion) : ?>
                            <tr>
                                <td scope="row"><?php echo $evaluacion->titulo ?></td>
                                <td><?php echo $evaluacion->periodo ?></td>
                                <td><?php echo $evaluacion->estatus ?></td>
                                <td><?php foreach ($tipos_evaluacion as  $tipo) {
                                        if ($tipo->id == $evaluacion->tipo) {
                                            echo $tipo->nombre;
                                        }
                                    }  ?> </td>
                                <td>
                                    <?php foreach ($roles as  $rol) {
                                        if ($rol->id == $evaluacion->rol) {
                                            echo $rol->nombre;
                                        }
                                    }  ?>
                                </td>
                                <td width="100px">
                                    <div class="d-flex justify-content-end">
                                        <?php if ($evaluacion->estatus == 'CERRADO') : ?>
                                            <div>
                                                <button value="<?php echo  $evaluacion->id . '|' . $evaluacion->estatus . '|' . $evaluacion->titulo  . '|' . $evaluacion->periodo ?>" type="button" class="btn btn-primary mx-1" onclick="activar(this,true,false)">Activar</button>
                                            </div>
                                            <div>
                                                <button value="<?php echo  $evaluacion->id . '|' . $evaluacion->estatus . '|' . $evaluacion->titulo  . '|' . $evaluacion->periodo ?>" type="button" class="btn btn-primary mx-1" onclick="activar(this,false,true)">Guardar</button>
                                            </div>

                                        <?php else : ?>
                                            <div>
                                                <button value="<?php echo  $evaluacion->id . '|' . $evaluacion->estatus . '|' . $evaluacion->titulo  . '|' . $evaluacion->periodo ?>" type="button" class="btn  btn-danger mx-1" onclick="activar(this,true,false)">Cerrar</button>
                                            </div>

                                            <div>
                                                <button value="<?php echo  $evaluacion->id . '|' . $evaluacion->estatus . '|' . $evaluacion->titulo  . '|' . $evaluacion->periodo ?>" type="button" class="btn btn-primary mx-1" onclick="activar(this,true,true)">Cerrar y Guardar</button>
                                            </div>

                                        <?php endif ?>
                                        <div>
                                            <a class="btn btn-secondary  mx-1" href="<?php echo base_url('EvaluacionDocenteController/Formularios/' . $evaluacion->id) ?>">Ver</a>
                                        </div>
                                        <div>
                                            <a class="btn btn-secondary;  mx-1 btn btn-success" href="<?php echo base_url('EvaluacionDocenteController/reporteRespuestasExcel/' . $evaluacion->id) ?>">Exportar</a>
                                        </div>
                                        <div>
                                            <div class="dropdown">
                                                <button class="btn btn-default btn-sm " type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                                                        <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                                    </svg>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="<?php echo base_url('EvaluacionDocenteController/ModificarEvaluacion/' . $evaluacion->id) ?>">Editar</a></li>
                                                    <li>
                                                        <button type="button" class="dropdown-item" onclick="validar(this)" value="<?php echo $evaluacion->id . '|' . $evaluacion->titulo ?>">Eliminar</button>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="4">
                                <div class="text-center">
                                    No hay evaluaciones registradas
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>

                </tbody>
                <tfoot>
                    <form action="<?php echo base_url('EvaluacionDocenteController/GenerarEvaluacion') ?>" method="post">
                        <div class="m-3">
                            <button type="submit" class="btn btn-secondary">Generar Nueva Evaluacion Docente</button>
                        </div>
                    </form>
                </tfoot>
            </table>
        </div>

    </div>

</div>
<form action="<?php echo base_url('EvaluacionDocenteController/EliminarEvaluacion/') ?>" method="post" id="form_eliminar">

</form>
<form action="<?php echo base_url('EvaluacionDocenteController/CambiarEstatus/') ?>" method="post" id="form_status">

</form>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function activar(id,bandera_status, bandera_documento) {
        console.log(id.value);
        var datos = id.value.split('|');
        var mensaje = '';
        var confirm = '';
        if(bandera_status == true){
            if (datos[1] == 'CERRADO') {
                mensaje = ' Esta por activar la evaluación "' + datos[2] + '" para el periodo ' + datos[3] + ', una vez activada los usuarios podrán visualizar y contestar dicha evaluación, <br>¿Está seguro de que desea continuar?'
                confirm = ' Se ha activado la evaluación "' + datos[2] + '".'
            } else if(datos[1] == 'ACTIVO') {
                mensaje = ' Esta por cerrar la ' + datos[2] + ' para el periodo ' + datos[3] + ', una vez cerrada los usuarios no podrán visualizar ni contestar dicha evaluación, <br>¿Está seguro de que desea continuar?'
                confirm = ' Se ha cerrrado la evaluación "' + datos[2] + '".'
            }
        }
        if (bandera_documento == true) {
            mensaje  += '<br> Se generará un documento compilatorio de las estadisticas de la evaluacion seleccionada';
            confirm  += '<br> Espere mientras se genera el registro de la evaluacion, este proceso puede tardar unos minutos ¡No ciere esta pestaña hasta que termine el proceso! ' + datos[2] ;
        }
        Swal.fire({
            'title': 'Confirmación',
            'html': mensaje,
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'html': confirm,
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });

                var form = document.getElementById('form_status');
                var input = document.createElement('input');
                input.value = datos[0];
                input.name = 'evaluacion';
                input.hidden = true;
                form.appendChild(input);
                var input = document.createElement('input');
                input.value = datos[1];
                input.name = 'estado_actual';
                input.hidden = true;
                form.appendChild(input);

                var input = document.createElement('input');
                input.value = bandera_documento;
                input.name = 'bandera_documento';
                input.hidden = true;
                form.appendChild(input);

                var input = document.createElement('input');
                input.value = bandera_status;
                input.name = 'bandera_status';
                input.hidden = true;
                form.appendChild(input);

                form.submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun cambio.',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }

    function validar(elemento) {
        // console.log('aqiuu');
        var evaluacion = elemento.value.split('|');
        Swal.fire({
            'title': 'Confirmación',
            'html': '¿Seguro que desea eliminar la evaluacion "' + evaluacion[1] + '"?',
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': 'Se ha eliminado la evaluacion "' + evaluacion[1] + '"?',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                var form = document.getElementById('form_eliminar');
                var input = document.createElement('input');
                input.value = evaluacion[0];
                input.name = 'evaluacion';
                input.hidden = true;
                form.appendChild(input);
                form.submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>

<?php echo $this->endSection() ?>