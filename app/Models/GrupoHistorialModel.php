<?php

namespace App\Models;

use CodeIgniter\Model;

class GrupoHistorialModel extends Model{
    protected $table      = 'view_grupos_historial';
    protected $primaryKey = '';

    protected $returnType    = 'App\Entities\Grupo';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'grupo_id', 'grupo_clave', 'grupo_folio', 'grupo_materia', 'grupo_docente',
        'grupo_periodo', 'grupo_lunes', 'grupo_lunes_fin', 'grupo_martes', 'grupo_martes_fin',
        'grupo_miercoles', 'grupo_miercoles_fin', 'grupo_jueves', 'grupo_jueves_fin', 'grupo_viernes',
        'grupo_sabado', 'grupo_sabado_fin', 'grupo_domingo', 'grupo_domingo_fin', 'grupo_ocupado', 
        'grupo_flag_acta','grupo_fechacaptura','grupo_fechaimpresion','grupo_historial_acta','historial_id',
        'historial_idc_grupo','historial_json_acta','historial_json_alumnos','historial_fecha_capturada',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function busca($filtros)
    {
        $this->creaFiltros($filtros);
        $grupos = $this->findAll();
        return $grupos;
    }
    /**
     * crea los filtros where para la busqueda de grupos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('materia', $filtros)) { // -- filtrar por materia
            if (strlen($filtros['materia'])) {
                $this->where('grupo_materia', $filtros['materia']);
            }
        }
        if (array_key_exists('seccion', $filtros)) { // -- filtrar por seccion
            if (strlen($filtros['seccion'])) {
                $this->where('grupo_clave', $filtros['seccion']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['periodo'])) {
                $this->where('grupo_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['docente'])) {
                $this->where('grupo_docente', $filtros['docente']);
            }
        }
        if (array_key_exists('idcgrupo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['idcgrupo'])) {
                $this->where('grupo_id', $filtros['idcgrupo']);
            }
        }
    }

}


?>