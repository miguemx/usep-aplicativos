<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\BitacoraAulasView;
class BitacoraAulasViewModel extends Model
{
    protected $table="view_bitacora_aulas";
    protected $primarykey=" ";
    protected $returntype="App/Entities/BitacoraAulas";
    protected $allowedFields=['gpoaula_aula','gpoaula_grupo','gpoaula_dia','gpoaula_inicio','gpoaula_fin','aula_nombre',
                              'grupo_docente','grupo_materia','grupo_periodo',];
    protected $db;
    protected $builder;


    public function buscaAula($docente,$h_inicio,$dia,$periodo){
        $consulta = $this->where('grupo_docente',$docente)->where('gpoaula_inicio',$h_inicio)->where('gpoaula_dia',$dia)->where('grupo_periodo',$periodo)->find();
        return $consulta;
    }
    
}
