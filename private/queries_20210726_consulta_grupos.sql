SELECT * 
FROM aca_grupos_alumnos
INNER JOIN aca_grupos ON gpoalumno_grupo=grupo_id
INNER JOIN alumnos_generales ON gpoalumno_alumno=alumno_id
WHERE gpoalumno_grupo IN (

SELECT grupo_id  FROM `aca_grupos` WHERE `grupo_clave` LIKE 'MED-1%' AND `grupo_periodo` LIKE '2021A' ORDER BY `grupo_clave`  ASC
)  
ORDER BY `aca_grupos_alumnos`.`gpoalumno_grupo` ASC