<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Estados;

class EstadosModel extends Model
{
    protected $table="estados";
    protected $primaryKey="id";
    protected $returnType='App\Entities\Estados';
    protected $allowedFields=['id','clave','nombre','abrev','activo'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * devuelve todos los estados en un arreglo donde la llave es el Id del estado
     * @return estados el arreglo con los estados
     */
    public function arreglo() {
        $edos = $this->findAll();
        $estados = [
            0 => 'EXTRANJERO'
        ];
        foreach ( $edos as $edo ) {
            $estados[ $edo->id ] = $edo->nombre;
        }
        return $estados;
    }

}
?>