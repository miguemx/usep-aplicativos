<?php

namespace App\Controllers;

use App\Models\AspiranteProcedenciaModel;
use App\Models\AspiranteRespuestasModel;
use App\Models\AspirantesCamposModel;
use App\Models\AspirantesGeneralesModel;
use App\Models\AspirantesEtapasModel;
use App\Models\EstadisticaRegionesModel;
use App\Models\AspirantesTiposCampoModel;
use App\Models\AspirantesEstadisticaRegDetModel;
use App\Models\AspirantesEstadisticaStatusDetModel;
use App\Models\AspirantesEstadisticaValDetModel;



/**
 * 
 */
class EstadisticasAdmision extends BaseController
{
    private $EstadisticasRegionesModel;
    private $procedenciaModel;
    private $respuestasModel;
    private $tipocampoModel;
    private $camposModel;
    private $aspirantesModel;
    private $AspirantesEstadisticaRegDetModel;
    //private $AspEstadStatusDetModel;

    public function __construct()
    {
        $this->procedenciaModel = new AspiranteProcedenciaModel();
        $this->EstadisticaRegionesModel = new EstadisticaRegionesModel();
        $this->respuestasModel = new AspiranteRespuestasModel();
        $this->tipocampoModel = new AspirantesTiposCampoModel();
        $this->camposModel = new AspirantesCamposModel();
        $this->aspirantesModel = new AspirantesGeneralesModel();
        $this->AspirantesEstadisticaRegDetModel = new AspirantesEstadisticaRegDetModel();
        //$this->$AspEstadStatusDetModel = new AspirantesEstadisticaStatusDetModel();
    }

    public function index()
    {
        return $this->MenuEstadisticas();
    }
    /**
     * Despliega vista de menu estadisticas
     */
    public function estadisticas()
    {
        $data['etapas'] = [];
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/estadisticas', $data);
    }
    /**
     * funcion para mostrar los datos de estadísticas por tipo de aspirante (NUEVO INGRESO/REVALIDACIÓN)
     */
    public function estadisticatipoaspirante()
    {
        $ingreso = $this->request->getPost('ingreso');
        $ban = $this->request->getPost('ban');
        $revalidacion = $this->request->getPost('revalidacion');
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $data = $this->request->getPost();
        if (!count($data)) {
            $data = ['sexo' => '', 'carrera' => '', 'todos' => ''];
            $aspirantes = [];
        } else {
            $aspirantes = $aspirantesgenerales->conteoaspirantes($data);
        }
        $numero = null;
        $conteohombre = null;
        $conteomujer = null;
        if (is_Array($aspirantes)) {
            $ingreso = $ban;
            foreach ($aspirantes as $aspirante) {
                $numero = $numero + 1;
                if ($aspirante->sexo == 'H') {
                    $conteohombre = $conteohombre + 1;
                }
                if ($aspirante->sexo == 'M') {
                    $conteomujer = $conteomujer + 1;
                }
            }
            $data['etapas'] = $aspirantes;
        } else {
            if ($ban != null) {
                $ingreso = $ban;
            }
            $numero = $aspirantes;
            $data['etapas'] = [];
        }
        $data['ingreso'] = $ingreso;
        $data['hombre'] = $conteohombre;
        $data['mujer'] = $conteomujer;
        $data['numero'] = $numero;
        $data['pager'] = $aspirantesgenerales->pager;
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/estadisticatipoaspirante', $data);
    }
    /**
     * Despliega la vista del estatus estadísticos de aspirantes por filtro  
     */
    public function estatusregistro()
    {
        $estatus = $this->request->getPost('status');
        $numero = null;
        $conteohombre = null;
        $conteomujer = null;
        $aspirantes = [];
        $data = $this->request->getPost();
        $nombreetapa = $this->request->getPost('nombreetapa');
        $data['nombreetapa'] = $estatus;
        if ($estatus == "Validacion de correo") {
            $data['validacorreo'] = 1;
            $data['general'] = 1;
        }
        if ($nombreetapa == "Validacion de correo") {
            $validacioncorreo = $this->validacioncorreo($data);
            $numero = $validacioncorreo["numero"];
            $conteohombre = $validacioncorreo["conteohombre"];
            $conteomujer = $validacioncorreo["conteomujer"];
            $data['validacorreo'] = 1;
            $data['nombreetapa'] = $nombreetapa;
            $data['general'] = 1;
        }
        $data['numero'] = $numero;
        $data['hombre'] = $conteohombre;
        $data['mujer'] = $conteomujer;
        $data['nombreestatus'] = $estatus;
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/estadisticaestatus', $data);
    }
    /**
     * @param data corresponde al nombre de la etapa
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    private function validacioncorreo($data)
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $numero = null;
        $conteohombre = null;
        $conteomujer = null;
        if (isset($data[0]->todos)) {
            $aspirantes = $aspirantesgenerales->findAll();
        } elseif ($data != null) {
            $aspirantes = $aspirantesgenerales->estatusregistro($data);
        }
        if (is_array($aspirantes)) {
            foreach ($aspirantes as $aspirante) {
                $numero = $numero + 1;
                if ($aspirante->sexo == 'H') {
                    $conteohombre = $conteohombre + 1;
                }
                if ($aspirante->sexo == 'M') {
                    $conteomujer = $conteomujer + 1;
                }
            }
            $datos = [
                'conteohombre' => $conteohombre,
                'conteomujer' => $conteomujer,
                'numero' => $numero
            ];
        }
        return $datos;
    }
    /**
     * despliega vista para la revision de datos de municipios de procedencias
     */
    public function AspirantesMunicipios()
    {
        $data['metropolitanos'] = count($this->procedenciaModel->getMetropolinatos());
        $data['interior'] = count($this->procedenciaModel->getInterior());
        $data['otros_estados'] = count($this->procedenciaModel->getOtrosEstados());
        $data['extranjeros'] = count($this->procedenciaModel->getExtranjeros());
        $data['total'] = count($this->procedenciaModel->asArray()->findAll());
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/estadisticamunicipios', $data);
    }
    /**
     * crea la vista de la estadistica por regiones
     * 
     */
    public function estadisticaregiones()
    {   
        $data['menu'] = $this->sesion->get('menu');
        $data['estadistica'] = $this->EstadisticaRegionesModel->conteoxregiones();
        $data['est_region_sum'] = $this->AspirantesEstadisticaRegDetModel->conteodetxregiones();
        $data['foraneo_m_enf'] = $this->aspirantesModel->conteo_foraneo ('1','M','MÉXICO');
        $data['foraneo_h_enf'] = $this->aspirantesModel->conteo_foraneo ('1','H','MÉXICO');
        $data['foraneo_m_med'] = $this->aspirantesModel->conteo_foraneo ('2','M','MÉXICO');
        $data['foraneo_h_med'] = $this->aspirantesModel->conteo_foraneo ('2','H','MÉXICO');
        $data['ext_m_enf'] = $this->aspirantesModel->conteo_foraneo ('1','M','EXTRANJERO');
        $data['ext_h_enf'] = $this->aspirantesModel->conteo_foraneo ('1','H','EXTRANJERO');
        $data['ext_m_med'] = $this->aspirantesModel->conteo_foraneo ('2','M','EXTRANJERO');
        $data['ext_h_med'] = $this->aspirantesModel->conteo_foraneo ('2','H','EXTRANJERO');
        
        return view('admision/estadisticaregiones', $data);
    }


    public function AspirantesDiscapacidades()
    {
        $campos = $this->tipocampoModel->find(16);
        $catalogo = explode(',', $campos->valores);
        $respuestas = $this->respuestasModel->getDiscapacidades();
        foreach ($respuestas as $respuesta) {
            $valores[] = $respuesta['respuesta_valor'];
        }
        $arr_contadores = array_count_values($valores);
        $contadores = [];
        foreach ($catalogo as $categoria) {
            if (array_key_exists($categoria, $arr_contadores)) {
                $contadores[] =   $arr_contadores[$categoria];
            } else {
                $contadores[] = 0;
            }
        }
        $data['total'] = count($this->procedenciaModel->asArray()->findAll());
        $data['opciones'] = $catalogo;
        $data['contadores'] = $contadores;
        return view('admision/estadisticadiscapacidades', $data);
    }
    //-----------------------------------------------------seccion de prueba
    /**
     * Despliga el menu de los estadisticos de los apirantes
     */
    public function MenuEstadisticas()
    {
        $data['campos'] = $this->tipocampoModel->getPorCalsificacion('COLECCION');
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/menu_estadisticas', $data);
    }
    /**
     * funcion que despliega el menu para la seleccion de los datos a graficar
     */
    public function EstadisticosAspirantes()
    {
        $data['campos'] = $this->tipocampoModel->getPorCalsificacion('COLECCION');
        $data['menu'] = $this->sesion->get('menu');
        return view('admision/estadistico_colecciones', $data);
    }
    /**
     * retorna arreglo de preguntas que coinciden con el tipo de dato
     */
    public function getCamposCorrespondientes($id_tipo)
    {
        if ($id_tipo) {

            $campos = $this->camposModel->getCamposByTipo($id_tipo);
            if ($campos) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($campos);
            } else {
                return null;
            }
        }
    }
    /**
     * @param id_campo corresponde al id del campo solicitado
     * @return arr_datos arreglo tipo JSON que contiene el conteo de respuestas y sus clasificaciones 
     * 
     */

    public function getArrayEstadisticos($id_campo)
    {
        if ($id_campo) {
            $datos = $this->respuestasModel->getRespuestasPorCampo($id_campo);
            if ($datos) {

                if ($datos[0]->clasificacion == 'COLECCION') {
                    $campos = $this->tipocampoModel->find($datos[0]->tipoCampo);
                    $catalogo = explode(',', $campos->valores);
                    foreach ($datos as $respuesta) {
                        $aspirante = $this->aspirantesModel->find($respuesta->aspirante);
                        if ($aspirante) {
                            if ($aspirante->estatus != 'REGISTRO') {
                                if ($aspirante->carrera == 1) {
                                    if ($aspirante->sexo == 'H') $carrera1hombres[] = $respuesta->respuesta_valor;
                                    elseif ($aspirante->sexo == 'M') $carrera1mujeres[] = $respuesta->respuesta_valor;
                                } elseif ($aspirante->carrera == 2) {
                                    if ($aspirante->sexo == 'H') $carrera2hombres[] = $respuesta->respuesta_valor;
                                    elseif ($aspirante->sexo == 'M') $carrera2mujeres[] = $respuesta->respuesta_valor;
                                }
                            }
                        }
                    }
                    $arr_datos = [
                        'cabecera' => $catalogo,
                        'hombres_leo' => $this->getCountArray($catalogo, array_count_values($carrera1hombres)),
                        'mujeres_leo' => $this->getCountArray($catalogo, array_count_values($carrera1mujeres)),
                        'hombres_med' => $this->getCountArray($catalogo, array_count_values($carrera2hombres)),
                        'mujeres_med' => $this->getCountArray($catalogo, array_count_values($carrera2mujeres)),
                        'titulo' => $datos[0]->nombreCampo
                    ];
                    $this->response->setHeader('Content-Type', 'application/json');
                    echo json_encode($arr_datos);
                    return $arr_datos;
                }
            } else {
                return null;
            }
        }
    }
    /**
     * @param catalogo arreglo de apuntadores correspondientes a los titulos de las columnas
     * @param arr_contadores arreglo de 
     */
    private function getCountArray($catalogo, $arr_contadores)
    {
        foreach ($catalogo as $categoria) {
            if (array_key_exists($categoria, $arr_contadores)) {
                $contadores[] =   $arr_contadores[$categoria];
            } else {
                $contadores[] = 0;
            }
        }
        return $contadores;
    }

    /**
     * @param tipo_nombre corresponde al nombre del tipo de dato
     * @return contadores arreglo de datos de los campos tipo calificacion en tipo JSON
     */
    public function getPromedio($tipo_nombre)
    {

        $respuestas = $this->respuestasModel->getRespuestaPorTipo($tipo_nombre);
        if ($respuestas) {
            $datos = [
                'HMED' => [0, 0, 0, 0, 0],
                'MMED' => [0, 0, 0, 0, 0],
                'HLEO' => [0, 0, 0, 0, 0],
                'MLEO' => [0, 0, 0, 0, 0],
            ];
            foreach ($respuestas as $respuesta) {
                $aspirante = $this->aspirantesModel->find($respuesta->aspirante);
                if ($aspirante) {
                    if ($aspirante->estatus != 'REGISTRO') {
                        if ($aspirante->carrera == 1) {
                            if ($aspirante->sexo == 'H') {
                                if ($respuesta->respuesta_valor == 10  && $respuesta->respuesta_valor >= 9.6) {
                                    $datos['HLEO'][0]++;
                                } elseif ($respuesta->respuesta_valor <= 9.5 && $respuesta->respuesta_valor >= 8.6) {
                                    $datos['HLEO'][1]++;
                                } elseif ($respuesta->respuesta_valor <= 8.5 && $respuesta->respuesta_valor >= 7.6) {
                                    $datos['HLEO'][2]++;
                                } elseif ($respuesta->respuesta_valor <= 7.5 && $respuesta->respuesta_valor >= 6.6) {
                                    $datos['HLEO'][3]++;
                                } elseif ($respuesta->respuesta_valor < 6.6) {
                                    $datos['HLEO'][4]++;
                                }
                            } elseif ($aspirante->sexo == 'M') {
                                if ($respuesta->respuesta_valor == 10  && $respuesta->respuesta_valor >= 9.6) {
                                    $datos['MLEO'][0]++;
                                } elseif ($respuesta->respuesta_valor <= 9.5 && $respuesta->respuesta_valor >= 8.6) {
                                    $datos['MLEO'][1]++;
                                } elseif ($respuesta->respuesta_valor <= 8.5 && $respuesta->respuesta_valor >= 7.6) {
                                    $datos['MLEO'][2]++;
                                } elseif ($respuesta->respuesta_valor <= 7.5 && $respuesta->respuesta_valor >= 6.6) {
                                    $datos['MLEO'][3]++;
                                } elseif ($respuesta->respuesta_valor < 6.6) {
                                    $datos['MLEO'][4]++;
                                }
                            }
                        } elseif ($aspirante->carrera == 2) {
                            if ($aspirante->sexo == 'H') {
                                if ($respuesta->respuesta_valor == 10  && $respuesta->respuesta_valor >= 9.6) {
                                    $datos['HMED'][0]++;
                                } elseif ($respuesta->respuesta_valor <= 9.5 && $respuesta->respuesta_valor >= 8.6) {
                                    $datos['HMED'][1]++;
                                } elseif ($respuesta->respuesta_valor <= 8.5 && $respuesta->respuesta_valor >= 7.6) {
                                    $datos['HMED'][2]++;
                                } elseif ($respuesta->respuesta_valor <= 7.5 && $respuesta->respuesta_valor >= 6.6) {
                                    $datos['HMED'][3]++;
                                } elseif ($respuesta->respuesta_valor < 6.6) {
                                    $datos['HMED'][4]++;
                                }
                            } elseif ($aspirante->sexo == 'M') {
                                if ($respuesta->respuesta_valor == 10  && $respuesta->respuesta_valor >= 9.6) {
                                    $datos['MMED'][0]++;
                                } elseif ($respuesta->respuesta_valor <= 9.5 && $respuesta->respuesta_valor >= 8.6) {
                                    $datos['MMED'][1]++;
                                } elseif ($respuesta->respuesta_valor <= 8.5 && $respuesta->respuesta_valor >= 7.6) {
                                    $datos['MMED'][2]++;
                                } elseif ($respuesta->respuesta_valor <= 7.5 && $respuesta->respuesta_valor >= 6.6) {
                                    $datos['MMED'][3]++;
                                } elseif ($respuesta->respuesta_valor < 6.6) {
                                    $datos['MMED'][4]++;
                                }
                            }
                        }
                    }
                }
            }
            $contadores = [
                'titulo' => $respuestas[0]->nombreCampo,
                'cabecera' => ['10', '9', '8', '7', 'menor de 7'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
            // $datos = $this->getPt($tipo_nombre);
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($contadores);
        } else return null;
    }
    public function algo($tipo_nombre)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $respuestas = $this->respuestasModel->getRespuestaPorTipo($tipo_nombre);
        $moda = $this->respuestasModel->obtenerModa();
        if ($respuestas) {
            $arr = [];
            foreach ($respuestas as $valor) {
                $arr[] = $valor->valor;
            }
            rsort($arr, SORT_NUMERIC); //Arreglo ordenado por califiacion mayor a menor
            $contadores = (array_count_values($arr)); //devuelve el conteo de repeticiones por cada calificacion
            $calificacion = 10;
            $arreglo_de_contador_calificaciones = [
                'otros' => 0
            ];
            $conteo = [];
            $categorias = [];
            $aux_contador = [];
            while ($calificacion >= 1) {
                // echo "<br>".$calificacion;
                if ($calificacion >= 7) {
                    if (array_key_exists("$calificacion", $contadores)) {
                        // echo " => ".$contadores["$calificacion"]."<br>";
                        $aux_contador["$calificacion"] = $contadores["$calificacion"];
                        $conteo[] = $contadores["$calificacion"];
                        $categorias[] = ["$calificacion"];
                    } else {
                        $aux_contador["$calificacion"] = 0;
                        $conteo[] = 0;
                        $categorias[] = ["$calificacion"];
                    }
                } else {
                    if (array_key_exists("$calificacion", $contadores)) {
                        $arreglo_de_contador_calificaciones["otros"] = $arreglo_de_contador_calificaciones["otros"] + $contadores["$calificacion"];
                    }
                }
                $calificacion = $calificacion - 0.1;
            }
            $conteo[] = $arreglo_de_contador_calificaciones["otros"];

            $contador_Arr = count($arr);
            sort($arr);
            $middleval = floor(($contador_Arr-1)/2);
            if($contador_Arr % 2) {
            $mediana = $arr[$middleval];
            } else {
            $low = $arr[$middleval];
            $high = $arr[$middleval+1];
            $mediana = (($low+$high)/2);
            }
            //var_dump("mediana".$mediana);
            $categorias[] = ["Menor de 7"];
            $arreglo_de_contador_calificaciones['category'] =  $categorias;
            $arreglo_de_contador_calificaciones['conteos'] =  $conteo;
            $arreglo_de_contador_calificaciones['mediana'] =  $mediana;
            arsort($aux_contador);
            $arreglo_de_contador_calificaciones['moda'] =  ['llave'=>array_key_first($aux_contador),'valor'=>array_shift($aux_contador)] ;
            
            echo json_encode($arreglo_de_contador_calificaciones);
            die();
        } else return null;
    }
    /**
     * @param tipo_nombre nombre del campo requerido para graficar
     * @return contadores retorna el arreglo de calificaciones por puntaje
     */
    public function getPt($tipo_nombre)
    {
        $respuestas = $this->respuestasModel->getRespuestaPorTipo($tipo_nombre);
        // var_dump(array_count_values($respuestas['valor']));
        // die();
        if ($respuestas) {
            $arr = [];
            foreach ($respuestas as $valor) {
                $arr[] = $valor->valor;
            }
            rsort($arr, SORT_NUMERIC); //Arreglo ordenado por califiacion mayor a menor
            $contadores = (array_count_values($arr)); //devuelve el conteo de repeticiones por cada calificacion
            // var_dump($contadores);
            // echo "<br>";
            $calificacion = 10;
            $arreglo_de_contador_calificaciones = [
                'otros' => 0
            ];
            while ($calificacion >= 1) {
                // echo "<br>".$calificacion;
                if ($calificacion >= 7) {
                    if (array_key_exists("$calificacion", $contadores)) {
                        // echo " => ".$contadores["$calificacion"]."<br>";
                        $arreglo_de_contador_calificaciones["$calificacion"] = $contadores["$calificacion"];
                    } else {
                        $arreglo_de_contador_calificaciones["$calificacion"] = 0;
                    }
                } else {
                    if (array_key_exists("$calificacion", $contadores)) {
                        $arreglo_de_contador_calificaciones["otros"] = $arreglo_de_contador_calificaciones["otros"] + $contadores["$calificacion"];
                    }
                }
                $calificacion = $calificacion - 0.1;
            }
            var_dump($arreglo_de_contador_calificaciones);
            return ($arreglo_de_contador_calificaciones);
        } else return null;
    }

    //-----------------------------------------------------seccion de prueba
    /**
     * @param nombre recibe el nombre de de la etapa para realizar la busqueda 
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    public function busquedaporetapa($nombre)
    {
        $etapasaspirantes = new AspirantesEtapasModel();
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $etapas = $etapasaspirantes->findAll();
        $Noetapa = null;
        $arregloinformacion = null;
        foreach ($etapas as $etapa) {
            if ($etapa->nombre == $nombre) {
                $Noetapa = $etapa->etapa;
                $arregloinformacion = $aspirantesgenerales->buscarporetapa($Noetapa);
            }else{
                $Noetapa = null;
            }
        }
/*         echo "etapa numero ". $Noetapa;
        var_dump($arregloinformacion); */

        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        if (is_array($arregloinformacion)) {
            foreach ($arregloinformacion as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => $nombre,
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * función que busca los registros de los aspirantes en estatus de revision
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    public function revision()
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $revision =  $aspirantesgenerales->revision();
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        if (is_array($revision)) {
            foreach ($revision as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => 'REVISIÓN DOCUMENTAL',
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * función que busca los registros de los aspirantes en estatus de observación
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    public function observacion()
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $observaciones =  $aspirantesgenerales->observaciones();
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        if (is_array($observaciones)) {
            foreach ($observaciones as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => 'OBSERVACIONES',
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * función que busca los registros de los aspirantes en estatus de aceptado
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    public function aceptado()
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $aceptados =  $aspirantesgenerales->validado();
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        if (is_array($aceptados)) {
            foreach ($aceptados as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => 'ACEPTADOS',
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * función que busca los registros de los aspirantes en estatus de declinado
     * @return datos arreglo de datos que contiene el conteo de hombres, mujeres y total de registros en tipo JSON
     */
    public function declinado()
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $declinado =  $aspirantesgenerales->declinado();
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        if (is_array($declinado)) {
            foreach ($declinado as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => 'REVISIÓN DOCUMENTAL',
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * función que realiza la busqueda general de los aspirantes
     * @return datos arreglo de datos que contiene el conteo general de los aspirantes en tipo JSON
     */
    public function total()
    {
        $datos = [
            'HMED' => [0, 0, 0, 0],
            'MMED' => [0, 0, 0, 0],
            'HLEO' => [0, 0, 0, 0],
            'MLEO' => [0, 0, 0, 0]
        ];
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $total =  $aspirantesgenerales->conteo_status_det();
        if (is_array($total)) {
            foreach ($total as $totalaspirante) {
                if ($totalaspirante->carrera == "1") {
                    if ($totalaspirante->sexo == 'H') {
                                                 if ($totalaspirante->validado == '1') {
                            $datos['HLEO'][0]++;
                        }else{
                            $datos['HLEO'][1]++;
                        } 
                        if ($totalaspirante->estatus == 'REVISION') {
                            $datos['HLEO'][0]++;
                        } elseif ($totalaspirante->estatus == 'OBSERVACIONES') {
                            $datos['HLEO'][1]++;
                        } elseif ($totalaspirante->estatus == 'ACEPTADO') {
                            $datos['HLEO'][2]++;
                        } elseif ($totalaspirante->estatus == 'DECLINADO') {
                            $datos['HLEO'][3]++;
                        }
                    }
                    if ($totalaspirante->sexo == 'M') {
                                                 if ($totalaspirante->validado == '1') {
                            $datos['MLEO'][0]++;
                        }else{
                            $datos['MLEO'][1]++;
                        } 
                        if ($totalaspirante->estatus == 'REVISION') {
                            $datos['MLEO'][0]++;
                        } elseif ($totalaspirante->estatus == 'OBSERVACIONES') {
                            $datos['MLEO'][1]++;
                        } elseif ($totalaspirante->estatus == 'ACEPTADO') {
                            $datos['MLEO'][2]++;
                        } elseif ($totalaspirante->estatus == 'DECLINADO') {
                            $datos['MLEO'][3]++;
                        }
                    }
                }
                if ($totalaspirante->carrera == "2") {
                    if ($totalaspirante->sexo == 'H') {
                                                 if ($totalaspirante->validado == '1') {
                            $datos['HMED'][0]++;
                        }else{
                            $datos['HMED'][1]++;
                        } 
                        if ($totalaspirante->estatus == 'REVISION') {
                            $datos['HMED'][0]++;
                        } elseif ($totalaspirante->estatus == 'OBSERVACIONES') {
                            $datos['HMED'][1]++;
                        } elseif ($totalaspirante->estatus == 'ACEPTADO') {
                            $datos['HMED'][2]++;
                        } elseif ($totalaspirante->estatus == 'DECLINADO') {
                            $datos['HMED'][3]++;
                        }
                    }
                    if ($totalaspirante->sexo == 'M') {
                                                 if ($totalaspirante->validado == '1') {
                            $datos['MMED'][0]++;

                        }else{
                            $datos['MMED'][1]++;
                        } 
                        if ($totalaspirante->estatus == 'REVISION') {
                            $datos['MMED'][0]++;
                        } elseif ($totalaspirante->estatus == 'OBSERVACIONES') {
                            $datos['MMED'][1]++;
                        } elseif ($totalaspirante->estatus == 'ACEPTADO') {
                            $datos['MMED'][2]++;
                        } elseif ($totalaspirante->estatus == 'DECLINADO') {
                            $datos['MMED'][3]++;
                        }
                    }
                }
            }
            $contadores = [
                'titulo' => 'Total',
                'cabecera' => ['REVISIÓN DOCUMENTAL', 'OBSERVACIONES', 'REGISTRO VALIDADO', 'REGISTRO DECLINADO'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($contadores);
        } else return null;
    }
    public function capturainfo()
    {
        $etapasaspirantes = new AspirantesEtapasModel();
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $etapas = $etapasaspirantes->buscaretapas();
        $Noetapa = null;
        foreach ($etapas as $etapa) {
            if ($etapa->nombre == "Captura de informacion") {
                $Noetapa = $etapa->etapa;
            }
        }
        $arregloinformacion = $aspirantesgenerales->buscarporetapa($Noetapa);

        $numero = null;
        $conteohombre = null;
        $conteomujer = null;
        if (is_array($arregloinformacion)) {
            foreach ($arregloinformacion as $info) {
                $numero = $numero + 1;
                if ($info->sexo == 'H') {
                    $conteohombre = $conteohombre + 1;
                }
                if ($info->sexo == 'M') {
                    $conteomujer = $conteomujer + 1;
                }
            }


            $datos = [
                'cabecera' => ['HOMBRES', 'MUJERES'],
                'valores' => [$conteohombre, $conteomujer],
                'titulo' => 'Captura de información'
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    /**
     * @return datos arreglo de datos tipo JSON con la cantidad de aspirantes y por clasificacion
     */
    public function getAspirantesNuevoIngreso()
    {
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0]
        ];
        $aspirantes = $this->aspirantesModel->findAll();
        if (is_array($aspirantes)) {
            foreach ($aspirantes as $info) {
                if ($info->sexo == 'H' & $info->carrera == "1") {
                    $datos['HLEO'][0]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "1") {
                    $datos['MLEO'][0]++;
                }
                if ($info->sexo == 'H' & $info->carrera == "2") {
                    $datos['HMED'][1]++;
                }
                if ($info->sexo == 'M' & $info->carrera == "2") {
                    $datos['MMED'][1]++;
                }
            }
            $datos = [
                'titulo' => 'REVISIÓN DOCUMENTAL',
                'cabecera' => ['ENFERMERÍA', 'MEDICINA'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
            ];
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($datos);
    }
    public function getAspirrantesValidados()
    {
        $aspirantesgenerales =  new AspirantesGeneralesModel();
        $aspirantes = $aspirantesgenerales->conteo_val_det();
        $datos = [
            'HMED' => [0, 0],
            'MMED' => [0, 0],
            'HLEO' => [0, 0],
            'MLEO' => [0, 0],
            'NOCON' =>[0, 0]
        ];
        $contador = null;
        if (is_array($aspirantes)) {
            foreach ($aspirantes as $totalaspirante) {
                $contador = $contador +1;
                if($contador == '1'){
                    $datos['NOCON'][1] = $totalaspirante->aspirante_id;
                }
                if($contador == '2'){
                    $datos['NOCON'][1]= $datos['NOCON'][1] + $totalaspirante->aspirante_id;
                }
                if($contador == '3'){
                    $datos['NOCON'][0] = $totalaspirante->aspirante_id;
                }
                if($contador == '4'){
                    $datos['HLEO'][0] = $datos['HLEO'][0] + $totalaspirante->aspirante_id;
                }
                if($contador == '5'){
                    $datos['MLEO'][0] =  $datos['MLEO'][0] + $totalaspirante->aspirante_id;
                }
                if($contador == '6'){
                    $datos['NOCON'][0] = $datos['NOCON'][0] + $totalaspirante->aspirante_id;
                }
                if($contador == '7'){
                    $datos['HMED'][0] = $datos['HMED'][0] + $totalaspirante->aspirante_id;
                }
                if($contador == '8'){
                    $datos['MMED'][0] = $datos['MMED'][0] + $totalaspirante->aspirante_id;
                }
            }
            $datos = [
                'titulo' => 'VALIDACIÓN DE CORREO',
                'cabecera' => ['VALIDADOS', 'NO VALIDADOS'],
                'hombres_leo' => $datos['HLEO'],
                'mujeres_leo' => $datos['MLEO'],
                'hombres_med' => $datos['HMED'],
                'mujeres_med' => $datos['MMED'],
                'sincontestar' => $datos['NOCON'],
            ];
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($datos);
        } else return null;
    }

    /**
     * crea la vista de la estadistica por estado de validado o no validado
     * 
     */
    
    public function estadisticavalidado()
    {   
        $data['menu'] = $this->sesion->get('menu');
        $data['estadistica'] = $this->aspirantesModel->conteo_validados();
        $data['estadistica_estatus'] = $this->aspirantesModel->conteo_status();
        return view('admision/estadisticavalidados', $data);
    }

    /**
     * crea la vista de la estadistica por estado de validado o no validado por género y también la vista de las gráficas del estatus del aspirante por carrera y género
     * 
     */
    public function estadisticavalidadodet()
    {   $AspEstadStatusDetModel = new AspirantesEstadisticaStatusDetModel();
        $AspEstadValDetModel = new AspirantesEstadisticaValDetModel();
        $data['menu'] = $this->sesion->get('menu');
        $data['estadistica'] = $AspEstadValDetModel->estadisticadetval();
        $data['est_st_det'] = $AspEstadStatusDetModel->estadisticadetstatus();
        return view('admision/estadisticavalestatusdet', $data);
    }
}
