<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Evaluacion extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'periodo' => null,
        'estatus' => null,
        'titulo' => null,
        'nombre' => null,
        'tipo' => null,
        'rol' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'evaluacion_id',
        'periodo' => 'evaluacion_periodo',
        'estatus' => 'evaluacion_estatus',
        'titulo' => 'evaluacion_titulo',
        'tipo' => 'evaluacion_tipo',
        'rol' => 'evaluacion_rol',
       
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
