DROP VIEW IF EXISTS view_aspirantes_campostodos;

CREATE VIEW view_aspirantes_campostodos AS 

SELECT 
	etapa_id, etapa_periodo, etapa_nombre, etapa_orden, etapa_formulario, etapa_responsable,
	formulario_id, formulario_nombre, formulario_titulo, formulario_orden, formulario_instrucciones,
    campo_id, campo_dependiente, campo_nombre, campo_descripcion, campo_orden, campo_obligatorio, campo_visualizar,
    tipocampo_id, tipocampo_nombre, tipocampo_valores, tipocampo_clasificacion,
    aspirantes_etapas.deleted_at,
    aspirantes_campos.deleted_at as campodeleted
FROM
	aspirantes_etapas 
    INNER JOIN aspirantes_formularios ON etapa_id=formulario_etapa
    INNER JOIN aspirantes_campos ON formulario_id=campo_formulario
    INNER JOIN aspirantes_tipos_campo ON tipocampo_id=campo_tipo
WHERE
	aspirantes_etapas.deleted_at IS NULL AND aspirantes_campos.deleted_at IS NULL
ORDER BY etapa_orden ASC, formulario_orden ASC, campo_orden ASC;