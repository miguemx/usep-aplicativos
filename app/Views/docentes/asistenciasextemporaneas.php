<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>

<div class="container ">
    <?php if (isset($mensajeok)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $mensajeok; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
        <div class="row ">
            <h1 class="text-center my-2">Asistencia Extemporánea</h1>
            <form action="<?php echo base_url('/docente/guardaasistenciasExtemporaneas'); ?>" method="post">
                <input hidden name="docente" type="" value="<?php echo $docente; ?>" />
                <div class="accordion accordion-flush mt-2" id="menu_estadisticcas">
                    <div class="row">
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">FECHA</span>
                            <input disabled name="fecha" type="date" id="fecha" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $fecha ?>">
                            <input  hidden name="fechayhora" type="text" id="dia" value="<?php echo $fechayhora ?>">
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">HORA</span>
                            <input disabled name="hora" type="text" id="hora" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $hora ?>">
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">GRUPO-CLAVE</span>
                            <input disabled name="grupo" type="text" id="grupo" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $grupo ?>">
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">AULA</span>
                            <input disabled name="aula" type="text" id="aula" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $aula ?>">
                        </div>
                        <br>
                        <br>
                    </div>
                    <div class="row">
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">MATERIA</span>
                            <input disabled name="materia" type="text" id="materia" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $materia ?>">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <input hidden name="idgrupo" type="number" id="idgrupo" value="<?php echo $idgrupo ?>">
                    <input hidden name="aula" type="text" id="aula" value="<?php echo $aula ?>">
                    <table class="table table-striped table-bordered">
                        <thead class="  " style="background-color: #84112c  ; color:white; border: 1px solid black">
                            <tr>
                                <th style="width: 100px; ">No.</th>
                                <th style="width: 180px; ">APELLIDO PATERNO</th>
                                <th style="width: 180px; ">APELLIDO MATERNO</th>
                                <th style="width: 180px; ">NOMBRE</th>
                                <th style="width: 125px; ">TIPO DE ASISTENCIA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $contador = 0;
                            foreach ($alumno as $alumnos) : ?>
                                <tr>
                                <?php echo "<td hidden ><input  name='idalumno[]' id='$alumnos->id_alumno' value='$alumnos->id_alumno' ></td>"; ?>
                                    <td><?php echo $contador = $contador + 1; ?></td>
                                    <td class="text-left"><?php echo $alumnos->alumno_ap_paterno?></td>
                                    <td><?php echo $alumnos->alumno_ap_materno?></td>
                                    <td><?php echo $alumnos->nombres?></td>
                                    <?php
                                        echo "<td style='text-align: center; vertical-align: middle;'>
                                                <select id='asistencia' class='custom-select' name='asistencia[]'>
                                                    <option value= 'ASISTENCIA' selected>ASISTENCIA</option>
                                                    <option value= 'INASISTENCIA'>INASISTENCIA</option>
                                                    <option VALUE='RETARDO'>RETARDO</option>
                                                    <option VALUE='FALTA JUSTIFICADA'>FALTA JUSTIFICADA</option>
                                                </select>
                                            </td>"?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="text-center mt-1">
                        <button type="submit" class="btn btn-secondary">Guardar</button>
                    </div>
                <div>
            </form>
        </div>
    </div>
<script>
    function habilitar(datos) {
        if (datos.checked){
            document.getElementById(datos.value).disabled = false;
            document.getElementById("btn2").hidden = false;
        } 
        if(datos.checked != true){
            document.getElementById(datos.value).disabled = true;
        }
    }
</script>
<?php echo $this->endSection() ?>