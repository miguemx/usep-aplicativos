<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<?php if ($mensaje) : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="text-center">
            <h3>Mapa de Prácticas Clínicas</h3>
        </div>
        <div>
            <div class="alert fade show alert-info" role="alert">
                <div id="text_periodo"></div>
            </div>
        </div>
        <form action="<?php echo base_url('/ProgramacionAcademica/PracticasClinicas') ?>" method="post" id="form-buscar">
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="select_periodo" class="form-label">Periodo</label>
                        <select class="form-select" name="select_periodo" id="select_periodo" onchange="getDescripccion(this)">
                            <option value="0">Seleccione un periodo</option>
                            <?php foreach ($periodos as $lista_periodo) : ?>
                                <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                            <?php endforeach; ?>
                        </select>
                        <small></small>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="select_carrera" class="form-label">Carrera</label>
                        <select class="form-select" name="select_carrera" id="select_carrera" <?php echo (($administrador) ? '' : 'disabled') ?> required>
                            <option value="0">Seleccione una opcion</option>
                            <?php foreach ($carreras as $carrera) : ?>
                                <option value="<?php echo $carrera->id ?>" <?php echo (($rol ==  $carrera->id) ? 'selected' : '') ?> <?php echo (($carrera_selected ==  $carrera->id) ? 'selected' : '') ?>><?php echo $carrera->nombre ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="selct_dia" class="form-label">Dia</label>
                        <select class="form-select" name="selct_dia" id="selct_dia">
                            <option value="0">Seleccione un dia</option>
                            <?php foreach ($dias as $dia) : ?>
                                <option value="<?php echo $dia ?>" <?php echo (($dia == $dia_selected) ? 'selected' : '') ?>><?php echo $dia ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" onclick="buscar_dia()">Buscar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<?php if ($dia_selected != '') : ?>
    <div class="container-fluid ">
        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <div class="table-responsive">

                <table class="table table-borderless  mt-3 text-center ">
                    <thead class="thead-default">
                        <tr>
                            <th scope="row">HORA</th>
                            <th colspan="20"><?php echo $dia_selected ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $hora = 7;
                        ?>

                        <?php while ($hora < 21) : ?>

                            <tr>
                                <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                                <?php if ($grupos) : ?>
                                    <td style="border: 2px #bdb994 solid;">
                                        <?php foreach ($grupos as $grupo) : ?>
                                            <div>
                                                <?php echo "$grupo->ma_nombre  <br> $grupo->em_nombre <br>"; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                            <?php $hora++; ?>
                        <?php endwhile; ?>


                    </tbody>
                </table>
            </div>


        </div>
    </div>
<?php endif; ?>



<!-- Modal -->
<div class="modal fade" id="listas_grupo" tabindex="-1" role="dialog" aria-labelledby="grupos" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lista de aula</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="main_body">
                <table class="table" hidden id="main_table">
                    <thead>
                        <tr id="head_table">
                            <th>Materia</th>
                            <th>Docente</th>
                            <th>Seccion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="body_table">

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    var listas_grupo = document.getElementById('listas_grupo');

    function buscar_dia() {
        let select = document.querySelectorAll('select');
        console.log(select);
        let bandera = true;
        select.forEach(element => {
            if (element.value == 0) bandera = false;
        });
        if (bandera) {
            alert('buscar datos');
            document.getElementById('form-buscar').submit();

        } else {
            alert('no selecciono nada');
        }

    }

    function getDescripccion(periodo) {
        let periodos = <?php echo json_encode($periodos) ?>;
        // console.log(periodo);
        // console.log(periodos);
        periodos.forEach(element => {
            if (element.id == periodo.value) {
                let texto = document.getElementById('text_periodo');
                switch (element.estado) {
                    case 'INICIO':
                        texto.innerHTML = 'El periodo seleccionado esta habilitado para realizar cambios de horarios y grupos';
                        break;
                    case 'INSCRIPCION':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para la modificacion de horarios y cambios de docentes';
                        break;
                    case 'CURSO':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'CALIFICACIONES':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'FINALIZADO':
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                }
            }
        });
    }
    let periodo_selected = document.getElementById('select_periodo');
    getDescripccion(periodo_selected);
</script>

<?php echo $this->endSection() ?>