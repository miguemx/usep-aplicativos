<?php

namespace App\Entities;

use CodeIgniter\Entity;

class ReporteCalificaciones extends Entity
{
    protected $attributes = [
        'matricula' => null,
        'ap_paterno' => null,
        'ap_materno' => null,
        'nombre' => null,
        'calificacion' => null,
        'letra' => null,
        'evaluacion' => null,
        'seccion' => null,
        'grupo' => null,
        'periodo' => null,
        'materia' => null,
        'materia_nombre' => null,
        'docente' => null,
        'semestre' => null,
        'docente_nom' => null,
    ];
    protected $datamap = [
        'matricula' => 'gpoalumno_alumno',
        'ap_paterno' => 'alumno_ap_paterno',
        'ap_materno' => 'alumno_ap_materno',
        'nombre' => 'alumno_nombres',
        'calificacion' => 'calificacion',
        'letra' => 'cal_letra',
        'evaluacion' => 'evaluacion',
        'seccion' => 'grupo_clave',
        'grupo' => 'grupo_idc',
        'periodo' => 'grupo_periodo',
        'materia' => 'materia_clave',
        'materia_nombre' => 'materia_nombre',
        'semestre' => 'materia_semestre',
        'carrera' => 'carrera_nombre',
        'docente' => 'empleado_id',
        'docente_nom' => 'empleado_nombre',

    ];
}
