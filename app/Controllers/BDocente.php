<?php
namespace App\Controllers;

use App\Models\AlumnosModel;
use \App\Models\UsuariosModel;

class BDocente extends BaseController {
    public function setSesion($email) {
		$usuariosModel = new UsuariosModel();
		$usuario = $usuariosModel->getSessionData( $email );

		if ( !is_null($usuario) ) {
			$this->sesion->set( 'useremail', $usuario['correo'] );
			$this->sesion->set( 'nombre', $usuario['nombre'] );
			$this->sesion->set( 'rol_id', $usuario['id_rol'] );
			$this->sesion->set( 'rol', $usuario['rol'] );
			$this->sesion->set( 'menu', $usuario['menu'] );
			$this->sesion->set( 'id', $usuario['id'] );
            return redirect()->to(base_url());
            die();
		}
		return 'El usuario no está registrado en el sistema o no cuenta con los permisos para acceder. Por favor contacte al administrador.';
	}
}