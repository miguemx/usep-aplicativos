<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .boton {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .boton button {
        width: 200px;
    }

    .container {
        margin-bottom: 20px;

    }

    .generales {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .contenedor-botones {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }
    .cabecera-general{
        margin: 10px 0;
    width: 100%;
    display: block;
    }
</style>
<br>
<?php if (!isset($banderaReporte)) : ?>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Generador de Reportes del Filtro Sanitario
            </div>
            <div class="card-body text-center">
                <form action="reporteCompleto" method="post">
                    <div class="container shadow py-2">
                        <div class="row justify-content-center">
                            <div class="col-lg-3 col-sm-6" style="margin-bottom: 30px;">
                                <label for="startDate">Fecha de Inicio de Busqueda</label>
                                <input id="startDate" class="form-control" type="date" name="fecha_reporte" required />

                            </div>
                            <div class="col-lg-3 col-sm-6" style="margin-bottom: 30px;">
                                <label for="endDate">Fecha de Fin de Busqueda</label>
                                <input id="endDate" class="form-control" type="date" name="fecha_reporte_fin" required />

                            </div>
                        </div>
                        <br>
                        <button class="btn btn-secondary" type="submit">Buscar Registros</button>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (isset($banderaReporte)) : ?>
    <div class="container">
        <div class="card ">
            <div class="card-header ">
                Registros del Filtro Sanitario
            </div>
            <div class="card-body">
                <div class="container generales">
                    <div>
                        <div style="width: 100%;">

                            <table class="table  table-responsive table-hover text-center generales" >
                                <thead class="thead-defoult">
                                    <tr>
                                        <td colspan="10" class="text-center">Datos Generales</td>
                                    </tr>
                                    <tr>
                                        <th><form action="generadorCSVcabeceraGeneral" method="post">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <input type="hidden" name="banderadescarga" value="todos">
                                            <button class="btn btn-secondary" type="submit">Descargar Resumen de Registros Totales</button>
                                        </form></th>
                                        <th><form action="generadorCSVgeneral" method="post">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <input type="hidden" name="banderadescarga" value="todos">
                                            <button class="btn btn-secondary" type="submit">Descargar Registros Totales</button>
                                        </form></th>
                                        
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        
                      
                        
                        
                        <table class="table  table-responsive table-hover text-center" style="width: 100%;">
                            <thead class="thead-defoult">
                                <tr>
                                    <td colspan="10" class="text-center">Hombres</td>
                                </tr>
                                <tr>
                                    <th>Registros totales</th>
                                    <th>Sintomas Mayores</th>
                                    <th>Sintomas Menores</th>
                                    <th>Primera Dosis</th>
                                    <th>Segunda Dosis</th>
                                    <th>Dosis de Refuerzo</th>
                                    <th style='width:130px'>Prefieren no decirlo</th>
                                    <th>Contagios</th>
                                    <th>No contagios</th>
                                    <th style='width:200px'>Cont con pers con COVID</th>
                                    <th>Descargar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $contadorhombres  ?></td>
                                    <td><?php echo $mayoreshombres  ?></td>
                                    <td><?php echo $menoreshombres  ?></td>
                                    <td><?php echo $primerahombres  ?></td>
                                    <td><?php echo $segundahombres  ?></td>
                                    <td><?php echo $tercerahombres  ?></td>
                                    <td><?php echo $cuartahombres  ?></td>
                                    <td><?php echo $contadorhombresfechapositiva?></td>
                                    <td><?php echo $contadorhombresfechanegativa?></td>
                                    <td><?php echo $contadorhombresfechacontacto?></td>
                                    <td class="contenedor-botones">
                                        <form action="generadorCSVcabeceraGeneral" method="post">
                                            <input type="hidden" name="banderadescarga" value="generalhombres">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <div class="boton_general">
                                                <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                            </div>
                                        </form>
                                        <form action="generadorCSVgeneral" method="post">
                                            <input type="hidden" name="banderadescarga" value="generalhombres">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <div class="boton_general" style="padding-left: 5px;">
                                                <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table  table-responsive table-hover text-center">
                            <thead class="thead-defoult">
                                <tr>
                                    <td colspan="10" class="text-center">Mujeres</td>
                                </tr>
                                <tr>
                                    <th>Registros totales</th>
                                    <th>Sintomas Mayores</th>
                                    <th>Sintomas Menores</th>
                                    <th>Primera Dosis</th>
                                    <th>Segunda Dosis</th>
                                    <th>Dosis de Refuerzo</th>
                                    <th style='width:130px'>Prefieren no decirlo</th>
                                    <th>Contagios</th>
                                    <th>No contagios</th>
                                    <th style='width:200px'>Cont con pers con COVID</th>
                                    <th>Descargar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $contadorMujeres  ?></td>
                                    <td><?php echo $mayoresMujeres  ?></td>
                                    <td><?php echo $menoresMujeres  ?></td>
                                    <td><?php echo $primeraMujeres  ?></td>
                                    <td><?php echo $segundaMujeres  ?></td>
                                    <td><?php echo $terceraMujeres  ?></td>
                                    <td><?php echo $cuartaMujeres  ?></td>
                                    <td><?php echo $contadormujeresfechapositiva  ?></td>
                                    <td><?php echo $contadorMujeresfechanegativa  ?></td>
                                    <td><?php echo $contadorMujeresfechacontacto  ?></td>
                                    <td class="contenedor-botones">
                                        <form action="generadorCSVcabeceraGeneral" method="post">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <input type="hidden" name="banderadescarga" value="generalmujeres">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <div class="boton_general">
                                                <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                            </div>
                                        </form>
                                        <form action="generadorCSVgeneral" method="post">
                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                            <input type="hidden" name="banderadescarga" value="generalmujeres">
                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                            <div class="boton_general" style="padding-left: 5px;">
                                                <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="generales">

                    <?php if ($contadorHombreAlumno > 0) : ?>
                        <div>
                            <div class="container">
                                <h5>Alumnos Hombres</h5>
                                <table class="table  table-responsive table-hover ">
                                    <thead class="thead-defoult">
                                        <tr>
                                            <th>Registros totales</th>
                                            <th>Sintomas Mayores</th>
                                            <th>Sintomas Menores</th>
                                            <th>Primera Dosis</th>
                                            <th>Segunda Dosis</th>
                                            <th>Dosis de Refuerzo</th>
                                            <th style='width:130px'>Prefieren no decirlo</th>
                                            <th>Contagios</th>
                                            <th>No contagios</th>
                                            <th style='width:200px'>Cont con pers con COVID</th>
                                            <th>Descargar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $contadorHombreAlumno  ?></td>
                                            <td><?php echo $mayoresHombreAlumno  ?></td>
                                            <td><?php echo $menoresHombreAlumno  ?></td>
                                            <td><?php echo $primeraHombreAlumno  ?></td>
                                            <td><?php echo $segundaHombreAlumno  ?></td>
                                            <td><?php echo $terceraHombreAlumno  ?></td>
                                            <td><?php echo $cuartaHombreAlumno  ?></td>
                                            <td><?php echo $fecha1HombreAlumno  ?></td>
                                            <td><?php echo $fecha2HombreAlumno  ?></td>
                                            <td><?php echo $fecha3HombreAlumno  ?></td>
                                            <td class="contenedor-botones">
                                                <form action="generadorCSVcabecera" method="post">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="banderadescarga" value="alumnoshombres">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general">
                                                        <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                    </div>
                                                </form>
                                                <form action="impresionCSV" method="post">
                                                    <input type="hidden" name="banderadescarga" value="alumnoshombres">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general" style="padding-left: 5px;">
                                                        <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($contadorMujeresAlumno > 0) : ?>
                        <div>
                            <div class="container">
                                <h5>Alumnos Mujeres</h5>
                                <table class="table  table-responsive table-hover ">
                                    <thead class="thead-defoult">
                                        <tr>
                                            <th>Registros totales</th>
                                            <th>Sintomas Mayores</th>
                                            <th>Sintomas Menores</th>
                                            <th>Primera Dosis</th>
                                            <th>Segunda Dosis</th>
                                            <th>Dosis de Refuerzo</th>
                                            <th style='width:130px'>Prefieren no decirlo</th>
                                            <th>Contagios</th>
                                            <th>No contagios</th>
                                            <th style='width:200px'>Cont con pers con COVID</th>
                                            <th>Descargar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $contadorMujeresAlumno  ?></td>
                                            <td><?php echo $mayoresMujeresAlumno  ?></td>
                                            <td><?php echo $menoresMujeresAlumno  ?></td>
                                            <td><?php echo $primeraMujeresAlumno  ?></td>
                                            <td><?php echo $segundaMujeresAlumno  ?></td>
                                            <td><?php echo $terceraMujeresAlumno  ?></td>
                                            <td><?php echo $cuartaMujeresAlumno  ?></td>
                                            <td><?php echo $fecha1MujeresAlumno  ?></td>
                                            <td><?php echo $fecha2MujeresAlumno  ?></td>
                                            <td><?php echo $fecha3MujeresAlumno  ?></td>
                                            <td class="contenedor-botones">
                                                <form action="generadorCSVcabecera" method="post">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="banderadescarga" value="alumnosmujeres">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general">
                                                        <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                    </div>
                                                </form>
                                                <form action="impresionCSV" method="post">
                                                    <input type="hidden" name="banderadescarga" value="alumnosmujeres">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general" style="padding-left: 5px;">
                                                        <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($contadorHombreEmpleados > 0) : ?>
                        <div>
                            <div class="container">
                                <h5>Tabajador USEP Hombres</h5>
                                <table class="table  table-responsive table-hover ">
                                    <thead class="thead-defoult">
                                        <tr>
                                            <th>Registros totales</th>
                                            <th>Sintomas Mayores</th>
                                            <th>Sintomas Menores</th>
                                            <th>Primera Dosis</th>
                                            <th>Segunda Dosis</th>
                                            <th>Dosis de Refuerzo</th>
                                            <th style='width:130px'>Prefieren no decirlo</th>
                                            <th>Contagios</th>
                                            <th>No contagios</th>
                                            <th style='width:200px'>Cont con pers con COVID</th>
                                            <th>Descargar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $contadorHombreEmpleados  ?></td>
                                            <td><?php echo $mayoresHombreEmpleados  ?></td>
                                            <td><?php echo $menoresHombreEmpleados  ?></td>
                                            <td><?php echo $primeraHombreEmpleados  ?></td>
                                            <td><?php echo $segundaHombreEmpleados  ?></td>
                                            <td><?php echo $terceraHombreEmpleados  ?></td>
                                            <td><?php echo $cuartaHombreEmpleados  ?></td>
                                            <td><?php echo $fecha1HombreEmpleados  ?></td>
                                            <td><?php echo $fecha2HombreEmpleados  ?></td>
                                            <td><?php echo $fecha3HombreEmpleados  ?></td>
                                            <td class="contenedor-botones">
                                                <form action="generadorCSVcabecera" method="post">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="banderadescarga" value="empleadoshombres">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general">
                                                        <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                    </div>
                                                </form>
                                                <form action="impresionCSV" method="post">
                                                    <input type="hidden" name="banderadescarga" value="empleadoshombres">
                                                    <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                    <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                    <div class="boton_general" style="padding-left: 5px;">
                                                        <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                        <?php if ($contadorMujeresEmpleados > 0) : ?>
                            <div>
                                <div class="container">
                                    <h5>Tabajador USEP Mujeres</h5>
                                    <table class="table  table-responsive table-hover ">
                                        <thead class="thead-defoult">
                                            <tr>
                                                <th>Registros totales</th>
                                                <th>Sintomas Mayores</th>
                                                <th>Sintomas Menores</th>
                                                <th>Primera Dosis</th>
                                                <th>Segunda Dosis</th>
                                                <th>Dosis de Refuerzo</th>
                                                <th style='width:130px'>Prefieren no decirlo</th>
                                                <th>Contagios</th>
                                                <th>No contagios</th>
                                                <th style='width:200px'>Cont con pers con COVID</th>
                                                <th>Descargar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $contadorMujeresEmpleados  ?></td>
                                                <td><?php echo $mayoresMujeresEmpleados  ?></td>
                                                <td><?php echo $menoresMujeresEmpleados  ?></td>
                                                <td><?php echo $primeraMujeresEmpleados  ?></td>
                                                <td><?php echo $segundaMujeresEmpleados  ?></td>
                                                <td><?php echo $terceraMujeresEmpleados  ?></td>
                                                <td><?php echo $cuartaMujeresEmpleados  ?></td>
                                                <td><?php echo $fecha1MujeresEmpleados  ?></td>
                                                <td><?php echo $fecha2MujeresEmpleados  ?></td>
                                                <td><?php echo $fecha3HombreEmpleados  ?></td>
                                                <td class="contenedor-botones">
                                                    <form action="generadorCSVcabecera" method="post">
                                                        <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                        <input type="hidden" name="banderadescarga" value="empleadosMujeres">
                                                        <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                        <div class="boton_general">
                                                            <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                        </div>
                                                    </form>
                                                    <form action="impresionCSV" method="post">
                                                        <input type="hidden" name="banderadescarga" value="empleadosMujeres">
                                                        <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                        <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                        <div class="boton_general" style="padding-left: 5px;">
                                                            <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                            <?php if ($contadorHombreOtros > 0) : ?>
                                <div>
                                    <div class="container">
                                        <h5>Externos Hombres</h5>
                                        <table class="table  table-responsive table-hover ">
                                            <thead class="thead-defoult">
                                                <tr>
                                                    <th>Registros totales</th>
                                                    <th>Sintomas Mayores</th>
                                                    <th>Sintomas Menores</th>
                                                    <th>Primera Dosis</th>
                                                    <th>Segunda Dosis</th>
                                                    <th>Dosis de Refuerzo</th>
                                                    <th style='width:130px'>Prefieren no decirlo</th>
                                                    <th>Contagios</th>
                                                    <th>No contagios</th>
                                                    <th style='width:200px'>Cont con pers con COVID</th>
                                                    <th>Descargar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $contadorHombreOtros  ?></td>
                                                    <td><?php echo $mayoresHombreOtros  ?></td>
                                                    <td><?php echo $menoresHombreOtros  ?></td>
                                                    <td><?php echo $primeraHombreOtros  ?></td>
                                                    <td><?php echo $segundaHombreOtros  ?></td>
                                                    <td><?php echo $terceraHombreOtros  ?></td>
                                                    <td><?php echo $cuartaHombreOtros  ?></td>
                                                    <td><?php echo $fecha1HombreOtros  ?></td>
                                                    <td><?php echo $fecha2HombreOtros  ?></td>
                                                    <td><?php echo $fecha3HombreOtros  ?></td>
                                                    <td class="contenedor-botones">
                                                        <form action="generadorCSVcabecera" method="post">
                                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                            <input type="hidden" name="banderadescarga" value="externoshombres">
                                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                            <div class="boton_general">
                                                                <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                            </div>
                                                        </form>
                                                        <form action="impresionCSV" method="post">
                                                            <input type="hidden" name="banderadescarga" value="externoshombres">
                                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                            <div class="boton_general" style="padding-left: 5px;">
                                                                <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($contadorMujeresOtros > 0) : ?>
                                <div>
                                    <div class="container">
                                        <h5>Externos Mujeres</h5>
                                        <table class="table  table-responsive table-hover ">
                                            <thead class="thead-defoult">
                                                <tr>
                                                    <th>Registros totales</th>
                                                    <th>Sintomas Mayores</th>
                                                    <th>Sintomas Menores</th>
                                                    <th>Primera Dosis</th>
                                                    <th>Segunda Dosis</th>
                                                    <th>Dosis de Refuerzo</th>
                                                    <th style='width:130px'>Prefieren no decirlo</th>
                                                    <th>Contagios</th>
                                                    <th>No contagios</th>
                                                    <th style='width:200px'>Cont con pers con COVID</th>
                                                    <th>Descargar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $contadorMujeresOtros  ?></td>
                                                    <td><?php echo $mayoresMujeresOtros  ?></td>
                                                    <td><?php echo $menoresMujeresOtros  ?></td>
                                                    <td><?php echo $primeraMujeresOtros  ?></td>
                                                    <td><?php echo $segundaMujeresOtros  ?></td>
                                                    <td><?php echo $terceraMujeresOtros  ?></td>
                                                    <td><?php echo $cuartaMujeresOtros  ?></td>
                                                    <td><?php echo $fecha1MujeresOtros  ?></td>
                                                    <td><?php echo $fecha2MujeresOtros  ?></td>
                                                    <td><?php echo $fecha3MujeresOtros  ?></td>
                                                    <td class="contenedor-botones">
                                                        <form action="generadorCSVcabecera" method="post">
                                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                            <input type="hidden" name="banderadescarga" value="externosMujeres">
                                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                            <div class="boton_general">
                                                                <button class="btn btn-secondary" type="submit">Descargar Resumen</button>
                                                            </div>
                                                        </form>
                                                        <form action="impresionCSV" method="post">
                                                            <input type="hidden" name="banderadescarga" value="externosMujeres">
                                                            <input type="hidden" name="fecha_reporte_fin" value="<?php echo $fecha_reporte_fin ?>">
                                                            <input type="hidden" name="fecha" value="<?php echo $fecha ?>">
                                                            <div class="boton_general" style="padding-left: 5px;">
                                                                <button class="btn btn-secondary" type="submit">Descargar Detalle</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    <?php endif; ?>
    <br>
    <?php echo $this->endSection() ?>