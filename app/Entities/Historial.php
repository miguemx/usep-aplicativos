<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Historial extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'idc' => null,
        'historico' => null,
        'alumnos' => null,
        'fecha' => null,
               
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'historial_id',
        'idc' => 'historial_idc_grupo',
        'historico' => 'historial_json_acta',
        'alumnos' => 'historial_json_alumnos',
        'fecha' => 'historial_fecha_capturada',        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
