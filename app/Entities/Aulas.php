<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Aulas extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id_aula' => NULL,
        'nombre_aula' => NULL,
        'descripcion' => NULL,
        'cupo_maximo' => NULL,
        'nombre_corto' => NULL,
        'visible' => NULL,
        'disponible' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
        'id_aula' => 'aula_id',
        'nombre_aula' => 'aula_nombre',
        'descripcion' => 'aula_descripccion',
        'cupo_maximo' => 'aula_cupo_mx',
        'nombre_corto' => 'aula_nombre_corto',
        'visible' => 'aula_disponible',
        'disponible' => 'aula_visible',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}