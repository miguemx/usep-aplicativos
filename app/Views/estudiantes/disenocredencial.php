<div style="width: 321px; height: 208px; background: url(<?php echo base_url('img/credencial/Fondo-01-small.png'); ?>); background-repeat: no-repeat; background-size: auto; font-family: Arial, Helvetica, sans-serif !important; font-size: 14px;">
    <table style="width: 321px">
        <tr>
            <td width="110" valign="top" style="padding-top: 5;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="height: 100px; padding-left: 7px;" >
                            <img src="<?php echo $foto ?>" style="vertical-align: middle; max-height: 90px; max-width: 90px;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #fff; margin-top: 5px; padding: 5px 7px; font-size: 7px; font-family: Arial, Helvetica, sans-serif">Vigencia: <?php echo $vigencia; ?></td>
                    </tr>
                    <tr>
                        <td style="color: #fff; padding: 0px 7px; font-size: 7px; font-family: Arial, Helvetica, sans-serif">
                            <img src="<?php echo $qrimage; ?>" alt="QR" style="max-height: 50px;" />
                        </td>
                    </tr>
                </table>
            </td>

            <td valign="top" style="font-family: Arial, Helvetica, sans-serif">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif">
                    <tr>
                        <td style="padding-top: 5px; height: 50px;">
                            <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; font-size: 12px; padding-top: 1px; ">
                            Estudiante
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888;  font-weight: bold; line-height: 1; width: 100px; height: 50px; ">
                            <?php echo $nombre; ?> 
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #8a0933; font-weight: bold; line-height: 1; padding-right: 5px; margin-top: 5px; font-size: 12px; height: 30px; ">
                            <?php echo $carrera; ?> 
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; line-height: 1; text-align: right; font-size: 10px; padding-right: 5px; padding-top: 5px;">
                            Matrícula
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #888; font-weight: bold; line-height: 1; text-align: right; font-size: 11px; padding-right: 5px">
                            <?php echo $matricula; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</div>

