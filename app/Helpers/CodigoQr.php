<?php  
namespace App\Helpers;

use \Endroid\QrCode\Builder\Builder;
use \Endroid\QrCode\Encoding\Encoding;
use \Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use \Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use \Endroid\QrCode\Label\Font\NotoSans;
use \Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use \Endroid\QrCode\Writer\PngWriter;

class CodigoQr {

    /**
     * Genera el codigo QR con la informacion proporcionada
     * @param info la información que alamacena el codigo QR
     * @param titulo (opcional) un título a colocar bajo el códgo QR
     * @return qr una cadena base64 con la imagen generada
     */
    public static function crea($info, $titulo='') {
        $result = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($info)
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->size(150)
            ->margin(0)
            ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->labelText($titulo)
            ->labelFont(new NotoSans(18))
            ->labelAlignment(new LabelAlignmentCenter())
            ->build();
        return $result->getDataUri();
    }

}