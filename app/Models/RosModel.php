<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Alumno;

class RosModel extends Model
{
    protected $table      = 'pagos_ros';
    protected $primaryKey = 'ros_id';

    protected $returnType    = 'App\Entities\Ros';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'ros_fecha_pago','ros_nombre','ros_concepto','ros_cuenta','ros_desc_cuenta','ros_importe',
        'ros_folio_inteligente','ros_referencia','ros_estatus_servicio','ros_fecha_otorgado','ros_usuario','ros_unidad_responsable',
        'ros_beneficiario','ros_cotejado','ros_matricula',  'updated_at', 'deleted_at'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    /**
     * busca los registros que coniciden con una referencia
     * @param referencia el numero de referencia
     * @return array con los resultados obtenidos; sera vacio si no se encuentra algun registro
     */
    public function findByReferencia($referencia) {
        $this->where('ros_referencia',$referencia);
        return $this->findAll();
    }

    /**
     * devuelve todos los pagos del ROS que no han sido cotejados
     */
    public function findNoCotejados() {
        $this->where( 'ros_cotejado', '0' );
        return $this->findAll();
    }

    /**
     * devuelve un objeto de pagoros de acuerdo a la referencia
     * @param referencia la referencia a buscar
     * @return object objeto con la referencia o null en caso de no existir
     */
    public function findReferencia($referencia) {
        $this->where( 'ros_referencia', $referencia );
        return $this->first();
    }

    /**
     * busca los grupos de acuerdo a los criterios de busqueda
     * @return grupos el listado de grupos encontrados
     */
    public function busca($filtros)
    {
        $this->creaFiltros($filtros);
        $grupos = $this->findAll();
        return $grupos;
    }

    /**
     * crea los filtros where para la busqueda de grupos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('materia', $filtros)) { // -- filtrar por materia
            if (strlen($filtros['materia'])) {
                $this->where('grupo_materia', $filtros['materia']);
            }
        }
    }
}
