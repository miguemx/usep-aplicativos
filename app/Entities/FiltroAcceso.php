<?php
namespace App\Entities;

use CodeIgniter\Entity;

class FiltroAcceso extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'            => null,
        'folio'         => null,
        'temperatura'   => null,
        'oximetria'     => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'filtroacceso_id',
        'folio'         => 'filtroacceso_folio',
        'temperatura'   => 'filtroacceso_temperatura',
        'oximetria'     => 'filtroacceso_oximetria',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
