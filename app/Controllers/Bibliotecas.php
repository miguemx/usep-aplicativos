<?php
namespace App\Controllers;

use App\Models\AlumnosModel;
use App\Models\EmpleadosModel;
use App\Helpers\AccesosBiblioteca;
use App\Helpers\CodigoQr;


class Bibliotecas extends BaseController {

    public function index() {
        $data['nombre'] = $this->sesion->get('nombre');
        $data['menu'] = $this->sesion->get('menu');
        if ( $this->sesion->get('rol') == 'ALUMNOS' ) {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find( $this->sesion->get('id') );
            if ( !is_null($alumno) ) {
                if ( !is_null($alumno->elseRegid) ) {
                    $data['elseRegid'] = $alumno->elseRegid;
                    $data['elseRegidpass'] = $alumno->elseRegidpass;
                    $data['ligaRegElsevier'] = 'https://www.clinicalkey.com/student/register';
                }
            }
        }
        else {
            $empleadosModel = new EmpleadosModel();
            $empleado = $empleadosModel->find( $this->sesion->get('id') );
            if ( !is_null($empleado) ) {
                if ( !is_null($empleado->elseRegid) ) {
                    $data['elseRegid'] = $empleado->elseRegid;
                    $data['elseRegidpass'] = $empleado->elseRegidpass;
                    $data['ligaRegElsevier'] = 'https://www.clinicalkey.com/student/register';
                }
            }
        }
        return view('bibliotecas/index', $data);
    }

    /**
     * 
     */
    public function acceso() {
        $esAlumno = ( $this->sesion->get('rol_id') === '4' )? true: false;
        $alumnosModel = new AlumnosModel();
        $empleadosModel = new EmpleadosModel();
        $user = null;
        if ( $esAlumno ) {
            $user = $alumnosModel->find( $this->sesion->get("id") );
        }
        else {
            $user = $empleadosModel->find( $this->sesion->get("id") );
        }

        if ( !is_null($user) ) {
            $accesoBiblioteca = new AccesosBiblioteca();
            $id = $accesoBiblioteca->crea( $user, $esAlumno );
            if ( $id ) {
                $link = base_url().'/E/bl/'.$id;
                $data['qr'] = CodigoQr::crea( $link );
                $data['menu'] = $this->sesion->get('menu');
            }
            else {
                $data['error'] = $accesoBiblioteca->getLastError();
            }
            return view( 'bibliotecas/acceso', $data );
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

}