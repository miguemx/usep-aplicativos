<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>


<div class="container-fluid row" >
    <form method="get" action="" class="gy-2 gx-3 align-items-center mb-3">
        <div class="row">
            <div class="col">
                <label class="visually-hidden" for="autoSizingInputGroup">Username</label>
                <div class="input-group">
                    <div class="input-group-text">Desde</div>
                    <input type="date" class="form-control form-control-sm" id="inicio" name="inicio" value="<?php echo $inicio; ?>" />
                </div>
            </div>
            <div class="col">
                <label class="visually-hidden" for="autoSizingInputGroup">Username</label>
                <div class="input-group">
                    <div class="input-group-text">Hasta</div>
                    <input type="date" class="form-control form-control-sm" id="fin" name="fin" value="<?php echo $fin; ?>" />
                </div>
            </div>
            <div class="col">
                <label class="visually-hidden" for="autoSizingInputGroup">Nombre</label>
                <div class="input-group">
                    <div class="input-group-text">Nombre</div>
                    <input type="text" class="form-control form-control-sm" id="nombre" name="nombre" value="<?php echo $nombre; ?>" placeholder="Busque por un solo nombre o apellido" />
                </div>
            </div>
            <!-- <div class="form-check col">
                <input class="form-check-input" type="checkbox" value="1" name="personal" id="personal" />
                <label class="form-check-label" for="personal">Default checkbox</label>
            </div>
            <div class="form-check col">
                <input class="form-check-input" type="checkbox" value="1" name="alumnos" id="alumnos" />
                <label class="form-check-label" for="alumnos">Default checkbox</label>
            </div>
            <div class="form-check col">
                <input class="form-check-input" type="checkbox" value="1" name="Externos" id="Externos" />
                <label class="form-check-label" for="externos">Default checkbox</label>
            </div> -->
            
        </div>
        <div class="mt-2 text-right">
            <a href="<?php echo base_url('Filtro/Consulta'); ?>" class="btn btn-secondary btn-sm">Mostrar todos</a>
            <button type="submit" class="btn btn-secondary btn-sm">Buscar</button>
        </div>
    </form>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Folio del QR</th>
                <th>Fecha de ingreso</th>
                <th>Tipo</th>
                <th>Sexo</th>
                <th>Temperatura</th>
                <th>Nombre</th>
                <th>Fecha de cuestionario</th>
            </tr>
        </thead>
        <tbody>
            <?php if( count($registros)<1 ): ?>
                <tr>
                    <td colspan="6" style="text-align: center;"><em>No se encontraron registros</em></td>
                </tr>
            <?php else: ?>
                <?php foreach($registros as $registro): ?>
                    <tr>
                        <td>
                            <a href="javascript:void(0)" onclick="expande('<?php echo $registro->id ?>')">
                                <?php echo $registro->id; ?>
                            </a>
                        </td>
                        <td><?php echo $registro->fechaAcceso; ?></td>
                        <td><?php
                            if ( $registro->empleado) echo 'TRABAJADOR USEP';
                            else if ( $registro->alumno) echo 'ESTUDIANTE USEP';
                            else echo 'EXTERNO';
                        ?>
                        <td><?php
                            if ( $registro->empleadoSexo) echo $registro->empleadoSexo;
                            else if ( $registro->alumnoSexo) echo $registro->alumnoSexo;
                            else echo $registro->externoSexo;
                        ?>
                        </td>
                        <td><?php echo $registro->temperatura; ?></td>
                        <td><?php
                            if ( $registro->empleado) echo $registro->empleadoNombre.' '.$registro->empleadoApPaterno.' '.$registro->empleadoApMaterno.' ('.$registro->empleado.')';
                            else if ( $registro->alumno) echo $registro->alumnoNombre.' '.$registro->alumnoApPaterno.' '.$registro->alumnoApMaterno.' ('.$registro->alumno.')';
                            else echo $registro->externoNombre;
                        ?>
                        </td>
                        <td><?php echo $registro->fechaCuestionario; ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="display: none;" id="fqr-<?php echo $registro->id; ?>">
                            <?php foreach($registro->respuestas as $respuesta): ?>
                                <div>
                                    <?php echo $respuesta->pregunta; ?>: <em><?php echo $respuesta->respuesta; ?></em>
                                </div>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?> 
        </tbody>    
    </table>

    <div class="pager-container">
        <?php echo $pager->links(); ?>
    </div>
</div>

<script type="text/javascript">
    function expande(id) {
        let elem = document.getElementById("fqr-"+id);
        if ( elem.style.display == 'none' ) {
            elem.style.display = 'table-cell';
        }
        else {
            elem.style.display = 'none';
        }
    }
</script>

<?php echo $this->endSection() ?>