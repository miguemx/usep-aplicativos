<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Horario extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'nombre' => null,
        'prorroga' => null,
        'limite' => null,
        'salidaAnticipada' => null,
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'               => 'horario_id',
        'nombre'           => 'horario_nombre',
        'prorroga'         => 'horario_prorroga',
        'limite'           => 'horario_limite',
        'salidaAnticipada' => 'horario_salida_anticipada',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
