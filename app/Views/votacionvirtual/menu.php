<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    .labels{
        width: 20%;
    }
</style>
<div class="container my-sm-5">
    <h1 style="display:flex; justify-content:center">VOTO ELECTRÓNICO</h1>
    <br>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/procesos'); ?>">
        <div class="proceso" style="display: table-cell; text-align: center;">
            <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">REGISTRO DE PROCESOS</button>
        </div>
    </form>
    <br>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/candidatos'); ?>">
        <div class="candidato" style="display: table-cell;  text-align: center;">
            <button type="submit" name='candidato' class="btn btn-secondary" style="left: 100px">REGISTRO DE CANDIDATOS</button>
        </div>
    </form>
    <br>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/padron'); ?>">
        <div class="padron" style="display: table-cell;  text-align: center;">
            <button type="submit" name='padron' class="btn btn-secondary" style="left: 100px">REGISTRO DE PADRON</button>
        </div>
    </form>
    <br>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/estadisticas'); ?>">
        <div class="estadisticas" style="display: table-cell;  text-align: center;">
            
            <button type="submit" name='estadisticas' class="btn btn-secondary" style="left: 100px">CONSULTA DE ESTADÍSTICAS</button>
        </div>
    </form>
</div>
<?php echo $this->endSection() ?>