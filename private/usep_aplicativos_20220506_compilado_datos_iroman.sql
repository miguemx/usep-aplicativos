CREATE TABLE `aspirantes_campos` (
  `campo_id` int UNSIGNED NOT NULL,
  `campo_formulario` int UNSIGNED NOT NULL,
  `campo_tipo` int UNSIGNED NOT NULL,
  `campo_dependiente` int UNSIGNED DEFAULT NULL COMMENT 'Indica si otro campo hace visible a esta campo',
  `campo_nombre` varchar(10001) NOT NULL,
  `campo_descripcion` text,
  `campo_orden` int UNSIGNED NOT NULL,
  `campo_obligatorio` int UNSIGNED DEFAULT NULL,
  `campo_visualizar` int NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;


INSERT INTO `aspirantes_campos` (`campo_id`, `campo_formulario`, `campo_tipo`, `campo_dependiente`, `campo_nombre`, `campo_descripcion`, `campo_orden`, `campo_obligatorio`, `campo_visualizar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, NULL, 'Acta de nacimiento', 'aqui va una descripcion mas adecuada', 1, 1, 0, '2022-03-25', '2022-03-29', '2022-03-29'),
(2, 2, 1, NULL, 'Titulo favorito', '', 1, 1, 0, '2022-03-28', '2022-03-29', '2022-03-29'),
(3, 3, 2, NULL, 'Escriba a cuanto asciende su ingreso familiar <strong>mensual</strong> en pesos mexicanos.', 'El ingreso familiar es la suma de las percepciones económicas de todos los integrantes de su hogar.', 1, 1, 0, '2022-03-29', '2022-05-03', NULL),
(4, 3, 6, NULL, 'Estado civil', '', 2, 1, 0, '2022-03-29', '2022-03-29', NULL),
(5, 3, 2, NULL, 'Número de hijos', '', 3, 1, 0, '2022-03-29', '2022-04-18', NULL),
(6, 3, 2, NULL, 'Número de dependientes económicos', '', 4, 1, 0, '2022-03-29', '2022-03-29', NULL),
(7, 3, 5, NULL, '¿Usted es padre o madre soltero(a)?', '', 5, 1, 0, '2022-03-29', '2022-03-29', NULL),
(8, 3, 2, NULL, 'Número de personas que viven en su hogar', '', 6, 1, 0, '2022-03-29', '2022-03-29', NULL),
(9, 3, 7, NULL, 'Tipo de vivienda:', '', 7, 1, 0, '2022-03-29', '2022-04-18', NULL),
(10, 3, 9, NULL, 'En caso de ser aceptado(a) y radicar FUERA de la zona metropolitana de Puebla, ¿qué tipo de residencia/habitación ocuparía?', '.', 10, 1, 0, '2022-03-29', '2022-04-18', NULL),
(11, 3, 5, 54, '¿Trabaja en el sector salud?', '', 9, 1, 0, '2022-03-29', '2022-04-07', NULL),
(12, 3, 5, NULL, '¿Pertenece una comunidad indígena?', '', 25, 1, 0, '2022-03-29', '2022-04-07', NULL),
(13, 3, 5, NULL, '¿Se encuentra en alguna situación vulnerable?', '', 26, 1, 0, '2022-03-29', '2022-04-07', NULL),
(14, 3, 1, 13, 'En caso de responder que se encuentra en situación vulnerable, indique cual.', '', 27, NULL, 0, '2022-03-29', '2022-04-07', NULL),
(15, 5, 5, NULL, '¿En años anteriores ha realizado algún proceso para ingresar a alguna licenciatura en el área de la salud?', '', 37, 1, 0, '2022-03-29', '2022-04-08', NULL),
(16, 5, 1, 15, 'Indique en qué proceso participó durante el 2021', '', 38, NULL, 0, '2022-03-29', '2022-04-08', '2022-04-08'),
(17, 5, 5, NULL, '¿En este año participará en algún proceso de admisión para el área de la salud? Si su respuesta es si, especifique la universidad y licenciatura solicitada.', '', 39, 1, 0, '2022-03-29', '2022-04-08', '2022-04-08'),
(18, 5, 1, 17, 'Indique en qué proceso participa o participará durante el 2022', '', 40, NULL, 0, '2022-03-29', '2022-04-08', '2022-04-08'),
(19, 3, 5, NULL, '¿Tiene computadora de escritorio?', '', 17, 1, 0, '2022-03-29', '2022-03-29', NULL),
(20, 3, 5, NULL, '¿Tiene computadora portátil (laptop)?', '', 18, 1, 0, '2022-03-29', '2022-03-29', NULL),
(21, 3, 5, NULL, 'Si cuenta con alguna de las dos, ¿su computadora cuenta con micrófono y cámara?', '', 19, 1, 0, '2022-03-29', '2022-03-29', NULL),
(22, 3, 5, NULL, 'En caso de no contar con computadora con cámara y micrófono, ¿Tiene la posibilidad de conseguir una para la aplicación de su examen?', '', 20, NULL, 0, '2022-03-29', '2022-05-04', NULL),
(23, 3, 5, NULL, '¿Tiene servicio de internet?', '', 21, 1, 0, '2022-03-29', '2022-03-29', NULL),
(24, 3, 5, NULL, '¿Tiene tableta inteligente?', '', 22, 1, 0, '2022-03-29', '2022-04-07', NULL),
(25, 3, 5, NULL, '¿Tiene teléfono inteligente (smartphone)?', '', 23, 1, 0, '2022-03-29', '2022-03-29', NULL),
(26, 3, 8, NULL, 'En caso de que tenga alguna beca, por favor indique cual.', '', 24, 1, 0, '2022-03-29', '2022-03-29', NULL),
(27, 4, 10, NULL, 'Tipo de sangre', '', 1, 1, 0, '2022-03-29', '2022-03-29', NULL),
(28, 4, 1, NULL, 'Describa si tiene algún padecimiento', '', 2, 1, 0, '2022-03-29', '2022-04-08', NULL),
(29, 4, 5, NULL, '¿Tiene alguna discapacidad?', 'El uso de lentes por miopía y/o astigmatismo  no es considerado como una discapacidad.', 3, 1, 0, '2022-03-29', '2022-04-09', NULL),
(30, 4, 5, NULL, '¿Cuenta con certificado médico o constancia de discapacidad?', '', 4, 1, 0, '2022-03-29', '2022-04-08', NULL),
(31, 4, 5, NULL, '¿Usa lentes?', '', 5, 1, 0, '2022-03-29', '2022-03-29', NULL),
(32, 4, 5, NULL, '¿Tiene alguna prótesis?', '', 6, 1, 0, '2022-03-29', '2022-03-29', NULL),
(33, 4, 11, NULL, '¿Cuenta con algún servicio de salud?', '', 7, 1, 0, '2022-03-29', '2022-04-08', NULL),
(34, 5, 1, NULL, 'Clave del centro de trabajo', '', 1, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(35, 5, 12, NULL, 'Tipo', '', 2, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(36, 5, 1, NULL, 'Nombre de la institución', '', 3, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(37, 5, 1, NULL, 'País', '', 4, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(38, 5, 1, NULL, 'Estado', '', 5, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(39, 5, 1, NULL, 'Municipio', '', 6, 1, 0, '2022-03-29', '2022-04-01', '2022-04-01'),
(40, 5, 3, NULL, 'Fecha de conclusión (proporcione en caso de haber concluido en el año 2021 o anterior)', '', 7, NULL, 0, '2022-03-29', '2022-04-08', NULL),
(41, 5, 21, NULL, 'Promedio', 'Si actualmente se encuentra cursando la Educación Medio Superior, el promedio debe coincidir con su Constancia de Estudios; en caso de haber concluido la Educación Media Superior, el promedio debe coincidir con el Certificado de Estudios concluidos.', 8, 1, 1, '2022-03-29', '2022-05-03', NULL),
(42, 6, 13, NULL, 'Parentesco', '', 1, 1, 0, '2022-03-29', '2022-04-08', NULL),
(43, 6, 1, NULL, 'Nombre completo', '', 2, 1, 0, '2022-03-29', '2022-03-29', NULL),
(44, 6, 2, NULL, 'Teléfono', '', 3, 1, 0, '2022-03-29', '2022-03-29', NULL),
(45, 6, 20, NULL, 'Correo electrónico', '', 4, 1, 0, '2022-03-29', '2022-05-02', NULL),
(46, 7, 4, NULL, 'Extracto del Acta de Nacimiento', '', 1, 1, 0, '2022-03-29', '2022-04-18', NULL),
(47, 7, 4, NULL, 'Clave Única de Registro de Población (CURP)', 'Actualizada al año 2022.', 4, 1, 0, '2022-03-29', '2022-05-04', NULL),
(48, 7, 4, NULL, 'Identificación oficial', '', 3, 1, 0, '2022-03-29', '2022-04-18', NULL),
(49, 7, 4, NULL, 'Identificación oficial', 'En caso de ser menor de edad, adjunta tu credencial de la escuela donde estés actualmente', 3, 1, 0, '2022-03-29', '2022-04-08', '2022-04-08'),
(50, 7, 4, NULL, 'Certificado de Estudios concluidos o Constancia de Estudios parciales', 'Para las personas aspirantes egresadas del nivel medio superior en 2021 y años anteriores: Certificado de Estudios concluidos; para personas aspirantes que egresan en 2022 del nivel medio superior: Constancia de Estudios parciales', 5, 1, 0, '2022-03-29', '2022-05-04', NULL),
(51, 7, 4, NULL, 'Comprobante de domicilio', '', 6, 1, 0, '2022-03-29', '2022-04-08', '2022-04-08'),
(52, 7, 22, NULL, 'Fotografía', 'Rostro descubierto, de frente, mirando a la cámara (no selfies), sin lentes, con fondo blanco, sin sombras, sin filtros, sin fecha y sin retoques.', 4, 1, 1, '2022-03-29', '2022-05-02', NULL),
(53, 5, 14, NULL, 'Escuela de procedencia', '', 3, 1, 1, '2022-04-01', '2022-05-04', NULL),
(54, 3, 5, NULL, '¿Trabaja?', '', 8, 1, 0, '2022-04-07', '2022-04-07', NULL),
(55, 3, 15, NULL, '¿Habitualmente, qué medio de transporte utiliza?', '', 28, 1, 0, '2022-04-07', '2022-04-07', NULL),
(56, 3, 5, NULL, '¿Habla alguna lengua indígena?', '', 25, 1, 0, '2022-04-07', '2022-04-07', NULL),
(57, 4, 16, 29, '¿Cuál es la discapacidad que tiene?', '', 3, 1, 0, '2022-04-08', '2022-04-18', NULL),
(58, 4, 18, NULL, '¿Se encuentra vacunado(a) contra COVID-19?', '', 9, 1, 0, '2022-04-08', '2022-04-08', NULL),
(59, 5, 1, NULL, 'Mencione las instituciones que considera como opción de ingreso para realizar sus estudios de licenciatura (Opción 1)', '', 9, 1, 0, '2022-04-08', '2022-04-08', '2022-04-08'),
(62, 5, 1, NULL, 'Mencione las instituciones que considera como opción de ingreso para realizar sus estudios de licenciatura (Opción 2)', '', 10, 1, 0, '2022-04-08', '2022-04-08', '2022-04-08'),
(63, 5, 1, NULL, 'Mencione las instituciones que considera como opción de ingreso para realizar sus estudios de licenciatura (Opción 3)', '', 11, 1, 0, '2022-04-08', '2022-04-08', '2022-04-08'),
(64, 8, 1, NULL, 'Opción 1', '', 1, 1, 0, '2022-04-08', '2022-04-08', NULL),
(65, 8, 1, NULL, 'Opción 2', '', 2, 1, 0, '2022-04-08', '2022-04-08', NULL),
(66, 8, 1, NULL, 'Opción 3', '', 3, 1, 0, '2022-04-08', '2022-04-08', NULL),
(67, 6, 1, NULL, 'Calle', '', 7, 1, 0, '2022-04-08', '2022-05-02', NULL),
(68, 6, 1, NULL, 'Número Exterior', '', 6, 1, 0, '2022-04-08', '2022-05-02', NULL),
(69, 6, 1, NULL, 'Número Interior', '', 8, NULL, 0, '2022-04-08', '2022-05-02', NULL),
(70, 6, 1, NULL, 'Colonia', '', 9, 1, 0, '2022-04-08', '2022-05-02', NULL),
(71, 6, 1, NULL, 'Código Postal', '', 10, 1, 0, '2022-04-08', '2022-05-02', NULL),
(72, 6, 1, NULL, 'Municipio', '', 11, 1, 0, '2022-04-08', '2022-05-02', NULL),
(73, 6, 1, NULL, 'Estado', '', 12, 1, 0, '2022-04-08', '2022-05-02', NULL),
(74, 6, 5, NULL, 'Utilizar la misma dirección que mis datos generales', '', 5, NULL, 0, '2022-04-18', '2022-05-02', '2022-05-02');


CREATE TABLE `aspirantes_comentarios` (
  `comentarios_id` int UNSIGNED NOT NULL,
  `comentarios_texto` text NOT NULL,
  `comentarios_campo` int UNSIGNED NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;



INSERT INTO `aspirantes_comentarios` (`comentarios_id`, `comentarios_texto`, `comentarios_campo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'El Extracto de Acta de Nacimiento no es legible (borrosa)', 46, '2022-05-02', '2022-05-02', NULL),
(2, 'El Extracto de Acta de Nacimiento corresponde a otra persona', 46, '2022-05-02', '2022-05-02', NULL),
(3, 'El Extracto de Acta de Nacimiento no está digitalizada de manera correcta (es una fotografía)', 46, '2022-05-02', '2022-05-02', NULL),
(4, 'El Extracto de Acta de Nacimiento se encuentra cortada (escaneo incompleto)', 46, '2022-05-02', '2022-05-02', NULL),
(5, 'Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria', 46, '2022-05-02', '2022-05-02', NULL),
(6, 'La Clave Única de Registro de Población (CURP) no es legible (borrosa).', 47, '2022-05-02', '2022-05-02', NULL),
(7, 'La Clave Única de Registro de Población (CURP) no se encuentra actualizada al año 2022.', 47, '2022-05-02', '2022-05-02', NULL),
(8, 'La Clave Única de Registro de Población (CURP) corresponde a otra persona.', 47, '2022-05-02', '2022-05-02', NULL),
(9, 'La Clave Única de Registro de Población (CURP) no está digitalizada de manera correcta (no es visible la fecha de emisión del documento).', 47, '2022-05-02', '2022-05-02', NULL),
(10, 'La Clave Única de Registro de Población (CURP) se encuentra cortada (escaneo incompleto).', 47, '2022-05-02', '2022-05-02', NULL),
(11, 'Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.', 47, '2022-05-02', '2022-05-02', NULL),
(12, 'El documento cuenta con promedio inferior a 8.0 (ocho punto cero).', 50, '2022-05-02', '2022-05-02', NULL),
(13, 'El documento cuenta con adeudo de materias.', 50, '2022-05-02', '2022-05-02', NULL),
(14, 'El documento no es legible (borroso).', 50, '2022-05-02', '2022-05-02', NULL),
(15, 'El documento corresponde a otra persona.', 50, '2022-05-02', '2022-05-02', NULL),
(16, 'El certificado es de estudios parciales, debe enviar Certificado de Estudios concluidos de nivel medio superior.', 50, '2022-05-02', '2022-05-02', NULL),
(17, 'El documento cuenta con adeudo de materias.', 50, '2022-05-02', '2022-05-02', NULL),
(18, 'El Certificado de Estudios no corresponde al nivel medio superior.', 50, '2022-05-02', '2022-05-02', NULL),
(19, 'Debe adjuntar en este mismo archivo el Transcript y la traducción simple.', 50, '2022-05-02', '2022-05-02', NULL),
(20, 'La Constancia de Estudios no fue expedida en el año 2022.', 50, '2022-05-02', '2022-05-02', NULL),
(21, 'La Constancia de Estudios no cuenta con desglose de materias y calificaciones.', 50, '2022-05-02', '2022-05-02', NULL),
(22, 'El desglose de materias debe de ser desde el primer semestre - año y hasta el quinto semestre - segundo año según corresponda al plan de estudios de la escuela de procedencia.', 50, '2022-05-02', '2022-05-02', NULL),
(23, 'El documento proporcionado no está digitalizado de manera correcta (es una fotografía).', 50, '2022-05-02', '2022-05-02', NULL),
(24, 'El documento se encuentra cortado (escaneo incompleto).', 50, '2022-05-02', '2022-05-02', NULL),
(25, 'El escaneo del documento debe ser por ambos lados.', 50, '2022-05-02', '2022-05-02', NULL),
(26, 'Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.', 50, '2022-05-02', '2022-05-02', NULL),
(27, 'La identificación no es legible (borrosa).', 48, '2022-05-02', '2022-05-02', NULL),
(28, 'La identificación no cuenta con sello y firma de la institución.', 48, '2022-05-02', '2022-05-02', NULL),
(29, 'La identificación corresponde a otra persona.', 48, '2022-05-02', '2022-05-02', NULL),
(30, 'El documento no corresponde a una identificación oficial estipulada en la convocatoria de admisión 2022.', 48, '2022-05-02', '2022-05-02', NULL),
(31, 'El documento proporcionado no está digitalizado de manera correcta (es una fotografía).', 48, '2022-05-02', '2022-05-02', NULL),
(32, 'El documento se encuentra cortado (escaneo incompleto).', 48, '2022-05-02', '2022-05-02', NULL),
(33, 'El escaneo del documento debe ser por ambos lados.', 48, '2022-05-02', '2022-05-02', NULL),
(34, 'Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.', 48, '2022-05-02', '2022-05-02', NULL),
(35, 'La fotografía es borrosa.', 52, '2022-05-02', '2022-05-02', NULL),
(36, 'La fotografía es una selfie.', 52, '2022-05-02', '2022-05-02', NULL),
(37, 'La fotografía no debe tener filtros, textos ni fechas.', 52, '2022-05-02', '2022-05-02', NULL),
(38, 'La fotografía debe abarcar del pecho hacia arriba.', 52, '2022-05-02', '2022-05-02', NULL),
(39, 'La fotografía corresponde a otra persona.', 52, '2022-05-02', '2022-05-02', NULL),
(40, 'El fondo de la fotografía debe ser blanco y liso.', 52, '2022-05-02', '2022-05-02', NULL),
(41, 'El rostro se debe encontrar descubierto (no lentes, no fleco, de frente y preferentemente mirando a la cámara)', 52, '2022-05-02', '2022-05-02', NULL),
(42, 'La dimensión de la fotografía no es la solicitada.', 52, '2022-05-02', '2022-05-02', NULL),
(43, 'La fotografía se encuentra cortada.', 52, '2022-05-02', '2022-05-02', NULL),
(44, 'Has excedido las oportunidades de corrección para tus documentos, conforme a lo establecido en convocatoria.', 52, '2022-05-02', '2022-05-02', NULL);



CREATE TABLE `aspirantes_configuracion` (
  `configuracion_id` int NOT NULL,
  `configuracion_nombre` varchar(40) NOT NULL,
  `configuracion_status` int NOT NULL DEFAULT '1',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;


INSERT INTO `aspirantes_configuracion` (`configuracion_id`, `configuracion_nombre`, `configuracion_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Proceso de Admisión Abierto', 0, '2022-03-23', NULL, NULL),
(2, 'Revalidación de Materias', 0, '2022-03-23', NULL, NULL);


CREATE TABLE `aspirantes_etapas` (
  `etapa_id` int UNSIGNED NOT NULL,
  `etapa_periodo` varchar(7) NOT NULL,
  `etapa_nombre` varchar(80) NOT NULL,
  `etapa_orden` int NOT NULL,
  `etapa_formulario` varchar(1) NOT NULL COMMENT 'Indica si esta etapa contiene un formulario relacionado',
  `etapa_responsable` varchar(1) NOT NULL DEFAULT '1' COMMENT '1 si el responsable es el aspirante, 0 cuando es admision de escolares',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Volcado de datos para la tabla `aspirantes_etapas`
--

INSERT INTO `aspirantes_etapas` (`etapa_id`, `etapa_periodo`, `etapa_nombre`, `etapa_orden`, `etapa_formulario`, `etapa_responsable`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2022A', 'Captura de informacion', 1, '1', '1', '2022-03-25', '2022-03-29', '2022-03-29'),
(2, '2022A', 'editable', 2, '1', '1', '2022-03-25', '2022-03-29', '2022-03-29'),
(3, '2022A', 'Datos socioeconómicos', 1, '1', '1', '2022-03-29', '2022-03-29', NULL),
(4, '2022A', 'Datos de salud', 2, '1', '1', '2022-03-29', '2022-03-29', NULL),
(5, '2022A', 'Escuela de procedencia', 3, '1', '1', '2022-03-29', '2022-04-08', NULL),
(6, '2022A', 'Contacto de emergencia', 4, '1', '1', '2022-03-29', '2022-03-29', NULL),
(8, '2022A', 'Carga de documentos', 5, '1', '1', '2022-03-29', '2022-03-29', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirantes_formularios`
--

CREATE TABLE `aspirantes_formularios` (
  `formulario_id` int UNSIGNED NOT NULL,
  `formulario_etapa` int UNSIGNED NOT NULL,
  `formulario_nombre` varchar(45) NOT NULL,
  `formulario_titulo` varchar(120) NOT NULL,
  `formulario_orden` int NOT NULL,
  `formulario_instrucciones` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Volcado de datos para la tabla `aspirantes_formularios`
--

INSERT INTO `aspirantes_formularios` (`formulario_id`, `formulario_etapa`, `formulario_nombre`, `formulario_titulo`, `formulario_orden`, `formulario_instrucciones`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Documentos', 'Carga de documentos del usuario', 1, 'favor de corroborar los documentos que se subirán para revisión de escolar ', '2022-03-25', '2022-03-29', '2022-03-29'),
(2, 1, 'Segundo Formulario', 'Datos de vivienda', 2, 'a', '2022-03-25', '2022-03-29', '2022-03-29'),
(3, 3, 'Datos socioeconómicos', 'Datos socioeconómicos', 1, 'Conteste lo que se le pide de acuerdo a su situación actual', '2022-03-29', '2022-03-29', NULL),
(4, 4, 'Datos de salud', 'Datos de salud', 1, 'Conteste lo que se solicita con referencia a su estado de salud actual', '2022-03-29', '2022-03-29', NULL),
(5, 5, 'Escuela de procedencia', 'Escuela de procedencia', 1, 'Proporcione la información de acuerdo a su escuela de procedencia.', '2022-03-29', '2022-04-08', NULL),
(6, 6, 'Contacto de emergencia', 'Contacto de emergencia', 1, 'Proporcione los datos del contacto al cual debemos llamar en caso de alguna situación de emergencia', '2022-03-29', '2022-04-08', NULL),
(7, 8, 'Carga de documentos', 'Carga de documentos', 1, '<p>Para este apartado es indispensable consultar el  <a href=\"https://google.com\" target=\"_blank\" class=\"text-primary font-weight-bold\"> manual de digitalización </a> disponible <a href=\"https://google.com\" target=\"_blank\" class=\"text-primary font-weight-bold\"> aquí </a>.</p> <p> En caso de observaciones, solo tiene derecho a una oportunidad de corrección en sus documentos, conforme a lo establecido en la <a href=\"https://usep.puebla.gob.mx/images/site/content/Servicios%20escolares/Convocatoria%202022%20-%202%20mayo%20subsanada.pdf\" target=\"_blank\" class=\"text-primary font-weight-bold\"> convocatoria </a>.</p>', '2022-03-29', '2022-05-03', NULL),
(8, 5, 'Opciones de ingreso', 'Opciones de ingreso a Educación Superior', 2, 'Mencione las instituciones que considera como opciones de ingreso para realizar sus estudios de licenciatura.', '2022-04-08', '2022-04-18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirantes_generales`
--

CREATE TABLE `aspirantes_generales` (
  `aspirante_id` varchar(9) NOT NULL,
  `aspirante_carrera` int UNSIGNED NOT NULL,
  `aspirante_etapa` int UNSIGNED DEFAULT NULL,
  `aspirante_validado` varchar(1) NOT NULL,
  `aspirante_curp` varchar(18) NOT NULL,
  `aspirante_nombre` varchar(80) NOT NULL,
  `aspirante_ap_paterno` varchar(80) NOT NULL,
  `aspirante_ap_materno` varchar(80) NOT NULL,
  `aspirante_correo` varchar(80) NOT NULL,
  `aspirante_telefono` varchar(14) DEFAULT NULL,
  `aspirante_calle` varchar(50) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_colonia` varchar(50) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_numero` varchar(10) DEFAULT NULL COMMENT 'Datos de domicilio (num exterior)',
  `aspirante_num_int` varchar(20) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_cp` varchar(5) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_municipio` varchar(80) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_estado` varchar(80) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_pais` varchar(80) DEFAULT NULL COMMENT 'Datos de domicilio',
  `aspirante_sexo` varchar(1) DEFAULT NULL COMMENT 'Colocar H para hombre y M para mujer',
  `aspirante_nac_fecha` date DEFAULT NULL COMMENT 'Fecha de nacimiento',
  `aspirante_nac_municipio` varchar(80) DEFAULT NULL COMMENT 'Municipio o ciudad de nacimiento',
  `aspirante_nac_estado` varchar(80) DEFAULT NULL COMMENT 'Estado de nacimiento',
  `aspirante_nac_pais` varchar(80) DEFAULT NULL COMMENT 'Pais de nacimiento',
  `aspirante_estatus` enum('EXITOSO','OBSERVACIONES','DECLINADO','ESPERA','REVISION','REGISTRO') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'REGISTRO',
  `aspirante_revalida` varchar(2) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Estructura de tabla para la tabla `aspirantes_notificaciones`
--

CREATE TABLE `aspirantes_notificaciones` (
  `notificaciones_id` int UNSIGNED NOT NULL,
  `notificaciones_aspirante` varchar(9) NOT NULL,
  `notificaciones_mensaje` text NOT NULL,
  `notificaciones_formulario` int UNSIGNED DEFAULT NULL,
  `notificaciones_leido` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;


--
-- Estructura de tabla para la tabla `aspirantes_respuestas`
--

CREATE TABLE `aspirantes_respuestas` (
  `respuesta_id` int UNSIGNED NOT NULL,
  `respuesta_aspirante` varchar(9) NOT NULL,
  `respuesta_campo` int UNSIGNED NOT NULL,
  `respuesta_valor` text,
  `respuesta_comentario` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;


--
-- Estructura de tabla para la tabla `aspirantes_tipos_campo`
--

CREATE TABLE `aspirantes_tipos_campo` (
  `tipocampo_id` int UNSIGNED NOT NULL,
  `tipocampo_nombre` varchar(45) NOT NULL,
  `tipocampo_valores` text NOT NULL,
  `tipocampo_clasificacion` enum('BASIC','COLECCION','ARCHIVO','CATALOGO','CALIFICACION') CHARACTER SET utf8mb4  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Volcado de datos para la tabla `aspirantes_tipos_campo`
--

INSERT INTO `aspirantes_tipos_campo` (`tipocampo_id`, `tipocampo_nombre`, `tipocampo_valores`, `tipocampo_clasificacion`) VALUES
(1, 'TEXTO', '/^[\\w\\sáéíóúñüÁÉÍÓÚÑÜ]+$/', 'BASIC'),
(2, 'NUMERO', '/^[0-9]+$/', 'BASIC'),
(3, 'FECHA', '/^[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}$/', 'BASIC'),
(4, 'DOCUMENTO', 'pdf,jpg,jpeg', 'ARCHIVO'),
(5, 'RESPUESTA SI O NO', 'Sí,No', 'COLECCION'),
(6, 'ESTADO CIVIL', 'Casado(a),Soltero(a)', 'COLECCION'),
(7, 'ESTATUS DE VIVIENDA', 'Rentada,Prestada,Propia', 'COLECCION'),
(8, 'TIPOS BECA', 'Manutención,Jóvenes escribiendo el futuro,No cuento con beca,Otra', 'COLECCION'),
(9, 'RADICADOS FUERA DE PUEBLA', 'Rentada,Prestada,Propia,No radico fuera de la zona metropoitana', 'COLECCION'),
(10, 'TIPOS DE SANGRE', 'O+,O-,A+,A-,B+,B-,AB+,AB-', 'COLECCION'),
(11, 'SERVICIOS DE SALUD', 'IMSS,ISSSTE,ISSSTEP,HU,No cuento con servicio de salud,OTRO', 'COLECCION'),
(12, 'TIPO EDUCACION', 'Pública,Privada', 'COLECCION'),
(13, 'PARENTESCOS', 'Madre,Padre,Cónyuge,Hermano(a),Abuelo(a),Tío(a),Primo(a),Otro', 'COLECCION'),
(14, 'ESCUELA', 'cat_ccts', 'CATALOGO'),
(15, 'TRANSPORTES', 'Automóvil propio,Taxi o plataforma,Transporte colectivo,Otro', 'COLECCION'),
(16, 'DISCAPACIDADES', 'Sensorial y de la comunicación,Motriz,Mental,Múltiple,Temporal,Ninguna', 'COLECCION'),
(18, 'VACUNA', 'Primera Dosis,Segunda Dosis,Tercera Dosis,Cuarta Dosis,No estoy vacunado(a)', 'COLECCION'),
(19, 'DECIMAL', '/^[0-9]+(\\.[0-9]+)?$/', 'BASIC'),
(20, 'CORREO', '/^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$/', 'BASIC'),
(21, 'CALIFICACION', '/^((10)|([0-9]{1}(\\.[0-9]{1,2})?))$/', 'BASIC'),
(22, 'FOTO', 'jpg,jpeg', 'ARCHIVO');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_aspirantes_procedencia`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_aspirantes_procedencia` (
`aspirante_folio` varchar(9)
,`municipio_id` varchar(80)
,`municipio_nombre` varchar(100)
,`estado_id` int
,`estado_nombre` varchar(40)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_aspirantes_respuestas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_aspirantes_respuestas` (
`respuesta_id` int unsigned
,`respuesta_aspirante` varchar(9)
,`respuesta_valor` text
,`respuesta_comentario` text
,`created_at` date
,`updated_at` date
,`deleted_at` date
,`campo_id` int unsigned
,`campo_nombre` varchar(10001)
,`campo_descripcion` text
,`campo_orden` int unsigned
,`campo_obligatorio` int unsigned
,`campo_visualizar` int
,`tipocampo_id` int unsigned
,`tipocampo_nombre` varchar(45)
,`tipocampo_valores` text
,`tipocampo_clasificacion` enum('BASIC','COLECCION','ARCHIVO','CATALOGO','CALIFICACION')
,`formulario_id` int unsigned
,`formulario_etapa` int unsigned
,`formulario_nombre` varchar(45)
,`formulario_titulo` varchar(120)
,`formulario_orden` int
,`formulario_instrucciones` text
,`etapa_nombre` varchar(80)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `view_aspirantes_procedencia`
--
DROP TABLE IF EXISTS `view_aspirantes_procedencia`;

CREATE ALGORITHM=UNDEFINED DEFINER=`usep`@`localhost` SQL SECURITY DEFINER VIEW `view_aspirantes_procedencia`  AS SELECT `aspirantes_generales`.`aspirante_id` AS `aspirante_folio`, `aspirantes_generales`.`aspirante_municipio` AS `municipio_id`, `municipios`.`nombre` AS `municipio_nombre`, `municipios`.`estado_id` AS `estado_id`, `estados`.`nombre` AS `estado_nombre` FROM ((`aspirantes_generales` left join `municipios` on((`municipios`.`id` = `aspirantes_generales`.`aspirante_municipio`))) left join `estados` on((`estados`.`id` = `municipios`.`estado_id`)))  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_aspirantes_respuestas`
--
DROP TABLE IF EXISTS `view_aspirantes_respuestas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`usep`@`localhost` SQL SECURITY DEFINER VIEW `view_aspirantes_respuestas`  AS SELECT `aspirantes_respuestas`.`respuesta_id` AS `respuesta_id`, `aspirantes_respuestas`.`respuesta_aspirante` AS `respuesta_aspirante`, `aspirantes_respuestas`.`respuesta_valor` AS `respuesta_valor`, `aspirantes_respuestas`.`respuesta_comentario` AS `respuesta_comentario`, `aspirantes_respuestas`.`created_at` AS `created_at`, `aspirantes_respuestas`.`updated_at` AS `updated_at`, `aspirantes_respuestas`.`deleted_at` AS `deleted_at`, `aspirantes_campos`.`campo_id` AS `campo_id`, `aspirantes_campos`.`campo_nombre` AS `campo_nombre`, `aspirantes_campos`.`campo_descripcion` AS `campo_descripcion`, `aspirantes_campos`.`campo_orden` AS `campo_orden`, `aspirantes_campos`.`campo_obligatorio` AS `campo_obligatorio`, `aspirantes_campos`.`campo_visualizar` AS `campo_visualizar`, `aspirantes_tipos_campo`.`tipocampo_id` AS `tipocampo_id`, `aspirantes_tipos_campo`.`tipocampo_nombre` AS `tipocampo_nombre`, `aspirantes_tipos_campo`.`tipocampo_valores` AS `tipocampo_valores`, `aspirantes_tipos_campo`.`tipocampo_clasificacion` AS `tipocampo_clasificacion`, `aspirantes_formularios`.`formulario_id` AS `formulario_id`, `aspirantes_formularios`.`formulario_etapa` AS `formulario_etapa`, `aspirantes_formularios`.`formulario_nombre` AS `formulario_nombre`, `aspirantes_formularios`.`formulario_titulo` AS `formulario_titulo`, `aspirantes_formularios`.`formulario_orden` AS `formulario_orden`, `aspirantes_formularios`.`formulario_instrucciones` AS `formulario_instrucciones`, `aspirantes_etapas`.`etapa_nombre` AS `etapa_nombre` FROM ((((`aspirantes_respuestas` join `aspirantes_campos` on((`aspirantes_respuestas`.`respuesta_campo` = `aspirantes_campos`.`campo_id`))) join `aspirantes_formularios` on((`aspirantes_campos`.`campo_formulario` = `aspirantes_formularios`.`formulario_id`))) join `aspirantes_etapas` on((`aspirantes_formularios`.`formulario_etapa` = `aspirantes_etapas`.`etapa_id`))) join `aspirantes_tipos_campo` on((`aspirantes_campos`.`campo_tipo` = `aspirantes_tipos_campo`.`tipocampo_id`)))  ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aspirantes_campos`
--
ALTER TABLE `aspirantes_campos`
  ADD PRIMARY KEY (`campo_id`),
  ADD KEY `fkcamporform_idx` (`campo_formulario`),
  ADD KEY `fkcampotipo_idx` (`campo_tipo`),
  ADD KEY `fkcampodepende_idx` (`campo_dependiente`);

--
-- Indices de la tabla `aspirantes_comentarios`
--
ALTER TABLE `aspirantes_comentarios`
  ADD PRIMARY KEY (`comentarios_id`),
  ADD KEY `comentarios_campo` (`comentarios_campo`);

--
-- Indices de la tabla `aspirantes_configuracion`
--
ALTER TABLE `aspirantes_configuracion`
  ADD PRIMARY KEY (`configuracion_id`);

--
-- Indices de la tabla `aspirantes_etapas`
--
ALTER TABLE `aspirantes_etapas`
  ADD PRIMARY KEY (`etapa_id`),
  ADD KEY `fketapaperiodo_idx` (`etapa_periodo`);

--
-- Indices de la tabla `aspirantes_formularios`
--
ALTER TABLE `aspirantes_formularios`
  ADD PRIMARY KEY (`formulario_id`),
  ADD KEY `fkformetapa_idx` (`formulario_etapa`);

--
-- Indices de la tabla `aspirantes_generales`
--
ALTER TABLE `aspirantes_generales`
  ADD PRIMARY KEY (`aspirante_id`),
  ADD UNIQUE KEY `aspirante_id_UNIQUE` (`aspirante_id`),
  ADD KEY `fkaspirantescarrera_idx` (`aspirante_carrera`),
  ADD KEY `fkaspirantesetapa_idx` (`aspirante_etapa`);

--
-- Indices de la tabla `aspirantes_notificaciones`
--
ALTER TABLE `aspirantes_notificaciones`
  ADD PRIMARY KEY (`notificaciones_id`),
  ADD KEY `notificaciones_aspirante` (`notificaciones_aspirante`),
  ADD KEY `notificaciones_formulario` (`notificaciones_formulario`);

--
-- Indices de la tabla `aspirantes_respuestas`
--
ALTER TABLE `aspirantes_respuestas`
  ADD PRIMARY KEY (`respuesta_id`),
  ADD KEY `fkrespaspirante_idx` (`respuesta_aspirante`),
  ADD KEY `fkrespuestacampo_idx` (`respuesta_campo`);

--
-- Indices de la tabla `aspirantes_tipos_campo`
--
ALTER TABLE `aspirantes_tipos_campo`
  ADD PRIMARY KEY (`tipocampo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aspirantes_campos`
--
ALTER TABLE `aspirantes_campos`
  CHANGES `campo_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_comentarios`
--
ALTER TABLE `aspirantes_comentarios`
  CHANGES `comentarios_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_configuracion`
--
ALTER TABLE `aspirantes_configuracion`
  CHANGES `configuracion_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_etapas`
--
ALTER TABLE `aspirantes_etapas`
  CHANGES `etapa_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_formularios`
--
ALTER TABLE `aspirantes_formularios`
  CHANGES `formulario_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_notificaciones`
--
ALTER TABLE `aspirantes_notificaciones`
  CHANGES `notificaciones_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_respuestas`
--
ALTER TABLE `aspirantes_respuestas`
  CHANGES `respuesta_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `aspirantes_tipos_campo`
--
ALTER TABLE `aspirantes_tipos_campo`
  CHANGES `tipocampo_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aspirantes_campos`
--
ALTER TABLE `aspirantes_campos`
  ADD CONSTRAINT `fkcampodepende` FOREIGN KEY (`campo_dependiente`) REFERENCES `aspirantes_campos` (`campo_id`),
  ADD CONSTRAINT `fkcamporform` FOREIGN KEY (`campo_formulario`) REFERENCES `aspirantes_formularios` (`formulario_id`),
  ADD CONSTRAINT `fkcampotipo` FOREIGN KEY (`campo_tipo`) REFERENCES `aspirantes_tipos_campo` (`tipocampo_id`);

--
-- Filtros para la tabla `aspirantes_comentarios`
--
ALTER TABLE `aspirantes_comentarios`
  ADD CONSTRAINT `aspirantes_comentarios_ibfk_1` FOREIGN KEY (`comentarios_campo`) REFERENCES `aspirantes_campos` (`campo_id`),
  ADD CONSTRAINT `aspirantes_comentarios_ibfk_2` FOREIGN KEY (`comentarios_campo`) REFERENCES `aspirantes_campos` (`campo_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `aspirantes_etapas`
--
ALTER TABLE `aspirantes_etapas`
  ADD CONSTRAINT `fketapaperiodo` FOREIGN KEY (`etapa_periodo`) REFERENCES `aca_periodos` (`periodo_id`);

--
-- Filtros para la tabla `aspirantes_formularios`
--
ALTER TABLE `aspirantes_formularios`
  ADD CONSTRAINT `fkformetapa` FOREIGN KEY (`formulario_etapa`) REFERENCES `aspirantes_etapas` (`etapa_id`);

--
-- Filtros para la tabla `aspirantes_generales`
--
ALTER TABLE `aspirantes_generales`
  ADD CONSTRAINT `fkaspirantescarrera` FOREIGN KEY (`aspirante_carrera`) REFERENCES `prein_carreras` (`carrera_id`),
  ADD CONSTRAINT `fkaspirantesetapa` FOREIGN KEY (`aspirante_etapa`) REFERENCES `aspirantes_etapas` (`etapa_id`);

--
-- Filtros para la tabla `aspirantes_notificaciones`
--
ALTER TABLE `aspirantes_notificaciones`
  ADD CONSTRAINT `aspirantes_notificaciones_ibfk_1` FOREIGN KEY (`notificaciones_aspirante`) REFERENCES `aspirantes_generales` (`aspirante_id`),
  ADD CONSTRAINT `aspirantes_notificaciones_ibfk_2` FOREIGN KEY (`notificaciones_formulario`) REFERENCES `aspirantes_formularios` (`formulario_id`);

--
-- Filtros para la tabla `aspirantes_respuestas`
--
ALTER TABLE `aspirantes_respuestas`
  ADD CONSTRAINT `fkrespaspirante` FOREIGN KEY (`respuesta_aspirante`) REFERENCES `aspirantes_generales` (`aspirante_id`),
  ADD CONSTRAINT `fkrespuestacampo` FOREIGN KEY (`respuesta_campo`) REFERENCES `aspirantes_campos` (`campo_id`);

