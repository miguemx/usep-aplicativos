DROP TABLE IF EXISTS `filtro_usuarios_ext`;
CREATE TABLE `filtro_usuarios_ext` (
  `externo_correo` varchar(60) NOT NULL,
  `externo_nombre` varchar(60) NOT NULL DEFAULT '',
  `externo_dependencia` varchar(60) DEFAULT NULL,
  `externo_edad` varchar(60) DEFAULT NULL,
  `externo_sexo` varchar(60) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`externo_correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `filtro_sanitario`;
CREATE TABLE `filtro_sanitario` (
  `filtro_folio` int(10) NOT NULL AUTO_INCREMENT,
  `filtro_correo` varchar(60) DEFAULT NULL,
  `filtro_correo_externo` varchar(60) DEFAULT NULL,
  `filtro_diabetes` varchar(1) DEFAULT NULL,
  `filtro_hipertension` varchar(1) DEFAULT NULL,
  `filtro_antecedentecard` varchar(1) DEFAULT NULL,
  `filtro_enfermedadres` varchar(1) DEFAULT NULL,
  `filtro_otropadecimiento` varchar(1) DEFAULT NULL,
  `filtro_fumador` varchar(1) DEFAULT NULL,
  `filtro_mayorfiebre` varchar(1) DEFAULT NULL,
  `filtro_mayortos` varchar(1) DEFAULT NULL,
  `filtro_mayordifrespiratoria` varchar(1) DEFAULT NULL,
  `filtro_mayordolorcabeza` varchar(1) DEFAULT NULL,
  `filtro_menordolormuscular` varchar(1) DEFAULT NULL,
  `filtro_menordolorarticular` varchar(1) DEFAULT NULL,
  `filtro_menorfatiga` varchar(1) DEFAULT NULL,
  `filtro_menoralimentos` varchar(1) DEFAULT NULL,
  `filtro_menorabdominal` varchar(1) DEFAULT NULL,
  `filtro_menoralteragusto` varchar(1) DEFAULT NULL,
  `filtro_menoralteraolfato` varchar(1) DEFAULT NULL,
  `filtro_menornasal` varchar(1) DEFAULT NULL,
  `filtro_menorconjuntivitis` varchar(1) DEFAULT NULL,
  `filtro_menorescalofrios` varchar(1) DEFAULT NULL,
  `filtro_menorviaje` varchar(1) DEFAULT NULL,
  `filtro_menorcovid` varchar(1) DEFAULT NULL,
  `filtro_menorotro` varchar(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`filtro_folio`),
  KEY `filtro_correo` (`filtro_correo`),
  KEY `fk_externo` (`filtro_correo_externo`),
  CONSTRAINT `fk_correo` FOREIGN KEY (`filtro_correo`) REFERENCES `aplicativo_usuarios` (`usuario_correo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_externo` FOREIGN KEY (`filtro_correo_externo`) REFERENCES `filtro_usuarios_ext` (`externo_correo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

