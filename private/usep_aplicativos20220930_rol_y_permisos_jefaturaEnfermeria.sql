INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'JEFATURA ENFERMERIA', 'default');

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'bibliotecas', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'capturacalificaciones');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'estudiantes', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'home', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'renderfiles', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'buscagrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'capturacalificacionesporgrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'docente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'listaescolar');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'generalistapdf');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'asistencia');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'consultahorario');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'conviertedias');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '25', 'escolar', 'guardaasistencia');