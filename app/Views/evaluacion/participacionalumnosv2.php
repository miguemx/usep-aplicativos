<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 1800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<div class="container ">
<div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
  <div class="card-header">
    <h1 style="display:flex; justify-content:center">Estadística de Alumnos que han contestado la evaluación Docente (Cuestionarios)</h1>
    <h3 style="display:flex; justify-content:center">Porcentaje Total de Alumnos: &nbsp<?php echo '<b>'. round($porcentajeAlumnosTotal,2).'</b>'; ?></h2>
    <h4 style="display:flex; justify-content:center"><?php echo 'Porcentaje de Enfermería: '. round($porcentajeAlumnosEnfermeria,2); ?></h2>
    <h4 style="display:flex; justify-content:center"><?php echo 'Porcentaje de Medicina: '. round($porcentajeAlumnosMedicina,2); ?></h2>
  </div>
  <div class="card-body">
    <figure class="highcharts-figure">
      <div id="container"></div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Sección</th>
            <th>% de Alumnos</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($arrayParticipacionAlumnos as $seccion => $porcentaje) :?>
            <tr>
              <td><?php echo $seccion; ?></td>
              <td><?php echo round($porcentaje,2); ?></td>
            </tr>
          <?php endforeach;?> 
        </tbody>
      </table>
    </figure>
  </div>
</div>
</div>
<script>
   Highcharts.chart('container', {

    chart: {
  type: 'column'
},

title: {
  text: 'Porcentaje de Alumnos por Sección'
},

xAxis: {
  categories: [<?php foreach ($arrayParticipacionAlumnos as $seccion => $porcentaje) :
           echo "'".$seccion."',";
         endforeach; 
         ?>
]
},

yAxis: {
  allowDecimals: false,
  min: 0,
  title: {
    text: 'Porcentaje de Alumnos'
  }
},

tooltip: {
  formatter: function () {
    return '<b>' + this.x + '</b><br/>' +
      this.series.name + ': ' + this.y + '<br/>' +
      'Total: ' + this.point.stackTotal;
  }
},


plotOptions: {
  column: {
    stacking: 'normal'
  },
  series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        verticalAlign:'top',
        format: '{point.y:.1f}%'
      }
    }
},

series: [{
  name: 'Alumnos',
  data: [<?php
    foreach ($arrayParticipacionAlumnos as $seccion => $porcentaje) :
          echo $porcentaje.",";
        endforeach; 
        ?>
  ],
  color: '#B37D0B',
  stack: 'Alumnos2'
}]
});
  let creditos = document.getElementsByClassName('highcharts-credits');
    creditos[0].childNodes[0].data = '';
    creditos[1].childNodes[0].data = '';
</script>
<?php echo $this->endSection() ?>