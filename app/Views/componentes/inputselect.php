<?php 
if ( $disabled == true ) $disabled = 'disabled="disabled"';
?>
<div class="col-md-12"><!-- inicio del form group -->
    <?php echo $label; ?><?php if($mandatory) echo '*'; ?>
    <div class="input-group input-group-outline" <?php if($description) echo 'style="margin-bottom: 0px !important;"'; ?>>
        <select class="form-control" name="<?php echo $name; ?>" value="" <?php echo $disabled; ?>>
            <option value="">-- Seleccione --</option>
            <?php foreach($options as $text): ?>
                <option value="<?php echo $text; ?>" <?php if($text==$value) echo 'selected="selected"'; ?> ><?php echo mb_strtoupper($text); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    
    <!-- descripcion del campo -->
    <?php if($description): ?>
        <div style="margin-bottom: 0.5rem !important; font-size: 13px;"><?php echo $description; ?></div>
    <?php endif; ?>
</div><!-- fin del form group -->