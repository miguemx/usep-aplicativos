<?php echo $this->include('menus/_inicio'); ?>



<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav me-auto mb-2 mb-lg-0">
    <li class="nav-item">
      <a class="nav-link active" href="<?php echo base_url() ?>">Inicio</a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Estudiantes
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/secciones') ?>">Cambio de Sección</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/BajadeMateria') ?>">Bajas de Materias</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/AltaMateria') ?>">Altas de Materias</a></li>
      </ul>
    </li>



    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Docente
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/CapturaCalificaciones') ?>">Captura de calificaciones</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/Horario'); ?>">Horarios</a></li>
        <li>
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/listaescolar') ?>">Lista de grupos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/bitacoraAulas'); ?>">Bitacora de Aulas</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('SubdireccionAcademica/paseListaDocentes') ?>">Pase de Lista Global</a></li>
        <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Grupos'); ?>">Listas de Asistencia</a></li>-->
        <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Calificaciones'); ?>">Calificaciones</a></li>-->
        <!--<li><hr class="dropdown-divider"></li>-->
        <!--<li><a class="dropdown-item" href="<?php echo base_url('CapitalHumano/Docentes'); ?>">Listado de Docentes</a></li>-->
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Voto electrónico
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/procesos'); ?>">Registro de procesos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/candidatos'); ?>">Registro de candidatos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/padron'); ?>">Registro de padrón</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/gestionprocesos'); ?>">Gestión de procesos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/estadisticas'); ?>">Consulta de estadísticas</a></li>
      </ul>
    </li>

    <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Filtro Sanitario
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?php echo base_url('FiltroSanitario/ReporteporFechas'); ?>">Descarga de Reportes</a></li>
          </ul>
        </li> -->

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Programacion Academica
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/GeneradorPA'); ?>">Generador de Programacion Academica</a></li>
        <!-- <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/ConfiguradorPC'); ?>">Configurador de Prácticas Clínicas</a></li> -->
        <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/Menu'); ?>">Mapa de Aulas</a></li>
        <!-- <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/PracticasClinicas/'); ?>">Mapa de Prácticas Clínicas</a></li> -->
        <!-- <li><a class="dropdown-item" href="<?php // echo base_url('ProgramacionAcademica/AulaVirtual/'); ?>">Listas de Grupos Virtuales</a></li> -->
                    <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/MenuDocentes/'); ?>">Horario por Docentes</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/HorarioSecciones/'); ?>">Horario por Secciones</a></li>
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Herramientas
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="https://mail.google.com/" target="_blank">Mi correo</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Bibliotecas') ?>">Bibliotecas</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="http://172.10.8.207/" target="_blank">Gestor de Proyectos</a></li>
        <li><a class="dropdown-item" href="http://172.10.9.212:8080/" target="_blank">Gestor Documental</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Login/Logout'); ?>">Cerrar Sesión</a></li>
      </ul>
    </li>
  </ul>
  <div style="color: #fff;">
    Bienvenido
    <?php if (!is_null(session('nombre'))) :
      echo session('nombre');
    endif;
    ?>
  </div>
</div>



<?php echo $this->include('menus/_fin'); ?>