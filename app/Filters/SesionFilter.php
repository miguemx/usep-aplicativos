<?php
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class SesionFilter implements FilterInterface {

    public function before( RequestInterface $request, $arguments = null ) {
        $session = \Config\Services::session();
        $useremail = $session->get('useremail');
        //$id = $session->get('aspirante');
        $id = $session->get('id');
        if ( is_null($useremail) || is_null($id) )
        {
            $session->destroy();
            return redirect()->to(base_url('login'));
        }
    }

    public function after( RequestInterface $request, ResponseInterface $response, $arguments = null ) {

    }

}