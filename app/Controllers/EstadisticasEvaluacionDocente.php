<?php

namespace App\Controllers;

use App\Entities\Evaluacion;
use App\Entities\EvaluacionesFormulario;
use App\Entities\EvaluacionesPreguntas;
use App\Libraries\Estadisticas;
use App\Models\EvaluacionesFormulariosModel;
use App\Models\EvaluacionesPreguntasModel;
use App\Models\EvaluacionesTipoPreguntasModel;
use App\Models\EvaluacionModel;
use App\Models\PeriodoModel;
use App\Models\TipoEvaluacionesModel;
use App\Models\AlumnosModel;
use App\Models\GrupoAlumnoModel;
use App\Models\GrupoModel;
use App\Models\CapturaCalificacionesModel;
use App\Models\EstadisticaEvaluacionDocenteModel;
use App\Models\EvaluacionDocentePorcentajesModel;
use App\Models\EvaluacionesRespuestasModel;
use App\Models\GruposMateriasModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Coordinate;
use App\Models\EmpleadosModel;
use App\Models\MateriaModel;

class EstadisticasEvaluacionDocente extends BaseController
{
    private $tipoEvaluacionesModel;
    private $evaluacionModel;
    private $periodosModel;
    private $fromularioModel;
    private $preguntasModel;
    private $tipoPreguntasModel;
    private $alumnosModel;
    private $grupoAlumnosModel;
    private $periodoModel;
    private $grupoModel;
    private $capturaCalificacionesModel;
    private $estadisticaEvaluacionDocenteModel;
    private $porcentajesEvaluacionModel;
    private $respuestasModel;
    private $gruposMateriasModel;
    private $evaluacionesRespuestasModel;
    private $empleadosModel;
    private $materiaModel;

    public function __construct()
    {
        $this->evaluacionModel = new EvaluacionModel();
        $this->periodosModel = new PeriodoModel();
        $this->fromularioModel = new EvaluacionesFormulariosModel();
        $this->preguntasModel = new EvaluacionesPreguntasModel();
        $this->tipoPreguntasModel = new EvaluacionesTipoPreguntasModel();
        $this->tipoEvaluacionesModel = new TipoEvaluacionesModel();
        $this->alumnosModel = new AlumnosModel();
        $this->grupoAlumnosModel = new GrupoAlumnoModel();
        $this->periodoModel = new PeriodoModel();
        $this->grupoModel = new PeriodoModel();
        $this->capturaCalificacionesModel = new CapturaCalificacionesModel();
        $this->estadisticaEvaluacionDocenteModel = new EstadisticaEvaluacionDocenteModel();
        $this->porcentajesEvaluacionModel = new EvaluacionDocentePorcentajesModel();
        $this->respuestasModel = new EvaluacionesRespuestasModel();
        $this->grupoModel = new GrupoModel();
        $this->gruposMateriasModel = new GruposMateriasModel();;
        $this->evaluacionesRespuestasModel = new EvaluacionesRespuestasModel();
        $this->grupoModel = new GrupoModel();
        $this->empleadosModel = new EmpleadosModel();
        $this->materiaModel = new MateriaModel();
    }

/**
 * muestra el procentaje de participación en la evaluación docente
 * @param periodoid periodo de la evaluacion a consultar la particiácion de los alumnos
 * @param evaluacion evaluacion a consultar la particiácion de los alumnos
 */
public function participacionalumnos($periodoid,$evaluacion){
    ini_set('memory_limit', '256M');
    //$periodoid = $this->periodoModel->periodoFsys ();
    $alumnostodaslasmateriascontestadas=[];
    $totalgruposxalumno = $this->capturaCalificacionesModel->n_alumnos_grupos($periodoid);
    foreach ($totalgruposxalumno as $cantidadmaterias){
        $alumno=$cantidadmaterias->gpoalumno_alumno;
        $num_materias=$cantidadmaterias->n_grupos;
        $num_cuestionarios=$this->estadisticaEvaluacionDocenteModel->conteo_grupos($evaluacion,$alumno);
        if ($num_materias==$num_cuestionarios->n_grupos){
            $alumnostodaslasmateriascontestadas[]=$alumno;
        }
    }; 
    $totalAlumnosEnfermeria=($this->alumnosModel->cuentaAlumnos('1')->n_alumnos);//alumnos leo
    $totalAlumnosMedicina=($this->alumnosModel->cuentaAlumnos('2')->n_alumnos);//alumnos med
    $numAlumnosEvaluacionesEnfermeria=($this->estadisticaEvaluacionDocenteModel->cuentaAlumnosEvaluacion ($evaluacion,$alumnostodaslasmateriascontestadas,'1')->n_alumnos);
    $numAlumnosEvaluacionesMedicina=($this->estadisticaEvaluacionDocenteModel->cuentaAlumnosEvaluacion ($evaluacion,$alumnostodaslasmateriascontestadas,'2')->n_alumnos);
    $participacionalumnos = $this->estadisticaEvaluacionDocenteModel-> participacion_alumnos ($periodoid,$alumnostodaslasmateriascontestadas);
    $porcentajeAlumnosEnfermeria = ($numAlumnosEvaluacionesEnfermeria*100/$totalAlumnosEnfermeria);
    $porcentajeAlumnosMedicina = ($numAlumnosEvaluacionesMedicina*100/$totalAlumnosMedicina);
    $porcentajeAlumnosTotal = (($numAlumnosEvaluacionesEnfermeria+$numAlumnosEvaluacionesMedicina)*100/($totalAlumnosEnfermeria+$totalAlumnosMedicina));
    foreach ($participacionalumnos as $nalumnosxseccion){
        $totalAlumnos=$this->capturaCalificacionesModel->n_alumnos_secciones($periodoid,$nalumnosxseccion->seccion);
        $n=intval($nalumnosxseccion->n_alumnos_p);
        $pp=($n*100)/$totalAlumnos->n_alumnos;
        $arrayParticipacionAlumnos[$nalumnosxseccion->seccion]=$pp;
        };
    $data['periodo'] = $periodoid;
    $data['arrayParticipacionAlumnos'] = $arrayParticipacionAlumnos;
    $data['porcentajeAlumnosEnfermeria'] = $porcentajeAlumnosEnfermeria;
    $data['porcentajeAlumnosMedicina'] = $porcentajeAlumnosMedicina;
    $data['porcentajeAlumnosTotal'] = $porcentajeAlumnosTotal;
    $data['menu'] = $this->sesion->get('menu');
    return view('evaluacion/participacionalumnos',$data);
}

/**
 * muestra el procentaje de participación en la evaluación docente
 * @param periodoid periodo de la evaluacion a consultar la particiácion de los alumnos
 * @param evaluacion evaluacion a consultar la particiácion de los alumnos
 */
public function participacionalumnosv2($periodoid,$evaluacion){
    $porcentajeAlumnosEnfermeria=0;
    $porcentajeAlumnosMedicina=0;
    $porcentajeAlumnosTotal=0;
    $totalAlumnosEnfermeria=0;
    $totalAlumnosMedicina=0;
    $totalRespAlumnosEnfermeria=0;
    $totalRespAlumnosMedicina=0;
    $arrayestpartAlumnos=[];
    $sec_resp=$this->estadisticaEvaluacionDocenteModel->est_secciones($evaluacion);
    foreach ($sec_resp as $seccion){
        $gruposxseccion=$this->grupoModel->getBySeccion($seccion->seccion,$periodoid);
        $n_respuestasXseccion=0;
        $n_alumnosXseccion=0;
        $preguntas = $this-> getArrPregruntas($evaluacion);
        foreach ($gruposxseccion as $grupos){
            //se calcula el total de cuestionarios que DEBEN contestarse
            $n_alumnosgrupo =$this->grupoAlumnosModel->n_alumnosxgrupov2($grupos->id);
            $n_alumnosXseccion=$n_alumnosXseccion+$n_alumnosgrupo->n_alumnos;
            //se calcula el total de cuestionarios contestados
            $n_alumnos=$this->evaluacionesRespuestasModel->getNumAlumnosxGrupo($grupos->id,$preguntas);
            $n_respuestasXseccion=$n_respuestasXseccion+$n_alumnos->n_alumnos;            
            //se calculan totales tanto de medicina como de enfermería
            if(substr($seccion->seccion,0,3)=='MED'){
                $totalAlumnosMedicina=$totalAlumnosMedicina+$n_alumnosgrupo->n_alumnos;
                $totalRespAlumnosMedicina= $totalRespAlumnosMedicina+$n_alumnos->n_alumnos;
            }
            if(substr($seccion->seccion,0,3)=='LEO'){
                $totalAlumnosEnfermeria=$totalAlumnosEnfermeria+$n_alumnosgrupo->n_alumnos;
                $totalRespAlumnosEnfermeria= $totalRespAlumnosEnfermeria+$n_alumnos->n_alumnos;
            }
        }
        $arrayestpartAlumnos[$seccion->seccion]=($n_respuestasXseccion*100)/$n_alumnosXseccion;
    }
    $porcentajeAlumnosEnfermeria=($totalRespAlumnosEnfermeria*100)/$totalAlumnosEnfermeria;
    $porcentajeAlumnosMedicina=($totalRespAlumnosMedicina*100)/$totalAlumnosMedicina;
    $porcentajeAlumnosTotal=(($totalRespAlumnosEnfermeria+$totalRespAlumnosMedicina)*100)/($totalAlumnosEnfermeria+$totalAlumnosMedicina);
    $data['periodo'] = $periodoid;
    $data['arrayParticipacionAlumnos'] = $arrayestpartAlumnos;
    $data['porcentajeAlumnosEnfermeria'] = $porcentajeAlumnosEnfermeria;
    $data['porcentajeAlumnosMedicina'] = $porcentajeAlumnosMedicina;
    $data['porcentajeAlumnosTotal'] = $porcentajeAlumnosTotal;
    $data['menu'] = $this->sesion->get('menu');
    return view('evaluacion/participacionalumnosv2',$data);
}

    /**
     * muestra a los docentes evaluados con participación representativa
     */

    public function evaluaciondocentes()
    {
        $periodoid = $this->periodoModel->periodoFsys();
        $resultadosdocentes = $this->estadisticaEvaluacionDocenteModel->resultados_docentes($periodoid->periodo_id);
        foreach ($resultadosdocentes as $calificacionesdocentes) {
            $pp = ($nalumnosxseccion->n_alumnos_p * 100) / $totalAlumnos;
            $arrayParticipacionAlumnos[$nalumnosxseccion->seccion] = $pp;
        }
        $data['arrayparticipacionxgrupo'] = 0;
        $data['periodo'] = $periodoid;
        $data['arrayParticipacionAlumnos'] = $arrayParticipacionAlumnos;
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/participacionalumnos', $data);
    }

    /**
     * depliga los resultados de la vista generada
     */
    public function Estadisticos()
    {

        $data['respuestas'] = false;
        $data['periodos'] = $this->periodoModel->lista();
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/estadisticas', $data);
    }


    /**
     * retorna un JSON con las evaluaciones disponibles en el periodo indicado
     * @param periodo ID del periodo
     */
    public function getEvaluacionesPeriodos($periodo)
    {
        $evaluaciones = $this->evaluacionModel->buscar(['periodo' => $periodo]);
        $this->response->setHeader('Content-Type', 'application/json');
        if ($evaluaciones) {
            echo json_encode($evaluaciones);
        }
        return false;
    }

    /**
     * retorna los datos estadisticos de la evaluacion seleccionada
     * @param evaluacion ID de la evaluacion seleccionada
     */
    public function getDatosEstadisticos($periodo, $evaluacion)
    {
        $estadistica = new Estadisticas();
        $datos = $estadistica->getlistacarpeta($periodo, $evaluacion);
        $this->response->setHeader('Content-Type', 'application/json');
        if ($datos) {
            echo json_encode($datos);
        }
        return false;
    }

    /**
     * retorna a que carrera pertenece un grupo seleccionadp
     * @param grupo IDC del grupo
     */
    public function getCarrera($grupo)
    {
        $grupo_model = $this->grupoModel->find($grupo);
        // echo json_encode($grupo_model);
        if ($grupo_model) {
            $dato = explode('-', $grupo_model->clave);
            return (($dato[0] == 'LEO') ? 'LEO' : 'MED');
        }
        return 'null';
    }

    /**
     * devuelve un arreglo de alumnos que contestaron al menos una pregunta de la evaluacion seleccionada
     * @param grupo ID del grupo que se esta analizando
     * @param array_preguntas arreglo de preguntas que pertenecen a una evaluacion
     * @return count Alumnos que contestaron a la evaluacion
     */
    public function getAlumnosEvaluacion($grupo, $array_preguntas)
    {
        $resp = $this->respuestasModel->getAlumnosGrupo($grupo, $array_preguntas);
        if (!is_null($resp))
            return count($resp);
        return 0;
    }

    /**
     * retorna el arreglo de las preguntas pertenecientes a una evaluacion
     */
    private function getArrPregruntas($evaluacion)
    {
        $formularios = $this->fromularioModel->getByEvaluacion($evaluacion);
        $array_preguntas = [];
        foreach ($formularios as $formulario) {
            $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
            foreach ($preguntas as $pregunta) {
                $array_preguntas[] = $pregunta->id;
            }
        }
        return $array_preguntas;
    }

    /**
     * forma un vector con las suma de las respuestas de la evaluacion
     * @param clasificacion rasgo a evaluar
     * @param arrayrespuestas array con las respuestas
     * @return arrayclasificacion vector con el factor para sacar el porcentaje del razgo a evaluar
     */
    public function forma_arreglo($clasificacion, $arrayrespuestas)
    {
        $arrayclas = [];
        $index = 0;
        switch ($clasificacion) {
            case 1:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    //$dato=intval($arrayclasificacion->clasificacion1);
                    $arrayclas[$index] = $arrayclasificacion->clasificacion1;
                    $index++;
                }
                break;
            case 2:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion2;
                    $index++;
                }
                break;
            case 3:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion3;
                    $index++;
                }
                break;
            case 4:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion4;
                    $index++;
                }
                break;
        }
        return $arrayclas;
    }
    public function estadisticamaterias(){
        $arrayporcentajesDocentes[]=array(
            'empleado_id'               =>'',
            'app_pat'                   =>'',
            'app_mat'                   =>'',
            'nombre_docente'            =>'',
            'especialidad'              =>'',
            'materia_id'                =>'',
            'materia_nombre'            =>'',
            'calidad_recursos'          =>'',
            'pertenencia_academica'     =>'',
            'relacion_academica'        =>'',
            'transmision_conocimientos' =>'',
            'estadisticadocentes' => []
        );
        $data['estadisticadocentes'] = $arrayporcentajesDocentes;
        $data['menu'] = $this->sesion->get('menu');
        return view ('evaluacion/estadisticasDocentesMaterias',$data);
    }

    /**
     * crea el arreglo con las calificaciones de los docentes por materia de los 4 rubros
     * @param evaluacion id de la evaluacion de la cual se desea mostrar la estadística
     * @return arrayporcentajesDocentes arreglo con los datos de las calificaciones de los Docentes
     */
    public function calcularEstDocentesMaterias ($evaluacion,$periodo){
        $arrayporcentajesDocentes=array();
        $docentes = $this->grupoModel->getDocentes($periodo); //se obtienen todos los docentes
        $estadistica = new Estadisticas();
        $preguntas=$estadistica->getArrPregruntas($evaluacion);
        $grupos = $this->evaluacionesRespuestasModel->getGruposEvaluacion($preguntas);
        foreach ($docentes as $docenteid){
            $docente=$docenteid->grupo_docente;
            $materias=$this->grupoModel->getDocentesMaterias($periodo,$docente,$grupos); //se obtienen todas las materias de 1 docente y se contemplan sólo los grupos que hayan contestado la evaluacion
            $docenteinfo=$this->empleadosModel->findById($docente);
            foreach ($materias as $materiaid){
                $materia=$materiaid->grupo_materia;
                // se calcula el 100% de las respuestas
                $materianombre=$this->materiaModel->getMateriaById($materia)->materia_nombre;
                $nRespuestasClasificacion1 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo,$materia,$docente,1,$evaluacion );
                $nRespuestasClasificacion2 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo,$materia,$docente,2,$evaluacion );
                $nRespuestasClasificacion3 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo,$materia,$docente,3,$evaluacion );
                $nRespuestasClasificacion4 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo,$materia,$docente,4,$evaluacion );
                $totalC1=$nRespuestasClasificacion1->n_preguntas*4;
                $totalC2=$nRespuestasClasificacion2->n_preguntas*4;
                $totalC3=$nRespuestasClasificacion3->n_preguntas*4;
                $totalC4=$nRespuestasClasificacion4->n_preguntas*4;
                $porcentajes=$this->porcentajesEvaluacionModel->estxdocentematResp($evaluacion,$docente,$materia);
                $clasificacion1 = (($totalC1 == 0) ? 0 : ((array_sum($this->forma_arreglo(1, $porcentajes)) / $totalC1) * 100));
                $clasificacion2 = (($totalC2 == 0) ? 0 : ((array_sum($this->forma_arreglo(2, $porcentajes)) / $totalC2) * 100));
                $clasificacion3 = (($totalC3 == 0) ? 0 : ((array_sum($this->forma_arreglo(3, $porcentajes)) / $totalC3) * 100));
                $clasificacion4 = (($totalC4 == 0) ? 0 : ((array_sum($this->forma_arreglo(4, $porcentajes)) / $totalC4) * 100));
                $arrayporcentajesDocentes[]=array(
                    'empleado_id'               =>$docente,
                    'app_pat'                   =>$docenteinfo->empleado_ap_paterno,
                    'app_mat'                   =>$docenteinfo->empleado_ap_materno,
                    'nombre_docente'            =>$docenteinfo->empleado_nombre,
                    'materia_id'                =>$materia,
                    'especialidad'              =>(($materiaid->materia_carrera == 1) ? 'LEO' : 'LMC'),
                    'materia_nombre'            =>$materianombre,
                    'calidad_recursos'          =>$clasificacion1,
                    'pertenencia_academica'     =>$clasificacion2,
                    'relacion_academica'        =>$clasificacion3,
                    'transmision_conocimientos' =>$clasificacion4,
                    'global'                    =>($clasificacion1+$clasificacion2+$clasificacion3+$clasificacion4)/4,
                );
            }   
        }
        return $arrayporcentajesDocentes;
    }

    public function generartxtEvaluacionMateria($evaluacion)
    {
        $periodoid = $this->periodoModel->periodoFsys();
        $periodo =$periodoid->periodo_id;
        $evaluacion_model  = $this->evaluacionModel->find($evaluacion);
        $ruta = WRITEPATH . 'evaluacion/' . $evaluacion_model->periodo . '/';
        switch ($evaluacion_model->tipo) {
            case '1':
                $datos = $this->calcularEstDocentesMaterias($evaluacion,$periodo);
                try {
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $myfile = fopen($ruta . 'EVDOCMAT_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt", "w");
                    fwrite($myfile, json_encode($datos));
                    fclose($myfile);
                    log_message("notice", "Se ha generado un reporte EVDOCMAT de la evaluacion {id} {nombre} por job. Archivo de datos txt ({documento}) guardado con exito ", ['id' => $evaluacion_model->id, 'nombre' => $evaluacion_model->titulo, 'documento' => 'EVDOCMAT_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt"]);
                    return true;
                } catch (\Exception $ex) {
                    log_message("error", "ERROR al guardar el archivo de datos del documento {documento}: {exception}: ", ['exception' => $ex, 'documento' => 'EVDOCMAT_' . $evaluacion_model->id . "_" . date("Y_m_d_H_i_s") .  ".txt"]);
                    return false;
                }
                break;
        }
    }

    public function estDocentesMaterias ($periodo, $archivo, $evaluacion){
        $ruta = WRITEPATH . "/evaluacion/" . $periodo . '/';
        $data['estadisticadocentes'] = (json_decode(file_get_contents($ruta . $archivo), true));
        $data['estadisticadocentesAll'] = (json_decode(file_get_contents($ruta . $archivo), true));
        $data['menu'] = $this->sesion->get('menu');
        $data['tabla'] = '1';
        $data['check'] = '0';
        return view('evaluacion/estadisticasDocentesMaterias', $data);
    }

    private function estDocentesMaterias_old ($evaluacion){
        $periodoid = $this->periodoModel->periodoFsys();
        $periodo =$periodoid->periodo_id;
        $ruta = WRITEPATH . "/evaluacion/" . $periodo . '/';
        //$materias = (json_decode(file_get_contents($ruta . $archivo), true));
        $data['estadisticadocentes'] = $this->calcularEstDocentesMaterias($evaluacion,$periodo);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/estadisticasDocentesMaterias', $data);
    }


    /**
     * procesa los grupos pertenecientes al periodo de la evaluacion seleccionada para 
     * @param evaluacion ID de la evaluacion seleccionada   
     */
    public function BaseEVDOC($evaluacion)
    {
        $data['evaluacion'] = ($evaluacion);
        // $data['indicadores'] = $this->generarEVDOC($evaluacion);
        // $data['indicadores'] = Estadisticas::generarEVDOC($evaluacion);
        $data['menu'] = $this->sesion->get('menu');
        return view('evaluacion/evdoc', $data);
    }


    /**
     * Genera un archivo Excel con los datos del archivo de EVDOC
     * @param archivo  nombre del archivo con el que formar el archivo
     * @param periodo ID del periodo en el que se generó
     */
    public function ExcelEVDOC($periodo, $archivo, $evaluacion)
    {
        $ruta = WRITEPATH . "/evaluacion/" . $periodo . '/';
        $materias = (json_decode(file_get_contents($ruta . $archivo), true));
        $evaluacionModel = $this->evaluacionModel->find($evaluacion);
        $spreadsheet = new Spreadsheet();
        $index = 0;
        $spreadsheet->getProperties()->setCreator('USEP.DIGITAL')->setTitle($evaluacionModel->titulo)->setKeywords($periodo);
        $spreadsheet->setActiveSheetIndex($index)
            ->setCellValueByColumnAndRow(1, 1,  'Tipo')
            ->setCellValueByColumnAndRow(2, 1,  'PE')
            ->setCellValueByColumnAndRow(3, 1,  'Grupo')
            ->setCellValueByColumnAndRow(4, 1,  'GrupoS')
            ->setCellValueByColumnAndRow(5, 1,  'Asignatura')
            ->setCellValueByColumnAndRow(6, 1,  'Horas conducidas')
            ->setCellValueByColumnAndRow(7, 1,  'Horas por semana')
            ->setCellValueByColumnAndRow(8, 1,  'Tipo de contratación')
            ->setCellValueByColumnAndRow(9, 1,  'Docente')
            ->setCellValueByColumnAndRow(10, 1,  'Alumnos inscritos')
            ->setCellValueByColumnAndRow(11, 1,  'Participantes')
            ->setCellValueByColumnAndRow(12, 1,  'Significancia')
            ->setCellValueByColumnAndRow(13, 1,  '% participantes')
            ->setCellValueByColumnAndRow(14, 1,  'Calidad Recursos')
            ->setCellValueByColumnAndRow(15, 1,  'Pertinencia Aca')
            ->setCellValueByColumnAndRow(16, 1,  'Relación Aca')
            ->setCellValueByColumnAndRow(17, 1,  'Transmisión Conoc')
            ->setCellValueByColumnAndRow(18, 1,  'Promedio');
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('A', 'B', 'C', 'D')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'E')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'F')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'G')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'H')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'I')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'J')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'K')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'L')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'M')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'N')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'O')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'P')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'Q')->setAutoSize(true);
        $spreadsheet->setActiveSheetIndex($index)->getColumnDimension( 'R')->setAutoSize(true);
        $row = 2;
        foreach ($materias as $materia) {
            $spreadsheet->setActiveSheetIndex($index)
                ->setCellValueByColumnAndRow(1, $row,  $materia['tipo'])
                ->setCellValueByColumnAndRow(2, $row,  $materia['PE'])
                ->setCellValueByColumnAndRow(3, $row,  $materia['grupo'])
                ->setCellValueByColumnAndRow(4, $row,  $materia['grupoS'])
                ->setCellValueByColumnAndRow(5, $row,  $materia['asignatura'])
                ->setCellValueByColumnAndRow(6, $row,  $materia['horas_conducidas'])
                ->setCellValueByColumnAndRow(7, $row,  $materia['horas_semanales'])
                ->setCellValueByColumnAndRow(8, $row,  $materia['tipo_contratacion'])
                ->setCellValueByColumnAndRow(9, $row,  $materia['docente'])
                ->setCellValueByColumnAndRow(10, $row, $materia['alumnos_inscritos'])
                ->setCellValueByColumnAndRow(11, $row, $materia['alumnos_participantes'])
                ->setCellValueByColumnAndRow(12, $row, $materia['significancia'])
                ->setCellValueByColumnAndRow(13, $row, $materia['participacion'])
                ->setCellValueByColumnAndRow(14, $row, $materia['calidad_recursos'])
                ->setCellValueByColumnAndRow(15, $row, $materia['pertenencia_academica'])
                ->setCellValueByColumnAndRow(16, $row, $materia['relacion_academica'])
                ->setCellValueByColumnAndRow(17, $row, $materia['transmision_conocimientos'])
                ->setCellValueByColumnAndRow(18, $row, $materia['promedio']);
            $row++;
        }
        // $spreadsheet->getActiveSheet()->setTitle('libro 1');
        $spreadsheet->getActiveSheet()->setTitle($evaluacionModel->titulo);
        $spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $titulo = str_replace(' ', '_', $evaluacionModel->titulo).".Xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$titulo.'"');
        header('Cache-Control: max-age=0');
         
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;

        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="' . $titulo . '"');
        // header('Cache-Control: max-age=0');

        // $writer = IOFactory::createWriter($titulo, 'Xlsx');
        // $writer->save('php://output');
        // exit;
        // $archivo = fopen($direcctorio . '/Lista_' . $seccion . '_' . $periodo . '.xlsx', 'r');
        // if ($archivo) {
        //     fclose($archivo);
        //     return 'Lista_' . $seccion . '_' . $periodo . '.xlsx';
        // } else return false;
    }
    /**
     * despliega la vista de las estadísticas dependiendo del filtro por el cual se requiere consultar
     */
    public function filtrosDeEstadistica(){
        $arreglodatos = json_decode($this->request->getPost('arreglodatos'),true);
        $selectcontratacion = $this->request->getPost('selectcontratacion');
        $selectcarrera = $this->request->getPost('selectcarrera');
        if($selectcontratacion != null){
            $arraydatos = $this->filtrosxContratacion($arreglodatos,$selectcontratacion)['datos'];
            $tabla = $this->filtrosxContratacion($arreglodatos,$selectcontratacion)['tabla'];
            if($tabla == 'tabla'){
                $data['tabla'] = $tabla;
            }
            elseif($tabla == 'resumen'){
                $data['resumen'] = $tabla;
            }
        }
        if($selectcarrera != null){
            $arraydatos = $this->filtrosxCarrera($arreglodatos,$selectcarrera);
            $data['tabla'] = '';
        }
        $data['check'] = '0';
        $data['estadisticadocentes'] = $arraydatos;
        $data['estadisticadocentesAll'] = $arreglodatos;
        $data['menu'] = $this->sesion->get('menu');
        return view ('evaluacion/estadisticasDocentesMaterias',$data);
    }
    /**
     * Genera un arreglo con los datos dependiendo de tipo de contratación seleccionada
     * @param arreglodatos arreglo con todos los datos a validar
     * @param  selectcontratacion valor del tipo de contratación que se seleccionó
     * @return arraydatos Arreglo con la información generada por el tipo de contratación que se seleccionó
     */
    public function filtrosxContratacion($arreglodatos,$selectcontratacion){
        $array = [];
            if($selectcontratacion == 'tiempocompleto'){
                foreach($arreglodatos as $datos){
                    if($datos['empleado_id'] < '200000'){
                        $array[]=array(
                            'empleado_id'               =>$datos['empleado_id'],
                            'app_pat'                   =>$datos['app_pat'],
                            'app_mat'                   =>$datos['app_mat'],
                            'nombre_docente'            =>$datos['nombre_docente'],
                            'materia_id'                =>$datos['materia_id'],
                            'especialidad'              =>$datos['especialidad'],
                            'materia_nombre'            =>$datos['materia_nombre'],
                            'calidad_recursos'          =>$datos['calidad_recursos'],
                            'pertenencia_academica'     =>$datos['pertenencia_academica'],
                            'relacion_academica'        =>$datos['relacion_academica'],
                            'transmision_conocimientos' =>$datos['transmision_conocimientos'],
                            'global'                    =>$datos['global']
                        );
                    }
                }
                $arraydatos['datos']= $array;
                $arraydatos['tabla']= 'tabla';
            }
            elseif($selectcontratacion == 'horaclase'){
                foreach($arreglodatos as $datos){
                    if($datos['empleado_id'] >= '200000'){
                        $array[]=array(
                            'empleado_id'               =>$datos['empleado_id'],
                            'app_pat'                   =>$datos['app_pat'],
                            'app_mat'                   =>$datos['app_mat'],
                            'nombre_docente'            =>$datos['nombre_docente'],
                            'especialidad'              =>$datos['especialidad'],
                            'materia_id'                =>$datos['materia_id'],
                            'materia_nombre'            =>$datos['materia_nombre'],
                            'calidad_recursos'          =>$datos['calidad_recursos'],
                            'pertenencia_academica'     =>$datos['pertenencia_academica'],
                            'relacion_academica'        =>$datos['relacion_academica'],
                            'transmision_conocimientos' =>$datos['transmision_conocimientos'],
                            'global'                    =>$datos['global']
                        );
                    }
                }
                $arraydatos['datos']= $array;
                $arraydatos['tabla']= 'tabla';
            }elseif($selectcontratacion == 'resumen'){
                $arraydatos['datos'] = $this->promedioxcontratacion($arreglodatos);
                $arraydatos['tabla']= 'resumen';
            }
        return $arraydatos;
    }
    /**
     * Genera los promedios por contratación (capitulo 1000,2000)
     * @param arraydatos arreglo con todos los datos a validar
     * @return arreglopromedios contiene los promedios generales por categoría de contratacion
     */
    private function promedioxcontratacion($arraydatos){
        $capitulomil = 0;
        $crmil = 0;
        $pamil = 0;
        $ramil = 0;
        $tcmil = 0;
        $globalmil = 0;
        $capitulodosmil = 0;
        $crdosmil = 0;
        $padosmil = 0;
        $radosmil = 0;
        $tcdosmil = 0;
        $globaldosmil = 0;
        foreach($arraydatos as $datos){
            if($datos['empleado_id'] < '200000'){
                $capitulomil = $capitulomil+1;
                $crmil = $crmil + $datos['calidad_recursos'];
                $pamil = $pamil + $datos['pertenencia_academica'];
                $ramil = $ramil + $datos['relacion_academica'];
                $tcmil = $tcmil + $datos['transmision_conocimientos'];
                $globalmil = $globalmil + $datos['global'];
            }
            if($datos['empleado_id'] >=  '200000'){
                $capitulodosmil = $capitulodosmil+1;
                $crdosmil = $crdosmil + $datos['calidad_recursos'];
                $padosmil = $padosmil + $datos['pertenencia_academica'];
                $radosmil = $radosmil + $datos['relacion_academica'];
                $tcdosmil = $tcdosmil + $datos['transmision_conocimientos'];
                $globaldosmil = $globaldosmil + $datos['global'];
            }
        }
        $arreglopromedios[] =[
            'tipocontratacion'  => 'CAPITULO 1000',
            'calidad_recursos'          =>substr($crmil/$capitulomil, 0, 5),
            'pertenencia_academica'     =>substr($pamil/$capitulomil, 0, 5),
            'relacion_academica'        =>substr($ramil/$capitulomil, 0, 5),
            'transmision_conocimientos' =>substr($tcmil/$capitulomil, 0, 5),
            'global'                    =>substr($globalmil/$capitulomil, 0, 5)
        ];
        $arreglopromedios + $arreglopromedios[] =[
            'tipocontratacion'  => 'CAPITULO 2000',
            'calidad_recursos'          =>substr($crdosmil/$capitulodosmil, 0, 5),
            'pertenencia_academica'     =>substr($padosmil/$capitulodosmil, 0, 5),
            'relacion_academica'        =>substr($radosmil/$capitulodosmil, 0, 5),
            'transmision_conocimientos' =>substr($tcdosmil/$capitulodosmil, 0, 5),
            'global'                    =>substr($globaldosmil/$capitulodosmil, 0, 5)
        ];
        $arreglopromedios + $arreglopromedios[] =[
            'tipocontratacion'  => 'TOTAL,FINAL',
            'calidad_recursos'          =>substr((substr($crdosmil/$capitulodosmil, 0, 5) + substr($crmil/$capitulomil, 0, 5))/2,0,5),
            'pertenencia_academica'     =>substr((substr($padosmil/$capitulodosmil, 0, 5) + substr($pamil/$capitulomil, 0, 5))/2,0,5),
            'relacion_academica'        =>substr((substr($radosmil/$capitulodosmil, 0, 5) + substr($ramil/$capitulomil, 0, 5))/2,0,5),
            'transmision_conocimientos' =>substr((substr($tcdosmil/$capitulodosmil, 0, 5) + substr($tcmil/$capitulomil, 0, 5))/2,0,5),
            'global'                    =>substr((substr($globaldosmil/$capitulodosmil, 0, 5) + substr($globalmil/$capitulomil, 0, 5))/2,0,5)
        ];
        return $arreglopromedios;
    }
    /**
     * Genera un arreglo de las estadisticas dependiendo de la carrera solicitada
     * @param arreglodatos conjunto de datos de la evaluacion docente
     * @param selectcarrera nombre de la carrera que se selecciono
     * @return arraydatos Arreglo de evaluacion docente filtrada por la carrera seleccionada
     */
    public function filtrosxCarrera($arreglodatos,$selectcarrera){
        $arraydatos = [];
        foreach($arreglodatos as $datos){
            if($selectcarrera == 'leo'){
                if(substr($datos['materia_id'], 0, 3) == 'LEO'){
                    $arraydatos[]=array(
                        'empleado_id'               =>$datos['empleado_id'],
                        'app_pat'                   =>$datos['app_pat'],
                        'app_mat'                   =>$datos['app_mat'],
                        'nombre_docente'            =>$datos['nombre_docente'],
                        'especialidad'              =>$datos['especialidad'],
                        'materia_id'                =>$datos['materia_id'],
                        'materia_nombre'            =>$datos['materia_nombre'],
                        'calidad_recursos'          =>$datos['calidad_recursos'],
                        'pertenencia_academica'     =>$datos['pertenencia_academica'],
                        'relacion_academica'        =>$datos['relacion_academica'],
                        'transmision_conocimientos' =>$datos['transmision_conocimientos'],
                        'global'                    =>$datos['global']
                    );
                }
            }
            elseif($selectcarrera == 'med'){
                if(substr($datos['materia_id'], 0, 2) == 'MC'){
                    $arraydatos[]=array(
                        'empleado_id'               =>$datos['empleado_id'],
                        'app_pat'                   =>$datos['app_pat'],
                        'app_mat'                   =>$datos['app_mat'],
                        'nombre_docente'            =>$datos['nombre_docente'],
                        'especialidad'              =>$datos['especialidad'],
                        'materia_id'                =>$datos['materia_id'],
                        'materia_nombre'            =>$datos['materia_nombre'],
                        'calidad_recursos'          =>$datos['calidad_recursos'],
                        'pertenencia_academica'     =>$datos['pertenencia_academica'],
                        'relacion_academica'        =>$datos['relacion_academica'],
                        'transmision_conocimientos' =>$datos['transmision_conocimientos'],
                        'global'                    =>$datos['global']
                    );
                }
            }
        }
        return $arraydatos;
    }
    /**
     * Genera el reporte de estadídticas englobando resultados por docente
     */
    public function filtroEstadisticaGeneral(){
        $arreglodatos = json_decode($this->request->getPost('arreglodatos'),true);
        $switch = $this->request->getPost('switchreporte');
        if($switch == 1){
            $iddocentes =$this->obtenerIdDocente($arreglodatos);
            foreach($iddocentes as $iddocente){
                $datospromedio = $this->generapromedios($iddocente,$arreglodatos);
                $arraydatos[]=array(
                    'empleado_id'               =>$iddocente,
                    'app_pat'                   =>$datospromedio['appaterno'],
                    'app_mat'                   =>$datospromedio['apmaterno'],
                    'nombre_docente'            =>$datospromedio['nombre'],
                    'especialidad'              =>$datospromedio['especialidad'],
                    'calidad_recursos'          =>substr($datospromedio['calidad_recursos'], 0, 5),
                    'pertenencia_academica'     =>substr($datospromedio['pertenencia_academica'],0,5),
                    'relacion_academica'        =>substr($datospromedio['relacion_academica'],0,5),
                    'transmision_conocimientos' =>substr($datospromedio['transmision_conocimientos'],0,5),
                    'global'                    =>substr($datospromedio['global'],0,5)
                );
            }
            $data['check'] = '1';
            $data['estadisticadocentes'] = $arraydatos;
            $data['estadisticadocentesAll'] = $arreglodatos;
            $data['general']= 'general';
            $data['menu'] = $this->sesion->get('menu');
            return view ('evaluacion/estadisticasDocentesMaterias',$data);
        }
        else{
            foreach($arreglodatos as $datos){
                $arraydatos[]=array(
                    'empleado_id'               =>$datos['empleado_id'],
                    'app_pat'                   =>$datos['app_pat'],
                    'app_mat'                   =>$datos['app_mat'],
                    'nombre_docente'            =>$datos['nombre_docente'],
                    'especialidad'              =>$datos['especialidad'],
                    'materia_id'                =>$datos['materia_id'],
                    'materia_nombre'            =>$datos['materia_nombre'],
                    'calidad_recursos'          =>$datos['calidad_recursos'],
                    'pertenencia_academica'     =>$datos['pertenencia_academica'],
                    'relacion_academica'        =>$datos['relacion_academica'],
                    'transmision_conocimientos' =>$datos['transmision_conocimientos'],
                    'global'                    =>$datos['global']
                );
            }
            $data['check'] = '0';
            $data['estadisticadocentes'] = $arraydatos;
            $data['estadisticadocentesAll'] = $arreglodatos;
            $data['tabla']= 'tabla';
            $data['menu'] = $this->sesion->get('menu');
            return view ('evaluacion/estadisticasDocentesMaterias',$data);
        }
    }
    /**
     * Genera un array de los id de docentes generados en el reporte
     * @return arreglo de los ids de los docentes sin repetirse
     */
    private function obtenerIdDocente($arreglodatos){
        $iddocentes = null;
        foreach($arreglodatos as $datos){
            $iddocentes[] = $datos['empleado_id'];
        }
        return array_unique($iddocentes);
    }
    /**
     * Realiza el calculo de promedio de cada campo evaluado por docente
     * @param iddocente valor de id de docente
     * @param arreglodatos arreglo que contiene todos los datos de la evaluación docente
     * @return promedios arreglo de promedios por categoría calificada del docente
     */
    private function generapromedios($iddocente,$arreglodatos){
        $calidad = 0;
        $pertenecia = 0;
        $relacion = 0;
        $transmision = 0;
        $global = 0;
        $arreglopromedios = [];
        foreach($arreglodatos as $datos){
            if($datos['empleado_id'] == $iddocente){
                if($datos['calidad_recursos']){
                    $calidad++;
                    $arreglopromedios['calidad_recursos'][] = $datos['calidad_recursos'];
                }
                if($datos['pertenencia_academica']){
                    $pertenecia++;
                    $arreglopromedios['pertenencia_academica'][]= $datos['pertenencia_academica'];
                }
                if($datos['relacion_academica']){
                    $relacion++;
                    $arreglopromedios['relacion_academica'][] = $datos['relacion_academica'];
                }
                if($datos['transmision_conocimientos']){
                    $transmision++;
                    $arreglopromedios['transmision_conocimientos'][] = $datos['transmision_conocimientos'];
                }
                if($datos['global']){
                    $global++;
                    $arreglopromedios['global'][] = $datos['global'];
                }
                $promedios['nombre'] = $datos['nombre_docente'];
                $promedios['appaterno'] = $datos['app_pat'];
                $promedios['apmaterno'] = $datos['app_mat'];
                $promedios['especialidad'] = $datos['especialidad'];
            }
        }
        $promedios['calidad_recursos'] = array_sum($arreglopromedios['calidad_recursos'])/$calidad;
        $promedios['pertenencia_academica'] = array_sum($arreglopromedios['pertenencia_academica'])/$pertenecia;
        $promedios['relacion_academica'] = array_sum($arreglopromedios['relacion_academica'])/$relacion;
        $promedios['transmision_conocimientos'] = array_sum($arreglopromedios['transmision_conocimientos'])/$transmision;
        $promedios['global'] = array_sum($arreglopromedios['global'])/$global;
        return $promedios;
    }
}
