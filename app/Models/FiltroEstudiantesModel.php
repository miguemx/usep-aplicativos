<?php

namespace App\Models;

use CodeIgniter\Model;

class FiltroEstudiantesModel extends Model
{
    protected $table = 'view_filtro_estudiantes';
    protected $primaryKey = 'ID';

    protected $returnType = 'App\Entities\FiltroAlumnos';
    protected $useSoftDeletes = false;
    protected $allowedFields = [
        'ID',
        'apPaterno',
        'apMaterno',
        'nombre',
        'correo',
        'sexo',
        'folio',
        'diabetes',
        'hipertension',
        'cardiacos',
        'enfermedadRespiratoria',
        'otroPadecimiento',
        'esFumador',
        'fiebre',
        'tos',
        'dificultadRespiratoria',
        'dolorCabeza',
        'dolorMuscular',
        'dolorArticular',
        'fatiga',
        'dolorAbdominal',
        'alteracionGusto',
        'alteracionOlfato',
        'escurrimientoNasal',
        'conjuntivitis',
        'escalofrios',
        'estuvoViaje',
        'contactoPersonaCovid',
        'temperatura',
        'oxigenacion',
        'fecha',
        'vacuna',
        'fecha_prueba_positiva',
        'fecha_prueba_negativo',
        'fecha_contactocovid',
        'tipo_tos'
    ];

    public function registros($sexo,$fecha,$fechafin)
    {
        $this->where('sexo',$sexo);
        $this->where('fecha <=',$fechafin);
        $this->where('fecha >=',$fecha);
        $this->orderBy('fecha','ASC');
        $registros = $this->findAll();
       //$registros = $this->getCompiledSelect();
       return $registros;
    }
}
