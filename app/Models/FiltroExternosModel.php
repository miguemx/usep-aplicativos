<?php

namespace App\Models;

use CodeIgniter\Model;

class FiltroExternosModel extends Model
{
    protected $table = 'view_filtro_externos';
    protected $primaryKey = 'correo';

    protected $returnType = 'App\Entities\FiltroExternos';
    protected $useSoftDeletes = false;
    protected $allowedFields = [
        'nombre','correo','dependencia','rango_edad','sexo','categoria','folio',
        'diabetes','hipertension','cardiacos','enfermedadRespiratoria','otroPadecimiento','esFumador','fiebre','tos','dificultadRespiratoria',
        'dolorCabeza','dolorMuscular','dolorArticular','fatiga','dolorAbdominal','alteracionGusto','alteracionOlfato','escurrimientoNasal',
        'conjuntivitis',
        'escalofrios',
        'estuvoViaje',
        'contactoPersonaCovid',
        'temperatura',
        'oxigenacion',
        'fecha',
        'vacuna',
        'fecha_prueba_positiva',
        'fecha_prueba_negativo',
        'fecha_contactocovid',
        'tipo_tos'
    ];

    public function registros($sexo,$fecha,$fechafin)
    {
         $this->where('sexo',$sexo);
         $this->where('fecha <=',$fechafin);
         $this->where('fecha >=',$fecha);
         $this->orderBy('fecha','ASC');
         $registros = $this->findAll();
        //$registros = $this->getCompiledSelect();
        return $registros;
    }
}
