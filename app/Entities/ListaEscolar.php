<?php

namespace App\Entities;

use CodeIgniter\Entity;

class ListaEscolar extends Entity
{
    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'gpoalumno' => null,
        'app_estudiante' => null,
        'apm_estudiante' => null,
        'nombre_estudiante' => null,
        'gpoalumnogpo' => null,
        'id_grupo' => null,
        'idc_grupo' => null,
        'clave_grupo' => null,
        'folio_grupo' => null,
        'materia_grupo' => null,
        'id_docente' => null,
        'app_docente' => null,
        'apm_docente' => null,
        'nombre_docente' => null,
        'periodo_grupo' => null,
        'clave_materia' => null,
        'numero_materia' => null,
        'nombre_materia' => null,
        'nombre_materiaCorto' => null,
        'creditos_materia' => null,
        'obligatoria_materia' => null,
        'tipo_materia' => null,
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'gpoalumno' => "gpoalumno_alumno",
        'app_estudiante' => "Ap_paterno",
        'apm_estudiante' => "Ap_materno",
        'nombre_estudiante' => "Alumno_nombres",
        'gpoalumnogpo' => "gpoalumno_grupo",
        'id_grupo' => "grupo_id",
        'idc_grupo' => "grupo_idc",
        'clave_grupo' => "grupo_clave",
        'folio_grupo' => "grupo_folio",
        'materia_grupo' => "grupo_materia",
        'id_docente' => "grupo_docente",
        'app_docente' => "docente_ap_paterno",
        'apm_docente' => "docente_ap_materno",
        'nombre_docente' => "docente_nombre",
        'periodo_grupo' => "grupo_periodo",
        'clave_materia' => "materia_clave",
        'numero_materia' => "materia_carrera",
        'nombre_materia' => "materia_nombre",
        'nombre_materiaCorto' => "materia_nombre_corto",
        'creditos_materia' => "materia_creditos",
        'obligatoria_materia' => "materia_obligatoria",
        'tipo_materia' => "materia_tipo",

       
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}