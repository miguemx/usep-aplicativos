<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<div class="container ">
    <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2 ">
        <div class="row">
            <div class="col">
                <form action="<?php echo base_url('ProgramacionAcademica/MapaAulas/101'); ?>" method="post">
                    <button type="submit" class="btn btn-secondary">Asignar horarios Presenciales</button>
                </form>
            </div>
            <div class="col">
                <form action="<?php echo base_url('ProgramacionAcademica/AulaVirtual/'); ?>" method="post">
                    <button type="submit" class="btn btn-secondary">Asignar horarios En linea</button>
                </form>
            </div>
            <div class="col">
                <form action="<?php echo base_url('ProgramacionAcademica/HorarioDocente/'); ?>" method="post">
                    <button type="submit" class="btn btn-secondary">Horarios de docentes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>