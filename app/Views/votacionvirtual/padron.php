<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
<?php if (isset($noregistro)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $noregistro; ?>
        </div>
        <?php endif; ?>
<?php if(isset($votorealizado)) :?>
            <div class="alert alert-danger" role="alert">
                <?php echo $votorealizado; ?>
            </div>
        <?php endif; ?>
<?php if (isset($mensajeexito)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensajeexito; ?>
    </div>
<?php endif; ?><?php if (isset($error)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $error; ?>
    </div>
<?php endif; ?>
<?php if (isset($seleccionaproceso)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $seleccionaproceso; ?>
    </div>
<?php endif; ?>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/voto'); ?>">
        <h1>Votación</h1>
                <div class="correo">
                    <label for="correoproceso" class="form-label">Correo Votante</label>
                    <input type="text" disabled class="form-control" style="justify-content:center" id="correoproceso" name="correoproceso" value="<?php echo $correo; ?>" required>
                </div>
                <div class="idproceso">
                    <label for="selectproceso" class="form-label">Procesos</label>
                    <br>
                    <select id="selectproceso " class="custom-select" style="width: 80%;" name="selectproceso" onchange="this.form.submit();" required >
                        <option value="" >Selecciona procesos</option>
                        <?php foreach ($procesos as $arregloprocesos) : ?>
                            <option value = <?php echo $arregloprocesos->id ?> <?php if($arrprocesos== $arregloprocesos->id) echo 'selected="selected"'; ?> > <?php
                                    echo $arregloprocesos->nombre
                            ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
    </form>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/registrovoto'); ?>">
        <div class="table-responsive">
                    <table name='calif_arr' class="table table-light text-center">
                        <thead>
                            <tr>
                                <th class="th-head">No</th>
                                <th class="th-head">Nombre de proceso</th>
                                <th class="th-head">Correo de candidato</th>
                                <th class="th-head">Nombre de candidato</th>
                                <th class="th-head">Voto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $contador = 0;
                            ?>
                        <?php foreach ($candidatos as $candidato) : ?>
                            <?php $contador = $contador + 1; ?>
                                <tr>
                                    <td><?php echo $contador; ?></td>
                                    <td><?php echo $candidato->nombre_proceso ; ?></td>
                                    <td><?php echo $candidato->correo_candidatos ; ?></td>
                                    <td><?php echo $candidato->nombre_candidatos ; ?></td>
                                    <?php echo "<td><input type='checkbox' class='checkbx' onChange='comprobar(this);' id='candidato[]' name='idcandidato[]' value='$candidato->id_candidatos'></td>";?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
        </div>
        <div class="guardar" style="display:flex; justify-content:center">
            <input name="correo" type="hidden" value="<?php echo $correo2; ?>" />
            <input name="proceso" type="hidden" value="<?php echo $proceso2; ?>" />
            <button type="submit" class="btn btn-secondary" id="btnvoto" name= "voto" value='1' disabled >Votar</button>
            <button style="margin-left: 20px;" type="submit" class="btn btn-secondary" id="btnabstencion" name='voto' value='0'>Abstener</button>
        </div>
    <form>
</div>
<script>
 function habilitaabstener(elemento)  
{  
    console.log(elemento);
    //checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
    btnabstencion = document.getElementById('btnabstencion');
    if(elemento != ""){

        btnabstencion.disabled = false;
    }else{
        btnabstencion.disabled = true;
    }
}    
</script>
<script>
function comprobar(elemento)  
{  
    console.log(elemento.value);
    checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
    boton = document.getElementById('btnvoto')
    btnabstencion = document.getElementById('btnabstencion')	
        for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
		{
            console.log(i);
			if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
			{
                if(checkboxes[i].value != elemento.value){//condicion para omitir el checkbox seleccionado
                    checkboxes[i].checked=false; //si es un checkbox le damos el valor del checkbox que lo llamó
                    console.log("entra ");
                    boton.disabled = false;
                    //btnabstencion.disabled = false;
                     
                }
                if(i < 4){
                    boton.disabled = false;
                    //btnabstencion.disabled = false;
                }
			}
            console.log(elemento.checked);
            if(elemento.checked == false){
                boton.disabled = true;
                //btnabstencion.disabled = true;
            }
		}
}  
</script>
<?php echo $this->endSection() ?>