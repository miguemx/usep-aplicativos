<?php $pager->setSurroundCount(2) ?>

<style type="text/css">
    .pager-container {
        padding: 10px;
        text-align: center;
    }
    .pager-container > nav {
        margin: 0; padding: 0;
    }
    .pager-container > nav > ul { margin: 0; padding: 0; }
    .pagination {
        justify-content: center;
        align-items: center;
    }
    .pagination li > a {
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        user-select: none;
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
        font-size: 0.785rem;
        border-radius: 0.2rem;
        padding: 0.25rem 0.5rem;
        cursor: pointer;
        margin: 2px;
    }
</style>

<nav aria-label="Page navigation">
    <ul class="pagination">
        <li>
            <a href="<?= $pager->getFirst() ?>" aria-label="<?= lang('Pager.first') ?>">
                <span aria-hidden="true">|&lt;</span>
            </a>
        </li>
    <?php if ($pager->hasPrevious()) : ?>
        
        <li>
            <a href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
                <span aria-hidden="true">&lt;&lt;</span>
            </a>
        </li>
    <?php endif ?>

    <?php foreach ($pager->links() as $link): ?>
        <li <?= $link['active'] ? 'class="active"' : '' ?>>
            <a href="<?= $link['uri'] ?>">
                <?= $link['title'] ?>
            </a>
        </li>
    <?php endforeach ?>

    <?php if ($pager->hasNext()) : ?>
        <li>
            <a href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
                <span aria-hidden="true">&gt;&gt;</span>
            </a>
        </li>
        
    <?php endif ?>
        <li>
            <a href="<?= $pager->getLast() ?>" aria-label="<?= lang('Pager.last') ?>">
                <span aria-hidden="true">&gt;|</span>
            </a>
        </li>
    </ul>
</nav>