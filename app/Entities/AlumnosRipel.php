<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AlumnosRipel extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'matricula' => null,
        'ip' => null,
        'fecha' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'matricula' => 'ripel_matricula',
        'ip' => 'ripel_ip',
        'fecha' => 'ripel_fecha',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}