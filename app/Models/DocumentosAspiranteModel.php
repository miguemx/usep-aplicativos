<?php
namespace App\Models;
use CodeIgniter\Model;

class DocumentosAspiranteModel extends Model {
    protected $table      = 'prein_documentos';
    protected $primaryKey = 'documento_id';

    protected $returnType    = 'App\Entities\DocumentoAspirante';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'documento_aspirante','documento_tipo','documento_fecha','documento_ruta','documento_status',
        'documento_comentarios',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un registro de archivo de aspirante con base en su matricula y el tipo de archivo
     */
    public function busca($matricula, $tipo) {
        $documento = $this->where('documento_aspirante', $matricula)->where('documento_tipo', $tipo)->first();
        return $documento;
    }

    /**
     * obtiene todos los archivos de un aspirante 
     */
    public function getArchivos($matricula) {
        $archivos = $this->where('documento_aspirante', $matricula)->findAll();
        return $archivos;
    }

    /**
     * obtiene el archivo de foto del aspirante
     */
    public function getFoto($matricula) {
        $archivos = $this->where('documento_aspirante', $matricula)->where('documento_tipo','9')->findAll();
        if ( count($archivos) ) {
            return $archivos[0];
        }
        return null;
    }

}
