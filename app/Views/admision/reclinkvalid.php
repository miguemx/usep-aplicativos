<!--
=========================================================
* Material Dashboard 2 - v3.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('templates'); ?>assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url('favicon.ico'); ?>">
  <title>
    Registro de Aspirantes :: Universidad de la Salud
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="<?php echo base_url('templates'); ?>/material-dashboard/assets/css/material-dashboard.css?v=3.0.0" rel="stylesheet" />
</head>

<body class="">
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <?php echo $this->include('admision/menu'); ?>
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
              <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center" style="background-image: url('<?php echo base_url('img/fondos/patio_noche.jpg'); ?>'); background-size: cover;">
              </div>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
              <div class="card card-plain">
                <div class="card-header">
                  <h4 class="font-weight-bolder">Atención: </h4>
                  <p class="mb-0 text-justify">
                      La CURP, el segundo apellido y la opción ingreso corresponden a un aspirante que ya ha visitado el link de validación anteriormente.<br /><br />
                      
                  </p>
                  <p class="mb-0 text-justify">
                      Ahora puede acceder al Portal de Aspirantes con los siguientes datos:<br /><br />
                      <strong>CURP: </strong><?php echo $curp; ?><br />
                      <strong>Folio: </strong><?php echo $folio; ?><br /><br />
                      
                  </p>
                </div>
                <div class="card-body">
                  
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-2 text-sm mx-auto">
                   Puede ingresa al Portal de Aspirantes
                    <a href="<?php echo base_url('Admision/Inicia'); ?>" class="text-primary text-gradient font-weight-bold">aquí</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/core/bootstrap.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?php echo base_url('templates'); ?>/material-dashboard/assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('templates'); ?>/material-dashboard//assets/js/material-dashboard.min.js?v=3.0.0"></script>



  <script type="text/javascript">
    var onloadCallback = function() {
        
    };

    function prepare() {
        document.getElementById('btnReg').disabled = false;
    }

    let cforms = document.getElementsByClassName('form-control');
    for ( let i=0; i<cforms.length; i++ ) {
      if ( cforms[i].value != "" ) {
        cforms[i].parentElement.classList.add('is-filled');
      }
    }
</script>


</body>

</html>