<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
    .periodo_select {
        display: flex;
        justify-content: left;
    }
</style>
<div class="container-fluid mt-3">
    <div class="form-group">
        <div class="row">
            <div class="col">
                <form action="<?php echo base_url('ProgramacionAcademica/getGruposPorDia'); ?>" method="post">
                    <label for="select-dia" class="form-label">Seleccione un Dia</label>
                    <div class="mb-3 periodo_select">
                        <select class="form-select col-1" name="select-dia" id="select-dia" name="periodos">
                            <option value="LUNES">LUNES</option>
                            <option value="MARTES">MARTES</option>
                            <option value="MIÉRCOLES">MIÉRCOLES</option>
                            <option value="JUEVES">JUEVES</option>
                            <option value="VIERNES">VIERNES</option>
                            <option value="SÁBADO">SÁBADO</option>
                        </select>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-secondary">Buscar</button>
                    </div>
                </form>
            </div>

        </div>
        <?php if (isset($dia)) : ?>
            <div class="table-responsive">
                <div class="card-title" id="titulo">
                    <?php echo $dia ?>
                </div>
                <table class="table table-hover  table-fluid">
                    <thead class="thead-default">
                        <tr>
                            <th style="width: 110px;">Horario</th>
                            <?php foreach ($aulas as $aula) : ?>
                                <th><?php echo $aula->aula_nombre ?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr>
                            <td>07:00 - 08:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        <form action="#" method="post">
                                            <input type="hidden" name="horario" value="07:00 - 08:00">
                                            <input type="hidden" name="salon" value="<?php echo $aula_dias->id_aula  ?>">
                                            <button type="button" class="btn btn-secondary">+</button>

                                        </form>
                                        <!-- Sin Asignar -->
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>08:00 - 09:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        <form action="#" method="post">
                                            <input type="hidden" name="horario" value="08:00 - 09:00">
                                            <input type="hidden" name="salon" value="<?php echo $aula_dias->id_aula  ?>">
                                            <button type="button" class="btn btn-secondary">+</button>

                                        </form>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>09:00 - 10:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        <form action="#" method="post">
                                            <input type="hidden" name="horario" value="09:00 - 10:00">
                                            <input type="hidden" name="salon" value="<?php echo $aula_dias->id_aula  ?>">
                                            <button type="button" class="btn btn-secondary">+</button>

                                        </form>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>10:00 - 11:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>11:00 - 12:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>12:00 - 13:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>13:00 - 14:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>14:00 - 15:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>15:00 - 16:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>17:00 - 18:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>18:00 - 19:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                        <tr>
                            <td>20:00 - 21:00</td>
                            <?php foreach ($aulas as $aula_dias) : ?>
                                <td>
                                    <?php $bandera_dia = false ?>
                                    <?php foreach ($grupos as $grupo) : ?>
                                        <?php if ($aula_dias->id_aula == $grupo->aula_id && $grupo->gpoaula_inicio == $hora_inicio && $grupo->gpoaula_dia == $dia) : ?>
                                            <?php $bandera_dia = true ?>
                                            <?php echo "$grupo->aula_id <br> $grupo->grupo_clave <br> $grupo->gpoaula_dia <br> $grupo->gpoaula_inicio - $grupo->gpoaula_fin " ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <?php if ($bandera_dia == false) : ?>
                                        Sin Asignar
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                            <?php $hora_inicio++ ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php echo $this->endSection() ?>