<?php

namespace App\Models;

use CodeIgniter\Model;

class EvaluacionesFormulariosModel extends Model
{
    protected $table = 'evaluacion_formulario';
    protected $primaryKey = 'formulario_id';
    protected $returnType = 'App\Entities\EvaluacionesFormulario';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['formulario_id', 'formulario_nombre', 'formulario_evaluacion','formulario_orden', 'formulario_instrucciones', 'created_at', 'updated_at', 'deleted_at'];
    protected $db;
    protected $builder;

    protected $useTimestamps = true;

    public function getByEvaluacion($evaluacion)
    {
        $this->where('formulario_evaluacion', $evaluacion);
        $this->orderBy('formulario_orden','ASC');
        return $this->find();
    }
}
