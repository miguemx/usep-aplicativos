<?php
namespace App\Controllers;

use App\Models\IncidenciasModel;
use App\Models\EmpleadosModel;
use App\Models\RegistrosModel;
use App\Models\UsuariosModel;
use App\Models\HorariosModel;
use App\Entities\Registro;
use App\Entities\Empleado;
use App\Entities\Usuario;

class CapitalHumano extends BaseController {

    private $empleadosModel = null;
    private $horariosModel = null;
    private $usuariosModel = null;

    public function __construct() {
        $this->empleadosModel = new EmpleadosModel();
        $this->horariosModel = new HorariosModel();
        $this->usuariosModel = new UsuariosModel();
    }

    public function index() {
        $data['nombre'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
		return view('home', $data);
    }

    /**
     * muestra un listado de todo el personal existente 
     */
    public function personal() {
        $data = $this->request->getGet();
        if ( !count($data) || !array_key_exists('id', $data) ) {
            $data = [ 'id'=>'', 'apPaterno'=>'', 'apMaterno'=>'', 'nombre'=>'' ];
        }
        $empleados = $this->empleadosModel->buscar( $data, 25, true );
        $data['empleados'] = $empleados;
        $data['pager'] = $this->empleadosModel->pager;
		$data['menu'] = $this->sesion->get('menu');
		return view('capitalhumano/listado', $data);
    }

    public function incidencias() {
        $error = null;
        $incidencias = [];
        $inicioTime = time() - (15*DAY);
        $fechaInicio = ($this->request->getPost('fechaInicio')) ? $this->request->getPost('fechaInicio'): date("Y-m-d",$inicioTime);
        $fechaFin = ($this->request->getPost('fechaFinal')) ? $this->request->getPost('fechaFinal'): date("Y-m-d");
        $empleadoId = $this->request->getPost('empleado');
        if ( !is_null($empleadoId) ) {
            $timeInicial = strtotime($fechaInicio);
            $timeFinal = strtotime($fechaFin);

            if ( $timeInicial > $timeFinal ) {
                $error = "La fecha final no puede ser menor que la fecha final";
            }
            else {
                $incidenciasModel = new IncidenciasModel();
                $personasModel = new EmpleadosModel();
                $empleado = $personasModel->find($empleadoId);
                $empleados[] = $empleado;
                $incidencias = $incidenciasModel->listar( $empleados, $fechaInicio, $fechaFin );
            }
            
            $data = [
                'empleadoid' => $empleadoId,
                'nombre' => $empleado->apPaterno.' '.$empleado->apMaterno.' '.$empleado->nombre,
                'titulo' => 'Incidencias',
                'inicio' => $fechaInicio,
                'fin' => $fechaFin,
                'error' => $error,
                'incidencias' => $incidencias,
                'menu' => $this->sesion->get('menu')
            ];

            return view('capitalhumano/incidencias', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    public function registros() {
        $data['nombre'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
		return view('capitalhumano/cargaregistros', $data);
    }

    public function cargar() {
        $data['nombre'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
		return view('capitalhumano/cargaregistros', $data);
    }

    /**
     * recibe un archivo para procesar, lo valida y manda a llamar al modelo para guardar
     * los datos
     */
    public function subirRegistros() {
        $file = $this->request->getFile('archivo');
        if ( $file->isValid() ) {
            $clientMime = $file->getClientMimeType();
            $actualMime = $file->getMimeType();
            if ( $actualMime == 'text/plain' ) {
                $dir = WRITEPATH.'uploads';
                $filename = time().'.csv';
                if ( $file->move( $dir , $filename ) ) {
                    $res = $this->guardaDatos($dir.'/'.$filename);
                    if ( $res === true ) {
                        $data['exito'] = 2;
                    }
                    else {
                        $data['error'] = $res;
                    }
                }
                else {
                    $data['error'] = 'No se puede crear el archivo para procesar';    
                }
            }
            else {
                $data['error'] = 'Formato de archivo no permitido.'. $clientMime.' -- '.$actualMime;
            }
        }
        else {
            $data['error'] = $file->getErrorString();
        }
        $data['nombre'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
		return view('capitalhumano/cargaregistros', $data);
    }

    /**
     * recibe un archivo para abrirlo y procesar su contenido
     * @param filepath la ruta donde se encuentra el archivo
     * @return true en caso de que se pueda procesar toda la informacion
     * @return error la cadena con el error encontrado
     */
    protected function guardaDatos($filepath) {
        $error = 'No se pudo abrir el archivo para su procesamiento.';
        $file = fopen( $filepath, 'r' );
        if ( $file ) {
            $error = '';
            $fila = 1;
            $empleadosModel = new EmpleadosModel();
            $registroModel = new RegistrosModel();
            while ( ($datos = fgetcsv($file, 10000, ",")) !== FALSE) {
                if ( $empleadosModel->find( $datos[6] )  ) {
                    if ( preg_match("/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/", $datos[1]) ) {
                        $registro = new Registro();
                        $registro->persona = $datos[6];
                        $registro->num = $datos[0];
                        $registro->fecha = $datos[1];
                        $registro->tipo = $datos[13];
                        $registroModel->insert( $registro );
                    }
                    else {
                        $error .= 'FILA: '.$fila.' - La fecha es incorrecta. Fecha buscada: '.$datos[1]." <br />\n";
                    }
                }
                else {
                    $error .= 'FILA: '.$fila.' - La persona no se puede encontrar. Buscando: '.$datos[6]." <br />\n";
                }
                $fila ++;
            }
            fclose( $file );
        }
        if ( strlen($error) == 0 ) return true;
        return $error;
    }

    /**
     * muestra un listado de los docentes para poder acceder a sus perfiles por puerta trasera
     */
    public function docentes() {
        $empleados = $this->empleadosModel->buscar( $this->request->getGet(), 50);
        $data['id'] = ( $this->request->getGet('id') )? $this->request->getGet('id'): '';
        $data['apPaterno'] = ( $this->request->getGet('apPaterno') )? $this->request->getGet('apPaterno'): '';
        $data['apMaterno'] = ( $this->request->getGet('apMaterno') )? $this->request->getGet('apMaterno'): '';
        $data['nombre'] = ( $this->request->getGet('nombre') )? $this->request->getGet('nombre'): '';

        $data['empleados'] = $empleados;
        $data['pager'] = $this->empleadosModel->pager;
		$data['menu'] = $this->sesion->get('menu');
		return view('capitalhumano/docentes', $data);
    }

    /**
     * muestra el formulario de edición o creación de un nuevo empleado
     */
    public function empleado() {
        $empleado = new Empleado();
        
        if ( $this->request->getPost('app') === 'empleado' && $this->request->getPost('id') ) {
            $encontrar = $this->empleadosModel->withDeleted()->find( $this->request->getPost('id') );
            if ( !is_null($encontrar) ) $empleado = $encontrar;
            $data['edit'] = '1';
        }
        $data['horarios'] = $this->horariosModel->todos();
        $data['empleado'] = $empleado;
        $data['menu'] = $this->sesion->get('menu');
        return view('capitalhumano/empleado', $data);
    }

    /**
     * realiza la acción de guardado y muestra el mismo formulario
     */
    public function guardar() {
        $empleado = new Empleado();
        if ( $this->request->getPost('app') === 'empleado' && $this->request->getPost('id') ) {
            try {
                $datos = $this->mayusDatos( 1, [ 'correo'=>2 ] ); 
                $empleado = $this->empleadosModel->withDeleted()->find( $this->request->getPost('id') );
                if ( is_null($empleado) ) { // si no existe el empleado, se crea
                    $empleado = new Empleado( $datos );
                    $usuario = new Usuario();
                    $usuario->correo = $datos['correo'];
                    $usuario->rol = '5'; // general
                    $usuario->nombre = $empleado->nombre.' '.$empleado->apPaterno.' '.$empleado->apMaterno;
                    $this->usuariosModel->insert( $usuario );
                    $this->empleadosModel->insert( $empleado );
                }
                else {
                    $empleado->apPaterno = $datos['apPaterno'];
                    $empleado->apMaterno = $datos['apMaterno'];
                    $empleado->nombre = $datos['nombre'];
                    $empleado->horario = $datos['horario'];
                    $this->empleadosModel->update( $empleado->id, $empleado );
                }
                $data['exito'] = 'Guardado correctamente.';
                $data['edit'] = '1';
            }
            catch (\Exception $ex) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                $data['error'] = 'Ocurrió un error al guardar el registro. Por favor contacte al administrador con el siguiente folio: '.date("YmdHis");
            }
        }
        else {
            $data['error'] = 'No se cuenta con el permiso para realizar esta acción.';
        }
        $data['empleado'] = $empleado;
        $data['horarios'] = $this->horariosModel->todos();
        $data['menu'] = $this->sesion->get('menu');
        return view('capitalhumano/empleado', $data);
    }

    /**
     * realiza la acción de eliminar o des eliminar un empleado
     * @param action bandera para indicar si se elimina un empleado: 1 para deseliminar, 0 para eliminar
     */
    public function activarEmpleado($action) {
        $empleado = $this->empleadosModel->withDeleted()->find( $this->request->getPost('id') );
        if ( !is_null($empleado) ) {
            try {
                if ( $action === "1" ) {
                    $this->empleadosModel->update( $empleado->id, ['deleted_at'=>null] );
                }
                if ( $action === "0" ) {
                    $this->empleadosModel->delete( $empleado->id );
                }
            }
            catch ( \Exception $ex ) {
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            }
        }
        // return redirect()->to( '/CapitalHumano/Personal' );
        return redirect()->back();
    }

    /**
     * pone los apellidos en los campos respectivos
     */
    function ponerApellidos() {
        $empleados = $this->empleadosModel->withDeleted()->findAll();
        foreach ( $empleados as $empleado ) {
            $apellidos = explode( ' ', $empleado->apellido, 2 );
            var_dump( $apellidos );
            try {
                $empleado->apPaterno = $apellidos[0];
                $empleado->apMaterno = $apellidos[1];
                $this->empleadosModel->save( $empleado );
            }
            catch ( \Exception $ex ) {
                echo "ERROR: ".$ex->getMessage();
            }
            echo '<hr />';
        }
    }

}