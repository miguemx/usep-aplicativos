insert into aplicativo_roles (rol_id,rol_nombre,rol_menu) values (22,"SUBDIRECCION ENFERMERIA", "subacademica");
insert into aplicativo_roles (rol_id,rol_nombre,rol_menu) values (23,"SUBDIRECCION MEDICINA", "subacademica");
insert into aplicativo_permisos (permiso_rol,permiso_modulo,permiso_funcion) select 22,permiso_modulo,permiso_funcion from aplicativo_permisos where permiso_rol = 20;
insert into aplicativo_permisos (permiso_rol,permiso_modulo,permiso_funcion) select 23,permiso_modulo,permiso_funcion from aplicativo_permisos where permiso_rol = 20;
update aplicativo_usuarios set usuario_rol = 22 where usuario_correo in ("luis.mendoza@usalud.edu.mx","elena.celis@usalud.edu.mx","lilia.ortega@usalud.edu.mx");
update aplicativo_usuarios set usuario_rol = 23 where usuario_correo in ("flavia.aguilar@usalud.edu.mx","goretti.cordero@usalud.edu.mx");