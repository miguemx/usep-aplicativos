<?php
namespace App\Entities;

use CodeIgniter\Entity;

class CampoTodos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'etapaId' => null,
        'periodo' => null,
        'etapa' => null,
        'etapaOrden' => null,
        'tieneFormulario' => null,
        'responsableAspirante' => null,
        'formularioId' => null,
        'formulario' => null,
        'titulo' => null,
        'formularioOrden' => null,
        'instrucciones' => null,
        'id' => null,
        'campoDependiente' => null,
        'pregunta' => null,
        'descripcion' => null,
        'campoOrden' => null,
        'obligatorio' => null,
        'visualizar' => null,
        'tipoCampoId' => null,
        'tipoCampo' => null,
        'valoresCampo' => null,
        'clasificacion' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'etapaId' => 'etapa_id',
        'periodo' => 'etapa_periodo',
        'etapa' => 'etapa_nombre',
        'etapaOrden' => 'etapa_orden',
        'tieneFormulario' => 'etapa_formulario',
        'responsableAspirante' => 'etapa_responsable',
        'formularioId' => 'formulario_id',
        'formulario' => 'formulario_nombre',
        'titulo' => 'formulario_titulo',
        'formularioOrden' => 'formulario_orden',
        'instrucciones' => 'formulario_instrucciones',
        'id' => 'campo_id',
        'campoDependiente' => 'campo_dependiente',
        'pregunta' => 'campo_nombre',
        'descripcion' => 'campo_descripcion',
        'campoOrden' => 'campo_orden',
        'obligatorio' => 'campo_obligatorio',
        'visualizar' => 'campo_visualizar',
        'tipoCampoId' => 'tipocampo_id',
        'tipoCampo' => 'tipocampo_nombre',
        'valoresCampo' => 'tipocampo_valores',
        'clasificacion' => 'tipocampo_clasificacion',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
