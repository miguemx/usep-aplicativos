<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var string[]
	 */
	public $ruleSets = [
		Rules::class,
		FormatRules::class,
		FileRules::class,
		CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array<string, string>
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
	// -------------------- registro ----------------------------------------------------------------------
	public $aspirante = [
		'curp'         		=> 'required|regex_match[/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/]',
        'apPaterno'        	=> 'required',
        'apMaterno'        	=> 'required',
		'nombre'        	=> 'required',
		'correo'            => 'required|valid_email',
		'carrera'        	=> 'required|numeric',
		'terminos'        	=> 'required|in_list[1]',
		'aviso'  	      	=> 'required',
	];
	public $aspirante_errors = [
		'apPaterno' => [ 
			'required' => 'El Apellido Paterno es requerido.', 
		],
		'apMaterno' => [ 
			'required' => 'El Apellido Materno es requerido.', 
		],
		'nombre' => [ 
			'required' => 'El Nombre es requerido.', 
		],
		'curp' => [ 
			'required' => 'La CURP es requerida.', 
			'regex_match' => 'El formato de tu CURP no es correcto.'
		],
        'correo' => [
			'required' => 'El Correo Electrónico es requerido.',
            'valid_email' => 'El correo electrónico no parece ser correcto.'
		],
		'carrera' => [
			'required' => 'Selecciona una opción de ingreso.',
            'numeric' => 'Selecciona una opción de ingreso.',
        ],
		'terminos' => [
			'required' => 'Debes aceptar todos los términos de la convocatoria de admisión.',
			'in_list' => 'Debes aceptar todos los términos de la convocatoria de admisión.'
		],
		'aviso' => [
			'required' => 'Debes aceptar el aviso de privacidad.',
			
		]
	];

	public $alumno = [
		'id'			=> 'required|regex_match[/^[0-9]{9}$/]',
		'curp'         		=> 'required|regex_match[/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/]',
        'apPaterno'        	=> 'required',
        'apMaterno'        	=> 'required',
		'nombre'        	=> 'required',
		'correoPersonal'    => 'required|valid_email',
		'carrera'        	=> 'required|numeric',
		'sexo'				=> 'required|in_list[H,M]'
	];
	public $alumno_errors = [
		'id' => [ 
			'required' => 'Debe especificar la matrícula del estudiante.', 
			'regex_match' => 'El formato de la matrícula no es correcto.'
		],
		'apPaterno' => [ 
			'required' => 'El Apellido Paterno es requerido.', 
		],
		'apMaterno' => [ 
			'required' => 'El Apellido Materno es requerido.', 
		],
		'nombre' => [ 
			'required' => 'El Nombre es requerido.', 
		],
		'curp' => [ 
			'required' => 'La CURP es requerida.', 
			'regex_match' => 'El formato de tu CURP no es correcto.'
		],
        'correoPersonal' => [
			'required' => 'El Correo Electrónico es requerido.',
            'valid_email' => 'El correo electrónico no parece ser correcto.'
		],
		'carrera' => [
			'required' => 'Selecciona una opción de ingreso.',
            'numeric' => 'La carrera no se encuentra registrada en el sistema.',
        ],
		'sexo' => [
			'required' => 'Por favor especifique el sexo.',
            'in_list' => 'El valor para el campo sexo debe ser H o M.',
        ],
	];



	// -------------------- altas CCT ----------------------------------------------------------------------
	public $cct = [
		'estado'        	=> 'required',
		'municipio'        	=> 'required',
		'cct'         		=> 'required',
        'nombre'        	=> 'required',
	];
	public $cct_errors = [
		'cct' => [ 
			'required' => 'Por favor proporciona la clave del centro de trabajo.', 
		],
		'nombre' => [ 
			'required' => 'El nombre del centro educativo es requerido.', 
		],
		'estado' => [ 
			'required' => 'Por favor selecciona el estado donde se ubica tu escuela.', 
		],
		'municipio' => [ 
			'required' => 'Por favor selecciona el municipio donde se ubica tu escuela.', 
		]
	];

}
