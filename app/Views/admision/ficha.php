<?php

$font_size = ' font-size: 6.5px ;';
$font_size_firma = 'font-size: 5px;';

?>
<html lang='en'>

<head>
    <!--<script src="JsBarcode.all.min.js"></script> -->
</head>
<style>
    a {color:#910433;}
    body {
        height: 1650px;
        width: 1275px;
        font-family: Arial, Helvetica, sans-serif;        
    }

    .titulo {
        text-align: center;
        font-size: 20px;
        font-weight: bold;

    }
    .datos {
        font-size: 15px;
        font-family: Arial, Helvetica, sans-serif;
        line-height:1.8;
    }
    .inst {
        font-size: 12px;
        font-family: Arial, Helvetica, sans-serif;
        line-height:1.8;
        
    }
    .pie {
        font-size: 10px;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        text-align: center;
        
    }
    .foto{
        border-width:4px;
        margin: 30px;
    }

</style>

<body>
<div style='background: url(<?php echo base_url('img/ficha_admision/fondoficha.jpg') ?>); background-repeat: no-repeat; background-position: left bottom; background-size: 100%; height: 100%; width: 100%;'>
    

    <div>
        <p class='titulo' style='padding-top: 135px'>&quot;FICHA DE EXAMEN&quot;</p>
        <table class="table"> 
            <thead>
                <tr >
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row" width ="70px"></td>
                    <td width="150px"  ><div style="padding:50px"><img src="<?php echo $foto ?>" class="img-fluid rounded-top" alt="" width="130" height="130"></div></td>
                    <td><br/><br/><p style='padding-top: 10px;' class ="datos">
                        <b>Folio:</b> <?php echo $folio ?><br/>
                        <b>Nombre:</b> <?php echo $ap_pat.' '.$ap_mat.' '.$nombre ?><br/>
                        <b>CURP:</b> <?php echo $curp ?><br/>
                        <b>Opción de ingreso:</b> <?php if ($carrera==1) {echo "Licenciatura en Enfermería y Obstetricia";} else {echo "Licenciatura en Médico Cirujano";} ?><br/>
                        <b>Modalidad de examen:</b> <?php echo $modalidad ?><br/>
                        <b>Día:</b> <?php echo $dia ?><br/>
                        <b>Hora:</b> <?php echo $hora ?><br/>
                        <b>Usuario:</b> <?php echo $usuario ?><br/>
                        <b>Contraseña:</b> <?php echo $pass ?><br/>
                        <b>Liga de acceso: </b><a href=" <?php echo $liga ?>" target="_blank"><?php echo $liga ?></a></p>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style='padding-left: 50px; padding-right: 50px;'>
        <p class ='inst'><b>Instrucciones generales para presentar el examen de admisión 2022:</b></p>
        <p class ='inst'>
        <b>1.	Veriﬁque el día y hora asignado para presentar su examen de admisión.</b><br />
        Importante: no habrá cambio de fecha u hora.<br />
        <b>2.	Asegúrese que su equipo de cómputo</b> (cámara, micrófono, teclado y ratón) así como su conexión a internet <b>se encuentren en óptimas condiciones.</b> Estos deberán contar al menos con las siguientes características:<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Equipo con sistema operativo Windows 7 o superior,<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Navegador Google Chrome instalado en su versión más actual (99 o superior),<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	La velocidad de conexión de su internet deberá, al menos, contar con 5 Mbps de descarga y 2 Mbps de subida.<br />
        <b>3.	El día del examen tenga a la mano una identificación oficial con fotografía y la presente “Ficha de examen”.</b><br />
        <b>4.	Conéctese puntualmente en el día asignado para la aplicación de su examen.</b><br />
        5.	Recuerde que, en su espacio de trabajo, <b>únicamente podrá tener a la mano hojas blancas, lápiz, goma y sacapuntas</b> en caso de ser necesario.
        <b>Está prohibido el uso de cualquier auxiliar como lo son calculadoras, celulares, audífonos, reloj inteligente y tabletas.</b><br />
        6.	Una vez verificados los puntos anteriores y <b>solo en caso de solicitar “apoyo técnico” antes de la aplicación del examen,</b>
        podrá ponerse en contacto mediante WhatsApp al: <?php echo env('admision.ficha.whatsapp')?>. No se atenderá otro tipo de dudas que no sean situaciones <b>técnicas</b> para la aplicación de este.<br />
        7.	Ponemos a su disposición el siguiente material de apoyo:<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Reglamento para aplicación del examen: <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo env('admision.ficha.urlreglamentoexamen')?>" target="_blank"><?php echo env('admision.ficha.urlreglamentoexamen')?></a></p>
        <p>&nbsp;</p>
        <p class='pie'><i>La “Ficha de examen” de admisión en línea 2022 es personal e intransferible, el uso indebido de la misma será causa de la cancelación del examen, así como del Proceso de Admisión 2022.</i><br />
        </p>
        <p>&nbsp; </p>
    </div>
</div>
</body>
<script>
    //JsBarcode(".barcode").init();
</script>

</html>