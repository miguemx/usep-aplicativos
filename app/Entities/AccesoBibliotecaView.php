<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AccesoBibliotecaView extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                => null,
        'alumno'            => null,
        'empleado'          => null,
        'tipo'              => null,
        'sexo'              => null,
        'edad'              => null,
        'entrada'           => null,
        'salida'            => null,
        'created_at'        => null,
        'updated_at'        => null,
        'deleted_at'        => null,
        'matricula'         => null,
        'alumnoCorreo'      => null,
        'alumnoNombre'      => null,
        'alumnoApPaterno'   => null,
        'alumnoApMaterno'   => null,
        'empleadoId'        => null,
        'empleadoCorreo'    => null,
        'empleadoNombre'    => null,
        'empleadoApPaterno' => null,
        'empleadoApMaterno' => null,
        'empleadoEsDocente' => null,
        'carrera'           => null,
        'foto'              => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'biblioacceso_id',
        'alumno'            => 'biblioacceso_alumno',
        'empleado'          => 'biblioacceso_empleado',
        'tipo'              => 'biblioacceso_tipo',
        'sexo'              => 'biblioacceso_sexo',
        'edad'              => 'biblioacceso_edad',
        'entrada'           => 'biblioacceso_entrada',
        'salida'            => 'biblioacceso_salida',
        'created_at'        => 'created_at',
        'updated_at'        => 'updated_at',
        'deleted_at'        => 'deleted_at',
        'matricula'         => 'alumno_id',
        'alumnoCorreo'      => 'alumno_correo',
        'alumnoNombre'      => 'alumno_nombres',
        'alumnoApPaterno'   => 'alumno_ap_paterno',
        'alumnoApMaterno'   => 'alumno_ap_materno',
        'empleadoId'        => 'empleado_id',
        'empleadoCorreo'    => 'empleado_correo',
        'empleadoNombre'    => 'empleado_nombre',
        'empleadoApPaterno' => 'empleado_ap_paterno',
        'empleadoApMaterno' => 'empleado_ap_materno',
        'empleadoEsDocente' => 'empleado_docente',
        'carrera'           => 'carrera_nombre',
        'foto'              => 'alumno_foto',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
