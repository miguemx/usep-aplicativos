<?php

namespace App\Entities;

use CodeIgniter\Entity;

class Ripel extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'matricula' => null,
        'nombres' => null,
        'paterno' => null,
        'materno' => null,
        'correo'  => null,
        'fecha'  => null,
        'ip' => null,
        'conexion' => null,
        'carrera' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'matricula' => 'ripel_matricula',
        'nombres' => 'alumno_nombres',
        'paterno' => 'alumno_ap_patern',
        'materno' => 'alumno_ap_matern',
        'correo' => 'usuario_correo',
        'fecha' => 'ripel_fecha',
        'ip' => 'ripel_ip',
        'conexion' => 'usuario_ultcnx',
        'carrera' => 'alumno_carrera',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
