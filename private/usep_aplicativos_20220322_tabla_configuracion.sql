-- creación de tabla de configuraciones
CREATE TABLE `usep_aplicativos`.`aspirantes_configuracion`(
    `configuracion_id` INT NOT NULL AUTO_INCREMENT,
    `configuracion_nombre` VARCHAR(40) NOT NULL,
    `configuracion_status` INT(1) NOT NULL DEFAULT '1',
    `created_at` DATE NULL,
    `updated_at` DATE NULL,
    `deleted_at` DATE NULL,
    PRIMARY KEY(`configuracion_id`)
) ENGINE = INNODB;
-- inserción de configuraciones al momento
INSERT INTO `aspirantes_configuracion` (`configuracion_id`, `configuracion_nombre`, `configuracion_status`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL, 'Proceso de Admisión Abierto', '1', '2022-03-23', NULL, NULL),
       (NULL, 'Revalidación de Materias', '1', '2022-03-23', NULL, NULL);

-- adición de columna que guarda si el alumno revalida materias o no
ALTER TABLE `aspirantes_generales` ADD `aspirante_revalida` VARCHAR(2) NULL AFTER `aspirante_nac_pais`;