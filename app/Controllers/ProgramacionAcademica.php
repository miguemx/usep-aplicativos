<?php

namespace App\Controllers;

use App\Entities\Aulas;
use App\Entities\AulasGrupos;
use App\Entities\Grupo;
use App\Entities\GruposHorarios;
use App\Entities\Rol;
use App\Models\AulasGrupoModel;
use App\Models\AulasGruposDetallesModel;
use App\Models\AulasModel;
use App\Models\CarrerasModel;
use App\Models\EmpleadosModel;
use App\Models\GrupoHistorialModel;
use App\Models\GrupoModel;
use App\Models\GruposHorariosModel;
use App\Models\GruposMateriasModel;
use App\Models\HorarioAlumnosModel;
use App\Models\MateriaModel;
use App\Models\PeriodoModel;

class ProgramacionAcademica extends BaseController
{

    private $materiasModel;
    private $gruposModel;
    private $periodoModel;
    private $aulasModel;
    private $aulasgruposModel;
    private $gruposmateriasModel;
    private $gruposaulasModel;
    private $docenteModel;
    private $periodo_curso;
    private $periodo_inicio;
    private $periodoProgramacionAcademica;
    private $carrerasModel;
    private $horariosModel;
    private $roles_medicina = ['DIRECTOR MEDICINA', 'SUBDIRECCION MEDICINA', 'JEFATURA MEDICINA'];
    private $roles_enfermeria = ['DIRECTOR ENFERMERIA', 'SUBDIRECCION ENFERMERIA', 'JEFATURA ENFERMERIA'];
    private $roles_administradores = ['ADMINISTRADOR', 'SECRETARIA ACADEMICA'];


    public function __construct()
    {
        $this->materiasModel = new MateriaModel();
        $this->gruposModel = new GrupoModel();
        $this->periodoModel = new PeriodoModel();
        $this->aulasModel = new AulasModel();
        $this->aulasgruposModel = new GruposHorariosModel();
        $this->gruposmateriasModel = new GruposMateriasModel();
        $this->gruposaulasModel = new AulasGrupoModel();
        $this->docenteModel = new EmpleadosModel();
        $this->periodo_curso = $this->periodoModel->getPeriodoEstatus('CURSO');
        $this->periodo_inicio = $this->periodoModel->getPeriodoEstatus('INICIO');
        $this->periodoProgramacionAcademica = $this->periodoModel->getPeriodoProgramacionAcademica();
        $this->carrerasModel = new CarrerasModel();
        $this->horariosModel = new HorarioAlumnosModel();
    }
    public function index()
    {
    }
    /**
     * menu para la generacion de la programacion academica
     * @param periodo ID del periodo
     */
    public function GeneradorPA($periodo = false)
    {
        if (!$periodo) $periodo = $this->request->getPost('select_periodo');
        $data['periodos'] = $this->periodoModel->getPeriodoEstatus('inicio');
        $data['periodo'] = $periodo;
        $data['menu'] = $this->sesion->get('menu');
        $data['rol'] = ($this->sesion->get('rol_id'));
        $data['docentes'] = $this->docenteModel->getEmpleadosDocentes();
        return view('academica/generadorprogramacion_v2', $data);
    }

    public function getaulasgrupos()
    {
        $aulasmodel = new AulasModel();
        $gruposaulasmodel = new AulasGrupoModel();
        $data['dia'] = 'LUNES';
        $data['hora_inicio'] = 7;
        $data['grupos'] = $gruposaulasmodel->gruposaulas('2022A', 'LUNES');
        $data['aulas'] = $aulasmodel->getAulasId();
        return view('academica/inicio', $data);
    }
    public function getGruposPorDia()
    {
        // $this->request->getPost();
        $dia = $this->request->getPost('select-dia');
        $aulasmodel = new AulasModel();
        $gruposaulasmodel = new AulasGrupoModel();
        $data['dia'] = strtoupper($dia);
        $data['hora_inicio'] = 7;
        $data['grupos'] = $gruposaulasmodel->gruposaulas($this->periodoProgramacionAcademica, strtoupper($dia));
        $data['aulas'] = $aulasmodel->getAulasId();
        return view('academica/inicio', $data);
    }

    //----------------------------seccion de prueba


    /**
     * @return datos que contiene el arreglo de materias disponibles 
     */
    public function getMaterias($carrera, $semestre)
    {
        $materias = $this->materiasModel->getMaterias($semestre, $carrera);
        if ($materias) {
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($materias);
        } else {
            $this->response->setHeader('Content-Type', 'application/json');
            return null;
        }
    }
    //---------------------------- fin de seccion de prueba

    /**
     * Crea la cantidad de grupos seleccionados con las materias
     */
    public function CreateGrupos()
    {
        $datos = $this->request->getPost();
        $año = date('y');
        $mensaje = [
            'tipo' => 'alert-danger',
            'texto' => 'No se pudo generar los grupos solicitados.'
        ];
        $arr_grupos_coorectos = [];
        $arr_grupos_errores = [];
        $periodo_id = $this->request->getPost('periodo');
        // var_dump($periodo_id);
        // die();
        if ($periodo_id) {
            $periodomodel = $this->periodoModel->find($periodo_id);
            $mes = explode('-', $periodomodel->inicio);
            $periodo = ($mes[1] >= 8) ? '01' : '02';
            $carrera = ($datos['carrera'] == 1) ? '01' : '02';
            $id_grupo = $año . $periodo . $carrera;
            $grupos_periodo = $this->gruposModel->getGruposPorCarrea($id_grupo, $periodo_id);
            if ($grupos_periodo) {
                $last_id = end($grupos_periodo);
                $siguiente = $last_id->id + 1;
            } else {
                $siguiente =  $id_grupo . "0001";
            }
            // $arr_secciones = array_unique($datos['seccion']);
            $x = 0;
            $grupoEntiti = new Grupo();
            foreach ($datos['materia'] as $grupos) {
                $arreglo = explode(' ', $grupos);
                $grupoEntiti->id = $siguiente;
                $grupoEntiti->idc = $siguiente;
                $grupoEntiti->clave = $datos['seccion'][$x];
                $grupoEntiti->materia = $arreglo[0];
                $grupoEntiti->docente = $datos['select_docente'][$x];
                $grupoEntiti->periodo = $periodo_id;
                try {
                    $this->gruposModel->insert($grupoEntiti);
                    $arr_grupos_coorectos[] = $grupoEntiti;
                } catch (\Exception $ex) {
                    $arr_grupos_errores[] = $siguiente;
                    log_message('info', 'No se pudo crear el grupo ' . $siguiente);
                }
                $siguiente++;
                $x++;
            }
            if ($arr_grupos_coorectos) {
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => 'Se crearon ' . count($arr_grupos_coorectos) . ' grupos de forma correcta'
                ];
            }
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/ProgramacionAcademica/HorarioSecciones/');
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => 'No hay un periodo disponible para generar la Programación Academica'
            ];
            log_message('info', 'No hay periodo en etapa de INICIO para la generacion de la programación academica');
            $this->sesion->setFlashdata('datos', $mensaje);

            return redirect()->to('/ProgramacionAcademica/Menu/');
        }
    }

    /**
     * @return grupos si encuentra grupos creados para el periodo seleccionado que pertenezcan a la carrera y el semestre solicitado
     * @return nuill si no encuentra coincidencias
     * @param periodo corresponde al periodo activo
     * @param carrera
     */
    public function getGruposExistentes($carrera, $semestre, $periodo)
    {

        // $periodo =  $this->periodoProgramacionAcademica;
        if ($periodo) {
            $grupos_periodo = $this->gruposmateriasModel->getBySemestrePeriodoCarrera($semestre, $periodo, $carrera);
            $secciones = $this->gruposmateriasModel->getSeccionesPeriodo($semestre, $periodo, $carrera);
            $datos = ['secciones' =>  $secciones, 'grupos' =>  $grupos_periodo];
            if ($grupos_periodo) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($datos);
            } else return null;
        } else {
            return 'Sin periodo disponible';
        }
    }

    /**
     * elimina grupos existentes siempre que no tengan horas asignadas
     * @param grupo ID del grupo seleccionado
     */
    public function eliminarGrupo($grupo)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $datos_grupo = $this->gruposModel->find($grupo);
        if ($datos_grupo) {
            $periodo = $this->periodoModel->find($datos_grupo->periodo);
            if ($periodo && $periodo->estado == 'INICIO') {
                // return json_encode($periodo->estado);
                $horarios = $this->aulasgruposModel->getByIDC($grupo);
                if (!$horarios) {

                    try {
                        $this->gruposModel->delete($grupo);
                        log_message('info', 'Se ha eliminado el grupo ' . $grupo . ' por el usuario ' . $this->sesion->get('useremail'));
                        return json_encode('realizado');
                    } catch (\Exception $ex) {
                        log_message('info', 'No se pudo eliminar el grupo ' . $grupo);
                        return json_encode('error');
                    }
                } else {
                    return json_encode('horario');
                }
                // return null;
            } else return json_encode('periodo');
        }
    }
    //-----------------------------------------------------------------------------------------------------------
    //                                  seccion de asignacion de grupos a horarios
    //-----------------------------------------------------------------------------------------------------------
    /**
     * retorna a menu pero con mensaje de cancelacion
     */
    public function Home($aula = null)
    {
        $mensaje = [
            'tipo' => 'alert-info',
            'texto' => "No se ha generado ningun cambio"
        ];
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/ProgramacionAcademica/MapaAulas/' . $aula);
    }

    /**
     * despliega el menu principal para la asignacion de horarios en las aulas
     */
    public function Menu()
    {
        $data['aula'] = '';
        $data['aula_clave'] = false;
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['aulas'] = $this->aulasModel->getAulasId(true);
        $data['horario'] = false;
        $data['periodos'] =  $this->periodoModel->findAll();
        $data['periodo'] =  '';
        $data['carreras'] = $this->carrerasModel->findAll();
        $rol_sesion = $this->sesion->get('rol_id');
        $rol =  (($rol_sesion == 9) ? '2' : (($rol_sesion == 10) ? '1' : 'administrador'));
        $data['rol'] =  $rol;
        $data['menu'] = $this->sesion->get('menu');
        $data['administrador'] = (($this->sesion->get('rol_id') == 1) ? true : false);
        return view('academica/programacion_v2', $data);
    }
    /**
     * busca todas las clases que se dan en un aula en especifico
     * @param aula id correspondiente al aula solicitada
     * @return mapa_aulas arreglo de grupos asiganos al aula solicitada
     * 
     */
    public function MapaAulas($aula = null, $periodo = false, $rol = false)
    {

        $data['carreras'] = $this->carrerasModel->findAll();
        $data['periodos'] =  $this->periodoModel->findAll();
        if (!$aula) {
            $aula = $this->request->getPost('aulas_select');
        }

        if (!$periodo) {
            $periodo = $this->request->getPost('select_periodo');
            // var_dump($periodo);
        }
        // var_dump($periodo);
        if (!$periodo) {

            $periodo = $this->periodoProgramacionAcademica;
            // $periodo = $periodo[0]->id;
        }

        $data['aulaId'] = $aula;
        $data['bandera_edicion'] = $this->periodoModel->PermisoEdicionPeriodo($periodo);
        if($aula == 101){
            $data['bandera_edicion'] = false;
        }
        // var_dump( $data['bandera_edicion']);
        $aula_detalles = $this->aulasModel->find($aula);
        // var_dump( $aula_detalles);

        // echo $aula;
        // echo $periodo[0]->id;
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['aula_nombre'] = strtoupper($aula_detalles->nombre_aula);
        $data['aula_clave'] = $aula_detalles->id_aula;

        $data['aulas'] = $this->aulasModel->getAulasId(true);
        $data['grupos'] = $this->aulasgruposModel->getGruposAula($periodo, strtoupper($aula));
        // echo json_encode($data['grupos']);
        $data['grupos_periodo'] = $this->gruposModel->getByPeriodo($periodo);
        $data['materias'] = $this->materiasModel->findAll();
        $data['periodo'] =  $periodo;

        // var_dump( $data['grupos'] );
        $data['dias'] = [
            '1' => 'LUNES',
            '2' => 'MARTES',
            '3' => 'MIÉRCOLES',
            '4' => 'JUEVES',
            '5' => 'VIERNES',
            '6' => 'SÁBADO',
        ];
        $data['horario'] = true;
        $data['bandera'] = 'MapaAulas';
        // var_dump($data['bandera']);
        $rol_sesion = $this->sesion->get('rol_id');
        $rol =  (($rol_sesion == 9) ? '2' : (($rol_sesion == 10) ? '1' : (($rol_sesion == 1)?'administrador':false)));
        $data['rol'] =  $rol;
        $data['administrador'] = (($rol_sesion == 1) ? true : false);
        // var_dump($data['rol']);
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/programacion_v2', $data);
    }

    /**
     * busca todas las clases que se dan en un aula en especifico y renderiza una tabla sin estilos
     * @param aula id correspondiente al aula solicitada
     * @return mapa_aulas arreglo de grupos asiganos al aula solicitada
     * 
     */
    public function MapaAulasCsv($aula = null, $periodo = false, $rol = false)
    {

        $data['carreras'] = $this->carrerasModel->findAll();
        $data['periodos'] =  $this->periodoModel->findAll();
        if (!$aula) {
            $aula = $this->request->getPost('aulas_select');
        }

        if (!$periodo) {
            $periodo = $this->request->getPost('select_periodo');
            // var_dump($periodo);
        }
        // var_dump($periodo);
        if (!$periodo) {

            $periodo = $this->periodoProgramacionAcademica;
            // $periodo = $periodo[0]->id;
        }
        $data['bandera_edicion'] = $this->periodoModel->PermisoEdicionPeriodo($periodo);
        // var_dump( $data['bandera_edicion']);
        $aula_detalles = $this->aulasModel->find($aula);
        // var_dump( $aula_detalles);

        // echo $aula;
        // echo $periodo[0]->id;
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['aula_nombre'] = strtoupper($aula_detalles->nombre_aula);
        $data['aula_clave'] = $aula_detalles->id_aula;

        $data['aulas'] = $this->aulasModel->getAulasId(true);
        $data['grupos'] = $this->aulasgruposModel->getGruposAula($periodo, strtoupper($aula));
        // echo json_encode($data['grupos']);
        $data['grupos_periodo'] = $this->gruposModel->getByPeriodo($periodo);
        $data['materias'] = $this->materiasModel->findAll();
        $data['periodo'] =  $periodo;

        // var_dump( $data['grupos'] );
        $data['dias'] = [
            '1' => 'LUNES',
            '2' => 'MARTES',
            '3' => 'MIÉRCOLES',
            '4' => 'JUEVES',
            '5' => 'VIERNES',
            '6' => 'SÁBADO',
        ];
        $data['horario'] = true;
        $data['bandera'] = 'MapaAulas';
        // var_dump($data['bandera']);
        $rol_sesion = $this->sesion->get('rol_id');
        $rol =  (($rol_sesion == 9) ? '2' : (($rol_sesion == 10) ? '1' : 'administrador'));
        $data['rol'] =  $rol;
        $data['administrador'] = (($rol_sesion == 1) ? true : false);
        // var_dump($data['rol']);
        $data['menu'] = $this->sesion->get('menu');

        $filename = $periodo . '-' . $data['aula_nombre'] . '.xls';

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$filename");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        return view('academica/programacioncsv', $data);
    }


    /**
     * Agrega una materia en el horario seleccionado
     * 
     */
    public function SeleccionarGrupo($periodo = false)
    {
        $datos = explode('|', $this->request->getPost('posicion'));
        // var_dump($datos);

        if (!$periodo) $periodo = $this->request->getPost('select_periodo');
        if (!$periodo) {
            $periodo = $this->periodoProgramacionAcademica;
        }
        $filtros = [
            'mat_nombre' => (($this->request->getPost('mat_nombre')) ? $this->request->getPost('mat_nombre') : ''),
            'mat_calve' => (($this->request->getPost('mat_calve')) ? $this->request->getPost('mat_calve') : ''),
            'seccion' => (($this->request->getPost('seccion')) ? $this->request->getPost('seccion') : ''),
            'nombre' => (($this->request->getPost('nombre')) ? $this->request->getPost('nombre') : ''),
            'apPaterno' => (($this->request->getPost('apPaterno')) ? $this->request->getPost('apPaterno') : ''),
            'apMaterno' => (($this->request->getPost('apMaterno')) ? $this->request->getPost('apMaterno') : ''),
            'periodo' => ($periodo),
            'carrera' => (($datos[5]))
        ];
        $data = $filtros;
        $data['inicio'] =  $datos[0];
        $data['fin'] =  $datos[1];
        $data['aula'] =  $datos[2];
        // echo $data['periodo'];
        // var_dump($data['aula']);
        $data['dia'] =  $datos[3];
        $data['periodo'] =  $datos[4];
        $data['carrera'] =  $datos[5];
        $data['bandera'] = $this->request->getPost('bandera');
        // var_dump($data['bandera']);
        $grupos = $this->gruposmateriasModel->buscar($filtros);
        // var_dump($grupos);
        $grupos_contador = $this->aulasgruposModel->getByPeriodo($periodo); //----------------------
        $arr_id = [];
        foreach ($grupos_contador as $grupo) {
            $arr_id[] =  $grupo->grupo;
        }
        $data['pager'] = $this->gruposmateriasModel->pager;
        $data['grupos_contador'] = array_count_values($arr_id);
        $data['grupos_periodo'] = $grupos;
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/listagrupos', $data);
    }



    /**
     * elimina la asignacion de horario de un grupo
     * @param grupo identificacdor del grupo seleccionado
     * @param dia dial del cual se eliminara 
     * @param hora hora de inicio de la clase 
     */
    public function eliminarGrupoHora()
    {
        $grupo_detalles = explode('|', $this->request->getPost('grupo_detalles'));
        $grupo = $grupo_detalles[0];
        $dia = $grupo_detalles[1];
        $hora =  explode('-', $grupo_detalles[2]);
        $materia = $grupo_detalles[3];
        $gruposaula =  $this->gruposaulasModel->getGrupoHoraDia($grupo, $dia, $hora[0]);
        if ($gruposaula) {
            try {
                $this->gruposaulasModel->delete($gruposaula[0]->id);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Se ha eliminado el grupo '$materia' del dia $dia a la hora $grupo_detalles[2]"
                ];
                log_message('info', 'Se ha eliminado el grupo ' . $grupo . ' del dia ' . $dia . ' de la hora ' . $grupo_detalles[2]);
                $this->sesion->setFlashdata('datos', $mensaje);
            } catch (\Exception $ex) {
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => "No se ha podido eliminado el grupo $materia del dia $dia a la hora $grupo_detalles[2]"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
                log_message('info', 'No se pudo crear el grupo ');
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => "No se ha encontrado el grupo $materia del dia $dia a la hora $grupo_detalles[2]"
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
        }
        if ($this->request->getPost('bandera')) {
            return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
        }
        return redirect()->to('/ProgramacionAcademica/Menu/');
    }

    /**
     * retorna a vista para la edicion del grupo
     */
    public function editarGrupoHora()
    {
        $grupo_detalles = explode('|', $this->request->getPost('grupo_detalles'));
        $grupo = $grupo_detalles[0];
        $dia = $grupo_detalles[1];
        $hora =  explode('-', $grupo_detalles[2]);
        $materia = $grupo_detalles[3];
        $aula = $grupo_detalles[4];
        $data['bandera'] = $this->request->getPost('bandera');
        // var_dump($data['bandera']); //------------------------------------------------------->
        if ($data['bandera']) {
            $data['valores']  =  $this->request->getPost('posicion');
        }
        $data['aula'] = $aula;
        // var_dump($data['aula']); //------------------------------------------------------->
        $datos_grupo = $this->gruposModel->find($grupo);
        $data['grupo'] =  $datos_grupo;
        $data['docentes'] = $this->docenteModel->getEmpleadosDocentes();
        $data['materia'] = $this->materiasModel->find($datos_grupo->materia);
        $docente = $this->docenteModel->find($datos_grupo->docente);
        $data['docente_actual'] = (($docente) ? $docente : false);
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/editarGrupo', $data);
    }

    /**
     * obtiene datos para editar el registro del grupo
     * @param grupo id del grupo que se editara
     * 
     */
    public function EditarGrupo($grupo)
    {
        /*  var_dump($this->request->getPost('select_periodo'));
        $bandera = ($this->request->getPost('bandera'));
        var_dump($bandera);
        die(); */
        $mensaje = false;
        $grupo_detalles = $this->gruposModel->find($grupo);
        $grupo_comparativo = $this->gruposModel->find($grupo);
        if ($grupo_detalles) {
            $datos = $this->request->getPost();

            if ($grupo_detalles->docente !=  $datos['docente_select'] || $grupo_detalles->max != $datos['cupo_max']) {
                $grupo_detalles->docente =  (($datos['docente_select'] == null || $datos['docente_select'] == '') ? $grupo_detalles->docente : $datos['docente_select']);
                $grupo_detalles->maximo =  (($datos['cupo_max'] == null || $datos['cupo_max'] == '') ? $grupo_detalles->maximo : $datos['cupo_max']);
                try {

                    if ($grupo_comparativo != $grupo_detalles) {

                        $this->gruposModel->update($grupo, $grupo_detalles);
                        $mensaje = [
                            'tipo' => 'alert-success',
                            'texto' => "Se ha modificado el grupo " . $datos['materia'] . " de la seccion " . $datos['clave']
                        ];
                    } else {

                        $mensaje = [
                            'tipo' => 'alert-info',
                            'texto' => "No se ha generado ningun cambio"
                        ];
                    }
                    // die();

                    $this->sesion->setFlashdata('datos', $mensaje);
                } catch (\Exception $ex) {
                    $mensaje = [
                        'tipo' => 'alert-danger ',
                        'texto' => "No se ha podido modificar el grupo " . $datos['materia'] . " de la seccion " . $datos['clave']
                    ];
                    $this->sesion->setFlashdata('datos', $mensaje);
                }
            } else {
                $mensaje = [
                    'tipo' => 'alert-info',
                    'texto' => "No se ha generado ningún cambio"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => "No se ha encontrado el grupo buscado"
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
        }
        $bandera = ($this->request->getPost('bandera'));
        // die();
        // var_dump($bandera);
        if ($bandera == 'MapaAulas') {
            return redirect()->to('/ProgramacionAcademica/MapaAulas/' . $this->request->getPost('aula') . "/" . $this->request->getPost('select_periodo'));
        } else if ($bandera == 'AulaVirtual') {
            return redirect()->to('/ProgramacionAcademica/AulaVirtual/');
        } else {
            return redirect()->to('/ProgramacionAcademica/' . $bandera . "/" . $this->request->getPost('select_periodo'));
        }
    }

    /**
     * asigna un grupo al aula y hora solicitada
     */
    public function asignarHoraMateria()
    {
        $datos = explode('|', $this->request->getPost('grupo_detalles'));
        $horas = explode('-', $datos[2]);
        $periodo = $this->request->getPost('select_periodo');
        $aulasgruposEntity  = new AulasGrupos();
        $aulasgruposEntity->gpoaula_grupo = $datos[0];
        $aulasgruposEntity->gpoaula_dia = $datos[5];
        $aulasgruposEntity->gpoaula_inicio = $horas[0];
        $aulasgruposEntity->gpoaula_fin = $horas[1];
        $aulasgruposEntity->aula =  $datos[1];
        $aulasgruposEntity->periodo =  $periodo;
        $bandera = $this->request->getPost('bandera');
        $bandera_empalme = $this->validarEmpalme($datos[4], $datos[6], $datos[1], $datos[5], $horas[0], $horas[1], $periodo);

        if ($bandera_empalme === true) {

            try {
                $this->gruposaulasModel->insert($aulasgruposEntity);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Se asignado la materia " . $datos[3] . " de la seccion " . $datos[4] . "en el aula $datos[1], los dias $datos[5] en un horario de $datos[2]"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
            } catch (\Exception $ex) {
                $mensaje = [
                    'tipo' => 'alert-danger ',
                    'texto' => "No se ha podido  la materia " . $datos[3] . " en el horario y dia seleccionado"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger ',
                'texto' => "No se ha podido asignar la materia " . $datos[3] . " en el horario y dia seleccionado por un empalme de " . $bandera_empalme
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
        }
        /*  var_dump($mensaje);
        die(); */

        if ($bandera == 'AulaVirtual') {
            return redirect()->to('/ProgramacionAcademica/AulaVirtual');
        } else if ($bandera == 'MapaAulas') {
            return redirect()->to('/ProgramacionAcademica/MapaAulas/' . $datos[1] . "/" . $periodo);
        }
    }

    /**
     * despliga el menu princiapal para redireccionar 
     */
    public function Main()
    {
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/home', $data);
    }

    /**
     * despliega vista para agregar materias en linea
     */
    public function AulaVirtual($rol = false)
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['menu'] = $this->sesion->get('menu');
        $data['carreras'] = $this->carrerasModel->findAll();
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $data['periodo'] = $this->periodoProgramacionAcademica;
        $data['bandera_edicion'] = $this->periodoModel->PermisoEdicionPeriodo($data['periodo']);
        $rol_sesion = $this->sesion->get('rol_id');
        switch ($rol_sesion) {
            case '9':
                $rol = 2;
                break;
            case '10':
                $rol = 1;
                break;
            case '1':
                $rol = 'administrador';
                break;

            default:
                $rol = 1;
                break;
        }
        $data['rol'] =  $rol;
        $data['administrador'] = (($rol_sesion == 1) ? true : false);
        return view('academica/aulasvirtuales', $data);
    }

    /**
     * despliga el listado de las aulas virtuales
     */
    public function listadoVirtual($hora_inicial = 0)
    {
        // echo "$hora_inicial <br>";
        // echo $this->request->getPost('bandera') . "<br>";
        $datos = explode('|', $hora_inicial);
        // var_dump($datos);

        $data['grupos'] = $this->aulasgruposModel->getGruposAulasDiaHora('1', $datos[2], $datos[0], $this->periodoProgramacionAcademica);
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/listagruposvirtuales', $data);
    }
    /**
     * devuelve el listadod e grupos en el horario seleccionado
     */
    public function getListadoHorario($indicadores)
    {
        $datos = explode('|', $indicadores);
        $grupos = $this->aulasgruposModel->getGruposAulasDiaHora('1', $datos[3], $datos[0], $datos[4]);
        if ($grupos) {
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($grupos);
        } else return false;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------validacion de empalmes inicio
    /**
     * validacion de la programacion academica
     * @return true si no hay ningun empalme de la seccion o del docente 
     * @param seccion id de la seccion  
     * @param docente id del docente
     * @param aula id del aula 
     */
    public function validarEmpalme($seccion, $docente, $aula, $dia, $hora, $fin, $periodo)
    {
        $grupos_totales = $this->aulasgruposModel->getByPeriodo($periodo);
        $data['dias'] = [
            '1' => 'LUNES',
            '2' => 'MARTES',
            '3' => 'MIÉRCOLES',
            '4' => 'JUEVES',
            '5' => 'VIERNES',
            '6' => 'SÁBADO',
        ];
        // validar que la seccion no tenga otra materia a la misma hora
        echo " $seccion, $docente, $aula , $dia , $hora, $fin , $periodo <br>";
        $arr_grupo = [];
        /*  foreach ($grupos_totales as  $grupo) {
            if($grupo->h_inicio <= $hora && $grupo->h_fin >= $fin){
                echo "$grupo->h_inicio-$grupo->h_fin ==> $hora-$fin <br>";
            }
        } */
        if ($seccion) {
            foreach ($grupos_totales as  $grupo) {
                if ($seccion == $grupo->seccion) {
                    // echo "$grupo->seccion => $grupo->ma_nombre [$grupo->h_inicio-$grupo->h_fin] $grupo->dia $grupo->aula<br>";
                    if ($grupo->dia == $dia) {
                        if ($grupo->h_inicio <= $hora && $grupo->h_fin >= $fin) {
                            return 'seccion';
                        } elseif ($hora <= $grupo->h_inicio  && $fin >= $grupo->h_fin) {
                            return 'seccion';
                        }
                    }
                }
            }
        }   // si conluye el ciclo normal entonces esta seccion no tiene otra materia a la misma hora el mismo dia
        if ($docente) {
            if ($docente != 1) {
                foreach ($grupos_totales as  $grupo) {
                    if ($grupo->docente == $docente) {
                        //  echo "$grupo->seccion => $grupo->ma_nombre [$grupo->em_nombre] [$grupo->h_inicio-$grupo->h_fin] $grupo->dia $grupo->aula<br>";
                        if ($grupo->dia == $dia) {
                            if ($grupo->h_inicio <= $hora && $grupo->h_fin >= $fin) {
                                return 'docente';
                            } elseif ($hora <= $grupo->h_inicio  && $fin >= $grupo->h_fin) {
                                return 'docente';
                            }
                        }
                    }
                }
            }
        } // Si concluye este proceso con exito entonces no existe otra materia impatida por este docente a la hora seleccionada

        if ($aula) {
            // echo 'aula ' . $aula ."";
            if ($aula != 1) {
                foreach ($grupos_totales as  $grupo) {
                    if ($grupo->aula == $aula && $aula != 1) {
                        // echo "$grupo->seccion => $grupo->ma_nombre [$grupo->em_nombre] [$grupo->h_inicio-$grupo->h_fin] $grupo->dia $grupo->aula<br>";
                        if ($grupo->dia == $dia) {
                            if ($grupo->h_inicio <= $hora && $grupo->h_fin >= $fin) {
                                return 'aula';
                            } elseif ($hora <= $grupo->h_inicio  && $fin >= $grupo->h_fin) {
                                return 'aula';
                            }
                        }
                    }
                }
            }
        }
        // echo 'true';
        return true;
        // echo json_encode($arr_grupo);
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------validacion de empalmes fin

    /**
     * elimina del horario una materia seleccionada en las aulas virtuales
     * @param datos contiene los datos de identificacion del grupo
     */
    public function popHorarioVirtual($datos)
    {
        $informacion = explode('|', $datos);
        $grupo  = $this->gruposaulasModel->getGrupoHoraDia($informacion[0], $informacion[4], $informacion[2]);
        if ($grupo) {
            try {
                $this->gruposaulasModel->delete($grupo[0]->id);
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode('exito');
            } catch (\Exception $ex) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode('falso');
            }
        } else return false;
    }

    /**
     * Despliega el menu de docentes para revisar su respectivo horario por periodo
     */
    public function MenuDocentes()
    {
        $data = $this->request->getGet();
        if (!count($data) || !array_key_exists('id', $data)) {
            $data = ['id' => '', 'apPaterno' => '', 'apMaterno' => '', 'nombre' => '',];
        }
        $empleados = $this->docenteModel->buscar($data);
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['empleados'] = $empleados;
        $data['pager'] = $this->docenteModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        $data['periodos'] = $this->periodoModel->lista();
        return view('academica/listadodocentes', $data);
    }

    /**
     * retorna la vista del horario seleccionado en el periodo indicado
     * @param empleado ID docente
     */
    public function HorarioDocente($docente, $periodo = false)
    {
        $data['rol'] = $this->sesion->get('rol');
        // $data['rol'] = 'DIRECTOR MEDICINA';
        $data['docente'] = $this->docenteModel->find($docente);
        $data['periodo']  = (($periodo != false) ? $periodo : $this->request->getPost('periodo'));
        $data['grupos'] = $this->aulasgruposModel->getGruposByDocente($data['periodo'], $docente);
        $data['mensaje'] = false;
        if (is_null($data['grupos'])) {
            $data['grupos'] = false;
            $data['mensaje'] = [
                'tipo' => 'alert-info',
                'texto' => "No se han encontrado grupos impartidos por el docente " . $data['docente']->apPaterno . " " . $data['docente']->apMaterno . " " . $data['docente']->nombre  . " en el periodo " . $data['periodo'],
            ];
        }
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['permisos'] = $this->permisosPorRol($data['rol'], $data['periodo']);
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $data['permiso'] = ((in_array($data['rol'], $this->roles_administradores)) ? true : false);
        $data['materias'] =  $this->gruposmateriasModel->buscar(['periodo' => $data['periodo'], 'docente' => $docente]);
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/mapadocentes', $data);
        // $mensaje = 
        // $this->sesion->setFlashdata('datos', $mensaje);
        // return redirect()->to('/ProgramacionAcademica/MenuDocentes');
    }

    /**
     * retorna el menu principal para el listado de horarios para el docente
     */
    private function HorarioDocente_old($docente = false)
    {
        if (!$docente) {
            $data['select_docente'] = (($this->request->getPost('select_docente') && $this->request->getPost('select_docente') != '0') ? $this->request->getPost('select_docente') : false);
        } else {
            $data['select_docente'] = $docente;
        }
        $periodo = $this->periodoProgramacionAcademica;
        $rol_sesion = $this->sesion->get('rol_id');
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        switch ($rol_sesion) {
            case '9':
                $rol = 2;
                break;
            case '10':
                $rol = 1;
                break;
            case '1':
                $rol = 'administrador';
                break;

            default:
                $rol = 1;
                break;
        }
        $data['rol'] =  $rol;
        $data['bandera_edicion'] = $this->periodoModel->PermisoEdicionPeriodo($periodo);
        $data['bandera'] = 'HorarioDocente';
        $data['grupos'] = $this->aulasgruposModel->getGruposByDocente($periodo, $data['select_docente']);
        $data['periodo'] =  $periodo;
        $data['docentes'] = $this->docenteModel->getEmpleadosDocentes();
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/listasdocentes', $data);
    }

    /**
     * funcion para retornar la aulas que no se encuentran ocupadas en el horario seleccionado
     * @param parametros  [[0]=>hora_inicio, [1]=> hora_fin, [2]=>dia, [3]=>periodo] 
     * @return arr_aulas compilado de aulas disponibles
     */
    public function getAulasDisponibles($datos)
    {
        $parametros = explode('|', $datos);
        $listados =  $this->aulasgruposModel->getAulasDia($parametros[2], $parametros[0], $parametros[1], $parametros[3]);
        $aulas = $this->aulasModel->getAulasId(false);
        $arr_aulas = [];
        foreach ($aulas as $aula) {
            $bandera_coincidencia = true;
            foreach ($listados as $grupo) {
                if ($grupo->aula == $aula->id_aula || $aula->id_aula == 'VES') {
                    $bandera_coincidencia = false;
                }
            }
            if ($bandera_coincidencia || $aula->id_aula == 1) {
                $arr_aulas[] = $aula;
            }
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($arr_aulas);
    }

    /**
     * validacion para el cambio de docente de un grupo
     * @param docente_actual id del docente acutamente responsable del grupo
     * @param docente_cambio ID del docente que reemplazara al enterior en el grupo
     * @param grupo ID del grupo que se realiza el cambio
     * @return false si no existen errores por empalmes
     * @return true si extiste algun emplame del docente 
     */
    public function cambioDocente($grupo, $docente_cambio)
    {
        $grupos_propios = $this->aulasgruposModel->getGruposByDocente($this->periodoProgramacionAcademica, $docente_cambio);
        $grupo_actual  = $this->aulasgruposModel->getByIDC($grupo);
        if ($grupos_propios && $grupo_actual && $docente_cambio != 1) {
            foreach ($grupos_propios as $propios) {
                foreach ($grupo_actual as $actual) {
                    if ($propios->dia == $actual->dia) {
                        if ($propios->h_inicio <= $actual->h_inicio && $propios->h_fin >= $actual->h_fin) {
                            $this->response->setHeader('Content-Type', 'application/json');
                            echo json_encode('empalme');
                            die();
                        } elseif ($actual->h_inicio <= $propios->h_inicio  &&  $actual->h_fin >=  $propios->h_fin) {
                            $this->response->setHeader('Content-Type', 'application/json');
                            echo json_encode('empalme');
                            die();
                        }
                    }
                }
            }
            return null;
        } elseif ($docente_cambio == 1) {
            return null;
        } else {
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode('Sin datos');
            die();
        }
    }
    /**
     * retorna el menú para busqueda y despliegue de horario de las secciones disponibles
     */
    public function HorarioSecciones($seccion = false, $periodo = false)
    {
        $data['rol'] = $this->sesion->get('rol');
        // $data['rol'] = 'GENERAL';
        // echo json_encode($data['rol']);
        $data['carrera'] = ((in_array($data['rol'], $this->roles_medicina)) ? 'MED' : ((in_array($data['rol'], $this->roles_enfermeria)) ? 'LEO' : 'TODOS'));
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['periodos'] = $this->periodoModel->lista();
        $data['menu'] = $this->sesion->get('menu');
        $data['periodo'] = false;
        if (($this->request->getPost('seccion') && $this->request->getPost('periodo'))) {

            $data['seccion'] = $this->request->getPost('seccion');
            $data['periodo'] = $this->request->getPost('periodo');
        } else if ($seccion == true && $periodo == true) {
            $data['seccion'] = $seccion;
            $data['periodo'] = $periodo;
        }
        if (isset($data['seccion']) && isset($data['periodo'])) {
            $data['permisos'] = $this->permisosPorRol($data['rol'], $data['periodo']);
            $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
            $data['grupos'] = $this->aulasgruposModel->getGruposBySeccion($data['periodo'], $data['seccion']);
            $data['permiso'] = ((in_array($data['rol'], $this->roles_administradores)) ? true : false);
            $data['materias'] =  $this->gruposmateriasModel->getBySeccion($data['seccion'], $data['periodo']);
        }
        return view('academica/mapasecciones', $data);
    }

    /**
     * retorna los permisos correspondientes al rol seleccionado
     * @param rol Nombre del rol del usuario
     * @param periodo Id del periodo
     */
    private function permisosPorRol($rol, $periodo)
    {
        $permiso_periodo = false;
        $periodo_model = $this->periodoModel->find($periodo);
        if (in_array($rol, $this->roles_administradores)) {
            switch ($periodo_model->estado) {
                case 'INICIO':
                    $permiso_periodo = true;
                    break;
                case 'ADMISION':
                    $permiso_periodo = true;
                    break;
                case 'CURSO':
                    $permiso_periodo = true;
                    break;
                default:
                    $permiso_periodo = false;
                    break;
            }
            return ['periodo' => $permiso_periodo, 'rol' => 'ADMINISTRADOR', 'carrera' => ''];
        } else if (in_array($rol, $this->roles_enfermeria)) {
            switch ($periodo_model->estado) {
                case 'INICIO':
                    $permiso_periodo = true;
                    break;
                case 'ADMISION':
                    $permiso_periodo = true;
                    break;
                case 'CURSO':
                    $permiso_periodo = false;
                    break;

                default:
                    $permiso_periodo = false;
                    break;
            }
            return ['periodo' => $permiso_periodo, 'carrera' => '1', 'rol' => (($rol == 'DIRECTOR ENFERMERIA') ? 'DIRECTOR' : FALSE)];
        } else if (in_array($rol, $this->roles_medicina)) {
            switch ($periodo_model->estado) {
                case 'INICIO':
                    $permiso_periodo = true;
                    break;
                case 'ADMISION':
                    $permiso_periodo = true;
                    break;
                case 'CURSO':
                    $permiso_periodo = false;
                    break;

                default:
                    $permiso_periodo = false;
                    break;
            }
            return ['periodo' => $permiso_periodo, 'carrera' => '2', 'rol' => (($rol == 'DIRECTOR MEDICINA') ? 'DIRECTOR' : FALSE)];
        }
        return ['periodo' => $permiso_periodo, 'carrera' => '', 'rol' => false];
    }

    /**
     * retorna las secciones disponilbes en el periodo seleccionado
     */
    public function getSeccionesPeriodo($carrera, $periodo)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $secciones = $this->gruposModel->getSeccionesPeriodo($periodo, (($carrera == 'TODOS') ? '' : $carrera));
        // $secciones = $periodo;
        if ($secciones) {
            echo json_encode($secciones);
        } else return false;
    }

    /**
     * funcion para desplegar las secciones disponibles
     */
    private function HorarioSecciones_old($seccion = false)
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        if (!$seccion) {
            $data['select_seccion'] = (($this->request->getPost('select_seccion') && $this->request->getPost('select_seccion') != '0') ? $this->request->getPost('select_seccion') : false);
        } else {
            $data['select_seccion'] = $seccion;
        }
        $periodo = $this->periodoProgramacionAcademica;
        $data['bandera'] = 'HorarioDocente';
        $grupos = false;
        if ($data['select_seccion']) {
            $grupos = $this->aulasgruposModel->getGruposBySeccion($periodo, $data['select_seccion']);
        }
        $data['periodos'] = $this->periodoModel->lista();
        $data['grupos'] = $grupos;
        $data['periodo'] =  $periodo;
        $rol = $this->sesion->get('rol_id');
        $carrera = (($rol == 9) ? 'MED' : (($rol == 10) ? 'LEO' : ''));
        switch ($rol) {
            case '9':
                $rol = 2;
                break;
            case '10':
                $rol = 1;
                break;
            case '1':
                $rol = 'administrador';
                break;

            default:
                $rol = 1;
                break;
        }
        $data['rol'] =  $rol;
        $data['bandera_edicion'] = $this->periodoModel->PermisoEdicionPeriodo($periodo);
        $data['secciones'] = $this->gruposModel->getSeccionesPeriodo($periodo, $carrera);
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/listasecciones', $data);
    }

    /**
     * funcion de prueba
     */
    public function getPediodo()
    {
        echo json_encode($this->periodoProgramacionAcademica);
    }

    /**
     * despliga vista para la asignacion de horas de practicas clinicas por dia
     * @param periodo ID del periodo
     * @param dia 
     * 
     */
    public function PracticasClinicas($periodo = false, $dia = false)
    {
        if (!$periodo) $periodo = $this->request->getPost('select_periodo');
        $carrera = $this->request->getPost('select_carrera');
        // $periodo = (($periodo == false) ? false : $periodo);

        $data['periodo'] = $periodo;
        $data['carrera_selected'] = (($carrera) ? $carrera : '');

        $data['periodos'] =  $this->periodoModel->findAll();
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['menu'] = $this->sesion->get('menu');
        $data['carreras'] = $this->carrerasModel->findAll();
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $dia = $this->request->getPost('selct_dia');
        $data['dia_selected'] = (($dia) ? $dia : '');
        $rol_sesion = $this->sesion->get('rol_id');
        $rol =  (($rol_sesion == 9) ? '2' : (($rol_sesion == 10) ? '1' : 'administrador'));
        $data['rol'] =  $rol;
        $data['administrador'] = (($rol_sesion == 1) ? true : false);
        $grupos = false;
        // var_dump($periodo, $dia, '2');
        if ($periodo && $dia) {
            $grupos = $this->aulasgruposModel->getByAulaDia($periodo, $dia, '2');
            $data['periodo_detalles'] = $this->periodoModel->find($periodo);
        }
        $data['grupos'] = $grupos;
        $data['bandera'] = 'PracticasClinicas/' . $periodo;
        // var_dump($grupos);
        return view('academica/practicasClinicas', $data);
    }

    /**
     * despliga las listas de las practicas clinicas
     * @param periodo ID del periodo
     */
    public function ListadoMaterias($periodo)
    {
        // var_dump($periodo);
        $data['bandera'] = 'PracticasClinicas/' . $periodo;
        // var_dump($grupos);
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/listasPC', $data);
    }

    /**
     * despliega la vista para la configuracion de las practicas clinicas
     * @param periodo ID del periodo
     * @param seccion ID de seccion
     */
    public function ConfiguradorPC($periodo = false, $seccion = false)
    {
        if (!$periodo) $periodo = $this->request->getPost('select_periodo');
        $data['periodos'] =  $this->periodoModel->findAll();
        // $rol = $this->sesion->get('rol_id');
        $rol = 1;
        $carrera = (($rol == 9) ? '2' : (($rol == 10) ? '1' : ''));
        $data['carreras'] = $this->carrerasModel->findAll();
        $data['carrera_select'] = $carrera;
        // $rol = 1;
        $data['rol'] = $rol;
        $data['periodo'] = $periodo;
        $data['menu'] = $this->sesion->get('menu');
        return view('academica/configuradorpc', $data);
    }

    /**
     * retorna la lista de secciones disponibles en un periodo seleccionado
     * @param periodo ID del periodo
     * @param rol ID del rol de la sesion
     */
    public function getSeccionesDisponibles($periodo, $carrera)
    {
        if ($periodo && $carrera) {
            $carrera = (($carrera == 2) ? 'MED' : (($carrera == 1) ? 'LEO' : ''));
            $secciones = $this->gruposModel->getSeccionesPeriodo($periodo, $carrera);
            if ($secciones) {
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($secciones);
            } else return 'secciones';
        } else return 'parametros';
    }

    /**
     * retorna el listado de grupos de practicas clinicas para la seccion indicada
     * @param periodo ID del periodo
     * @param carrera ID de la carrera
     * @param seccion ID de seccion
     */
    public function getPracticasClinicas($periodo)
    {
        if ($periodo) {
            $materias = $this->gruposmateriasModel->getPracticasClinicas($periodo);
            if ($materias) {
                // $materias[]= ['algo'=>'valor'];
                foreach ($materias as $materia) {
                    $horarios =  $this->gruposaulasModel->getByGrupo($materia->grupo);
                    // $horarios =$this->getHorarioPC();
                    if ($horarios) {
                        $materia->inicio = $horarios[0]->inicio;
                        $materia->fin = $horarios[0]->fin;
                    }
                }
                $this->response->setHeader('Content-Type', 'application/json');
                echo json_encode($materias);
            } else return null;
        } else return null;
    }

    /**
     * retorna los horarios en los que se da la practica clinica del grupo seleccionado
     * @param grupo ID del grupo seleccionado
     * @return horarios si encuentra registro de la practica
     * @return null si no hay horario registrado de la practica clinica
     */
    public function getHorarioPC($grupo)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $horarios =  $this->gruposaulasModel->getByGrupo($grupo);
        if ($horarios) {
            // echo $horarios[0]->inicio." ".$horarios[0]->fin."<br>";
            return json_encode($horarios[0]);
        } else return null;
    }

    public function guardarhorario()
    {
        var_dump($this->request->getPost());
        $grupos = $this->request->getPost();
    }

    /**
     * Genera el cambio de aulas del grupo seleccionado
     * 
     */
    public function CambioAulas()
    {
        $grupo_detalles = explode('|', $this->request->getPost('grupo_detalle'));
        $grupo_horas = $this->gruposaulasModel->getGrupoHoraDia($grupo_detalles[4], $grupo_detalles[2], $grupo_detalles[0]);
        $grupo = $this->gruposModel->find($grupo_detalles[4]);
        $bandera_empalme = ($this->validarCambioAula($this->request->getPost('grupo_aula'), $grupo_detalles[2], $grupo_detalles[0], $grupo_detalles[1], $grupo_detalles[3]));
        if ($bandera_empalme == true) {
            try {
                $aula_anterior = $grupo_horas[0]->aula;
                $grupo_horas[0]->aula = $this->request->getPost('grupo_aula');
                $this->gruposaulasModel->update($grupo_horas[0]->id, $grupo_horas[0]);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Se asignado el aula " . $this->request->getPost('grupo_aula') . " a la materia " . $grupo_detalles[5] . ".",
                ];
                log_message('info', 'Se modifico el aula del grupo ' . $grupo_detalles[4] . " del aula " . $aula_anterior . " al aula " . $this->request->getPost('grupo_aula'));
            } catch (\Exception $ex) {
                $mensaje = [
                    'tipo' => 'alert-danger',
                    'texto' => "No se ha podido reasignar el aula de la materia " . $grupo_detalles[5] . " " . $ex,
                ];
                log_message('info', 'No hacer el cambio de aulas del grupo ' . $grupo_detalles[4] . " " . $ex);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger',
                'texto' => "No se ha podido reasignar el aula de la materia " . $grupo_detalles[5] . " debido a un empalme de aula.",
            ];
            log_message('info', 'Empalme en el aula  ' . $this->request->getPost('grupo_aula') . " $grupo_detalles[2] $grupo_detalles[0] $grupo_detalles[1] ");
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
    }

    /**
     * valida unicamente el espacio de aula seleccionado
     * @param aula ID del aula
     * @param dia 
     * @param hora hora de inicio de la clase
     * @param fin hora de final de la clase
     * @param perdio ID del periodo de busqueda
     * @return true en caso de que no haya coincidencias, es decir que esta disponible para hacer el cambio
     * @return aula string que indica que el aula seleccionada encuentra un empalme
     */
    public function validarCambioAula($aula, $dia, $hora, $fin, $periodo)
    {
        $grupos_totales = $this->aulasgruposModel->getByPeriodo($periodo);
        $data['dias'] = [
            '1' => 'LUNES',
            '2' => 'MARTES',
            '3' => 'MIÉRCOLES',
            '4' => 'JUEVES',
            '5' => 'VIERNES',
            '6' => 'SÁBADO',
        ];
        if ($aula) {
            if ($aula != 1) {
                foreach ($grupos_totales as  $grupo) {
                    if ($grupo->aula == $aula && $aula != 1) {
                        if ($grupo->dia == $dia) {
                            if ($grupo->h_inicio <= $hora && $grupo->h_fin >= $fin) {
                                return 'aula';
                            } elseif ($hora <= $grupo->h_inicio  && $fin >= $grupo->h_fin) {
                                return 'aula';
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * retorna los grupo impartidos por el docente 
     * @param docente ID del docente
     * @param periodo ID del periodo
     */
    public function GruposDocente($docente, $periodo)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $grupos_totales = $this->gruposmateriasModel->getByDocentePeriodo($docente, $periodo);
        if ($grupos_totales) {
            foreach ($grupos_totales as $grupo) {
                $contador_horas = 0;
                $grupo_actual = $this->aulasgruposModel->getByIDC($grupo->grupo);
                foreach ($grupo_actual as $detalles) {
                    $horas = date_diff(date_create($detalles->h_inicio), date_create($detalles->h_fin));
                    // echo ($horas->h)."<br>" ;
                    if ($horas->h < 1) {
                        $contador_horas++;
                    } else {
                        $contador_horas += $horas->h;
                    }
                }
                $grupo->horas_asignadas = $contador_horas;
                // echo json_encode($grupo_actual[0]);
                // die();

            }
            // echo json_encode($horas);
            return json_encode($grupos_totales);
            die();
        }
        return false;
    }

    /**
     * Genera el registro de hora de un grupo seleccionado
     * @param 
     */
    public function SaveGroup()
    {
        $datos = explode('|', $this->request->getPost('grupo_detalles'));
        $aulasgruposEntity  = new AulasGrupos();
        $aulasgruposEntity->gpoaula_grupo = $datos[1];
        $aulasgruposEntity->gpoaula_dia = $datos[4];
        $aulasgruposEntity->gpoaula_inicio = $datos[2];
        $aulasgruposEntity->gpoaula_fin = $datos[3];
        $aulasgruposEntity->aula =  $datos[6];
        $aulasgruposEntity->periodo =  $datos[5];
        $grupo = $this->gruposModel->find($datos[1]);
        // echo json_encode($datos);
        // echo "<br>";
        // echo json_encode($grupo);
        // die();
        $bandera_empalme = $this->validarEmpalme($grupo->clave, $grupo->docente, $datos[6], $datos[4], $datos[2],  $datos[3], $datos[5]);

        if ($bandera_empalme === true) {

            try {
                $this->gruposaulasModel->insert($aulasgruposEntity);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Se asignado la materia " . mb_strtoupper($datos[0])  . " de la seccion " . $grupo->clave . " en el aula $datos[6], los dias $datos[4] en un horario de $datos[2] a $datos[3]"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
            } catch (\Exception $ex) {
                $mensaje = [
                    'tipo' => 'alert-danger ',
                    'texto' => "No se ha podido  la materia " . mb_strtoupper($datos[0]) . " en el horario y dia seleccionado"
                ];
                $this->sesion->setFlashdata('datos', $mensaje);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-danger ',
                'texto' => "No se ha podido asignar la materia " . mb_strtoupper($datos[0]) . " en el horario y dia seleccionado por un empalme de " . $bandera_empalme
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
        }
        return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
    }
    /**
     * retorna los docentes disponibles para dar clase a la hora seleccionada   
     * @param inicio Hora de inicio de la clase
     * @param fin hora de fin de la clase
     * @param dia 
     * @param periodo 
     */
    public function getDocentesDisponibles($inicio, $fin, $dia, $periodo)
    {
        $listados =  $this->aulasgruposModel->getAulasDia($dia, $inicio, $fin, $periodo);
        $docentes = $this->docenteModel->getEmpleadosDocentes();
        foreach ($docentes as $docente) {
            $bandera = true;
            foreach ($listados as $grupo) {
                if ($grupo->empleado_id == $docente->id) {
                    $bandera = false;
                }
            }
            if ($bandera || $docente->id == 1) {
                $arr_docentes[] = $docente;
            }
        }
        if ($arr_docentes) {
            $this->response->setHeader('Content-Type', 'application/json');
            echo json_encode($arr_docentes);
        } else return null;
    }

    /**
     * Cambia el docente de la materia seleccionada
     */
    public function CambioDocenteGrupo()
    {
        // var_dump();
        $grupo = $this->request->getPost('grupo_detalle');
        $docente_cambio = $this->request->getPost('docente');
        $grupo_actual  = $this->aulasgruposModel->getByIDC($grupo);
        $mensaje = [
            'tipo' => 'alert-danger',
            'texto' => "No se ha podido cambiar al docente de la materia " . $grupo_actual[0]->ma_nombre . " de la seccion " . $grupo_actual[0]->seccion . " debido a un empalme de aula.",
        ];
        $grupos_propios = $this->aulasgruposModel->getGruposByDocente($this->periodoProgramacionAcademica, $docente_cambio);
        if ($docente_cambio != 1) {
            if ($grupos_propios && $grupo_actual) {
                foreach ($grupos_propios as $propios) {
                    foreach ($grupo_actual as $actual) {
                        if ($propios->dia == $actual->dia) {
                            if ($propios->h_inicio <= $actual->h_inicio && $propios->h_fin >= $actual->h_fin) {
                                $this->sesion->setFlashdata('datos', $mensaje);
                                return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
                                // return false;
                            } elseif ($actual->h_inicio <= $propios->h_inicio  &&  $actual->h_fin >=  $propios->h_fin) {
                                $this->sesion->setFlashdata('datos', $mensaje);
                                return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
                            }
                        }
                    }
                }
            }

            $mensaje =  $this->updateDocenteGrupo($grupo, $docente_cambio);
        } else {
            $mensaje =  $this->updateDocenteGrupo($grupo, $docente_cambio);
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/ProgramacionAcademica/' . $this->request->getPost('bandera'));
    }

    /**
     * realiza el update de docente en un grupo
     * @param grupo ID del grupo
     * @param docente_cambio ID del docente nuevo para el grupo
     */
    public function updateDocenteGrupo($grupo, $docente_cambio)
    {
        $grupo_model = $this->gruposModel->find($grupo);
        try {
            $grupo_model->docente = $docente_cambio;
            $this->gruposModel->update($grupo, $grupo_model);
            log_message('info', 'Se modifico el docente del grupo ' . $grupo);
            return [
                'tipo' => 'alert-success',
                'texto' => "Se ha actualizado correctamente el docente del grupo $grupo.",
            ];
        } catch (\Exception $ex) {

            log_message('info', 'No se pudo realizar el cambio de docente grupo ' . $grupo . " " . $ex);
            return [
                'tipo' => 'alert-alert',
                'texto' => 'No se pudo realizar el cambio de docente grupo ' . $grupo,
            ];
        }
    }
    public function GrupoSecciones($seccion, $periodo)
    {
        $this->response->setHeader('Content-Type', 'application/json');
        $grupos_totales = $this->gruposmateriasModel->buscarporseccion($seccion, $periodo);
        if ($grupos_totales) {
            foreach ($grupos_totales as $grupo) {
                $contador_horas = 0;
                $grupo_actual = $this->aulasgruposModel->getByIDC($grupo->grupo);
                foreach ($grupo_actual as $detalles) {
                    $horas = date_diff(date_create($detalles->h_inicio), date_create($detalles->h_fin));
                    if ($horas->h < 1) {
                        $contador_horas++;
                    } else {
                        $contador_horas += $horas->h;
                    }
                }
                $grupo->horas_asignadas = $contador_horas;
            }
            return json_encode($grupos_totales);
            die();
        }
        return false;
    }

    /**
     * retorna un menú de los espacios totales disponibles por dia
     */
    public function EspaciosDisponibles($dia = false, $periodo = false)
    {
        $data['periodos'] = $this->periodoModel->lista();
        $data['periodo'] = $periodo;
        $data['menu'] = $this->sesion->get('menu');
        $data['dias'] = ['1' => 'LUNES', '2' => 'MARTES', '3' => 'MIÉRCOLES', '4' => 'JUEVES', '5' => 'VIERNES', '6' => 'SÁBADO',];
        $data['dia_selected'] = false;
        $data['periodo_selected'] = false;
        $data['horas_ocupadas']  = false;
        $data['aulas'] = $this->aulasModel->aulasMapa();
        $data['aulas_filtros'] = ((!is_null($this->request->getPost('filtro_aulas'))) ? $this->aulasModel->getAulasFiltradas($this->request->getPost('filtro_aulas')) : $data['aulas']);
        if ($dia != false && $periodo != false) {
            $data['dia_selected'] = $dia;
            $data['periodo_selected'] = $periodo;
        } else if ($this->request->getPost('select_dia') != false && $this->request->getPost('select_periodo')) {
            $data['dia_selected'] =  $this->request->getPost('select_dia');
            $data['periodo_selected'] = $this->request->getPost('select_periodo');
        }

        if ($data['dia_selected'] != false &&  $data['periodo_selected'] != false) {
            $data['horas_ocupadas'] =  $this->aulasgruposModel->busca(['periodo' => $data['periodo_selected'], 'dia' => $data['dia_selected']]);
        }
        return view('academica/espaciosdisponibles_v2', $data);
    }
}
