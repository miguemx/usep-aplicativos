DROP VIEW IF EXISTS view_aspirantes_respuestas;

CREATE VIEW view_aspirantes_respuestas AS
select `aspirantes_respuestas`.`respuesta_id` AS `respuesta_id`,
    `aspirantes_respuestas`.`respuesta_aspirante` AS `respuesta_aspirante`,
    `aspirantes_respuestas`.`respuesta_valor` AS `respuesta_valor`,
    `aspirantes_respuestas`.`respuesta_comentario` AS `respuesta_comentario`,
    `aspirantes_campos`.`campo_id` AS `campo_id`,
    `aspirantes_campos`.`campo_nombre` AS `campo_nombre`,
    `aspirantes_campos`.`campo_descripcion` AS `campo_descripcion`,
    `aspirantes_campos`.`campo_orden` AS `campo_orden`,
    `aspirantes_campos`.`campo_obligatorio` AS `campo_obligatorio`,
    `aspirantes_campos`.`campo_visualizar` AS `campo_visualizar`,
    `aspirantes_tipos_campo`.`tipocampo_id` AS `tipocampo_id`,
    `aspirantes_tipos_campo`.`tipocampo_nombre` AS `tipocampo_nombre`,
    `aspirantes_tipos_campo`.`tipocampo_valores` AS `tipocampo_valores`,
    `aspirantes_tipos_campo`.`tipocampo_clasificacion` AS `tipocampo_clasificacion`,
    `aspirantes_formularios`.`formulario_id` AS `formulario_id`,
    `aspirantes_formularios`.`formulario_etapa` AS `formulario_etapa`,
    `aspirantes_formularios`.`formulario_nombre` AS `formulario_nombre`,
    `aspirantes_formularios`.`formulario_titulo` AS `formulario_titulo`,
    `aspirantes_formularios`.`formulario_orden` AS `formulario_orden`,
    `aspirantes_formularios`.`formulario_instrucciones` AS `formulario_instrucciones`,
    `aspirantes_etapas`.`etapa_nombre` AS `etapa_nombre`,
    `aspirantes_etapas`.`etapa_periodo` AS `etapa_periodo`,
    `aspirantes_etapas`.`etapa_orden` AS `etapa_orden`,
    `aspirantes_etapas`.`etapa_formulario` AS `etapa_formulario`,
    `aspirantes_etapas`.`etapa_responsable` AS `etapa_responsable`
    
from (
        (
            (
                (`aspirantes_respuestas` 
                    join `aspirantes_campos` on(`aspirantes_respuestas`.`respuesta_campo` = `aspirantes_campos`.`campo_id`)
                ) 
                join `aspirantes_formularios` on(`aspirantes_campos`.`campo_formulario` = `aspirantes_formularios`.`formulario_id`)
            ) 
            join `aspirantes_etapas` on(`aspirantes_formularios`.`formulario_etapa` = `aspirantes_etapas`.`etapa_id`)
        ) 
        join `aspirantes_tipos_campo` on(`aspirantes_campos`.`campo_tipo` = `aspirantes_tipos_campo`.`tipocampo_id`)
    )