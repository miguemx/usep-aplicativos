-- se añade campo para visualizar en la revision documental
ALTER TABLE `aspirantes_campos` ADD `campo_visualizar` INT NOT NULL AFTER `campo_obligatorio`;


-- se altera vista para agregar el campo visualizacion
ALTER
 VIEW `view_aspirantes_respuestas` as
SELECT
    `usep_aplicativos`.`aspirantes_respuestas`.`respuesta_id` AS `respuesta_id`,
    `usep_aplicativos`.`aspirantes_respuestas`.`respuesta_aspirante` AS `respuesta_aspirante`,
    `usep_aplicativos`.`aspirantes_respuestas`.`respuesta_valor` AS `respuesta_valor`,
    `usep_aplicativos`.`aspirantes_respuestas`.`respuesta_comentario` AS `respuesta_comentario`,
    `usep_aplicativos`.`aspirantes_respuestas`.`created_at` AS `created_at`,
    `usep_aplicativos`.`aspirantes_respuestas`.`updated_at` AS `updated_at`,
    `usep_aplicativos`.`aspirantes_respuestas`.`deleted_at` AS `deleted_at`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_id` AS `campo_id`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_nombre` AS `campo_nombre`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_descripcion` AS `campo_descripcion`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_orden` AS `campo_orden`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_obligatorio` AS `campo_obligatorio`,
    `usep_aplicativos`.`aspirantes_campos`.`campo_visualizar` AS `campo_visualizar`,
    `usep_aplicativos`.`aspirantes_tipos_campo`.`tipocampo_id` AS `tipocampo_id`,
    `usep_aplicativos`.`aspirantes_tipos_campo`.`tipocampo_nombre` AS `tipocampo_nombre`,
    `usep_aplicativos`.`aspirantes_tipos_campo`.`tipocampo_valores` AS `tipocampo_valores`,
    `usep_aplicativos`.`aspirantes_tipos_campo`.`tipocampo_clasificacion` AS `tipocampo_clasificacion`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_id` AS `formulario_id`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_etapa` AS `formulario_etapa`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_nombre` AS `formulario_nombre`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_titulo` AS `formulario_titulo`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_orden` AS `formulario_orden`,
    `usep_aplicativos`.`aspirantes_formularios`.`formulario_instrucciones` AS `formulario_instrucciones`,
    `usep_aplicativos`.`aspirantes_etapas`.`etapa_nombre` AS `etapa_nombre`
FROM
    (
        (
            (
                (
                    `usep_aplicativos`.`aspirantes_respuestas`
                JOIN `usep_aplicativos`.`aspirantes_campos` ON
                    (
                        `usep_aplicativos`.`aspirantes_respuestas`.`respuesta_campo` = `usep_aplicativos`.`aspirantes_campos`.`campo_id`
                    )
                )
            JOIN `usep_aplicativos`.`aspirantes_formularios` ON
                (
                    `usep_aplicativos`.`aspirantes_campos`.`campo_formulario` = `usep_aplicativos`.`aspirantes_formularios`.`formulario_id`
                )
            )
        JOIN `usep_aplicativos`.`aspirantes_etapas` ON
            (
                `usep_aplicativos`.`aspirantes_formularios`.`formulario_etapa` = `usep_aplicativos`.`aspirantes_etapas`.`etapa_id`
            )
        )
    JOIN `usep_aplicativos`.`aspirantes_tipos_campo` ON
        (
            `usep_aplicativos`.`aspirantes_campos`.`campo_tipo` = `usep_aplicativos`.`aspirantes_tipos_campo`.`tipocampo_id`
        )
    );