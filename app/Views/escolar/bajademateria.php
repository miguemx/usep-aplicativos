<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .input-error {
        border: 1px red solid !important;
    }
</style>
<br>

<?php if (!isset($bandera_busqueda)) : ?>
    <?php if ($mensaje) : ?>
        <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
            <?php echo $mensaje['texto']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <form action="<?php echo base_url('Escolar/buscarmaterias') ?>" method="post">
                <h3>Baja de Materia </h3>
                <div class="mb-3">
                    <label for="matricula" class="form-label">MATRICULA DE ALUMNO</label>
                    <input type="text" name="matricula" id="matricula" class="form-control" placeholder="" aria-describedby="helpId">
                    <small id="helpId" class="text-muted">Ingrese la matricula del alumno para consultar sus materias</small>
                </div>

                <div class="d-grid gap-2">
                    <div class="text-center">
                        <button type="submit" name="busqueda" id="busqueda" class="btn btn-secondary">Buscar Alumno</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php endif; ?>
<?php if (isset($bandera_busqueda)) : ?>
    <div class="container">
        <div>
            <div class="card">
                <form action="<?php echo base_url('Escolar/Bajadesecciones') ?>" method="post" id="eliminar_materia">
                    <div class="card-body">
                        <input type="hidden" name="matricula" value=" <?php echo $alumno->id; ?> ">
                        <div class="datos text-center">
                            <label style="margin: 0 10px">MATRICULA:<h4 class="card-title"> <?php echo $alumno->id;  ?></h4> </label>
                            <label style="margin: 0 10px">NOMBRE:<h4 class="card-title"> <?php echo $alumno->apPaterno . " " . $alumno->apMaterno . " " . $alumno->nombre;  ?></h4> </label>
                        </div>
                        <h4 class="card-title"> </h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class=" text-center">CLAVE MATERIA</th>
                                    <th>SECCION</th>
                                    <th>MATERIA</th>
                                    <th>SELECCIONAR</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if (isset($materias)) : ?>
                                    <?php foreach ($materias as $valores => $datos) : ?>
                                        <tr>
                                            <td class='text-center'><?php echo  $datos->grupo_materia ?></td>
                                            <td><?php echo  $datos->seccion ?></td>
                                            <td><?php echo  $datos->materia_nombre ?></td>
                                            <td class='text-center'>
                                                <div class='form-check form-switch'>
                                                    <input class='form-check-input' type='checkbox' id='id_materia' name='id_materia[]' value=' <?php echo $datos->grupo_id ?>'>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-secondary" type="button" id="confirm-btn" onclick="confirmacion()">Eliminar Seleccionados </button>
                        <button class="btn btn-secondary" type="button" onclick="regresar()">Regresar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form action="<?php echo base_url('Escolar/BajadeMateria') ?>" id="buscar_seccion" method="post">
            <input type="hidden" name="matricula" value="<?php echo $alumno->id ?>">
        </form>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function confirmacion() {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! Los cambios que se realizaran tendrán efecto de inmediato y no se podrán cancelar ¿Está seguro que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',

            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('eliminar_materia').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }
        function regresar(){
            document.getElementById('buscar_seccion').submit();
            // console.log('hola');
        }
    </script>
<?php endif; ?>
<?php echo $this->endSection() ?>