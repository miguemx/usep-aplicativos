<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesCampos;
class AspirantesComentariosModel extends Model
{
    protected $table='aspirantes_comentarios';
    protected $primaryKey='comentario_id';
    protected $returnType='App\Entities\AspirantesComentarios';
    protected $useSoftDeletes = true;
    protected $allowedFields=['comentarios_campo'];
    protected $db;
    protected $builder;
    protected $useTimestamps = true;

    /**
     * @param comentario_campo id del campo de referencia 
     * 
     */
    public function getComentarios($comentario_campo)
    {
        return $this->where('comentarios_campo',$comentario_campo)->find();
    }

}

?>