<?php
namespace App\Controllers;

use App\Models\FiltroFoliosModel;
use App\Models\FiltroRespuestasModel;
use App\Models\FiltroAccesosModel;
use App\Models\FiltroSanitarioPreguntasModel;
use App\Models\FiltroAccesosReportesModel;
use App\Models\FiltroRespuestasPreguntasModel;

use App\Entities\FiltroAcceso;

class Filtro extends BaseController {

    private $filtroAccesosModel;

    public function __construct() {
        $this->filtroAccesosModel = new FiltroAccesosModel();
    }

    public function index() {
        return redirect()->to('/');
    }

    /**
     * muestra la interfaz para validación de codigo QR de filtro sanitario
     * @param folio el folio generado
     */
    public function acceso($folio) {
        $filtroFoliosModel = new FiltroFoliosModel();
        $filtroPreguntasModel = new FiltroSanitarioPreguntasModel();
        $filtroRespuestasModel = new FiltroRespuestasModel();
        $data['menu'] = $this->sesion->get('menu');
        $data['folio'] = null;
        $data['preguntas'] = [];

        $folioFiltro = $filtroFoliosModel->find( $folio );
        if ( !is_null($folioFiltro) ) {
            $exp = strtotime($folioFiltro->created_at) + ( 60*60*24*10 );
            $hoy = time();
            if ( $hoy < $exp ) {
                $data['preguntas'] = $filtroPreguntasModel->orderBySintomas();
                $data['respuestas'] = $filtroRespuestasModel->folioArray( $folioFiltro->id );
                $data['folio'] = $folioFiltro;
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        return view( 'filtro_sanitario/validacion', $data );
    }

    /**
     * guarda un acceso registrando oximetría y temperatura
     */
    public function accede() {
        $filtroAcceso = new FiltroAcceso( $this->request->getPost() );
        $data['error'] = null;
        try {
            $this->filtroAccesosModel->insert( $filtroAcceso );
        }
        catch ( \Exception $ex ) {
            log_message("error", "ERROR REGISTRO FILTRO {exception}", ["exception" => $ex]);
            $data['error'] = $ex->getMessage();
        }
        $data['menu'] = $this->sesion->get('menu');
        return view( 'filtro_sanitario/acceso', $data );
    }

    /**
     * muestra una pantalla para poder consultar los datos del filtro sanitario
     */
    public function consulta() {
        $pager = \Config\Services::pager();
        $reportesModel = new FiltroAccesosReportesModel();
        $registros = $reportesModel->buscar( $this->request->getGet() );
        $data['nombre'] = ( !is_null($this->request->getGet('nombre')) )? $this->request->getGet('nombre'): '';
        $data['inicio'] = ( !is_null($this->request->getGet('inicio')) )? $this->request->getGet('inicio'): '';
        $data['fin'] = ( !is_null($this->request->getGet('fin')) )? $this->request->getGet('fin'): '';
        $registros = $this->llenaRespuestas( $registros );
        $data['registros'] = $registros;
        $data['pager'] = $reportesModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        return view( 'filtro_sanitario/consulta', $data );
    }

    /**
     * agrega las respuestas proporcionadas por la persona que contesta el cuestionario del filtro
     * @param registros un arreglo de objetos con los registros de accesos
     * @return registros el mismo arreglo con las respuestas agregadas
     */
    private function llenaRespuestas($registros) {
        $regs = $registros;
        $registros = array();
        $pregsModel = new FiltroRespuestasPreguntasModel();
        foreach ( $regs as $reg ) {
            $respuestas = $pregsModel->findByFolio( $reg->folio );
            $reg->respuestas = $respuestas;
            $registros[] = $reg;
        }
        return $registros;
    }
    
}