<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div>
        <img src="<?php echo base_url('img/logobn.jpeg'); ?>" alt="USEP" width="150" />
    </div>
    <div>
        <h3 style="text-align: center;">
            Universidad de la Salud del Estado de Puebla<br />
            Dirección de Servicios Escolares y Titulación<br />
        </h3>
        <h4 style="text-align: center;">Kardex simple</h4>
    </div>
    
    <hr />
    <div class="row">
        <div class="col-sm-12">
            
        </div>
        <?php 
        $promGeneral = 0;
        $sumGeneral = 0;
        $numPeriodosGeneral = 0;
        ?>
        <?php foreach( $kardex as $periodo=>$materias ): ?>
            <?php $numPeriodosGeneral++; ?>
            <div class="col-sm-6">
                <h5><?php echo $periodo.' '.$materias[0]->periodoNombre; ?></h5>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>CLAVE</th>
                            <th>ASIGNATURA</th>
                            <th>CALIFICACIÓN</th>
                            <th>TIPO DE CALIFICACIÓN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sum = 0;
                        $prom = 0;
                        $mat = 0;
                        $aprobadas = 0;
                        $reprobadas = 0;
                        ?>
                        <?php foreach( $materias as $materia ): ?> 
                            <tr>
                                <td><?php echo $materia->materiaClave ?></td>
                                <td><?php echo $materia->materia ?></td>
                                <td>
                                    <?php 
                                    $calif = '';
                                    if( $materia->extraordinario ) {
                                        $calif = $materia->extraordinario;
                                    }
                                    else {
                                        $calif = $materia->ordinario;
                                    }
                                    if ( $calif < 6 ) $reprobadas++;
                                    else $aprobadas++;
                                    $sum+=$calif;
                                    ?>
                                    <?php echo $calif; ?>
                                </td>
                                <td>
                                    <?php
                                    if($materia->ordinario == '' && $materia->extraordinario == ''){
                                        echo '---';
                                    } 
                                    elseif($materia->extraordinario) {
                                        echo 'EXT';
                                    }
                                    else {
                                        echo 'ORD';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php  $mat++; ?>
                        <?php endforeach; ?>
                        <?php 
                            $prom = $sum / $mat;
                            $sumGeneral += $prom;
                        ?>
                        <tr>
                            <td colspan="4">
                                Núm. de asignaturas aprobadas: <?php echo $aprobadas; ?>&nbsp;&nbsp;&nbsp;
                                Núm. de asignaturas reprobadas: <?php echo $reprobadas; ?>&nbsp;&nbsp;&nbsp;
                                Promedio: <?php echo number_format($prom,2,'.',','); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php if($prom == 0){
                $numPeriodosGeneral = $numPeriodosGeneral-1;
            } ?>
        <?php endforeach; ?>
    </div>
    <?php 
    if ( $numPeriodosGeneral > 0 ) $promGeneral = $sumGeneral / $numPeriodosGeneral; 
    ?>
    <div style="text-align: right; margin-top: 35px;">
        PROMEDIO GENERAL: <?php echo number_format( $promGeneral, 2, '.', ',' ); ?><br />
        EL PRESENTE DOCUMENTO NO CUENTA CON VALIDEZ OFICIAL
    </div>
</div>



<?php echo $this->endSection() ?>
