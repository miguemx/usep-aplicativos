<?php echo $this->extend('plantillabase'); ?>

  <?php echo $this->section('content') ?>
  <script>
    function mostrar(dato){
        if(dato=="1"){
            document.getElementById("defsexo").style.display = "block";
            document.getElementById("txtsexo").style.display = "block";
        }
        if (dato==0) {
        	document.getElementById("defsexo").style.display = "none";
            document.getElementById("txtsexo").style.display = "none";
        }
    }
    </script>
<style>
		body{
			height: 100vh;
			background-color: #dcdcdc;
			padding: 40px 5px;
			font-family: 'Open Sans', sans-serif;
		}
		.fixed-bottom {
			background-color: #880c34;
		}	
		.escudo{
			background:url(/img/OIP.jpg) no-repeat center center;
		}
		footer{
			color: seashell;
			display: flex;
		}
		
		
		</style>
	  <section>
		  <div class="container py-5 h-100">
			  <div class="row d-flex justify-content-center align-items-center h-100">
				  <div class="col col-xl-10">
					  <div class="card" style="border-radius: 1rem;">
						<div class="row g-0">
							<div class="col-md-7 col-lg-5 d-none d-md-block escudo" style="background-color: #880c34; border-radius: 1rem 0 0 1rem;">
							</div>
							<div class="col-md-6 col-lg-7 d-flex align-items-center">
								<div class="card-body p-4 p-lg-5 text-black">
									<form  method="POST" id="contenedor1" action= "<?php echo base_url('FiltroSanitario/validaDatos'); ?>" > 
										<div id="div1">
											<CEnter>
												<h3>CUESTIONARIO DE FILTRO SANITARIO</h3>
												<BR></BR>
												<input type="hidden" name="varcorr" id="varcorr" value="<?php echo $varcorr;?>";>
												<h5>Datos generales</h5>
												<b>
													<br>
													<input style ="width : 40%" type="text" name="correo" id="correo" class="form-control" value="<?php echo $correo;?>"; placeholder ="Ingresa correo electronico">
												</b>
												<br>
												<b>
													<input class="form-control" style ="width : 40%" type="text" name="nombre" id="nombre" value="<?php echo $nombre;?>"; placeholder =" Nombre";>
												</b>
												<br>
												<input class="form-control" style ="width : 40%" type="text" name="dependencia"id="dependencia" value="<?php echo $dependencia;?>";  placeholder =" DEPENDENCIA" ;>
												<br>
												
												<input class="form-control" style ="width : 40%" type="text" name="carrera" placeholder =" Carrera" ></b>
												<br>
												<select style ="width : 40%" class="form-select" aria-label="Default select example" name="categoria" id="categoria">
													<option>Categoria</option>
													<option value="Estudiante">Estudiante</option>
													<option value="Estructura">Estructura</option>
													<option value="Servicios profesionales">Servicios profesionales</option>
													<option value="visitantes">Visitantes</option>
												</select>
											</b>
											<br>
											<select style ="width : 40%" class="form-select" aria-label="Default select example"  name="edad" id="edad">
												<option>Edad</option>
												<option>18 a 30</option>
												<option>31 a 40</option>
												<option>41 a 50</option>
												<option>61 y más</option>
											</select>
										</b>
										<br>
										<select style ="width : 40%" class="form-select" aria-label="Default select example"  name="sexo" onchange="mostrar(this.value);" id="sexo">
											<option value="Femenino">Sexo</option>
											<option value="Femenino">Femenino</option>
											<option value="Masculino" >Masculino</option>
											<option value="1">Otro</option>
										</select>
									</b>
									<b><label class="form group" id="defsexo" style="display: none;">Define sexo</label><input type="text" id="txtsexo" name="txtsexo" style="display: none;">
								</b>
							</div>
							<center>
								<br>
								
								<div class="col-12">
									<button type="submit" class="btn btn-secondary">Registra/valida</button>
								</div>
							</center>
						</CEnter>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>

<footer class="navbar footer fixed-bottom container-fluid text-center" >
	
	<center>
		
		Si se presenta fiebre asociada con el resto de los sintomas,
		acuda a la unidad de salud más cercana a su domicilio y siga las indicaciones del personal médico.
	</center>
	
	
	
</footer>

<?php echo $this->endSection() ?>






