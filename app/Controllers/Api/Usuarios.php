<?php
namespace App\Controllers\Api;

use App\Models\UsuariosGeneralModel;

class Usuarios extends BaseApi {

    private $usuariosGeneralModel;

    public function __construct() {
        $this->usuariosGeneralModel = new UsuariosGeneralModel();
    }

    /**
     * devuelve un json con todos los usuarios registrados en el sistema
     */
    public function index() {
        $usuarios = $this->usuariosGeneralModel->findAll();
        $data = [
            'status' => 'ok',
            'message' => 'Usuarios listados correctamente.',
            'count' => count($usuarios),
            'results' => $usuarios
        ];
        $this->response->setHeader('xauth', 'bearer:JWT');
        return $this->respond($data,200);
    }

    /**
     * devuelve un usuario
     */
    public function ver($id) {
        $usuario = $this->usuariosGeneralModel->find( $id );
        if ( !is_null($usuario) ) {
            $this->response->setHeader('xauth', 'bearer:JWT');
            return $this->respond($usuario,200);
        }
        else {
            $data = [
                'status' => 'error',
                'message' => 'Usuario no encontrado'
            ];
            return $this->respond($data,404);
        }
    }

    /**
     * busca registros de usuarios de acuerdo al querystring enviado por GET
     */
    public function buscar() {
        $filtros = $this->request->getGet();
        $results = $this->usuariosGeneralModel->buscar( $filtros );
        $page = $this->request->getGet('page');
        $page = ( is_null($page) )? 1: (int)$page;
        $totalPaginas = $this->usuariosGeneralModel->pager->getPageCount();
        $page = ( $page>$totalPaginas )? $totalPaginas: $page;
        $data = [
            'status' => 'ok',
            'message' => 'Usuarios listados correctamente.',
            'count' => count($results),
            'pages' => $totalPaginas,
            'page' => $page,
            'results' => $results
        ];
        $this->response->setHeader('xauth', 'bearer:JWT');
        return $this->respond($data,200);
    }

}