<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<div class="container-fluid">
    <form class="row g-3" method="post">

        <!-- <div class="col-12">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div> -->

    </form>
</div>
<div class="container-fluid" style="margin-top: 15px;">

    <?php if (isset($error)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <?php if (($mensaje)) : ?>
        <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
            <?php echo $mensaje['texto']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <form method="get" action="<?php echo base_url('Escolar/Estudiantes') ?>">

        <div class="row" style="padding-top: 10px;">
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-2 col-form-label">Matrícula</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="id" value="<?php echo $id; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-2 col-form-label">Carrera</label>
                <div class="col-sm-10">
                    <select class="form-select form-select-sm" name="carrera">
                        <option value="">Selecciona una carrera</option>
                        <option value="1" <?php if ($carrera == '1') echo 'selected="selected"'; ?>>LIC. EN ENFERMERÍA Y OBSTETRICIA </option>
                        <option value="2" <?php if ($carrera == '2') echo 'selected="selected"'; ?>>LIC. EN MÉDICO CIRUJANO</option>
                    </select>
                </div>
            </div>
            <div class="mb-3 col-sm-4" style="text-align: right;">
                <a href="<?php echo base_url('Escolar/Estudiantes') ?>" class="btn btn-secondary btn-sm">Ver todos</a>
                <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
            </div>

        </div>
        <div class="row">
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Apellido Paterno</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="apPaterno" value="<?php echo $apPaterno; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Apellido Materno</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="apMaterno" value="<?php echo $apMaterno; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Nombre</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="nombre" value="<?php echo $nombre; ?>" />
                </div>
            </div>
        </div>

    </form>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Matrícula</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Nombre</th>
                    <th>Carrera</th>
                    <th>Estatus</th>
                    <th>Kardex</th>
                    <th>Portal del Estudiante</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($alumnos as $alumno) : ?>
                    <tr>
                        <td><?php echo $alumno->id ?></td>
                        <td><?php echo $alumno->apPaterno ?></td>
                        <td><?php echo $alumno->apMaterno ?></td>
                        <td><?php echo $alumno->nombres ?></td>
                        <td>
                            <?php
                            if ($alumno->carrera == '1') echo 'LIC. EN ENFERMERÍA Y OBSTETRICIA';
                            else if ($alumno->carrera == '2') echo 'LIC. EN MEDICO CIRUJANO';
                            ?>
                        </td>
                        <td>
                            <div class="d-flex justify-content-center align-items-center">
                                <?php echo $alumno->status ?>
                                <button data-bs-toggle="modal" data-bs-target="#cambiarstatus" type="button" class="btn" name="bandera" value="<?php echo $alumno->id ?>" onclick="cambiarstatus(this)"><i class="bi bi-pencil-square" style="font-size: 1rem; color: grey;"></i></button>

                            </div>

                        </td>
                        <td>
                            <form action="<?php echo base_url('Calificaciones/Kardex'); ?>" method="post">
                                <input name="matricula" type="hidden" value="<?php echo $alumno->id; ?>" />
                                <button type="submit" name="kx" value="1" class="btn btn-secondary btn-sm">
                                    Consultar
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="<?php echo base_url('Escolar/EstudiantePerfil'); ?>" method="post" onsubmit="return confirma()">
                                <input name="matricula" type="hidden" value="<?php echo $alumno->id; ?>" />
                                <button type="submit" name="bd" value="entra" class="btn btn-secondary btn-sm">
                                    Acceder
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div>
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="cambiarstatus" tabindex="-1" role="dialog" aria-labelledby="cambio de estatus del aspirante" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cambiar estatus</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('Escolar/cambiarStatus/Estudiantes') ?>" method="post" id="cambio_status">
                    <input type="hidden" name="alumno_id" id="alumno_id">
                    <div class="mb-3">
                        <label for="select_status" class="form-label">Estatus</label>
                        <select class="form-select" name="select_status" id="select_status">
                            <option selected>Selecciona un estatus disponible</option>
                            <option value="SUSPENDIDO">SUSPENDIDO</option>
                            <option value="BAJA TEMPORAL">BAJA TEMPORAL</option>
                            <option value="BAJA VOLUNTARIA">BAJA VOLUNTARIA</option>
                            <option value="BAJA POR REGLAMENTO">BAJA POR REGLAMENTO</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="motivo_cambio" class="form-label">Motivo</label>
                        <textarea class="form-control" name="motivo_cambio" id="motivo_cambio" rows="5"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="enviar()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    function confirma() {
        let mensaje = 'Está a punto de acceder al portal de este estudiante. \n\n Eso significa que se cerrará su sesión para acceder con la del estudiante seleccionado.';
        return confirm(mensaje);
    }

    function cambiarstatus(valor) {
        // console.log(valor);
        var input = document.getElementById('alumno_id');
        input.value = valor.value;
        var textarea = document.getElementById('motivo_cambio');
        textarea.innerHTML = '';
    }

    function enviar() {
        var textarea = document.getElementById('motivo_cambio');
        console.log(textarea.value);
        if (textarea.value.length != 0) {
            Swal.fire({
                title: 'Confirmar cambio de status',
                text: "El cambio de estatus inhabilitara al alumno para el acceso a la plataforma, ¿Estas seguro que deseas continuar?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Guardar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Guardado!',
                        'Se aplicado el cambio de estatus del alumno seleccionado.',
                        'success'
                    );
                    document.getElementById('cambio_status').submit();
                }

            })
        } else {
            Swal.fire({
                title: 'Cancelado',
                text: 'Se requiere un comentario como justificacion del estatus del alumno.',
                icon: 'error',
                confirmButtonColor: '#3085d6'
            })
        }
    }
</script>

<?php echo $this->endSection() ?>