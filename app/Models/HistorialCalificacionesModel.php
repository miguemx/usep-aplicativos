<?php

namespace App\Models;

use CodeIgniter\Model;

class HistorialCalificacionesModel extends Model
{
    protected $table      = 'view_historial_calificacion';
    protected $primaryKey = 'IDC';

    protected $returnType    = 'App\Entities\HistorialCalificaciones';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'IDC', 'Periodo', 'Ordinario', 'Extraordinaria', 'Seccion', 'Materia', 'ID_Docente',
        'Nombre_docente', 'Martricula', 'Nombre_alumno', 'Correo_Alumno', 'Carrera', 'Aprobada','Materia_clave'

    ];


    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;
    


    public function buscarReprobados($matricula)    
    {
        return 
        $this
        ->where('Martricula',$matricula)
        ->where('Periodo !=','2022A')
        ->find();
    }
    public function buscarRecruso($matricula,$calve_materia)    
    {
        return 
        $this
        ->where('Martricula',$matricula)
        ->where('Materia_clave',$calve_materia)
        ->where('Periodo !=','2022A')
        ->find();
    }

}

