<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>

<div class="container ">
    <?php if (isset($mensajeok)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $mensajeok; ?>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
        <div class="row ">
            <h1 class="text-center my-2">Captura de asistencias por docente</h1>
            <div class="accordion accordion-flush mt-2" id="menu_estadisticcas">
                <form action="<?php echo base_url('docente/horarioGrupos'); ?>" method="post">
                    <div class="row">
                        <br>
                        <br>
                        <div class="col-sm-12">
                            <span class="input-group-text" id="inputGroup-sizing-default">DOCENTE</span>
                            <select id="docente" class="custom-select"  name="docente" onchange="this.form.submit();">
                                <option value = ''>Selecciona un docente</option>
                                <?php foreach ($docenteinfo as $docentes) : ?>
                                    <option value = <?php echo $docentes->empleado_id ?> ><?php
                                        echo $docentes->empleado_nombre.' '.$docentes->empleado_ap_paterno.' '.$docentes->empleado_ap_materno
                                    ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>