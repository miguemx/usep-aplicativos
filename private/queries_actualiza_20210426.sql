-- ----------------------------------------------------
-- Consulta para ingresar los REGID de elsevier
-- ----------------------------------------------------

ALTER TABLE `alumnos_generales` ADD `alumno_else_regid` VARCHAR(20) NULL AFTER `alumno_vigente`, ADD `alumno_else_regpass` VARCHAR(20) NULL AFTER `alumno_else_regid`;
ALTER TABLE `aplicativo_empleados` ADD `empleado_else_regid` VARCHAR(20) NULL AFTER `empleado_apellido`, ADD `empleado_else_regidpass` VARCHAR(20) NULL AFTER `empleado_else_regid`;

-- -----------------------------    AUN NO EJECUTADAS EN PRODUCCION -------------
-- Actualizar el tipo de campo para hacer relacion con la tabla de carreras
-- ------------------------------------------------------------------------------
UPDATE `alumnos_generales` SET alumno_carrera=1 WHERE `alumno_carrera` LIKE 'Lic. en Enfermer�a y Obstetricia';
UPDATE `alumnos_generales` SET alumno_carrera='2' WHERE `alumno_carrera` LIKE 'Lic. en M�dico Cirujano';

ALTER TABLE `alumnos_generales` MODIFY alumno_carrera INT UNSIGNED NOT NULL;

