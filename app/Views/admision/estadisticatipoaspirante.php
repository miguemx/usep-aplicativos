<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<div class="container my-sm-5">
    <h1 style="display:flex; justify-content:center">Gestion de procesos</h1>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/estadisticatipoaspirante'); ?>">
        <div>
            <br>
            <h2>Filtros</h2>
            <div class="row" style="padding-top: 10px;">
              <input type="hidden" name="ban" type="text" value="<?php echo $ingreso?>"/>
                <div class="mb-1 row col-sm-2" >
                    <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                        <label for="correocandidato" class="form-label">Medicina</label>
                        <input type='radio' id="medicina" onclick="habilita()" name="carrera" value="2" request/>
                    </div>
                    <?php if (isset($ingreso)) : ?>
                      <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                          <label for="correocandidato" class="form-label">Enfermería</label>
                          <input type='radio' id="enfermeria" onclick="habilita()" name="carrera" value="1" request/>
                      </div>
                    <?php endif; ?>
                </div>
                <h2>Selecciona el tipo de información que requiere consultar</h2>
                <div class="row" style="padding-top: 10px;">
                    <div class="mb-1 row col-sm-2" >
                        <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                            <label for="correocandidato" class="form-label">hombres</label>
                            <input type='radio' disabled id="sexoh" name="sexo" value="h" onchange="this.form.submit();"/>
                        </div>
                        <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                            <label for="correocandidato" class="form-label">mujeres</label>
                            <input type='radio' disabled id="sexom" name="sexo" value="m" onchange="this.form.submit();"/>
                        </div>
                        <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                            <label for="correocandidato" class="form-label">Todos</label>
                            <input type='radio' disabled id="todos" name="todos" value="0" onchange="this.form.submit();"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
    <h1 style="display:flex; justify-content:center">Número total de registros</h1>
    <input style="text-align: center;"  class="form-control" name="ganadorproceso" type="text" value="<?php echo $numero; ?>" disabled/>
    <br>
<figure class="highcharts-figure">
  <div id="container"></div>

  <table id="datatable">
    <thead>
      <tr>
        <th></th>
        <th>Hombres</th>
        <th>Mujeres</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <th>Candidad</th>
        <td><?php echo $hombre?></td>
        <td><?php echo $mujer?></td>
      </tr>
    </tbody>
  </table>
</figure>
<script>
    Highcharts.chart('container', {
    data: {
      table: 'datatable'
    },
    chart: {
      type: 'column'
    },
    title: {
      text: 'Grafica en tiempo real'
    },
    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Cantidad'
      }
    },
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          this.point.y + ' ' + this.point.name.toLowerCase();
      }
    }
  });
</script>
<script>
  function habilita(){
    document.getElementById("sexoh").disabled = false;
    document.getElementById("sexom").disabled = false;
    document.getElementById("todos").disabled = false;
    
    let checkid = document.getElementById(id);
    if ( checkid.checked === true ) {
		  document.getElementByclass("tabla").style.display = "block";
    }
  }
</script>
<?php echo $this->endSection() ?>