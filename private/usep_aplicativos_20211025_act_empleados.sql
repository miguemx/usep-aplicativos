ALTER TABLE `aplicativo_empleados` 
ADD `empleado_categoria` VARCHAR(60) NULL AFTER `empleado_ap_materno`, 
ADD `empleado_edad` VARCHAR(10) NULL AFTER `empleado_categoria`, 
ADD `empleado_sexo` VARCHAR(20) NULL AFTER `empleado_edad`;