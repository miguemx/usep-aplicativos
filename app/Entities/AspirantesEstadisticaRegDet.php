<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesEstadisticaRegDet extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'region'      => null,
        'mujeres_enf' => null,
        'hombres_enf' => null,
        'mujeres_med' => null,
        'hombres_med' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
       
        'region'      => 'region_nombre',
        'mujeres_enf' => 'n_alumnos_m_enf',
        'hombres_enf' => 'n_alumnos_h_enf',
        'mujeres_med' => 'n_alumnos_m_med',
        'hombres_med' => 'n_alumnos_h_med',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}