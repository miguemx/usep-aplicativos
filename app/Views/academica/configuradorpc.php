<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .btn_usep {
        background-color: #840f31;
        color: whitesmoke;
    }

    .btn_usep:hover {
        color: whitesmoke;
        box-shadow: 5px 2px #888888;
    }
</style>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="row">
            <div>
                <div class="alert fade show alert-info" role="alert">
                    <div id="text_periodo"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="select_periodo" class="form-label">Periodo</label>
                    <select class="form-select" name="select_periodo" id="select_periodo" onchange="getDescripccion(this)">
                        <option value="0">Seleccione un periodo</option>
                        <?php foreach ($periodos as $lista_periodo) : ?>
                            <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                        <?php endforeach; ?>
                    </select>
                    <small></small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-center">
                <button id="btn_buscar" type="button" class="btn btn_usep" onclick="buscar_practicas()" disabled>Buscar</button>
            </div>
        </div>
    </div>
</div>
<div class="container" id="main_content" hidden>
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <form action="<?php echo base_url('ProgramacionAcademica/guardarhorario') ?>" method="post" id="form_table">
            <table name='calif_arr' class="text-center text-white" style="width: 100%; border-top: 2px solid white;">
                <thead class="thead-dark" style="background-color: #840f31; ">
                    <tr>
                        <th>IDC</th>
                        <th>PERIODO</th>
                        <th>SECCIÓN</th>
                        <th>MATERIA</th>
                        <th>HORARIO</th>
                        <th>TURNO <br> MATUTINO/VESPERTINO</th>
                    </tr>
                </thead>
                <tbody id="table_detalles" style="color:black">

                </tbody>
            </table>
        </form>
    </div>
</div>
<div class="container" id="sin_datos" hidden>

    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg">
        sin datos disponibles
    </div>
</div>
<input type="text" id="bandera_periodo">
<script>
    let bandera = document.getElementById('bandera_periodo');

    function getDescripccion(periodo) {
        let periodos = <?php echo json_encode($periodos) ?>;
        periodos.forEach(element => {
            if (element.id == periodo.value) {
                let texto = document.getElementById('text_periodo');
                switch (element.estado) {
                    case 'INICIO':
                        bandera.value = true;
                        texto.innerHTML = 'El periodo seleccionado esta habilitado para realizar cambios de horarios y grupos';
                        break;
                    case 'INSCRIPCION':
                        bandera.value = false;
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para la modificacion de horarios y cambios de docentes';
                        break;
                    case 'CURSO':
                        bandera.value = false;
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'CALIFICACIONES':
                        bandera.value = false;
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                    case 'FINALIZADO':
                        bandera.value = false;
                        texto.innerHTML = 'El periodo seleccionado solo esta habilitado para consulta de datos';
                        break;
                }

            }
        });
        buscar();
    }

    function buscar() {
        if (document.getElementById('select_periodo').value != 0) {
            document.getElementById('btn_buscar').disabled = false;
        } else {
            document.getElementById('btn_buscar').disabled = true;
        }
    }
    /*  var getSecciones = async () => {
         let periodo = document.getElementById('select_periodo').value;
         let carrera = document.getElementById('select_carrera').value;
         console.log(periodo, carrera);
         if (carrera != 0 && periodo != 0) {
             let response = await fetch('<?php // echo base_url("ProgramacionAcademica/getSeccionesDisponibles") 
                                            ?>' + '/' + periodo + '/' + carrera, {
                 method: 'POST',
             }).then(response => response.json()).then(result => {
                 return result;
             }).catch(error => {
                 return null;
             });
             console.log(response);
             generar_secciones(response);
         }
     }

     function generar_secciones(datos) {
         let selector = document.getElementById('select_seccion');
         selector.disabled = false;
         selector.innerHTML = 'Selecciona una seccion disponible';
         let option = document.createElement('option');
         option.value = 0;
         option.text = 'Selecciona una seccion disponible';
         selector.appendChild(option);
         datos.forEach(element => {
             let option = document.createElement('option');
             option.value = element.clave;
             option.text = element.clave;
             selector.appendChild(option);
         });
     }

     

     */
    /* function buscar_practicas() {
        let periodo = document.getElementById('select_periodo').value;
        if (periodo != 0) {
            document.getElementById('main_content').hidden = false;
            getPC(periodo);
        } else {
            document.getElementById('main_content').hidden = true;
        }
    } */

    var buscar_practicas = async () => {
        let periodo = document.getElementById('select_periodo').value;
        console.log(periodo);
        if (periodo != 0) {
            let response = await fetch('<?php echo base_url("ProgramacionAcademica/getPracticasClinicas") ?>' + '/' + periodo, {
                method: 'POST',
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                return null;
            });
            if (response) {
                console.log(response);
                console.log(bandera);
                document.getElementById('main_content').hidden = false;
                let table = document.getElementById('table_detalles');
                table.innerHTML = '';
                response.forEach(element => {
                    let tr = document.createElement('tr');
                    table.appendChild(tr);

                    var td = document.createElement('td');
                    td.innerHTML = element.grupo_idc;
                    if (!element.inicio && !element.fin){
                        var input = document.createElement('input');
                        input.name = "grupos[]";
                        input.value = element.grupo_idc;
                        input.hidden = true;
                        td.appendChild(input);
                    }
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    td.innerHTML = element.periodo;
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    td.innerHTML = element.clave;
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    td.innerHTML = element.materiaNombre;
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    td.id = element.grupo_idc;
                    if (element.inicio && element.fin) {
                        td.innerHTML = element.inicio + "-" + element.fin;
                    } else if (bandera.value == 'true') {
                        td.innerHTML = '08:00 - 13:00';
                    } else {
                        td.innerHTML = "Sin definir";
                    }
                    tr.appendChild(td);

                    var td = document.createElement('td');
                    var div = document.createElement('div');
                    // console.log (bandera.value);
                    if (bandera.value == 'true') {
                        if (!element.inicio && !element.fin){
                            var checkbox = document.createElement('input');
                            checkbox.disabled = false;
                            checkbox.name = 'turno[]';
                            div.classList.add('form-check', 'form-switch');
                            checkbox.classList.add('form-check-input');
                            checkbox.type = 'checkbox';
                            checkbox.checked = false;
                            checkbox.value = element.grupo_idc;
                            checkbox.setAttribute('onclick', 'change_horario(this)')
                            div.appendChild(checkbox);
                            td.appendChild(div);
                        }
                    }
                    tr.appendChild(td);
                });
                if (bandera.value == 'true') {
                    let tr = document.createElement('tr');
                    table.appendChild(tr);

                    var td = document.createElement('td');
                    td.classList.add('p-2');
                    td.colSpan = 6;
                    var boton = document.createElement('button');
                    boton.classList.add('btn', 'btn_usep');
                    boton.innerHTML = 'Guardar';
                    boton.setAttribute('onclick', 'guardar_datos()');
                    td.appendChild(boton);
                    tr.appendChild(td);
                }
            } else {
                document.getElementById('main_content').hidden = true;
            }
        }

    }

    function change_horario(grupo) {
        console.log(grupo);
        var horario = document.getElementById(grupo.value);
        console.log(horario);
        console.log(grupo.checked);
        if (grupo.checked == false) {
            horario.innerHTML = '08:00 - 13:00';
        } else {
            horario.innerHTML = '13:00 - 18:00';
        }
    }

    function guardar_datos() {
        document.getElementById('form_table').submit();
    }
    let periodo_selected = document.getElementById('select_periodo');
    getDescripccion(periodo_selected);
</script>
<?php echo $this->endSection() ?>