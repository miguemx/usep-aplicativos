<?php 
namespace App\Models;
use CodeIgniter\Model;

class FiltroSanitarioModel extends Model{
    protected $table= 'filtro_usuarios_ext';
    protected $primaryKey= 'externo_correo';

    protected $returnType= 'App\Entities\FiltroUsuariosExternos';
    protected $useSoftDeletes = true;
    protected $allowedFields= [
        'externo_correo','externo_nombre','externo_dependencia','externo_edad','externo_sexo','externo_categoria'];

    public function findByEmail($correo) {
        $empleado = $this->where('externo_correo',$correo)->first();
        //print_r ($empleado);
        return $empleado;
    }
}
?>