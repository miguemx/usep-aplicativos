<?php
namespace App\Models;
use CodeIgniter\Model;

class CredencialModel extends Model {
    protected $table      = 'alumnos_credenciales';
    protected $primaryKey = 'credencial_alumno';

    protected $returnType    = 'App\Entities\Credencial';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'credencial_alumno','credencial_foto','credencial_identificacion','comentario_foto','comentario_identificacion', 'credencial_valida', 'credencial_revisada',
        'credencial_vigencia'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }
    public function updateVigencia($alumno,$arr_update){
        return $this
        ->db
        ->table('alumnos_credenciales')
        ->where(["alumno_periodo" => '2021A', '	alumno_id ' => $alumno])
        ->set($arr_update)
        ->update();

    }
}