ALTER TABLE `aca_aulas` ADD `aula_nombre_corto` VARCHAR(30) NOT NULL AFTER `aula_nombre`;
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-202' WHERE `aca_aulas`.`aula_id` = '202';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-203' WHERE `aca_aulas`.`aula_id` = '203';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-204' WHERE `aca_aulas`.`aula_id` = '204';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'ABP' WHERE `aca_aulas`.`aula_id` = '205';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'ABP-B' WHERE `aca_aulas`.`aula_id` = '205-B';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AUDITORIO' WHERE `aca_aulas`.`aula_id` = 'AUD';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'VESTIDOR' WHERE `aca_aulas`.`aula_id` = 'VES';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B101' WHERE `aca_aulas`.`aula_id` = 'B101';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B102' WHERE `aca_aulas`.`aula_id` = 'B102';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B103' WHERE `aca_aulas`.`aula_id` = 'B103';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B104' WHERE `aca_aulas`.`aula_id` = 'B104';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B201' WHERE `aca_aulas`.`aula_id` = 'B201';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B202' WHERE `aca_aulas`.`aula_id` = 'B202';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B203' WHERE `aca_aulas`.`aula_id` = 'B203';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B204' WHERE `aca_aulas`.`aula_id` = 'B204';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B205' WHERE `aca_aulas`.`aula_id` = 'B205';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'AULA-B206' WHERE `aca_aulas`.`aula_id` = 'B206';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'S MAESTROS' WHERE `aca_aulas`.`aula_id` = 'SM';
UPDATE `aca_aulas` SET `aula_nombre_corto` = 'PREO' WHERE `aca_aulas`.`aula_id` = 'PREO';

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES 
(NULL, '22', 'programacionacademica', 'horariosecciones'),
(NULL, '23', 'programacionacademica', 'horariosecciones'),
(NULL, '22', 'programacionacademica', 'gruposecciones'), 
(NULL, '23', 'programacionacademica', 'gruposecciones'),
(NULL, '22', 'programacionacademica', 'getseccionesdisponibles'),
(NULL, '23', 'programacionacademica', 'getseccionesdisponibles'),
(NULL, '22', 'programacionacademica', 'menuDocentes'),
(NULL, '23', 'programacionacademica', 'menudocentes'),
(NULL, '22', 'programacionacademica', 'getseccionesperiodo'),
(NULL, '23', 'programacionacademica', 'getseccionesperiodo');


INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES 
(NULL, '19', 'programacionacademica', 'horariosecciones'),
(NULL, '19', 'programacionacademica', 'gruposecciones'), 
(NULL, '19', 'programacionacademica', 'getseccionesdisponibles'),
(NULL, '19', 'programacionacademica', 'menuDocentes'),
(NULL, '19', 'programacionacademica', 'getseccionesperiodo'),
(NULL, '19', 'programacionacademica', 'horariodocente'),
(NULL, '19', 'programacionacademica', 'espaciosdisponibles');