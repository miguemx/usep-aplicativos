<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <h3 class="mt-4">Aforo actual</h3>
    <?php if( isset($message) ): ?>
        <div class="alert alert-info">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>

    <?php if( isset($error) ): ?>
        <div class="alert alert-danger">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <div class="card">
        <div class="card-body row">
            <div class="col-sm-4 text-center">
                Aforo actual es de <h1><?php echo count($registros); ?></h1> 
            </div>
            <div class="col-sm-4 text-center">
                Lugares disponibles:
                <?php
                $disponibles = 59 - count($registros);
                if ( $disponibles > 20 ) echo '<h1 style="color: #1da308; font-weight: bold;">'.$disponibles.'</h1>';
                else if ( $disponibles > 10 ) echo '<h1 style="color: #abb025; font-weight: bold;">'.$disponibles.'</h1>';
                else echo '<h1 style="color: #ed0707; font-weight: bold;">'.$disponibles.'</h1>';
                ?>
            </div>
            <div class="col-sm-4 text-center">
                <div><a href="<?php echo base_url('Biblioteca/Entrada'); ?>" class="btn btn-lg btn-success">Añadir entrada</a></div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Hora de entrada</th>    
                    <th>ID o matrícula</th>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Carrera</th>
                    <th>Sexo</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach( $registros as $registro ): ?>
                    <tr>
                        <td class="text-center">
                            <?php 
                            // Este es un parche feo para poner las fotos
                            if( !is_null($registro->foto) ) {
                                try {
                                    $path = WRITEPATH.'estudiantes/'.$registro->alumno.'/'.$registro->foto;
                                    $file = fopen( $path, 'r' );
                                    $data = fread( $file, filesize($path) );
                                    $foto = base64_encode( $data );
                                    // Format the image SRC:  data:{mime};base64,{data};
                                    $src = 'data: ' . mime_content_type($path) . ';base64,' . $foto;
                                    echo "<img src=\"$src\" style=\"max-height: 100px;\" />";
                                }
                                catch (\Exception $ex) {
                                    echo '<img src="'.base_url('img/icons/user_gray_big.png').'" style="max-height: 100px;" />';
                                    log_message( 'notice', "Aforo en filtro: {ex}", [ 'ex' => $ex->getMessage() ] );
                                }
                            }
                            else {
                                echo '<img src="'.base_url('img/icons/user_gray_big.png').'" style="max-height: 100px;" />';
                            }
                            ?>
                        </td>
                        <td><?php echo $registro->entrada; ?></td>
                        <td><?php echo ( ( is_null($registro->alumno) )? $registro->empleado: $registro->alumno  ); ?></td>
                        <td><?php echo $registro->tipo; ?></td>
                        <td>
                            <?php 
                            if( is_null($registro->alumno) ){
                                echo $registro->empleadoNombre.' '.$registro->empleadoApPaterno.' '.$registro->empleadoApMaterno;
                            }
                            else {
                                echo $registro->alumnoNombre.' '.$registro->alumnoApPaterno.' '.$registro->alumnoApMaterno;
                            }
                            ?>
                        </td>
                        <td><?php echo $registro->carrera; ?></td>
                        <td><?php echo $registro->sexo; ?></td>
                        <td>
                            <form method="post" action="<?php echo base_url('Biblioteca/MarcarSalida'); ?>" onsubmit="return salida()">
                                <input type="hidden" name="folio" value="<?php echo $registro->id; ?>" />
                                <input type="hidden" name="ref" value="aforo" />
                                <button type="submit" class="btn btn-sm btn-light">
                                    Registrar salida
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    function cierra() {
        return confirm("Al hacer esta acción, todos los accesos registrados como hora de entrada, se marcarán como salida con la fecha y hora actual.\n\n¿Confirma que desea continuar?");
    }

    function salida() {
        return confirm("Al hacer esta acción, se marcará este registro como salida con la fecha y hora actual y desaparecerá del listado.\n\n¿Confirma que desea continuar?");
    }
</script>

<?php echo $this->endSection() ?>