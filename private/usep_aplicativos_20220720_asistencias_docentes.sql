DROP TABLE IF EXISTS `aca_asistencias_diarias`;
CREATE TABLE `aca_asistencias_diarias` (
  `asistencia_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asistencia_grupo` int(10) unsigned NOT NULL,
  `asistencia_matricula` varchar(9) DEFAULT NULL,
  `asistencia_docente` int(10) DEFAULT NULL,
  `asistencia_fecha` datetime DEFAULT NULL,
  `asistencia_aula` varchar(20) DEFAULT NULL,
  `asistencia_tipo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`asistencia_id`),
  KEY `asistencia_grupo` (`asistencia_grupo`),
  KEY `fk_alumnos` (`asistencia_matricula`),
  CONSTRAINT `aca_asistencias_diarias_ibfk_1` FOREIGN KEY (`asistencia_grupo`) REFERENCES `aca_grupos` (`grupo_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_alumnos` FOREIGN KEY (`asistencia_matricula`) REFERENCES `alumnos_generales` (`alumno_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

CREATE VIEW view_asistencias_alumnos AS(
SELECT
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_grupo` AS `asistencia_grupo`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_matricula` AS `asistencia_matricula`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_docente` AS `asistencia_docente`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_fecha` AS `asistencia_fecha`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_aula` AS `asistencia_aula`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_tipo` AS `asistencia_tipo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_id` AS `alumno_id`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres` AS `alumno_nombres`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno` AS `alumno_ap_paterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno` AS `alumno_ap_materno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_status` AS `alumno_status`
FROM
    (
        `usep_aplicativos`.`aca_asistencias_diarias`
    JOIN `usep_aplicativos`.`alumnos_generales` ON
        (
            `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_matricula` = `usep_aplicativos`.`alumnos_generales`.`alumno_id`
        )
    ))