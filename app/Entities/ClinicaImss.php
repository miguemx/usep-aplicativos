<?php
namespace App\Entities;

use CodeIgniter\Entity;

class ClinicaImss extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'nombre' => null,
        'entidad' => null,
        'municipio' => null,
        'localidad' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'imss_id',
        'nombre' => 'imss_nombre',
        'entidad' => 'imss_entidad',
        'municipio' => 'imss_municipio',
        'localidad' => 'imss_localidad',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
