<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AlumnoCalificacion extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'nombre' => null,
        'curp' => null,
        'sexo' => null,
        'correo' => null,
        'carrera' => null,
        'vigente' => null,
        'elseRegid' => null,
        'elseRegidpass' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'nombres' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,

        'alumno' => null,
        'grupo' => null,
        'ordinario' => null,
        'extraordinario' => null,
        'aprobada' => null,

        'idc' => null,
        'clave' => null,
        'folio' => null,
        'materia' => null,
        'docente' => null,
        'periodo' => null,
        
      
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'alumno_id',
        'nombre' => 'alumno_nombre',
        'curp' => 'alumno_curp',
        'sexo' => 'alumno_sexo',
        'correo' => 'alumno_correo',
        'carrera' => 'alumno_carrera',
        'vigente' => 'alumno_vigente',
        'elseRegid' => 'alumno_else_regid',
        'elseRegidpass' => 'alumno_else_regpass',
        'apPaterno' => 'alumno_ap_paterno',
        'apMaterno' => 'alumno_ap_materno',
        'nombres' => 'alumno_nombres',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',

        'alumno' => 'gpoalumno_alumno',
        'grupo' => 'gpoalumno_grupo',
        'ordinario' => 'gpoalumno_ordinario',
        'extraordinario' => 'gpoalumno_extraordinario',
        'aprobada' => 'aprobada',

        'idc' => 'grupo_id',
        'clave' => 'grupo_clave',
        'folio' => 'grupo_folio',
        'materia' => 'grupo_materia',
        'docente' => 'grupo_docente',
        'periodo' => 'grupo_periodo',
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
