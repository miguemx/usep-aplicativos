<?php

namespace App\Entities;

use CodeIgniter\Entity;

class GrupoMateria extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'grupo' => null,
        'clave' => null,
        'folio' => null,
        'materia' => null,
        'docente' => null,
        'periodo' => null,
        'max' => null,
        'ocupado' => null,
        'periodoNombre' => null,
        'periodoInicio' => null,
        'periodoFin' => null,
        'carrera' => null,
        'materiaNombre' => null,
        'materiaNombreCorto' => null,
        'creditos' => null,
        'obligatoria' => null,
        'tipo' => null,
        'semestre' => null,
        'horas' => null,
        'horas_conducidas' => NULL,
        'empleado' => NULL,
        'correo' => NULL,
        'nombre_emp' => NULL,
        'apellido_emp' => NULL,
        'apPaterno' => NULL,
        'apMaterno' => NULL,
        'sexo_emp' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'grupo' => 'grupo_id',
        'clave' => 'grupo_clave',
        'folio' => 'grupo_folio',
        'materia' => 'grupo_materia',
        'docente' => 'grupo_docente',
        'periodo' => 'grupo_periodo',
        'max' => 'grupo_max',
        'ocupado' => 'grupo_ocupado',
        'periodoNombre' => 'periodo_nombre',
        'periodoInicio' => 'periodo_inicio',
        'periodoFin' => 'periodo_fin',
        'carrera' => 'materia_carrera',
        'materiaNombre' => 'materia_nombre',
        'materiaNombreCorto' => 'materia_nombre_corto',
        'creditos' => 'materia_creditos',
        'obligatoria' => 'materia_obligatoria',
        'tipo' => 'materia_obligatoria',
        'semestre' => 'materia_semestre',
        'horas' => 'materia_horas_semanales',
        'horas_conducidas' => 'materia_horas_conducidas',
        'empleado' => 'empleado_id',
        'correo' => 'empleado_correo',
        'nombre_emp' => 'empleado_nombre',
        'apellido_emp' => 'empleado_apellido',
        'apPaterno' => 'empleado_ap_paterno',
        'apMaterno' => 'empleado_ap_materno',
        'sexo_emp' => 'empleado_sexo',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
