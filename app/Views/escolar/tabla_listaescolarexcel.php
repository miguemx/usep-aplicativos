<?php

$font_size = ' font-size: 15px ;';
$font_size_firma = 'font-size: 5px;';

?>
<html lang='en'>

<head>
    <script src="JsBarcode.all.min.js"></script>
</head>
<style>
    body {
        height: 100%;
        width: 100%;
        font-family: Arial, Helvetica, sans-serif;
    }

    .barcode_img {
        padding-top: 190px;
        padding-left: 570px;
        height: 15px;
    }

    .datos_materia {
        padding-top: 0;
        padding-left: 75px;
        padding-right: 125px;
        padding-bottom: 0px;
        width: 100%;
        /* border: 3px solid slategrey; */
    }

    .tb-alumno th {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }

    .inf-col-1 {
        width: 175px;
        font-weight: bold;
    }

    .inf-col-2 {
        width: 115px
    }

    .inf-col-3 {
        font-weight: bold;
        width: 115px;
    }

    .inf-col-4 {
        width: 150px;
    }

    .tb-alumno {
        padding-top: 3px;
        /* border: 3px solid slateblue; */

    }

    .tb-alumno td {
        border-bottom: 1px solid black;
        padding: 0;
        margin: 0;
    }

    .col-no {
        width: 30px;
    }

    .col-no-hijo,
    .col-mat-hijo {
        text-align: center;
    }

    .col-mat {
        width: 60px;
    }

    .col-nom {
        width: 270px;
        text-align: center;
    }

    .col-calif {
        width: 140px;
        text-align: center;
    }

    .col-calif-num {
        text-align: center;
        padding-left: 5px;
    }

    .col-calif-let {
        text-align: center;
        width: 10px;
    }

    .col-tipo {
        text-align: center;
        width: 80px;
    }


    .tb-firma {
        position: absolute;
        top: 1563px;
        left: 95px;
        /*  border: 3px solid tomato; */
    }

    .firm-col-1 {
        width: 20px;
        text-align: center;
    }

    .firm-col-2 {
        width: 50px;
        text-align: center;
    }

    .firm-col-3 {
        width: 50px;
        text-align: center;
    }

    .firm-col-4 {
        width: 50px;
        text-align: center;
        padding-right: 100px;
    }

    .firm2-col-1 {
        width: 40px;
        padding-left: 10px;
        text-align: center;
    }

    .firm2-col-2 {
        width: 150px;
        text-align: center;
        /*background-color: sandybrown;*/
    }

    .firm2-col-3 {
        width: 140px;
        max-width: 140px;
        min-width: 140px;
        text-align: center;
        /* background-color:seagreen; */
    }

    .firm2-col-4 {
        width: 175px;
        max-width: 175px;
        /* background-color: royalblue; */
        text-align: right;
    }

    .firm2-col-5 {
        text-align: center;
        /*background-color: salmon;*/
    }
</style>

    <form method="Post" action="<?php echo base_url('Escolar/generalistapdf') ?>">
        <div style='background-repeat: no-repeat; background-position: left bottom; background-size: 100%; height: 100%; width: 100%; <?php echo $font_size; ?>'>
            <div>
                <table name='calif_arr' class="text-center text-white" style="background-color: #840f31; width: 100%; border-top: 2px solid white;">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 120px; ">IDC</th>
                            <th style="width: 120px; ">PERIODO</th>
                            <th style="width: 120px; "><?php echo utf8_decode('SECCIÓN') ?></th>
                            <th style="width: 300px; ">CARRERA</th>
                            <th style="width: 300px; " colspan="2">MATERIA</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <td><?php echo $idc ?></td>
                        <td><?php echo $periodo; ?></td>
                        <td><?php echo $seccion; ?></td>
                        <td><?php echo utf8_decode($carrera); ?></td>
                        <td colspan="2"><?php echo utf8_decode($nombremateria); ?></td>
                    </tbody>
                </table>
            </div>  
            
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th><?php echo utf8_decode('Matrícula') ?></th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Nombre</th>
                        <th>Materia</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $contador = 0; ?>
                    <?php foreach($alumnos as $alumno): ?>
                        <tr>
                            <td><?php echo $contador = $contador + 1; ?></td>
                            <td><?php echo $alumno->gpoalumno ?></td>
                            <td><?php echo utf8_decode($alumno->app_estudiante )?></td>
                            <td><?php echo utf8_decode($alumno->apm_estudiante) ?></td>
                            <td><?php echo utf8_decode($alumno->nombre_estudiante) ?></td>
                            <td>
                                <?php echo utf8_decode($alumno->nombre_materia) ?>
                            </td>
                        
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table> 
            </div>
        </div>
    </form>

