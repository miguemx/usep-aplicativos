INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'horariodocente');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'horariosecciones');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'getaulasdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'cambioaulas');

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'horariodocente');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'horariosecciones');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'getaulasdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'cambioaulas');


UPDATE `aplicativo_roles` SET `rol_menu` = 'enfermeria' WHERE `aplicativo_roles`.`rol_id` = 10;
UPDATE `aplicativo_roles` SET `rol_menu` = 'medicina' WHERE `aplicativo_roles`.`rol_id` = 9;

