<?php

namespace App\Models;

use CodeIgniter\Model;

class FiltroReportesModel extends Model
{
    protected $table = 'filtro_reportes_docentes';
    protected $primaryKey ='reportes_id';

    protected $returnType = 'App\Entities\FiltroReportes';
    protected $useSoftDeletes = false;
    protected $allowedFields = [
        'reportes_id','reportes_correo','reportes_matricula','reportes_fechacontagio'
    ];

public function buscaregistros($correo){
    $consulta = $this->where('reportes_correo',$correo)->last();
    var_dump($consulta);
    return $consulta;
}    
}