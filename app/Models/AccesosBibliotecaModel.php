<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\AccesoBiblioteca;

class AccesosBibliotecaModel extends Model {
    protected $table      = 'biblio_accesos';
    protected $primaryKey = 'biblioacceso_id';

    protected $returnType    = 'App\Entities\AccesoBiblioteca';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'biblioacceso_id', 'biblioacceso_alumno', 'biblioacceso_empleado', 'biblioacceso_tipo', 'biblioacceso_sexo',
        'biblioacceso_edad', 'biblioacceso_entrada', 'biblioacceso_salida', 
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un registro que no tenga fecha de entrada
     * @param usuarioId el id o matricula de usuario
     * @param esAlumno indica si es alumno o no
     * @return id el ID del folio de acceso
     * @return false falso si no se encuentra algun acceso
     */
    public function vigente( $usuarioId, $esAlumno ) {
        if ( $esAlumno ) $this->where( 'biblioacceso_alumno', $usuarioId );
        else $this->where( 'biblioacceso_empleado', $usuarioId );
        $this->groupStart();
        $this->where( 'biblioacceso_entrada IS NULL' );
        $this->orWhere( 'biblioacceso_salida IS NULL' );
        $this->groupEnd();
        $rows = $this->findAll();
        if ( count($rows) > 0 ) {
            return $rows[0]->id;
        }
        return false;
    }

    /**
     * devuelve un folio que está en uso, es decir, que registró entrada a biblioteca
     * @param usuarioId el id o matricula de usuario
     * @param esAlumno indica si es alumno o no
     * @return id el ID del folio de acceso
     * @return false falso si no se encuentra algun acceso
     */
    public function enUso( $usuarioId, $esAlumno ) {
        if ( $esAlumno ) $this->where( 'biblioacceso_alumno', $usuarioId );
        else $this->where( 'biblioacceso_empleado', $usuarioId );
        $this->where( 'biblioacceso_entrada IS NOT NULL' );
        $this->Where( 'biblioacceso_salida IS NULL' );
        $rows = $this->findAll();
        if ( count($rows) > 0 ) {
            return $rows[0]->id;
        }
        return false;
    }

    /**
     * devuelve un folio que solo se creó pero que no tiene registrado acceso ni salida
     * @param usuarioId el id o matricula de usuario
     * @param esAlumno indica si es alumno o no
     * @return id el ID del folio de acceso
     * @return false falso si no se encuentra algun acceso
     */
    public function creado( $usuarioId, $esAlumno ) {
        if ( $esAlumno ) $this->where( 'biblioacceso_alumno', $usuarioId );
        else $this->where( 'biblioacceso_empleado', $usuarioId );
        $this->where( 'biblioacceso_entrada IS NULL' );
        $this->Where( 'biblioacceso_salida IS NULL' );
        $rows = $this->findAll();
        if ( count($rows) > 0 ) {
            return $rows[0]->id;
        }
        return false;
    }

    /**
     * ajusta la hora de salida a todos los registros que solo tienen entrada;
     * la hora de salida es la hora actual del sistema
     */
    public function cerrar() {
        $salida = date("Y-m-d H:i:s");
        $this->where('biblioacceso_entrada IS NOT NULL')->where('biblioacceso_salida IS NULL')->set( [ 'biblioacceso_salida' => $salida ] )->update();
    }

}