<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AlumnoSeguro extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'matricula' => null,
        'regPatrimonial' => null, // es patronal pero me pasé de listo
        'ssn' => null,
        'salario' => null,
        'generico6' => null,
        'tipoContratacion' => null,
        'tipoSalario' => null,
        'semana' => null,
        'fecha' => null,
        'umf' => null,
        'generico2' => null,
        'tipoMovimiento' => null,
        'guia' => null,
        'generico1' => null,
        'formato' => null,
        'archivo' => null,
        'status' => null,
        'razon' => null,
        'razonSecundaria' => null,
        'comentarios' => null,
        'fechaDescarga' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'matricula' => 'seguro_matricula',
        'regPatrimonial' => 'seguro_reg_patrimonial',
        'ssn' => 'seguro_ssn',
        'salario' => 'seguro_salario',
        'generico6' => 'seguro_generico_6',
        'tipoContratacion' => 'seguro_tipo_contratacion',
        'tipoSalario' => 'seguro_tipo_salario',
        'semana' => 'seguro_semana',
        'fecha' => 'seguro_fecha',
        'umf' => 'seguro_umf',
        'generico2' => 'seguro_generico_2',
        'tipoMovimiento' => 'seguro_tipo_movimiento',
        'guia' => 'seguro_guia',
        'generico1' => 'seguro_generico_1',
        'formato' => 'seguro_formato',
        'archivo' => 'seguro_archivo',
        'status' => 'seguro_status',
        'razon' => 'seguro_razon',
        'razonSecundaria' => 'seguro_razon_secundaria',
        'comentarios' => 'seguro_comentarios',
        'fechaDescarga' => 'seguro_fecha_descarga',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
