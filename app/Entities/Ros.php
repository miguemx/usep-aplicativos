<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Ros extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'fechaPago' => null,
        'nombre' => null,
        'concepto' => null,
        'cuenta' => null,
        'descCuenta' => null,
        'importe' => null,
        'folioInteligente' => null,
        'referencia' => null,
        'estatusServicio' => null,
        'fechaOtorgado' => null,
        'usuario' => null,
        'unidadResponsable' => null,
        'beneficiario' => null,
        'cotejado' => null,
        'matricula' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'ros_id',
        'fechaPago'         => 'ros_fecha_pago',
        'nombre'            => 'ros_nombre',
        'concepto'          => 'ros_concepto',
        'cuenta'            => 'ros_cuenta',
        'descCuenta'        => 'ros_desc_cuenta',
        'importe'           => 'ros_importe',
        'folioInteligente'  => 'ros_folio_inteligente',
        'referencia'        => 'ros_referencia',
        'estatusServicio'   => 'ros_estatus_servicio',
        'fechaOtorgado'     => 'ros_fecha_otorgado',
        'usuario'           => 'ros_usuario',
        'unidadResponsable' => 'ros_unidad_responsable',
        'beneficiario'      => 'ros_beneficiario',
        'cotejado'          => 'ros_cotejado',
        'matricula'         => 'ros_matricula',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
