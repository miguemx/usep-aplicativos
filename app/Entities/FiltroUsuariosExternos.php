<?php
namespace App\Entities;

use CodeIgniter\Entity;

class FiltroUsuariosExternos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'correo' => null,
        'nombre' => null,
        'dependencia' => null,
        'edad'=> null,
        'sexo'=> null,
        'categoria'=>null

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'correo' => "externo_correo",
        'nombre' => "externo_nombre",
        'dependencia' => "externo_dependencia",
        'edad'=> "externo_edad",
        'sexo'=>"externo_sexo",
        'categoria'=>'externo_categoria',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}