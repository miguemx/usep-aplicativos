<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<div class="container-fluid" style="margin-top: 15px;">

    <?php if ($mensaje) : ?>
        <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
            <?php echo $mensaje['texto']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow p-3 m-2 ">
        <span>
            <h4>Listado de Docentes</h4>
        </span>
        <form method="get" action="<?php echo base_url('ProgramacionAcademica/MenuDocentes/') ?>">

            <div class="row" style="padding-top: 10px;">
                <div class="mb-3 row col-sm-6">
                    <label for="matricula" class="col-sm-2 col-form-label">No. Empleado</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="id" name="id" value="<?php echo $id; ?>" />
                    </div>
                </div>

                <div class="mb-3 col-sm-6" style="text-align: right;">
                    <a href="<?php echo base_url('ProgramacionAcademica/MenuDocentes/') ?>" class="btn btn-secondary btn-sm">Ver todos</a>
                    <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
                </div>

            </div>
            <div class="row">
                <div class="mb-3 row col-sm-4">
                    <label for="matricula" class="col-sm-4 col-form-label">Apellido Paterno</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="matricula" name="apPaterno" value="<?php echo $apPaterno; ?>" />
                    </div>
                </div>
                <div class="mb-3 row col-sm-4">
                    <label for="matricula" class="col-sm-4 col-form-label">Apellido Materno</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="matricula" name="apMaterno" value="<?php echo $apMaterno; ?>" />
                    </div>
                </div>
                <div class="mb-3 row col-sm-4">
                    <label for="matricula" class="col-sm-4 col-form-label">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="matricula" name="nombre" value="<?php echo $nombre; ?>" />
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>No. Empleado</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Nombre</th>
                    <th>Correo Institucional</th>
                    <th>Periodos</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($empleados as $empleado) : ?>
                    <tr>
                        <td class="col-1"><?php echo $empleado->id; ?></td>
                        <td class="col-2"><?php echo $empleado->apPaterno; ?></td>
                        <td class="col-2"><?php echo $empleado->apMaterno; ?></td>
                        <td class="col-2"><?php echo $empleado->nombre; ?></td>
                        <td class="col-2"><?php echo $empleado->correo; ?></td>
                        <td class="col-2">
                            <div class="row">
                                <div class="col-8">
                                    <select class="form-select form-select-sm" name="select_periodo" id="<?php echo "select_periodo" . $empleado->id; ?>" onchange="validar(<?php echo $empleado->id ?>,this)">
                                        <option value="0">Selecciona un periodo</option>
                                        <?php foreach ($periodos as $periodo_select) : ?>
                                            <option value="<?php echo $periodo_select->id ?>"><?php echo $periodo_select->id ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-1">
                                    <form action="<?php echo base_url('ProgramacionAcademica/HorarioDocente/' . $empleado->id) ?>" method="post" id="<?php echo 'form_' . $empleado->id ?>">
                                        <button class="btn btn-default btn-sm " type="button" disabled id="<?php echo "btn_buscar_" . $empleado->id; ?>" onclick="buscar(<?php echo $empleado->id; ?>)">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                            <span class="mx-1">Buscar</span>
                                        </button>
                                    </form>
                                </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div>
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>

<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }

    .button-switch {
        margin: 0px;
        padding: 0px;
        border: 0px;
        background: none;
    }
</style>

<script type="text/javascript">
    function validar(empleado, select) {
        console.log(empleado);
        console.log(select.value);
        if (select.value != '0') {
            document.getElementById('btn_buscar_' + empleado).disabled = false;
        } else {
            document.getElementById('btn_buscar_' + empleado).disabled = true;

        }
    }

    function buscar(docente) {
        var select = document.getElementById('select_periodo' + docente);
        if (select.value != '0') {
            var input = document.createElement('input');
            input.name = 'periodo';
            input.value = select.value;
            input.hidden = true;
            document.getElementById('btn_buscar_' + docente).appendChild(input);
            document.getElementById('form_' + docente).submit();
        }
    }
</script>

<?php echo $this->endSection() ?>