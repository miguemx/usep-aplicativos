<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">

	<title>USEP</title>

	<style type="text/css">
		<?= preg_replace('#[\r\n\t ]+#', ' ', file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'debug.css')) ?>
	</style>
</head>
<body>

	<div class="container text-center">

		<h1 class="headline">Lo sentimos!</h1>

		<p class="lead">Al parecer ocurrió un error inesperado. Por favor intenta más tarde o comunícate con un administrador.</p>

	</div>

</body>

</html>
