<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .td2,.td3,.td4,.td5,.td6,.td7,.td8{
        width: 250px;
    }
    .aula{
        font-size: 12px;
        
    }
.clase1{
    display: none;
}
.mensaje1{
    font-size: 100px;
    text-align: center;
    text-stroke: 5px;
}
</style>
<script>
     
	function mostrar_aula() {

		if ($('input[name="aulavalida"]').is(':checked')) {
            document.getElementById("div1").style.display = "none";
			document.getElementById("idaula").value = "<?php if (isset ($idaula)) {echo $idaula;} ?>";
		}
        if ($('input[name="aulavalida"]').is(':checked')== false) {
			document.getElementById("div1").style.display = "block"
			document.getElementById("idaula").value = "";
        }
	}

    function mostrar_aula_elementos() {
		if (($('input[name="itemsvalida"]').is(':checked'))) {
            document.getElementById("tabla_elementos").style.display = "block";
            
		}
        if ($('input[name="itemsvalida"]').is(':checked')== false) {
			document.getElementById("tabla_elementos").style.display = "none";
        }
	}
    
</script>
<?php
if (isset ($conhorario)): ?>
<div class="container">
    <div class="card">
        <div class="card-header text-center" >Bienvenido(a)</div>
            <div class="card-body">
                <form id="datos_aula" method="POST" action="<?php echo base_url('Docente/bitacoraInserta'); ?>" style="text-align:left">
                    <div class="form-check form-switch">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row"></td>
                                    <td><input class="form-control" hidden type="text" name="idaula" id="idaula" value="<?php echo $idaula; ?>">
                                        <label style="padding-right:100px" class="form-check-label" for="mySwitch">¿Se encuentra usted en el <b><?php echo $aula ?></b>? </label></td>
                                    <td>
                                        <input  class="form-check-input" type="checkbox" id="aulavalida" name="aulavalida" value="1" checked onchange="mostrar_aula();">
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    <div class="form-group text-left required clase1" id="div1">
                                    <label for="aulaname" id="labaulaname" >¿En que Aula/Laboratorio esta ubicado?</label>
                                    <select id="aulaname " class="custom-select" style="width: 80%;" name="aulaname" >
                                    <?php foreach ($selectaula as $aulas) : ?>
                                        <option value = <?php echo $aulas->id_aula ?>><?php
                                                echo $aulas->id_aula." - ".$aulas->aula_nombre;
                                                ?></option>
                                    <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-check form-switch">
                            <label style="padding-right:100px" class="form-check-label" for="mySwitch">¿Faltan elementos del Aula donde se encuentra? </label>
                            <input  class="form-check-input" type="checkbox" id="itemsvalida" name="itemsvalida" value="1" onchange="mostrar_aula_elementos();">
                    </div>
                    <div id="tabla_elementos" class ="clase1">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Elemento</th>
                                    <th>¿Hace Falta?</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/pantalla.jpg') ?>" alt="" height ="60">Pantalla</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="pantalla" name="pantalla" value="1"> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/cpu.jpg') ?>" alt="" height ="60">CPU</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="cpu" name="cpu" value="I">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/teclado.jpg') ?>" alt="" height ="60">Teclado</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="teclado" name="teclado" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/mouse.jpg') ?>" alt="" height ="60">Mouse</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="mouse" name="mouse" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/microfono.jpg') ?>" alt="" height ="60">Microfono</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="microfono" name="microfono" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/camara.jpg') ?>" alt="" height ="60">Camara</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="camara" name="camara" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/control.jpg') ?>" alt="" height ="60">Control</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="control" name="control" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/hubdetab.jpg') ?>" alt="" height ="60">Hub de Tab</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="hubdetab" name="hubdetab" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/hubdedisplay.jpg') ?>" alt="" height ="60">Hub de Display</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="hubdedisplay" name="hubdedisplay" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/bocinas.jpg') ?>" alt="" height ="60">Bocinas</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="bocinas" name="bocinas" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"><img src="<?php echo base_url('img/elementosaula/cables.jpg') ?>" alt="" height ="60">Cables</td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="cables" name="cables" value="1">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <input class="btn btn-secondary text-center" type="submit" id="guardar" value="Aceptar">
                    </div>
                </form>
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
<?php endif;

if (isset($sinhorario)) :
?>
<div class="container shadow py-2">
                        <div class="row justify-content-center">
                            <div class="col-lg-3 col-sm-6" style="mensaje1">
                               
                                <br>Sin datos</br>

                            </div>
                          
                        </div>
                    </div>
<?php endif; ?>

<?php 
if (isset($acept)) :

?>
<div class="container shadow py-2" class>
                        <div class="row justify-content-center">
                           
                            <div class="col-lg-3 col-sm-6" style="mensaje1">
                                <br>Registro guardado</br>
                                
                            </div>
                        </div>
                    </div>
<?php endif; ?>

<?php 
if (isset($error)) :

?>

<br>Algo ha impedido esta acción</br>
<?php endif; ?>
<?php echo $this->endSection() ?>