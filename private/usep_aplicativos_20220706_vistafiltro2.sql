DROP VIEW IF EXISTS view_filtro_accesos;

CREATE VIEW view_filtro_accesos AS

SELECT 
    filtroacceso_id,
    filtroacceso_temperatura,
    filtroacceso_oximetria,
    filtro_accesos.created_at as fecha_acceso,
    filtrofolio_id,
    filtro_folios.created_at as fecha_cuestionario,
    empleado_id,
    empleado_correo,
    empleado_nombre,
    empleado_ap_paterno,
    empleado_ap_materno,
    empleado_sexo,
    alumno_id,
    alumno_correo,
    alumno_nombres,
    alumno_ap_paterno,
    alumno_ap_materno,
    alumno_sexo,
    externo_correo,
    externo_nombre,
    externo_dependencia,
    externo_edad,
    externo_sexo
FROM `filtro_accesos`
INNER JOIN filtro_folios ON filtroacceso_folio=filtrofolio_id
LEFT JOIN aplicativo_usuarios ON filtrofolio_usuario=usuario_correo
LEFT JOIN aplicativo_empleados ON usuario_correo=empleado_correo
LEFT JOIN alumnos_generales ON usuario_correo=alumno_correo
LEFT JOIN filtro_usuarios_ext ON filtrofolio_usuario_ext=externo_correo;

DROP VIEW IF EXISTS view_filtro_respuestas;
CREATE VIEW view_filtro_respuesta AS
SELECT *  
FROM `filtro_respuestas` 
INNER JOIN filtro_preguntas ON filtroresp_pregunta=filtropreg_id;