CREATE
 VIEW `view_calificaciones_edit`
as SELECT
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno` AS `gpoalumno_alumno`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo` AS `gpoalumno_grupo`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_ordinario` AS `gpoalumno_ordinario`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_extraordinario` AS `gpoalumno_extraordinario`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_comentario` AS `gpoalumno_comentario`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`aprobada` AS `aprobada`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_flg_pago` AS `gpoalumno_flg_pago`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_num_oficio` AS `gpoalumno_num_oficio`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_autor` AS `gpoalumno_autor`,
    `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_fecha_modificacion` AS `gpoalumno_fecha_modificacion`,
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_lunes` AS `grupo_lunes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_martes` AS `grupo_martes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_miercoles` AS `grupo_miercoles`,
    `usep_aplicativos`.`aca_grupos`.`grupo_jueves` AS `grupo_jueves`,
    `usep_aplicativos`.`aca_grupos`.`grupo_viernes` AS `grupo_viernes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_sabado` AS `grupo_sabado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_domingo` AS `grupo_domingo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_flag_acta` AS `grupo_flag_acta`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechacaptura` AS `grupo_fechacaptura`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechaimpresion` AS `grupo_fechaimpresion`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`aca_materias`.`materia_clasificacion` AS `materia_clasificacion`,
    `usep_aplicativos`.`aca_materias`.`materia_seriacion` AS `materia_seriacion`,
    `usep_aplicativos`.`aca_materias`.`materia_semestre` AS `materia_semestre`,
    `usep_aplicativos`.`prein_carreras`.`carrera_id` AS `carrera_id`,
    `usep_aplicativos`.`prein_carreras`.`carrera_nombre` AS `carrera_nombre`,
    `usep_aplicativos`.`prein_carreras`.`carrera_numero` AS `carrera_numero`,
    `usep_aplicativos`.`prein_carreras`.`carrera_clave` AS `carrera_clave`,
    `usep_aplicativos`.`prein_carreras`.`carrera_clave_programa` AS `carrera_clave_programa`,
    `usep_aplicativos`.`prein_carreras`.`carrera_facultad` AS `carrera_facultad`,
    `usep_aplicativos`.`aca_periodos`.`periodo_id` AS `periodo_id`,
    `usep_aplicativos`.`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,
    `usep_aplicativos`.`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,
    `usep_aplicativos`.`aca_periodos`.`periodo_fin` AS `periodo_fin`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_id` AS `alumno_id`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_correo` AS `alumno_correo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombre` AS `alumno_nombre`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres` AS `alumno_nombres`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno` AS `alumno_ap_paterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno` AS `alumno_ap_materno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_curp` AS `alumno_curp`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_sexo` AS `alumno_sexo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_carrera` AS `alumno_carrera`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_vigente` AS `alumno_vigente`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_else_regid` AS `alumno_else_regid`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_else_regpass` AS `alumno_else_regpass`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_periodo` AS `alumno_periodo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_semestre` AS `alumno_semestre`,
    `usep_aplicativos`.`alumnos_generales`.`created_at` AS `created_at`,
    `usep_aplicativos`.`alumnos_generales`.`updated_at` AS `updated_at`,
    `usep_aplicativos`.`alumnos_generales`.`deleted_at` AS `deleted_at`,
    CONCAT(
        `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre`,
        ' ',
        `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno`,
        ' ',
        `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno`
    ) AS `empleado_nombre`
FROM
    (
        (
            (
                (
                    (
                        (
                            `usep_aplicativos`.`aca_grupos_alumnos`
                        JOIN `usep_aplicativos`.`aca_grupos` ON
                            (
                                `usep_aplicativos`.`aca_grupos`.`grupo_id` = `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_grupo`
                            )
                        )
                    JOIN `usep_aplicativos`.`aca_materias` ON
                        (
                            `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
                        )
                    )
                JOIN `usep_aplicativos`.`prein_carreras` ON
                    (
                        `usep_aplicativos`.`prein_carreras`.`carrera_id` = `usep_aplicativos`.`aca_materias`.`materia_carrera`
                    )
                )
            JOIN `usep_aplicativos`.`aca_periodos` ON
                (
                    `usep_aplicativos`.`aca_periodos`.`periodo_id` = `usep_aplicativos`.`aca_grupos`.`grupo_periodo`
                )
            )
        JOIN `usep_aplicativos`.`alumnos_generales` ON
            (
                `usep_aplicativos`.`alumnos_generales`.`alumno_id` = `usep_aplicativos`.`aca_grupos_alumnos`.`gpoalumno_alumno`
            )
        )
    JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aca_grupos`.`grupo_docente` = `usep_aplicativos`.`aplicativo_empleados`.`empleado_id`
        )
    )
ORDER BY
    `usep_aplicativos`.`aca_periodos`.`periodo_fin`
DESC;
    