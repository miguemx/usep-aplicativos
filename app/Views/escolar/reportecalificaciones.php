<div class="container">
    <table class="table table-hover  table-responsive">
        <thead class="thead-default">
            <tr>
                <th>MATRICULA</th>
                <th>NOMBRE</th>
                <th colspan="2">CALIFICACION</th>
                <th>EVALUACION</th>
                <th>SECCION</th>
                <th>GRUPO</th>
                <th>PERIODO</th>
                <th>MATERIA</th>
                <th>SEMESTRE</th>
                <th>CARRERA</th>
                <th>DOCENTE</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($reporte as $registro): ?>
                <tr>
                    <td><?php echo $registro->matricula ?></td>
                    <td><?php echo $registro->ap_paterno." ".$registro->ap_materno." ".$registro->nombre ?></td>
                    <td><?php echo $registro->calificacion ?></td>
                    <td><?php echo $registro->letra ?></td>
                    <td><?php echo $registro->evaluacion ?></td>
                    <td><?php echo $registro->seccion ?></td>
                    <td><?php echo $registro->grupo ?></td>
                    <td><?php echo $registro->periodo ?></td>
                    <td><?php echo $registro->materia."-".$registro->materia_nombre ?></td>
                    <td><?php echo $registro->semestre ?></td>
                    <td><?php echo $registro->docente ?></td>
                    <td><?php echo $registro->docente_nom ?></td>
                    
                </tr>
            <?php endforeach; ?>
    
        </tbody>
    </table>

</div>