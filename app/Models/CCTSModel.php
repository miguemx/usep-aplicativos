<?php
namespace App\Models;
use CodeIgniter\Model;

class CCTSModel extends Model {
    protected $table      = 'cat_ccts';
    protected $primaryKey = 'cct_cct';

    protected $returnType    = 'App\Entities\CCTS';
    protected $useSoftDeletes = false;
    protected $allowedFields = [
        'cct_cct','cct_nombre','cct_tipo_sep','cct_calle','cct_num','cct_estado',
        'cct_municipio','cct_localidad','cct_colonia','cct_cp','cct_tipo','cct_nivel'
    ];
    protected $useTimestamps = false;
    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca registros de CCTS de acuerdo a los filtros recibidos en el array
     * @param filtros un arreglo con llaves para buscar registros
     * @return ccts devuelve un arreglo con objetos de los CCTs encontrados
     */
    public function busca($filtros) {
        $this->creaFiltros( $filtros );
        return $this->findAll();
    }

    /**
     * retorna todos los centros pertenecientes al municipio
     * @param municipio corresponde al nombre del municipio a buscar
     */
    public function getcctsbymunicipio($municipio)
    {
        return $this->where('cct_municipio',$municipio)->findAll();
    }

    /**
     * Obtiene todos los estados listados en el catálogo de CCT
     * @return estados el listado con los nombres de los estados
     */
    public function getEstados() {
        $estados = [];
        $sql = "SELECT DISTINCT(cct_estado) FROM $this->table ORDER BY cct_estado";
        $query = $this->db->query( $sql );
        $rows = $query->getResult();
        foreach ( $rows as $row ) {
            $estados[] = $row->cct_estado;
        }
        return $estados;
    }

    /**
     * obtiene todos los municipios de un estado, registrados en la tabla CCTs
     * @param estado la cadena con el nombre del estado
     * @return municipios el listado con los nombres de los municipios
     */
    public function getMunicipios($estado) {
        $municipios = [];
        $sql = "SELECT DISTINCT(cct_municipio) FROM $this->table WHERE cct_estado='$estado' ORDER BY cct_municipio";
        $query = $this->db->query( $sql );
        $rows = $query->getResult();
        foreach ( $rows as $row ) {
            $municipios[] = $row->cct_municipio;
        }
        return $municipios;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('cct', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['cct']) ) {
                $this->where( 'cct_cct', $filtros['cct'] );
            }
        }
        if ( array_key_exists('estado', $filtros) ) { 
            if ( strlen($filtros['estado']) ) {
                $this->like( 'cct_estado', trim($filtros['estado']), 'both' ); 
            }
        }
        if ( array_key_exists('municipio', $filtros) ) { 
            if ( strlen($filtros['municipio']) ) {
                $this->like( 'cct_municipio', trim($filtros['municipio']), 'both' ); 
            }
        }
        $this->like( 'cct_tipo_sep', 'PLANTEL (MEDIA SUPERIOR)', 'both' ); 
        $this->orderBy('cct_nombre','ASC');
    }
    

}