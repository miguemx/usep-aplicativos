<?php
namespace App\Entities;

use CodeIgniter\Entity;

class VotacionProcesos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'inicio' => null,
        'fin' => null,
        'nombre' => null,
        'terminado' => null
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'proceso_id',
        'inicio' => 'proceso_inicio',
        'fin' => 'proceso_fin',      
        'nombre' => 'proceso_nombre',
        'terminado' => 'proceso_terminado'

    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
