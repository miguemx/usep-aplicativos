ALTER TABLE `aspirantes_generales` 
    ADD `aspirante_inscripcion_fecha` DATE NULL AFTER `aspirante_aceptado`, 
    ADD `aspirante_inscripcion_hora` TIME NULL AFTER `aspirante_inscripcion_fecha`;

ALTER TABLE `aspirantes_generales` ADD `aspirante_inscrito` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `aspirante_inscripcion_hora`;