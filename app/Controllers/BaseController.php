<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use App\Models\NotificacionesModel;
use App\Entities\Notificacion;
use App\Models\PermisosModel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	public $sesion = null;

	public $misnotificaciones = [];

	public $errorMessage = '';

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();

		$this->sesion = \Config\Services::session();
		$this->Validacion = \Config\Services::validation();
		$this->leeNotificaciones();

		$this->permiso();

	}

	/**
	 * obtiene las notificaciones del usuario actual
	 */
	protected function leeNotificaciones() {
		if ( $this->sesion->get('useremail')  ) {
			$notificacionesModel = new NotificacionesModel();
			$this->misnotificaciones = $notificacionesModel->forUser( $this->sesion->get('useremail') );
		}
	}

	/**
	 * genera una notificacion para un usuario determinado
	 * @param usuario el correo del usuario al que se le desea notificar
	 * @param texto el texto de la notificacion
	 */
	protected function notifica( $usuario, $texto ) {
		$notificacionesModel = new NotificacionesModel();
		$notificacion = new Notificacion();
		$notificacion->creador = $this->sesion->get( 'useremail' );
		$notificacion->destinatario = $usuario;
		$notificacion->texto = $texto;
		$notificacion->hora = date("H:i:s");
		try {
			$notificacionesModel->insert( $notificacion );
		}
		catch ( \Exception $ex ) {
			echo $ex->getMessage();
		}
	}

	/**
	 * devuelve el nombre del mes en texto a partir del numero dado
	 * @param mes el numero del mes
	 * @return nombre el nombre del mes
	 */
	protected function getMes($mes) {
		$meses = [
			'ND', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'
		];
		$i = intval($mes);
		return $meses[$i];
	}

	/**
     * obtiene los datos de la peticion post y los devuelve en el mismo arreglo convertido a mayusculas o minusculas dependiedo del parametro
     * @param mayus indica si se cambia el texto: 1 para mayusculas, 2 para minúsculas, 0 (default) sin cambio
     * @param except un arreglo de la siguiente manera: [0] => llave del campo a convertir, [1] => 0 sin cambio, 1=>mayus, 2=>minus
     */
    protected function mayusDatos($mayus=0, $except = []) {
        $datos = $this->request->getPost(); 
        $post = array();
        foreach ( $datos as $llave=>$valor ) {
            if ( $mayus === 1 ) $post[ $llave ] = mb_strtoupper( $valor );
            if ( $mayus === 2 ) $post[ $llave ] = mb_strtolower( $valor );
        }
		foreach ( $except as $llave => $cambio ) {
			if ( array_key_exists( $llave, $datos) ) {
				if ( $cambio === 0 ) $post[$llave] = $datos[$llave];
				if ( $cambio === 1 ) $post[$llave] = mb_strtoupper( $datos[$llave] );
				if ( $cambio === 2 ) $post[$llave] = mb_strtolower( $datos[$llave] );
			}
		}
		return $post;
    }

	/**
	 * verifica que un rol iniciado tenga permiso sobre la solicitud recibida
	 * y dibuja la pantalla de error 403 en caso de no tener permiso.
	 */
	protected function permiso() {
		$except = [
			'login', 'jobs', 'e', 'filtrosanitario', 'renderfiles', 'admision'
		];
		$rol = $this->sesion->get('rol_id');
		$segmentos = $this->request->uri->getTotalSegments();
		if ( $segmentos > 0 ) {  // cuando se accede a algun modulo
			$modulo = trim( strtolower( $this->request->uri->getSegment(1) ) );
			$funcion = null;
			$permisosModel = new PermisosModel();
			if ( !in_array($modulo, $except) ) {
				$funcion = null;
				if ( $segmentos > 1 ) $funcion = trim( strtolower( $this->request->uri->getSegment(2) ) );
				if ( !$permisosModel->permitido( $rol, $modulo, $funcion ) ) {
					echo view('errors/html/error_403');
					die();
				}
			}
		}
	}

	/**
	 * cifra una cadena 
	 * @param cadena la cadena a cifrar
	 * @return cifrado la cadena cifrada
	 * @deprecated utilizar la librería de cifrado \App\Libraries\Cifrado::cifrar
	 */
	protected function cifrar( $cadena ) {
		$config         = new \Config\Encryption();
		$config->key    = env('crypt.llave');
		$config->driver = env('crypt.algoritmo');

		$encrypter = \Config\Services::encrypter($config);
		$ciphertext = $encrypter->encrypt($cadena);
		$cifrado = base64_encode( $ciphertext );
		$cifrado = str_replace( '+', '_MAS_', $cifrado );
		$cifrado = str_replace( '/', '_DIA_', $cifrado );
		return $cifrado;
	}

	/**
	 * descifra una cadena
	 * @param cifrado la cadena cifrada
	 * @return cadena la cadena descifrada
	 * @return null en caso de error en el descifrado
	 * @deprecated utilizar la librería de cifrado \App\Libraries\Cifrado::descifrar
	 */
	protected function descifrar ( $cifrado ) {
		$cadena = null;

		$cifrado = str_replace( '_MAS_', '+', $cifrado );
		$cifrado = str_replace( '_DIA_', '/', $cifrado );

		$config         = new \Config\Encryption();
		$config->key    = env('crypt.llave');
		$config->driver = env('crypt.algoritmo');
		$encrypter = \Config\Services::encrypter($config);

		try {
			$ciphertext = base64_decode( $cifrado );
			$cadena = $encrypter->decrypt($ciphertext);
		}
		catch(\CodeIgniter\Encryption\Exceptions\EncryptionException $ex) {
			$this->errorMessage = 'No se puede encontrar descifrar el valor. '.$ex->getMessage();	
		}
		return $cadena;
	}

	/**
	 * envia un correo electrónico a la dirección especificada con el mensaje proporcionado
	 * @param destinatario la dirección de correo del destinatario
	 * @param mensaje el mensaje a enviar en formato HTML
	 * @param asunto el asunto del correo electrónico
	 * @param correoDe (opcional) indica la dirección de correo de la cual se envía el mensaje
	 * @param nombreDe (opcional) indica el nombre que desea que aparezca como remitente
	 */
	protected function correo( $destinatario, $mensaje, $asunto, $correoDe='sistemas@usep.mx', $nombreDe='Universidad de la Salud' ) {
		$email = \Config\Services::email();

		$config['mailType'] = 'html';
		$config['protocol'] = env('mail.protocol');
		$config['SMTPHost'] = env('mail.smtphost');
		$config['SMTPUser'] = env('mail.smtpuser');
		$config['SMTPPass'] = env('mail.smtppass');
		$config['SMTPPort'] = env('mail.smtpport');
		$config['SMTPCrypto'] = env('mail.smtpcrypto');
		$email->initialize($config);
		
		$email->setFrom($correoDe, $nombreDe);
		
		if ( env('CI_ENVIRONMENT') == 'production' ) {
			$email->setTo( $destinatario );
			$email->setBCC( env('mail.debug') );
		}
		else {
			$email->setTo( env('mail.debug') );
		}
		
		$email->setSubject( $asunto );
		$email->setMessage( $mensaje );

		try {
			$email->send(false);
		}
		catch (\Exception $ex) {
			log_message("error","MAIL ERROR {exception}", ["exception"=>$ex]);
		}
	}

	/**
	 * invoca la libreria de validacion y regresa el resultado de dicha validacion
	 * @param data los datos a validar en un array asociativo
	 * @param regla una cadena con la regla de validacion a utilizar, registrada en la libreria de validacion
	 * @return true verdadero si no existe ningun error
	 * @return result arreglo con los mensajes de error
	 */
	protected function valida($data, $regla) {
		if ( $this->Validacion->run( $data, $regla ) ) { 
			return true;
		}
		else {
			$errors = array();
			foreach ( $this->Validacion->getErrors() as $error ) {
				$errors[] = $error;
			}
			return $errors;
		}
	}



}
