<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>
<style>
.negro{
    color: black !important;
}
</style>   
<?php 
$disabled = false;
foreach ( $etapas as $etapa ) {
    if( $formulario->etapa == $etapa->etapa  ) {
        if( $etapa->completado == true ) {
            $disabled = true;
        }
    }
}
?>

<h4><?php echo $formulario->titulo; ?></h4>
<div class="negro">
<?php echo $formulario->instrucciones; ?>
</div>
<hr />
<div class="container"> 
    <?php if(isset($errores)): ?>
        <div class="alert alert-primary" style="color: #fff;">
            <?php echo $errores; ?>
        </div>
    <?php endif; ?>

    <?php if(isset($msg)): ?>
        <div class="alert alert-success" style="color: #fff;">
            <?php echo $msg; ?>
            <script>
                setTimeout( redir, 2000 );
                function redir() {
                    document.location.href = '<?php echo $redir; ?>';
                }
            </script>
        </div>
    <?php endif; ?>
    <form method="post" action="<?php echo base_url('Admision/Formulario').'/'.$formulario->formulario; ?>" enctype="multipart/form-data">
        <div class="row">
            <?php foreach( $campos as $campo ): ?>
                <div class="card mb-2">
                <div class="card-body" >

                <?php if( array_key_exists($campo->tipo, $tiposCampos) ): 
                    $value = (array_key_exists($campo->campo, $respuestas))? $respuestas[$campo->campo]['valor']: null;
                    $props = null;
                    if ( !is_null($value) ) {
                        $props['respuesta'] = $respuestas[$campo->campo]['id'];
                        $props['comentario'] = $respuestas[$campo->campo]['comentario'];
                        $props['aspirante'] = $respuestas[$campo->campo]['aspirante'];
                        $props['status'] = $aspirante->estatus;
                    }
                    echo myInput( $campo, $tiposCampos[$campo->tipo], $value, $disabled, 'status', 'comment', $props );
                    ?>
                <?php endif; ?>
                </div>
            </div>
            <?php endforeach; ?>
            
            <hr />
            <div class="col-md-12" style="font-size: 14px; margin-bottom: 10px;">Campos marcados con asterisco (*) son obligatorios.</div>
            
            <div class="col-md-12">
                <?php if( $aspirante->etapa == $formulario->etapa ):  ?>
                    <button type="submit" class="btn bg-gradient-primary" name="prc" value="1">
                        Guardar
                    </button>
                <?php endif; ?>
                <a type="button" class="btn bg-gradient-secondary" href="<?php echo base_url('Admision/Etapa').'/'.$formulario->etapa; ?>" >
                    Regresar
                </a>
            </div>
        </div>
    </form>

</div>

<script>
    var files = document.getElementsByClassName('file-value');
    for ( let i=0; i<files.length; i++ ) {
        files[i].removeAttribute('name');
        let myId = files[i].id;
        files[i].onchange = function(control) {
            let input =  control.srcElement;
            input.name = input.id;
        };
    }
</script>

<?php echo $this->endSection() ?>