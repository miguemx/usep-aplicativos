<?php
namespace App\Entities;

use CodeIgniter\Entity;

class BitacoraAulas extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'numero' => null,
        'docente' => null,
        'aula_id' => null,
        'aula_bandera' => null,
        'pantalla' => null,
        'cpu' => null,
        'teclado' => null,
        'mouse' => null,
        'microfono' => null,
        'camara' => null,
        'control' => null,
        'hubtab' => null,
        'hubdisplay' => null,
        'bocinas' => null,
        'cables' => null,
        'f_actual' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'numero' => 'num',
        'docente' => 'id_docente',
        'aula_id' => 'aula',
        'aula_bandera' => 'aula_conf',
        'pantalla' => 'f_pantalla',
        'cpu' => 'f_cpu',
        'teclado' => 'f_teclado',
        'mouse' => 'f_mouse',
        'microfono' => 'f_microfono',
        'camara' => 'f_camara',
        'control' => 'f_control',
        'hubtab' => 'f_hubtab',
        'hubdisplay' => 'f_hubdisplay',
        'bocinas' => 'f_bocinas',
        'cables' => 'f_cables',
        'f_actual' => 'fecha',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}