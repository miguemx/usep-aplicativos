<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Constancia;

class ConstanciasModel extends Model
{
    protected $table      = 'alumnos_constancias';
    protected $primaryKey = 'constancia_id';

    protected $returnType    = 'App\Entities\Constancia';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'constancia_id', 'constancia_alumno', 'constancia_periodo', 'constancia_folio', 'constancia_tipo',
        'constancia_fecha_exp',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function listar($filtros = array())
    {
        $pager = \Config\Services::pager();
        $this->creaFiltros($filtros);
        $alumnos = $this->paginate(25);
        return $alumnos;
    }

    /**
     * busca todas las ocurrencias de constancias de acuerdo a los filtros
     * @param filtros array con las llaves de la entidad, y los valores a buscar
     * @param orden el campo para ordenar, en llave de la entidad
     * @return constancias un arreglo con las constancias encontradas; vacio si no se encuentran
     */
    public function buscar($filtros, $orden = 'estudiante')
    {
        // TODO definir el orden de las constancias de acuerdo al segundo parámetro
        $this->creaFiltros($filtros, ['constancia_fecha_exp' => 'DESC', 'constancia_alumno' => 'ASC']);
        $constancias = $this->findAll();
        return $constancias;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     * @param orden un arreglo con los campos para ordenar (llave) y el tipo de orden (valor); opcional
     */
    private function creaFiltros($filtros, $orden = array())
    {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if (array_key_exists('alumno', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['alumno'])) {
                $this->where('constancia_alumno', $filtros['alumno']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['periodo'])) {
                $this->where('constancia_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('tipo', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['tipo'])) {
                $this->where('constancia_tipo', $filtros['tipo']);
            }
        }

        // despues definir el orden
        if (count($orden) === 0) {
            $this->orderBy('constancia_fecha_exp', 'DESC');
        } else {
            foreach ($orden as $campo => $tipo) {
                $this->orderBy(strtolower(trim($campo)), strtoupper(trim($tipo)));
            }
        }
    }

    /**
     * Obtiene el folio siguiente para un periodo determinado
     */
    public function getNextFolio($periodo, $tipo)
    {
        $folio = 1;
        $this->where('constancia_tipo', $tipo);
        $this->where('constancia_periodo', $periodo);
        $this->orderBy('constancia_folio', 'DESC');
        $constancia = $this->first();
        if ($constancia) {
            $folio = $constancia->folio + 1;
        }
        return $folio;
    }

    /**
     * obtiene las constancias de un estudiante
     * @param id el ID del estudiante para obtener las constancias
     */
    public function getEstudiante($id)
    {
        $this->creaFiltros(['alumno' => $id]);
        $constancias = $this->findAll();
        return $constancias;
    }

    /**
     * genera un registro de una constancia de estudios
     * @param alumno el ID del alumno
     * @param periodo el Id del periodo
     * @param tipo el tipo de constancia (opcional, inicialmente solo SIMPLE)
     * @return true en caso de que se pueda crear la constancia o de que ya exista
     * @return false en caso de que no se pueda crear
     */
    public function crea($alumno, $periodo, $tipo = 'SIMPLE')
    {
        $constancias = $this->buscar(['alumno' => $alumno, 'periodo' => $periodo, 'tipo' => $tipo]);
        if (count($constancias) > 0) {
            return true;
        }
        $folio = $this->getNextFolio($periodo,$tipo);
        try {
            $constancia = new Constancia();
            $constancia->alumno = $alumno;
            $constancia->periodo = $periodo;
            $constancia->folio = $folio;
            $constancia->tipo = $tipo;
            $constancia->fechaExp = date('Y-m-d');
            $this->insert($constancia);
            return true;
        } catch (\Exception $ex) {
            var_dump($ex);
            return false;
        }
    }
}
