<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <?php if (($errores)) : ?>
        <div class="alert alert-primary text-black-50">
            <?php echo $errores; ?>
        </div>
    <?php endif; ?>
    <?php if (($guardado)) : ?>
        <div class="alert alert-success text-black-50">
            Se han guardado correctamente las preguntas
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow mb-4 mt-4 ml-2 mr-2 ">
        <?php if ($evaluacion->tipo == 1) : ?>
            <div class="card-header">
                <?php echo $evaluacion->titulo ?>
            </div>
            <div class="row row-cols-1 row-cols-md-2 g-4 px-lg-4 py-3  ">
                <?php foreach ($materias as $materia) : ?>
                    <div class="col">
                        <div class="card">
                            <div class="card-body d-flex justify-content-between">
                                <div>
                                    <h5 class="card-title"><?php echo $materia->materia ?></h5>
                                    <div class="card-text">
                                        Docente: <?php echo $materia->docente_nombre ?>
                                        <br>
                                        Seccion: <?php echo $materia->seccion ?>
                                        <br>
                                        Grupo: <?php echo $materia->idc ?>
                                        <br>
                                        Preguntas: <?php echo "$materia->respuestas/$preguntas_totales";  ?>

                                    </div>
                                </div>
                                <div>

                                    <?php if ($materia->respuestas < $preguntas_totales) : ?>
                                        <a class="btn btn-secondary" href="<?php echo base_url('Evaluaciones/Materia/' . $evaluacion->id . '/' . $materia->idc) ?>" role="button">Contestar</a>
                                    <?php else : ?>
                                        <span>
                                            Contestado
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php echo $this->endSection() ?>