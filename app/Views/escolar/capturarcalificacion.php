<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<?php function buscacarrera()
{
    echo base_url('Escolar/buscacarrera');
}

$bandera = 'hidden';
?>

<style>
    .select-calif {
        width: 70%;
    }

    .main {
        display: flex;
        flex-direction: column;
        justify-content: center;
        background-color: #840f31;
        align-items: center;
    }

    .main select {
        background-color: whitesmoke;
        border-radius: 10px;
    }

    .input-error {
        border: 1px red solid !important;
    }

    .form_style {
        width: 100%;
        display: flex;
        padding-top: 15px;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .cal-in {
        padding: 3px;
        font-size: 18px;
        border-width: 1px;
        border-color: #CCCCCC;
        background-color: #FFFFFF;
        color: #000000;
        border-style: hidden;
        border-radius: 0px;
        box-shadow: 0px 0px 0px rgba(66, 66, 66, .0);
        text-shadow: 0px 0px 2px rgba(66, 66, 66, .70);
    }

    .cal-in:focus {
        outline: none;
    }

    .th-head {
        background-color: #840f31 !important;
        color: white !important;
        font-size: 0.875rem;
        text-transform: uppercase;
        letter-spacing: 2%;
    }
</style>
<div class="container my-sm-5">
    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <div>
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
            </symbol>
            <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
            </symbol>
            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </symbol>
        </svg>
        <?php if (isset($mensaje)) {
            if ($mensaje == 1) {
        ?>
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                        <use xlink:href="#check-circle-fill" />
                    </svg>
                    <div style="padding-left: 20px;">
                        ¡Registro realizado con exito!
                    </div>
                </div>
            <?php
            } elseif ($mensaje == 0) {
            ?>
                <div class="alert alert-warning d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:">
                        <use xlink:href="#exclamation-triangle-fill" />
                    </svg>
                    <div style="padding-left: 20px;">
                        Error de registro por duplicidad de datos.
                    </div>
                </div>

            <?php

            }
        } else {
            //echo 'bbbbbbbbb';
        }

        if (isset($grupo_flg_acta)) {
            if ($grupo_flg_acta == 1) {
            ?>
                <div class="alert alert-primary d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:">
                        <use xlink:href="#info-fill" />
                    </svg>
                    <div>
                        Grupo ya calificado, por favor recuerde pasar a Escolares a firmar su acta antes del día 30 de junio
                    </div>
                </div>
        <?php
            }
        }
        ?>

    </div>
    <div class="card">
        <div class="card-body text-left container-sm main text-white">
            <h5 class="text-lg-center"> Captura de Calificaciones</h5>

            <form class="form text-center form_style" method="post" action="<?php echo base_url('Escolar/buscagrupo'); ?>" enctype="multipart/form-data">
                <div class="form-group select-calif text-center col-sm-12">
                    <label for="select-grupo" style="font-size: 20px; padding-right: 10px">Grupo</label>
                    <input type="hidden" name="docente" id="docente" value="<?php echo $docente; ?>" ;>
                    <select id="grupo " class="custom-select" style="width: 80%;" name="grupo" onchange="this.form.submit();">
                        <option selected>Selecciona grupo</option>
                        <?php foreach ($grupo as $grupos) : ?>
                            <option value = <?php echo $grupos->grupo ?> <?php if($idgrupo == $grupos->grupo) echo 'selected="selected"'; ?>><?php
                                    echo $grupos->grupo_clave . " - " . $grupos->materia_nombre . " (" . $grupos->materia_clave . ")" . $grupos->grupo_flag_acta;
                                    ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

            </form>

        </div>


        <form method="post" id="contenedor2" action="<?php echo base_url('Escolar/capturaCalificacionesporGrupo'); ?> ">
            <input type="hidden" name="folio" value="<?php if (isset($folio)) {
                                                            echo $folio;
                                                        }  ?>">
            <div>
                <table name='calif_arr' class="text-center text-white" style="background-color: #840f31; width: 100%; border-top: 2px solid white;">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 120px; ">IDC</th>
                            <th style="width: 120px; ">PERIODO</th>
                            <th style="width: 120px; ">SECCIÓN</th>
                            <th style="width: 300px; ">CARRERA</th>
                            <th style="width: 300px; ">MATERIA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td><?php echo $idc ?></td>
                        <td><?php echo $periodo; ?></td>
                        <td><?php echo $seccion; ?></td>
                        <td><?php echo $carrera; ?></td>
                        <td><?php echo $informacion; ?></td>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table name='calif_arr' class="table table-light text-center">
                    <thead>
                        <tr>
                            <th class="th-head">No.</th>
                            <th class="th-head">Matricula</th>
                            <th class="th-head">Nombre</th>
                            <th class="th-head">Ordinario</th>
                            <th class="th-head">Extraordinario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <input name="grupo_clave" hidden id="grupo_clave" value="<?php echo $grupo_clave; ?>" ;>
                        <?php
                        $calificacion = null;
                        $tipoCalificacion = null;

                        ?>
                        <?php
                        if (!$grupo_clave) {
                            $bandera = ' hidden ';
                        } else {
                            $bandera = null;
                        }
                        if ($grupo_flg_acta == 1) {
                            $validacion = ' disabled ';
                            $bandera = ' hidden ';
                        } else {
                            $validacion = '';
                        }
                        $contador = 0;
                        foreach ($alumno as $alumnos) : ?>
                            <tr>
                                <td><?php echo $contador = $contador + 1; ?></td>
                                <td><?php
                                    if ($alumnos->gpoalumno_flg_pago == 0) {

                                        echo "<input name='idalumno[]' id='idalumno[]' value='$alumnos->id_alumno' hidden>";
                                    }
                                    echo $alumnos->id_alumno; ?></td>
                                <td class="text-left"><?php echo $alumnos->alumno_ap_paterno . " " . $alumnos->alumno_ap_materno . " " . $alumnos->nombres ?></td>
                                <?php
                                if ($alumnos->gpo_ordinario == 0) {
                                    $calificacion = null;
                                } else {
                                    $calificacion = $alumnos->gpo_ordinario;
                                }

                                if ($alumnos->gpoalumno_flg_pago == null || $alumnos->gpoalumno_flg_pago == 0) {
                                    echo "";
                                    echo "<td><input class='form-control text-center cal-in' type='number' name='calif[]' id='calif'  min='5' max='10' type='text' value='$calificacion' $validacion required></td>";
                                    echo "<td></td>";
                                }
                                if ($alumnos->gpoalumno_flg_pago == 1) {
                                    echo "";
                                    echo "<input name='idalumnoextra[]' id='idalumnoextra[]' value='$alumnos->id_alumno' hidden>";
                                    echo "<td class='text-center'> 5 </td>";
                                    echo "<td><input class='form-control text-center cal-in' type='number' name='califextra[]' id='extra' type='text' value='$alumnos->gpo_extraordinario' min='5' max='8' $validacion required ></td>";
                                }

                                ?>
                            </tr>
                        <?php endforeach; ?>



                    </tbody>

                </table>
            </div>

            <div class="card-footer text-center">
                <button type="button" id="btn2" class="btn btn-primary" style="background-color: #883232 ;" <?php echo $bandera;
                                                                                                            echo $validacion; ?>>
                    <input name="idc_grupo" type="hidden" value="<?php echo $idc; ?>" />
                    Guardar
                </button>
                <?php $bandera = null; ?>
            </div>

        </form>
    </div>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    'use strict';
    const btn_guardar = document.querySelector('#btn2');
    let validar = () => {
        //let inputs_requeridos = document.querySelectorAll('#contenedor2 [required]');
        let inputs_requeridos = document.querySelectorAll("input[id=calif]");
        let input_extra = document.querySelectorAll("input[id=extra]");
        let error_empty = false;

        for (let j = 0; j < input_extra.length; j++) {
            if (input_extra[j].value == '' || input_extra[j].value > 8 || input_extra[j].value < 5) {
                console.log(j);
                input_extra[j].classList.add('input-error');
                error_empty = true;
            } else {
                input_extra[j].classList.remove('input-error');
            }
        }

        for (let i = 0; i < inputs_requeridos.length; i++) {
            if (inputs_requeridos[i].value == '' || inputs_requeridos[i].value > 10 || inputs_requeridos[i].value < 5) {
                inputs_requeridos[i].classList.add('input-error');
                error_empty = true;
            } else {
                inputs_requeridos[i].classList.remove('input-error');
            }
        }
        console.log('hola');
        return error_empty;
    };
    let obtener_datos = () => {
        let flg_validar = validar();
        if (flg_validar) {
            Swal.fire({
                'title': 'ERROR',
                'text': 'Por favor revise los campos marcados en rojo ',
                'icon': 'error',
                confirmButtonColor: '#883232',


            });
        } else {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! No podrá realizar cambios posteriores una vez que continue ¿Está seguro que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('contenedor2').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }

    }
    btn_guardar.addEventListener('click', obtener_datos);
</script>
<!-- 
    <script src="/app/Views/escolar/validacion.js"></script>

    <script>
        function myFunction() {
            confirm("¡¡¡ATENCIÓN !!! \n No se podrá volver a modificar las calificaciones una vez que confirme el envío, ¿está seguro de que desea continuar? ");
        }
    </script>
-->
<?php echo $this->endSection() ?>