<?php
namespace App\Entities;

use CodeIgniter\Entity;

class PagoAlumno extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'alumno' => null,
        'concepto' => null,
        'referencia' => null,
        'status' => null,
        'monto' => null,
        'conceptoTexto' => null,
        'fecha' => null,
        'comprobante' => null,
        'tipo' => null,
        'observaciones' => null,
        'materia' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'pago_id',
        'alumno'        => 'pago_alumno',
        'concepto'      => 'pago_concepto',
        'referencia'    => 'pago_referencia',
        'status'        => 'pago_status',
        'monto'         => 'pago_monto',
        'conceptoTexto' => 'pago_concepto_texto',
        'fecha'         => 'pago_fecha',
        'comprobante'   => 'pago_comprobante',
        'tipo'          => 'pago_tipo',
        'materia'       => 'pago_materia',
        'observaciones' => 'pago_observaciones',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
