<?php
namespace App\Models;
use CodeIgniter\Model;
use \App\Entities\AsistenciasAlumnos;

class AsistenciasAlumnosModel extends Model {
    protected $table      = 'view_asistencias_alumnos';
    protected $primaryKey = 'asistencia_grupo';

    protected $returnType    = 'App\Entities\AsistenciasAlumnos';
    protected $useSoftDeletes = false;

    protected $allowedFields = [

    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function buscaasistencias($fecha,$iddocente)
    {
        $asistencias = $this->where(['asistencia_docente'=>$iddocente, 'asistencia_fecha'=>$fecha])->findAll();
        return $asistencias;
    }
    public function buscaasistenciasxgrupo($fecha,$iddocente,$id_grupo)
    {
        $asistencias = $this->where(['asistencia_docente'=>$iddocente, 'asistencia_fecha'=>$fecha, 'asistencia_grupo'=>$id_grupo])->findAll();
        return $asistencias;
    }
    public function grupo($fechainicio,$fechafin,$iddocente,$grupo)
    {
        return $this->where('asistencia_docente',$iddocente)->where('grupo_clave',$grupo)
        ->where("asistencia_fecha <= '$fechafin 23:59:59'")->where("asistencia_fecha >= '$fechainicio'")
        ->orderBy('alumno_ap_paterno','Asc')->orderBy('alumno_ap_materno','Asc')->orderBy('alumno_nombres','Asc')->orderBy('asistencia_fecha','Asc')
        ->groupBy('alumno_id')->find();

    }
    public function asistenciasReporte($fechainicio,$fechafin,$iddocente,$grupo)
    {
        return $this->where('asistencia_docente',$iddocente)->where('grupo_clave',$grupo)->where("asistencia_fecha <= '$fechafin 23:59:59'")
        ->where("asistencia_fecha >= '$fechainicio'")
        ->orderBy('alumno_ap_paterno','Asc')->orderBy('alumno_ap_materno','Asc')->orderBy('alumno_nombres','Asc')->orderBy('asistencia_fecha','Asc')->find();
    }
} 