<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }

    .formulario-cabecera,
    .formulario-datos {
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }
</style>
<br>
<?php if (isset($mensaje)) : ?>
    <div class="alert <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
    </div>
<?php endif; ?>
<?php if (isset($configuradoretapa)) : ?>
    <div class="container">
        <?php if (isset($bandera_updateetapa)) : ?>
            <form action="modificaretapaexistente" method="post">
                <input type="hidden" name="etapa-identificador" value="<?php echo $identificacdor ?>">
            <?php else : ?>
                <form action="generaretapa" method="post">
                <?php endif; ?>
                <div class="card">
                    <div class="card-header">
                        <h4>Configurardor de Etapas</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <div>
                                <label for="select-periodo" class="form-label">Selecciona el Periodo de la Etapa</label>
                                <select class="form-select" name="select-periodo" id="select-periodo" required>
                                    <?php if (!isset($periodo_selected)) : ?>
                                        <option selected value="0">Selecciona un Periodo</option>
                                    <?php else : ?>
                                        <option value="0">Selecciona un Periodo</option>
                                    <?php endif;  ?>
                                    <?php foreach ($periodos as $periodo) : ?>
                                        <?php if (isset($periodo_selected)) : ?>

                                            <?php if ($periodo->periodo_id == $periodo_selected) : ?>
                                                <option selected value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                            <?php else :  ?>
                                                <option value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                            <?php endif;  ?>

                                        <?php else : ?>
                                            <option value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                        <?php endif;  ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <br>
                            <div>

                                <label for="nombre-etapa" class="form-label">Nombre de la Etapa</label>
                                <?php if (isset($nombre_selected)) : ?>
                                    <input type="text" class="form-control" name="nombre-etapa" id="nombre-etapa" aria-describedby="helpId" required value="<?php echo $nombre_selected ?>">
                                <?php else : ?>
                                    <input type="text" class="form-control" name="nombre-etapa" id="nombre-etapa" aria-describedby="helpId" required>
                                <?php endif; ?>
                                <small id="helpId" class="form-text text-muted">Ingrese el nombre que se le asignara a la Etapa</small>
                            </div>
                            <br>
                            <div>
                                <label for="orden-etapa" class="form-label">Numero de Secuencia</label> <br>
                                <?php if (isset($secuencia_selected)) : ?>
                                    <input class="form-control" type="number" name="orden-etapa" id="orden-etapa" required value="<?php echo $secuencia_selected ?>">
                                <?php else : ?>
                                    <input class="form-control" type="number" name="orden-etapa" id="orden-etapa" required value="<?php echo $conta_etapas ?>">
                                <?php endif; ?>
                                <small id="helpId" class="form-text text-muted">Elegir el numero de secuencia </small>
                            </div>
                            <br>
                            <div class="verificacion">

                                <div class="form-check form-switch">
                                    <?php if (isset($vinculado_selected)) : ?>
                                        <input class="form-check-input" type="checkbox" id="form-vinculado" name="form-vinculado" value="true" checked>
                                    <?php else : ?>
                                        <input class="form-check-input" type="checkbox" id="form-vinculado" name="form-vinculado" value="true">
                                    <?php endif; ?>
                                    <label class="form-check-label" for="form-vinculado">Esta etapa tiene algun formulario vinculado?</label>
                                </div>
                                <div class="form-check form-switch">
                                    <?php if (isset($responsable_selected)) : ?>
                                        <input class="form-check-input" type="checkbox" id="responsabilidad-rol" name="responsabilidad-rol" value="true" checked>
                                    <?php else : ?>
                                        <input class="form-check-input" type="checkbox" id="responsabilidad-rol" name="responsabilidad-rol" value="true">
                                    <?php endif; ?>
                                    <label class="form-check-label" for="responsabilidad-rol">Esta etapa es responsable del aspirante?</label>
                                </div>
                            </div>
                            <br>

                        </div>

                    </div>
                    <div class="card-footer text-muted text-center">
                        <?php if (isset($bandera_updateetapa)) : ?>
                            <button type="submit" class="btn btn-secondary">Modificar Etapa</button>
                        <?php else : ?>
                            <button type="submit" class="btn btn-secondary">Agregar Etapa</button>
                        <?php endif; ?>
                        <button class="btn btn-secondary" type="button" id="cancelar">Cancelar</button>

                    </div>
                </div>
                </form>
    </div>
    <form action="DiseñadordeModelo" method="post" id="home-fomr">
        <input type="hidden" name="home-back" value="true">
    </form>
    <script>
        'use strict';
        const btn_guardar = document.querySelector('#cancelar');
        let regresarhome = () => {
            document.getElementById('home-fomr').submit()
        }
        btn_guardar.addEventListener('click', regresarhome);
    </script>
<?php endif; ?>
<?php echo $this->endSection() ?>