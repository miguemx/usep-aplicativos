<?php echo $this->include('menus/_inicio'); ?>
    


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url() ?>">Inicio</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Estudiantes
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/Estudiantes') ?>">Listado</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/secciones') ?>">Cambio de Sección</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/BajadeMateria') ?>">Baja de Sección por Matricula</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/AltaMateria') ?>">Altas de Sección por Matricula</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/EstudiantesBaja') ?>">Alumnos no activos</a></li>
            <!-- 
              <li><a class="dropdown-item" href="<?php // echo base_url('Escolar/historialalumnos') ?>">Historial de Materias Reprobadas</a></li>
            -->
            <li><hr class="dropdown-divider"></li>
            <!--<li><a class="dropdown-item" href="<?php //echo base_url('Escolar/CapturaCalificaciones') ?>">Capturar una calificación</a></li>-->
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/ActualizacionDeCalificaciones') ?>">Modificar calificaciones</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/ActasDeCalificaciones') ?>">Actas de Calificaciones</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/ReporteCalificaciones') ?>">Reportes de Calificaciones</a></li>
            <!-- 
              
              <li><a class="dropdown-item" href="<?php  echo base_url('Escolar/generadogrupos') ?>">Generador de Grupos</a></li>
              -->
        <li>
          <hr class="dropdown-divider">
        </li>
        <!-- <li><a class="dropdown-item" href="<?php // echo base_url('Escolar/CargarCalificaciones') ?>">Horarios</a></li> -->
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/listaescolar') ?>">Lista de grupos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/DatosRIPEL') ?>">Reporte RIPEL</a></li>
        <hr class="dropdown-divider">
        <li><a class="dropdown-item" href="<?php echo base_url('Credenciales') ?>">Credenciales</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('SeguroFacultativo/Estudiantes') ?>">Seguro Facultativo</a></li>
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Admisión
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('ControlAdmision/DiseñadordeModelo') ?>">Administración del proceso</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('ControlAdmision/ListadoAspirantes') ?>">Lista de Aspirantes</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="<?php echo base_url('EstadisticasAdmision/MenuEstadisticas') ?>">Estadisticas por Respuestas</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="<?php echo base_url('ControlAdmision/InformacionGeneral') ?>">Descarga de información</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Inscripcion') ?>">Inscripción</a></li>                
      </ul>    
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Docente
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/CapturaCalificaciones') ?>">Captura de calificaciones</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/Horario'); ?>">Horarios</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/bitacoraAulas'); ?>">Bitacora de Aulas</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/consultahorario'); ?>">Captura de asistencias</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/horarioGrupos'); ?>">Modificación de asistencias o captura extemporanea</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/resportesAsistencias'); ?>">Reporte de asistencias</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('SubdireccionAcademica/paseListaDocentes') ?>">Pase de Lista Global</a></li>
        <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Grupos'); ?>">Listas de Asistencia</a></li>-->
        <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Calificaciones'); ?>">Calificaciones</a></li>-->
        <!--<li><hr class="dropdown-divider"></li>-->
        <!--<li><a class="dropdown-item" href="<?php echo base_url('CapitalHumano/Docentes'); ?>">Listado de Docentes</a></li>-->
      </ul>
    </li>

    <!-- <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Capital Humano
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php // echo base_url('CapitalHumano/Personal'); ?>">Personal</a></li>

        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php // echo base_url('CapitalHumano/Cargar'); ?>">Subir registros</a></li>
      </ul>
    </li> -->

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Pagos
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Pagos'); ?>">Ver cuotas de estudiantes</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Pagos/CargaRos'); ?>">Cargar archivo ROS</a></li>
      </ul>
    </li>


    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Academia
      </a>
      <ul class="dropdown-menu mx-1" aria-labelledby="navbarDropdown">
        <div class="m-2">

          <strong>
            <span>Programación Academica</span>
          </strong>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/GeneradorPA'); ?>">Generador de Programacion Academica</a></li>
          <!-- <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/ConfiguradorPC'); ?>">Configurador de Prácticas Clínicas</a></li> -->
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/Menu'); ?>">Mapa de Aulas</a></li>
          <!-- <li><a class="dropdown-item" href="<?php // echo base_url('ProgramacionAcademica/PracticasClinicas/'); ?>">Mapa de Prácticas Clínicas</a></li> -->
          <!-- <li><a class="dropdown-item" href="<?php // echo base_url('ProgramacionAcademica/AulaVirtual/'); ?>">Listas de Grupos Virtuales</a></li> -->
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/MenuDocentes/'); ?>">Horario por Docentes</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/HorarioSecciones/'); ?>">Horario por Secciones</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/EspaciosDisponibles/'); ?>">Espacios Disponibles</a></li>
          <strong>
            <span>Evaluaciones</span>
          </strong>
          <li><a class="dropdown-item" href="<?php echo base_url('EvaluacionDocenteController/Menu/'); ?>">Evaluación Docente</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('EstadisticasEvaluacionDocente/Estadisticos/'); ?>">Estadísticos de Evaluación</a></li>
        </div>
      </ul>
    </li>
    <!-- <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Horarios Generales
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php // echo base_url('Docente/BuscarHorarios'); ?>">Buscar Horarios</a></li>
      </ul>
    </li> -->
        <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Horarios Generales
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?php // echo base_url('Docente/BuscarHorarios'); ?>">Buscar Horarios</a></li>
          </ul>
        </li> -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Voto electrónico
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
          <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/procesos'); ?>">Registro de procesos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/candidatos'); ?>">Registro de candidatos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/padron'); ?>">Registro de padrón</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/gestionprocesos'); ?>">Gestión de procesos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/estadisticas'); ?>">Consulta de estadísticas</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Herramientas
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="https://mail.google.com/" target="_blank">Mi correo</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Bibliotecas') ?>">Bibliotecas</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="http://172.10.8.207/" target="_blank">Gestor de Proyectos</a></li>
            <li><a class="dropdown-item" href="http://172.10.9.212:8080/" target="_blank">Gestor Documental</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Filtro/Consulta') ?>">Filtro Sanitario</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Login/Logout'); ?>">Cerrar Sesión</a></li>
          </ul>
        </li>
      </ul>
      <div style="color: #fff;">
            Bienvenido
            <?php if ( !is_null(session('nombre')) ):
                echo session('nombre');
            endif;  
            ?>
      </div>
    </div>



<?php echo $this->include('menus/_fin'); ?>