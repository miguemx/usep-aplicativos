<?php
namespace App\Models;
use CodeIgniter\Model;

class GruposHorariosModel extends Model {
    protected $table      = 'view_grupos_horarios';
    protected $primaryKey = 'gpoaula_grupo';

    protected $returnType    = 'App\Entities\GruposHorarios';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }   

    public function busca($filtros)
    {
        $this->creaFiltros($filtros);
        $grupos = $this->findAll();
        return $grupos;
    }
    /**
     * crea los filtros where para la busqueda de grupos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('dia', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['dia'])) {
                $this->where('gpoaula_dia', $filtros['dia']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['periodo'])) {
                $this->where('gpoaula_periodo', $filtros['periodo']);
            }
        }
    }


    /**
     * @return query arreglo de horario de los grupos impartidos en el periodo seleccionado
     * @param periodo id del periodo al que pertenecen los grupos
     * @param aula id del aula
     */
    public function getGruposAula($periodo,$aula)
    {
        return $this->where('gpoaula_aula',$aula)->where('gpoaula_periodo',$periodo)->find();
    }

    /**
     * @return query contiene el arreglo de materias con respecto al periodo que se requiere
     */
    public function getByPeriodo($periodo)
    {
        return $this->where('gpoaula_periodo',$periodo)->find();
    }
    
    /**
     * @return query arreglo de grupos en un periodo de tiempo seleccionado
     */
    public function getGruposAulasDiaHora($aula,$dia,$hora_inicio,$periodo)
    {
        return $this
        ->where('gpoaula_aula',$aula)
        ->where('gpoaula_dia',$dia)
        ->where('gpoaula_inicio',$hora_inicio)
        ->where('gpoaula_periodo',$periodo)
        ->find()
        // ->getCompiledSelect()
        ;
    }

    /**
     * @param periodo ID del periodo de busqueda
     * @param docente  ID del docente
     * @return query compilado de grupos que son impatidos por el docente 
     */
    public function getGruposByDocente($periodo,$docente = false)
    {
        $query = $this->where('gpoaula_periodo',$periodo)->where('grupo_docente',$docente)->find();
        if(count($query)){
            return $query;
        }
        return null;
    }

    /**
     * retorna el conjunto de aulas que tienen al menos una materia en un periodo de tiempo definido en un dia
     * @param dia 
     * @param hora_inicio
     * @param hora_fin
     * @param periodo
     */
    public function getAulasDia($dia,$hora_inicio,$hora_fin,$periodo)
    {
        return $this->where('gpoaula_dia',$dia)->where('gpoaula_inicio >=',$hora_inicio)->where('gpoaula_fin <=',$hora_fin)->where('gpoaula_periodo',$periodo)->find();
    }

    /**
     * retorna lista de los dias y horas de un grupo en especifico
     * @param grupo ID del grupo seleccionado
     */
    public function getByIDC($grupo)
    {
        return $this->where('gpoaula_grupo',$grupo)->find();
    }

     /**
     * @param periodo ID del periodo de busqueda
     * @param seccion  ID de seccion
     * @return query compilado de grupos que tiene asignada la seccion 
     */
    public function getGruposBySeccion($periodo,$seccion)
    {
        return $this->where('gpoaula_periodo',$periodo)->where('grupo_clave',$seccion)->find();
    }

    /**
     * retorna las horas de clase segun el dia y el aula selceccionada 
     * @param periodo ID del periodo
     * @param dia dia seleccionado
     * @param aula ID del aula
     */
    public function getByAulaDia($periodo,$dia,$aula)
    {
        return $this->where('gpoaula_periodo',$periodo)->where('gpoaula_dia',$dia)->where('gpoaula_aula',$aula)->find();
    }

    /**
     * @param grupo
     * @param dia 
     * @param hora_incio
     * @param hora_fin
     */
}
