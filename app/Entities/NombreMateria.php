<?php
namespace App\Entities;

use CodeIgniter\Entity;

class NombreMateria extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'grupo_id' => null,
        'empleado_id' => null,
        'empleado_email' => null,
        'docente' => null,
        'docente_ap' => null,
        'periodo' => null,
        'materia' => null,
        'carrera' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'grupo_id'=>'grupo_id',
        'empleado_id' =>'empleado_id',
        'empleado_email'=>'empleado_correo',
        'docente'=>'empleado_nombre',
        'docente_ap'=>'empleado_apellido',
        'periodo'=>'periodo_id',
        'materia'=>'materia_nombre',
        'carrera'=>'carrera_nombre',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
