<style type="text/css">
    *, div, p {
        font-size: 1rem;
        font-family: Arial, Helvetica, sans-serif;
    }
    p {
        text-align: justify;
        margin: 15px 25px;
    }
    td {
        border-bottom: 1px solid #555;
        border-right: 1px solid #555;
        border-top: 1px solid #555;
        border-left: 1px solid #555;
        padding: 5px;
        font-family: Arial, Helvetica, sans-serif;
    }
</style>


<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta de adeudo de documentos
</p>
<p>
    El(la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente inscrito(a) 
    en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id ?></u></strong>, en 
    acepto que en cumplimiento al Artículo 11 del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD 
    DE LA SALUD DEL ESTADO DE PUEBLA (RIPEA), me doy por enterado(a) y me comprometo, a que con fecha límite del <strong>20 de enero del 2023</strong>, 
    entregaré el(los) siguiente(s) documento que adeudo ante la “Universidad de la Salud” del Estado de Puebla:
</p>

<div style="margin: 0 25%";>
    <table cellpadding="0" cellspacing="0">
        <?php if($reldocumental->acta == '0'): ?>
            <tr>
                <td>&nbsp;</td>
                <td>Extracto de Acta de Nacimiento actualizado a 2022</td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->curp == '0'): ?>
            <tr>
                <td>&nbsp;</td>
                <td>Clave Única de Registro de Población (CURP) actualizada a 2022</td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->certificado == '0' && $reldocumental->legalizado == '0'): ?>
            <tr>
                <td>&nbsp;</td>
                <td>Certificado de Estudios concluidos de preparatoria / bachillerato, <strong>legalizado</strong></td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->identificacion == '0'): ?>
            <tr>
                <td>&nbsp;</td>
                <td>Identificación oficial</td>
            </tr>
        <?php endif; ?>
    </table>
</div>

<p >
    De conformidad con los Artículos 40, inciso d y 41 inciso b del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA 
    UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA (RIPEA), ACEPTO que, al no dar cumplimento a la entrega de los documentos señalados, a más 
    tardar en la fecha límite establecida, la “Universidad de la Salud” del Estado de Puebla, procederá a la suspensión de mis derechos y 
    posteriormente a causar baja institucional definitiva sin perjuicio para la Institución.
</p>
<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('<?php echo base_url('img/inscripcion/marcadseyt.jpg')?>'); background-position: 94% 0%;">
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px;">
        <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>


<div>
    <img src="<?php echo base_url('/img/inscripcion/footerdocs.jpg') ?>" />
</div>

<hr style="border 1px dotted #444;" />

<div>
    <img src="<?php echo base_url('/img/inscripcion/headerdocs.jpg') ?>" />
</div>
<p style="text-align: center; font-weight: bold; text-align: center; font-size: 0.65rem;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Carta de Adeudo<br />
</p>
<p style="text-align: justify; margin: 5px 20px; font-size: 0.65rem;">
    El(la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente inscrito(a) 
    en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id ?></u></strong>, en 
    acepto que en cumplimiento al Artículo 11 del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA UNIVERSIDAD 
    DE LA SALUD DEL ESTADO DE PUEBLA (RIPEA), me doy por enterado(a) y me comprometo, a que con fecha límite del <strong>20 de enero del 2023</strong>, 
    entregaré el(los) siguiente(s) documento que adeudo ante la “Universidad de la Salud” del Estado de Puebla:
</p>
<div style="margin: 0 25%";>
    <table cellpadding="0" cellspacing="0">
        <?php if($reldocumental->acta == '0'): ?>
            <tr>
                <td style="font-size: 0.6rem; padding: 1px;">&nbsp;</td>
                <td style="font-size: 0.6rem; padding: 1px;">Extracto de Acta de Nacimiento actualizado a 2022</td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->curp == '0'): ?>
            <tr>
                <td style="font-size: 0.6rem; padding: 1px;">&nbsp;</td>
                <td style="font-size: 0.6rem; padding: 1px;">Clave Única de Registro de Población (CURP) actualizada a 2022</td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->certificado == '0' && $reldocumental->legalizado == '0'): ?>
            <tr>
                <td style="font-size: 0.6rem; padding: 1px;">&nbsp;</td>
                <td style="font-size: 0.6rem; padding: 1px;">Certificado de Estudios concluidos de preparatoria / bachillerato, <strong>legalizado</strong></td>
            </tr>
        <?php endif; ?>
        <?php if($reldocumental->identificacion == '0'): ?>
            <tr>
                <td style="font-size: 0.6rem; padding: 1px;">&nbsp;</td>
                <td style="font-size: 0.6rem; padding: 1px;">Identificación oficial</td>
            </tr>
        <?php endif; ?>
    </table>
</div>
<p  style="text-align: justify; margin: 5px 20px;  font-size: 0.65rem;">
De conformidad con los Artículos 40, inciso d y 41 inciso b del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA 
    UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA (RIPEA), ACEPTO que, al no dar cumplimento a la entrega de los documentos señalados, a más 
    tardar en la fecha límite establecida, la “Universidad de la Salud” del Estado de Puebla, procederá a la suspensión de mis derechos y 
    posteriormente a causar baja institucional definitiva sin perjuicio para la Institución.
</p>
<div style="padding: 0 25%; ">
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px; font-size: 0.65rem;">
        <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>
