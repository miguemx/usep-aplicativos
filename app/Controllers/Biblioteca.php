<?php

namespace App\Controllers;

use App\Models\AlumnosModel;
use App\Models\EmpleadosModel;
use App\Models\CredencialModel;
use App\Helpers\AccesosBiblioteca;


/**
 * Contiene las gestiones administrativas de la biblioteca
 */
class Biblioteca extends BaseController
{

    private $alumnosModel;
    private $empleadosModel;

    public function __construct() {
        $this->alumnosModel = new AlumnosModel();
        $this->empleadosModel = new EmpleadosModel();
    }

    public function index() {
        $data['menu'] = $this->sesion->get('menu');
        return view('bibliotecas/registroacceso', $data);
    }

    /**
     * registra un acceso a biblioteca
     * @param id el ID que ingresa
     */
    public function acceso($id) {
        $accesosBiblioteca = new AccesosBiblioteca();
        $registro = $accesosBiblioteca->registra( $id );
        if ( $registro !== false ) {
            $data['tipo'] = ( is_null($registro->salida) )? 'entrada': 'salida';
            $datos = $this->getDatosPersona( $registro->alumno, $registro->empleado );
            if ( $datos !== false ) {
                $data['datos'] = $datos;
            }
        }
        else {
            $data['error'] = $accesosBiblioteca->getLastError();
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('bibliotecas/registroacceso', $data);
    }

    /**
     * Obtiene los datos personales de quien accede a biblioteca
     * dependiendo de su tipo y ID; se pasan dos parámetros y uno puede ser null
     * @param matricula el ID del alumno (puede ser null)
     * @param id el ID de empleado (puede ser null)
     * @return false en caso de no encontrar los datos
     * @return datos un arreglo con los datos
     */
    private function getDatosPersona( $matricula, $id ) {
        $datos = false;
        if( !is_null($matricula) ) {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find( $matricula );
            if ( !is_null($alumno) ) {
                $fotosrc = null;
                try {
                    $credencialModel = new CredencialModel();
                    $credencial = $credencialModel->find($alumno->id);
                    $path = WRITEPATH . '/estudiantes/' . $credencial->alumno . '/' . $credencial->foto;
                    $foto = fopen( $path, 'r');
                    $cadena = fread( $foto, filesize($path) );
                    $fotosrc = "data:" . mime_content_type($foto) . ";base64," . base64_encode($cadena);
                }
                catch(\Exception $ex) {
                    log_message( 'error', 'ERROR {ex}', [ 'ex'=>$ex ] );
                }
                $datos = [
                    'id' => $alumno->id,
                    'nombre' => $alumno->nombres.' '.$alumno->apPaterno.' '.$alumno->apMaterno,
                    'foto' => $fotosrc
                ];
            }
        }
        else if( !is_null($id) ) {
            $empleadosModel = new EmpleadosModel();
            $empleado = $empleadosModel->find( $id );
            $datos = [
                'id' => $empleado->id,
                'nombre' => $empleado->nombre.' '.$empleado->apPaterno.' '.$empleado->apMaterno,
                'foto' => null
            ];
        }
        return $datos;
    }

    /**
     * muestra el listado del aforo actual en la biblioteca
     */
    public function aforo() {
        $accesosBiblioteca = new AccesosBiblioteca();
        $data['registros'] = $accesosBiblioteca->aforo();
        $data['menu'] = $this->sesion->get('menu');
        
        if ( $msg = $this->sesion->getFlashdata('msg') ) {
            $data['message'] = $msg;
        }
        if ( $err = $this->sesion->getFlashdata('err') ) {
            $data['error'] = $err;
        }
        return view('bibliotecas/aforo', $data);
    }

    /**
     * 
     */
    public function entrada() {
        $data['menu'] = $this->sesion->get('menu');
        return view('bibliotecas/entrada', $data);
    }

    /**
     * ejecuta una consulta para marcar salidas a todos los usuarios dentro de la biblioteca
     */
    public function cerrarBiblioteca() {
        $accesosBiblioteca = new AccesosBiblioteca();
        $accesosBiblioteca->cierra();
        return redirect()->to('Biblioteca/Aforo');
    }

    /**
     * registra la salida de un folio en específico
     */
    public function marcarSalida() {
        $id = $this->request->getPost('folio');
        $ref = $this->request->getPost('ref');
        if ( $ref === 'aforo' ) {
            $accesosBiblioteca = new AccesosBiblioteca();
            if ( $accesosBiblioteca->registra($id) ) {
                $this->sesion->setFlashdata('msg', 'Se ha registrado la salida correctamente.');
            }
            else {
                $this->sesion->setFlashdata('err', 'Ocurrió un error al registrar la salida.');
            }
        }
        return redirect()->to('Biblioteca/Aforo');
    }

    /**
     * lista el reporte de accesos
     */
    public function reporteAccesos() {
        $accesosBiblioteca = new AccesosBiblioteca();
        $data['inicio'] = ( is_null($this->request->getGet('inicio')) )? '': $this->request->getGet('inicio');
        $data['fin'] = ( is_null($this->request->getGet('fin')) )? '': $this->request->getGet('fin');
        $data['registros'] = $accesosBiblioteca->reporte( $this->request->getGet() );
        $data['menu'] = $this->sesion->get('menu');
        return view('bibliotecas/reporte', $data);
    }

    /**
     * lista el reporte de accesos
     */
    public function reporteAccesosDescarga() {
        $accesosBiblioteca = new AccesosBiblioteca();
        $registros = $accesosBiblioteca->reporte( $this->request->getPost() );
        $filename = 'AccesosBiblioteca_'.date("Ymd_His").'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; charset=us-ascii");
        $file = fopen("php://output","w");
        $head = [ 'Entrada', utf8_decode('ID o Matrícula'), 'Tipo', 'Nombre', 'Carrera', 'Sexo', 'Edad', 'Salida' ];
        fputcsv( $file, $head );
        foreach ( $registros as $registro ) {
            $nombre = '';
            if( is_null($registro->alumno) ){
                $nombre =  $registro->empleadoNombre.' '.$registro->empleadoApPaterno.' '.$registro->empleadoApMaterno;
            }
            else {
                $nombre =  $registro->alumnoNombre.' '.$registro->alumnoApPaterno.' '.$registro->alumnoApMaterno;
            }
            $id = ( is_null($registro->alumno) )? $registro->empleado: $registro->alumno;
            $data = [
                $registro->entrada,
                $id,
                $registro->tipo,
                utf8_decode($nombre),
                $registro->carrera,
                $registro->sexo,
                $registro->edad,
                $registro->salida
            ];
            fputcsv( $file, $data );
        }
        fclose( $file );
        exit;
    }
    
}
