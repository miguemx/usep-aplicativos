CREATE VIEW view_aspirantes_procedencia AS SELECT
    aspirantes_generales.aspirante_id AS aspirante_folio,
    aspirantes_generales.aspirante_municipio AS municipio_id,
    municipios.nombre AS municipio_nombre,
    municipios.estado_id AS estado_id,
    estados.nombre as estado_nombre
FROM
    aspirantes_generales
LEFT JOIN municipios ON municipios.id = aspirantes_generales.aspirante_municipio
LEFT JOIN estados ON estados.id = municipios.estado_id;