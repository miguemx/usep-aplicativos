<?php

namespace App\Controllers;

use App\Models\CredencialModel;
use App\Models\AlumnosModel;
use App\Models\CarrerasModel;
use App\Models\ConstanciasModel;

use \Endroid\QrCode\Builder\Builder;
use \Endroid\QrCode\Encoding\Encoding;
use \Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use \Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use \Endroid\QrCode\Label\Font\NotoSans;
use \Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use \Endroid\QrCode\Writer\PngWriter;
use App\Models\InsertFiltroModel;
use App\Models\KardexModel;
use Faker\Extension\Helper;
use App\Helpers\CodigoQr;
use App\Models\PeriodoModel;

class E extends BaseController
{

    /**
     * valida la credencial de un estudiante
     */
    public function v($matricula)
    {
        $credencialModel = new CredencialModel();
        $credencial = $credencialModel->find($matricula);
        $data['error'] = '404.';
        if (!is_null($credencial)) {
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find($credencial->alumno);
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            if (!is_null($alumno)) {
                $data['nombre'] =  $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres;
                $data['matricula'] = $matricula;
                $data['carrera'] = $carrera->nombre;
                $path =  WRITEPATH . 'estudiantes/' . $credencial->alumno . '/' . $credencial->foto;
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $datos = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($datos);
                $data['foto'] = $base64;
                $data['qrimage'] = $this->generaQr($credencial->alumno);
                $data['vigencia'] = $credencial->vigencia;
                unset($data['error']);
            }
        }
        return view('estudiantes/validacion', $data);
    }

    /**
     * valida una constancia por su ID
     */
    public function c($id)
    {
        $constanciasModel = new ConstanciasModel();
        $constancia = $constanciasModel->find($id);
        if (!is_null($constancia)) {
            $data['id'] = $id;
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find($constancia->alumno);
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            if (!is_null($alumno)) {
                $data['nombre'] =  $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres;
                $data['matricula'] = $alumno->id;
                $data['carrera'] = $carrera->nombre;
                $data['folio'] = 'W' . $constancia->folio . '/' . date("Y");
                $data['periodo'] = $constancia->periodo;
                return view('estudiantes/validacionconstancia', $data);
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * registra un acceso a biblioteca
     */
    public function bl($id)
    {
        return redirect()->to('/Biblioteca/Acceso/' . $id);
    }

    /**
     * devuelve un JSON con la lista de municipios del estado especificado por post
     */
    public function mpiosCct($estado)
    {
        $cctsModel = new \App\Models\CCTSModel();
        $municipios = $cctsModel->getMunicipios($estado);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($municipios);
    }

    /**
     * devuelve un JSON con la lista de municipios del estado especificado por post
     */
    public function cctByMpio($estado, $municipio)
    {
        $cctsModel = new \App\Models\CCTSModel();
        $filtro = ['estado' => $estado, 'municipio' => $municipio];
        $escuelas = $cctsModel->busca($filtro);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($escuelas);
    }

    /**
     * devuelve un JSON con la lista de municipios del estado especificado por post
     */
    public function addEscuela()
    {
        $error = false;
        $output = [];
        $cctsModel = new \App\Models\CCTSModel();
        $datos = $this->request->getJSON();
        $cct = $cctsModel->find($datos->cct);
        if (is_null($cct)) {
            $res = $this->insertCct($datos);
            if ($res[0] == true) {
                $output = ['status' => 'ok', 'data' => ['nombre' => $res[1]->nombre, 'cct' => $res[1]->id]];
            } else {
                $error = $res[1];
            }
        } else {
            $output = ['status' => 'ok', 'data' => ['nombre' => $cct->nombre, 'cct' => $cct->id]];
        }
        if ($error) {
            $output = [
                'status' => 'error', 'data' => $error
            ];
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($output);
    }

    /**
     * realiza la operacion de insercion del CCT
     * @param datos los datos a insertar
     * @return res un arreglo de dos posiciones [ 0=> true|false, 1=>objetoCct|null ]
     */
    private function insertCct($datos)
    {
        $cctsModel = new \App\Models\CCTSModel();
        $res = false;
        $validar = [
            'cct' => $datos->cct, 'nombre' => $datos->nombre,
            'estado' => $datos->estado, 'municipio' => $datos->municipio,
        ];
        $validacion = $this->valida($validar, 'cct');
        if ($validacion === true) {
            try {
                $mycct = [
                    'cct_cct' => mb_strtoupper($datos->cct),
                    'nombre' => mb_strtoupper($datos->nombre),
                    'sep' => 'USR',
                    'calle' => 'CALLE 1',
                    'numero' => '0',
                    'estado' => mb_strtoupper($datos->estado),
                    'municipio' => mb_strtoupper($datos->municipio),
                    'localidad' => mb_strtoupper($datos->municipio),
                    'colonia' => 'SIN COLONIA',
                    'cp' => '01000',
                    'tipo' => 'USR',
                    'nivel' => '',
                ];
                $cct = new \App\Entities\CCTS($mycct);
                $cctsModel->insert($cct);
                $res = [true, $cct];
            } catch (\Exception $ex) {
                $res = [false, "Ocurrió un error al generar el registro de tu escuela."];
                log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
            }
        } else {
            $res = [false, implode("\n", $validacion)];
        }
        return $res;
    }

    /**
     * Genera el codigo QR que irá en la parte baja de la credencial con base en la matricula del estudiante
     * @param matricula la matricula del estudiante
     * @return 
     */
    private function generaQr($matricula)
    {
        $infoQr = base_url('E/V/' . $matricula);
        //$infoQr = 'https://bit.ly/fsdrfgdf';
        $result = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($infoQr)
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->size(150)
            ->margin(1)
            ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->labelText('')
            ->labelFont(new NotoSans(20))
            ->labelAlignment(new LabelAlignmentCenter())
            ->build();
        return $result->getDataUri();
    }

    /**
     * Descarga un archivo en pdf
     * @param folio ID del kardex generado
     */
    public function d($folio)
    {
        // $folio = $this->descifrar($folio);
        $datos = explode('-', $folio);
        // echo json_encode($datos);
        // die();
        if (crc32($datos[0]) == $datos[1]) {
            $documentos =  new ConstanciasModel();
            $documentos = $documentos->find($folio);
            $alumnoModel = new AlumnosModel();
            $alumno = $alumnoModel->find($documentos->alumno);
            $kardexModel = new KardexModel();
            $kardex = $kardexModel->alumno($alumno);
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            $periodoModel = new PeriodoModel();
            $periodo = $periodoModel->find($documentos->periodo);
            $data['receso'] = ($periodo->receso) ? true : false;
            $data['periodoInicio'] = $periodo->inicio_c;
            $data['periodoFin'] = $periodo->fin_c;
            $data['periodoEvaluaciones'] = $periodo->evaluaciones;
            $data['periodoVacaciones'] = $periodo->vacaciones;
            $data['periodoAdministrativas'] = $periodo->administrativa;
            $data['periodoRecesoEscolar'] = $periodo->receso;
            $data['kardex'] = $kardex;
            $data['documento'] = $documentos;
            $data['folio'] =  $documentos->folio;
            $data['alumno'] = $alumno;
            $data['genero'] = ($alumno->sexo == 'M') ? 'la' : 'el';
            $data['carrera'] = $carrera;
            $link = base_url() . '/E/d/' . $datos[1];
            $data['qr'] = CodigoQr::crea($link);
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'letter', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 10,
                'margin_bottom' => 15,
                'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            $mpdf->AliasNbPages('[pagetotal]');
            switch ($documentos->tipo) {
                case 'CONSTANCIA':
                    $mpdf->SetHTMLHeader(view('escolar/kardexheader'));
                    $mpdf->SetHTMLFooter(view('escolar/kardexfooter'));
                    $mpdf->WriteHTML(view('escolar/constanciapdf', $data));
                    break;
                case 'KARDEX OFICIAL':
                    $mpdf->SetHTMLHeader(view('escolar/kardexheader'));
                    $mpdf->SetHTMLFooter(view('escolar/kardexfooter'));
                    $mpdf->WriteHTML(view('escolar/kardexpdf', $data));
                    break;
                case 'KARDEX SIMPLE':
                    $mpdf->WriteHTML(view('escolar/kardexsimplepdf', $data));
                    break;
            }

            $mpdf->Output($alumno->id . "_" . $documentos->tipo . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
            die();
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        // return view( 'escolar/kardexpdf', $data );
    }

    public function unauthorized() {
        $data = [
            'status' => 'error'
        ];
        $this->response->setStatusCode(401, 'No autorizado')->setJSON( $data );
    }
}
