ALTER TABLE `alumnos_generales` ADD `alumno_semestre` TINYINT NOT NULL DEFAULT '1' COMMENT 'Indica el número de semestre en el que está el estudiante' AFTER `alumno_periodo`;

UPDATE `alumnos_generales` SET alumno_semestre='1' WHERE `alumno_id` LIKE '21%';
UPDATE `alumnos_generales` SET alumno_semestre='3' WHERE `alumno_id` LIKE '20%';