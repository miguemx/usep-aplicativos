<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<?php function buscacarrera()
{
    echo base_url('Escolar/buscacarrera');
}

$bandera = 'hidden';
?>

<style>
    .select-calif {
        width: 70%;
    }

    .main {
        display: flex;
        flex-direction: column;
        justify-content: center;
        background-color: #840f31;
        align-items: center;
    }

    .main select {
        background-color: whitesmoke;
        border-radius: 10px;
    }

    .input-error {
        border: 1px red solid !important;
    }

    .form_style {
        width: 100%;
        display: flex;
        padding-top: 15px;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .cal-in {
        padding: 3px;
        font-size: 18px;
        border-width: 1px;
        border-color: #CCCCCC;
        background-color: #FFFFFF;
        color: #000000;
        border-style: hidden;
        border-radius: 0px;
        box-shadow: 0px 0px 0px rgba(66, 66, 66, .0);
        text-shadow: 0px 0px 2px rgba(66, 66, 66, .70);
    }

    .cal-in:focus {
        outline: none;
    }

    .th-head {
        background-color: #840f31 !important;
        color: white !important;
        font-size: 0.875rem;
        text-transform: uppercase;
        letter-spacing: 2%;
    }
    
</style>
<div class="container my-sm-5">
    <?php if (isset($error)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($exito)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $exito; ?>
        </div>
    <?php endif; ?>
    
    <div class="card">
        <div class="card-body text-left container-sm main text-white">
            <h5 class="text-lg-center"> Actualización de Calificaciones</h5>

            <form class="form text-center form_style" method="post" action="<?php echo base_url('Escolar/ActualizacionDeCalificacionesGrupos'); ?>" enctype="multipart/form-data">
                <div class="form-group select-calif text-center col-sm-12">
                    <label for="select-docente" style="font-size: 20px; padding-right: 10px">Docente</label>
                    <select id="selectdocente " class="custom-select" style="width: 80%;" name="selectdocente"value = "1" onchange="this.form.submit();">
                        <option>Selecciona Docente</option>
                        <?php foreach ($docentes as $arreglodocente) : ?>
                            <option value = <?php echo $arreglodocente->id ?> <?php if($docente == $arreglodocente->id) echo 'selected="selected"'; ?>><?php
                                    echo $arreglodocente->id.'-'.$arreglodocente->nombre.'_'.$arreglodocente->apPaterno.'_'.$arreglodocente->apMaterno
                                    ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group select-calif text-center col-sm-12">
                    <label for="select-grupo" style="font-size: 20px; padding-right: 10px">Grupo</label>
                    <input type="hidden" name="docente" id="docente" value="<?php echo $docente; ?>" ;>
                    <select id="grupo " class="custom-select" style="width: 80%;" name="grupo" onchange="this.form.submit();">
                    <option>Selecciona grupo</option>
                        <?php foreach ($grupo as $grupos) : ?>
                            <option value = <?php echo $grupos->grupo ?> <?php if($idgrupo == $grupos->grupo) echo 'selected="selected"'; ?>><?php
                                    echo $grupos->grupo_clave . " - " . $grupos->materia_nombre . " (" . $grupos->materia_clave . ")" . $grupos->grupo_flag_acta;
                                    ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input type="hidden" name="estatus" id="estatus" value="<?php echo $estatus; ?>" ;>
            </form>

        </div> 
        

        <form method="post" id="contenedor2" action="<?php echo base_url('Escolar/capturacalificacionesServicios/'); ?> ">
            <div>
                <table name='calif_arr' class="text-center text-white" style="background-color: #840f31; width: 100%; border-top: 2px solid white;">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 120px; ">IDC</th>
                            <th style="width: 120px; ">PERIODO</th>
                            <th style="width: 120px; ">SECCIÓN</th>
                            <th style="width: 300px; ">CARRERA</th>
                            <th style="width: 300px; ">MATERIA</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <td><?php echo $idc ?></td>
                        <td><?php echo $periodo; ?></td>
                        <td><?php echo $seccion; ?></td>
                        <td><?php echo $carrera; ?></td>
                        <td><?php echo $informacion; ?></td>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table name='calif_arr' class="table table-light text-center">
                    <thead>
                        <tr>
                            <th class="th-head">selecciona</th>
                            <th class="th-head">No.</th>
                            <th class="th-head">Matricula</th>
                            <th class="th-head">Nombre</th>
                            <th class="th-head">Ordinario</th>
                            <th class="th-head">Extraordinario</th>
                            <th class="th-head">Numero oficio</th>
                            <th class="th-head">Comentario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <input name="grupo_clave" hidden id="grupo_clave" value="<?php echo $grupo_clave; ?>" ;>
                        <?php
                        $calificacion = null;
                        $tipoCalificacion = null;

                        ?>
                        <?php
                        if (!$grupo_clave){
                            $bandera= '  ';    
                        }else {
                            $bandera=null;
                        }
                        if ($grupo_flg_acta == 0) {
                            $validacion = '  ';
                            $bandera= '  ';
                        } else {
                            $validacion = '';
                        }
                        $contador = 0;
                        foreach ($alumno as $alumnos) : ?>
                             <?php $contador = $contador + 1; ?>
                            <tr>
                                <?php echo "<td><input type='checkbox' class='checkbx' onChange='comprobar(this);'  id='chbox' form-control text-center cal-in' type='number' name='cbox[]' value='$contador'></td>";?>

                                <td><?php echo $contador?></td>
                                
                                <td><?php
                                    echo "<input name='idalumno[]' id='idalumno[]' value='$alumnos->id_alumno' hidden>";
                                    echo $alumnos->id_alumno; ?>
                                </td>
                                <td class="text-left" disabled><?php echo $alumnos->alumno_ap_paterno . " " . $alumnos->alumno_ap_materno . " " . $alumnos->nombres ?></td>
                                <?php
                                if ($alumnos->gpo_ordinario == 0) {
                                    $calificacion = null;
                                } else {
                                    $calificacion = $alumnos->gpo_ordinario;
                                }
                                    echo "";
                                    echo "<td><input onChange='validaordinario($contador);'class='form-control text-center cal-in' type='number' name='calif[]' id=".$contador."a"." min='5' max='10' type='text' value='$calificacion' disabled ></td>";
                                    echo "<td><input onChange='validaextraordinario($contador);' class='form-control text-center cal-in' type='number' name='califextra[]' id=".$contador."b"." type='text' value='$alumnos->gpo_extraordinario' min='5' max='8' disabled ></td>";
                                    echo "<td><input class='form-control text-center cal-in' type='text' name='oficio[]' id=".$contador."c"." disabled required></td> ";
                                    echo "<td><textarea class='form-control text-center cal-in' type='text' name='comentario[]' id=".$contador."d"." disabled  required></textarea></td>";
                                    echo "<td><input name='ids[]' id=".$contador."e"." value='$alumnos->id_alumno' disabled hidden></td>";

                                ?>
                            </tr>
                        <?php endforeach; ?>



                    </tbody>

                </table>
            </div>
           
            <div class="card-footer text-center">
                <button type="button" id="btn2" class="btn btn-primary" style="background-color: #883232 ;" hidden >
                <input name="idc_grupo" type="hidden" value="<?php echo $idc; ?>" />
                    Guardar
                </button>
                <?php $bandera = null; ?>
            </div>

        </form>
    </div>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    'use strict';
    const btn_guardar = document.querySelector('#btn2');
    let validar = () => {
        //let inputs_requeridos = document.querySelectorAll('#contenedor2 [required]');
        let inputs_requeridos = document.getElementsByClassName("texto");
        let input_extra= document.getElementsByClassName("oficio");
        let error_empty = false;
        console.log(inputs_requeridos);
        for (let j=0; j < input_extra.length; j++){
            if (input_extra[j].value == '' ){
                console.log(j);
                input_extra[j].classList.add('input-error');
                error_empty = true;
            }else {
                input_extra[j].classList.remove('input-error');
            }
        }
        
        for (let i = 0; i < inputs_requeridos.length; i++) {
            if (inputs_requeridos[i].value == '' ) {
                inputs_requeridos[i].classList.add('input-error');
                error_empty = true;
            } else {
                inputs_requeridos[i].classList.remove('input-error');
            }
        }
        console.log('hola');
        return error_empty;
    };
    let obtener_datos = () => {
        let flg_validar = validar();
        if (flg_validar) {
            Swal.fire({
                'title': 'ERROR',
                'text': 'Por favor revise los campos marcados en rojo ',
                'icon': 'error',
                confirmButtonColor: '#883232',


            });
        } else {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! No podrá realizar cambios posteriores una vez que continue ¿Está seguro que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('contenedor2').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }

    }
    btn_guardar.addEventListener('click', obtener_datos);
</script>
<script>
    function comprobar(obj){
     if (obj.checked){
       document.getElementById(obj.value+'a').disabled = false;
       document.getElementById(obj.value+'b').disabled = false;
        document.getElementById(obj.value+'c').disabled = false;
        document.getElementById(obj.value+'d').disabled = false;
        document.getElementById(obj.value+'e').disabled = false;

       document.getElementById(obj.value+'c').classList.add('oficio');
       document.getElementById(obj.value+'d').classList.add('texto');

       document.getElementById("btn2").hidden = false;

     } 
     if(obj.checked != true){
       document.getElementById(obj.value+'a').disabled = true;
       document.getElementById(obj.value+'b').disabled = true;
       document.getElementById(obj.value+'c').disabled = true;
       document.getElementById(obj.value+'d').disabled = true;
       document.getElementById(obj.value+'e').disabled = true;
       document.getElementById(obj.value+'c').classList.remove('oficio');
       document.getElementById(obj.value+'d').classList.remove('texto');
       document.getElementById(obj.value+'c').classList.remove('input-error');
       document.getElementById(obj.value+'d').classList.remove('input-error');

       var suma = 0;
        var los_cboxes = document.getElementsByName('cbox[]'); 
        console.log(los_cboxes);
        for (var i = 0, j = los_cboxes.length; i < j; i++) {
            
            if(los_cboxes[i].checked == true){
            suma++;
            }
        }
      
         if (suma == 0) {       
            document.getElementById("btn2").hidden = true;
        } 

     }
   }

   
</script>
<script>
    function validaextraordinario(elemento){
        console.log(elemento);
    //if(document.getElementById(elemento+'b') == document.activeElement){
        document.getElementById(elemento+'a').value = "5";
    //}
    } 
</script>
<script>
    function validaordinario(elemento){
        document.getElementById(elemento+'b').value = null;
    }
</script>

<?php echo $this->endSection() ?>