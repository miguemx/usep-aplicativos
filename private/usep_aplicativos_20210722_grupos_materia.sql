CREATE VIEW view_grupos_materias AS

SELECT *
FROM aca_grupos
INNER JOIN aca_periodos ON grupo_periodo=periodo_id
INNER JOIN aca_materias ON grupo_materia=materia_clave;