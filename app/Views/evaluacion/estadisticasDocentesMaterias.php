<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>  
  letrareducida{
    font-size: 5px important!;
  }
</style>
<head>
  <!-- links para exportar a excel -->
  <script src="https://unpkg.com/xlsx@0.16.9/dist/xlsx.full.min.js"></script>
  <script src="https://unpkg.com/file-saverjs@latest/FileSaver.min.js"></script>
  <script src="https://unpkg.com/tableexport@latest/dist/js/tableexport.min.js"></script>
</head>
<div class="container " > 
  <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="card-header" >
      <h1 style="display:flex; justify-content:center">Estadística de Docentes</h1>
    </div>
    <div class="card-body" >
        <div class="row">
          <form action="<?php echo base_url('EstadisticasEvaluacionDocente/filtrosDeEstadistica/'); ?>" method="post">
            <input type="hidden" name="arreglodatos" id= 'arreglodatos' value='<?php echo json_encode($estadisticadocentesAll)?>'>
            <div class="input-group mb-3 col">
              <span class="input-group-text" id="inputGroup-sizing-default">Tipo de contratación</span>
              <select id="selectcontratacion" class="custom-select" style="width: 80%;" name="selectcontratacion" onchange="this.form.submit();" required>
                  <option value = "">Selecciona una opción</option>
                  <option value = "resumen">Resumen</option>
                  <option value = "tiempocompleto">CAPÍTULO 1000</option>
                  <option value = "horaclase">CAPÍTULO 2000</option>
              </select>
            </div>
            <div class="input-group mb-3 col">
              <span class="input-group-text" id="inputGroup-sizing-default">Resultados por carrera</span>
              <select id="selectcarrera" class="custom-select" style="width: 80%;" name="selectcarrera" onchange="this.form.submit();"  required>
                  <option value = "">Selecciona una opción</option>
                  <option value = "leo">LICENCIATURA EN ENFERMERIA Y OBSTETRICIA</option>
                  <option value = "med">LICENCIATURA EN MÉDICO CIRUJANO </option>
              </select>
            </div>
          </form>
        </div>
        <form action="<?php echo base_url('EstadisticasEvaluacionDocente/filtroEstadisticaGeneral'); ?>" method="post">
          <input type="hidden" name="arreglodatos" value='<?php echo json_encode($estadisticadocentesAll)?>'>
          <div class="mb-3">
            <div class="form-check form-switch">
              <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="switchreporte" value ='<?php $check?>'<?php if($check == 1) echo 'checked';?> onclick = valor() onchange="this.form.submit();">
              <label class="form-check-label" for="flexSwitchCheckDefault">Mostrar reporte general</label>
            </div>
          </div>
        </form>
        <div class="container-fluid">
        <button id="btnExportar" class="btn btn-success" onclick="exportarexcel()">Exportar</button>
        <?php if (isset($tabla)) : ?>
          <table id="tabla" class="table table-striped table-bordered ">
            <thead style="background-color: #84112c; color:white; border: 1px solid black;">
              <tr class="something" style="font-size:12px">
                <th>NO</th>
                <th>ID DE DOCENTE</th>
                <th>NOMBRE</th>
                <th>ESPECIALIDAD</th>
                <th>CLAVE DE MATERIA</th>
                <th>NOMBRE DE MATERIA</th>
                <th>CALIDAD DE RECURSOS</th>
                <th>PERTENENCIA ACADÉMICA</th>
                <th>RELACIÓN ACADÉMICA</th>
                <th>TRANSMISIÓN DE CONOCIMIENTOS</th>
                <th>GLOBAL</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $contador = 0;
              foreach ($estadisticadocentes as $docentes) :?>
                <tr>
                  <td><?php echo $contador = $contador + 1; ?></td>
                  <td><?php echo $docentes['empleado_id'];?></td>
                  <td><?php echo mb_strtoupper($docentes['app_pat'].' '.$docentes['app_mat'].' '.$docentes['nombre_docente']); ?></td>
                  <td><?php echo $docentes['especialidad']; ?></td>
                  <td class="text-center"><?php echo '('.$docentes['materia_id'].')'; ?></td>
                  <td><?php echo mb_strtoupper($docentes['materia_nombre']); ?></td>
                  <td class="text-center"><?php echo round($docentes['calidad_recursos'],2).'%'; ?></td>
                  <td class="text-center"><?php echo round($docentes['pertenencia_academica'],2).'%'; ?></td>
                  <td class="text-center"><?php echo round($docentes['relacion_academica'],2).'%'; ?></td>
                  <td class="text-center"><?php echo round($docentes['transmision_conocimientos'],2).'%'; ?></td>
                  <td class="text-center"><?php echo round($docentes['global'],2).'%'; ?></td>
                </tr>
              <?php endforeach;?> 
            </tbody>
          </table>
        <?php endif; ?>
        <?php if (isset($resumen)) : ?>
          <table id="tabla" class="table table-striped table-bordered ">
            <thead style="background-color: #84112c; color:white; border: 1px solid black;">
              <tr class="something">
                <th>NO</th>
                <th>TIPO DE CONTRATACION</th>
                <th>CALIDAD DE RECURSOS</th>
                <th>PERTENENCIA ACADÉMICA</th>
                <th>RELACIÓN ACADÉMICA</th>
                <th>TRANSMISIÓN DE CONOCIMIENTOS</th>
                <th>GLOBAL</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $contador = 0;
              foreach ($estadisticadocentes as $docentes) :?>
                <tr>
                  <td class="text-center"><?php echo $contador = $contador + 1; ?></td>
                  <td class="text-center"><?php echo $docentes['tipocontratacion']; ?></td>
                  <td class="text-center"><?php echo $docentes['calidad_recursos'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['pertenencia_academica'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['relacion_academica'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['transmision_conocimientos'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['global'].'%'; ?></td>
                </tr>
              <?php endforeach;?> 
            </tbody>
          </table>
        <?php endif; ?>
        <?php if (isset($general)) : ?>
          <table id="tabla" class="table table-striped table-bordered ">
            <thead style="background-color: #84112c; color:white; border: 1px solid black;">
              <tr class="something">
                <th>NO</th>
                <th>ID DE DOCENTE</th>
                <th>NOMBRE</th>
                <th>ESPECIALIDAD</th>
                <th>CALIDAD DE RECURSOS</th>
                <th>PERTENENCIA ACADÉMICA</th>
                <th>RELACIÓN ACADÉMICA</th>
                <th>TRANSMISIÓN DE CONOCIMIENTOS</th>
                <th>GLOBAL</th>
                <th>DETALLES</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $contador = 0;
              foreach ($estadisticadocentes as $docentes) :?>
                <tr>
                  <td class="text-center"><?php echo $contador = $contador + 1; ?></td>
                  <td class="text-center"><?php echo $docentes['empleado_id']; ?></td>
                  <td class="text-center"><?php echo mb_strtoupper($docentes['app_pat'].' '.$docentes['app_mat'].' '.$docentes['nombre_docente']); ?></td>
                  <td class="text-center"><?php echo $docentes['especialidad']; ?></td>
                  <td class="text-center"><?php echo $docentes['calidad_recursos'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['pertenencia_academica'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['relacion_academica'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['transmision_conocimientos'].'%'; ?></td>
                  <td class="text-center"><?php echo $docentes['global'].'%'; ?></td>
                  <td>
                    <button type="submit" name="modal" id='empleado_id' value="<?php echo $docentes['empleado_id'];?>" onclick="tablaXDocente('<?php echo $docentes['empleado_id'];?>')" data-bs-toggle="modal" data-bs-target="#descargar_lista" class="btn btn-secondary btn-sm">
                        Ver detalles
                    </button>
                  </td>
                </tr>
              <?php endforeach;?> 
            </tbody>
          </table>
        <?php endif; ?>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="descargar_lista" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" role="dialog" aria-labelledby="modalTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
                  <h5 class="modal-title" id="modalTitleId">Detalle de estadísticas de evaluación docente</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <table id="tabla-mod" class="table table-striped table-bordered ">
          </table>
        </div>
      </div>
    </div>
</div>
<script>
    function exportarexcel() {
        const $btnExportar = document.querySelector("#btnExportar"),
            $tabla = document.querySelector("#tabla");
            let tableExport = new TableExport($tabla, {
                exportButtons: false, // No queremos botones
                filename: "reporte-estadisticas", //Nombre del archivo de Excel
                sheetname: "reporte-estadisticas",//Título de la hoja
            });
            let datos = tableExport.getExportData();
            let preferenciasDocumento = datos.tabla.xlsx;
            tableExport.export2file(preferenciasDocumento.data, preferenciasDocumento.mimeType, 
            preferenciasDocumento.filename, preferenciasDocumento.fileExtension, preferenciasDocumento.merges, preferenciasDocumento.RTL, 
            preferenciasDocumento.sheetname);
    }
    function valor(){
      let boton = document.getElementById('flexSwitchCheckDefault');
      if(boton.checked){
        document.getElementById('flexSwitchCheckDefault').value = 1;
      }
      if(!boton.checked){
        document.getElementById('flexSwitchCheckDefault').value = 2;
      }
    }

    let tablaXDocente = async (iddocente) => {
        let arreglodatos = <?php echo json_encode($estadisticadocentesAll)?>;
        var table = document.getElementById('tabla-mod');
            table.innerHTML = "";
            let thead = document.createElement('thead');
            let trcabecera = document.createElement('tr');
            table.appendChild(thead);
            thead.appendChild(trcabecera);
            let th1 = document.createElement('th');
            let th2 = document.createElement('th');
            let th3 = document.createElement('th');
            let th4 = document.createElement('th');
            let th5 = document.createElement('th');
            let th6 = document.createElement('th');
            let th7 = document.createElement('th');
            let th8 = document.createElement('th');
            let th9 = document.createElement('th');
            let th10 = document.createElement('th');
            th1.innerHTML = "ID DE DOCENTE";
            th2.innerHTML = "NOMBRE";
            th3.innerHTML = "ESPECIALIDAD";
            th4.innerHTML = "CLAVE DE MATERIA";
            th5.innerHTML = "NOMBRE DE MATERIA";
            th6.innerHTML = "CALIDAD DE RECURSOS";
            th7.innerHTML = "PERTENENCIA ACADÉMICA";
            th8.innerHTML = "RELACIÓN ACADÉMICA";
            th9.innerHTML = "TRANSMISIÓN DE CONOCIMIENTOS";
            th10.innerHTML = "GLOBAL";
            trcabecera.appendChild(th1);
            trcabecera.appendChild(th2);
            trcabecera.appendChild(th3);
            trcabecera.appendChild(th4);
            trcabecera.appendChild(th5);
            trcabecera.appendChild(th6);
            trcabecera.appendChild(th7);
            trcabecera.appendChild(th8);
            trcabecera.appendChild(th9);
            trcabecera.appendChild(th10);
            let tbody = document.createElement('tbody');
            table.appendChild(tbody);
            for(let x = 0;x < arreglodatos.length;x++){
              if(arreglodatos[x].empleado_id == iddocente){
                let tr = document.createElement('tr');
                tbody.appendChild(tr);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].empleado_id;
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].app_pat + ' ' +arreglodatos[x].app_mat + ' ' +arreglodatos[x].nombre_docente;
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].especialidad;
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].materia_id;
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].materia_nombre;
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].calidad_recursos.toFixed(2);
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].pertenencia_academica.toFixed(2);
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].relacion_academica.toFixed(2);
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].transmision_conocimientos.toFixed(2);
                tr.appendChild(td);
                var td = document.createElement('td');
                td.innerHTML = arreglodatos[x].global.toFixed(2);
                tr.appendChild(td);
              }
            }
    }
</script>
<?php echo $this->endSection() ?>