<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class GrupoAlumnoModel extends Model {
    protected $table      = 'aca_grupos_alumnos';
    protected $primaryKey = 'gpoalumno_id';

    protected $returnType    = 'App\Entities\GrupoAlumno';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'gpoalumno_alumno','gpoalumno_grupo','gpoalumno_ordinario','gpoalumno_extraordinario','aprobada','gpoalumno_flg_pago','gpoalumno_num_oficio',
        'gpoalumno_autor','gpoalumno_fecha_modificacion','gpoalumno_id','gpoalumno_comentario',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca si un alumno pertenece a un grupo especifico
     * @param grupoId el ID del grupo donde buscar
     * @param alumnoId el ID del alumno a buscar
     * @return true si el alumno está en el grupo
     * @return false si el alumno no esta en el grupo, o en caso de error
     */
    public function existe( $grupoId, $alumnoId ) {
        $this->creaFiltros( [ 'alumno'=>$alumnoId, 'grupo'=> $grupoId ] );
        $grupoAlumno = $this->findAll();
        return ( count($grupoAlumno) === 0 )?false:true;
    }

    /**
     * realiza una actualizacion con base en las llaves foraneas de la tabla
     * @param GrupoAlumno la entidad de GrupoAlumno a cambiar los datos (solo los que no son llaves foraneas)
     */
    public function actualiza ($grupoAlumno) {
        $this->where( 'gpoalumno_alumno', $grupoAlumno->alumno );
        $this->where( 'gpoalumno_grupo', $grupoAlumno->grupo );

        $gpoAlumno = $this->first();

        if ( $gpoAlumno ) {
            if ( $grupoAlumno->ordinario > 0 ) {
                $sql = "UPDATE $this->table SET gpoalumno_ordinario='$grupoAlumno->ordinario' WHERE gpoalumno_alumno='$grupoAlumno->alumno' AND gpoalumno_grupo='$grupoAlumno->grupo'";
                $this->db->query( $sql );
            }
        }        
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('alumno', $filtros) ) { // -- filtrar por alumno
            if ( strlen($filtros['alumno']) ) {
                $this->where( 'gpoalumno_alumno', $filtros['alumno'] );
            }
        }
        if ( array_key_exists('grupo', $filtros) ) { // -- filtrar por grupo
            if ( strlen($filtros['grupo']) ) {
                $this->where( 'gpoalumno_grupo', $filtros['grupo'] );
            }
        }
    }


    public function borra() {
        $sql = "DELETE FROM aca_grupos_alumnos";
        $this->db->query($sql);
    }

    public function quita($alumno, $grupo) {
        $sql = "DELETE FROM aca_grupos_alumnos WHERE gpoalumno_alumno='$alumno' AND gpoalumno_grupo='$grupo'";
        $this->db->query($sql);
    }

    /**
     * 
     */
    public function horario($matricula) {
        $this->where('gpoalumno_alumno',$matricula);
        $this->where('gpoalumno_ordinario IS NULL');
        $grupos = $this->findAll();
        if ( count($grupos) ) {
            return $grupos;
        }
        return null;
    }
    public function grupos($idgrupo){
        $consulta = $this->where('gpoalumno_grupo',$idgrupo)->findAll();
        return $consulta;
    }
    public function updatecalificacion($id,$grupo,$arreglo){
        //$resultados = $this->db
        return $this->db
        ->table('aca_grupos_alumnos')
        ->where(["gpoalumno_alumno" => $id, 'gpoalumno_grupo' => $grupo])
        ->set($arreglo)
        //->getCompiledUpdate();
        ->update();

        //return $resultados;

    }
    public function actualizarCalificacionordinario($calificacion,$comentario,$status,$oficio,$profesor,$fecha,$grupo,$matricula)
    {
        $sql = "UPDATE `aca_grupos_alumnos` SET `gpoalumno_ordinario` = '$calificacion',`gpoalumno_extraordinario` = null,`gpoalumno_comentario` = '$comentario', `aprobada` = '$status', `gpoalumno_num_oficio`= '$oficio',
               `gpoalumno_autor` = '$profesor',`gpoalumno_fecha_modificacion`= '$fecha' WHERE `gpoalumno_grupo`= '$grupo' AND `gpoalumno_alumno` = '$matricula'";
        $this->db->query($sql);
    }
    public function actualizarCalificacionExtra($calificacion,$comentario,$status,$oficio,$profesor,$fecha,$grupo,$matricula)
    {
       /*  echo $matricula;
        die(); */
        $sql = "UPDATE `aca_grupos_alumnos` SET `gpoalumno_ordinario` ='5', `gpoalumno_extraordinario` = '$calificacion',`gpoalumno_comentario` = '$comentario', `aprobada` = '$status', `gpoalumno_num_oficio`= '$oficio',
               `gpoalumno_autor` = '$profesor',`gpoalumno_fecha_modificacion`= '$fecha' WHERE `gpoalumno_grupo`= '$grupo' AND `gpoalumno_alumno` = '$matricula'";
        $this->db->query($sql);
    }
    public function actualizarCalificacion($matricula,$seccion,$data)
    {
        $sql = "UPDATE `aca_grupos_alumnos` SET `gpoalumno_ordinario` = '$data->ordinario',`gpoalumno_extraordinario` = '$data->extraordinario',`gpoalumno_comentario` = '$data->comentarios', `aprobada` = '$data->aprobada', `gpoalumno_num_oficio`= '$data->Nooficio',
               `gpoalumno_autor` = '$data->autor',`gpoalumno_fecha_modificacion`= '$data->fecha' WHERE `gpoalumno_grupo`= '$seccion' AND `gpoalumno_alumno` = '$matricula'";
        $this->db->query($sql);
    }
    public function historial($id_alumno)
    {
        $query = $this->db->query("SELECT * FROM aca_grupos_alumnos WHERE gpoalumno_alumno ='$id_alumno' ");
       return $query->getResultArray();
    }

    /**
     * retorna todos los alumnos de un grupo seleccionado
     * @param grupo ID del grupo
     */
    public function getByGrupo($grupo)
    {
        return $this->where('gpoalumno_grupo',$grupo)->find();
    }

    /**
     * retorna el registro del alumno en un grupo
     * @param alumno ID alumno
     * @param grupo ID de grupo
     */
    public function getRegistroAlumnoGrupo($alumno,$grupo)
    {
        return $this->where('gpoalumno_alumno',$alumno)->where('gpoalumno_grupo',$grupo)->find();
    }

    /**
     * elimina todos los grupos donde esté registrado un alumno
     * @param matricula el ID o matricula del alumno
     * @return true cuando se borra correctamente
     * @return false cuando ocurre un error al borrar
     */
    public function borrarAlumno( $matricula ) {
        $this->where('gpoalumno_alumno', $matricula)->delete();
    }

    /**
     * elimina el registro de un alumno en un grupo
     * @param matricula ID del alumno
     * @param idc ID del grupo 
     */
    public function popAlumnoGrupo($matricula,$grupo)
    {
        $this->where('gpoalumno_alumno',$matricula)->where('gpoalumno_grupo',$grupo)->delete();
    }
    
    /**
     * devuelve un arreglo con el grupo y la cantidad de alumnos que tiene cada grupo
     * @param grupo para delimitar la busqueda a un solo grupo
     * @return arreglo grupo junto con cantidades de alumnos 
     */
    public function n_alumnosxgrupo($grupo = null)
    {
        if(!is_null($grupo) ){
            $this->where('gpoalumno_grupo',$grupo);
        }
        $arreglo=$this->select ('gpoalumno_grupo')->selectCount('gpoalumno_alumno')->groupBy('gpoalumno_grupo')->find();
        if(count($arreglo)){
            return ($arreglo);
        }
        return null;
    }
    /**
     * devuelve la cantidad de alumnos que tiene un conjunto de idc's
     * @param seccion arreglo de idcs de grupo
     * @return cantidad numero de alumnos por conjunto otorgado
     */
    public function n_alumnosxgruposeccion($seccion)
    {
        $arreglo=selectCount('gpoalumno_alumno')->whereIn('gpoalumno_grupo',$seccion)->find();
        $cantidad = $arreglo[0]->gpoaluumno_alumno;
        return ($cantidad);
    }

    /**
     * cuenta el número de alumnos de un grupo en específico
     * @param grupoid id/idc del grupo a consultar
     * @return consulta numero de alumnos pertenecientes al grupo consultado
     */

     public function n_alumnosxgrupov2($grupoid){
        $consulta=$this->select('COUNT(distinct gpoalumno_alumno) as n_alumnos')
                       ->where('gpoalumno_grupo',$grupoid)
                       ->first();
        return $consulta;
     }
}