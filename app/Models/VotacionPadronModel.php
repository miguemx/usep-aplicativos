<?php
namespace App\Models;
use CodeIgniter\Model;

class VotacionPadronModel extends Model {
    protected $table      = 'votacion_padron';
    protected $primaryKey = 'padron_id';

    protected $returnType    = 'App\Entities\VotacionPadron';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'padron_id','padron_correo','padron_voto_abstencion','padron_proceso','padron_tipovotante'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
public function buscavoto($idproceso,$correo){
    $consulta = $this->where('padron_correo', $correo)->where('padron_proceso', $idproceso)->findAll();
    return $consulta;
}
public function updatePadron($correo,$proceso,$voto){
    return $this->db
    ->table('votacion_padron')
    ->where(["padron_correo" => $correo, 'padron_proceso' => $proceso])
    ->set($voto)
    ->update();
}
public function buscapadron($tipovotante,$proceso){
    $consulta = $this->where('padron_tipovotante', $tipovotante)->where('padron_proceso', $proceso)->findAll();
    return $consulta;
}
public function buscavotos($idproceso){
    $consulta = $this->where('padron_proceso',$idproceso)
    ->findAll();
    return $consulta;
}
public function buscavotosabstencion($idproceso){
     $consulta = $this->where('padron_proceso',$idproceso)
    ->where('padron_voto_abstencion','0')->findAll();
    return $consulta;
}
public function buscacorreovoto($correo){
    $consulta = $this->where('padron_correo',$correo)
    ->where('padron_voto_abstencion',null)->findAll();
    return $consulta;
}
}