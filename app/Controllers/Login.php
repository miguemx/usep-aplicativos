<?php 
namespace App\Controllers;

use App\Models\UsuariosModel;

class Login extends BaseController {

    protected $googleClient = null;

    public function index() {

        $this->initGoogleClient();
		$auth_url = $this->googleClient->createAuthUrl(); 
		$data['googleLink'] = filter_var($auth_url, FILTER_SANITIZE_URL);
		$flasherror = $this->sesion->getFlashdata('flasherror');
		if ( !is_null($flasherror) ) {
			$data['error'] = $flasherror;
		}
        return view('login/login', $data);
    }

    public function inicia() {
		$usuariomodel = new UsuariosModel();
        $code = $this->request->getGet('code');
		try {
			$this->initGoogleClient();
			$this->googleClient->authenticate($code);
			$access_token = $this->googleClient->getAccessToken();
			$oauth2 = new \Google_Service_Oauth2($this->googleClient);
			$userInfo = $oauth2->userinfo->get();
			if ( $userInfo->email ) {
                $resultSesion = $this->setSesion( $userInfo->email );
				if ( $resultSesion === true ) {
					$usuariologin=$usuariomodel->findByEmail($this->sesion->get('useremail'));
					// aqui se guarda la última conexión del usuario
					if ($usuariologin){
						$usuariologin->usuario_ultcnx=date("Y-m-d H:i:s");
						$usuariomodel->update($this->sesion->get('useremail'),$usuariologin);
					}
					return redirect()->to( base_url() );
				}
				else {
					$this->sesion->setFlashdata('flasherror', $resultSesion );
					return redirect()->to('/Login');
				}
			}
			else {
				$this->sesion->setFlashdata('flasherror', 'No se ha iniciado sesion correctamente desde Google.' );
				return redirect()->to('/Login');
			}
		}
		catch (\Google\Service\Exception $ex) {
			$this->sesion->setFlashdata('flasherror', 'Error en la sesión de Google. ' );
			log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
			return redirect()->to('/Login');
		}
		catch (\GuzzleHttp\Exception\ConnectException $ex) {
			$this->sesion->setFlashdata('flasherror', 'No se puede validar la sesión de Google. ' );
			log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
			return redirect()->to('/Login');
		}
		catch (\Exception $ex) {
			$this->sesion->setFlashdata('flasherror', $ex->getMessage() );
			log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
			return redirect()->to('/Login');
		}
    }

    /**
     * elimina la galleta de sesion y redirige a la pagina de login
     */
    public function logout() {
		$this->sesion->destroy();
		return redirect()->to('/Login');
    }

    /**
     * Inicializa el cliente de google para poder hacer inicio de sesion mediante oauth2
     */
    private function initGoogleClient() {
		if ( is_null($this->googleClient) ) {
			$clientSecretFile = ROOTPATH.'private/client_secret_usalud.json';

			$this->googleClient = new \Google_Client();
			$this->googleClient->setAuthConfig($clientSecretFile);
			$this->googleClient->setApplicationName("USEP");
			$this->googleClient->addScope( 'https://www.googleapis.com/auth/userinfo.email' );
			$this->googleClient->setRedirectUri( base_url('Login/Inicia') );			
			
			// offline access will give you both an access and refresh token so that
			// your app can refresh the access token without user interaction.
			$this->googleClient->setAccessType('offline');
			// Using "consent" ensures that your application always receives a refresh token.
			// If you are not using offline access, you can omit this.
			// $this->googleClient->setApprovalPrompt("select_account");
			$this->googleClient->setIncludeGrantedScopes(true);   // incremental auth
		}
	}

    /**
	 * coloca los valores de la sesion dependiendo si encuentra el usuario autenticado por google
	 * @param email el EMAIL que se recibe de google
	 * @return true en caso de que todos los valores se puedan settear de forma correcta
	 * @return error el mensaje de error en caso de fallo
	 */
	private function setSesion($email) {
		$usuariosModel = new UsuariosModel();
		$usuario = $usuariosModel->getSessionData( $email );
		if ( !is_null($usuario) ) {
			$this->sesion->set( 'useremail', $usuario['correo'] );
			$this->sesion->set( 'nombre', $usuario['nombre'] );
			$this->sesion->set( 'rol_id', $usuario['id_rol'] );
			$this->sesion->set( 'rol', $usuario['rol'] );
			$this->sesion->set( 'menu', $usuario['menu'] );
			$this->sesion->set( 'id', $usuario['id'] );
			return true;
		}
		return 'El usuario no está registrado en el sistema o no cuenta con los permisos para acceder. Por favor contacte al administrador.';
	}


}
