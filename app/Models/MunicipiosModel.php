<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Municipios;

class MunicipiosModel extends Model
{
    protected $table="municipios";
    protected $primaryKey="id";
    protected $returnType='App\Entities\Municipios';
    protected $allowedFields=['id','estado_id','clave','nombre','activo'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * realiza una búsqueda de aspirantes de acuerdo a los filtros proporcionados.
     * Si no se envía nungun filtro, se devuelven todos. Devolverá páginas de 25 registros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     * @return aspirantes devuelve un array con entidades de AspirantesGenerales
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros );
        $aspirantes = $this->paginate(25);
        return $aspirantes;
    }

    public function getByEstadosmunicipio($id_estado)
    {
         //$this->where('estado_id',$id_estado)->findAll();
        return $this->where('estado_id',$id_estado)->findAll();
    }

    /**
     * devuelve todos los minicipios en un arreglo donde la llave es el Id del mpio
     * @return municipios el arreglo con los estados
     */
    public function arreglo() {
        $mpios = $this->findAll();
        $municipios = [
            0 => 'EXTRANJERO'
        ];
        foreach ( $mpios as $mpio ) {
            $municipios[ $mpio->id ] = $mpio->nombre;
        }
        return $municipios;
    }

}
