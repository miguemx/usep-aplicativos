<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Alumno;
use phpDocumentor\Reflection\Types\This;

class GruposMateriasModel extends Model
{
    protected $table      = 'view_grupos_materias';
    protected $primaryKey = 'grupo_id';

    protected $returnType    = 'App\Entities\GrupoMateria';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca un grupo con espacio para asignar a un alumno
     */
    public function buscaGrupos($carrera)
    {
        $this->where('grupo_periodo', '2021A');
        $this->where('materia_carrera', $carrera);
        $this->where('grupo_ocupado < grupo_max');
        $this->orderBy('grupo_clave',  'ASC');
        $grupo = $this->first();
        if (!is_null($grupo)) {
            $this->where('grupo_clave', $grupo->clave);
            $this->where('grupo_periodo', '2021A');
            $this->where('materia_carrera', $carrera);
            $this->where('grupo_ocupado < grupo_max');
            $this->orderBy('grupo_materia',  'ASC');
            $grupos = $this->findAll();
            return $grupos;
        }
        return null;
    }
    public function buscamaterias($grupo, $docente, $materia)
    {
        //var_dump($grupo."_".$docente."_".$materia);
        $consulta = $this->where('grupo_id', $grupo)->findAll();
        return $consulta;
    }
    public function busquedageneral($periodo, $docente)
    {
        $materias_sncal[0] = 'MC-TUT';
        $materias_sncal[1] = 'LEO-TUT';
        $consulta = $this->where('grupo_periodo', $periodo)->where('grupo_docente', $docente)->whereNotIn('materia_clave', $materias_sncal)->findAll();
        return $consulta;
    }
    public function buscamateria($idgrupo)
    {
        $consulta = $this->where('grupo_id', $idgrupo)->findAll();
        return $consulta;
    }
    public function buscarporseccion($seccion, $periodo)
    {
        $consulta = $this->where('grupo_clave', $seccion)->where('grupo_periodo', $periodo)->findAll();
        return $consulta;
    }
    /**
     * retorna los grupos que pertenezcan a la seccion en el periodo correspondiente ordenados por la clave de la materia
     * @param seccion ID de seccion
     * @param periodo ID del periodo
     */
    public function getBySeccion($seccion, $periodo)
    {
        $consulta = $this->where('grupo_clave', $seccion)->where('grupo_periodo', $periodo)->orderBy('grupo_materia', 'ASC')->find();
        return $consulta;
    }

    /**
     * devuelve todos los grupos de la vista de acuerdo al semestre, periodo y carrera
     * @param semestre el número de semestre a obtener
     * @param periodo el ID o nombre del periodo
     * @param carrera el Id de la carrera
     * @param soloSemestre (opcional) true si se desea solo los grupos del semestre solicitado; false para devolver materias que tienen independencia de semestre
     * @return query 
     */
    public function getBySemestrePeriodoCarrera($semestre, $periodo, $carrera, $soloSemestre = false)
    {
        $this->where('grupo_periodo', $periodo);
        $this->groupStart();
        $this->where('materia_semestre', $semestre);
        if (!$soloSemestre) $this->orWhere('materia_semestre is null');
        $this->groupEnd();
        $this->where('materia_carrera', $carrera);
        $this->orderBy('grupo_clave', 'ASC');
        return $this->find();
    }
    /**
     * @return query con las secciones creadas en el periodo seleccionado 
     * @param carrera id de la carrera seleccionada
     * @param periodo periodo activo
     * @param semestre 
     */
    public function getSeccionesPeriodo($semestre, $periodo, $carrera)
    {
        return $this->select('grupo_clave')->where('grupo_periodo', $periodo)->where('materia_semestre', $semestre)->where('materia_carrera', $carrera)->orderBy('grupo_clave', 'ASC')->distinct()->find();
    }

    /**
     * @param periodo id del periodo activo
     * @param carrera id del la carrera
     * @return query devuelve el arreglo de grupos que pertenecen al periodo seleccionado
     */
    public function getGruposByPeriodoCarrera($periodo, $carrera)
    {
        return
            $this->where('grupo_periodo', $periodo)
            ->where('materia_carrera', $carrera)
            ->orderBy('grupo_clave', 'ASC')
            ->orderBy('materia_nombre', 'ASC')
            ->find();
    }

    /**
     * realiza una búsqueda de aspirantes de acuerdo a los filtros proporcionados.
     * Si no se envía nungun filtro, se devuelven todos. Devolverá páginas de 25 registros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     * @return aspirantes devuelve un array con entidades de AspirantesGenerales
     */
    public function buscar($filtros = array())
    {
        $this->creaFiltros($filtros);
        $this->orderBy('grupo_clave', 'ASC');
        $this->orderBy('materia_nombre', 'ASC');
        $grupos = $this->find();
        return $grupos;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('mat_nombre', $filtros)) {
            if (strlen($filtros['mat_nombre'])) {
                $this->like('materia_nombre', trim($filtros['mat_nombre']), 'both');
            }
        }
        if (array_key_exists('mat_calve', $filtros)) {
            if (strlen($filtros['mat_calve'])) {
                $this->like('grupo_materia', mb_strtoupper($filtros['mat_calve']), 'both');
            }
        }
        if (array_key_exists('seccion', $filtros)) {
            if (strlen($filtros['seccion'])) {
                $this->like('grupo_clave', mb_strtoupper(trim($filtros['seccion'])), 'both');
            }
        }
        if (array_key_exists('nombre', $filtros)) {
            if (strlen($filtros['nombre'])) {
                $this->like('empleado_nombre', trim($filtros['nombre']), 'both');
            }
        }
        if (array_key_exists('apPaterno', $filtros)) {
            if (strlen($filtros['apPaterno'])) {
                $this->like('empleado_ap_paterno', trim($filtros['apPaterno']), 'both');
            }
        }
        if (array_key_exists('apMaterno', $filtros)) {
            if (strlen($filtros['apMaterno'])) {
                $this->like('empleado_ap_materno', trim($filtros['apMaterno']), 'both');
            }
        }
        if (array_key_exists('periodo', $filtros)) {
            if (strlen($filtros['periodo'])) {
                $this->where('grupo_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('docente', $filtros)) {
            if (strlen($filtros['docente'])) {
                $this->where('grupo_docente', $filtros['docente']);
            }
        }
        if (array_key_exists('carrera', $filtros)) {
            if (strlen($filtros['carrera'])) {
                if ($filtros['carrera'] != 'administrador') {
                    # code...administrador
                    $this->where('materia_carrera', $filtros['carrera']);
                }
            }
        }
        if (array_key_exists('docente', $filtros)) {
            if (strlen($filtros['docente'])) {
                $this->where('grupo_docente', $filtros['docente']);
            }
        }
        if (array_key_exists('mat_curriculares', $filtros)) {
            $this->notLike('grupo_materia', 'TUT','before');
            $this->notLike('grupo_materia', 'SIM','before');
        }

        $this->orderBy('grupo_clave', 'ASC');
        $this->orderBy('grupo_materia', 'ASC');
    }

    /**
     * retorna la informacion de las practicas clinicas de una seccion
     * @param seccion ID de seccion 
     * @param periodo
     */
    public function getPracticasClinicas($periodo)
    {
        return $this->where('grupo_periodo', $periodo)->like('materia_nombre', 'practica', 'both')->find();
    }

    /**
     * devielve todas las materias impartidas por un docente en el periodo seleccionado
     * @param docente ID docente
     * @param periodo ID periodo
     */
    public function getByDocentePeriodo($docente, $periodo)
    {
        return $this->where('grupo_periodo', $periodo)->where('grupo_docente', $docente)->find();
    }
}
