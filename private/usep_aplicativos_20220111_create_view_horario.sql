create VIEW view_horario_alumnos as
SELECT
    gpoalumno_alumno,
    gpoalumno_grupo,
    aca_grupos.grupo_materia,
    aca_grupos.grupo_clave,
    aca_grupos.grupo_docente,
    aplicativo_empleados.empleado_correo,
    aplicativo_empleados.empleado_nombre,
    aplicativo_empleados.empleado_ap_paterno,
    aplicativo_empleados.empleado_ap_materno,
    aca_materias.materia_nombre,
    aca_grupos_aulas.gpoaula_dia,
    aca_grupos_aulas.gpoaula_inicio,
    aca_grupos_aulas.gpoaula_fin,
    aca_aulas.aula_nombre
FROM
    aca_grupos_alumnos
INNER JOIN aca_grupos ON aca_grupos.grupo_id = aca_grupos_alumnos.gpoalumno_grupo
INNER JOIN aca_grupos_aulas ON aca_grupos_aulas.gpoaula_grupo = aca_grupos.grupo_id
INNER JOIN aca_aulas on aca_aulas.aula_id = aca_grupos_aulas.gpoaula_aula
INNER JOIN aplicativo_empleados ON aca_grupos.grupo_docente = aplicativo_empleados.empleado_id
INNER JOIN aca_materias on aca_materias.materia_clave = aca_grupos.grupo_materia;