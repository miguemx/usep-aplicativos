<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<h4><?php echo $etapa->nombre; ?></h4>
<hr />
<div class="container"> 
    <?php if(isset($warn) && $warn!=null): ?>
        <div class="alert alert-primary" style="color: #fff;">
            <?php echo $warn; ?>
        </div>
    <?php endif; ?>

    <?php if(isset($msg)): ?>
        <div class="alert alert-secondary">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>

    <?php if( $etapa->responsable == '1' ): ?>
        <div class="row">
            <p class="negro" style="color: #000;">
                Conteste el siguiente formulario como se indica. Al hacer clic en FINALIZAR APARTADO, usted acepta que ha contestado
                la información que se solicita de forma correcta, completa y acepta que no podrá modificarla posteriormente.
            </p>
            
            <hr />
            <?php foreach( $formularios as $formulario ): ?>
                <div class="col-md-4">
                    <div class="card" data-animation="true" style="margin-bottom: 10px;">
                        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                            
                        </div>
                        <div class="card-body text-center">
                            <h5 class="font-weight-normal mt-3">
                                <a href="<?php echo base_url('Admision/Formulario').'/'.$formulario->formulario; ?>"><?php echo $formulario->titulo; ?></a>
                            </h5>
                            <p class="mb-0">
                                <?php echo $formulario->instrucciones; ?>
                            </p>
                        </div>
                        <hr class="dark horizontal my-0">
                        <div class="card-footer d-flex">
                            <p class="font-weight-normal my-auto">&nbsp;</p>
                            <i class="material-icons position-relative ms-auto text-lg me-1 my-auto">edit</i>
                            <p class="text-sm my-auto">
                                <a href="<?php echo base_url('Admision/Formulario').'/'.$formulario->formulario; ?>">Ver / Contestar</a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <hr />
            <form method="post" action="<?php echo base_url('Admision/FinalizaEtapa'); ?>" enctype="multipart/form-data" onsubmit="return confirmaFin();">
                <div class="row">
                    <div class="col-md-12">
                        <?php if( $aspirante->etapa == $etapa->etapa ):  ?>
                            <input type="hidden" name="etapa" value="<?php echo $etapa->etapa; ?>" />
                            <button type="submit" class="btn bg-gradient-primary" name="prc" value="1">
                                Finalizar Apartado
                            </button>
                        <?php else:  ?>
                            <h6>Apartado concluido</h6>
                        <?php endif;  ?>
                    </div>
                </div>
            </form>
        </div>

        
        
    <?php else: ?>
        <div class="card">
            <div class="card-header">
                Esta etapa esta a cargo del Responsable de Admisión.
            </div>
            <div class="card-body">
                Por el momento, estás en una etapa de espera. Vuelve a ingresar al sistema para obtener las siguientes indicaciones
            </div>
        </div>
    <?php endif; ?>

    
</div>

<script>
    function confirmaFin() {
        return confirm("¿Está seguro que desea finalizar este apartado?\nSi acepta, ya no podrá modificar los datos que ha capturado.");
    }
</script>


<?php echo $this->endSection() ?>