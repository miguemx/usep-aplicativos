<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Constancia extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'alumno' => null,
        'periodo' => null,
        'folio' => null,
        'tipo' => null,
        'fechaExp' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'constancia_id',
        'alumno' => 'constancia_alumno',
        'periodo' => 'constancia_periodo',
        'folio' => 'constancia_folio',
        'tipo' => 'constancia_tipo',
        'fechaExp' => 'constancia_fecha_exp',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
