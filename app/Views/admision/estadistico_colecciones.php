<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
        height: 400px;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #ebebeb;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<div class="container mt-1 rounded-3 border-0">
    <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
        <div class="card-body">
            <div class="mb-3">
                <label for="select-tipocampo" class="form-label">Seleccione el tipo de campo</label>
                <select class="form-select" name="select-tipocampo" id="select-tipocampo" onchange="getPreguntas()">
                    <option selected>Seleccione una opcion disponible</option>
                    <?php foreach ($campos as $campo) : ?>
                        <option value="<?php echo $campo->id ?>"><?php echo $campo->nombre ?></option>
                    <?php endforeach; ?>
                </select>
                <small id="help-tipo-campo" class="form-text text-muted">Selecciona un tipo de dato para que se desplieguen las opciones de la seleccion de preguntas</small>

            </div>
            <div class="mb-3 select-pregunta">
                <label for="selec-pregunta" class="form-label">Selecciona la pregunta que se requiera graficar</label>
                <select class="form-select" name="selec-pregunta" id="selec-pregunta">
                </select>
                <small id="help-campo" class="form-text text-muted">Si no hay ningun elemento en este selector por favor selecciona un tipo de campo diferente y vuelve a intentar</small>

            </div>
            <div class="text-center mt-1">
                <button type="button" class="btn btn-secondary" onclick="getDatosGrafico()">Buscar estadistica</button>
            </div>
        </div>
        <div class="card-body">
            <figure class="highcharts-figure">
                <div id="container"></div>
            </figure>
        </div>
    </div>
</div>
<script>
    var getPreguntas = async () => {
        let tipo_campo = document.getElementById('select-tipocampo').value;
        const formData = new FormData();
        formData.append('tipo_campo', tipo_campo);
        document.getElementById('selec-pregunta').innerHTML = '';

        let response = await fetch('<?php echo base_url("EstadisticasAdmision/getCamposCorrespondientes") ?>' + '/' + tipo_campo, {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return false;
        });
        if (response) {
            document.getElementById('selec-pregunta').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'Seleccione una opcion';
            document.getElementById('selec-pregunta').appendChild(option);
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].campo;
                option.innerHTML = response[i].nombre;
                document.getElementById('selec-pregunta').appendChild(option);
            }
        } else {
            document.getElementById('selec-pregunta').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'No hay opciones disponibles';
            document.getElementById('selec-pregunta').appendChild(option);
        }
    }

    let getDatosGrafico = async () => {
        let campo = document.getElementById('selec-pregunta').value;
        if (campo != '') {
            //  console.log(campo);
            const formData = new FormData();
            formData.append('id_campo', campo);
            let response = await fetch('<?php echo base_url("EstadisticasAdmision/getArrayEstadisticos") ?>' + '/' + campo, {
                method: 'POST',
                body: formData,
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                return false;
            });
            if (response) {
                const chart = Highcharts.chart('container', {
                    chart: {
                        inverted: false,
                        polar: false
                    },
                    title: {
                        text: 'USEP Estadisticos'
                    },
                    subtitle: {
                        text: response.titulo
                    },
                    xAxis: {
                        categories: response.cabecera,
                    },
                    series: [{
                        type: 'column',
                        colorByPoint: true,
                        data: response.valores,
                        showInLegend: false
                    }]

                });
                let suma=0;
                for (let index = 0; index < response.valores.length; index++) {
                    suma += response.valores[index];
                }
                let axios = document.getElementsByClassName('highcharts-axis-title');
                let creditos = document.getElementsByClassName('highcharts-credits');
                axios[0].childNodes[0].data = 'Registros totales: '+suma;
                creditos[0].childNodes[0].data = '';

            }
        } else {
            document.getElementById('container').innerHTML = '';
            alert("No se econtraron resultados de esta busqueda");
        }
    }
</script>
<?php echo $this->endSection() ?>