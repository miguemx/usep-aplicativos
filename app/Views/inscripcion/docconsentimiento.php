<style type="text/css">
    *, div, p {
        font-size: 1.0rem;
        font-family: Arial, Helvetica, sans-serif;
    }
    p {
        text-align: justify;
        margin: 15px 25px;
    }
</style>


<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Formato de consentimiento para uso de datos personales<br />
</p>
<p>
    El(la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente inscrito(a) 
    en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id ?></u></strong>, en 
    cumplimiento al Artículo 11 inciso c, del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA 
    UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA, entiendo, estoy de acuerdo y doy mi consentimiento para  que la Dirección 
    de Servicios Escolares y Titulación (DSEyT) de la Universidad de la Salud del Estado de Puebla (USEP), recabe, utilice y 
    conserve mis datos personales de conformidad con las disposiciones de la Ley General de Protección de Datos Personales en 
    Posesión de Sujetos obligados (LGPDPPSP), y la Ley de Protección de Datos Personales en Posesión de sujetos obligados del 
    Estado de Puebla (LPDPPSPEP); los artículos 134, 135 y 136 de la Ley de Transparencia y Acceso a la Información Pública del 
    Estado de Puebla y demás disposiciones que resulten responsables, para los efectos de prestar servicios educativos en el nivel 
    superior, según lo establecido de manera expresa en el Aviso de Privacidad y Protección de Datos Personales que Resguarda la 
    Universidad de la Salud; por lo que la DSEyT-USEP no podrá utilizar o divulgar mis datos personales para propósitos distintos 
    a los de la realización de sus fines fundamentales, en el ámbito educativo y que cumplan con los principios de pertinencia y finalidad.
</p>
<p >
    Para dicho efecto y finalidad, <strong><u>doy consentimiento expreso voluntario</u></strong> a la DSEyT-USEP, para que transmita mis datos personales a las 
    dependencias universitarias o entidades vinculadas en conformidad con los Artículos 6, 7, 8 y 9 de la Ley de Protección de Datos Personales 
    en Posesión de los Sujetos Obligados del Estado de Puebla.
</p>
<!--<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('https://usep.digital/img/inscripcion/marcadseyt.jpg1'); background-position: 94% 0%;">-->
<div style="padding: 0 25%; background-repeat: no-repeat; background-image: url('<?php echo base_url('img/inscripcion/marcadseyt.jpg')?>'); background-position: 94% 0%;">
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px;">
    <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>


<div>
    <img src="<?php echo base_url('/img/inscripcion/footerdocs.jpg') ?>" />
</div>

<hr style="border 1px dotted #444;" />

<div>
    <img src="<?php echo base_url('/img/inscripcion/headerdocs.jpg') ?>" />
</div>
<p style="text-align: center; font-weight: bold; text-align: center; font-size: 0.7rem;">
    “Universidad de la Salud” del Estado de Puebla<br />
    Secretaría de Planeación, Evaluación y Tecnologías de la Información<br />
    Dirección de Servicios Escolares y Titulación<br />
    Formato de consentimiento para uso de datos personales<br />
</p>
<p style="text-align: justify; margin: 5px 20px; font-size: 0.7rem;">
    El(la) que suscribe: <strong><u><?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?></u></strong> en mi carácter de estudiante/aprendiente inscrito(a) 
    en la <strong><u><?php echo $carrera->nombre; ?></u></strong>, con número de matrícula: <strong><u><?php echo $alumno->id ?></u></strong>, en 
    cumplimiento al Artículo 11 inciso c, del REGLAMENTO DE INGRESO, PERMANENCIA Y EGRESO DE LOS APRENDIENTES DE LA 
    UNIVERSIDAD DE LA SALUD DEL ESTADO DE PUEBLA, entiendo, estoy de acuerdo y doy mi consentimiento para  que la Dirección 
    de Servicios Escolares y Titulación (DSEyT) de la Universidad de la Salud del Estado de Puebla (USEP), recabe, utilice y 
    conserve mis datos personales de conformidad con las disposiciones de la Ley General de Protección de Datos Personales en 
    Posesión de Sujetos obligados (LGPDPPSP), y la Ley de Protección de Datos Personales en Posesión de sujetos obligados del 
    Estado de Puebla (LPDPPSPEP); los artículos 134, 135 y 136 de la Ley de Transparencia y Acceso a la Información Pública del 
    Estado de Puebla y demás disposiciones que resulten responsables, para los efectos de prestar servicios educativos en el nivel 
    superior, según lo establecido de manera expresa en el Aviso de Privacidad y Protección de Datos Personales que Resguarda la 
    Universidad de la Salud; por lo que la DSEyT-USEP no podrá utilizar o divulgar mis datos personales para propósitos distintos 
    a los de la realización de sus fines fundamentales, en el ámbito educativo y que cumplan con los principios de pertinencia y finalidad.
</p>
<p  style="text-align: justify; margin: 5px 20px;  font-size: 0.7rem;">
    Para dicho efecto y finalidad, doy consentimiento expreso voluntario a la DSEyT-USEP, para que transmita mis datos personales a las 
    dependencias universitarias o entidades vinculadas en conformidad con los Artículos 6, 7, 8 y 9 de la Ley de Protección de Datos Personales 
    en Posesión de los Sujetos Obligados del Estado de Puebla.
</p>
<div style="padding: 0 25%";>
    <div style="text-align: center; border-top: 1px solid #000; margin-top: 50px; font-size: 0.7rem;">
        <?php echo $alumno->nombre.' '.$alumno->apPaterno.' '.$alumno->apMaterno; ?><br />
        H. Puebla de Zaragoza, a <?php echo date("d") ?> de julio del 2022.
    </div>
</div>
