<?php

namespace App\Controllers;

use App\Entities\HorarioDocente;
use App\Models\GrupoModel;
use App\Models\HorarioDocentesModel;
use App\Models\BitacoraAulasModel;
use App\Entities\BitacoraAulas;
use App\Models\BitacoraAulasViewModel;
use App\Entities\BitacoraViewAulas;
use App\Models\AulasModel;
use App\Models\AsistenciasDiariasModel;
use App\Entities\AsistenciasDiarias;
use App\Models\PeriodoModel;
use App\Models\CapturaCalificacionesModel;
use App\Models\AsistenciasAlumnosModel;
use App\Models\EmpleadosModel;


class SubdireccionAcademica extends BaseController
{
    private $periodoModel;
    private $HorarioDocentesModel;
    private $CapturaCalificacionesModel;
    private $AsistenciasDiarias;
    private $AsistenciasAlumnos;
    private $empleadosModel;
    private $grupoModel;
    
    public function __construct()
    {
        $this->periodoModel = new PeriodoModel();
        $this->HorarioDocentesModel = new HorarioDocentesModel();
        $this->CapturaCalificacionesModel = new CapturaCalificacionesModel();
        $this->AsistenciasDiarias = new AsistenciasDiariasModel();
        $this->AsistenciasAlumnos = new AsistenciasAlumnosModel();
        $this->empleadosModel = new EmpleadosModel();
        $this->grupoModel = new GrupoModel();
    }
    
    /**
     * recaba los datos para que un subdirector pase lista por un docente
     * @return view vista donde un subdirector selecciona un docente para pasar lista a uno de sus grupos
     */

    public function paseListaDocentes(){
        $docentesids=array();
        $rol =$this->sesion->get('rol_id');
        $periodoid = $this->periodoModel->periodoFsys ()->periodo_id;
        switch ($rol) {
            case 1:
                $docentes = $this->grupoModel->getDocentes($periodoid);
                break;
            case 9:
                $docentes = $this->grupoModel->like('grupo_clave','M','after')->getDocentes($periodoid);
                break;
            case 23:
                $docentes = $this->grupoModel->like('grupo_clave','M','after')->getDocentes($periodoid);
                break;
            case 10:
                $docentes = $this->grupoModel->like('grupo_clave','L','after')->getDocentes($periodoid);
                    break;
            case 22:
                $docentes = $this->grupoModel->like('grupo_clave','L','after')->getDocentes($periodoid);
                    break;
            }
        
        foreach ($docentes as $docente){
            $docentesids[]=$docente->grupo_docente;
        }
        $docenteinfo =$this->empleadosModel->findByIds($docentesids);
        $data['docenteinfo'] = $docenteinfo;
        $data['menu'] = $this->sesion->get('menu');
        return view ('docentes/asistenciasporSubdireccion',$data);
    }

}