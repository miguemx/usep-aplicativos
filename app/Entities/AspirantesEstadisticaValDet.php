<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesEstadisticaValDet extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'n_mujeres_enf' => null,
        'n_hombres_enf' => null,
        'n_mujeres_med' => null,
        'n_hombres_med' => null,
        'validado'      => null,
        'carrera'       => null,
        'sexo'          => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [

        'n_mujeres_enf' => 'm_enf',
        'n_hombres_enf' => 'h_enf',
        'n_mujeres_med' => 'm_med',
        'n_hombres_med' => 'h_med',
        'status'        => 'aspirante_validado',
        'carrera'       => 'aspirante_carrera',
        'sexo'          => 'aspirante_sexo',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}