<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="row ">
            <div class="col">
                <div class="mb-3">
                    <label for="select_evaluacion" class="form-label">Selecciona un tipo de estadística</label>
                    <select class="form-select form-select-sm" name="select_tipo" id="select_tipo" onchange="activar_periodos(this)">
                    <option value="0">Seleccione un tipo disponible</option>
                        <option value="1">Participación de Alumnos(Carga de Materias)</option>
                        <option value="3">Participación de Alumnos(Cuestionarios)</option>
                        <option value="2">Docentes</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="select_periodo" class="form-label">Seleccione un periodo</label>
                    <select class="form-select form-select-sm" name="select_periodo" id="select_periodo" onchange="getEvaluacionesDisponibles(this)" disabled>
                        <option value="null">Seleccione un periodo disponible</option>
                        <?php foreach ($periodos as $periodo_select) : ?>
                            <option value="<?php echo $periodo_select->id ?>"><?php echo $periodo_select->id ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="select_evaluacion" class="form-label">Selecciona una evaluacion</label>
                    <select class="form-select form-select-sm" name="select_evaluacion" id="select_evaluacion" onchange="activar_boton(this)" disabled>
                    </select>
                </div>
            </div>
            <div class="col-2 mt-3 text-center">
                <button type="button" class="btn btn-secondary" id="btn_buscar" disabled onclick="buscar()">Buscar</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="contenedor_documentos" class="card-body bg-white rounded shadow p-2" hidden>
    </div>

</div>
<script>
    
    var getEvaluacionesDisponibles = async (periodo) => {
        if (periodo.value != "null") {
            console.log(periodo.value);
            var evaluacion = document.getElementById('select_evaluacion');
            evaluacion.innerHTML = '';
            let response = await fetch('<?php echo base_url("EstadisticasEvaluacionDocente/getEvaluacionesPeriodos") ?>' + '/' + periodo.value, {
                method: 'POST',
            }).then(response => response.json()).then(result => {
                console.log(result);
                evaluacion.disabled = false;
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'Selecciona una opcion';
                evaluacion.appendChild(option);
                result.forEach(element => {
                    var option = document.createElement('option');
                    option.value = element.id;
                    option.innerHTML = element.titulo;
                    evaluacion.appendChild(option);
                });
            }).catch(error => {
                var option = document.createElement('option');
                option.value = 0;
                option.innerHTML = 'No hay evaluaciones disponibles en este periodo';
                evaluacion.appendChild(option);
            });
        } else {
            alert('seleccione un periodo diferente');
        }
    }

    function activar_boton(evaluacion) {
        if (evaluacion.value != 0) {
            document.getElementById('btn_buscar').disabled = false;
        } else {
            document.getElementById('btn_buscar').disabled = true;

        }
    }

    function activar_periodos(evaluacion) {
        if (evaluacion.value != 0) {
            document.getElementById('select_periodo').disabled = false;
        } else {
            document.getElementById('select_periodo').disabled = true;

        }
    }

    var buscar = async () => {
        var estadistica = document.getElementById('select_tipo').value;
        var evaluacion = document.getElementById('select_evaluacion').value;
        var periodo = document.getElementById('select_periodo').value;
        if (estadistica !='0'){
            if (estadistica =='1'){
                let contenedor = document.getElementById('contenedor_documentos');
                contenedor.innerHTML = '';
                contenedor.hidden = false;
                var div = document.createElement('div');
                var titulo = document.createElement('label');
                titulo.innerHTML = '<label style="margin: 0 10px"><h4>Participación de Alumnos (Carga de Materias)</h4></label>';
                div.appendChild(titulo);
                contenedor.appendChild(div);
                var botonppa = document.createElement('a');
                botonppa.setAttribute('href', "<?php echo base_url('EstadisticasEvaluacionDocente/participacionalumnos/') ?>/" + periodo + '/'+ evaluacion);
                botonppa.classList.add('btn', 'btn-primary', 'm-2');
                botonppa.innerHTML="Ver Participación de Alumnos";
                contenedor.appendChild(botonppa);
            } else if (estadistica==3){
                let contenedor = document.getElementById('contenedor_documentos');
                contenedor.innerHTML = '';
                contenedor.hidden = false;
                var div = document.createElement('div');
                var titulo = document.createElement('label');
                titulo.innerHTML = '<label style="margin: 0 10px"><h4>Participación de Alumnos (Cuestionarios)</h4></label>';
                div.appendChild(titulo);
                contenedor.appendChild(div);
                var botonppa = document.createElement('a');
                botonppa.setAttribute('href', "<?php echo base_url('EstadisticasEvaluacionDocente/participacionalumnosv2/') ?>/" + periodo + '/'+ evaluacion);
                botonppa.classList.add('btn', 'btn-primary', 'm-2');
                botonppa.innerHTML="Ver Participación de Alumnos";
                contenedor.appendChild(botonppa);
            }else{
                if (periodo != '0') {
                    if (evaluacion != 'null') {
                        let estadisticos = await fetch('<?php echo base_url("EstadisticasEvaluacionDocente/getDatosEstadisticos") ?>' + '/' + periodo + '/' + evaluacion, {
                            method: 'POST',
                        }).then(estadisticos => estadisticos.json()).then(documentos => {
                            console.log(documentos);
                            // var evdoc = documentos.evdoc;
                            let contenedor = document.getElementById('contenedor_documentos');
                            contenedor.innerHTML = '';
                            contenedor.hidden = false;
                            var div = document.createElement('div');
        
                            var titulo = document.createElement('label');
                            titulo.innerHTML = '<label style="margin: 0 10px"><h4>EVDOC</h4></label>';
                            div.appendChild(titulo);
                            contenedor.appendChild(div);
        
                            documentos.evdoc.forEach(item => {
                                var a = document.createElement('a');
                                a.setAttribute('href', "<?php echo base_url('EstadisticasEvaluacionDocente/ExcelEVDOC/') ?>/" + periodo + '/' + item + '/' + evaluacion);
                                a.setAttribute('button', "button");
                                a.classList.add('btn', 'btn-primary', 'm-2');
        
                                var inner = item.split('_');
                                a.innerHTML = inner[0] + " " + inner[2] + "/" + inner[3] + "/" + inner[4] + " " + inner[5] + ":" + inner[6];
                                contenedor.appendChild(a);
                            });
        
                            var div = document.createElement('div');
        
                            var titulo = document.createElement('label');
                            titulo.innerHTML = '<label style="margin: 0 10px"><h4>EVDOCMAT</h4></label>';
                            div.appendChild(titulo);
                            contenedor.appendChild(div);
                            documentos.docmat.forEach(item => {
                                var a = document.createElement('a');
                                a.setAttribute('href', "<?php echo base_url('EstadisticasEvaluacionDocente/estDocentesMaterias/') ?>/" + periodo + '/' + item + '/' + evaluacion);
                                a.setAttribute('button', "button");
                                a.classList.add('btn', 'btn-primary', 'm-2');
        
                                var inner = item.split('_');
                                a.innerHTML = inner[0] + " " + inner[2] + "/" + inner[3] + "/" + inner[4] + " " + inner[5] + ":" + inner[6];
                                contenedor.appendChild(a);
                            });
        
                        }).catch(error => {
                            console.log(error);
                        });
                    }
                }
            }
        }
    }
</script>
<?php echo $this->endSection() ?>