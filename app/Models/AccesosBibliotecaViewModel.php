<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\AccesoBiblioteca;

class AccesosBibliotecaViewModel extends Model {
    protected $table      = 'view_accesos_biblioteca';
    protected $primaryKey = 'biblioacceso_id';

    protected $returnType    = 'App\Entities\AccesoBibliotecaView';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca registros de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $this->creaFiltros( $filtros );
        $registros = $this->findAll();
        return $registros;
    }

    /**
     * busca registros de acuerdo a sus filtros y regresa el resultado paginado
     * @param filtros (opcional) un arreglo con los filtros para buscar
     * @return registros devuelve un arreglo con los registros encontrados
     */
    public function paginar($filtros=array()) {
        $pager = \Config\Services::pager();
        //$this->withDeleted()->creaFiltros( $filtros );
        $this->creaFiltros( $filtros );
        $registros = $this->paginate(25);
        return $registros;
    }

    /**
     * devuelve un arreglo de objetos con los usuarios que tienen solo registro de entrada en la biblioteca
     * @return aforo un arreglo con objetos de tipo AccesoBiblitecaView
     */
    public function aforo() {
        $this->where( 'biblioacceso_entrada IS NOT NULL' );
        $this->where( 'biblioacceso_salida IS NULL' );
        $this->orderBy( 'biblioacceso_entrada','DESC' );
        $aforo = $this->findAll();
        return $aforo;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('inicio', $filtros) ) {
            if ( strlen($filtros['inicio']) ) {
                $this->where( 'biblioacceso_entrada >=', $filtros['inicio'].' 00:00:01' );
            }
        }
        if ( array_key_exists('fin', $filtros) ) {
            if ( strlen($filtros['fin']) ) {
                $this->where( 'biblioacceso_entrada <=', $filtros['fin'].' 23:59:59' );
            }
        }
        if ( array_key_exists('entrada', $filtros) ) {
            if ( $filtros['entrada'] == 'SI' ) {
                $this->where( 'biblioacceso_entrada IS NOT NULL' );
            }
        }
        
        $this->orderBy('biblioacceso_entrada','DESC');

    }

}