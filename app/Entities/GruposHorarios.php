<?php

namespace App\Entities;

use CodeIgniter\Entity;

class GruposHorarios extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'grupo' => NULL,
        'dia' => NULL,
        'h_inicio' => NULL,
        'h_fin' => NULL,
        'aula' => NULL,
        'periodo' => NULL,
        'seccion' => NULL,
        'folio' => NULL,
        'materia' => NULL,
        'docente' => NULL,
        'cupo' => NULL,
        'ocupacion' => NULL,
        'au_nombre' => NULL,
        'au_descripccion' => NULL,
        'au_cupo' => NULL,
        'carrera' => NULL,
        'ma_nombre' => NULL,
        'ma_nombre_c' => NULL,
        'semestre' => NULL,
        'em_correo' => NULL,
        'em_nombre' => NULL,
        'em_apellido' => NULL,
        'em_paterno' => NULL,
        'em_materno' => NULL,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [
        'grupo' => 'gpoaula_grupo',
        'dia' => 'gpoaula_dia',
        'h_inicio' => 'gpoaula_inicio',
        'h_fin' => 'gpoaula_fin',
        'aula' => 'gpoaula_aula',
        'periodo' => 'gpoaula_periodo',
        'seccion' => 'grupo_clave',
        'folio' => 'grupo_folio',
        'materia' => 'grupo_materia',
        'docente' => 'grupo_docente',
        'cupo' => 'grupo_max',
        'ocupacion' => 'grupo_ocupado',
        'au_nombre' => 'aula_nombre',
        'au_descripccion' => 'aula_descripccion',
        'au_cupo' => 'aula_cupo_mx',
        'carrera' => 'materia_carrera',
        'ma_nombre' => 'materia_nombre',
        'ma_nombre_c' => 'materia_nombre_corto',
        'semestre' => 'materia_semestre',
        'em_correo' => 'empleado_correo',
        'em_nombre' => 'empleado_nombre',
        'em_apellido' => 'empleado_apellido',
        'em_paterno' => 'empleado_ap_paterno',
        'em_materno' => 'empleado_ap_materno',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
