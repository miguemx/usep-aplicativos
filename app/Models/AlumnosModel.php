<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AlumnosModel extends Model {
    protected $table      = 'alumnos_generales';
    protected $primaryKey = 'alumno_id';

    protected $returnType    = 'App\Entities\Alumno';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        'alumno_ap_paterno','alumno_ap_materno','alumno_nombres','alumno_periodo','alumno_semestre','alumno_comentario','deleted_at','alumno_status',
        'alumno_correo_personal','alumno_telefono','alumno_calle','alumno_colonia','alumno_numero','alumno_num_int','alumno_cp',
        'alumno_municipio','alumno_estado','alumno_pais','alumno_nac_fecha','alumno_nac_municipio','alumno_nac_estado','alumno_nac_pais',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un alumno mediante su direccion de correo
     * @param correo el correo que se va a buscar
     * @return Alumno entidad de Alumno con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByEmail($correo) {
        $alumno = $this->where('alumno_correo',$correo)->first();
        return $alumno;
    }

    /**
     * Ontiene toda la informacion de un estudiante
     * @param matricula el ID o matricula del estudiante
     * @return App\Models\Alumno el objeto alumno con toda su informacion
     * @return null si no se encuentra el estudiante
     */
    public function getInfo($matricula) {
        $alumno = $this->withDeleted()->find( $matricula );
        if ( !is_null($alumno) ) {
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find( $alumno->carrera );
            $alumno->carrera = $carrera;

            $segurosModel = new AlumnosSegurosModel();
            $seguro = $segurosModel->find( $alumno->id );
            $alumno->ssn = $seguro->ssn;
        }
        return $alumno;
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        //$this->withDeleted()->creaFiltros( $filtros );
        $this->creaFiltros( $filtros );
        $alumnos = $this->paginate(25);
        return $alumnos;
    }

    /**
     * busca un estudiante por su nombre completo empezando por los apellidos;
     * el nombre solo viene separado por espacios
     * @param apellidosNombre la cadena con los apellidos y nombre solo separados por espacios
     * @return Alumno la entidad de alumno con el alumno encontrado
     * @return null en caso de que no exista o que se encuentren duplicados
     */
    public function buscaPorCompleto($apellidosNombre) { 
        $datosNombre = explode(' ',$apellidosNombre,3);
        $this->creaFiltros( ['apPaterno'=>trim($datosNombre[0]), 'apMaterno'=>trim($datosNombre[1]),'nombre'=>trim($datosNombre[2])] );
        $alumnos = $this->findAll();
        if ( count($alumnos) === 1  ) {
            return $alumnos[0];
        }
        return null;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['id']) ) {
                $this->where( 'alumno_id', $filtros['id'] );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'alumno_ap_paterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'alumno_ap_materno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'alumno_nombres', trim($filtros['nombre']), 'both' ); 
            }
        }
        if ( array_key_exists('carrera', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['carrera']) ) {
                $this->where( 'alumno_carrera', $filtros['carrera'] ); 
            }
        }
        //$this->where('alumno_status','ACTIVO');
        //$this->withDeleted();
        $this->orderBy('alumno_ap_paterno','ASC');
        $this->orderBy('alumno_ap_materno','ASC');
        $this->orderBy('alumno_nombres','ASC');

    }
    public function findById($id) {
        $alumno = $this->where('alumno_id',$id)->findAll();
        return $alumno;
    }
    public function findssall(){
       $alumno = $this->findAll();
       return  $alumno;
    }
    public function arrpruebas(){
        $alumno = $this->findAll();
        $arr = [
            $alumno[239],$alumno[81],$alumno[92],$alumno[123],
            $alumno[249],$alumno[600],$alumno[401],$alumno[112],
            $alumno[44],$alumno[601],$alumno[402],$alumno[113],
            $alumno[45],$alumno[602],$alumno[403],$alumno[114],
            $alumno[46],$alumno[603],$alumno[404],$alumno[115],
            $alumno[47],$alumno[604],$alumno[405],$alumno[116],
            $alumno[48],$alumno[605],$alumno[406],$alumno[117],
            $alumno[49],$alumno[606],$alumno[407],$alumno[118],
            $alumno[50],$alumno[607],$alumno[408],$alumno[119],
            $alumno[51],$alumno[608],$alumno[409],$alumno[120],
            $alumno[52],$alumno[609],$alumno[410],$alumno[121],
            $alumno[53],$alumno[610],$alumno[411],$alumno[122],
            $alumno[54],$alumno[611],$alumno[412],$alumno[123],
            $alumno[55],$alumno[612],$alumno[413],$alumno[142],
            $alumno[56],$alumno[613],$alumno[41],$alumno[124],
            $alumno[57],$alumno[614],$alumno[414],$alumno[192],
            $alumno[58],$alumno[615],$alumno[415],$alumno[162],
            $alumno[59],$alumno[616],$alumno[416],$alumno[482],
            $alumno[60],$alumno[617],$alumno[417],$alumno[125],
       ];
       return $arr;
    }
    public function updateperiodo($id_alumno,$arr_update){
        return $this
        ->db
        ->table('alumnos_generales')
        ->where(["alumno_periodo" => '2021A', '	alumno_id ' => $id_alumno])
        ->set($arr_update)
        ->update();

    }
    public function updatecalificacion($id,$grupo,$arreglo){
        //$resultados = $this->db
        return $this->db
        ->table('aca_grupos_alumnos')
        ->where(["gpoalumno_alumno" => $id, 'gpoalumno_grupo' => $grupo])
        ->set($arreglo)
        //->getCompiledUpdate();
        ->update();

        //return $resultados;

    }

    /**
     * retorna arreglo de alumnos en el estatus seleccionado
     * @param estatus
     */
    public function getByEstatus($estatus)
    {
        return $this->where('alumno_status',$estatus)->find();
    }
    public function getByEstatusPeriodo($estatus)
    {
        return $this->where('alumno_periodo',"2022B")
                    ->where('alumno_status',$estatus)->find();
    }

    public function buscarBaja($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->withDeleted()->creaFiltrosBaja( $filtros);
        $alumnos = $this->paginate(25);
        return $alumnos;
    }
    private function creaFiltrosBaja($filtros) {
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['id']) ) {
                $this->where( 'alumno_id', $filtros['id'] );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'alumno_ap_paterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'alumno_ap_materno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'alumno_nombres', trim($filtros['nombre']), 'both' ); 
            }
        }
        if ( array_key_exists('carrera', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['carrera']) ) {
                $this->where( 'alumno_carrera', $filtros['carrera'] ); 
            }
        }
        $this->where('alumno_status !=', 'ACTIVO');
        $this->orderBy('alumno_ap_paterno','ASC');
        $this->orderBy('alumno_ap_materno','ASC');
        $this->orderBy('alumno_nombres','ASC');
    }
    public function busca($matricula){
        $consulta = $this->withDeleted()->find($matricula);
        return $consulta;
    }

/**
 * cuenta alumnos activos por especialidad 
 * @param especialidad criterio del conteo
 * @return cuenta cantidad de alumnos
 */
    public function cuentaAlumnos($especialidad){
        $cuenta= $this->select ('COUNT(alumno_id) as n_alumnos')
        ->where  ('alumno_carrera',$especialidad)
        ->where  ('alumno_status','ACTIVO')
        ->first();
        return $cuenta;
    }
}