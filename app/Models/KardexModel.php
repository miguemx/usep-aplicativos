<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class KardexModel extends Model {
    protected $table      = 'view_horario_kardex';
    protected $primaryKey = 'gpoalumno_alumno';

    protected $returnType    = 'App\Entities\Kardex';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'gpoalumno_alumno','gpoalumno_grupo','gpoalumno_ordinario','gpoalumno_extraordinario','aprobada',
        'grupo_id','grupo_clave','grupo_folio','grupo_materia','grupo_docente',
        'grupo_periodo','grupo_max','grupo_ocupado',
        'periodo_id','periodo_nombre','periodo_inicio','periodo_fin','materia_clave',
        'materia_carrera','materia_nombre','materia_nombre_corto','materia_creditos','materia_obligatoria',
        'materia_tipo','grupo_idc','materia_semestre','gpoalumno_flg_pago','periodo_estado',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca el kardex del alumno especificado
     * @param alumno el objeto de entidad \App\Entities\Alumno con el alumno a buscar su kardex
     * @return kardex un array asociativo con los datos de los periodos del alumno, devuelve arreglo vacío si no se encuentra el kardex asociado
     */
    public function alumno($alumno) {
        $kardex = [];
        if ( !is_null($alumno) ) {
            try {
                $this->where( 'gpoalumno_alumno', $alumno->id );
                //$this->where("periodo_id != '".$alumno->periodo."' ");
                $this->orderBy('periodo_inicio','ASC');
                $this->orderBy('materia_clave','ASC');
                $this->notLike('grupo_materia','MC-TUT');
                $this->notLike('grupo_materia','LEO-TUT');
                $this->notLike('grupo_materia','MC-SIM');
                $registros = $this->findAll();
                foreach ( $registros as $registro ) {
                    $kardex[ $registro->periodo ][] = $registro;
                }
            }
            catch ( \Exceptio $ex ) {
                log_message('error', '[ERROR] {exception}', ['exception' => $e]);
            }
        }
        return $kardex;
    }

    /**
     * busca el kardex del alumno especificado
     * @param matricula el ID del alumno
     * @return kardex un array asociativo con los datos de los periodos del alumno
     */
    public function horario($matricula) {
        $horario = [];
        $this->where('gpoalumno_alumno',$matricula);
        $this->where('gpoalumno_ordinario IS NULL ');
        $this->where('gpoalumno_extraordinario IS NULL ');
        $this->notLike('materia_clave','TUT','before');
        $this->notLike('grupo_materia','MC-SIM');
        $this->orderBy('periodo_inicio','ASC');
        $this->orderBy('materia_clave','ASC'); 
        $registros = $this->findAll();
        foreach ( $registros as $registro ) {
            $horario[] = $registro;
        }
        return $registros;
    }

    /**
     * retorna las materias que cursa en el periodo actual
     */
    public function getMateriasActulales($matricula,$periodo)
    {
        $horario = [];
        $this->where('gpoalumno_alumno',$matricula);
        $this->where('grupo_periodo',$periodo);
        $this->notLike('materia_clave','TUT','before');
        $this->notLike('materia_clave','SIM','before');
        $this->notLike('materia_nombre','Práctica Clínica','both');
        $this->orderBy('periodo_inicio','ASC');
        $this->orderBy('materia_clave','ASC'); 
        $registros = $this->findAll();
        foreach ( $registros as $registro ) {
            $horario[] = $registro;
        }
        return $horario;
    }
    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID 
            if ( strlen($filtros['id']) ) {
                $this->where( 'alumno_id', $filtros['id'] );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->where( 'alumno_ap_paterno', $filtros['apPaterno'] );
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->where( 'alumno_ap_materno', $filtros['apMaterno'] );
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->where( 'alumno_nombres', $filtros['nombre'] );
            }
        }

        $this->orderBy('alumno_ap_paterno','ASC');
        $this->orderBy('alumno_ap_materno','ASC');
        $this->orderBy('alumno_nombres','ASC');
    }

    /**
     * realiza una actializacion de calificacion de acuerdo a las llaves de la tabla
     * @param matricula la matricula del estudiante a editar
     * @param grupo el id del grupo en el que se va a modificar la calificaicon
     * @param calificacion el numero de la calificacion
     * @param tipo el tipo de calificacion a registrar
     * @param nota el comentario de porque se cambia la nota
     */
    public function actualiza( $matricula, $grupo, $calificacion, $tipo, $nota ) {
        try {
            $updateAprobada = "aprobada = '0'";
            $updateCalif = "gpoalumno_ordinario = :calificacion:, gpoalumno_extraordinario = NULL ";
            if ( strtoupper($tipo) == 'EXT' ) {
                $updateCalif = "gpoalumno_ordinario = NULL, gpoalumno_extraordinario = :calificacion:";
            }
            if ( intval($calificacion) > 5 ) {
                $updateAprobada = "aprobada = '1'";
            }

            $sql = "UPDATE aca_grupos_alumnos 
                    SET $updateCalif, $updateAprobada, gpoalumno_comentario=:nota: 
                    WHERE gpoalumno_alumno = :matricula: AND gpoalumno_grupo = :grupo: ";
            
            $this->db->query($sql, [
                'matricula'     => $matricula,
                'grupo' => $grupo,
                'nota'   => $nota,
                'calificacion' => $calificacion,
            ]);
            log_message('notice', "[CALIF] Se anexa nota a la calificacion($calificacion) de $matricula en el grupo $grupo como lo siguiente: $nota");
            return true;
        }
        catch ( \Exception $ex ) {
            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
        }
        return false;
    }

    /**
     * busca una materia que un estudiante este cursando actualmente
     * @param matricula el ID o matrcula del estudiante
     * @param materia la clave de la materia
     */
    public function materiaCursando($matricula, $materia) {
        $this->where('gpoalumno_alumno',$matricula);
        $this->where('materia_clave',$materia);
        $this->where('gpoalumno_ordinario IS NULL');
        $this->where('gpoalumno_extraordinario IS NULL');
        $result = $this->findAll();
        if( count($result) === 1 ) {
            return $result[0];
        }
        return null;
    }
}