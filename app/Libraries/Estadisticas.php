<?php

namespace App\Libraries;

use App\Models\EstadisticaEvaluacionDocenteModel;
use App\Models\EvaluacionDocentePorcentajesModel;
use App\Models\EvaluacionesFormulariosModel;
use App\Models\EvaluacionesPreguntasModel;
use App\Models\EvaluacionesRespuestasModel;
use App\Models\EvaluacionModel;
use App\Models\GrupoAlumnoModel;
use App\Models\GruposMateriasModel;
use App\Models\PeriodoModel;

class Estadisticas
{
    private $periodoModel;
    private $gruposMateriasModel;
    private $grupoAlumnosModel;
    private $estadisticaEvaluacionDocenteModel;
    private $porcentajesEvaluacionModel;
    private $fromularioModel;
    private $preguntasModel;
    private $respuestasModel;
    private $evaluacionModel;


    public function __construct() {
        $this->periodoModel = new PeriodoModel();
        $this->gruposMateriasModel = new GruposMateriasModel();
        $this->grupoAlumnosModel = new GrupoAlumnoModel();
        $this->estadisticaEvaluacionDocenteModel = new EstadisticaEvaluacionDocenteModel();
        $this->porcentajesEvaluacionModel = new EvaluacionDocentePorcentajesModel();
        $this->fromularioModel = new EvaluacionesFormulariosModel();
        $this->preguntasModel = new EvaluacionesPreguntasModel();
        $this->respuestasModel = new EvaluacionesRespuestasModel();
        $this->evaluacionModel = new EvaluacionModel();
    }

    /**
     * funcion de prueba //eliminar despues de su uso
     * @param evaluacion ID de evaliacion seleccionado
     */
    public function generarEVDOC($evaluacion)
    {   
        
        $indicadores_por_grupo = array();
        $periodoid = $this->periodoModel->periodoFsys();
        $periodo = $periodoid->periodo_id;
        $grupos_detalles = $this->gruposMateriasModel->buscar(['mat_curriculares' => true, 'periodo' => $periodo/* , 'docente' => 200034 */]);
        // $grupos_detalles = $this->gruposMateriasModel->find(2201020133);
        $evaluacion_model = $this->evaluacionModel->find($evaluacion);
        $preguntas_evaluacion = $this->getArrPregruntas($evaluacion);
        foreach ($grupos_detalles as $grupo) {
            $total_alumnos = $this->grupoAlumnosModel->n_alumnosxgrupo($grupo->grupo_idc);
            // echo json_encode( $total_alumnos)."<br>";
            if (!is_null($total_alumnos)) {
                $nRespuestasClasificacion1 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo, $grupo->materia_clave, $grupo->docente, 1, $evaluacion, $grupo->grupo_idc);
                $nRespuestasClasificacion2 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo, $grupo->materia_clave, $grupo->docente, 2, $evaluacion, $grupo->grupo_idc);
                $nRespuestasClasificacion3 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo, $grupo->materia_clave, $grupo->docente, 3, $evaluacion, $grupo->grupo_idc);
                $nRespuestasClasificacion4 = $this->estadisticaEvaluacionDocenteModel->conteo_preguntas($periodo, $grupo->materia_clave, $grupo->docente, 4, $evaluacion, $grupo->grupo_idc);
                $totalC1 = $nRespuestasClasificacion1->n_preguntas * 4;
                $totalC2 = $nRespuestasClasificacion2->n_preguntas * 4;
                $totalC3 = $nRespuestasClasificacion3->n_preguntas * 4;
                $totalC4 = $nRespuestasClasificacion4->n_preguntas * 4;
                $porcentajes = $this->porcentajesEvaluacionModel->estxdocentematResp($evaluacion, $grupo->docente, $grupo->materia_clave, $grupo->grupo_idc);
                $clasificacion1 = (($totalC1 == 0) ? 0 : ((array_sum($this->forma_arreglo(1, $porcentajes)) / $totalC1) * 100));
                $clasificacion2 = (($totalC2 == 0) ? 0 : ((array_sum($this->forma_arreglo(2, $porcentajes)) / $totalC2) * 100));
                $clasificacion3 = (($totalC3 == 0) ? 0 : ((array_sum($this->forma_arreglo(3, $porcentajes)) / $totalC3) * 100));
                $clasificacion4 = (($totalC4 == 0) ? 0 : ((array_sum($this->forma_arreglo(4, $porcentajes)) / $totalC4) * 100));
                $alumnos_paricipantes = $this->getAlumnosEvaluacion($grupo->grupo_idc,  $preguntas_evaluacion);
                $secciones = explode('-', $grupo->clave);
                $materiahoras=true;
                $materiaid = $grupo->materia_clave;
                $participacion =  round((($alumnos_paricipantes * 100) / $total_alumnos[0]->alumno), 0);
                if (($materiaid=='LEO-02')||($materiaid=='LEO-04')||($materiaid=='LEO-14')||($materiaid=='LEO-21')||($materiaid=='LEO-35')||($materiaid=='LEO-42')||($materiaid=='LEO-48')||($materiaid=='MC-511')||($materiaid=='MC-521')){
                    $materiahoras= false;
                }
                $indicadores_por_grupo[] = array(
                    'idc'                       => $grupo->grupo_idc,
                    'tipo'                      => $evaluacion_model->evaluacion_titulo,
                    'PE'                        => (($grupo->carrera == 1) ? 'LEO' : 'LMC'),
                    'grupo'                     => $secciones[1],
                    'grupoS'                    => $grupo->clave,
                    'asignatura'                => mb_strtoupper($grupo->materia_clave . " " . $grupo->materiaNombre),
                    'horas_conducidas'          => (($materiahoras) ? $grupo->materia_horas_conducidas : 'sin especificar'),
                    'horas_semanales'           => (($materiahoras) ? $grupo->materia_horas_conducidas/18 : 'sin especificar'),
                    'tipo_contratacion'         => 'sin especificar',
                    'docente'                   => mb_strtoupper($grupo->apPaterno . " " . $grupo->apMaterno . " " . $grupo->empleado_nombre),
                    'alumnos_inscritos'         => $total_alumnos[0]->alumno,
                    'alumnos_participantes'     => $alumnos_paricipantes,
                    'significancia'             => (($participacion>=68) ? 'Significativo' : 'No Significativo'),
                    'participacion'             => $participacion,
                    'calidad_recursos'          => number_format($clasificacion1, 2, '.', ',') ,
                    'pertenencia_academica'     => number_format($clasificacion2, 2, '.', ',') ,
                    'relacion_academica'        => number_format($clasificacion3, 2, '.', ',') ,
                    'transmision_conocimientos' => number_format($clasificacion4, 2, '.', ',') ,
                    'promedio'                  => number_format(((($clasificacion1 + $clasificacion2 + $clasificacion3 + $clasificacion4) / 4)), 2, '.', ',') ,

                );
               
            }
        }
        return $indicadores_por_grupo;
    }

    
    /**
     * retorna el arreglo de las preguntas pertenecientes a una evaluacion
     */
    public function getArrPregruntas($evaluacion)
    {
        $formularios = $this->fromularioModel->getByEvaluacion($evaluacion);
        $array_preguntas = [];
        foreach ($formularios as $formulario) {
            $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
            foreach ($preguntas as $pregunta) {
                $array_preguntas[] = $pregunta->id;
            }
        }
        return $array_preguntas;
    }

      /**
     * forma un vector con las suma de las respuestas de la evaluacion
     * @param clasificacion rasgo a evaluar
     * @param arrayrespuestas array con las respuestas
     * @return arrayclasificacion vector con el factor para sacar el porcentaje del razgo a evaluar
     */
    public function forma_arreglo($clasificacion, $arrayrespuestas)
    {
        $arrayclas = [];
        $index = 0;
        switch ($clasificacion) {
            case 1:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    //$dato=intval($arrayclasificacion->clasificacion1);
                    $arrayclas[$index] = $arrayclasificacion->clasificacion1;
                    $index++;
                }
                break;
            case 2:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion2;
                    $index++;
                }
                break;
            case 3:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion3;
                    $index++;
                }
                break;
            case 4:
                foreach ($arrayrespuestas as $arrayclasificacion) {
                    $arrayclas[$index] = $arrayclasificacion->clasificacion4;
                    $index++;
                }
                break;
        }
        return $arrayclas;
    }

    /**
     * devuelve un arreglo de alumnos que contestaron al menos una pregunta de la evaluacion seleccionada
     * @param grupo ID del grupo que se esta analizando
     * @param array_preguntas arreglo de preguntas que pertenecen a una evaluacion
     * @return count Alumnos que contestaron a la evaluacion
     */
    public function getAlumnosEvaluacion($grupo, $array_preguntas)
    {
        $resp = $this->respuestasModel->getAlumnosGrupo($grupo, $array_preguntas);
        if (!is_null($resp))
            return count($resp);
        return 0;
    }

    /**
     * retorna todos los archivos EVDOC encontrados en el periodo seleccionado
     * @param periodo ID del periodo
     * @param evalucion ID de evaluacion
     * @return arr_evdoc contiene la ruta de las versiones generadas agrupadas por evaluacion
     */
    public function getlistacarpeta($periodo,$evaluacion)
    {
        $ruta = WRITEPATH . 'evaluacion/' . $periodo . '/';
        $arr_evdoc = array();
        $arr_evdocmat = array();
        if ($handler = opendir($ruta)) {
            while (false !== ($file = readdir($handler))) {
                    $datos = explode('_',$file);
                    if($datos[0]=='EVDOC'){
                        if($datos[1]==$evaluacion){
                            $arr_evdoc['evdoc'][] = $file;
                        }
                    }
                    if($datos[0]=='EVDOCMAT'){
                        if($datos[1]==$evaluacion){
                            $arr_evdoc['docmat'][]= $file;
                        }
                    }
            }
            closedir($handler);
        }
        if(count($arr_evdoc))
        return ($arr_evdoc);
        return false;
    }

    /**
     * retorna el listado de 
     */
}
