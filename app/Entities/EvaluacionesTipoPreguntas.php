<?php

namespace App\Entities;

use CodeIgniter\Entity;

class EvaluacionesTipoPreguntas extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'nombre' => null,
        'valores' => null,
        'clasificacion' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'id' => 'tipo_id',
        'nombre' => 'tipo_nombre',
        'valores' => 'tipo_valores',
        'clasificacion' => 'tipo_clasificacion',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
