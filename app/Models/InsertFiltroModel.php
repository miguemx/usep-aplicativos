<?php
namespace App\Models;
use CodeIgniter\Model;
use \App\Entities\InsertarFiltroSanitario;

class InsertFiltroModel extends Model{
    protected $table= 'filtro_sanitario';
    protected $primaryKey= 'filtro_folio';

    protected $returnType= 'App\Entities\InsertarFiltroSanitario';
    protected $useSoftDeletes = false;
    protected $allowedFields= [
        'filtro_correo','filtro_correo_externo','filtro_vacuna','filtro_diabetes','filtro_hipertension','filtro_antecedentecard','filtro_enfermedadres','filtro_otropadecimiento','filtro_fumador','filtro_mayorfiebre',
        'filtro_mayortos','filtro_mayordifrespiratoria','filtro_mayordolorcabeza','filtro_menordolormuscular','filtro_menordolorarticular','filtro_menorfatiga','filtro_menoralimentos',
        'filtro_menorabdominal','filtro_menoralteragusto','filtro_menoralteraolfato','filtro_menornasal','filtro_menorconjuntivitis','filtro_menorescalofrios','filtro_menorviaje','filtro_menorcovid',
        'filtro_menorotro','filtro_temp','filtro_oxigen','filtro_fecha','filtro_fechacontactocovid','filtro_fechapruebanegativo','filtro_fechapruebapositiva','filtro_tipotos'];

    public function lastId() {
        return $this->db->insertID();
    }
    
    public function lastIdcomor($CorreoComor,$bandera) {
        if ($bandera==0){
            $consulta =$this

                        ->where('filtro_correo',$CorreoComor)
                        ->orderby('filtro_folio','desc')->limit(1)->find();
         
        }
        else{
            $consulta=$this->where('filtro_correo_externo',$CorreoComor)
                        ->orderby('filtro_folio','desc')
                        ->limit(1)
                        ->find();
        }
        return $consulta;
    }
    public function actualizaregistros($folio, $temp,$oxigen){
        $sql = "UPDATE `filtro_sanitario` SET `filtro_temp` = '$temp',
                                            `filtro_oxigen` = '$oxigen',
                                            `filtro_fecha` = NOW()
                WHERE `filtro_folio`= '$folio'";
        $this->db->query($sql);

    }
}

?>
