<?php

namespace App\Models;

use CodeIgniter\Model;


class HorarioDocentesModel extends Model
{
    protected $table      = 'view_horario_docente';
    protected $primaryKey = 'grupo_id';

    protected $returnType    = 'App\Entities\HorarioDocente';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'grupo_id',
        'grupo_clave',
        'grupo_materia',
        'grupo_docente',
        'empleado_correo',
        'empleado_nombre',
        'empleado_apellido',
        'empleado_ap_paterno',
        'empleado_ap_materno',
        'grupo_periodo',
        'materia_nombre',
        'gpoaula_dia',
        'gpoaula_inicio',
        'gpoaula_fin',
        'aula_id',
        'aula_nombre',

    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function buscarGrupos($docente, $periodo)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)
        ->orderBy('grupo_id','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')
        ->findAll();
        return $consulta;
    }
    public function buscarSecciones($seccion)
    {
        $consulta = $this->where('grupo_clave', $seccion)
        ->orderBy('grupo_id','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')
        ->findAll();
        return $consulta;
    }
    public function buscarAula($aula)
    {
        $consulta = $this->where('aula_id', $aula)
        ->orderBy('grupo_id','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')
        ->findAll();
        return $consulta;
    }
    public function buscahorario($docente,$periodo,$dia,$hora)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)->where('gpoaula_dia',$dia)->where('gpoaula_inicio',$hora)
        ->orderBy('grupo_id','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')
        ->findAll();
        return $consulta;
    }
    public function buscahorarioAsistencias($docente,$periodo,$dia,$hora,$id_grupo)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)->where('gpoaula_dia',$dia)->where('gpoaula_inicio',$hora)->where('grupo_id',$id_grupo)
        ->orderBy('grupo_id','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')
        ->findAll();
        return $consulta;
    }
        /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array(), $iddocente,$periodo) {
        $pager = \Config\Services::pager();
        $this->withDeleted()->creaFiltros( $filtros, $iddocente,$periodo);
        $alumnos = $this->paginate(25);
        return $alumnos;
    }
    private function creaFiltros($filtros, $iddocente,$periodo) {
        if ( array_key_exists('grupo', $filtros) ) { 
            if ( strlen($filtros['grupo']) ) {
                $this->where('grupo_clave', $filtros['grupo']);
            }
        }
        if ( array_key_exists('materia', $filtros) ) {
            if ( strlen($filtros['materia']) ) {
                $this->where( 'grupo_materia', trim($filtros['materia']), 'both' ); 
            }
        }
        if ( array_key_exists('dias', $filtros) ) {
            if ( strlen($filtros['dias']) ) {
                $this->where( 'gpoaula_dia', trim($filtros['dias']), 'both' ); 
            }
        }
        $this->where('grupo_docente', $iddocente);
        $this->where('grupo_periodo', $periodo);
        $this->groupBy('grupo_id');
    }
    public function buscamaterias($docente, $periodo)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)
        ->groupBy('grupo_clave')
        ->findAll();
        return $consulta;
    }
    public function buscadias($docente, $periodo)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)
        ->groupBy('gpoaula_dia')
        ->findAll();
        return $consulta;
    }
    public function buscadocentes($carrera,$periodo){
        return $this->like('grupo_clave', $carrera, 'after')
        ->where('grupo_periodo', $periodo)
        ->groupBy('grupo_docente')
        ->findAll();
    }
    public function materiasxcarrera($id_docente,$periodo)
    {
        return $this->where('grupo_docente', $id_docente)
        ->where('grupo_periodo', $periodo)
        ->groupBy('grupo_clave')
        ->findAll();
    }
    public function docentesAll($periodo){
        return $this->where('grupo_periodo',$periodo)
        ->orderBy('grupo_docente','ASC')
        ->groupBy('grupo_docente')
        ->findAll();
    }
    public function materiasAll(){
        return $this->where('grupo_periodo',$periodo)
        ->groupBy('grupo_materia')
        ->findAll();
    }
}
