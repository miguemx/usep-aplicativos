<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Materia extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'clave' => null,
        'carrera' => null,
        'nombre' => null,
        'nombreCorto' => null,
        'creditos' => null,
        'obligatoria' => null,
        'tipo' => null,
        'clasificacion' => null,
        'serie' => null,
        'semestre' => null,
        'horas' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'clave' => 'materia_clave',
        'carrera' => 'materia_carrera',
        'nombre' => 'materia_nombre',
        'nombreCorto' => 'materia_nombre_corto',
        'creditos' => 'materia_creditos',
        'obligatoria' => 'materia_obligatoria',
        'tipo' => 'materia_tipo',
        'clasificacion' => 'materia_clasificacion ',
        'serie' => 'materia_seriacion',
        'semestre' => 'materia_semestre',
        'horas' => 'materia_horas_semanales',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];

}
