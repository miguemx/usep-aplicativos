<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroRespuestasModel extends Model {
    protected $table      = 'filtro_respuestas';
    protected $primaryKey = 'filtroresp_id';

    protected $returnType    = 'App\Entities\FiltroRespuesta';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'filtroresp_pregunta','filtroresp_folio','filtroresp_respuesta',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene las respuestas de un determinado folio, y las ordena en un array con las llaves de las preguntas
     * @param folio el folio a buscar
     * @return respuestas las respuestas acomodades por [ $preguntaId => $valor ]
     */
    public function folioArray( $folio ) {
        $respuestas = [];
        $this->where( 'filtroresp_folio', $folio );
        $resps = $this->findAll();
        foreach ( $resps as $resp ) {
            $respuestas[ $resp->pregunta ] = $resp->respuesta;
        }
        return $respuestas;
    }

}