<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">

    <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
        <div class="text-center form-group mt-4">
            <?php if ($evaluaciones) : ?>
                <div class="table-responsive-xl">
                    <table class="table table-striped-columns
                table-hover	
                table-borderless
                
                align-middle">
                        <thead class="table-light">
                            <caption>Evaluaciones Disponibles</caption>
                            <tr>
                                <th>Nombre</th>
                                <th>Tipo de evaluación</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            <?php foreach ($evaluaciones as $evaluacion) : ?>
                                <tr>
                                    <td><?php echo $evaluacion->titulo ?></td>
                                    <td><?php foreach ($tipos_evaluacion as $tipo) {
                                            if ($tipo->id == $evaluacion->tipo) echo $tipo->nombre;
                                        } ?></td>
                                    <td>
                                        <a name="" id="" class="btn btn-secondary" href="<?php echo base_url('Evaluaciones/Contestar/' . $evaluacion->id) ?>" role="button">Contestar</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            <?php else : ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?>