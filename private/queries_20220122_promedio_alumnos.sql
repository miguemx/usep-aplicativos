create view view_promedios_alumnos as (
SELECT 
	a.gpoalumno_alumno, 
    b.alumno_nombres, 
    b.alumno_ap_paterno, 
    b.alumno_ap_materno, 
    b.alumno_carrera, 
    b.alumno_semestre, 
    avg(a.gpoalumno_ordinario) as promedio 
    FROM 
    	aca_grupos_alumnos as a, 
        alumnos_generales b 
    WHERE 
    	a.gpoalumno_alumno = b.alumno_id and
        (b.deleted_at is null or b.deleted_at ="")
    group by 
    	a.gpoalumno_alumno
    order by b.alumno_carrera,b.alumno_semestre,promedio desc
)