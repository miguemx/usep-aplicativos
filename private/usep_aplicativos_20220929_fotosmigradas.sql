ALTER TABLE `alumnos_generales` ADD `alumno_foto` VARCHAR(120) NULL AFTER `alumno_nac_pais`;

UPDATE alumnos_generales
    INNER JOIN alumnos_credenciales ON alumno_id = credencial_alumno
SET alumno_foto = credencial_foto;