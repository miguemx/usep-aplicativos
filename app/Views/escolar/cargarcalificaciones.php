<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    
<div class="container-fluid">
    <form class="row g-3" method="post">
        
        <!-- <div class="col-12">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div> -->
        
    </form>
</div>
<div class="container-fluid row" style="margin-top: 15px;">

    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                Cargar calificaciones desde layout
            </div>
            <div class="card-body">
                <form class="form" method="post" action="<?php echo base_url('Escolar/CargarCalificaciones'); ?>" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <label for="layout" class="form-label">Selecciona el archivo con la plantilla llena</label>
                        <input type="file" class="form-control" id="layout" name="layout" />
                        <div id="helpLay" class="form-text">Aqui puedes cargar el layout definido.</div>
                    </div>
                    <button type="submit" name="lay" value="layout" class="btn btn-secondary">
                        Cargar
                    </button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                Cargar calificaciones desde OpenEducat
            </div>
            <div class="card-body">
                <form class="form" method="post" action="<?php echo base_url('Escolar/CargarCalificaciones'); ?>" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <label for="layout" class="form-label">Selecciona el archivo con la plantilla llena</label>
                        <input type="file" class="form-control" id="educat" name="layout" />
                        <div id="helpLay" class="form-text">Aqui puedes cargar el archivo exportado en Open Educat en formato CSV</div>
                    </div>
                    <button type="submit" name="lay" value="educat" class="btn btn-secondary">
                        Cargar
                    </button>
                </form>
            </div>
        </div>
    </div>


    

    
</div>


<?php echo $this->endSection() ?>
