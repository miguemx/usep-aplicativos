<?php

namespace App\Entities;

use CodeIgniter\Entity;

class EvaluacionDocentePorcentaje extends Entity
{


    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'clasificacion1' => null,
        'clasificacion2' => null,
        'clasificacion3' => null,
        'clasificacion4' => null,
        'valor' => null,
        'calve' => null,
        'materia_nombre' => null,
        'semestre' => null,
        'carrera' => null,
        'empleado' => null,
        'paterno' => null,
        'materno' => null,
        'nombre' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'clasificacion1' => 'clasificacion1',
        'clasificacion2' => 'clasificacion2',
        'clasificacion3' => 'clasificacion3',
        'clasificacion4' => 'clasificacion4',
        'valor' => 'respuestas_valor',
        'calve' => 'materia_clave',
        'materia_nombre' => 'materia_nombre',
        'semestre' => 'materia_semestre',
        'carrera' => 'materia_carrera',
        'empleado' => 'empleado_id',
        'paterno' => 'empleado_ap_paterno',
        'materno' => 'empleado_ap_materno',
        'nombre' => 'empleado_nombre',
        'grupo' => 'gpoalumno_grupo',
        'seccion' => 'grupo_clave',
        'evaluacion' => 'evaluacion_id',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
