<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesRespuestas;
use Google\Service\CloudFunctions\Retry;

class AspirantesRespuestasModel extends Model
{
    protected $table='aspirantes_respuestas';
    protected $primaryKey='respuesta_id';
    protected $returnType='App\Entities\AspirantesRespuestas';
    protected $useSoftDeletes = true;
    protected $allowedFields=['respuesta_id','respuesta_aspirante','respuesta_campo','respuesta_valor','respuesta_comentario'];
    protected $useTimestamps = true;
    protected $db;
    protected $builder;
    
    /**
     * indica si un campo ha sido contestado por un aspirante, devolviendo el registro del campo contestado, o null si no existe
     * @param campo el ID del campo
     * @param aspirante el ID del aspirante
     * @return AspirantesRespuestas un objeto con la respuesta realizada
     * @return null en caso de que no se haya respondido
     */
    public function contestado($campo, $aspirante) {
        $this->where( 'respuesta_campo', $campo );
        $this->where( 'respuesta_aspirante', $aspirante );
        $respuestas = $this->find(); 
        if ( count($respuestas) ) {
            return $respuestas[0];
        } 
        return null;
    }
    

    /**
     * Devuelve el listado de respuestas dado por el aspirante seleccionado
     * @param aspirante el ID del aspirante
     */
    public function getRespuestas($aspirante)
    {
        return $this->where('respuesta_aspirante',$aspirante)->find();
    }
    public function updateaspirantes($matricula,$datos,$numeros){
        return $this->db
        ->table('aspirantes_respuestas')
        ->where('respuesta_aspirante',$matricula)
        ->where('respuesta_campo',$numeros)
        ->set($datos)
        ->update();
    }
}

