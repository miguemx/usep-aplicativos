<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  
</head>
<br>
<br>
<h1 style=" text-align: center;">Estadísticas</h1>

<form class="row g-3" method="post" action="<?php echo base_url('Votaciones/estadisticas'); ?>">
  <div class="idproceso" style=" text-align: center;">
    <br><br>
    <label for="selectproceso" class="form-label">Selecciona un proceso</label>
    <br>
        <select id="selectproceso" class="custom-select" style="width: 40%;" name="selectproceso"  onchange="this.form.submit(); "  required>
          <option value="">Selecciona procesos</option>
          <?php foreach ($procesos as $arregloprocesos) : ?>
            <option value = <?php echo $arregloprocesos->id ?> <?php if($procesoid== $arregloprocesos->id) echo 'selected="selected"'; ?>> <?php
            echo $arregloprocesos->nombre
            ?></option>
          <?php endforeach; ?>
        </select>
  </div>
</form>
<br><br>
<figure class="highcharts-figure">
  <div id="container"></div>

  <table id="datatable">
    <thead>
      <tr>
        <th></th>
        <?php foreach ($candidatos as $candidato) : ?>
            <th><?php echo $candidato->nombre_candidatos ; ?></th>          
        <?php endforeach; ?>
      </tr>
    </thead>
    <tbody>
      <tr>
      <th>Votos</th>
      <?php foreach ($candidatos as $voto) : ?>
          <td><?php echo $voto->voto_candidatos ; ?></td>
      <?php endforeach; ?>
      </tr>
    </tbody>
  </table>
</figure>
<form class="row g-3; " method="post" action="<?php echo base_url('Votaciones/ganador'); ?>" >
  <div class="idproceso" style=" text-align: center;">
    <br><br>
    <label for="selectproceso" class="form-label">Da click aquí para dirigirte a a los resultados finales del proceso seleccionado</label>
    <br>
      <input name="idproceso" type="hidden" value="<?php echo $procesoid; ?>" />
      <button id="ganador" type="submit" name='padron' class="btn btn-secondary" disabled>Mostrar ganador</button>
  </div>
</form>
<script>
    Highcharts.chart('container', {
    data: {
      table: 'datatable'
    },
    chart: {
      type: 'column'
    },
    title: {
      text: 'Estadisticas de votos al momento'
    },
    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Votos'
      }
    },
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          this.point.y + ' ' + this.point.name.toLowerCase();
      }
    }
  });
</script>
<script>
    function ocultaganador(){
      let idselect = document.getElementById('selectproceso').value;
      console.log(idselect);
        if(idselect != ""){
          document.getElementById("ganador").disabled = false;
        }
        else{
          document.getElementById("ganador").disabled = true;
        }
      }
      ocultaganador();
</script>
<?php echo $this->endSection() ?>

