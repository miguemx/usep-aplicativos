<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\PagoConcepto;

class PagosConceptosModel extends Model {
    protected $table      = 'pagos_conceptos';
    protected $primaryKey = 'concepto_id';

    protected $returnType    = 'App\Entities\PagoConcepto';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'concepto_nombre','concepto_descripcion','concepto_monto',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros );
        $alumnos = $this->paginate(25);
        return $alumnos;
    }
    
    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['id']) ) {
                $this->where( 'alumno_id', $filtros['id'] );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'alumno_ap_paterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'alumno_ap_materno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'alumno_nombres', trim($filtros['nombre']), 'both' ); 
            }
        }
        if ( array_key_exists('carrera', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['carrera']) ) {
                $this->where( 'alumno_carrera', $filtros['carrera'] ); 
            }
        }

        $this->orderBy('alumno_ap_paterno','ASC');
        $this->orderBy('alumno_ap_materno','ASC');
        $this->orderBy('alumno_nombres','ASC');
    }
}