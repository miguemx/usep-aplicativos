<div>
    <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
    <p>
        Bienvenido <strong><?php echo $nombre ?></strong>
    </p>
    <p>
    Te informamos que se ha terminado la revisión de tus respuestas y documentos, con base en nuestros estándares se ha determinado 
    que su solicitud para el examen de ingreso ha sido 
    <?php if($estatus=='aceptado'): ?>
        aceptado por lo tanto en breve se te brindará tu ficha para el examen.
    <?php elseif($estatus == 'observaciones'): ?>
        declinado con observaciones, esto quiere decir que no has cumplido con la corrección de tus documentos
    <?php else: ?>
        declinado por lo que te deseamos suerte en tu búsqueda por aprender, sigue adelante.
    <?php endif; ?>
    </p>
    
    <p>
        Da clic
        <a href="<?php echo $enlace ?>">aquí</a>
        para dirigirte al portal de Aspirantes.
    </p>
    <p>
        Todo el staff de admisión te desea el mejor de los éxitos
    </p>
</div>