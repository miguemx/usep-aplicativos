<?php
namespace App\Entities;

use CodeIgniter\Entity;

class MovimientosAlumnos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'matricula' => null,
        'autor' => null,
        'oficio' => null,
        'comentario' => null,
        'tipo' => null,
        'fecha' => null,
        'idc' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'movimiento_id',
        'matricula' => 'movimiento_matricula',
        'autor' => 'movimiento_autor',
        'oficio' => 'movimiento_oficio',
        'comentario' => 'movimiento_comentarios',
        'tipo' => 'movimiento_tipo',
        'fecha' => 'movimiento_fecha',
        'idc' => 'movimiento_idc',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
