<?php
namespace App\Entities;

use CodeIgniter\Entity;

class FiltroPregunta extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'            => null,
        'pregunta'      => null,
        'opciones'      => null,
        'mayor'         => null,
        'created_at'    => null,
        'updated_at'    => null,
        'deleted_at'    => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'            => 'filtropreg_id',
        'pregunta'      => 'filtropreg_pregunta',
        'opciones'      => 'filtropreg_opciones',
        'mayor'         => 'filtropreg_mayor',
        'created_at'    => 'created_at',
        'updated_at'    => 'updated_at',
        'deleted_at'    => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
