<?php

namespace App\Models;

use CodeIgniter\Model;

class ActaHistorial extends Model
{
    protected $table      = 'aca_historial_actas';
    protected $primaryKey = 'historial_id';

    protected $returnType    = 'App\Entities\Historial';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'historial_id', 'historial_idc_grupo', 'historial_json_acta', 'historial_json_alumnos', 'historial_fecha_capturada'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function getjson($grupoidc){
        //return $this->where('historial_idc_grupo',$grupoidc)->find();
       $query = $this->db->query("SELECT * FROM aca_historial_actas WHERE historial_idc_grupo ='$grupoidc' ");
       return $query->getResultArray();
    }
    public function tablacompleta(){
        $query = $this->db->query("SELECT historial_id, historial_idc_grupo,historial_json_acta  FROM aca_historial_actas");
        return $query->getResultArray();
    }
    public function getarreglos($id){
        //return $this->where('historial_idc_grupo',$grupoidc)->find();
       $query = $this->db->query("SELECT * FROM aca_historial_actas WHERE historial_id ='$id' ");
       return $query->getResultArray();
    }
    public function contador($idcgrupo){
        $consulta = $this->where('historial_idc_grupo',$idcgrupo)->find();

        return $consulta;
    }
    public  function getid(){
        $consulta = $this->orderBy('historial_id', 'DESC')->findAll();
        $folio = $consulta[0]->historial_id;
        $id = explode('-', $folio);
        $idb = $id[0];
        return $idb;
         
    }
}
