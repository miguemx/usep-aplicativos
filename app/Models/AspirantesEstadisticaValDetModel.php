<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesEstadisticaRegDet;

class AspirantesEstadisticaValDetModel extends Model
{
    protected $table="view_estadistica_aspirantes_valdet";
    protected $returnType='App\Entities\AspirantesEstadisticaValDet';
    protected $allowedFields=['m_enf','h_enf','m_med','h_med','aspirante_validado','aspirante_carrera','aspirante_sexo'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
/**
 * muestra la estadística agrupada por apirante validado
 * @return consulta colección de registros agrupados por región
 */
    public function estadisticadetval(){
        $consulta = $this->selectSum('m_enf')
                         ->selectSum('h_enf')
                         ->selectSum('m_med')
                         ->selectSum('h_med')
                         ->select('aspirante_validado')
                         ->groupBy('aspirante_validado')
                         ->orderBy('aspirante_validado')
                         ->findAll();
                         
        //$consulta = $this->selectSum('n_alumnos')->select('region_nombre')->groupBy('region_nombre')->orderBy('region_nombre')->findAll();
        return $consulta;
    }
    
}