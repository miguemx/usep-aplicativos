ALTER TABLE `alumnos_seguro` ADD `seguro_status` ENUM('SOLICITADO','RECHAZADO','OBSERVADO','REPORTADO') NOT NULL DEFAULT 'SOLICITADO' AFTER `seguro_archivo`, ADD `seguro_razon` VARCHAR(100) NULL AFTER `seguro_status`, ADD `seguro_razon_secundaria` VARCHAR(1000) NULL AFTER `seguro_razon`, ADD `seguro_comentarios` VARCHAR(1000) NULL AFTER `seguro_razon_secundaria`;


ALTER TABLE `alumnos_seguro` ADD `created_at` DATE NULL AFTER `seguro_comentarios`, ADD `updated_at` DATE NULL AFTER `created_at`, ADD `deleted_at` DATE NULL AFTER `updated_at`;

ALTER TABLE `alumnos_seguro` CHANGE `seguro_status` `seguro_status` ENUM('SOLICITADO','RECHAZADO','OBSERVADO','COMPLETADO','VISITADO','ESPERA','SOLICITAR') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'SOLICITADO';