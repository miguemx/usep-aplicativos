ALTER TABLE
    `aspirantes_tipos_campo` CHANGE `tipocampo_regex` `tipocampo_valores` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;

ALTER TABLE `aspirantes_tipos_campo` ADD `materia_clacificacion` ENUM('BASIC','COLECCION','ARCHIVO','CATALOGO') NOT NULL AFTER `tipocampo_valores`;
ALTER TABLE `aspirantes_tipos_campo` CHANGE `materia_clacificacion` `tipocampo_clasificacion` ENUM('BASIC','COLECCION','ARCHIVO','CATALOGO') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;

CREATE VIEW view_aspirantes_respuestas AS

SELECT 
	respuesta_id,
    respuesta_aspirante,
    respuesta_valor,
    aspirantes_respuestas.created_at,
    aspirantes_respuestas.updated_at,
    aspirantes_respuestas.deleted_at,
    
    campo_id,
    campo_nombre,
    campo_descripcion,
    campo_orden,
    campo_obligatorio,
    
    tipocampo_id,
    tipocampo_nombre,
    tipocampo_valores,
    tipocampo_clasificacion,
    
    formulario_id,
    formulario_etapa,
    formulario_nombre,
    formulario_titulo,
    formulario_orden,
    formulario_instrucciones
    
FROM `aspirantes_respuestas`
INNER JOIN aspirantes_campos on respuesta_campo=campo_id
INNER JOIN aspirantes_formularios on campo_formulario=formulario_id
INNER JOIN aspirantes_etapas on formulario_etapa=etapa_id
INNER JOIN aspirantes_tipos_campo on campo_tipo=tipocampo_id;

ALTER TABLE `aspirantes_respuestas` CHANGE `respuesta_id` `respuesta_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;