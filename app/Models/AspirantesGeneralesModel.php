<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesGenerales;

class AspirantesGeneralesModel extends Model
{
    protected $table='aspirantes_generales';
    protected $primaryKey='aspirante_id';
    protected $returnType ='App\Entities\AspirantesGenerales';
    protected $useSoftDeletes = true;
    protected $allowedFields=['aspirante_id','aspirante_carrera','aspirante_etapa','aspirante_validado','aspirante_curp','aspirante_nombre','aspirante_ap_paterno','aspirante_ap_materno',
    'aspirante_correo','aspirante_telefono','aspirante_calle','aspirante_colonia','aspirante_numero','aspirante_num_int','aspirante_cp','aspirante_municipio','aspirante_estado',
    'aspirante_pais','aspirante_sexo','aspirante_nac_fecha','aspirante_nac_municipio','aspirante_nac_estado','aspirante_nac_pais', 'aspirante_revalida','aspirante_estatus',
    'aspirante_dia','aspirante_hora','aspirante_ficha','aspirante_puntaje','aspirante_aceptado','aspirante_inscripcion_fecha','aspirante_inscripcion_hora','aspirante_inscrito',
    'created_at','updated_at','deleted_at'];
    
    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * realiza una búsqueda de aspirantes de acuerdo a los filtros proporcionados.
     * Si no se envía nungun filtro, se devuelven todos. Devolverá páginas de 25 registros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     * @return aspirantes devuelve un array con entidades de AspirantesGenerales
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros );
        $this->orderBy('aspirante_ap_paterno','ASC');
        $this->orderBy('aspirante_ap_materno','ASC');
        $this->orderBy('aspirante_nombre','ASC');
        $aspirantes = $this->paginate(25);
        return $aspirantes;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {  
        if ( array_key_exists('folio', $filtros) ) { 
            if ( strlen($filtros['folio']) ) {
                $this->where( 'aspirante_id ', trim( $filtros['folio'] ) );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { 
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'aspirante_ap_paterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { 
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'aspirante_ap_materno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { 
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'aspirante_nombre', trim($filtros['nombre']), 'both' ); 
            }
        }
        if ( array_key_exists('correo', $filtros) ) { 
            if ( strlen($filtros['correo']) ) {
                $this->where( 'aspirante_correo', $filtros['carrera'] ); 
            }
        }
        if ( array_key_exists('curp', $filtros) ) { 
            if ( strlen($filtros['curp']) ) {
                $this->like( 'aspirante_curp', strtoupper($filtros['curp']), 'both' ); 
            }
        }
        if ( array_key_exists('sexo', $filtros) ) { 
            if ( strlen($filtros['sexo']) ) {
                $this->where( 'aspirante_sexo', strtoupper($filtros['sexo']) ); 
            }
        }
        if ( array_key_exists('carrera', $filtros) ) { 
            if ( strlen($filtros['carrera']) ) {
                $this->where( 'aspirante_carrera', $filtros['carrera']); 
            }
        }
        if ( array_key_exists('estatus', $filtros) ) { 
            if ( strlen($filtros['estatus']) ) {
                $this->where( 'aspirante_estatus', strtoupper($filtros['estatus']) ); 
            }
        }

        if ( array_key_exists('aceptado', $filtros) ) { 
            if ( strlen($filtros['aceptado']) ) {
                $this->where( 'aspirante_aceptado', $filtros['aceptado'] ); 
            }
        }

        $this->orderBy('updated_at','ASC');
        $this->orderBy('aspirante_ap_paterno','ASC');
        $this->orderBy('aspirante_ap_materno','ASC');
        $this->orderBy('aspirante_nombre','ASC');
    }

    /**
     * crea una matrícula nueva a partir de los registros
     * @param anio los dos ultimos digitos del año al que aspira
     * @param carrera la clave o Id de la carrera
     * @return matricula la matrícula nueva creada
     */
    public function creaMatricula($anio, $carrera) {
        $matricula = '999999999';
        if ( $carrera < 10 ) $carrera = '0'.$carrera;
        $this->like( 'aspirante_id', $anio.$carrera, 'after' );
        $this->orderBy( 'aspirante_id','DESC' );
        $aspirantes = $this->findAll();
        if ( count($aspirantes) === 0 ) {
            $matricula = $anio.$carrera.'10101';
        }
        else {
            $last = $aspirantes[0]->folio;
            $matricula = $last + 1;
        }
        return $matricula;
    }

    /**
     * verifica que la CURP proporcionada no exista
     * @param curp la CURP a buscar
     * @return true si existe 
     * @return false si no existe el CUR¨P
     */
    public function existe( $curp ) {
        $this->like( 'aspirante_curp', $curp, 'both' );
        $registros = $this->findAll();
        if ( count($registros) ) {
            return true;
        }
        return false;
    }
    
    /**
     * busca un registro de aspirante mediante su curp
     * @param curp el correo que se va a buscar
     * @return Aspirante entidad de Aspirante con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByCurp($curp) {
        $intento = $this->where('aspirante_curp',$curp)->first();
        return $intento;
    }

    /**
     * @return query con el contenido total del los alumnos y las etapas a las que pertenece
     */
    public function listaEtapas($id = null)
    {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_etapas', 'etapa_id=aspirante_etapa', 'left' );
        $this->builder->orderBy('aspirantes_generales.created_at','ASC');
        $this->builder->orderBy('aspirantes_generales.aspirante_estatus','ASC');
       
        if($id)  $this->builder->where('aspirantes_generales.aspirante_id',$id);

        $query = $this->builder->get();
        return $query->getResult();
    }
    public function conteoaspirantes($filtros){
        if ( array_key_exists('sexo', $filtros) ) {
            if ( strlen($filtros['sexo']) ) {
                $consulta = $this->where('aspirante_carrera', strtoupper($filtros['carrera']))
                ->where('aspirante_sexo', strtoupper($filtros['sexo']))->findAll();
                $contador = null;
                foreach($consulta as $numero){
                    $contador = $contador +1;
                }
                return $contador;
            }
        }
        if ( array_key_exists('todos', $filtros) ) {
            if ( strlen($filtros['todos']) ) {
                $consulta = $this->where('aspirante_carrera', strtoupper($filtros['carrera']))
                ->findAll();
                return count($consulta);
            }
        }
        $this->orderBy('aspirante_ap_paterno','ASC');
        $this->orderBy('aspirante_ap_materno','ASC');
        $this->orderBy('aspirante_nombre','ASC');

    }
    public function estatusregistro($filtros){
        if ( array_key_exists('validado', $filtros) ) {
            if ( strlen($filtros['validado']) ) {
                $consulta = $this->where('aspirante_validado', 1)
                ->findAll();
                return $consulta;
            }
        }
        if ( array_key_exists('novalidado', $filtros) ) {
            if ( strlen($filtros['novalidado']) ) {
                $consulta = $this->where('aspirante_validado', 0)->find();
                return $consulta;
            }
        }
        if ( array_key_exists('todos', $filtros) ) {
            if ( strlen($filtros['todos']) ) {
                $consulta = $this->findAll();
                return $consulta;
            }
        }
        $this->orderBy('aspirante_ap_paterno','ASC');
        $this->orderBy('aspirante_ap_materno','ASC');
        $this->orderBy('aspirante_nombre','ASC');

        //validacion' => '', 'capturainfo' => '', 'cargadoc' => '','revisiondoc' => '','observaciones' => '','validado' => '','declinado' => '','total' => ''
    }
    public function revision(){
        $consulta = $this->where('aspirante_estatus', 'REVISION')->findAll();
        return $consulta;
    }
    public function observaciones(){
        $consulta = $this->where('aspirante_estatus', 'OBSERVACIONES')->findAll();
        return $consulta;
    }
    public function validado(){
        $consulta = $this->where('aspirante_estatus', 'EXITOSO')->findAll();
        return $consulta;
    }
    public function declinado(){
        $consulta = $this->where('aspirante_estatus', 'DECLINADO')->findAll();
        return $consulta;
    }
    public function buscarporetapa($idetapa){
        $consulta = $this->where('aspirante_etapa', $idetapa)->findAll();
        return $consulta;
    }

    /**
     * obtiene todos los aspirantes que hayan validado su correo
     */
    public function correoValidado() {
        $this->where( 'aspirante_validado', '1' );
        $this->orderBy( 'aspirante_id', 'ASC' );
        return $this->findAll();
    }

    /**
     * obtiene todos los aspirantes que hayan terminado su registro
     */
    public function registroConcluido() {
        $this->where( 'aspirante_validado', '1' );
        $this->where( "aspirante_estatus != 'REGISTRO' " );
        $this->orderBy( 'aspirante_id', 'ASC' );
        return $this->findAll();
    }


    /**
     * busca un aspirante para recuperar su liga de activación
     * @param curp curp del aspirante
     * @param ap_mat apellido materno del aspirante
     * @param carrera opción de carrera del aspirante
     * @return registro aspirante que coincide con el criterio de búsqueda
     */

    public function recuperaliga ($curp,$ap_mat,$carrera){
        $registro = $this->where('aspirante_curp',$curp)->where('aspirante_ap_materno',$ap_mat)->where('aspirante_carrera',$carrera)->find();
        return $registro;
    }

    /**
     * cuenta a los aspirantes clasificandolos por el correo validado o no
     * @return consulta 
     */
    public function conteo_validados(){
        $consulta= $this->selectCount('aspirante_id')->select('aspirante_validado')->groupBy('aspirante_validado')->findAll();
        return $consulta;
    }

    /**
     * cuenta a los aspirantes clasificandolos por el estatus de su registro
     * @return consulta 
     */
    public function conteo_status(){
        $consulta= $this->selectCount('aspirante_id')->select('aspirante_estatus')->where('aspirante_validado','1')->orderBy('aspirante_estatus')->groupBy('aspirante_estatus')->findAll();
        return $consulta;
    }    
    /**
     * cuenta a los aspirantes clasificandolos por el estatus de su registro, género y carrera
     * @return consulta 
     */
    public function conteo_status_det(){
        $consulta= $this->selectCount('aspirante_id')
                        ->select('aspirante_estatus')
                        ->select('aspirante_carrera')
                        ->select('aspirante_sexo')
                        ->where('aspirante_validado','1')
                        ->groupBy('aspirante_estatus')
                        ->groupBy('aspirante_carrera')
                        ->groupBy('aspirante_sexo')
                        ->orderBy('aspirante_estatus')
                        ->orderBy('aspirante_carrera')
                        ->orderBy('aspirante_sexo')
                        ->findAll();
        return $consulta;
    }

    /**
     * cuenta a los aspirantes clasificandolos por el estatus de su registro,género y carrera
     * @return consulta 
     */
    public function conteo_val_det(){
        $consulta= $this->selectCount('aspirante_id')
                        ->select('aspirante_validado')
                        ->select('aspirante_carrera')
                        ->select('aspirante_sexo')
                        ->groupBy('aspirante_validado')
                        ->groupBy('aspirante_carrera')
                        ->groupBy('aspirante_sexo')
                        ->orderBy('aspirante_validado')
                        ->orderBy('aspirante_carrera')
                        ->orderBy('aspirante_sexo')
                        ->findAll();
        return $consulta;
    }

    /**
     * cuenta a los aspirantes que son de otros estados y extrangeros
     * @param sexo género del aspirante
     * @param carrera carrera elegida por el aspirante leo/med
     * @param país país de procedencia del aspirante Extranjero/México
     * @return conteo número de aspirantes 
     */

     public function conteo_foraneo ($carrera,$sexo,$pais) {
         $conteo= $this->selectCount('aspirante_id')
                       ->where('aspirante_sexo',$sexo)
                       ->where('aspirante_carrera',$carrera)
                       ->where('aspirante_pais',$pais)
                       ->where('aspirante_estado !=','21')
                       ->first();
         return $conteo;                 
     }
  /**
   * devuelve el registro de un aspirante siempre y cuando su registro sea exitoso
   * @param id folio del estudiante
   */
  public function aspirante_exitoso($id){
    $consulta =$this->where('aspirante_id',$id)
                    ->where('aspirante_estatus',"EXITOSO")
                    ->first();
    return $consulta;
  }

  /**
   * devuelve todos los aspirantes con estatus de "exitoso" y con la ficha del examen de admision descargada
   */
  public function asp_exitosos(){
    $consulta =$this->where('aspirante_estatus',"EXITOSO")
                    ->where('aspirante_ficha',"1")
                    ->findAll();
    return $consulta;
  }
  public function updateaspirante($matricula,$update){
    return $this->db
    ->table('aspirantes_generales')
    ->where('aspirante_id',$matricula)
    ->set($update)
    ->update();
  }
}

?>