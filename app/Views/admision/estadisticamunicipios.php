<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
        height: 400px;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #ebebeb;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure">
    <div id="container"></div>
    <div class="text-center">
        <form action="<?php echo base_url('EstadisticasAdmision/'); ?>">
            <button type="submit" class="btn btn-secondary">Regresar</button>
        </form>
    </div>
</figure>

<script>
    let metropolitanos = <?php echo $metropolitanos ?> ;
    let interior = <?php echo $interior ?> ;
    let otros_estados = <?php echo $otros_estados ?> ;
    let extranjeros = <?php echo $extranjeros ?> ;
    let total = <?php echo $total ?> ;
    const chart = Highcharts.chart('container', {
        chart: {
            inverted: false,
            polar: false
        },
        title: {
            text: 'USEP'
        },
        subtitle: {
            text: 'ASPIRANTES POR MUNICIPIOS'
        },
        xAxis: {
            categories: ['Metropolitanos', 'Interior del estado','Otros estados','Extranjeros']
        },
        series: [{
            type: 'column',
            colorByPoint: true,
            data: [metropolitanos, interior,otros_estados,extranjeros],
            showInLegend: false
        }]
    });
    let axios = document.getElementsByClassName('highcharts-axis-title');
    let creditos = document.getElementsByClassName('highcharts-credits');
    axios[0].childNodes[0].data = 'ASPIRANTES TOTALES: '+<?php echo $total ?>;
    creditos[0].childNodes[0].data = '';


</script>
<?php echo $this->endSection() ?>