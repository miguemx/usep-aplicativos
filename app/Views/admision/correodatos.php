<div>
    <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
    <p>
        <strong><?php echo $nombre ?></strong>
    </p>
    <p>
        Por este medio hacemos de su conocimiento los datos para ingresar al Portal de Aspirantes de la  
        <strong>&quot;Universidad de la Salud&quot;</strong> del Estado de Puebla.
    </p>
    <p>
        <strong>CURP: </strong><?php echo $curp; ?><br />
        <strong>Folio: </strong><?php echo $folio; ?><br /><br />
    </p>
    <p>
        Dé clic
        <a href="<?php echo $enlace ?>">aquí</a>
        para acceder.
    </p>
    <p>
        Le deseamos éxito en su proceso.
    </p>
    
</div>