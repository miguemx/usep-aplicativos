<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesConfiguracion extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'id' => null,
        'nombre' => null,
        'status' => null,
        'creado_al' => null,
        'actualizado_al' => null,
        'borrado_al' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id' => 'configuracion_id',
        'nombre' => 'configuracion_nombre',
        'status' => 'configuracion_status',
        'created_at' => 'creado_al',
        'updated_at' => 'actualizado_al',
        'deleted_at' => 'borrado_al',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}