<?php
namespace App\Models;
use CodeIgniter\Model;

class AspirantesNotificacionesModel extends Model
{
    protected $table="aspirantes_notificaciones";
    protected $primaryKey="notificaciones_id";
    protected $returnType='App\Entities\AspirantesNotificaciones';
    protected $useSoftDeletes = true;
    protected $allowedFields=['notificaciones_id','notificaciones_aspirante','notificaciones_mensaje','notificaciones_formulario','notificaciones_leido','created_at','updated_at','deleted_at'];
    protected $db;
    protected $builder;
    protected $useTimestamps = true;
    

    /**
     * retorna todas las notificaciones correspondientes al aspirante seleccionado 
     * @param aspirante folio del aspirante
     */
    public function getNotificacionesAspirante($aspirante)
    {
        return $this->where('notificaciones_aspirante',$aspirante)->orderBy('created_at','DESC')->find();

    }

   
    
}

?>