<?php

use SebastianBergmann\CodeCoverage\Report\PHP;

echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous" />
<style>
    .main-content {
        display: flex;
        flex-direction: row;
    }

    .sidebar-menu {
        height: 100hv;
        max-height: 100hv;
        /* background-color: red; */
    }

    .sidebar-menu li {
        text-decoration: none;
        list-style: none;
    }

    a {
        text-decoration: none;
        color: black;
    }

    a:hover {
        text-decoration: none;
        color: #840f31;
    }

    .pdfobject-container {
        height: 600px;
        border: 1rem solid rgba(0, 0, 0, .1);
    }

    .sider {
        min-height: 93vh;
        max-height: 93vh;
        overflow-x: scroll;
        overflow-x: scroll;
    }

    .barra {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }

    .img_respuestas {
        max-height: 150px;
    }

    .botones {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
</style>
<?php //var_dump($mensaje);  
?>
<?php if ($mensaje != '') : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<?php

?>
<div class=" container-fluid main-content mt-2">

    <div class="sidebar-menu me-2 w-25 bd-sidebar container-fluid sider">
        <div class="card rounded-2 mb-2">
            <div class="col-md-12 mt-2">
                <div class="text-center">
                    <h4>Datos del Aspirante</h4>
                </div>
                <div class="barra">
                    <div>
                        <form action="<?php echo base_url('ControlAdmision/terminarProceso'); ?>" method="post" id="form-proceso-aceptado">
                            <button type="button" class="btn btn-success" onclick="terminar_proceso_aceptado(this)" value="exitoso">Exitoso </button>
                            <input type="hidden" name="folio" value="<?php echo $apirante->aspirante_id; ?>">
                        </form>
                    </div>
                    <div>
                        <form action="<?php echo base_url('ControlAdmision/generarAlertas'); ?>" method="post" id="form-proceso-observaciones">
                            <input type="hidden" name="folio" value="<?php echo $apirante->aspirante_id; ?>">
                            <button class="btn btn-warning" type="button" class="btn btn-secondary" type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#modal-etapa">Con observaciones</button>
                        </form>
                    </div>
                    <div>
                        <form action="<?php echo base_url('ControlAdmision/terminarProceso'); ?>" method="post" id="form-proceso-rechazado">
                            <input type="hidden" name="folio" value="<?php echo $apirante->aspirante_id; ?>">
                            <button type="button" class="btn btn-danger" onclick="terminar_proceso(this)" value="declinado">Declinado</button>
                        </form>
                    </div>

                </div>
            </div>
            <?php // echo var_dump ($apirante) 
            ?>
            <div class="col-md mb-1 mt-2">
                <table class="table table-borderless  table-fluid">
                    <tbody>
                        <tr>
                            <td><strong>Folio:</strong> <?php echo $apirante->folio; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $apirante->apPaterno . " " . $apirante->apMaterno . " " . $apirante->nombre; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $apirante->correo ?></td>
                        </tr>
                        <tr>
                            <td><?php echo ($apirante->carrera == 1) ? 'Licenciatura en Enfermería y Obstetricia' : 'Licenciatura en Médico Cirujano'; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $apirante->curp ?></td>
                        </tr>
                        <tr>
                            <td><?php echo  date("Y/m/d", strtotime($apirante->nac_fecha)) ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $apirante->nac_pais ?></td>
                        </tr>
                        <?php foreach ($campos_visibles as $campos) : ?>
                            <?php if ($campos->tipoNombre != 'DOCUMENTO' && $campos->tipoNombre != 'FOTO') : ?>
                                <tr>
                                    <?php

                                    if ($campos->tipoNombre == 'CALIFICACION') {
                                        if ($campos->valor < 8) { ?>
                                            <td>
                                                <div class="row d-flex justify-content-between">
                                                    <div class="col">
                                                        <strong> <?php echo $campos->nombreCampo ?></strong>
                                                        <var class="text-danger fs-5"><?php echo  $campos->valor ?></var>
                                                    </div>
                                                    <div class="col-3 text-right">
                                                        <button type="button" class="btn btn-secondary" value="<?php echo $campos->id ?>" onclick="editar(this)">editar</button>
                                                    </div>
                                                </div>
                                                <div class=" input-group mt-1" id="<?php echo 'contenedor-' . $campos->id ?>" hidden>
                                                    <input type="text" class="form-control" id="<?php echo 'valor-' . $campos->id ?>">
                                                    <button class="btn btn-primary" value="<?php echo $campos->id ?>" type="button" onclick="actualizarDatos(this)">Guardar</button>
                                                </div>
                                            </td>
                                        <?php
                                        } else { ?>
                                            <td>
                                                <div class="row d-flex justify-content-between">
                                                    <div class="col">
                                                        <?php echo $campos->nombreCampo . " : " . $campos->valor ?>
                                                    </div>
                                                    <div class="col-3 text-right">
                                                        <button id="" type="button" class="btn btn-secondary" value="<?php echo $campos->id ?>" onclick="editar(this)">editar</button>
                                                    </div>
                                                </div>
                                                <div class=" input-group mt-1" id="<?php echo 'contenedor-' . $campos->id ?>" hidden>
                                                    <input type="text" class="form-control" id="<?php echo 'valor-' . $campos->id ?>">
                                                    <button class="btn btn-primary" type="button" value="<?php echo $campos->id ?>" onclick="actualizarDatos(this)">Guardar</button>
                                                </div>
                                            </td>
                                        <?php
                                        }
                                    } else { ?>
                                        <td>
                                            <div class="row d-flex justify-content-between">
                                                <div class="col">
                                                    <?php echo $campos->nombreCampo . " : " . $campos->valor ?>
                                                </div>
                                                <div class="col-3 text-right">
                                                    <button type="button" class="btn btn-secondary" value="<?php echo $campos->id ?>" onclick="editar(this)">editar</button>
                                                </div>
                                            </div>
                                            <div class=" input-group mt-1" id="<?php echo 'contenedor-' . $campos->id ?>" hidden>
                                                <input type="text" class="form-control" id="<?php echo 'valor-' . $campos->id ?>">
                                                <button class="btn btn-primary" type="button" value="<?php echo $campos->id ?>" onclick="actualizarDatos(this)">Guardar</button>
                                            </div>
                                        </td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if (isset($campos_documentos)) : ?>
                    <?php foreach ($campos_documentos as $documento) : ?>
                        <div>
                            <div class="text-center">
                                <img class="img_respuestas" src="<?php echo $documento ?>" alt="foto de perfil">
                            </div>

                        </div>

                    <?php endforeach; ?>
                <?php endif ?>
                <!-- <?php // var_dump($campos_documentos) 
                        ?> -->
            </div>
        </div>
        <div class="">
            <li class="list-group-item ml-1">

                <div class="fw-bold mb-1">Archivos </div>
                <?php $etapa_anterior = "" ?>
                <?php foreach ($respuestas as $archivos) : ?>

                    <?php if ($etapa_anterior != $archivos->nombreEtapa) : ?>
                        <div>
                            <?php echo mb_strtoupper($archivos->nombreEtapa) ?>
                        </div>
                        <?php $etapa_anterior = $archivos->nombreEtapa ?>
                    <?php endif; ?>
                    <?php ?>
                    <div class="row">
                        <button type="button" class="btn btn-secondary btn-sm mb-1" onclick="showframe(this)" value="<?php echo $archivos->aspirante . "/" . $archivos->valor . '|' . $archivos->respuesta_id . '|' . $archivos->campo_id;   ?>">
                            <?php echo $archivos->campo_nombre;   ?>
                        </button>


                    </div>
                <?php endforeach; ?>

            </li>
        </div>
        <div class="card rounded-2">
            <div class="list-group-item btn-home ">
                <h4>Datos Generales</h4>
            </div>
            <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Etapas </div>
                </div>
            </li>
            <div class="accordion" id="accordionExample">
                <?php $etapa_anterior = '';
                $contador = 1; ?>
                <?php foreach ($etapas_formularios as $listado) : ?>
                    <?php if ($etapa_anterior != $listado->etapa_id) : ?>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="<?php echo "heading" . $contador ?>">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="<?php echo "#collapse" . $contador ?>" aria-expanded="false" aria-controls="<?php echo "collapse" . $contador ?>">
                                    <?php echo mb_strtoupper($listado->etapa_nombre) ?>
                                    <?php $titulo = $listado->etapa_nombre ?>
                                </button>
                            </h2>
                        </div>
                        <div id="<?php echo "collapse" . $contador ?>" class="accordion-collapse collapse" aria-labelledby="<?php echo "heading" . $contador ?>" data-bs-parent="#accordionExample">
                            <div class="list-group" id="myList" role="tablist">
                                <?php foreach ($etapas_formularios as $items) : ?>
                                    <?php if ($items->etapa_nombre == $titulo) : ?>
                                        <a class="list-group-item list-group-item-action item_formulario" data-bs-toggle="list" href="<?php echo "#data" . $items->formulario_id ?>" role="tab"><?php echo $items->formulario_nombre ?></a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php $etapa_anterior = $listado->etapa_id;
                    $contador++; ?>
                <?php endforeach; ?>

            </div>
        </div>
        <?php //var_dump($respuestas) 
        ?>

    </div>
    <div class="datos-menu  w-75 sider">
        <form action="<?php echo base_url('ControlAdmision/guardarComentarios'); ?>" method="post" id="form-comentarios">
            <div class="card rounded-2">
                <div class="card-body respuestaaaa">
                    <div class="tab-content ">
                        <div class="tab-pane  " id="dataHome" role="tabpanel">
                            <input type="hidden" name="id_documento">
                            <div class="documento" id="documento">
                            </div>
                            <div>
                                <div class="mb-3">
                                    <div class="mb-3">
                                        <label for="selec-comentario" class="form-label">Lista de Comentarios</label>
                                        <select class="form-select" name="selec-comentario" id="selec-comentario" onchange="agregar_coment(this)">
                                            <option selected>Seleccione un comentario</option>
                                        </select>
                                    </div>
                                    <?php $etapa_anterior = "" ?>
                                    <?php $contador = 1; ?>
                                    <?php foreach ($respuestas as $archivos) : ?>
                                        <div hidden id="<?php echo "resp-" . $archivos->respuesta_id ?>" name="comentarios-archivos">
                                            <label for="<?php echo "formComent" . $contador  ?>" class="form-label">Comentarios</label>
                                            <textarea class="form-control" name="coment_doc[]" id="<?php echo "formComent" . $contador  ?>" rows="4"><?php echo $archivos->comentario ?></textarea>
                                        </div>
                                    <?php endforeach; ?>
                                    <div class="text-center card-body">
                                        <button type="button" class="btn btn-secondary" id="btn-guardarcoment" onclick="confirmaFormulario()">Guardar Comentarios</button>
                                        <!-- <button type="button" class="btn btn-secondary" type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#modal-etapa">Generar Alertas</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($etapas_formularios as $contenido) : ?>
                            <div class="tab-pane form_content" id="<?php echo "data" . $contenido->formulario_id ?>" role="tabpanel">
                                <?php echo mb_strtoupper($contenido->formulario_nombre) ?>
                                <div class="row respuestas mb-2">

                                    <?php foreach ($campos_completos as $campo) : ?>
                                        <?php if ($campo->campo_formulario == $contenido->formulario_id) :  ?>
                                            <?php if (array_key_exists($campo->tipo, $tiposCampos)) :
                                                $value = (array_key_exists($campo->campo, $respuestas_bss)) ? $respuestas_bss[$campo->campo] : null;
                                                echo myInputBasic('text', $campo->nombre, '', $value);
                                            ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
            <input type="text" name="id_aspirante" value="<?php echo $apirante->aspirante_id; ?>" hidden />
        </form>
    </div>
</div>
<form action="<?php echo base_url('ControlAdmision/generarAlertas'); ?>" method="post" id="form-alertas">
    <!-- Modal -->
    <div class="modal fade" id="modal-etapa" tabindex="-1" role="dialog" aria-labelledby="modal-etapa-selector" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Seleccione la Etapa</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <?php //var_dump($etapas_totales) 
                        ?>
                        <div class="mb-3">
                            <label for="etapas_disponibles" class="form-label">Etapas disponibles</label>
                            <select class="form-select" name="etapas_disponibles" id="etapas_disponibles">
                                <option selected>Select one</option>
                                <?php foreach ($etapas_totales as $etapas) : ?>
                                    <?php
                                    $selected = '';
                                    if ($etapas_documentales != '') {
                                        foreach ($etapas_documentales as $etapa_doc) {
                                            if ($etapa_doc->etapa_id == $etapas->etapa) {
                                                $selected = 'selected';
                                            }
                                        }
                                    }
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $etapas->etapa ?>"><?php echo $etapas->nombre ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn-generaralertas">Guardar y Continuar</button>
                </div>
            </div>
        </div>
    </div>



    <input type="hidden" name="folio" value="<?php echo $apirante->aspirante_id; ?>">
</form>

<script src="https://unpkg.com/pdfobject@2.2.5/pdfobject.min.js "></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    'use strict';
    const collapse_list = document.getElementsByClassName('accordion-button');

    let desactivar = () => {
        let formularios = document.getElementsByClassName('item_formulario');
        for (let index = 0; index < formularios.length; index++) {
            formularios[index].classList.remove('active');
        }

    }
    for (let array = 0; array < collapse_list.length; array++) {
        collapse_list[array].addEventListener('click', desactivar);
    }
    let datahome = () => {
        let formularios = document.getElementsByClassName('item_formulario');
        for (let index = 0; index < formularios.length; index++) {
            formularios[index].classList.remove('active');
        }
        let datos = document.getElementsByClassName('form_content');
        for (let index = 0; index < datos.length; index++) {
            datos[index].classList.remove('active');

        }
        document.getElementById('dataHome').classList.add('active');
    }
    let arr_labels = document.querySelectorAll("label");
    arr_labels[6].classList.add("form-label");
    arr_labels[8].classList.add("form-label");

    let inputs = document.querySelectorAll("input");
    for (let index = 0; index < inputs.length; index++) {
        inputs[index].readOnly = true;
    }
    let hijos = document.getElementsByClassName('input-group-outline');
    let hijos2 = document.getElementsByClassName('input-group-static');
    for (let index = 0; index < hijos.length; index++) {
        hijos[index].classList.remove('input-group');

    }
    for (let index = 0; index < hijos2.length; index++) {
        hijos2[index].classList.remove('input-group');
    }

    var showframe = async (valor) => {
        //console.log(valor.value);
        let documento = valor.value.split('|');
        let input_bandera = document.getElementsByName('id_documento');
        input_bandera.value = valor.value;
        document.getElementById('selec-comentario').innerHTML = '';
        PDFObject.embed('<?php echo base_url("ControlAdmision/documentoPreview") . "/" ?>' + documento[0], "#documento");
        datahome();
        let datos = valor.value.split('|');
        let arr_comentarios = document.querySelectorAll('div[name=comentarios-archivos]');
        for (let index = 0; index < arr_comentarios.length; index++) {
            arr_comentarios[index].hidden = true;
        }
        let comentario_doc = document.getElementById('resp-' + datos[1]);
        comentario_doc.hidden = false;
        get_select(datos[2]);
    }
    var get_select = async (comentarios) => {
        // console.log(comentarios);
        const formData = new FormData();
        formData.append('estado_nac', comentarios);
        let response = await fetch('<?php echo base_url("ControlAdmision/getComentarios") ?>' + '/' + comentarios, {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            // console.error('Error:', error);
            return null;
        });
        document.getElementById('selec-comentario').innerHTML = '';
        console.log(response);
        if (response) {
            let option = document.createElement('option');
            option.value = '';
            option.innerHTML = "Selecciona un comentario";
            document.getElementById('selec-comentario').appendChild(option);
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].texto;
                option.innerHTML = response[i].texto;
                document.getElementById('selec-comentario').appendChild(option);
            }
        } else {
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = "No hay respuestas predeterminadas";
            document.getElementById('selec-comentario').appendChild(option);
        }
    }

    function add_texto() {
        if (select.value != ' ') {
            let select = document.getElementById('select-observaciones');
            //console.log(select.value);
            let comentario = document.getElementById('comentarioForm');
            let id_doc = document.getElementsByName('id_documento');
            comentario.value = comentario.value + select.value + "\n";
        }
    }

    let btn_guardar = document.getElementById('btn-generaralertas');

    btn_guardar.addEventListener('click', function() {
        let text_area = document.querySelectorAll('textarea');
        console.log(text_area);
        let bandera_comentarios = false;
        for (let index = 0; index < text_area.length; index++) {
            if (text_area[index].value != '') {
                //  console.log(text_area[index].value);
                bandera_comentarios = true;
            }

        }
        if (bandera_comentarios == true) {
            Swal.fire({
                'title': 'Confirmación',
                'text': 'Estas por generar los comentarios para el aspirante, ¿Estás seguro de que has revisado todos tus comentarios y deseas continuar?',
                'icon': 'question',
                confirmButtonColor: '#a1194b',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#a1194b'

                    });
                    document.getElementById('form-alertas').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                        'icon': 'info',
                        confirmButtonColor: '#a1194b'
                    });
                }
            });
        } else {
            Swal.fire({
                'title': 'Cancelado',
                'html': 'No tiene ningun comentario capturado, antes de actualizar el estatus del aspirante verifique que tenga al menos un comentario.',
                'icon': 'info',
                confirmButtonColor: '#a1194b'
            });
        }
    });

    function confirmaFormulario() {
        let text_area = document.querySelectorAll('textarea');
        let bandera_text = false;
        for (let index = 0; index < text_area.length; index++) {
            if (text_area[index].value != '' && text_area[index].value != ' ' && text_area[index].value != null) {
                bandera_text = true;
            }

        }
        if (bandera_text == true) {

            Swal.fire({
                'title': 'Confirmación',
                'html': '¿Está seguro de que desea agregar los comentarios? <br> Si continua esto no generara ninguna alerta al Aspirante"',
                'icon': 'question',
                confirmButtonColor: '#a1194b',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#a1194b'

                    });
                    document.getElementById('form-comentarios').submit();
                    // return true;
                    

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos de comentarios y vuelva a intentarlo',
                        'icon': 'info',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            });
        } else {
            Swal.fire({
                'title': 'Cancelado',
                'html': 'No tiene ningun comentario capturado, antes de actualizar el estatus del aspirante verifique que tenga al menos un comentario.',
                'icon': 'info',
                showConfirmButton: false,
                timer: 1500
            });
        }
    }

    function terminar_proceso_aceptado(form) {
        let text_area = document.querySelectorAll('textarea');
        let bandera_text = false;
        for (let index = 0; index < text_area.length; index++) {
            if (text_area[index].value != '' && text_area[index].value != ' ' && text_area[index].value != null) {
                bandera_text = true;
            }

        }
        if (bandera_text) {
            Swal.fire({
                'title': 'Confirmación',
                'text': 'Este aspirante tiene comentarios en sus archivos, ¿Estas seguro que deseas aceptarlo?',
                'icon': 'question',
                confirmButtonColor: '#a1194b',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',

            }).then((result) => {
                if (result.value) {
                    terminar_proceso(form);
                } else {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información',
                        'icon': 'info',
                        confirmButtonColor: '#a1194b'
                    });
                }
            });
        } else {
            terminar_proceso(form);
        }

    }

    function terminar_proceso(form) {
        console.log(form.value);
        if (form.value == 'declinado') {
            console.log('<?php echo ($bandera_comentarios) ? 'si' : 'no' ?>');
            if ('<?php echo ($bandera_comentarios) ? 'si' : 'no' ?>' == 'no') {
                console.log('correcto');
                Swal.fire({
                    'title': 'Sin comentarios',
                    'text': 'Antes de declinar al aspirante deberá registrar un comentario en sus documentos con el fin de hacer del conocimiento del aspirante el motivo de su solicitud declinada.',
                    'icon': 'info',
                    confirmButtonColor: '#a1194b'
                });

            } else {
                mensajes(form);
            }
        } else {

            mensajes(form);
        }
    }

    function mensajes(form) {
        Swal.fire({
            'title': 'Confirmación',
            'text': 'Estas por terminal el proceso del aspirante con el estatus de ' + form.value.toUpperCase() + ', ¿Estas seguro que has revisado bien sus respuestas?, este proceso es irreversible por lo tanto una vez enviado no podrás cambiar de elección. \n ¿Deseas continuar?',
            'icon': 'question',
            confirmButtonColor: '#a1194b',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',

        }).then((result) => {
            if (result.value) {
                let input = document.createElement('input');
                input.name = 'bandera';
                input.value = form.value;
                input.hidden = true;
                switch (form.value) {
                    case 'exitoso':
                        // console.log('aceptado');
                        let aceptado = document.getElementById('form-proceso-aceptado');
                        aceptado.appendChild(input);
                        aceptado.submit();
                        break;
                    case 'observaciones':
                        // console.log('observaciones');
                        let observaciones = document.getElementById('form-proceso-observaciones');
                        observaciones.appendChild(input);
                        observaciones.submit();
                        break;
                    case 'declinado':
                        let rechazado = document.getElementById('form-proceso-rechazado');
                        rechazado.appendChild(input);
                        rechazado.submit();
                        break;
                }

                // console.log(form);

            } else {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información',
                    'icon': 'info',
                    confirmButtonColor: '#a1194b'
                });
            }
        });
    }

    function agregar_coment(comentario) {
        let div_text_area = document.getElementsByName('comentarios-archivos');
        let text_area = document.querySelectorAll('textarea');
        for (let index = 0; index < div_text_area.length; index++) {
            if (div_text_area[index].hidden == false) {
                console.log(div_text_area[index]);
                console.log(text_area[index].value);
                if (text_area[index].value == ' ' || text_area[index].value == '') {
                    text_area[index].value = comentario.value;
                } else {
                    text_area[index].value = text_area[index].value + '\n' + comentario.value;
                }
            }

        }

    }
    var editar = async (respuesta) => {
        // console.log(respuesta);
        var campo = document.getElementById('valor-' + respuesta.value);
        // console.log(campo);
        campo.removeAttribute('readonly', '');
        var div = document.getElementById('contenedor-' + respuesta.value);
        // console.log(div);
        div.hidden = false;
    }
    var actualizarDatos = async (valor) => {
        // console.log(valor);
        var campo = document.getElementById('valor-' + valor.value);
        // console.log(campo.value);
        let response = await fetch('<?php echo base_url("ControlAdmision/UpdateCampo") ?>' + '/' + valor.value + '/' + campo.value, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            // console.error('Error:', error);
            return null;
        });
        console.log(response);
        if (response) {
            alert('Se ha modificacdo el campo');
            location.reload();
        } else {

            alert('No se pudo actualizar el registro, verifique los datos insertados');
        }

    }

    function recordatorio() {
        Swal.fire({
            'title': 'info',
            'text': 'Recuerde que en caso de haber comentarios deberá guardarlos antes de determinar el estatus del aspirante.',
            'icon': 'info',
            // showConfirmButton: false,
            // timer: 2000
            confirmButtonColor: '#a1194b',
            confirmButtonText: 'Continuar',
        });
    }
    // recordatorio();
</script>
<?php echo $this->endSection() ?>