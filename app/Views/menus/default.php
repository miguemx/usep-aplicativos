<?php echo $this->include('menus/_inicio'); ?>
    


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url() ?>">Inicio</a>
        </li>

        

        

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Docente
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/CapturaCalificaciones') ?>">Captura de calificaciones</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Docente/Horario'); ?>">Horarios</a></li>
           <li><li><a class="dropdown-item" href="<?php echo base_url('Escolar/listaescolar') ?>">Lista de grupos</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Docente/bitacoraAulas'); ?>">Bitacora de Aulas</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Docente/consultahorario'); ?>">Captura de asistencias</a></li>
            <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/asistenciasextemporaneas'); ?>">Modificación de asistencias o captura extemporanea</a></li>-->
            <li><a class="dropdown-item" href="<?php echo base_url('Docente/horarioGrupos'); ?>">Modificación de asistencias o captura extemporanea</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Docente/resportesAsistencias'); ?>">Reporte de asistencias</a></li>
            <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Grupos'); ?>">Listas de Asistencia</a></li>-->
            <!--<li><a class="dropdown-item" href="<?php echo base_url('Docente/Calificaciones'); ?>">Calificaciones</a></li>-->
            <!--<li><hr class="dropdown-divider"></li>-->
            <!--<li><a class="dropdown-item" href="<?php echo base_url('CapitalHumano/Docentes'); ?>">Listado de Docentes</a></li>-->
          </ul>
        </li>
        
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Herramientas
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="https://mail.google.com/" target="_blank">Mi correo</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Bibliotecas') ?>">Bibliotecas</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="http://172.10.8.207/" target="_blank">Gestor de Proyectos</a></li>
            <li><a class="dropdown-item" href="http://172.10.9.212:8080/" target="_blank">Gestor Documental</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Filtro/Consulta'); ?>">Reportes de Filtro Sanitario</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Login/Logout'); ?>">Cerrar Sesión</a></li>
          </ul>
        </li>
      </ul>
      <div style="color: #fff;">
            Bienvenido
            <?php if ( !is_null(session('nombre')) ):
                echo session('nombre');
            endif;  
            ?>
      </div>
    </div>



<?php echo $this->include('menus/_fin'); ?>