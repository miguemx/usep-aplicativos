<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }

    .formulario-cabecera,
    .formulario-datos {
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }
</style>
<?php if (isset($mensaje)) : ?>
    <div class="alert <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
    </div>
<?php endif; ?>
<div class="container">
    <form action="moficicaritem" method="post">
        <br>
        <div class="card">
            <div class="card-header">
                Modificador de Item
            </div>
            <div class="card-body">
                <div>
                    <div>
                        <div class="mb-3">
                            <div>

                                <label for="etiqueta-nombre" class="form-label">Nombre del campo</label>
                                <input type="text" class="form-control" name="etiqueta-nombre" id="etiqueta-nombre" aria-describedby="helpId" value="<?php echo $nombre ?>">
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>
                            <br>
                            <div>
                                <div class="mb-3">
                                    <label for="help-text" class="form-label">Descripccion del Campo</label>
                                    <input type="text" class="form-control" name="help-text" id="help-text" aria-describedby="helpId" value="<?php echo $descripcion ?>">
                                    <small id="helpId" class="form-text text-muted">Ingresa a una breve indicación de lo que deberán realizar en ese campo</small>
                                </div>
                            </div>
                            <br>
                            <div>

                                <label for="selector-tipo" class="form-label">Tipo de dato</label>
                                <select class="form-select" name="selector-tipo" id="selector-tipo" required>
                                    <option value="0">Selecciona una opccion</option>
                                    <?php foreach ($selecs_tipos as $opciones) : ?>
                                        <?php if ($selectedtipo == $opciones->id) {
                                            $tipo = "selected";
                                        } else $tipo = ""; ?>
                                        <option <?php echo $tipo ?> value="<?php echo $opciones->id ?>"><?php echo $opciones->nombre ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>
                            <br>
                            <div>

                                <label for="inputs-relacionados" class="form-label">Depende de Otro Campo</label>
                                <select class="form-select" name="inputs-relacionados" id="inputs-relacionados">
                                    <option value="0">Selecciona una opccion</option>
                                    <?php if (isset($items_activos)) : ?>
                                        <?php foreach ($items_activos as $items) : ?>
                                            <?php if ($selectedcampo ==  $items->campo) {
                                                $tipo = "selected";
                                            } else $tipo = ""; ?>
                                            <option <?php echo $tipo ?> value="<?php echo $items->campo ?>"><?php echo $items->nombre ?></option>
                                        <?php endforeach;  ?>
                                    <?php endif;  ?>

                                </select>

                            </div>
                            <br>
                            <div>
                                <label for="orden-item" class="form-label">Numero de Secuencia:</label> <br>
                                <input class="form-control" type="number" name="orden-item" id="orden-item" required value="<?php echo $secuencia ?>">
                                <small id="helpId" class="form-text text-muted">Ingrese el orden que tendra en el formulario este item </small>
                            </div>
                            <br>
                            <div class="form-check form-switch text-center">
                                <?php if ($obligatorio != null) {
                                    $tipo = "checked";
                                } else $tipo = ""; ?>
                                <input class="form-check-input" type="checkbox" id="campo-obligatorio" name="campo-obligatorio" value="true" <?php echo $tipo ?>>
                                <label class="form-check-label" for="campo-obligatorio">¿Este campo es obligatorio?</label>
                            </div>
                            <div class="form-check form-switch text-center mt-1">
                                <?php if ($visible == 1) {
                                    $tipo = "checked";
                                } else $tipo = ""; ?>
                                <input class="form-check-input" type="checkbox" id="campo-visible" name="campo-visible" value="true" <?php echo $tipo ?>>
                                <label class="form-check-label" for="campo-visible">¿Este campo es visible en el panel de revisión?</label>
                            </div>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted text-center">
                <button type="submit" class="btn btn-secondary">Modificar Item</button>
                <button class="btn btn-secondary" type="button" id="cancelar">Cancelar</button>
            </div>
        </div>
        <input type="hidden" name="formulario-identificador" value="<?php echo $id_form ?>"> 
        <input type="hidden" name="identidicadoritem" value="<?php echo $id_item ?>">
    </form>
</div>
<form action="DiseñadordeModelo" method="post" id="home-fomr">
    <input type="hidden" name="home-back" value="true">
</form>
<script>
    'use strict';
    const btn_guardar = document.querySelector('#cancelar');
    let regresarhome = () => {
        document.getElementById('home-fomr').submit()
    }
    btn_guardar.addEventListener('click', regresarhome);
</script>

<?php echo $this->endSection() ?>