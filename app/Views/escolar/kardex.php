<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style type="text/css">
.view-edicion {
    display: none;
}
</style>

<div class="container-fluid">
    <div>&nbsp;</div>
    <?php if( $error != null ): ?>
        <div class="alert alert-danger">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php if( $exito != null ): ?>
        <div class="alert alert-success">
            <?php echo $exito; ?>
        </div>
    <?php endif; ?>

    <div>
        <table class="table table-striped">
            <tr>
                <th>Matrícula</th>
                <td><?php echo $alumno->id; ?></td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td><?php echo $alumno->apPaterno.' '.$alumno->apMaterno.' '.$alumno->nombres; ?></td>
            </tr>
            <tr>
                <th>Carrera</th>
                <td><?php echo $carrera->nombre; ?></td>
            </tr>
        </table>
    </div>
    <?php
    $numPeriodosGeneral = 0;
    $sumGeneral = 0;
    ?>
    <?php  if ( count($kardex) < 1 ): ?>
        <div class="alert alert-info">
            El estudiante no cuenta con materias calificadas.
        </div>
    <?php else: ?>
        <?php foreach( $kardex as $periodo=>$materias ): ?>
            <?php $numPeriodosGeneral++; ?>
            <div>
                <h5><?php echo $periodo.' '.$materias[0]->periodoNombre; ?></h5>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>CLAVE</th>
                            <th>ASIGNATURA</th>
                            <th>SECCIÓN</th>
                            <th>CALIFICACIÓN</th>
                            <th>TIPO DE CALIFICACIÓN</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sum = 0;
                        $prom = 0;
                        $mat = 0;
                        $aprobadas = 0;
                        $reprobadas = 0;
                        ?>
                        <?php foreach( $materias as $materia ): ?> 
                            <tr>
                                <td><?php echo $materia->materiaClave ?></td>
                                <td><?php echo $materia->materia ?></td>
                                <td><?php echo $materia->seccion ?></td>
                                <td>
                                    <?php 
                                    $calif = '';
                                    if( $materia->extraordinario ) {
                                        $calif = $materia->extraordinario;
                                    }
                                    else {
                                        $calif = $materia->ordinario;
                                    }
                                    if ( $calif < 6 ) $reprobadas++;
                                    else $aprobadas++;
                                    $sum+=$calif;
                                    ?>
                                    <span class="view-normal-<?php echo $materia->grupo; ?>"><?php echo $calif; ?></span>
                                    <input type="text" class="form-control view-edicion view-edicion-<?php echo $materia->grupo; ?>" value="<?php echo $calif; ?>" id="ecalificacion-<?php echo $materia->grupo; ?>" onkeyup="actualiza('<?php echo $materia->grupo; ?>','calificacion')" />
                                </td>
                                <td>
                                    <span class="view-normal-<?php echo $materia->grupo; ?>"><?php 
                                    if($materia->ordinario == '' && $materia->extraordinario == ''){
                                        echo '---';
                                    } 
                                    elseif($materia->extraordinario) {
                                        echo 'EXT';
                                    }
                                    else {
                                        echo 'ORD';
                                    }
                                    ?></span>
                                    <select class="form-select view-edicion view-edicion-<?php echo $materia->grupo; ?>" id="etipo-<?php echo $materia->grupo; ?>" onchange="actualiza('<?php echo $materia->grupo; ?>','tipo')">
                                        <option value="ORD" <?php if($materia->ordinario) echo 'selected="selected"' ?>>Ordinario</option>
                                        <option value="EXT" <?php if(!$materia->ordinario) echo 'selected="selected"' ?>>Extraordinario</option>
                                    </select>
                                    <input type="text" placeholder="Razón del cambio" class="form-control view-edicion view-edicion-<?php echo $materia->grupo; ?>" id="enota-<?php echo $materia->grupo; ?>" id="enota-<?php echo $materia->grupo; ?>" onkeyup="actualiza('<?php echo $materia->grupo; ?>','nota')" />
                                </td>
                                <td>
                                    <form method="post">
                                        <input type="hidden" name="kx" value="1" />
                                        <input type="hidden" name="matricula" value="<?php echo $alumno->id; ?>" />
                                        <input type="hidden" name="grupo" value="<?php echo $materia->grupo; ?>" />
                                        <input type="hidden" name="calificacion" value="" id="calificacion-<?php echo $materia->grupo; ?>" />
                                        <input type="hidden" name="tipo" value="" id="tipo-<?php echo $materia->grupo; ?>" />
                                        <input type="hidden" name="nota" value="" id="nota-<?php echo $materia->grupo; ?>" />
                                        <button type="button" class="btn btn-secondary btb-sm view-normal-<?php echo $materia->grupo; ?>" onclick="edita('<?php echo $materia->grupo; ?>')">
                                            Editar
                                        </button>
                                        <button type="button" class="btn btn-danger btb-sm view-edicion view-edicion-<?php echo $materia->grupo; ?>" onclick="cancela('<?php echo $materia->grupo; ?>')">
                                            Cancelar
                                        </button>
                                        <button type="submit" class="btn btn-secondary btb-sm view-edicion view-edicion-<?php echo $materia->grupo; ?>" value="esc" name="crx">
                                            Guardar
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php  $mat++; ?>
                        <?php endforeach; ?>
                        <?php 
                            $prom = $sum / $mat;
                            $sumGeneral += $prom;
                        ?>
                        <tr>
                            <td colspan="6">
                                Núm. de asignaturas aprobadas: <?php echo $aprobadas; ?>&nbsp;&nbsp;&nbsp;
                                Núm. de asignaturas reprobadas: <?php echo $reprobadas; ?>&nbsp;&nbsp;&nbsp;
                                Promedio: <?php echo number_format($prom,2,'.',','); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>

<script type="text/javascript">

function edita(grupo) {
    let elemEdicion = document.getElementsByClassName( 'view-edicion-'+grupo );
    let elemNormales = document.getElementsByClassName( 'view-normal-'+grupo );
    for ( let i=0; i<elemEdicion.length; i++ ) {
        let elem = elemEdicion[i];
        elem.style.display = 'block';
    }
    for ( let i=0; i<elemNormales.length; i++ ) {
        let elem = elemNormales[i];
        elem.style.display = 'none';
    }
    actualiza(grupo, 'calificacion');
    actualiza(grupo, 'tipo');
}

function cancela(grupo) {
    let elemEdicion = document.getElementsByClassName( 'view-edicion-'+grupo );
    let elemNormales = document.getElementsByClassName( 'view-normal-'+grupo );
    for ( let i=0; i<elemEdicion.length; i++ ) {
        let elem = elemEdicion[i];
        elem.style.display = 'none';
    }
    for ( let i=0; i<elemNormales.length; i++ ) {
        let elem = elemNormales[i];
        elem.style.display = 'block';
    }
}

function actualiza(grupo, campo) {
    let edit = document.getElementById( 'e'+campo+'-'+grupo ).value;
    document.getElementById( campo+'-'+grupo ).value = edit;
}

</script>

<?php echo $this->endSection() ?>
