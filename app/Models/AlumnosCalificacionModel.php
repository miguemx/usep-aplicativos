<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AlumnosCalificacionModel extends Model {
    protected $table      = 'view_grupos_calificaciones';
    protected $primaryKey = 'alumno_id';

    protected $returnType    = 'App\Entities\AlumnoCalificacion';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    public function getGrupo($materia, $seccion) {
        $this->creaFiltros( ['materia'=>$materia, 'seccion'=>$seccion] );
        $this->orderBy( 'alumno_ap_paterno', 'ASC' );
        $this->orderBy( 'alumno_ap_materno', 'ASC' );
        $this->orderBy( 'alumno_nombres', 'ASC' );
        $grupo = $this->findAll();
        return $grupo;
    }

    /**
     * obtiene todos los alumnos de un grupo en especifico
     * @param grupoId el Id del grupo
     */
    public function getByGrupo($grupoId) {
        $this->where('gpoalumno_grupo', $grupoId);
        return $this->findAll();
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('materia', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['materia']) ) {
                $this->where( 'grupo_materia', $filtros['materia'] );
            }
        }
        if ( array_key_exists('seccion', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['seccion']) ) {
                $this->where( 'grupo_clave', trim($filtros['seccion']) ); 
            }
        }
    }

    /**
     * inserta en el log de classroom
     */
    public function registraclassroom($correo, $idc) {
        $this->query("INSERT INTO logalumnosclassroom ( idc, correo, rev ) VALUES ( '$idc', '$correo', '0' )");
    }

    /**
     * verifica si ya se ha asignado un alumno para evitar
     */
    public function existeclassroom( $correo, $idc ) {
        $qry = $this->query("SELECT * FROM logalumnosclassroom WHERE correo='$correo' AND idc='$idc'");
        $rows = $qry->getResult();
        if ( count($rows) == 0 ) return false;
        else return true;
    }

}