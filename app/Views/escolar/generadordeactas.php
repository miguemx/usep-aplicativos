<?php

use App\Controllers\E;

echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .btn_acta {
        background-color: grey;
        color: white;
    }

    .btn_acta:hover {
        background-color:  #84112c ;
        color: white;
    }
</style>
<div class="container">

    <div class="card" style="margin: 30px 0; ">
        
        <div class="card-body" style="padding: 0;">
            <div class="container" style="margin-top: 15px; padding:0">

                <?php if (isset($error)) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
                <div class="container-fluid">

                    <form method="Post" action="<?php echo base_url('Escolar/ListaDeActas') ?>">

                        <div class="row" style="padding-top: 10px;">
                            <div class="mb-3 row col-sm-4">
                                <label for="matricula">ID de docente</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-sm" id="docente" name="docente" value="<?php echo $docente; ?>" />
                                </div>
                            </div>
                            <div class="mb-3 row col-sm-4">
                                <label for="matricula">IDC grupo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-sm" id="idcgrupo" name="idcgrupo" value="<?php echo $idcgrupo; ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-3 row col-sm-4">
                                <label for="matricula">Periodo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-sm" id="periodo" name="periodo" value="<?php echo $periodo; ?>" />
                                </div>
                            </div>
                            <div class="mb-3 row col-sm-4">
                                <label for="matricula">Sección</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-sm" id="seccion" name="seccion" value="<?php echo $grupo; ?>" />
                                </div>
                            </div>
                            <div class="mb-3 row col-sm-4">
                                <label for="matricula">Materia</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-sm" id="materia" name="materia" value="<?php echo $materia; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="text-center" style="padding: 20px 0px;">
                            <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
                        </div>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="  " style="background-color: #84112c  ; color:white; border: 1px solid black">
                            <tr>
                                <th>IDC - GRUPO</th>
                                <th>SECCIÓN</th>
                                <th>DOCENTE</th>
                                <th>PERIODO</th>
                                <th>MATERIA</th>
                                <th>ACTAS GENERADAS</th>
                               <!--
                                   <th>ACTAS CSV</th>
                                -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lista as $listas) : ?>
                                <?php if ($listas->grupo_idc != '') { ?>
                                    <tr>
                                        <td><?php echo $listas->grupo_idc ?></td>
                                        <td><?php echo $listas->grupo_clave ?></td>
                                        <td><?php echo $listas->docente . " " . $listas->empleado_nombre . "" . $listas->empleado_apellido ?></td>
                                        <td><?php echo $listas->periodo ?></td>
                                        <td><?php echo $listas->materia ?></td>
                                        <td style="width: 250px; padding-left:25px">
                                            <table>
                                                <?php
                                                $i = null;
                                                foreach ($actas as $key) {
                                                    if ($key['idc'] == $listas->grupo_idc) {
                                                        if ($i == 3) {
                                                            echo "<br>";
                                                            $i = 0;
                                                        }
                                                        echo ' <form action="' . base_url('Escolar/generadoracta/' . $key['id']) . ' " > ';
                                                        echo '<button style="margin-left:10px;margin-top:5px; width:55px; height: 30px" type="submit" class="btn btn-sm btn_acta"> ' . $key['id'] . '</button>  ';
                                                        //echo $key['folio']." ";
                                                        echo '</form>';
                                                        $i++;
                                                    }
                                                } ?>
                                            </table>
                                        </td>
                                        <!-- <td>
                                        <table>
                                                <?php
                                                /* $i = null;
                                                foreach ($actas as $key) {
                                                    if ($key['idc'] == $listas->grupo_idc) {
                                                        if ($i == 3) {
                                                            echo "<br>";
                                                            $i = 0;
                                                        }
                                                        echo ' <form action="' . base_url('Escolar/generadoractaCSV/' . $key['id']) . ' " > ';
                                                        echo '<button style="margin-left:10px;margin-top:5px; width:55px; height: 30px" type="submit" class="btn btn-sm btn_acta"> ' . $key['id'] . '</button>  ';
                                                        //echo $key['folio']." ";
                                                        echo '</form>';
                                                        $i++;
                                                    }
                                                } */ ?>
                                            </table>
                                        </td> -->
                                    </tr>
                                <?php } ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="card-footer text-muted">
             
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>
