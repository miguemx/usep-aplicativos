<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>

<div class="container ">
    <?php if (isset($mensajeok)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $mensajeok; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($mensaje)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensaje; ?>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
        <div class="row ">
            <h1 class="text-center my-2">Modificación de asistencias</h1>
            <div class="accordion accordion-flush mt-2" id="menu_estadisticcas">
                <form action="<?php echo base_url('docente/buscaasistencias'); ?>" method="post">
                    <div class="input-group mb-3 col">
                        <span class="input-group-text" id="inputGroup-sizing-default">FECHA</span>
                        <input name="fecha" type="date" id="fecha" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $fecha ?>" readonly>
                        <span class="input-group-text" id="inputGroup-sizing-default">HORA</span>
                        <input name="hora" type="time" id="hora" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $hora ?>" readonly>
                    </div>
                    </div>
                    <div class="row">
                        <br>
                        <br>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">GRUPO</span>
                            <input disabled name="grupo" type="text" id="grupo" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $grupo ?>">
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">MATERIA</span>
                            <input disabled name="materia" type="text" id="materia" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $materia ?>">
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">AULA</span>
                            <input disabled name="aula" type="text" id="aula" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $aula ?>">
                        </div>
                    </div>
                </form>
            <div class="table-responsive">
            <div>
                <form action="<?php echo base_url('docente/guardaactualizacion'); ?>" method="post">
                    <input hidden name="docente" type="" value="<?php echo $docente; ?>" />
                    <input hidden name="idgrupo" type="text" id="idgrupo" value="<?php echo $idgrupo ?>">
                    <input hidden name="aula" type="text" id="aula" value="<?php echo $aula ?>">
                    <input hidden name="fechayhora" id="fechayhora" value="<?php echo $fechayhora ?>">
                    <input hidden name="fechamin" id="fechamin" value="<?php echo $fechamin ?>">
                    <input hidden name="fechamax" id="fechamax" value="<?php echo $fechamax ?>">
                    <input hidden name="grupo" id="grupo" value="<?php echo $grupo ?>">
                    <table class="table table-striped table-bordered">
                        <thead class="  " style="background-color: #84112c  ; color:white; border: 1px solid black">
                            <tr>
                                <th style="width: 30px; ">selecciona</th>
                                <th style="width: 100px; ">No.</th>
                                <th style="width: 180px; ">APELLIDO PATERNO</th>
                                <th style="width: 180px; ">APELLIDO MATERNO</th>
                                <th style="width: 180px; ">NOMBRE</th>
                                <th style="width: 125px; ">TIPO DE ASISTENCIA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $contador = 0;
                            foreach ($alumno as $alumnos) : ?>
                                <tr>
                                    <?php echo "<td><input type='checkbox' class='checkbx' onChange='comprobar(this);' id='chbox' form-control text-center cal-in' type='number' name='cbox[]' value='$alumnos->id_alumno'></td>";?>
                                    <?php echo "<td hidden ><input disabled name='idalumno[]' id='$alumnos->id_alumno' value='$alumnos->id_alumno' ></td>"; ?>
                                    <td><?php echo $contador = $contador + 1; ?></td>
                                    <td class="text-left"><?php echo $alumnos->alumno_ap_paterno?></td>
                                    <td><?php echo $alumnos->alumno_ap_materno?></td>
                                    <td><?php echo $alumnos->nombre_alumno?></td>
                                    <?php
                                        echo "<td style='text-align: center; vertical-align: middle;'>
                                                <select disabled id=".$alumnos->id_alumno.'a'." value='$alumnos->id_alumno' name='asistencia[]'>
                                                    <option value= 'ASISTENCIA'"?> <?php if($alumnos->tipo_asistencia == 'ASISTENCIA') echo 'selected="selected"'; ?><?php echo ">
                                                    ASISTENCIA</option>
                                                    <option value= 'INASISTENCIA'"?> <?php if($alumnos->tipo_asistencia == 'INASISTENCIA') echo 'selected="selected"'; ?><?php echo ">
                                                    INASISTENCIA</option>
                                                    <option VALUE='RETARDO'"?> <?php if($alumnos->tipo_asistencia == 'RETARDO') echo 'selected="selected"'; ?><?php echo ">
                                                    RETARDO</option>
                                                    <option VALUE='FALTA JUSTIFICADA'"?> <?php if($alumnos->tipo_asistencia == 'FALTA JUSTIFICADA') echo 'selected="selected"'; ?><?php echo ">
                                                    FALTA JUSTIFICADA</option>
                                                </select>
                                            </td>"?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                        <div class="text-center mt-1">
                            <button hidden type="submit" id='btnactualizar' class="btn btn-secondary">Actualizar</button>
                        </div>
                </form>
            <div>
        </div>
    </div>
<script>
    function comprobar(obj){
     if (obj.checked){
        console.log(obj.value);
        ids = String(obj.value);
       document.getElementById(ids).disabled = false;
       document.getElementById(ids+'a').disabled = false;
       document.getElementById("btnactualizar").hidden = false;

     } 
     if(obj.checked != true){
       document.getElementById(ids).disabled = true;
       document.getElementById(ids+'a').disabled = true;
       var suma = 0;
       var los_cboxes = document.getElementsByName('cbox[]'); 
        console.log(los_cboxes);
        for (var i = 0, j = los_cboxes.length; i < j; i++) {
            
            if(los_cboxes[i].checked == true){
            suma++;
            }
        }
      
         if (suma == 0) {       
            document.getElementById("btnactualizar").hidden = true;
        } 

     }
   } 
</script>
<script>
    function habilitar(datos) {
        if (datos.checked){
            document.getElementById(datos.value).disabled = false;
            document.getElementById("btn2").hidden = false;
        } 
        if(datos.checked != true){
            document.getElementById(datos.value).disabled = true;
        }
    }
</script>
<?php echo $this->endSection() ?>