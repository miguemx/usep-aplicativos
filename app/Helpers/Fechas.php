<?php
namespace App\Helpers;

class Fechas {

    /**
     * convierte un formato de fecha de dd/mm/aaaa a formato de mysql
     * @param fecha la fecha en formato dd/mm/aaaa
     * @param force (opcional) indica true si se desea forzar la conversión; en caso contrario se devolverá false si hay error
     * @return convertida la fecha convertida en formato aaaa-mm-dd
     * @return false si la fecha no se puede convertir o bien no se indica la bandera force
     */
    public static function ddmmaaaa2Mysql($fecha, $force=false) {
        if ( preg_match( "/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $fecha ) ) {
            $datosFecha = explode( "/", $fecha );
            $convertida = $datosFecha[2].'-'.$datosFecha[1].'-'.$datosFecha[0];
            return $convertida;
        }
        if ( !$force ) return false;
        else return '0000-00-00';
    }

}