<?php
namespace App\Models;
use CodeIgniter\Model;

class VotacionesModel extends Model {
    protected $table      = 'view_votaciones';
    protected $primaryKey = 'proceso_id';

    protected $returnType    = 'App\Entities\Votacion';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        "proceso_id", "proceso_nombre","proceso_inicio","proceso_fin","padron_id","padron_correo","padron_correo","candidatos_id","candidatos_correo","candidatos_nombre",
        "candidatos_voto","candidatos_ganador",
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function buscaprocesos($idproceso){
        $consulta = $this->where('proceso_id', $idproceso)->findAll();
        return $consulta;
    }
}