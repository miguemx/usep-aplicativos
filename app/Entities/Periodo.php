<?php

namespace App\Entities;

use CodeIgniter\Entity;

class Periodo extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'nombre' => null,
        'inicio' => null,
        'fin' => null,
        'estado' => null,
        'inicio_c' => null,
        'fin_c' => null,
        'evaluaciones' => null,
        'vacaciones' => null,
        'administrativa' => null,
        'receso' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => "periodo_id",
        'nombre' => "periodo_nombre",
        'inicio' => "periodo_inicio",
        'fin' => "periodo_fin",
        'estado' => "periodo_estado",
        'inicio_c' => 'periodo_const_inicio',
        'fin_c' => 'periodo_const_fin',
        'evaluaciones' => 'periodo_const_evaluaciones',
        'vacaciones' => 'periodo_const_vacaciones',
        'administrativa' => 'periodo_const_administrativas',
        'receso' => 'periodo_const_receso',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
