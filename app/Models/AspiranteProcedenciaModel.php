<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AspiranteProcedenciaModel extends Model {
    protected $table      = 'view_aspirantes_procedencia';
    protected $primaryKey = 'aspirante_folio';

    protected $returnType    = 'App\Entities\AspirantesProcedencia';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;
    /**
     * Compilado de id de municipios metropolitanos
     * 1693->puebla
     * 1698->San Andrés Cholula
     * 1719->San Pedro Cholula	
     * 1594->Amozoc
     * 1620->Cuautlancingo
     * 1613->Coronango
     * 1685->Ocoyucan
     * 1669->Juan C. Bonilla
     * 1704->San Gregorio Atzompa
     * 1760->Tlaltenango
     */
    protected $metropolitanos = [
        '1693','1698','1719','1594','1620','1613','1685','1669','1704','1760'
    ];

    /**
     * @return query contrene todos los aspirantes que pertenencen al area metropolitana
     */
    public function getMetropolinatos()
    {
        $query = $this->asArray()->whereIn('municipio_id',$this->metropolitanos)->find();
        return $query;
    }

    /**
     * @return query contiene todos los aspirantes del interior del estado
     */
    public function getInterior()
    {
        $query = $this->asArray()->whereNotIn('municipio_id',$this->metropolitanos)->find();
        return $query;
    }

    /**
     * @return query contiene todos los aspirantes que no pertenecen al estado de Puebla
     */
    public function getOtrosEstados()
    {
        $query = $this->asArray()->where('estado_id != 21')->find();
        return $query;
    }

    /**
     * @return query contiene todos los aspirantes que no pertenecen a Mexico
     */
    public function getExtranjeros()
    {
        $query = $this->asArray()->where('municipio_id = ','0')->find();
        return $query;
    }


}