<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroAccesosReportesModel extends Model {
    protected $table      = 'view_filtro_accesos';
    protected $primaryKey = 'filtroacceso_id';

    protected $returnType    = 'App\Entities\FiltroAccesoReporte';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros=array()) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros );
        $registros = $this->paginate(25);
        return $registros;
    }

    /**
     * crea los filtros where para la busqueda de registros
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        if ( array_key_exists('inicio', $filtros) ) { 
            if ( strlen($filtros['inicio']) ) {
                $this->where( 'fecha_acceso >=', $filtros['inicio'].' 00:00:01' );
            }
        }
        if ( array_key_exists('fin', $filtros) ) { 
            if ( strlen($filtros['fin']) ) {
                $this->where( 'fecha_acceso <=', $filtros['fin'].' 23:59:59' );
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { 
            if ( strlen($filtros['nombre']) ) {
                $this->groupStart();
                $this->like( 'empleado_nombre', $filtros['nombre'], 'both' );
                $this->orLike( 'empleado_ap_paterno', $filtros['nombre'], 'both' );
                $this->orLike( 'empleado_ap_materno', $filtros['nombre'], 'both' );
                $this->orLike( 'alumno_nombres', $filtros['nombre'], 'both' );
                $this->orLike( 'alumno_ap_materno', $filtros['nombre'], 'both' );
                $this->orLike( 'alumno_ap_materno', $filtros['nombre'], 'both' );
                $this->orLike( 'externo_nombre', $filtros['nombre'], 'both' );
                $this->groupEnd();
            }
        }
        
        $this->orderBy('fecha_acceso','DESC');
        $this->orderBy('fecha_cuestionario','DESC');
    }

}