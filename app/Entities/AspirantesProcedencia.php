<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesProcedencia extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'folio' => null,
        'municipio' => null,
        'm_nombre' => null,
        'estado' => null,
        'e_nombre' => null,
   ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'folio' => 'aspirante_folio',
        'municipio' => 'municipio_id',
        'm_nombre' => 'municipio_nombre',
        'estado' => 'estado_id',
        'e_nombre' => 'estado_nombre',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}