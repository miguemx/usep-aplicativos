<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Alumno;

class MateriaModel extends Model
{
    protected $table      = 'aca_materias';
    protected $primaryKey = 'materia_clave';

    protected $returnType    = 'App\Entities\Materia';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'materia_clave', 'materia_carrera', 'materia_nombre', 'materia_nombre_corto', 'materia_creditos',
        'materia_obligatoria', 'materia_tipo', 'materia_clasificacion', 'materia_seriacion', 'materia_semestre','materia_horas_semanales',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    /**
     * busca un alumno mediante su direccion de correo
     * @param carreraId el correo que se va a buscar
     * @return array con la lista de las carreras de la materia
     */
    public function findByCarrera($carreraId)
    {
        $materias = $this->where('materia_carrera', $carreraId)->findAll();
        return $materias;
    }

    /**
     * Ontiene toda la informacion de un estudiante
     * @param matricula el ID o matricula del estudiante
     * @return App\Models\Alumno el objeto alumno con toda su informacion
     * @return null si no se encuentra el estudiante
     */
    public function getInfo($matricula)
    {
        $alumno = $this->find($matricula);
        if (!is_null($alumno)) {
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            $alumno->carrera = $carrera;

            $segurosModel = new AlumnosSegurosModel();
            $seguro = $segurosModel->find($alumno->id);
            $alumno->ssn = $seguro->ssn;
        }
        return $alumno;
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros = array())
    {
        $pager = \Config\Services::pager();
        $this->creaFiltros($filtros);
        $alumnos = $this->paginate(25);
        return $alumnos;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if (array_key_exists('id', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['id'])) {
                $this->where('alumno_id', $filtros['id']);
            }
        }
        if (array_key_exists('apPaterno', $filtros)) { // -- filtrar por apPaterno
            if (strlen($filtros['apPaterno'])) {
                $this->where('alumno_ap_paterno', $filtros['apPaterno']);
            }
        }
        if (array_key_exists('apMaterno', $filtros)) { // -- filtrar por materno
            if (strlen($filtros['apMaterno'])) {
                $this->where('alumno_ap_materno', $filtros['apMaterno']);
            }
        }
        if (array_key_exists('nombre', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['nombre'])) {
                $this->where('alumno_nombres', $filtros['nombre']);
            }
        }

        $this->orderBy('alumno_ap_paterno', 'ASC');
        $this->orderBy('alumno_ap_materno', 'ASC');
        $this->orderBy('alumno_nombres', 'ASC');
    }

    public function buscarMaterias($valores)
    {
        $consulta = $this->where('materia_carrera', trim($valores))
            //->where('grupo_periodo',$periodo)
            ->findAll();
        return $consulta;
    }


    public function buscarGrupos($valores, $periodo)
    {

        $consulta = $this->distinct()
            ->where('materia_nombre', trim($valores))
            //->where('grupo_periodo',$periodo)
            ->findAll();

        return $consulta;
    }
    public function buscarsemestre($semestre, $carrera)
    {

        $consulta = $this->distinct()
            ->where('materia_semestre', $semestre)
            ->where('materia_carrera', $carrera)
            ->findAll();

        return $consulta;
    }

    /**
     * @return query arreglo de materias por semestre correspondiente a la carrera solicitada
     * @param semestre 
     * @param carrera 
     */
    public function getMaterias($semestre, $carrera)
    {
        return $this->distinct()->where('materia_semestre', $semestre)->where('materia_carrera', $carrera)->orderBy('materia_clave','ASC')->findAll();
    }

    /**
     * busca la info de una materia mediante su id
     * @param materia id de la materia
     * @return consulta info de la materia
     */
    public function getMateriaById($materia){
        $consulta=$this->where('materia_clave',$materia)
                       ->first();
        return $consulta;
    }
}
