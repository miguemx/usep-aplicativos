
CREATE TABLE `alumnos_documentos_fisicos` (
  `aludocfis_id` varchar(9) NOT NULL,
  `aludocfis_acta` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)',
  `aludocfis_curp` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)',
  `aludocfis_identificacion` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)',
  `aludocfis_cert` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)',
  `aludocfis_cert_legal` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)',
  `aludocfis_constancia` varchar(1) NOT NULL DEFAULT '0' COMMENT '1=entregado;0=adeudo;NULL=no obligatorio (cambiar a nullable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Almacena una lista de chequeo para la entrega física de docs';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos_documentos_fisicos`
--
ALTER TABLE `alumnos_documentos_fisicos`
  ADD PRIMARY KEY (`aludocfis_id`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnos_documentos_fisicos`
--
ALTER TABLE `alumnos_documentos_fisicos`
  ADD CONSTRAINT `alumnos_documentos_fisicos_ibfk_1` FOREIGN KEY (`aludocfis_id`) REFERENCES `alumnos_generales` (`alumno_id`);