<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<style>

.input-grupo.input-grupo-outline .formulario-control {
    background: none;
    border: 1px solid red;
    border-radius: 0.375rem;
    border-top-left-radius: 0.375rem !important;
    border-bottom-left-radius: 0.375rem !important;
    padding: 0.625rem 0.75rem
rem
 !important;
    line-height: 1.3 !important;
}

.div-select{

}

.negro{
    color: black !important;
}

.seleccion{
    border: 1px red solid !important;
}

.clase1{
    display: none;
}

.input-error {
        border: 3px red solid !important;
        border-radius: 10px;
        border-top: 0px !important;
        border-left: 0px !important;
    }

</style>
<h4>Bienvenido(a)</h4>
<div>
    <p class="negro" style="color: #000;">
        Complete su registro como aspirante llenando los siguientes datos:
    <p>
<hr />
<div class="container"> 
    <?php if(isset($msg)): ?>
        <div class="alert alert-secondary">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <form role="form" method="post" name ="formulario" id="formulario" action="<?php echo base_url('Admision/regAspiranteComp') ?>">
        <?php if (($carrera=='2') and ($revalida_flg=='1')){ 
                $band_valida_radios = 1;
                ?>
                <div class="card  mb-2">
                    <div class="card-body">

                <p>¿Desea realizar revalidación o equivalencia de materias de otra Institución de Educación Superior?<p>
                <div id="radios">
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="revalidar" id="customRadio1" value = "SI">
                        <label class="custom-control-label" for="customRadio1">Si</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="revalidar" id="customRadio2" value = "NO">
                        <label class="custom-control-label" for="customRadio2">No</label>
                    </div>
                </div>

                    </div>
                </div>
                <?php } 
          else { $band_valida_radios = 0;?>
                 <input type="hidden" id="revalidar" name="revalidar" value="NO">
             <?php }?>

        <div class="card  mb-2">
            <div class="card-body">


        <div class="input-group input-group-static my-3">
            <label class="form-label negro">CURP</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->curp; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-static my-3">
            <label class="form-label negro">Primer Apellido</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->apPaterno; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-static my-3">
            <label class="form-label negro">Segundo Apellido</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->apMaterno; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-static my-3">
            <label class="form-label negro">Nombre(s)</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->nombre; ?>" disabled="disabled" />
        </div>


          </div>
          </div>


          <div class="card  mb-2">
                    <div class="card-body">

        <p>
            <b>Dirección:<b>
        </p>
        <div class="input-group input-group-static mb-3">
            <label class="form-label negro">Calle</label>
            <input type="text" class="form-control obligatorio" name="calle" id="calle"/>
        </div>
        
        <div class="input-group input-group-static mb-3">
            <label class="form-label negro">Número exterior</label>
            <input type="text" class="form-control obligatorio" name="numext" id ="numext"/>
        </div>
        <div class="input-group input-group-static mb-3">
            <label class="form-label negro">Número Interior</label>
            <input type="text" class="form-control" name="numint" id="numint"/>
        </div>
        <div class="input-group input-group-static mb-3">
            <label class="form-label negro">Colonia</label>
            <input type="text" class="form-control obligatorio" name="colonia" id="colonia"/>
        </div>
        
        <div class="input-group input-group-static mb-3 div-select">
            <label class="form-label negro">País de residencia</label>
            <select type="text" class="form-control obligatorio" name="pais" id ="pais" onchange="mostrar_estado_municipio();">
                <option value ="0">Seleccione una opción</option>
                <option value ="EXTRANJERO">Extranjero</option>
                <option value ="MÉXICO">México</option>
            </select>
        </div>
        <div class="clase1"  id="bloque" >
            <div class="input-group input-group-static mb-3 div-select"  id="selectestado" >
                <label class="form-label negro">Estado</label>
                <select type="text" class="form-control obligatorio" name="estado"  id ="estado" onchange ="cambiaMunicipios()">
                    <option value ="0">Seleccione una opción</option>
                     <?php foreach ($estados as $estadosall) : ?>
                    <option value = <?php echo $estadosall->id ?>>
                        <?php echo $estadosall->nombre;?>
                    </option>
                    <?php endforeach;?> 
                </select>
            </div>
            <div class="input-group input-group-static mb-3 div-select" name="selectmunicipio" id="selectmunicipio">
                <label class="form-label negro">Municipio</label>
                <select type="text" class="form-control" name="municipio" id="municipio">
                    <option value ="0">Seleccione una opción</option>
                </select>

            </div> 
            <div class="input-group input-group-static mb-3" id ="codigopostal">
                <label class="form-label negro">Código Postal</label>
                <input type="text" class="form-control" name="cp" id="cp" onkeypress="return valideKey(event);"/>
            </div>
            
        </div>
        <div class="input-group input-group-static mb-3">
                <label class="form-label negro">Teléfono (10 dígitos)</label>
                <input type="text" class="form-control obligatorio" name="telefono" id="telefono" onblur="validaTelefono();"/>
            </div>
            



                     </div>
                     </div>



                     <div class="card  mb-2">
                    <div class="card-body">


            <p>
                <b>Datos de nacimiento:</b>
            </p>
       
        
        <div class="input-group input-group-static mb-3 div-select">
            <label class="form-label negro">Sexo</label>
            <select type="text" class="form-control obligatorio" name="sexo" id="sexo" onchange ="validasex()">
                <option value ="0">Seleccione una opción</option>
                <option value ="M">MUJER</option>
                <option value ="H">HOMBRE</option>
            </select>
        </div>
        <div class="input-group input-group-static mb-3">
            <label for ="fecha_nacimiento" class ="negro">Fecha de nacimiento &nbsp&nbsp&nbsp</label>
            <input type="date" class="form-control obligatorio" name="fecha_nacimiento" id="fecha_nacimiento" />
        </div>
        <div class="input-group input-group-static mb-3 div-select">
            <label class="form-label negro">País de nacimiento</label>
            <select type="text" class="form-control obligatorio" name="pais_nac" id ="pais_nac" onchange="mostrar_estado_municipio_nac();">
            <option value ="0">Seleccione una opción</option>
            <option value ="EXTRANJERO">Extranjero</option>    
            <option value ="MÉXICO">México</option>
            </select>
        </div>
        <div class="clase1"  id="bloque_nac" >
            <div class="input-group input-group-static mb-3"  id="selectestado_nac" >
                <label class="form-label negro">Estado</label>
                <select type="text" class="form-control" name="estados_nac"  id ="estados_nac" onchange ="cambiaMunicipios_nac()">
                    <option value ="0">Seleccione una opción</option>
                     <?php foreach ($estados as $estadosall) : ?>
                    <option value = <?php echo $estadosall->id ?>>
                        <?php echo $estadosall->nombre;?>
                    </option>
                    <?php endforeach;?> 
                </select>
            </div>
            <div class="input-group input-group-static mb-3" id ="selectmunicipio_nac">
                <label class="form-label negro">Municipio</label>
                <select type="text" class="form-control" name="municipios_nac" id="municipio_nac">
                    <option value ="0">Seleccione una opción</option>
                </select>
            </div> 
        </div>
        


                     </div>
                     </div>

        
        <div class="text-center">
            <input type="hidden" name="asp_upd" id = "asp_upd" />
            <button type="button" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0" id="btnReg" >Guardar</button>
        </div>
    </form>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function valida_radios(){
        var elmt_radio1 =document.getElementById("customRadio1");
        var elmt_radio2 =document.getElementById("customRadio2");
        var elmt_radios =document.getElementById("radios");
        var error_radios =true;
        if (elmt_radio1.checked || elmt_radio2.checked){
            elmt_radios.classList.remove('input-error');
            error_radios =false;
        }else {
            elmt_radios.classList.add('input-error');
            error_radios =true;            
        }
        return error_radios
    }
    </script>
<script>
    function valideKey(evt){
    
    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}
    function validacadenanumero(){ // onblur="validacadenanumero()" esta linea va en el input a validar
    var inputtxt = document.getElementById('cp'); 
    var valor = inputtxt.value;
    for(i=0;i<valor.length;i++){
     var code=valor.charCodeAt(i);
         if(code<=48 || code>=57){          
           inputtxt.value=""; 
           return;
         }    
   }
  
}
function validaTelefono(){
    var inputtxt = document.getElementById('telefono'); 
    var valor = inputtxt.value;
    var esnumero = false
    for(i=0;i<valor.length;i++){
     var code=valor.charCodeAt(i);
         if(code<=48 || code>=57){          
           //inputtxt.value=""; 
            esnumero=false;
           return;
         } else{
             esnumero=true;
         }
   }
  if (esnumero == false || valor.length!=10){
    inputtxt.value=""; 
  }
}

    function validasex(){
        var elmt_sexo = document.getElementById("sexo");
        if (elmt_sexo.value == '0'){
            elmt_sexo.classList.add("obligatorio");
        }
        else if (elmt_sexo.value =='MASCULINO' || elmt_sexo.value =='FEMENINO'){
            console.log("ENTRA AL IF DEL SEX");
            elmt_sexo.classList.remove("obligatorio");
            elmt_sexo.parentElement.classList.remove("input-error");
        }
        
    }
 
	function mostrar_estado_municipio() {
        var elmt_estado = document.getElementById("estado");
        var elmt_municipio = document.getElementById("municipio");
        var elmt_cp = document.getElementById("cp");
        var elmt_pais = document.getElementById("pais");
		if (document.getElementById("pais").value=="EXTRANJERO" || document.getElementById("pais").value=="0") {
            document.getElementById("bloque").style.display = "none";
            if (elmt_estado.classList.contains('obligatorio')){elmt_estado.classList.remove('obligatorio');}
            if (elmt_municipio.classList.contains('obligatorio')){elmt_municipio.classList.remove('obligatorio');}
            if (elmt_cp.classList.contains('obligatorio')){elmt_cp.classList.remove('obligatorio');}
            if (document.getElementById("pais").value=="0")
                    {elmt_pais.classList.add('obligatorio');}
                else 
                    {
                    elmt_pais.classList.remove('obligatorio');
                    //elmt_pais.parentElement.classList.remove('obligatorio');
                    }
		}

       else if ((document.getElementById("pais").value=="MÉXICO")) {
        document.getElementById("bloque").style.display = "block"
        elmt_estado.classList.add('obligatorio');
        elmt_municipio.classList.add('obligatorio');
        elmt_cp.classList.add('obligatorio');
        elmt_pais.classList.remove('obligatorio');
        }
	}

    var cambiaMunicipios = async () => {
        let estado = document.getElementById('estado').value;
        const formData = new FormData();
        console.log(estado);
        formData.append('estado', estado);
        document.getElementById('municipio').innerHTML = '';
        let response = await fetch('<?php echo base_url("Admision/obtenerMunicipios/1") ?>', {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            console.log('aqui');
            return result;
            
        }).catch(error => {
            console.log('aqui esta el error');

            console.error('Error:', error);
        });
        document.getElementById('municipio').innerHTML = '';
        console.log(municipio);
        for (let i = 0; i < response.length; i++) {
            let option = document.createElement('option');
            option.value = response[i].id_municipio;
            option.innerHTML = response[i].nombre_municipio;
            document.getElementById('municipio').appendChild(option);
        }
        
    }
    function mostrar_estado_municipio_nac() {
        var elmt_estado_nac = document.getElementById("estados_nac");
        var elmt_municipio_nac = document.getElementById("municipio_nac");
        var elmt_pais_nac = document.getElementById("pais_nac");
        if (document.getElementById("pais_nac").value=="0" || document.getElementById("pais_nac").value=="EXTRANJERO") {
            document.getElementById("bloque_nac").style.display = "none";
            if (elmt_estado_nac.classList.contains('obligatorio')){elmt_estado_nac.classList.remove('obligatorio');}
            if (elmt_municipio_nac.classList.contains('obligatorio')){elmt_municipio_nac.classList.remove('obligatorio');}
            if (document.getElementById("pais_nac").value=="0")
                    {elmt_pais_nac.classList.add('obligatorio');}
                else 
                    {elmt_pais_nac.classList.remove('obligatorio');}
        }
        else if ((document.getElementById("pais_nac").value=="MÉXICO")) {
            document.getElementById("bloque_nac").style.display = "block";
            elmt_estado_nac.classList.add('obligatorio');
            elmt_municipio_nac.classList.add('obligatorio');
        }
    }

    var cambiaMunicipios_nac = async () => {
        let estado = document.getElementById('estados_nac').value;
        const formData = new FormData();
        console.log(estado);

        formData.append('estado_nac', estado);
        document.getElementById('municipio_nac').innerHTML = '';
        let response_nac = await fetch('<?php echo base_url("Admision/obtenerMunicipios/2") ?>', {
            method: 'POST',
            body: formData,
        }).then(response_nac => response_nac.json()).then(result => {
            console.log('aqui');
            return result;
            
        }).catch(error => {
            console.log('aqui esta el error');
            console.error('Error:', error);
        });
        document.getElementById('municipio_nac').innerHTML = '';
        console.log(municipio);
        for (let i = 0; i < response_nac.length; i++) {
            let option = document.createElement('option');
            option.value = response_nac[i].id_municipio;
            option.innerHTML = response_nac[i].nombre_municipio;
            document.getElementById('municipio_nac').appendChild(option);
        }
        
    }

    'use strict';
    const btn_guardar = document.querySelector('#btnReg');
    let validar = () => {
        let inputs_requeridos = document.getElementsByClassName('obligatorio');
        let selects_requeridos = document.getElementsByClassName('div-select');
        let error_empty = false;
        if (document.getElementById('pais').value==0){
            document.getElementById('pais').parentElement.classList.add('input-error');
        }
        else {
            document.getElementById('pais').parentElement.classList.remove('input-error');
        }
        if (document.getElementById('pais_nac').value==0){
            document.getElementById('pais_nac').parentElement.classList.add('input-error');
        }
        else {
            document.getElementById('pais_nac').parentElement.classList.remove('input-error');
        }
        for (let j = 0; j < inputs_requeridos.length; j++) {
            if (inputs_requeridos[j].value == '' || inputs_requeridos[j].value == '0' ) {
                inputs_requeridos[j].parentElement.classList.add('input-error');
                error_empty = true;
            } else {
                inputs_requeridos[j].parentElement.classList.remove('input-error');
                inputs_requeridos[j].classList.remove('input-error');

            }
        }
        return error_empty;
    };

    let obtener_datos = () => {
        let flg_validar = validar();
        let flg_valida_radios_bis = <?php echo $band_valida_radios; ?>;
        let flg_valida_radios = false;
        if (flg_valida_radios_bis == 1){
            flg_valida_radios = valida_radios();
        } 
            
        
        if (flg_validar || flg_valida_radios) {
            Swal.fire({
                'title': 'ERROR',
                'text': 'Por favor revise los campos marcados en rojo ',
                'icon': 'error',
                confirmButtonColor: '#883232',


            });
        } else {
            Swal.fire({
                'title': '¡ATENCIÓN!',
                'text': 'No podrá realizar cambios posteriores una vez que continue ¿Está seguro(a) que desea continuar? ',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });

                    document.getElementById('formulario').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'No se ha guardado ninguna información',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }

    }
    btn_guardar.addEventListener('click', obtener_datos);

</script>


<?php echo $this->endSection() ?>