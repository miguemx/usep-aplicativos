<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroRespuestasPreguntasModel extends Model {
    protected $table      = 'view_filtro_respuesta';
    protected $primaryKey = 'filtroresp_id';

    protected $returnType    = 'App\Entities\FiltroRespuestaPregunta';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * devuelve las respuestas con las respectivas preguntas de un folio determinado
     * @param folio el numero de folio
     * @return respuestas
     */
    public function findByFolio($folio) {
        $this->where( 'filtroresp_folio', $folio );
        $respuestas = $this->findAll();
        return $respuestas;
    }
    
}