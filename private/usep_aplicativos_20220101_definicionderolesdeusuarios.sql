-- rol de administrador
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'admision','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'basecontroller','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'capitalhumano','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'credenciales','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'e','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'escolar','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'filtrosanitario','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'inscripcionreinscripcion','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'jobs','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'login','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'pagos','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'renderfiles','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'segurofacultativo','*');

-- rol de recursos humanos
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'capitalhumano','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'credenciales','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'e','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'escolar','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (2,'segurofacultativo','*');

-- rol de servicios escolares
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'admision','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'credenciales','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'e','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'escolar','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'inscripcionreinscripcion','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'pagos','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (3,'segurofacultativo','*');

-- rol de alumnos
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'admision','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'credenciales','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'login','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4,'segurofacultativo','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (4, 'bibliotecas', '*');

-- rol general
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'estudiantes','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'home','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (5,'renderfiles','*');

-- rol revisor documental
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (6,'admision','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (6,'credenciales','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (6,'e','*');

-- rol evaluador

-- filtro sanitario
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'bibliotecas','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'calificaciones','*');
INSERT INTO `aplicativo_permisos`(`permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (1,'filtrosanitario','*');