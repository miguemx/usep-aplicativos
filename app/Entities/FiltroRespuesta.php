<?php
namespace App\Entities;

use CodeIgniter\Entity;

class FiltroRespuesta extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'        => null,
        'pregunta'  => null,
        'folio'     => null,
        'respuesta' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'        => 'filtroresp_id',
        'pregunta'  => 'filtroresp_pregunta',
        'folio'     => 'filtroresp_folio',
        'respuesta' => 'filtroresp_respuesta',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
