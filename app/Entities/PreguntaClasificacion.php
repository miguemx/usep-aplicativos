<?php

namespace App\Entities;

use CodeIgniter\Entity;

class PreguntaClasificacion extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'nombre' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'id' => 'clasificacion_id',
        'nombre' => 'clasificacion_nombre',
        
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
