<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">


    <h2>Seguro facultativo</h2>
    <hr />
    <?php $errorMsg = $session->getFlashdata('error'); ?>
    <?php $success = $session->getFlashdata('msg'); ?>

    <?php if( !is_null($errorMsg) ): ?>
        <div class="alert alert-danger">
            <?php echo $errorMsg; ?>
        </div>
    <?php endif; ?>

    <?php if( !is_null($success) ): ?>
        <div class="alert alert-success">
            <?php echo $success; ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <form method="post">
                <div class="row">
                    <button type="submit" class="btn btn-secondary col" name="seleccionar" value="TODOS">
                        Todos
                    </button>
                    <button type="submit" class="btn btn-secondary col" name="seleccionar" value="SOLICITADO">
                        Quienes solicitaron
                    </button>
                    <button type="submit" class="btn btn-secondary col" name="seleccionar" value="RECHAZADO">
                        Quienes rechazaron
                    </button>
                    <button type="submit" class="btn btn-secondary col" name="seleccionar" value="COMPLETADO">
                        Por subir a IMSS
                    </button>
                    <button type="submit" class="btn btn-secondary col" name="seleccionar" value="ENVIADO">
                        Subidos a IMSS
                    </button>
                    <select name="fechaSubido" class="form-select col">
                        <option value="">Selecciona una fecha</option>
                        <?php foreach($fechasDescarga as $fechaDescarga): ?>
                            <option value="<?php echo $fechaDescarga; ?>"><?php echo $fechaDescarga; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                
            </form>
        </div>
        <div class="col-sm-6" style="text-align: right;">
            <form method="post" action="<?php echo base_url('SeguroFacultativo/Imss') ?>" onsubmit="prepara()" id="formdescarga">
                <button type="submit" class="btn btn-secondary" name="descargar" value="plantilla">
                    Descargar plantilla para IMSS
                </button>
                <button type="submit" class="btn btn-secondary" name="descargar" value="rechazo">
                    Descargar reporte de rechazados
                </button>
            </form>
        </div>
    </div>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><input type="checkbox" id="seltodo" onclick="checartodo()" /></th>
                <th scope="col">Matrícula</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Nombre(s)</th>
                <th scope="col">CURP</th>
                <th scope="col">Carrera</th>
                <th scope="col">NSS</th>
                <th scope="col">Status</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">Reinicio</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($seguros as $seguro): ?>
                <tr>
                    <td>
                        <?php if($seguro->status=='COMPLETADO'||$seguro->status=='ENVIADO'): ?>
                            <input type="checkbox" value="<?php echo $seguro->id; ?>" name="<?php echo 'alumno-'.$seguro->id; ?>" class="chkSeguro" />
                        <?php endif; ?>
                    </td>
                    <th scope="row"><?php echo $seguro->id; ?></th>
                    <td><?php echo $seguro->apPaterno; ?></td>
                    <td><?php echo $seguro->apMaterno; ?></td>
                    <td><?php echo $seguro->nombre; ?></td>
                    <td><?php echo $seguro->curp; ?></td>
                    <td><?php echo $seguro->licenciatura; ?></td>
                    <td><?php echo $seguro->ssn; ?></td>
                    <td><?php 
                        switch($seguro->status) {
                            case 'ENVIADO': echo 'DESCARGADO'; break;
                            case 'OBSERVADO': echo 'CON OBSERVACIONES'; break;
                            case 'COMPLETADO':
                            case 'SOLICITADO': 
                            case 'RECHAZADO': echo $seguro->status; break;
                            case 'ESPERA':
                            case 'SOLICITAR': 
                            default: echo 'EN ESPERA'; break;
                        }?>
                    </td>
                    <td>
                        <?php if($seguro->status=='SOLICITADO'): ?>
                            <a href="<?php echo base_url('SeguroFacultativo/Ver').'/'.$seguro->id; ?>" class="btn btn-primary btn-sm">Ver / Validar</a>
                        <?php elseif($seguro->status=='RECHAZADO'): ?>
                            <?php echo $seguro->razon.' '.$seguro->razonSecundaria; ?>
                        <?php elseif($seguro->status=='ENVIADO'): ?>
                            <?php echo $seguro->fechaDescarga; ?>
                        <?php else: ?>
                            
                        <?php endif; ?>
                        
                    </td>
                    <td>
                        <form method="post" action="<?php echo base_url('SeguroFacultativo/Reiniciar'); ?>" onsubmit="return confirmaReset();">
                            <button type="submit" class="btn btn-danger btn-sm" name="alumno" value="<?php echo $seguro->id; ?>">
                                Reiniciar
                            </button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">
    function checartodo() {
        let check = document.getElementById('seltodo').checked;
        let checks = document.getElementsByClassName('chkSeguro');
        for ( let i=0; i<checks.length; i++ ) {
            checks[i].checked = check;
        }
    }

    function prepara() {
        let formDescarga = document.getElementById('formdescarga');
        let checks = document.getElementsByClassName('chkSeguro');
        let ids = [];
        for ( let i=0; i<checks.length; i++ ) {
            if ( checks[i].checked === true ) {
                ids.push = checks[i].value;
                let inputId = document.createElement("input");
                inputId.type = 'hidden';
                inputId.name = checks[i].value;
                inputId.value = checks[i].value;
                formDescarga.appendChild( inputId );
            }
        }
    }

    function confirmaReset() {
        return confirm("Reiniciar el status del seguro facultativo eliminará todo el proceso que haya hecho el estudiante o la DSEyT. \n Por favor confirme que desea continuar");
    }
</script>

<?php echo $this->endSection() ?>