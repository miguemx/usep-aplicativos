<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Notificacion extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'creador' => null,
        'destinatario' => null,
        'texto' => null,
        'hora' => null,
        'status' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'         => 'notificacion_id',
        'creador'     => 'notificacion_creador',
        'destinatario'    => 'notificacion_destinatario',
        'texto'       => 'notificacion_texto',
        'hora'     => 'notificacion_hora',
        'status'   => 'notificacion_status',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

}
