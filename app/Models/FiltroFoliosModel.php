<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroFoliosModel extends Model {
    protected $table      = 'filtro_folios';
    protected $primaryKey = 'filtrofolio_id';

    protected $returnType    = 'App\Entities\FiltroFolio';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'filtrofolio_usuario','filtrofolio_usuario_ext',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene el ultimo folio de un usuario
     * @param correo el correo para buscar al usuario
     * @param tipo el tipo de usuario (interno (U) o externo (X) )
     * @return folio el ultimo folio encontrado
     * @return null en caso de no encontrar
     */
    public function getLastUser( $correo, $tipo ) {
        if ( $tipo === 'U' ) {
            $this->where( 'filtrofolio_usuario', $correo );
        }
        else {
            $this->where( 'filtrofolio_usuario_ext', $correo );
        }
        $this->orderBy( 'created_at', 'DESC' );
        $folio = null;
        $row = $this->first();
        if ( !is_null($row) ) {
            $folio = $row->id;
        }
        return $folio;
    }
}