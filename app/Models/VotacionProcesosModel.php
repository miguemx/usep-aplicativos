<?php
namespace App\Models;
use CodeIgniter\Model;

class VotacionProcesosModel extends Model {
    protected $table      = 'votacion_procesos';
    protected $primaryKey = 'proceso_id';

    protected $returnType    = 'App\Entities\VotacionProcesos';
    protected $useSoftDeletes =true;

    protected $allowedFields = [
        'proceso_id','proceso_inicio','proceso_fin','proceso_nombre','proceso_terminado'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
    public function buscaprocesos(){
        date_default_timezone_set('America/Mexico_City');
        $fechaHora = date("Y-m-d H:i:s");
        $consulta = $this->where('proceso_terminado',0)
        ->where('proceso_inicio <=',$fechaHora)
        ->where('proceso_fin >=',$fechaHora)->findAll();
        return $consulta;
    }
    public function buscaprocesos1(){
        date_default_timezone_set('America/Mexico_City');
        $fechaHora = date("Y-m-d H:i:s");
        $consulta = $this->where('proceso_terminado',1)
        ->where('proceso_inicio <=',$fechaHora)
        ->where('proceso_fin >=',$fechaHora)->findAll();
        return $consulta;
    }
    public function buscaprocesos2(){
        date_default_timezone_set('America/Mexico_City');
        $fechaHora = date("Y-m-d H:i:s");
        $consulta = $this->where('proceso_terminado',2)
        ->where('proceso_inicio <=',$fechaHora)
        ->where('proceso_fin >=',$fechaHora)->findAll();
        return $consulta;
    }
    public function buscaprocesos3(){
        $consulta = $this->where('proceso_terminado !=',3)->findAll();
/*         echo "<pre>";
        var_dump($consulta); */
        return $consulta;
    }
    public function buscaprocesosetapas($etapa){
        $consulta = $this->where('proceso_terminado',$etapa)
        ->findAll();
        return $consulta;
    }
    public function finalizaproceso($id,$proceso){
        return $this->db
        ->table('votacion_procesos')
        ->where(["proceso_id" => $id])
        ->set($proceso)
        ->update();
    }
    public function cambiaetapaproceso($idproceso,$finalizavotantes){
        return $this->db
        ->table('votacion_procesos')
        ->where(["proceso_id" => $idproceso])
        ->set($finalizavotantes)
        ->update();
    }
    public function busca($idproceso){
        $consulta = $this->where('proceso_id',$idproceso)
        ->findAll();
        return $consulta;
    }
    public function updateproceso1($proceso,$updateproceso){
        return $this->db
        ->table('votacion_procesos')
        ->where(["proceso_id" => $proceso])
        ->set($updateproceso)
        ->update();
    }
    public function validafinprocesos($idproceso){
        $consulta = $this->where('proceso_id',$idproceso)
        ->where('proceso_terminado', 3)
        ->findAll();
        return $consulta;
    }
}