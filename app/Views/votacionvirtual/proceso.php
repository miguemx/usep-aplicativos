<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
<?php if (isset($mensajeerror)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $mensajeerror; ?>
        </div>
        <?php endif; ?>
        <?php if(isset($error)) :?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        <?php endif; ?>   

<?php if (isset($mensaje)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensaje; ?>
    </div>
<?php endif; ?>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/procesos'); ?>">
        <h1>Registro de procesos</h1>
        <br>
        <div class="procesonombre">
            <label for="nombreproceso" class="form-label">Nombre de proceso</label>
            <input type="text" class="form-control" style="justify-content:center" id="nombreproceso" name="nombreproceso" required>
        </div>
        <div class="fechainicio">
            <label for="fechainicio" class="form-label">Fecha y hora de inicio</label>
            <br>
            <input type="date" id="fechainicio" name="fechainicio" required>
            <input type="time" id="fechainiciotime" name="fechainiciotime" required>
        </div>
        <div class="fechafinal">
            <label for="fechafinal" class="form-label">Fecha y hora final</label>
            <br>
            <input type="date" id="fechafinal" name="fechafinal" required>
            <input type="time" id="fechafintime" name="fechafintime" required>
        </div>
        <div class="guardar" style="display:flex; justify-content:center">
            <button type="submit" class="btn btn-secondary">Guardar</button>
        </div>
    </form>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/candidatos'); ?>">
        <div class="estadisticas" style="display: table-cell;  text-align: center;">
            <button type="submit" name='estadisticas' class="btn btn-secondary">Ir al registro de candidatos</button>
        </div>
    </form>
    <br>
    <?php if (isset($mensajefinalizado)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $mensajefinalizado; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($errorfinalizado)) : ?>
        <div class="alert alert-danger" role="alert">
        <?php echo $errorfinalizado; ?>
    </div>
    <?php endif; ?>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/finproceso'); ?>">
        <h1>Finaliza procesos</h1>
        <div class="idproceso">
            <label for="selectproceso" class="form-label">Procesos</label>
            <br>
            <select id="selectproceso " class="custom-select" style="width: 80%;" name="selectproceso"  required>
                <option value="">Selecciona procesos</option>
                <?php foreach ($procesos as $arregloprocesos) : ?>
                    <option value = <?php echo $arregloprocesos->id ?>> <?php
                        echo $arregloprocesos->nombre
                    ?></option>
                <?php endforeach; ?>
            </select>
        </div>
            <div class="guardar" style="display:flex; justify-content:center">
                <button type="submit" class="btn btn-secondary">Finalizar</button>
            </div>
    </form>

</div>
<?php echo $this->endSection() ?>