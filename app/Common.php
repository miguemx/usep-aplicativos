<?php

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */

use PhpParser\Node\Stmt\Else_;

/**
 * genera el código HTML para dibujar un input de cualquier tipo.
 * hace uso de las funciones auxiliares que se definen a continuacion de esta
 * @param campo el objeto de tipo App\Entities\AspirantesCampo
 * @param tipoCampo el tipo App\Entities\AspirantesTipoCampo
 * @param value (opcional) mel valor que tendrá por defecto el input
 * @param disabled (opcional) indicar si se desea que el control esté deshabilitado o no
 * @param status (opcional) una cadena con los niveles de log para informacion y clases css
 * @param comment (opcional) un comentario que se desee mostrar al usuario
 * @param props (opcional) recibe un arreglo con otras opciones para visualizacion
 */
function myInput($campo, $tipoCampo, $value = '', $disabled = false, $status = '', $comment = '', $props = null)
{
    $input = '';
    $mandatory = ($campo->obligatorio == '1') ? true : false;
    switch ($tipoCampo->clasificacion) {
        case 'BASIC':
            $type = 'text';
            switch ($tipoCampo->nombre) {
                case 'NUMERO':
                    $type = 'number';
                    break;
                case 'FECHA':
                    $type = 'date';
                    break;
                case 'CORREO':
                    $type = 'email';
                    break;
                default:
                    break;
            }
            $input = myInputBasic($type, $campo->nombre, $campo->campo, $value, $campo->descripcion, $mandatory, $disabled);
            break;
        case 'ARCHIVO':
            $input = myInputFile($campo->nombre, $campo->campo, $value, $campo->descripcion, $mandatory, $disabled, $props);
            break;
        case 'COLECCION':
            $options = explode(',', $tipoCampo->valores);
            $input = myInputoptions($options, $campo->nombre, $campo->campo, $value, $campo->descripcion, $mandatory, $disabled);
            break;
        case 'CATALOGO':
            switch ($tipoCampo->valores) {
                case 'cat_ccts':
                    $input = myCct($campo->nombre, $campo->campo, $value, $campo->descripcion, $mandatory, $disabled);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return $input;
}

/**
 * genera el codigo HTML para renderizar un input de tipo básico
 * @param type el tipo de dato a esperar de acuerdo al estandar de bootstrap
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myInputBasic($type, $label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $cssClass = '', $css = '')
{
    $data = [
        'type'        => $type,
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'disabled'    => $disabled,
        'cssClass'    => $cssClass,
        'css'         => $css,
    ];

    return view('componentes/inputbasic', $data);
}

/**
 * genera el codigo HTML para renderizar un input de tipo básico
 * @param type el tipo de dato a esperar de acuerdo al estandar de bootstrap
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param disabled 
 * @param props (opcional) indica un comentario que se desee mostrar al usuario
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myInputFile($label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $props = null, $cssClass = '', $css = '')
{
    $data = [
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'disabled'    => $disabled,
        'props'       => $props,
        'cssClass'    => $cssClass,
        'css'         => $css,
    ];

    return view('componentes/inputfile', $data);
}

/**
 * genera el codigo HTML para renderizar un input de tipo básico
 * @param options un array con llave de valor y valor el texto de la opción
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myInputOptions($options, $label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $cssClass = '', $css = '')
{
    $data = [
        'options'     => $options,
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'disabled'    => $disabled,
        'cssClass'    => $cssClass,
        'css'         => $css,
    ];
    if (count($options) < 4) {
        return view('componentes/inputradio', $data);
    } else {
        return view('componentes/inputselect', $data);
    }
}

/**
 * genera el codigo HTML para renderizar un buscador de escuelas
 * @param options un array con llave de valor y valor el texto de la opción
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myCct($label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $cssClass = '', $css = '')
{
    $cctsModel = new \App\Models\CCTSModel();
    $estados = $cctsModel->getEstados();
    $escuela = '';
    $cct = '';
    if ($value) {
        $find = $cctsModel->find($value);
        if (!is_null($find)) {
            $escuela = $find->id . ' - ' . $find->nombre;
            $cct = $find->id;
        }
    }
    $data = [
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'cssClass'    => $cssClass,
        'css'         => $css,
        'estados'     => $estados,
        'escuela'     => $escuela,
        'disabled'    => $disabled,
        'cct'         => $cct,
    ];
    return view('componentes/escuela', $data);
}

/**
 * genera el código HTML para dibujar un input de cualquier tipo.
 * hace uso de las funciones auxiliares que se definen a continuacion de esta
 * @param campo el objeto de tipo App\Entities\AspirantesCampo
 * @param tipoCampo el tipo App\Entities\AspirantesTipoCampo
 * @param value (opcional) mel valor que tendrá por defecto el input
 * @param disabled (opcional) indicar si se desea que el control esté deshabilitado o no
 * @param status (opcional) una cadena con los niveles de log para informacion y clases css
 * @param comment (opcional) un comentario que se desee mostrar al usuario
 * @param props (opcional) recibe un arreglo con otras opciones para visualizacion
 */
function myInputEvaluacion($campo, $tipoCampo, $value = '', $disabled = false, $status = '', $comment = '', $props = null)
{
    $input = '';
    $mandatory = ($campo->obligatorio == '1') ? true : false;
    switch ($tipoCampo->clasificacion) {
        case 'BASIC':
            $type = 'text';
            switch ($tipoCampo->nombre) {
                case 'NUMERO':
                    $type = 'number';
                    break;
                case 'FECHA':
                    $type = 'date';
                    break;
                case 'CORREO':
                    $type = 'email';
                    break;
                default:
                    break;
            }
            $input = myInputBasicEvaluacion($type, $campo->nombre, $campo->id, $value, $campo->descripcion, $mandatory, $disabled);
            break;
        case 'COLECCION':
            $options = explode(',', $tipoCampo->valores);
            $input = myInputoptionsEvaluacion($options, $campo->titulo, $campo->id, $value, $campo->descripcion, $mandatory, $disabled, null, null, (($tipoCampo->nombre == 'RANGO') ? true : null));
            break;
        default:
            break;
    }
    return $input;
}

/**
 * genera el codigo HTML para renderizar un input de tipo básico
 * @param options un array con llave de valor y valor el texto de la opción
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myInputoptionsEvaluacion($options, $label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $cssClass = '', $css = '', $rango = false)
{
    $data = [
        'options'     => $options,
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'disabled'    => $disabled,
        'cssClass'    => $cssClass,
        'css'         => $css,
    ];
    if ($rango) {
        return view('componentes/evaluaciones/inputradio', $data);
    } 
}

/**
 * genera el codigo HTML para renderizar un input de tipo básico
 * @param type el tipo de dato a esperar de acuerdo al estandar de bootstrap
 * @param label la etiqueta para el titulo del campo
 * @param name el atributo de NAME para el campo en el formulario; mismo para usar en el ID
 * @param value (opcional) el valor inicial del campo
 * @param description ( opcional) el texto descriptivo del campo
 * @param mandatory (opcional) la bandera que indica si es obligatorio
 * @param cssClass (opcional) si se requiere agregar una clase CSS más
 * @param css (opcional) si se agrega codigo CSS adicional inline
 */
function myInputBasicEvaluacion($type, $label, $name, $value = '', $description = '', $mandatory = false, $disabled = false, $cssClass = '', $css = '')
{
    // echo " tipo = $type <br> label: $label <br> nombre = $name <br>";
    $data = [
        'type'        => $type,
        'label'       => $label,
        'name'        => $name,
        'value'       => $value,
        'description' => $description,
        'mandatory'   => $mandatory,
        'disabled'    => $disabled,
        'cssClass'    => $cssClass,
        'css'         => $css,
    ];

    return view('componentes/evaluaciones/inputbasic', $data);
}
