<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    .labels {
        width: 20%;
    }
</style>
<div class="container my-sm-10">
    <h1 style="display:flex; justify-content:center">Datos estadísticos</h1>
    <div>
        <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/estadisticatipoaspirante'); ?>">
            <h3 style="text-align: center;" class="form-label">TIPO DE ASPIRANTE</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <button type="submit" name='ingreso' value='ingreso' class="btn btn-secondary" style="left: 100px">Nuevo ingreso</button>
                <button type="submit" name='revalidacion' value='revalida' class="btn btn-secondary" style="left: 100px">Revalidación</button>
            </div>
        </form>
    </div>
    <div>
        <br>
        <br>
        <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/estatusregistro'); ?>">
            <h3 for="selectproceso" style="text-align: center;" class="form-label">ESTATUS DE REGISTRO</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <button type="submit" name='status' value='Validacion de correo' class="btn btn-secondary" style="left: 100px">En validación de correo</button>
                <button type="submit" name='status' value='Captura de informacion' class="btn btn-secondary" style="left: 100px">En captura de información</button>
                <button type="submit" name='status' value='Carga de documentos' class="btn btn-secondary" style="left: 100px">En carga de documentos</button>
                <button type="submit" name='status' value='Revisión documental' class="btn btn-secondary" style="left: 100px">En revision documental</button>
            </div>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <button type="submit" name='status' value='Observaciones' class="btn btn-secondary" style="left: 100px">Con observaciones</button>
                <button type="submit" name='status' value='Registro validado' class="btn btn-secondary" style="left: 100px">Registro validado</button>
                <button type="submit" name='status' value='Registro declinado' class="btn btn-secondary" style="left: 100px">Registro declinado</button>
                <button type="submit" name='status' value='total' class="btn btn-secondary" style="left: 100px">total</button>
            </div>
        </form>
    </div>
    <div>
        <br>
        <br>
        <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/'); ?>">
            <h3 for="selectproceso" style="text-align: center;" class="form-label">REGIONES</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Anexo</button>
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Otros estados</button>
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Extranjeros</button>
            </div>
        </form>
    </div>
    <div>
        <br>
        <br>
        <div class="row g-3">
            <h3 for="selectproceso" style="text-align: center;" class="form-label">METROPOLI-INTERIOR DEL ESTADO</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <form method="post" action="<?php echo base_url('EstadisticasAdmision/AspirantesMunicipios'); ?>">
                    <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Contador de aspirantes por municipios</button>
                </form>
            </div>
        </div>
    </div>
    <div>
        <br>
        <br>
        <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/'); ?>">
            <h3 for="selectproceso" style="text-align: center;" class="form-label">PROMEDIO</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Número de aspirantes con promedios de 6 a 10</button>
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Media, mediana y moda</button>
            </div>
        </form>
    </div>
    <div>
        <br>
        <br>
        <div class="row g-3">
            <h3 for="selectproceso" style="text-align: center;" class="form-label">ASPIRANTES CON DISCAPACIDADES</h3>
            <div class="proceso" style="display: table-cell; text-align: center;">
            <form  method="post" action="<?php echo base_url('EstadisticasAdmision/AspirantesDiscapacidades'); ?>">
                <button type="submit" name='proceso' class="btn btn-secondary" style="left: 100px">Número de aspirantes con discapacidades</button>
            </form>
                
            </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>