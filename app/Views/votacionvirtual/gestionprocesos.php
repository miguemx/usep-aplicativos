<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<div class="container my-sm-5">
    <?php if (isset($actualizacorrecto)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $actualizacorrecto; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($noactualiza)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $noactualiza; ?>
        </div>
    <?php endif; ?>
    <h1 style="display:flex; justify-content:center">Gestion de procesos</h1>
    <br>
    <form class="row g-3" method="post" action="<?php echo base_url('votaciones/gestionprocesos'); ?>">
        <div>
            <h2>Busqueda por etapas de procesos</h2>
            <br>
                <div class="row" style="padding-top: 10px;">
                <div class="mb-1 row col-sm-2" >
                        <label for="correocandidato" class="form-label">Procesos sin etapas</label>
                        <div class="col-sm-10" style= 'text-align:"center"'>
                            <input type='radio' id="candidato" name="busquedaradio" value="0" onchange="this.form.submit();" onclick="muestratabla(this.id)"<?php if($busquedaradio == "0") echo 'checked'; ?>/>
                        </div>
                    </div>
                    <div class="mb-1 row col-sm-3" >
                        <label for="correocandidato" class="form-label">Registro candidatos finalizado</label>
                        <div class="col-sm-10" style= 'text-align:"center"'>
                            <input type='radio' id="candidato" name="busquedaradio" value="1" onchange="this.form.submit();" onclick="muestratabla(this.id)"<?php if($busquedaradio == 1) echo 'checked'; ?>/>
                        </div>
                    </div>
                    <div class="mb-1 row col-sm-3">
                        <label for="nombrecandidato" class="form-label">Registro padrón finalizado</label>
                        <div class="col-sm-10">
                            <input type='radio' id="padron" name="busquedaradio" onclick="muestratabla(this.id)" value="2" onchange="this.form.submit();" <?php if($busquedaradio == 2) echo 'checked'; ?>/>
                        </div>
                    </div>
                    <div class="mb-1 row col-sm-2">
                        <label for="nombrecandidato" class="form-label">Procesos finalizados</label>
                        <div class="col-sm-10">
                            <input type='radio' id="proceso" name="busquedaradio" onclick="muestratabla(this.id)" value="3" onchange="this.form.submit();" <?php if($busquedaradio == 3) echo 'checked'; ?>/>
                        </div>
                    </div>
                    <div class="mb-1 row col-sm-3">
                        <label for="nombrecandidato" class="form-label">Todos</label>
                        <div class="col-sm-10">
                            <input type='radio' id="todos" name="busquedaradio" onclick="muestratabla(this.id)" value="4" onchange="this.form.submit();" <?php if($busquedaradio == 4) echo 'checked'; ?>/>
                        </div>
                    </div>
                </div>
        </div>
        <div style="width: 400px;">
            <h2>Procesos</h2>
            <ul>
                <?php $contador = 0;
                foreach ($procesos as $proceso) : ?>
                <?php $contador = $contador + 1; ?>
                    <li>
                        <button style="width: 200px;" type="submit" name="btnidproceso" value='<?php echo $proceso->id ?>' class="btn btn-secondary btn-sm">
                            <?php echo $proceso->nombre; ?>
                        </button>
                    </li>
                    <br>
                <?php endforeach; ?>
            </ul>
            </div>
        <div style="width: 500px; " >
            <div class="datosproceso">
                <label for="correoproceso" class="form-label">Nombre</label>
                <input type="text" disabled class="form-control" style="justify-content:center" id="nombreproceso" name="nombreproceso" value="<?php echo $nombreproceso; ?>">
            </div>
            <div class="datosproceso">
                <label for="correoproceso" class="form-label">inicio de proceso</label>
                <br>
                <input type="date" disabled id="fechainicio" name="fechainicio" value="<?php echo $fechainicio; ?>">
                <input type="time" disabled id="fechainiciotime" name="fechainiciotime" value="<?php echo $fechainiciotime; ?>">
            </div>
            <div class="datosproceso">
                <label for="correoproceso" class="form-label">fin de proceso</label>
                <br>
                <input type="date" disabled id="fechafinal" name="fechafinal" value="<?php echo $fechafinal; ?>">
                <input type="time" disabled id="fechafintime" name="fechafintime" value="<?php echo $fechafintime; ?>">
            </div>
            <div class="datosproceso">
                <label for="correoproceso" class="form-label">Estatus de proceso</label>
                <select class="custom-select" disabled aria-label="Default select example" name="estatus" id="estatus" >
					<option value="">Selecciona una opción</option>
					<option value="0" <?php if ($estatus == "0") echo 'selected="selected"' ?>>Sin etapas finalizadas</option>
					<option value="1" <?php if ($estatus == "1") echo 'selected="selected"' ?>>Etapa de candidatos finalizada</option>
					<option value="2" <?php if ($estatus == "2") echo 'selected="selected"' ?>>Etapa de padrón finalizada</option>
					<option value="3" <?php if ($estatus == "3") echo 'selected="selected"' ?>>Proceso finalizado</option>
				</select>
            </div>
            <div class="estadisticas" style="text-align: center;">
                <br>
                <br>
                <input name="idproceso2" type="hidden" value="<?php echo $idproceso2; ?>" />
                <button type="submit" disabled name='guardar' id='guardar' value='1' class="btn btn-secondary">Guardar cambios</button>
            </div>
        </div>
        <div style="width: 300px; text-align: center;" >
            <br>
            <br>
            <br>
            <h5 for="correoproceso" class="form-label">Editar proceso</h5>
            <input type="checkbox" id='checkhabilita' onclick="muestracampos(this.id)">
        </div>
    </form>
</div>
<script>
    function muestracampos(id){
        let checkid = document.getElementById(id);
        if ( checkid.checked === true ) {
            document.getElementById("nombreproceso").disabled = false;
			document.getElementById("fechainicio").disabled = false;
			document.getElementById("fechainiciotime").disabled = false;
			document.getElementById("fechafinal").disabled = false;
			document.getElementById("fechafintime").disabled = false;
			document.getElementById("estatus").disabled = false;
			document.getElementById("guardar").disabled = false;
        } 
        if(checkid.checked === false){
            document.getElementById("nombreproceso").disabled = true;
			document.getElementById("fechainicio").disabled = true;
			document.getElementById("fechainiciotime").disabled = true;
			document.getElementById("fechafinal").disabled = true;
			document.getElementById("fechafintime").disabled = true;
			document.getElementById("estatus").disabled = true;
			document.getElementById("guardar").disabled = true;
        } 
    }
</script>
<?php echo $this->endSection() ?>