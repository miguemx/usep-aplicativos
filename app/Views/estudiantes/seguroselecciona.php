<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>
        <h2>Seguro Facultativo</h2>
        <hr />
    </div>
    <?php if(isset($error)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
    <form method="post" action="<?php echo base_url('Estudiantes/Seguro') ?>" enctype="multipart/form-data" onsubmit="return confirma()">
        <div class="row">
            <div class="col-sm-6">
                <div class="card alert alert-info">
                    <div class="card-body" style="text-align: center;">
                        <label for="si">Deseo registrarme para que la USEP me brinde el servicio de seguro facultativo</label><br />
                        <input type="radio" name="seguro" id="si" value="1" />
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card alert alert-danger">
                    <div class="card-body" style="text-align: center;">
                        <label for="no">Renuncio expresamente a que la USEP me brinde el seguro facultativo</label><br />
                        <input type="radio" name="seguro" id="no" value="0" />
                        <br />
                        <div style="text-align: left;">
                            <label>Motivo</label>
                            <select name="razon" class="form-control">
                                <option value="Estoy asegurado por parte de mis padres">Estoy asegurado por parte de mis padres</option>
                                <option value="Me encuentro afiliado por parte de mi trabajo">Me encuentro afiliado por parte de mi trabajo</option>
                                <option value="Estoy asegurado por parte de mi cónyuge">Estoy asegurado por parte de mi cónyuge </option>
                                <option value="OTRA">Otro motivo</option>
                            </select>

                            <label>Especifique</label>
                            <input type="text" name="otrarazon" class="form-control" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div>&nbsp;</div>
        <div style="text-align: center;">
            <button class="btn btn-secondary" type="submit" name="gf" value="seleccion">
                Continuar
            </button>
        </div>
    </form>

    <script type="text/javascript">
    function confirma() {
        if ( document.getElementById('no').checked == true ) {
            return confirm('¿Estás seguro que deseas renunciar al beneficio del seguro facultativo?');
        }
        return true;
    }
    </script>
   
</div>

<?php echo $this->endSection() ?>