<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>
<style>
    body {
        padding: 40px 5px;
        font-family: 'Open Sans', sans-serif;
        background-color: #dcdcdc;
        height: 100vh;
        width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
        background-size: 794px 1100px;
        background-repeat: no-repeat;
        background-position: left;

       /* filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/

    }

    .QR {
        display: flex;
        justify-content: center;

    }

    .card-qr {
        max-width: 40rem;
        border-radius: 20px;
        background-color: whitesmoke;
    }

    .contenedor-qr {
        border-radius: 50px;
        max-width: 40rem;
        background-color: #dcdcdc;
    }

    .logo-cabecera {
        background-image: url(<?php echo base_url('img/Logo_USEP_negativo.png') ?>);
        background-color: #841138;
        color: antiquewhite;
        text-align: center;
        border-top-left-radius: 20px !important;
        border-top-right-radius: 20px !important;
        padding: 20px;
        min-height: 100px;
        background-repeat: no-repeat;
        background-size: 200px;
        background-position: center;
        position: relative;
    }

    .correo {
        min-height: 100px;
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="card">
        <div class="card-header logo-cabecera">
        </div>
        <br>
        <div class="card-body">
                <form class="contenido" action="<?php echo base_url('FiltroSanitario/registrar'); ?>" method="post" name="login" id="contenedor1">
					<div class="form-group text-left">
						<label for="correo">Correo:</label>
						<input type="text" name="correo" id="correo" class="form-control" value="<?php echo $correo; ?>" placeholder="ejemplo@algo.com" required>
						</label>
					</div>
					<div class="form-group text-left required">
						<label for="nombre">Nombre:</label>
						<input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>" placeholder=" Nombre" required>
						</label>
					</div>
					<div class="form-group text-left required">
						<label for="nodependenciambre">Dependencia:</label>
						<input class="form-control" type="text" name="dependencia" id="dependencia" value="<?php echo $dependencia; ?>" placeholder=" DEPENDENCIA" required>
						</label>
					</div>


					<div class="form-group text-left required">
						<label for="categoria">Categoria: </label>
						<select class="custom-select" aria-label="Default select example" name="categoria" id="categoria" required>
							<option value="">Selecciona una opción</option>
							<option value="Servicios profesionales" <?php if ($categoria == "Servicios profesionales") echo 'selected="selected"' ?>>Servicios profesionales</option>
							<option value="Visitantes" <?php if ($categoria == "Visitantes") echo 'selected="selected"' ?>>Visitantes</option>
							<option value="Proveedor" <?php if ($categoria == "Proveedor") echo 'selected="selected"' ?>>Proveedor</option>
						</select>

					</DIV>
					<div class="form-group text-left required">
						<label for="edad">Edad: </label>
						<select class="custom-select" aria-label="Default select example" name="edad" id="edad" required>
							<option value="">Selecciona una opción</option>
							<option value="18 a 30" <?php if ($edad == "18 a 30") echo 'selected="selected"' ?>>18 a 30</option>
							<option value="31 a 40" <?php if ($edad == "31 a 40") echo 'selected="selected"' ?>>31 a 40</option>
							<option value="41 a 50" <?php if ($edad == "41 a 50") echo 'selected="selected"' ?>>41 a 50</option>
							<option value="61 y más" <?php if ($edad == "61 y más") echo 'selected="selected"' ?>>61 y más</option>
						</select>
					</div>
					<div class="form-group text-left required">
						<label for="sexo">Sexo: </label>
						<select class="custom-select" aria-label="Default select example" name="sexo" required>
							<option value="">Selecciona una opción</option>
							<option value="M" <?php if ($sexo == "M") echo 'selected="selected"' ?>>Femenino</option>
							<option value="H" <?php if ($sexo == "H") echo 'selected="selected"' ?>>Masculino</option>
						</select>
					</div>

                    <div class="col-12 text-center">
                        <a href="<?php echo base_url('FiltroSanitario'); ?>" class="btn btn-secondary">Cancelar</a>
                        <button type="submit" class="btn btn-secondary">Registrar</button>
                    </div>
				</form>
        </div>

    </div>
</div>

<?php echo $this->endSection() ?>