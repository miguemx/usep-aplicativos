<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<style>
    datalist {
        width: 50000px;
        height: 80px;
    }
</style>
<?php if ($mensaje) : ?>
    <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endif; ?>
<div class="container ">
    <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2 ">
        <form action="<?php echo base_url('ProgramacionAcademica/MapaAulas'); ?>" method="post" id="buscador">

            <label for="aulas_select" class="form-label">Seleccione el Aula</label>
            <select class="form-select" name="aulas_select" id="aulas_select" onchange="buscar()">
                <option selected>Seleccione una opcion</option>
                <?php foreach ($aulas as $aula) : ?>
                    <option value="<?php echo $aula->id_aula ?>"><?php echo $aula->nombre_aula ?></option>
                <?php endforeach; ?>
            </select>
            <div class="text-center mt-3">
                <button type="button" class="btn btn-secondary">Buscar</button>
            </div>
        </form>
    </div>

</div>
<?php if ($horario) : ?>
    <div class="container-fluid">
        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
            <div class="text-center form-group mt-4">
                <h4><?php echo $aula_nombre  ?></h4>
            </div>
            <table class="table table-borderless table-hover  mt-3 text-center">
                <thead class="thead-default">
                    <tr>
                        <th scope="row">HORA</th>
                        <th>LUNES</th>
                        <th>MARTES</th>
                        <th>MIERCOLES</th>
                        <th>JUEVES</th>
                        <th>VIERNES</th>
                        <th>SABADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hora = 7;
                    ?>

                    <?php while ($hora < 24) : ?>

                        <tr>
                            <td scope="row"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                            <?php foreach ($dias as $dia) : ?>
                                <?php $bandera_coincidencia = false; ?>
                                <?php foreach ($grupos as $grupo) : ?>
                                    <?php if ($grupo->dia == $dia) : ?>
                                        <?php if ($grupo->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                            <td><?php echo  $grupo->ma_nombre . '<br>' . (($grupo->docente == 1) ? 'Por asignar' : ($grupo->em_paterno . ' ' . $grupo->em_materno . ' ' . $grupo->em_nombre)) . "<br>" . $grupo->seccion ?></td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (!$bandera_coincidencia) : ?>
                                    <td>
                                        <form action="<?php echo base_url('ProgramacionAcademica/AddGrupo'); ?>" method="post">
                                            <!-- <input type="hidden" name="posicion" value="<?php // echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')).'-'.$aula_nombre  
                                                                                                ?>"> -->
                                            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#select-clase"><i class="bi bi-plus-circle" style="font-size: 2rem; color: grey;"></i></button>
                                        </form>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                        <?php $hora++; ?>
                    <?php endwhile; ?>


                </tbody>
            </table>

        </div>
    </div>
    <div class="modal fade" id="select-clase" tabindex="-1" aria-labelledby="select-claseLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="select-claseLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- seccion para la lista de grupos -->
                    <div class="row">
                        <div class="col">

                            <label for="exampleDataList" class="form-label">Datalist example</label>
                            <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
                            <datalist id="datalistOptions">
                                <!-- <option value="San Francisco"> -->
                                <?php foreach ($grupos_periodo as $lista) : ?>
                                    <?php foreach ($materias as $materia) : ?>
                                        <?php if ($lista->materia == $materia->id) ?>
                                        <option value="<?php echo $lista->clave . ' ' . $materia->nombre   ?>"><?php echo $lista->id ?></option>
                                        <?php ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </datalist>
                        </div>
                    </div>
                    <!-- ------------------fin ----------------->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary">Asignar hora</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    function buscar() {
        document.getElementById('buscador').submit();
    }
</script>
<script>
    var select_box_element = document.querySelector('#select_box');

    dselect(select_box_element, {
        search: true
    });
</script>
<?php echo $this->endSection() ?>