<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .error {
        border: red 2px solid;
    }
</style>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <?php if ($creacion) : ?>
            <form action="<?php echo base_url('EvaluacionDocenteController/CrearEvaluacion/') ?>" method="get" id="form_crear">
                <div class="mb-3">
                    <label for="titulo_evaluacion" class="form-label">Titulo de la Evaluacion</label>
                    <input type="text" class="form-control" name="titulo_evaluacion" id="titulo_evaluacion" aria-describedby="help_titulo" placeholder="Evaluacion de desempeño docente...">
                    <small id="help_titulo" class="form-text text-muted">Indique el nombre de la evaluacion</small>
                </div>
                <div class="mb-3">
                    <label for="select_periodo" class="form-label">Periodo</label>
                    <select class="form-select " name="select_periodo" id="select_periodo">
                        <option value="false">Seleccione un periodo disponible</option>
                        <?php foreach ($periodos as $periodo) : ?>
                            <option value="<?php echo $periodo->id ?>"><?php echo $periodo->id ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="select_tipo_evaluacion" class="form-label">Tipo de evaluacion</label>
                    <select class="form-select " name="select_tipo_evaluacion" id="select_tipo_evaluacion">
                        <?php foreach ($tipos_evaluacion as $tipo) : ?>
                            <option value="<?php echo $tipo->id ?>"><?php echo $tipo->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="selct_rol" class="form-label">A quien va dirigida</label>
                    <select class="form-select " name="selct_rol" id="selct_rol">
                        <option value="false">Seleccione un destinatario disponible</option>
                        <?php foreach ($roles as $rol) : ?>
                            <option value="<?php echo $rol->id ?>"><?php echo $rol->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="verificar('form_crear','¿Seguro que desea generar una nueva evaluación?','Se generará una nueva evaluacion')">Guardar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Menu') ?>" role="button">Cancelar</a>
                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        <?php else : ?>
            <form action="<?php echo base_url('EvaluacionDocenteController/EditarEvaluacion/' . $evaluacion->id) ?>" method="get" id="form_editar">
                <div class="mb-3">
                    <label for="titulo_evaluacion_edicion" class="form-label">Titulo de la Evaluacion</label>
                    <input type="text" class="form-control" name="titulo_evaluacion_edicion" id="titulo_evaluacion_edicion" aria-describedby="help_titulo" placeholder="Evaluacion de desempeño docente..." value="<?php echo $evaluacion->titulo ?> ">
                    <small id="help_titulo" class="form-text text-muted">Indique el nombre de la evaluacion</small>
                </div>
                <div class="mb-3">
                    <label for="select_periodo_edicion" class="form-label">Periodo</label>
                    <select class="form-select " name="select_periodo_edicion" id="select_periodo_edicion">
                        <option value="false">Seleccione un periodo disponible</option>
                        <?php foreach ($periodos as $periodo) : ?>
                            <option value="<?php echo $periodo->id ?>" <?php echo (($periodo->id == $evaluacion->periodo) ? 'selected' : '') ?>><?php echo $periodo->id ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="select_tipo_evaluacion_edicion" class="form-label">Tipo de evaluacion</label>
                    <select class="form-select " name="select_tipo_evaluacion_edicion" id="select_tipo_evaluacion_edicion">
                        <?php foreach ($tipos_evaluacion as $tipo) : ?>
                            <option value="<?php echo $tipo->id ?>" <?php echo (($tipo->id == $evaluacion->tipo) ? 'selected' : '') ?>><?php echo $tipo->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="selct_rol_edicion" class="form-label">A quien va dirigida</label>
                    <select class="form-select " name="selct_rol_edicion" id="selct_rol_edicion">
                        <option value="false">Seleccione un destinatario disponible</option>
                        <?php foreach ($roles as $rol) : ?>
                            <option value="<?php echo $rol->id ?>"<?php echo (($rol->id == $evaluacion->rol) ? 'selected' : '') ?>><?php echo $rol->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="mensaje('form_editar','¿Seguro que quiere editar la evaluación?','Se editará la evaluacion')">Editar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Menu') ?>" role="button">Cancelar</a>
                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        <?php endif; ?>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function verificar(submit, texto, confirmacion) {
        var periodo = document.getElementById('select_periodo');
        var rol = document.getElementById('selct_rol');
        var titulo = document.getElementById('titulo_evaluacion');
        var bandera = false;
        if (periodo.value == 'false') {
            periodo.classList.add('error');
            bandera = true

        } else {
            periodo.classList.remove('error');
        }
        if (rol.value == 'false') {
            rol.classList.add('error');
            bandera = true

        } else {
            rol.classList.remove('error');
        }
        if (titulo.value == '') {
            titulo.classList.add('error');
            bandera = true

        } else {
            titulo.classList.remove('error');
        }

        if (!bandera) {
            mensaje(submit, texto, confirmacion);
        } else {
            Swal.fire({
                'title': 'Error',
                'text': 'Verifica los elementos marcados en rojo y vuelve a intentarlo',
                'icon': 'info',
                confirmButtonColor: '#840f31'
            });
        }

    }

    function mensaje(submit, texto, confirmacion) {
        Swal.fire({
            'title': 'Confirmación',
            'html': texto,
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': confirmacion,
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                document.getElementById(submit).submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>