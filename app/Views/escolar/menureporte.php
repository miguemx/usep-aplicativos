<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg">
        <form action="<?php echo base_url('Escolar/DescargarReporte') ?>" method="post">
            <div class="row">
                <div class="col mt-3">
                    <label for="periodo" class="form-label">Periodo</label>
                    <select class="form-select" name="periodo" id="periodo">
                        <option value="0">Seleccione un periodo</option>
                        <?php foreach ($periodos as $lista_periodo) : ?>
                            <option value="<?php echo $lista_periodo->id ?>"><?php echo $lista_periodo->id ?></option>
                        <?php endforeach; ?>
                    </select>
                    <small id=" text_periodo"></small>
                </div>
                <div class="col mt-3">

                    <label for="seccion" class="form-label">Seccion: </label>
                    <input type="text" class="form-control" name="seccion" id="seccion" aria-describedby="seccion_texto" placeholder="MED-101">

                </div>

                <div class="col mt-3">
                    <label for="carrera">Carrera de las Secciones</label>
                    <select class="form-select" name="carrera" id="carrera">
                        <option value="0">Seleccione una carrera</option>
                        <?php foreach ($carreras as $carreara) : ?>
                            <option value="<?php echo $carreara->nombre ?>"><?php echo  $carreara->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row m-3 text-center">
                <div class="col text-center">
                    <button type="submit" class="btn btn-secondary">Descargar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php echo $this->endSection() ?>