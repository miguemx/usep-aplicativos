<?php

namespace App\Controllers;

use App\Models\FiltroSanitarioModel;
use App\Models\EmpleadosModel;
use App\Models\UsuariosModel;
use App\Models\InsertFiltroModel;
use \App\Entities\InsertarFiltroSanitario;
use \App\Entities\FiltroUsuariosExternos;
use App\Models\FiltroEmpleadosModel;
use App\Models\FiltroEstudiantesModel;
use App\Models\FiltroExternosModel;
use App\Models\FiltroUsuariosExternosModel;
use App\Models\FiltroReportesModel;
use App\Models\FiltroFoliosModel;
use App\Models\FiltroSanitarioPreguntasModel;
use App\Models\FiltroRespuestasModel;
use \App\Entities\FiltroReportes;
use \App\Entities\FiltroUsuarioExterno;
use \App\Entities\FiltroFolio;
use \App\Entities\FiltroRespuesta;
use \Endroid\QrCode\Builder\Builder;
use \Endroid\QrCode\Encoding\Encoding;
use \Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use \Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use \Endroid\QrCode\Label\Font\NotoSans;
use \Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use \Endroid\QrCode\Writer\PngWriter;


/**
 * 
 */
class FiltroSanitario extends BaseController
{
  //protected $varCorreo=0;
  public $varCorreo = 0;
  private $usuariosModel;
  private $preguntasModel;
  private $filtroFoliosModel;
  private $filtroRespuestasModel;

  /**
   * 
   */
  public function __construct() {
    $this->usuariosModel = new UsuariosModel();
    $this->preguntasModel = new FiltroSanitarioPreguntasModel();
    $this->filtroFoliosModel = new FiltroFoliosModel();
    $this->filtroRespuestasModel = new FiltroRespuestasModel();
  }
  
  /**
   * despliega la pantalla inicial para el filtro sanitario
   */
  public function index()
  {
    helper('cookie');    
    $user = $this->sesion->get('useremail');
    if ( is_null( $user ) ) {
      $user = get_cookie( 'mailfiltro' );
    }
    $data['correo'] = ( is_null($user) ) ? '': $user;
    return view("filtro_sanitario/inicio", $data);
  }

  /**
   * renderiza el cuestionario del filtro sanitario
   */
  public function cuestionario($tipo, $usr) {
    helper('cookie');    
    delete_cookie( 'mailfiltro' );
    $tipo = $this->descifrar($tipo);
    $usr = $this->descifrar($usr);
    set_cookie( 'mailfiltro', $usr, 60*60*24*10 );
    $preguntas = $this->preguntasModel->findAll();
    $data['preguntas'] = $preguntas;
    $data['correo'] = $usr;
    $data['tipo'] = $tipo;
    return view('filtro_sanitario/preguntas', $data);
  }

  /**
   * busca un usuario del filtro mediante su correo
   */
  public function usuario() {
    $usuariosFiltro = new FiltroUsuariosExternosModel();
    helper('cookie');
    delete_cookie( 'mailfiltro' );
    $mail = $this->request->getPost('id_correo');
    $tipo = 'U'; // se usa tipo U para identificar a los de USEP
    $usuario = $this->usuariosModel->getUserData( $mail );
    if ( $usuario['id'] == '0' ) {
      $usuario = $usuariosFiltro->sessionData( $mail );
      if ( $usuario['id'] == '0' ) {
        $this->sesion->setFlashdata( 'mailregistro', $mail );
        return redirect()->to('/FiltroSanitario/Registro');
      }
      $tipo = 'X'; // se usa tipo X para identificar usuarios externos
    }
    $id = $usuario['correo'];
    set_cookie( 'mailfiltro', $id, 60*60*24*10 );
    if ( $this->request->getPost('accion') === 'qr' ) {
      $folio = $this->filtroFoliosModel->getLastUser($id,$tipo);
      $to = '/FiltroSanitario/MiQr/'.$this->cifrar( $folio );
    }
    else {
      $to = '/FiltroSanitario/Cuestionario/'.$this->cifrar($tipo).'/'.$this->cifrar($id);
    }
    return redirect()->to( $to );
  }

  /**
   * renderiza el formulario de registro para usuarios externos
   */
  public function registro() {
    $mail = $this->sesion->getFlashdata( 'mailregistro' );
    $correo = ( is_null($mail) )? '':$mail;
    $data = [
      'correo' => $correo,
      'nombre' => '',
      'dependencia' => '',
      'categoria' => '',
      'edad' => '',
      'sexo' => '',
    ];
    return view( 'filtro_sanitario/alta', $data );
  }

  /**
   * crea un nuevo registro de un usuario externo
   */
  public function registrar() {
    $usuarioExterno = new FiltroUsuarioExterno( $this->request->getPost() );
    $usuariosFiltro = new FiltroUsuariosExternosModel();
    try {
      $usuariosFiltro->insert( $usuarioExterno );
      $to = '/FiltroSanitario/Cuestionario/'.$this->cifrar('X').'/'.$this->cifrar($usuarioExterno->correo);
      return redirect()->to( $to );
    }
    catch ( \Exception $ex ) {
      log_message("error", "ERROR REGISTRO FILTRO {exception}", ["exception" => $ex]);
    }
    return redirect()->to( '/FiltroSanitario' );
  }

  /**
   * guarda las respuestas de un cuestionario llenado por un usuario
   */
  public function responde() {
    $filtroFolio = new FiltroFolio();
    if( $this->request->getPost('tipo') === 'U' ) {
      $filtroFolio->usuario = $this->request->getPost('correo');
    }
    else {
      $filtroFolio->externo = $this->request->getPost('correo');
    }
    try {
      $this->filtroFoliosModel->insert( $filtroFolio );
      $filtroFolio->id = $this->filtroFoliosModel->lastId();
      $preguntas = $this->preguntasModel->findAll();
      foreach ( $preguntas as $pregunta ) {
        $filtroRespuesta = new FiltroRespuesta();
        $filtroRespuesta->pregunta = $pregunta->id;
        $filtroRespuesta->folio = $filtroFolio->id;
        $filtroRespuesta->respuesta = $this->request->getPost( $pregunta->id );
        $this->filtroRespuestasModel->insert( $filtroRespuesta );
      }
      $to = '/FiltroSanitario/MiQr/'.$this->cifrar( $filtroFolio->id );
      return redirect()->to( $to );
    }
    catch(\Exception $ex) {
      log_message("error", "ERROR REGISTRO FILTRO {exception}", ["exception" => $ex]);
    }
    return redirect()->to( '/FiltroSanitario' );
  }

  /**
   * muestra el codigo QR al usuario
   * @param folio el folio cifrado
   */
  public function miQr($folio) {
    $folio = $this->descifrar( $folio );
    $data['qr'] = null;
    $data['expirado'] = false;
    if ( !is_null($folio) && strlen($folio)>0 ) {
      $filtroFolio = $this->filtroFoliosModel->find( $folio );
      if ( !is_null($filtroFolio) ) {
        $data['expirado'] = false;
        $created = $filtroFolio->created_at;
        $exp = strtotime( $created ) + ( 60*60*24*10 );
        $expiracion = date( "d/m/Y", $exp );
        $hoy = time();
        if ( $exp < $hoy ) {
          $data['expirado'] = true;
        }
        $data['qr'] = $this->creaQr( base_url('Filtro/Acceso').'/'.$filtroFolio->id );
        $data['texto'] = 'Válido hasta '.$expiracion;
      }
      
    }
    return view("filtro_sanitario/qr", $data);
  }

  /**
   * Genera el codigo QR que leerá el doctor para dar acceso a la universidad
   * @param id identificador de la tabla  
   * @return 
   */
  private function creaQr($info, $titulo='') {
    $result = Builder::create()
      ->writer(new PngWriter())
      ->writerOptions([])
      ->data($info)
      ->encoding(new Encoding('UTF-8'))
      ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
      ->size(150)
      ->margin(0)
      ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
      ->labelText($titulo)
      ->labelFont(new NotoSans(18))
      ->labelAlignment(new LabelAlignmentCenter())
      ->build();
    return $result->getDataUri();
  }

}