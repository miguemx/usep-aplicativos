<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <div class="bg-white rounded shadow p-2 mt-2 mb-4">
        <div class="col-sm-12">
        <h5 class="p-2">Relación de cuotas reportadas</h5>
        <a href="<?php echo base_url('Inscripcion/AgregarCuota').'/'.($alumno->id); ?>" class="btn btn-secondary btn-sm m-3">Agregar</a>
        <a href="<?php echo base_url('Inscripcion/Documentacion').'/'.($alumno->id); ?>" class="btn btn-secondary btn-sm m-3">Regresar a documentacion</a>
        <!-- <a href="<?php // echo base_url('Inscripcion/AgregarCuota').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>" class="btn btn-secondary btn-sm m-3">Agregar</a> -->
        <!-- <a href="<?php // echo base_url('Inscripcion/Documentacion').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>" class="btn btn-secondary btn-sm m-3">Regresar a documentacion</a> -->
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Concepto</th>
                        <th>Núm. de Referencia</th>
                        <th>Monto</th>
                        <th>Estatus</th>
                        <th>Observaciones</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php foreach( $pagos as $pago ): ?>
                        <tr>
                            <td><?php echo $pago->conceptoTexto; ?></td>
                            <td><?php echo $pago->referencia; ?></td>
                            <td><?php echo $pago->monto; ?></td>
                            <td><?php echo $pago->status; ?></td>
                            <td>
                                <?php echo $pago->observaciones; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>