<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .filtro_item {
        display: inline-block;
        width: 150px;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1;
        color: #495057;
        /* background-color: #fff; */
        /* background-clip: padding-box; */
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        /* transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out; */
    }

    .contendor_colapse {
        height: 20vh;
    }
</style>
<div class="container  mt-3 ">
    <p>
    <form action="<?php echo base_url('ProgramacionAcademica/inputs/') ?>" method="post" id="form_busqueda">
        <div id="filtros">
        </div>
    </form>
    <button class="form-select" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Filtro de aulas
    </button>
    <button type="button" class="btn btn-secondary m-1" onclick="enviar()">Buscar</button>
    </p>
    <div class="collapse " id="collapseExample">
        <div class="card card-body overflow-auto contendor_colapse">
            <div class=" bg-white" style="position: sticky;top: -1px; background-color:whitesmoke;">
                <input type="text" id="myInput" onkeyup="buscar_enlista(this)" class="form-control m-2" placeholder="Bucar aula en el catalogo">

            </div>
            <div class="form-check ">

                <input class="form-check-input" id="btn_activar_todo" type="checkbox" onclick="activar_todo(this)" name="btn">
                <label class="form-check-label" for="btn_activar_todo" id="label_btn">
                    Seleccionar todo
                </label>
            </div>
            <?php foreach ($aulas as $aula) : ?>
                <div class="form-check" id="<?php echo $aula->id_aula . "_contenedor_checkbox" ?>">
                    <input class="form-check-input" name=" <?php echo $aula->nombre_aula ?>" type="checkbox" id="<?php echo $aula->id_aula ?>" onclick="agregarfiltro(this,'<?php echo $aula->nombre_aula ?>','<?php echo $aula->id_aula ?>')">
                    <label class="form-check-label" for="<?php echo $aula->id_aula  ?>">
                        <?php echo $aula->nombre_aula ?>
                    </label>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>
<style>
    .cont_filtro {
        border: 1px grey solid;
        padding: 6px;
    }
</style>

<script>
    function agregarfiltro(checkbox, nombre, id) {
        if (checkbox.checked == true) {
            var contenedor = document.getElementById('filtros');
            var div_contenedor = document.createElement('div');
            div_contenedor.classList.add('btn', 'cont_filtro');
            div_contenedor.id = 'item_' + id;
            div_contenedor.innerHTML = nombre;

            var input = document.createElement('input');
            input.value = nombre;
            input.name = 'filtro_aulas[]';
            input.hidden = true;
            div_contenedor.appendChild(input);

            var boton = document.createElement('button');
            boton.type = 'button';
            boton.classList.add('btn-close');
            boton.setAttribute('onclick', 'borrar_item(item_' + id + ')');
            div_contenedor.appendChild(boton);
            contenedor.appendChild(div_contenedor);
        } else {
            console.log('apagado');
            var element = document.getElementById("filtros");
            var child = document.getElementById('item_' + id);
            element.removeChild(child);
        }
    }

    function borrar_item(item) {
        // console.log(item.id);
        var element = document.getElementById("filtros");
        // var child = document.getElementById();
        var id = item.id.split('_');
        console.log(id[1]);
        document.getElementById(id[1]).checked = false;
        element.removeChild(item);
    }

    function buscar_enlista(filtro) {
        var buscador = filtro.value.toUpperCase();
        var lista = document.querySelectorAll('input[type=checkbox]');
        lista.forEach(checkbox => {
            if (checkbox.name != 'btn') {
                var txtValue = checkbox.name;
                if (txtValue.toUpperCase().indexOf(buscador) > -1) {
                    document.getElementById(checkbox.id + '_contenedor_checkbox').hidden = false;
                    document.getElementById(checkbox.id).hidden = false;
                } else {
                    document.getElementById(checkbox.id + '_contenedor_checkbox').hidden = true;
                    document.getElementById(checkbox.id).hidden = true;
                }
            }
        });
    }

    function activar_todo(boton) {
        var lista = document.querySelectorAll('input[type=checkbox]');
        var label = document.getElementById('label_btn');
        if (boton.checked == true) {
            label.innerHTML = 'Desmarcar todo';
            lista.forEach(objeto => {
                if (objeto.name != 'btn') {
                    if (objeto.hidden == false) {

                        // var contenedor = document.getElementById('filtros');
                        // var input = document.createElement('input');
                        // input.value = objeto.name;
                        // input.classList.add('filtro_item');
                        // input.id = 'item_' + objeto.id;
                        // input.name = 'filtro_aulas[]';
                        // // input.style()
                        // input.setAttribute('readonly', 'readonly');
                        // contenedor.appendChild(input);
                        objeto.checked = true;


                        var contenedor = document.getElementById('filtros');
                        var div_contenedor = document.createElement('div');
                        div_contenedor.classList.add('btn', 'cont_filtro');
                        div_contenedor.id = 'item_' + objeto.id;
                        div_contenedor.innerHTML =  objeto.name;

                        var input = document.createElement('input');
                        input.value = objeto.name;
                        input.name = 'filtro_aulas[]';
                        input.hidden = true;
                        div_contenedor.appendChild(input);

                        var boton = document.createElement('button');
                        boton.type = 'button';
                        boton.classList.add('btn-close');
                        boton.setAttribute('onclick', 'borrar_item(item_' + objeto.id + ')');
                        div_contenedor.appendChild(boton);
                        contenedor.appendChild(div_contenedor);
                    }
                }
            });
        } else {
            label.innerHTML = 'Seleccionar todo';

            var contenedor = document.getElementById('filtros');
            contenedor.innerHTML = '';
            lista.forEach(objeto => {
                if (objeto.name != 'btn') {
                    objeto.checked = false;
                }
            });
        }
    }

    function enviar() {
        document.getElementById('form_busqueda').submit();
    }
</script>
<?php echo $this->endSection() ?>