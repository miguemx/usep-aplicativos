<?php

namespace App\Entities;

use CodeIgniter\Entity;

class FiltroAlumnos extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'ID' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'nombre' => null,
        'correo' => null,
        'sexo' => null,
        'folio' => null,
        'diabetes' => null,
        'hipertension' => null,
        'cardiacos' => null,
        'enfermedadRespiratoria' => null,
        'otroPadecimiento' => null,
        'esFumador' => null,
        'fiebre' => null,
        'tos' => null,
        'dificultadRespiratoria' => null,
        'dolorCabeza' => null,
        'dolorMuscular' => null,
        'dolorArticular' => null,
        'fatiga' => null,
        'dolorAbdominal' => null,
        'alteracionGusto' => null,
        'alteracionOlfato' => null,
        'escurrimientoNasal' => null,
        'conjuntivitis' => null,
        'escalofrios' => null,
        'estuvoViaje' => null,
        'contactoPersonaCovid' => null,
        'temperatura' => null,
        'oxigenacion' => null,
        'fecha'       => null,
        'vacuna' => null,
        'fecha_prueba_positiva' => null,
        'fecha_prueba_negativo' => null,
        'fecha_contactocovid' => null,
        'tipotos' => null
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'ID' => 'ID',
        'apPaterno' => 'apPaterno',
        'apMaterno' => 'apMaterno',
        'nombre' => 'nombre',
        'correo' => 'correo',
        'sexo' => 'sexo',
        'folio' => 'folio',
        'diabetes' => 'diabetes',
        'hipertension' => 'hipertension',
        'cardiacos' => 'cardiacos',
        'enfermedadRespiratoria' => 'enfermedadRespiratoria',
        'otroPadecimiento' => 'otroPadecimiento',
        'esFumador' => 'esFumador',
        'fiebre' => 'fiebre',
        'tos' => 'tos',
        'dificultadRespiratoria' => 'dificultadRespiratoria',
        'dolorCabeza' => 'dolorCabeza',
        'dolorMuscular' => 'dolorMuscular',
        'dolorArticular' => 'dolorArticular',
        'fatiga' => 'fatiga',
        'dolorAbdominal' => 'dolorAbdominal',
        'alteracionGusto' => 'alteracionGusto',
        'alteracionOlfato' => 'alteracionOlfato',
        'escurrimientoNasal' => 'escurrimientoNasal',
        'conjuntivitis' => 'conjuntivitis',
        'escalofrios' => 'escalofrios',
        'estuvoViaje' => 'estuvoViaje',
        'contactoPersonaCovid' => 'contactoPersonaCovid',
        'temperatura' => 'temperatura',
        'oxigenacion' => 'oxigenacion',
        'fecha' =>         'fecha',
        'vacuna' => 'vacuna',
        'fecha_prueba_positiva' =>  'fecha_prueba_positiva',
        'fecha_prueba_negativo' => 'fecha_prueba_negativo',
        'fecha_contactocovid' => 'fecha_contactocovid',
        'tipotos' => 'tipo_tos'
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
