<?php 
$classGroup = 'input-group input-group-static';
$classLabel = 'form-label';
$classInput = 'form-control';

if ( $type == 'date' ) {
    $classGroup = 'input-group input-group-static';
    $classLabel = '';
    $classInput = 'form-control';
}

?>

<?php 
if ( $disabled == true ) $disabled = 'disabled="disabled"';
?>

<div class="col-md-12"><!-- inicio del form group -->
    <label class="<?php echo $classLabel; ?>"><?php echo $label; ?> <?php if($mandatory==true) echo '*'; ?></label>
    <div class="<?php echo $classGroup; ?>" <?php if($description) echo 'style="margin-bottom: 0px !important;"'; ?>>
        
        <input type="<?php echo $type ?>" 
                class="<?php echo $classInput; ?>" 
                value="<?php if( strlen($value) > 0) echo $value; ?>" 
                name="<?php echo $name; ?>"
                <?php echo $disabled; ?>  />
    </div>
    
    <!-- descripcion del campo -->
    <?php if($description): ?>
        <div style="margin-bottom: 0.5rem !important; font-size: 13px;"><?php echo $description; ?></div>
    <?php endif; ?>
</div><!-- fin del form group -->