<?php echo $this->include('menus/_inicio'); ?>



<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav me-auto mb-2 mb-lg-0">
    <li class="nav-item">
      <a class="nav-link active" href="<?php echo base_url() ?>">Inicio</a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Servicios Escolares
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/Kardex'); ?>">Kardex</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/horario'); ?>">Horario</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/Credencial'); ?>">Mi Credencial</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/MisCuotas') ?>">Mis Cuotas</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/Seguro') ?>">Seguro facultativo</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/Constancias') ?>">Constancias de Estudio</a></li>
        <!-- <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/DescargarDocumento/CONSTANCIA') ?>" target="__blank">Descargar constancia de estudio</a></li> -->
        <!-- <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/DescargarDocumento/KARDEX OFICIAL') ?>" target="__blank">Descargar kardex oficial</a></li> -->
        <!-- <li><a class="dropdown-item" href="<?php echo base_url('Estudiantes/DescargarDocumento/KARDEX SIMPLE') ?>" target="__blank">Descargar kardex simple</a></li> -->
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="https://aplicativos.usep.mx/citas/" target="_blank">Agenda tu cita</a></li>
      </ul>
    </li>
   

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Herramientas
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="https://mail.google.com/" target="_blank">Mi correo</a></li>
        <li><a class="dropdown-item" href="https://classroom.google.com/" target="_blank">Mis clases</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Bibliotecas') ?>">Bibliotecas</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Login/Logout'); ?>">Cerrar Sesión</a></li>
      </ul>
    </li>
  </ul>
  <div style="color: #fff;">
    <?php echo session('id'); ?>
    <?php if (!is_null(session('nombre'))) :
      echo ' - ' . session('nombre');
    endif;
    ?>
    <?php if (!is_null(session('trproced'))) : ?>
      <form method="post" action="<?php echo base_url('Escolar/VuelvePerfil'); ?>">
        <button type="submit" name="prback" value="<?php echo session('trproced'); ?>" class="btn-secondary">Regresar</button>
      </form>
    <?php endif; ?>
  </div>
</div>

<?php echo $this->include('menus/_fin'); ?>