<div style="background-image: url('<?php echo base_url('img/fondos/constancia_footer_fondo.png') ?>'); background-repeat: no-repeat; background-position: left bottom; background-size: 100%; height: 100px; width: 100%;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top: 65px; font-size: 4px">
        <tr>
            <td width="33%" style="text-align: left; font-size: 6.5px; font-weight: bold; padding-left: 10px; font-family: Arial, Helvetica, sans-serif;">
                Dirección de Servicios Escolares y Titulación USEP
            </td>
            <td width="34%" style="text-align: center; font-size: 6.5px; font-weight: bold;">
                Página {PAGENO} de [pagetotal]
            </td>
            <td width="33%" style="text-align: right">
                &nbsp;
            </td>
        </tr>   
    </table>
</div>