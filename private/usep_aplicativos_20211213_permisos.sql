CREATE TABLE `aplicativo_permisos` ( 
    `permiso_id`  INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    `permiso_rol`  INT UNSIGNED NOT NULL , 
    `permiso_modulo` VARCHAR(80) NOT NULL , 
    `permiso_funcion` VARCHAR(80) NULL,
    FOREIGN KEY (permiso_rol) REFERENCES aplicativo_roles(rol_id) 
) ENGINE = InnoDB;