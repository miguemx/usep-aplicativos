<?php
namespace App\Entities;

use CodeIgniter\Entity;

class GrupoAlumno extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'alumno' => null,
        'grupo' => null,
        'ordinario' => null,
        'extraordinario' => null,
        'comentarios'=>null,
        'aprobada' => null,
        'bandera' => null,
        'Noficio'=> null,
        'autor' => null,
        'fecha' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'gpoalumno_id',
        'alumno' => 'gpoalumno_alumno',
        'grupo' => 'gpoalumno_grupo',
        'ordinario' => 'gpoalumno_ordinario',
        'extraordinario' => 'gpoalumno_extraordinario',
        'aprobada' => 'aprobada',
        'bandera' => 'gpoalumno_flg_pago',
        'Noficio'=> 'gpoalumno_num_oficio',
        'autor' => 'gpoalumno_autor',
        'fecha' => 'gpoalumno_fecha_modificacion',
        'comentarios'=>'gpoalumno_comentario',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
