<?php
namespace App\Models;
use CodeIgniter\Model;
use \App\Entities\AsistenciasDiarias;

class AsistenciasDiariasModel extends Model {
    protected $table      = 'aca_asistencias_diarias';
    protected $primaryKey = 'asistencia_id';

    protected $returnType    = 'App\Entities\AsistenciasDiarias';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'asistencia_id','asistencia_grupo','asistencia_matricula','asistencia_docente','asistencia_fecha','asistencia_aula','asistencia_tipo'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function buscaasistencias($fecha,$iddocente)
    {
        $asistencias = $this->where(['asistencia_docente'=>$iddocente, 'asistencia_fecha'=>$fecha])->findAll();
        return $asistencias;
    }
    public function updateasistencias($matricula,$fecha,$asistencia){
        return $this->db
        ->table('aca_asistencias_diarias')
        ->where(["asistencia_matricula" =>$matricula, 'asistencia_fecha' =>$fecha])
        ->set($asistencia)
        ->update();
    }
}