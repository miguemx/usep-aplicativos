<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities;
class HorarioAlumnosModel extends Model
{
    protected $table="view_horario_alumnos";
    protected $primaryKey="gpoalumno_alumno";
    protected $returnType="App\Entities\HorarioAlumnos";
    protected $allowedFields=['gpoalumno_alumno','gpoalumno_grupo','grupo_materia','grupo_clave','grupo_docente','empleado_correo',
                            'empleado_nombre','empleado_ap_paterno','empleado_ap_materno','gpoaula_dia','gpoaula_inicio',
                            'gpoaula_fin','aula_nombre','materia_nombre','grupo_periodo',];
    protected $db;
    protected $builder;

    /**
     * consulta los registros de horario que puede tener un estudiante en particular
     * @param id el Id o matricula del estudiante
     * @return array un arreglo con los datos de la consulta
     */
    public function horarioAlumno($id){
        return $this->where('gpoalumno_alumno',$id)->orderBy('gpoalumno_grupo','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')->findAll();
    }

    /**
     * devuelve un arreglo con las posiciones para imprimir un horario de un estudiante
     * @param id el ID o matricula del estudiante
     * @return horario el arreglo con los datos del horario del estudiante
     */
    public function rejillaHorario($id) {
        $datos = $this->horarioAlumno( $id );
        
        $horario = [];
        foreach ( $datos as $registro ) {
            $horario[ $registro->idc ]['materia'] = $registro->materia.' ('.$registro->idmateria.')';
            $horario[ $registro->idc ]['docente'] = $registro->n_docente.' '.$registro->app_docente.' '.$registro->apm_docente;
            $horario[ $registro->idc ]['seccion'] = $registro->seccion;
            if ( $registro->dia == 'LUNES' ) {
                $horario[ $registro->idc ]['LUNES'][ $registro->h_ini ] = $registro->aula;
            }
            if ( $registro->dia == 'MARTES' ) {
                $horario[ $registro->idc ]['MARTES'][ $registro->h_ini ] = $registro->aula;
            }
            if ( $registro->dia == 'MIÉRCOLES' || $registro->dia == 'MIERCOLES'  ) {
                $horario[ $registro->idc ]['MIERCOLES'][ $registro->h_ini ] = $registro->aula;
            }
            if ( $registro->dia == 'JUEVES' ) {
                $horario[ $registro->idc ]['JUEVES'][ $registro->h_ini ] = $registro->aula;
            }
            if ( $registro->dia == 'VIERNES' ) {
                $horario[ $registro->idc ]['VIERNES'][ $registro->h_ini ] = $registro->aula;
            }
            if ( $registro->dia == 'SÁBADO' || $registro->dia == 'SABADO' ) {
                $horario[ $registro->idc ]['SABADO'][ $registro->h_ini ] = $registro->aula;
            }
        }
        return $horario;
    }

    /**
     * retorna el listado de grupos al que pertenece el alumno en un periodo seleccionado
     * @param alumno ID alumno
     * @param periodo ID del periodo
     */
    public function getByPeriodo($alumno,$periodo)
    {
        return $this->where('gpoalumno_alumno',$alumno)->where('grupo_periodo',$periodo)->orderBy('gpoalumno_grupo','ASC')->orderBy('gpoaula_dia','ASC')->orderBy('gpoaula_inicio','ASC')->find();
    }
    

}

?>