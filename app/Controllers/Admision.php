<?php

namespace App\Controllers;

use App\Models\AspirantesGeneralesModel;
use App\Models\AspirantesEtapasModel;
use App\Models\AspirantesFormulariosModel;
use App\Models\AspirantesCamposModel;
use App\Models\AspirantesRespuestasModel;
use App\Models\AspiranteRespuestasModel; // el singular refiere a la vista de respuestas
use App\Models\AspirantesTiposCampoModel;
use App\Models\CarrerasModel;
use App\Models\EstadosModel;
use App\Models\MunicipiosModel;
use App\Models\AspirantesConfiguracionModel;
use App\Entities\AspirantesGenerales;
use App\Entities\AspirantesRespuestas;
use App\Entities\AspirantesConfiguracion;
use App\Models\AspirantesNotificacionesModel;
use App\Entities\AspirantesNotificaciones;
use App\Libraries\RespuestaValidacion;
use CodeIgniter\Files\File;

/**
 * Controla todo el flujo del proceso de admisión
 * solo para los aspirantes. las cuestiones adminsitrativas
 * del proceso se hacen el el controlador CotrollerAdmision
 */
class Admision extends BaseController
{

    private $aspirantesModel;
    private $etapasModel;
    private $carrerasModel;
    private $formulariosModel;
    private $camposModel;
    private $respuestasModel;
    private $respModel;
    private $tiposCamposModel;

    private $cifraPrefijo = 'AdM-Ini';
    private $cifraSufijo = '_endAdm';
    private $baseDir = '';

    private $urlLogin = '/Admision/Inicia';
    private $estadosModel;
    private $municipiosModel;
    private $aspirantesConfiguracionModel;

    private $notificacionesModel;
    /**
     * 
     */
    public function __construct()
    {
        $this->aspirantesModel = new AspirantesGeneralesModel();
        $this->etapasModel = new AspirantesEtapasModel();
        $this->carrerasModel = new CarrerasModel();
        $this->formulariosModel = new AspirantesFormulariosModel();
        $this->camposModel = new AspirantesCamposModel();
        $this->respuestasModel = new AspirantesRespuestasModel();
        $this->respModel = new AspiranteRespuestasModel();
        $this->tiposCamposModel = new AspirantesTiposCampoModel();
        $this->estadosModel = new EstadosModel();
        $this->municipiosModel = new MunicipiosModel();
        $this->aspirantesConfiguracionModel = new AspirantesConfiguracionModel();
        $this->basedir = WRITEPATH . 'admision/2022A/';
        $this->notificacionesModel = new AspirantesNotificacionesModel();
    }

    /**
     * pantalla principal del portal de aspirantes; si no existe sesion, redirige a inicio
     * @todo detectar el periodo del aspirante
     */
    public function index()
    {
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $periodo = '2022A';

        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
        if ($notificaciones) {
            $data['notificaciones'] = $notificaciones;
        } else {
            $data['notificaciones'] = '';
        }
        $data['etapas'] = $this->getProgreso();
        $data['band_tiempo'] = $this->aspiranteaceptado();
        $data['aspirante'] = $this->aspirantesModel->find($this->sesion->get('aspirante'));
        $data['observaciones'] = $this->getObservaciones();
        return view('admision/panel', $data);
    }


    /**
     * renderiza una etapa
     * @todo validar el origen del periodo
     */
    public function etapa($id)
    {
        $periodo = '2022A';
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $aspirante = $this->aspirantesModel->find($this->sesion->get('aspirante'));
        if (is_null($aspirante)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            return 0;
        }
        if ($aspirante->estatus == 'DECLINADO') {
            return redirect()->to('/Admision');
        }
        if (is_null($aspirante->municipio)) {
            return redirect()->to('/Admision/DatosGenerales');
        }
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($aspirante->id);
        if ($notificaciones) {
            $data['notificaciones'] = $notificaciones;
        } else {
            $data['notificaciones'] = '';
        }
        $data['warn'] = $this->sesion->getFlashdata('warn');
        $data['etapa'] = $this->etapasModel->find($id);
        $data['etapas'] = $this->getProgreso();
        $data['aspirante'] = $aspirante;
        $etapa = $this->etapasModel->find($id);
        if (!is_null($etapa)) {
            $formularios = $this->formulariosModel->getFromEtapa($etapa->etapa);
            $data['etapa'] = $etapa;
            $data['formularios'] = $formularios;
            return view('admision/etapa', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * renderiza una formulario
     * @todo validar el origen del periodo
     */
    public function formulario($id)
    {
        $periodo = '2022A';
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $aspirante = $this->aspirantesModel->find($this->sesion->get('aspirante'));
        if (is_null($aspirante)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            return 0;
        }
        if (is_null($aspirante->municipio)) {
            return redirect()->to('/Admision/DatosGenerales');
        }
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
        if ($notificaciones) {
            $data['notificaciones'] = $notificaciones;
        } else {
            $data['notificaciones'] = '';
        }
        $data['formulario'] = $this->formulariosModel->find($id);
        $data['aspirante'] = $aspirante;
        $data['etapas'] = $this->getProgreso();

        if (!is_null($data['formulario'])) {
            $data['campos'] = $this->camposModel->getFromFormulario($data['formulario']->formulario);
            $data['tiposCampos'] = [];
            $tiposCampo = $this->tiposCamposModel->findAll();
            foreach ($tiposCampo as $tipoCampo) {
                $data['tiposCampos'][$tipoCampo->id] = $tipoCampo;
            }
            if ($this->request->getPost('prc')) { // se entra aqui cuando viene del clic de guardar
                $guardado = $this->guardar($data['campos']);
                if ($guardado !== true) {
                    $data['errores'] = $guardado;
                } else {
                    $redir = 'Etapa/' . $data['formulario']->etapa;
                    $next = $this->formHasNext($data['formulario']);
                    if ($next !== false) $redir = 'Formulario/' . $next;
                    $data['msg'] = 'Registro guardado correctamente.';
                    $data['redir'] = base_url('Admision') . '/' . $redir;
                }
            }
            $data['respuestas'] = $this->respModel->getFromFormulario($this->sesion->get('aspirante'), $id);
            return view('admision/formulario', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * guarda un registro de formulario por parte de un aspirante
     * @param listaCampos la lista con los datos de los campos
     */
    private function guardar($listaCampos)
    {
        $errores = '';
        $valores = $this->request->getPost();
        $tiposCampo = $this->tiposCamposModel->findAll();
        foreach ($valores as $idCampo => $valor) {
            $res = $this->guardaRespuesta($idCampo, $valor, $tiposCampo);
            if ($res !== true) {
                $errores .= $res . '<br />';
            }
        }
        try {
            $files = $this->request->getFiles();
            $dir = $this->basedir . $this->sesion->get('aspirante');
            if (!is_dir($dir)) mkdir($dir);
            foreach ($files as $idCampo => $archivo) {
                $res = $this->guardaArchivo($idCampo, $archivo, $dir, $tiposCampo, $listaCampos[$idCampo]->nombre);
                if ($res !== true) {
                    $errores .= $res . '<br />';
                }
            }
        } catch (\Exception $ex) {
            log_message("error", "ERROR {exception}", ["exception" => $ex]);
            $errores = 'No se pudo crear el directorio. <br />';
        }

        if (strlen(trim($errores)) > 6) {
            return $errores;
        }
        return true;
    }

    /**
     * realiza el guardado de un archivo subido por el aspirante, y genera la respuesta en base de datos
     * para mantener la relación del archivo
     * @param idCampo el ID del campo correspondiente
     * @param archivo el objeto del archivo subido
     * @param dir el directorio donde colocar el archivo
     * @param tiposCampo la colección de todos los tipos de dato
     * @return true en caso de que no exista error
     * @return errores una cadena con los errores 
     */
    private function guardaArchivo($idCampo, $archivo, $dir, $tiposCampo, $label)
    {
        $errores = '';
        $validationRule = [
            $idCampo => [
                'rules' => 'max_size[' . $idCampo . ',600]|mime_in[' . $idCampo . ',image/jpg,image/jpeg,image/gif,image/png,application/pdf]'
            ]
        ];
        if ($archivo->isValid()) {
            if ($this->validate($validationRule)) {
                $filename = preg_replace("/[^\w|^.]/", '_', $archivo->getClientName());
                $filename = str_replace("|", "_", $filename);
                $archivo->move($dir, $filename, true);
                if ($archivo->hasMoved()) {
                    $res = $this->guardaRespuesta($idCampo, $filename, $tiposCampo);
                    if ($res !== true) {
                        $errores .= $res . '<br />';
                        unlink($dir . '/' . $filename);
                    }
                } else {
                    $errores .= $filename . ' no fue cargado. Si ya lo había subido con anterioridad, ignore este mensaje.<br />';
                }
            } else {
                $errores .= '<strong>' . $label . '</strong> no tiene el formato correcto, o excede el tamaño permitido (600kb). <br />';
            }
        } else {
            $errores .= $label . ' no es un archivo correcto. <br />';
        }

        if (strlen($errores) == 0) {
            return true;
        }
        return $errores;
    }

    /**
     * realiza la subida de archivos del usuario
     * @param idCampo el Id del campo a guardar
     * @param valor la respuesta que se desea guardar
     * @param tiposCampo la lista completa de tipos de campo
     */
    private function guardaRespuesta($idCampo, $valor, $tiposCampo)
    {
        $errores = true;
        try {
            $campo = $this->camposModel->find($idCampo);
            if (!is_null($campo)) {
                $res = $this->validarRespuesta($campo, $valor, $tiposCampo);
                if ($res === true) {
                    $respuesta = $this->respuestasModel->contestado($idCampo, $this->sesion->get('aspirante'));
                    if (!is_null($respuesta)) {
                        $respuesta->valor = $valor;
                        $this->respuestasModel->update($respuesta->respuesta, $respuesta);
                    } else {
                        $respuesta = new AspirantesRespuestas();
                        $respuesta->aspirante = $this->sesion->get('aspirante');
                        $respuesta->campo = $idCampo;
                        $respuesta->valor = $valor;
                        $this->respuestasModel->insert($respuesta);
                    }
                } else {
                    $errores = $res;
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
            if ($ex->getMessage() != 'There is no data to update.') {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
                $errores = 'Ocurrió un error al guardar los datos del archivo. Por favor verifica o intenta nuevamente. <br />';
            }
        } catch (\Exception $ex) {
            log_message("error", "ERROR {exception}", ["exception" => $ex]);
            $errores = 'Ha ocurrido un error inesperado. Por favor cierra tu sesión y vuelve a iniciar. <br />';
        }
        return $errores;
    }

    /**
     * valida una respuesta de acuerdo a las reglas establecidas en la base de datos
     * @param campo el registro completo del campo
     * @param valor el valor que intenta introducir el usuario
     * @return true en caso de que el valor sea válido
     * @return error una cadena con el error reportado
     */
    private function validarRespuesta($campo, $valor, $tiposCampo)
    {
        $validador = new RespuestaValidacion($tiposCampo);
        if ($validador->do($campo, $valor)) {
            return true;
        } else {
            return implode('<br />', $validador->getErrors());
        }
    }

    /**
     * renderiza la vista de inicio de sesion
     * @todo detectar el periodo del aspirante
     */
    public function inicia()
    {
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
        if ($notificaciones) {
            $data['notificaciones'] = $notificaciones;
        } else {
            $data['notificaciones'] = '';
        }
        $data['folio'] = ($this->request->getPost("folio")) ? $this->request->getPost("folio") : '';
        $data['curp'] = ($this->request->getPost("curp")) ? $this->request->getPost("curp") : '';
        if ($this->request->getPost("loginasp") === "1" && $data['folio'] && $data['curp']) {
            if ($this->setSesion()) {
                $estados = $this->estadosModel->find();
                $aspiranteDatos = $this->aspirantesModel->find($this->request->getPost("folio"));
                $sts_revalida = $this->aspirantesConfiguracionModel->configuracion('2');
                if ($aspiranteDatos->aspirante_colonia == NULL) { //condición para verificar si el registro del aspirante se encuentra lleno en la tabla de aspirantes generales
                    return redirect()->to('/Admision/DatosGenerales');
                } else {
                    return redirect()->to('/Admision');
                }
            } else {
                $data['error'] = 'Su folio o la CURP son incorrectos, o bien, no ha validado su cuenta.';
            }
        }
        return view('admision/login', $data);
    }

    /**
     * renderiza la vista de los datos generales
     */
    public function datosGenerales()
    {
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $estados = $this->estadosModel->find();
        $aspiranteDatos = $this->aspirantesModel->find($this->sesion->get("aspirante"));
        $sts_revalida = $this->aspirantesConfiguracionModel->configuracion('2');

        if ($aspiranteDatos != NULL) { //condición para verificar si el registro del aspirante se encuentra lleno en la tabla de aspirantes generales
            $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
            $data['notificaciones'] = ($notificaciones) ? $notificaciones : '';
            $data['aspirante'] = $aspiranteDatos;
            $data['folio'] = $aspiranteDatos->folio;
            $data['aspirante_nombre'] = $aspiranteDatos->folio;
            $data['aspirante_ap_paterno'] = $aspiranteDatos->aspirante_ap_paterno;
            $data['aspirante_ap_materno'] = $aspiranteDatos->aspirante_ap_materno;
            $data['estados'] = $estados;
            $data['revalida_flg'] = $sts_revalida->configuracion_status;
            $data['carrera'] = $aspiranteDatos->aspirante_carrera;
            $periodo = '2022A'; // TODO detectar el periodo del aspirante     
            $data['etapas'] = $this->getProgreso();
            if ($aspiranteDatos->municipio == NULL) { //condición para verificar si el registro del aspirante se encuentra lleno en la tabla de aspirantes generales
                return view('admision/aspiranteDatosComp', $data);
            } else {
                if ($aspiranteDatos->aspirante_pais == "EXTRANJERO") {
                    $estado_res = 'N/A';
                    $municipio_res = 'N/A';
                    $data['municipio'] = $municipio_res;
                    $data['estado'] = $estado_res;
                } else {
                    $estado_res = $this->estadosModel->find($aspiranteDatos->aspirante_estado);
                    $municipio_res = $this->municipiosModel->find($aspiranteDatos->aspirante_municipio);
                    $data['municipio'] = $municipio_res->nombre;
                    $data['estado'] = $estado_res->nombre;
                }

                if ($aspiranteDatos->aspirante_nac_pais == "EXTRANJERO") {
                    $estado_nac = 'N/A';
                    $municipio_nac = 'N/A';
                    $data['nac_municipio'] = $municipio_nac;
                    $data['nac_estado'] = $estado_nac;
                } else {
                    $estado_nac = $this->estadosModel->find($aspiranteDatos->aspirante_nac_estado);
                    $municipio_nac = $this->municipiosModel->find($aspiranteDatos->aspirante_nac_municipio);
                    $data['nac_municipio'] = $municipio_nac->nombre;
                    $data['nac_estado'] = $estado_nac->nombre;
                }
                $data['pais'] = $aspiranteDatos->aspirante_pais;
                $data['nac_pais'] = $aspiranteDatos->aspirante_nac_pais;
                $data['revalidacion'] = $aspiranteDatos->aspirante_revalida;
                return view('admision/aspirantesGenerales', $data);
            }/**/
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * destruye la sesion y regresa a la pantalla de inicio de sesion
     */
    public function salir()
    {
        $this->sesion->destroy();
        return redirect()->to('/Admision/Inicia');
    }

    /**
     * Genera la vista del registro de aspirantes
     */
    public function registro()
    {
        $admision_flg = $this->aspirantesConfiguracionModel->configuracion('1');
        if ($admision_flg->configuracion_status != '1') {
            return redirect()->to('/Admision/');
        } //condicion que verifica si el proceso admision se encuentra abierto
        $data['curp'] = ($this->request->getPost('curp')) ? mb_strtoupper($this->request->getPost('curp')) : '';
        $data['apPaterno'] = ($this->request->getPost('apPaterno')) ? mb_strtoupper($this->request->getPost('apPaterno')) : '';
        $data['apMaterno'] = ($this->request->getPost('apMaterno')) ? mb_strtoupper($this->request->getPost('apMaterno')) : '';
        $data['nombre'] = ($this->request->getPost('nombre')) ? mb_strtoupper($this->request->getPost('nombre')) : '';
        $data['correo'] = ($this->request->getPost('correo')) ? $this->request->getPost('correo') : '';
        $data['carrera'] = ($this->request->getPost('carrera')) ? $this->request->getPost('carrera') : '';
        $data['terminos'] = ($this->request->getPost('terminos')) ? $this->request->getPost('terminos') : '';
        $data['aviso'] = ($this->request->getPost('aviso')) ? $this->request->getPost('aviso') : '';
        $data['carreras'] = $this->carrerasModel->activas();
        if ($this->request->getPost('aspreg')) {
            $res = $this->registra($data);
            if ($res[0] === true) {
                $this->sesion->setFlashdata('nombre', $data['nombre']);
                $this->sesion->setFlashdata('correo', $data['correo']);
                $this->correoValidacion($res[1]);
                return redirect()->to('/Admision/Registrado');
            } else $data['error'] = $res[1];
        }
        return view('admision/registro', $data);
    }

    /**
     * renderiza la vista de un aspirante registrado
     */
    public function registrado()
    {
        $data = [
            'nombre' => $this->sesion->getFlashdata('nombre'),
            'correo' => $this->sesion->getFlashdata('correo')
        ];
        return view('admision/registrado', $data);
    }

    /**
     * busca un registro de un aspirante de acuerdo a su cadena cifrada y
     * marca al aspirante como verificado, indicando a esta sus datos de acceso
     * @param cadena la cadena cifrada con el folio del estudiante
     */
    public function verifica($cadena)
    {
        $folio = $this->descifrar($cadena);
        $folio = str_replace($this->cifraPrefijo, '', $folio);
        $folio = str_replace($this->cifraSufijo, '', $folio);
        $aspirante = $this->aspirantesModel->find($folio);
        if (!is_null($aspirante)) {
            $aspirante->validado = '1';
            try {
                $this->aspirantesModel->update($aspirante->folio, $aspirante);
                $this->correoDatos($aspirante);
            } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
            } catch (\Exception $ex) {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
            }
            $data['nombre'] = $aspirante->nombre;
            $data['correo'] = $aspirante->correo;
            $data['folio'] = $aspirante->folio;
            $data['curp'] = $aspirante->curp;
            return view('admision/verificado', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * envia el correo para validar el correo electrónico
     * @param aspirante el objeto de tipo AspiranteEitity con los datos del registro generado
     * @return void
     */
    private function correoValidacion($aspirante)
    {
        $destinatario = $aspirante->correo;
        $data['nombre'] = $aspirante->nombre;
        $data['cifrado'] = $this->cifrar($this->cifraPrefijo . $aspirante->folio . $this->cifraSufijo);
        $data['enlace'] = base_url('Admision/Verifica') . '/' . $data['cifrado'];
        $mensaje = view('admision/correoregistro', $data);
        $asunto = 'Bienvenido(a) a su proceso de admisión de la Universidad de la Salud.';
        $correoDe = 'admision@usalud.edu.mx';
        $nombreDe = 'Admisión - Universidad de la Salud';
        $this->correo($destinatario, $mensaje, $asunto, $correoDe, $nombreDe);
    }

    /**
     * envia el correo para validar el correo electrónico
     * @param aspirante el objeto de tipo AspiranteEitity con los datos del registro generado
     * @return void
     */
    private function correoDatos($aspirante)
    {
        $destinatario = $aspirante->correo;
        $data['nombre'] = $aspirante->nombre;
        $data['curp'] = $aspirante->curp;
        $data['folio'] = $aspirante->folio;
        $data['enlace'] = base_url('Admision');
        $mensaje = view('admision/correodatos', $data);
        $asunto = 'Datos de acceso al Portal de Aspirantes de la Universidad de la Salud.';
        $correoDe = 'admision@usalud.edu.mx';
        $nombreDe = 'Admisión - Universidad de la Salud';
        $this->correo($destinatario, $mensaje, $asunto, $correoDe, $nombreDe);
    }

    /**
     * ajusta los valores de la sesion de acuerdo al aspirante
     * @return true en caso de que se pueda encontrar el aspirante e inicializar su sesion
     * @return false en caso 
     */
    private function setSesion()
    {
        $aspirante = $this->aspirantesModel->buscar($this->request->getPost());
        if (count($aspirante)) {
            if ($aspirante[0]->validado === '1') {
                $this->sesion->set('aspirante', $aspirante[0]->folio);
                $this->sesion->set('useremail', 'hgfg'); //borrar cuando se encuentre una solucion al inicio de sesión
                $this->sesion->set('id', 'jhgh'); //borrar cuando se encuentre una solucion al inicio de sesión
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * valida la sesion de los aspirantes
     */
    private function validaSesion()
    {
        if (is_null($this->sesion->get('aspirante'))) {
            return false;
        }
        return true;
    }

    /**
     * genera un registro nuevo de un aspirante
     * @param datos el arreglo asociativo para construir el objeto de AspirantesGenerales
     * @return res arreglo de la forma siguiente: [ 0=>true|false, 1=>"mensaje de error"|Aspirante ]
     * @todo validar el periodo para obtener dinamicamente de la tabla de periodos
     */
    private function registra($datos)
    {
        $validacion = $this->valida($datos, 'aspirante');
        if ($validacion === true) {
            if (!$this->aspirantesModel->existe($datos['curp'])) {
                $periodo = '2022A'; // TODO - actualizar la tabla para definir periodo o bandera de inscripcion
                $periodoAnioInicio = '22'; // TODO - actualizar la tabla para definir periodo o bandera de inscripcion
                $aspirante = new AspirantesGenerales($datos);
                $aspirante->sexo = substr($aspirante->curp, 10, 1);
                $etapas = $this->etapasModel->getPeriodo($periodo);
                if (count($etapas)) {
                    $aspirante->etapa = $etapas[0]->etapa;
                    $aspirante->validado = '0';
                    try {
                        $aspirante->folio = $this->aspirantesModel->creaMatricula($periodoAnioInicio, $aspirante->carrera);
                        $this->aspirantesModel->insert($aspirante);
                        $res = [true, $aspirante];
                    } catch (\Exception $ex) {
                        log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                        $res = [false, 'Ocurrió un error al generar tu registro.'];
                    }
                } else {
                    $res = [false, 'El periodo de registro para aspirantes se ha cerrado.'];
                }
            } else {
                $res = [false, '- La CURP ya ha sido registrada. Puedes verificar tu correo para obtener tu folio de registro e iniciar sesión en el Portal de Aspirantes.'];
            }
        } else {
            $res = [false, '- ' . implode('<br />- ', $validacion)];
        }
        return $res;
    }

    /**
     * Actualiza los valores de los campos restantes de la tabla de aspirantes_generales
     * 
     */
    public function regAspiranteComp()
    {
        $aspiranteModel = new AspirantesGeneralesModel();
        $aspirantefolio = $this->sesion->get('aspirante');
        $aspiranteDatos = $aspiranteModel->find($aspirantefolio); // empezar aqui
        $aspiranteDatos->aspirante_calle = $this->request->getPost('calle');
        $aspiranteDatos->aspirante_colonia = $this->request->getPost('colonia');
        $aspiranteDatos->aspirante_numero = $this->request->getPost('numext');
        $aspiranteDatos->aspirante_telefono = $this->request->getPost('telefono');
        $aspiranteDatos->aspirante_num_int = $this->request->getPost('numint');
        $aspiranteDatos->aspirante_cp = $this->request->getPost('cp');
        $aspiranteDatos->aspirante_municipio = $this->request->getPost('municipio');
        $aspiranteDatos->aspirante_estado = $this->request->getPost('estado');
        $aspiranteDatos->aspirante_pais = $this->request->getPost('pais');
        $aspiranteDatos->aspirante_sexo = $this->request->getPost('sexo');
        $aspiranteDatos->aspirante_nac_fecha = $this->request->getPost('fecha_nacimiento');
        $aspiranteDatos->aspirante_nac_municipio = $this->request->getPost('municipios_nac');
        $aspiranteDatos->aspirante_nac_estado = $this->request->getPost('estados_nac');
        $aspiranteDatos->aspirante_nac_pais = $this->request->getPost('pais_nac');
        $aspiranteDatos->aspirante_revalida = $this->request->getPost('revalidar');
        try {
            $aspiranteModel->update($aspirantefolio, $aspiranteDatos);
            $data['acept'] = 'Guardado exitoso ';
            return redirect()->to('/Admision');
        } catch (\Exception $ex) {
            $data['error'] = 'No fue posible guardar los datos. ' . $ex->getMessage();
            return view('docentes/bitacora_aulas', $data);
        }
    }

    /**
     * obtiene los municipios de un estado
     * @param tipo 1 si el select es un dato de domicilio, 2 si es un dato de nacimiento
     * @return municipios arreglo json de los municipos del estado
     */

    public function obtenerMunicipios($tipo)
    {
        $municipiosmodel = new MunicipiosModel();
        if ($tipo == 1) {
            $estado = $this->request->getPost('estado');
        }
        if ($tipo == 2) {
            $estado = $this->request->getPost('estado_nac');
        }
        $municipios = $municipiosmodel->getByEstadosmunicipio($estado);
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode($municipios);
    }

    /**
     * obtiene el progreso general del aspirante actual
     * @todo actualizar para obtener el periodo de forma dinámica
     */
    private function getProgreso()
    {
        $periodo = '2022A';
        $etapas = $this->etapasModel->getPeriodo($periodo);
        $aspirante = $this->aspirantesModel->find($this->sesion->get('aspirante'));
        $progreso = [];
        if (!is_null($aspirante)) {
            $completado = true;
            foreach ($etapas as $etapa) {
                $etapa->actual = false;
                if ($etapa->etapa == $aspirante->etapa) {
                    $completado = false;
                    $etapa->actual = true;
                }
                $etapa->completado = $completado;
                $progreso[] = $etapa;
            }
        }
        return $progreso;
    }

    /**
     * actualiza las notificaciones del alumno como leidas
     */
    public function updateAlerts($aspirante)
    {
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($aspirante);
        if ($notificaciones) {
            foreach ($notificaciones as $notificacion) {
                if ($notificacion->leido == null) {
                    $notificacion->leido = date("Y-m-d");
                    $this->notificacionesModel->update($notificacion->id, $notificacion);
                }
            }
        }
    }

    /**
     * avanza a la siguiente etapa a un aspirante y llama al método para renderizar
     */
    public function finalizaEtapa()
    {
        $etapaActual = $this->request->getPost('etapa');
        $aspirante = $this->aspirantesModel->find($this->sesion->get('aspirante'));
        $redir = '/Admision';
        if (!is_null($aspirante)) {
            $progreso = $this->getProgreso();
            $totalEtapas = count($progreso);
            for ($i = 0; $i < $totalEtapas; $i++) {
                if ($progreso[$i]->etapa == $etapaActual) {
                    if ($this->validaEtapa($etapaActual)) {
                        try {
                            if (array_key_exists($i + 1, $progreso)) {
                                $aspirante->etapa = $progreso[$i + 1]->etapa;
                                $redir = '/Admision/Etapa/' . $aspirante->etapa;
                            } else {
                                $aspirante->etapa = NULL;
                                $aspirante->estatus = 'ESPERA';
                                $notificacion = new AspirantesNotificaciones();
                                $notificacion->aspirante = $this->sesion->get('aspirante');
                                $notificacion->mensaje = 'Proceso de registro concluido: Ha concluido satisfactoriamente con el registro y carga de documentos, por lo que deberá consultar diariamente el "Portal de Aspirantes" para verificar las notificaciones u observaciones que genere el sistema.';
                                $this->notificacionesModel->insert($notificacion);
                            }
                            $this->aspirantesModel->update($aspirante->folio, $aspirante);
                            break;
                        } catch (\Exception $ex) {
                            log_message('error', '[ERROR] {exception}', ['exception' => $ex]);
                        }
                    } else {
                        $this->sesion->setFlashdata('warn', 'Aún tiene preguntas sin contestar. Por favor verifique sus formularios.');
                        $redir = '/Admision/Etapa/' . $etapaActual;
                    }
                }
            }
        }
        return redirect()->to($redir);
    }

    /**
     * valida que los campos requeridos de una etapa estén contestados
     * @param etapa el ID de la etapa a validar
     * @return true en caso de que la etapa esté completa
     * @return false en caso de que la etapa tenga faltantes
     */
    private function validaEtapa($etapa)
    {
        $campos = $this->camposModel->getByEtapa($etapa, true);
        $respuestas = $this->respModel->getByEtapa($this->sesion->get('aspirante'), $etapa, true);
        $totalCampos = count($campos);
        $totalRespuestas = count($respuestas);
        if ($totalRespuestas >= $totalCampos) {
            return true;
        }
        return false;
    }
    /**
     * Genera la vista de recuperacion de enlace de activación
     */
    public function recuperacceso()
    {
        $admision_flg=$this->aspirantesConfiguracionModel->configuracion('1');
        if ($admision_flg->configuracion_status!='1'){return redirect()->to('/Admision/');} //condicion que verifica si el proceso admision se encuentra abierto
        $data['curp'] = ($this->request->getPost('curp')) ? mb_strtoupper($this->request->getPost('curp')) : '';
        $data['apMaterno'] = ($this->request->getPost('apMaterno')) ? mb_strtoupper($this->request->getPost('apMaterno')) : '';
        $data['carrera'] = ($this->request->getPost('carrera')) ? $this->request->getPost('carrera') : '';
        $data['carreras'] = $this->carrerasModel->activas();
        if ($this->request->getPost('aspreg')) {
            $res = $this->registra($data);
            if ($res[0] === true) {
                $this->sesion->setFlashdata('nombre', $data['nombre']);
                $this->sesion->setFlashdata('correo', $data['correo']);
                $this->correoValidacion($res[1]);
                return redirect()->to('/Admision/Registrado');
            } else $data['error'] = $res[1];
        }
        return view('admision/recuperacceso', $data);
    }

    /**
     * recupera la liga de validación de un aspirante
     * @return linkvalid cuando ya está validado
     * @return linkinvalid cuando no esta validado
     * @return recuperacceso cuando los datos son erroneos
     */
    public function validarecacceso()
    {
        $data['carrera'] = ($this->request->getPost('carrera')) ? $this->request->getPost('carrera') : '';
        $data['carreras'] = $this->carrerasModel->activas();
        $data['curp'] = ($this->request->getPost('curp'));
        $data['apMaterno'] = ($this->request->getPost('apMaterno'));
        $data['carrera'] = ($this->request->getPost('carrera'));
        $posibleaspirante = $this->aspirantesModel->recuperaliga($data['curp'], $data['apMaterno'], $data['carrera']);
        if (count($posibleaspirante) == 1) {
            foreach ($posibleaspirante as $registro) {
                $band_validado = $registro->aspirante_validado;
                $folio = $registro->aspirante_id;
                $nombre = $registro->aspirante_nombre;
                $curp = $registro->aspirante_curp;
                $correo = $registro->aspirante_correo;
            }
            $data['nombre'] = $nombre;
            $data['folio'] = $folio;
            $data['curp'] = $curp;
            $data['correo'] = $correo;
            if ($band_validado == 1) {
                return view('admision/reclinkvalid', $data);
            } else {
                $data['cifrado'] = $this->cifrar($this->cifraPrefijo . $folio . $this->cifraSufijo);
                $data['enlace'] = base_url('Admision/Verifica') . '/' . $data['cifrado'];

                return view('admision/reclinkinvalid', $data);
            }
        } else {
            $data['error'] = 'Los datos del ingresados al formulario no proporcionan coincidencias';
            return view('admision/recuperacceso', $data);
        }
    }

    /**
     * funcion para renderizar fot de perfil del aspirante para la revision
     * @param aspirante id del aspirante
     * @param documento nombre del archivo que va a renderizar
     * retorna la imagen solicitada
     */
    public function renderImg($aspirante, $documento)
    {
        $foto =  WRITEPATH . 'admision/2022A/' . $aspirante . '/' . $documento;
        return $this->response->setContentType('image/jpg')->download($foto, null);
    }



    /**
     * muestra la vista que informa al aspirante que ha sido aceptado para 
     * @return band_tiempo si esta en un rango de fechas
     */
    public function aspiranteaceptado()
    {
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $aspiranteDatos = $this->aspirantesModel->find($this->sesion->get("aspirante"));
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
        $data['aspirante'] = $aspiranteDatos;
        $data['notificaciones'] = ($notificaciones) ? $notificaciones : '';
        $data['etapas'] = $this->getProgreso();
        $hoy = date_create(date("Y-m-d H:i:s"));
        $f_ficha_ini = date_create_from_format('d-m-Y', env('admision.ficha.fechaini')); //originalmente es para el 20
        $f_ficha_fin = date_create_from_format('d-m-Y', env('admision.ficha.fechafin'));
        if (($hoy >= $f_ficha_ini) and ($hoy <= $f_ficha_fin)) {
            return $band_tiempo = true;
        } else {

            return $band_tiempo = false;
        }
        //$data['band_tiempo'] = true;//$this->getProgreso();
        //return view('admision/aspiranteaceptado',$data);
    }

    /**
     * muestra la vista que informa al aspirante que ha sido aceptado para 
     * 
     */
    public function aspirantedeclinado()
    {
        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        $aspiranteDatos = $this->aspirantesModel->find($this->sesion->get("aspirante"));
        $notificaciones = $this->notificacionesModel->getNotificacionesAspirante($this->sesion->get('aspirante'));
        $data['aspirante'] = $aspiranteDatos;
        $data['notificaciones'] = ($notificaciones) ? $notificaciones : '';
        $data['etapas'] = $this->getProgreso();
        return view('admision/aspirantedeclinado', $data);
    }

    /**
     * obtiene el ID del siguiente formulario de una etapa, o en su defecto null si no existe otro
     * @param formulario el objeto App\Entities\Formulario con los datos del formulario actual
     * @return form el Id del formulario que sigue
     * @return false si no existe algún formulario siguiente
     */
    private function formHasNext($formulario)
    {
        $form = false;
        $formularios = $this->formulariosModel->getFromEtapa($formulario->etapa);
        $total = count($formularios);
        for ($i = 0; $i < $total; $i++) {
            if ($formularios[$i]->formulario == $formulario->formulario) {
                if (array_key_exists($i + 1, $formularios) === true) {
                    $form = $formularios[$i + 1]->formulario;
                    break;
                }
            }
        }
        return $form;
    }

    /**
     * obtiene todos los documentos que tienen observaciones y los almacena en un arreglo
     * @return observaciones un arreglo con las observaciones encontradas
     */
    private function getObservaciones()
    {
        $observaciones = $this->respModel->getObservaciones($this->sesion->get('aspirante'));
        return $observaciones;
    }

    /** obtiene los datos para la vista de la ficha de examen de admisión
     * 
     */
    public function fichaExamen()
    {

        if (!$this->validaSesion()) return redirect()->to($this->urlLogin);
        if ($this->aspiranteaceptado()){
            $aspiranteModel = new AspirantesGeneralesModel();
            $aspirantefolio = $this->sesion->get('aspirante');
            $aspiranteDatos = $aspiranteModel->aspirante_exitoso($aspirantefolio);
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'letter', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 0,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            if (!is_null($aspiranteDatos)) {
                try {
                    $campofoto = 52;
                    $estudiante = $aspiranteDatos->aspirante_id;
                    $this->updateDescarga($estudiante);
                    $arch_foto = $this->respuestasModel->contestado($campofoto, $aspiranteDatos->aspirante_id);
                    $data['folio'] = $estudiante;
                    $data['nombre'] = $aspiranteDatos->aspirante_nombre;
                    $data['ap_pat'] = $aspiranteDatos->aspirante_ap_paterno;
                    $data['ap_mat'] = $aspiranteDatos->aspirante_ap_materno;
                    $data['curp'] = $aspiranteDatos->aspirante_curp;
                    $data['carrera'] = $aspiranteDatos->aspirante_carrera;
                    $data['modalidad'] = "en linea"; 
                    $data['dia'] = $aspiranteDatos->aspirante_dia; 
                    $data['hora'] = $aspiranteDatos->aspirante_hora;
                    $data['usuario'] = $estudiante;
                    $data['pass'] = substr($aspiranteDatos->aspirante_curp, 0, 4);
                    $data['liga'] = env('admision.ficha.urlexamen');
                    $foto = WRITEPATH . 'admision/2022A/' . $estudiante . '/' . $arch_foto->respuesta_valor;
                    $data['foto'] = $foto;
                    $html = view('admision/ficha', $data);
                    $mpdf->WriteHTML($html);
                    $mpdf->Output($aspiranteDatos->aspirante_id . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    
                    die();
                    
                } catch (\Exception $x) {
                    log_message("error", "ERROR {exception}", ["exception" => $x]);
                }
                
                die();
            }
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
        else{
            return redirect()->to($this->urlLogin);
        }
        
    }

    /**
     * actualiza la bandera de descarga de la ficha
     * @param aspirante folio del aspirante
     */
    public function updateDescarga($aspirante)
    {
        $aspirante_general =  $this->aspirantesModel->find($aspirante);
        if ($aspirante_general) {
            try {
                $aspirante_general->ficha = 1;
                $this->aspirantesModel->update($aspirante,$aspirante_general);
            } catch (\Exception $x) {
                log_message("error", "ERROR {exception}", ["exception" => $x]);
            }
        }
        // die();
    }
}
