<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesComentarios extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'id' => null,
        'texto' => null,
        'tipo' => null,
        'campo' => null,
        'creado_al' => null,
        'actualizado_al' => null,
        'borrado_al' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id' => 'comentarios_id',
        'texto' => 'comentarios_texto',
        'campo' => 'comentarios_campo',
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}