<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .error {
        border: red 2px solid;
    }
</style>
<div class="container">
    <?php if ($mensaje) : ?>
        <div class="alert alert-dismissible fade show <?php echo $mensaje['tipo'] ?>" role="alert">
            <?php echo $mensaje['texto']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="datos text-center">
            <div class="datos text-center">
                <label style="margin: 0 10px">Nombre:<h4 class="card-title"> <?php echo $formulario->nombre;  ?></h4> </label>
                <label style="margin: 0 10px">Orden:<h4 class="card-title"> <?php echo $formulario->orden  ?></h4> </label>
                <label style="margin: 0 10px">Instrucciones:<h4 class="card-title"> <?php echo (($formulario->instrucciones) ? $formulario->instrucciones : '&nbsp');  ?></h4> </label>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="table-responsive-xl ">
            <table class="table table-striped-columns table-hover table-borderless align-middle " >
                <thead class="table-light">
                    <caption>Preguntas del formulario</caption>
                    <tr style="position: sticky;top: 0px; background-color:white;">
                        <th>#</th>
                        <th>Titulo</th>
                        <th>Tipo</th>
                        <th>Clasificación</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    <?php if ($preguntas) : ?>
                        <?php  // echo json_encode($formularios) 
                        ?>
                        <?php foreach ($preguntas as $pregunta) : ?>
                            <tr>
                                <td scope="row"><?php echo $pregunta->orden ?></td>
                                <td><?php echo $pregunta->titulo ?></td>
                                <td><?php
                                    foreach ($tipos as $tipo) {
                                        if ($tipo->id == $pregunta->tipo) {
                                            echo $tipo->nombre;
                                        }
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($clasificaciones as $clasificacion) {
                                        if ($clasificacion->id == $pregunta->clasificacion) {
                                            echo $clasificacion->nombre;
                                        }
                                    }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <div class="d-flex justify-content-end">
                                        <div>
                                            <div class="dropdown">
                                                <button class="btn btn-default btn-sm " type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                                                        <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                                    </svg>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="<?php echo base_url('EvaluacionDocenteController/DetallesPregunta/' . $pregunta->id) ?>">Editar</a></li>
                                                    <li>
                                                        <button type="button" class="dropdown-item" onclick="validar(this)" value="<?php echo  $pregunta->id . '|' . $pregunta->titulo ?>">Eliminar</button>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td class="text-center" colspan="4">No hay preguntas pertenecientes a este formulario</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <div class="m-2">
                        <form action="<?php echo base_url('EvaluacionDocenteController/AgregarPregunta/' . $formulario->id) ?>" method="post">
                            <button type="submit" class="btn btn-secondary">Agregar Pregunta</button>
                        </form>
                        <a class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Formularios/' . $formulario->evaluacion) ?>" role="button">Regresar atras</a>
                    </div>
                </tfoot>
            </table>
        </div>

    </div>
</div>
<form action="<?php echo base_url('EvaluacionDocenteController/EliminarPregunta/' . $formulario->id) ?>" method="post" id="form_eliminar">

</form>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function validar(datos) {
        console.log(datos.value);
        var pregunta = datos.value.split('|');
        Swal.fire({
            'title': 'Confirmación',
            'html': '¿Seguro que desea eliminar la pregunta "' + pregunta[1] + '"?',
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': 'Se ha eliminado la pregunta "' + pregunta[1] + '"?',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                var form = document.getElementById('form_eliminar');
                var input = document.createElement('input');
                input.value = pregunta[0];
                input.name = 'pregunta';
                input.hidden = true;
                form.appendChild(input);
                form.submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>