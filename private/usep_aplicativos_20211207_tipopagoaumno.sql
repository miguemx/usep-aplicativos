-- primero permitir nulos en matriculas de pagos para los ROS

ALTER TABLE `pagos_ros` CHANGE `ros_matricula` `ros_matricula` VARCHAR(9) NULL;

-- modificar la tabla de permisos
ALTER TABLE `alumnos_pagos` ADD `pago_tipo` ENUM('EXTRAORDINARIO','RECURSO','INSCRIPCION','REINSCRIPCION') NOT NULL AFTER `pago_comprobante`;

