<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Permiso extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'rol' => null,
        'modulo' => null,
        'funcion' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'        => "permiso_id",
        'rol'       => "permiso_rol",
        'modulo'    => "permiso_modulo",
        'funcion'   => "permiso_funcion",
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
