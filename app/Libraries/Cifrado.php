<?php 

namespace App\Libraries;

class Cifrado {

    /**
	 * cifra una cadena 
	 * @param cadena la cadena a cifrar
	 * @return cifrado la cadena cifrada
	 */
	public static function cifrar( $cadena ) {
		$config         = new \Config\Encryption();
		$config->key    = env('crypt.llave');
		$config->driver = env('crypt.algoritmo');

		$encrypter = \Config\Services::encrypter($config);
		$ciphertext = $encrypter->encrypt($cadena);
		$cifrado = base64_encode( $ciphertext );
		$cifrado = str_replace( '+', '_MAS_', $cifrado );
		$cifrado = str_replace( '/', '_DIA_', $cifrado );
		return $cifrado;
	}

	/**
	 * descifra una cadena
	 * @param cifrado la cadena cifrada
	 * @return cadena la cadena descifrada
	 * @return null en caso de error en el descifrado
	 */
	public static function descifrar ( $cifrado ) {
		$cadena = null;

		$cifrado = str_replace( '_MAS_', '+', $cifrado );
		$cifrado = str_replace( '_DIA_', '/', $cifrado );

		$config         = new \Config\Encryption();
		$config->key    = env('crypt.llave');
		$config->driver = env('crypt.algoritmo');
		$encrypter = \Config\Services::encrypter($config);

		try {
			$ciphertext = base64_decode( $cifrado );
			$cadena = $encrypter->decrypt($ciphertext);
		}
		catch(\CodeIgniter\Encryption\Exceptions\EncryptionException $ex) {
			$this->errorMessage = 'No se puede encontrar descifrar el valor. '.$ex->getMessage();	
		}
		return $cadena;
	}

}