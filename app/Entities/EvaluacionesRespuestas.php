<?php

namespace App\Entities;

use CodeIgniter\Entity;

class EvaluacionesRespuestas extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [

        'id' => null,
        'matricula' => null,
        'grupo' => null,
        'pregunta' => null,
        'valor' => null,
        'comentario' => null,
        'creado_al' => null,
        'actualizado_al' => null,
        'borrado_al' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos

    protected $datamap = [

        'id' => 'respuestas_id',
        'matricula' => 'respuestas_matricula',
        'grupo' => 'respuestas_grupo',
        'pregunta' => 'respuestas_pregunta',
        'valor' => 'respuestas_valor',
        'comentario' => 'respuestas_comentario',
    
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
