<div>
    <img src="<?php echo base_url('img/credencial/Logo_USEP-01.png'); ?>" style="max-height: 50px" />
    <p>
        &iexcl;Bienvenido(a) <strong><?php echo $nombre ?></strong>!
    </p>
    <p>
        Ha dado el primer paso para convertirse en aspirante a la 
        <strong>&quot;Universidad de la Salud&quot;</strong> del Estado de Puebla.
    </p>
    <p>
        Para continuar con su proceso de admisión, es necesario que valide su dirección de correo electrónico
        y genere su folio de acceso, dando clic <a href="<?php echo $enlace ?>" style ="font-size: 18px;">aquí</a>.
    </p>
    
    <p>
        Le deseamos éxito en su proceso.
    </p>
    <p style="font-size: 8px;">
        <?php // echo $cifrado; ?>
    </p>
</div>