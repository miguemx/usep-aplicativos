<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <h4>Lista de pagos reportados</h4>
    <hr />

    <?php if( isset($mensaje) ): ?>
        <div class="alert alert-success">
            <?php echo $mensaje; ?>
        </div>
    <?php endif; ?>
    
    <?php if( isset($errors) ): ?>
        <div class="alert alert-danger">
            <?php foreach( $errors as $error ): ?>
                <?php echo $error; ?> <br />
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <form method="get" action="<?php echo base_url('Pagos/Listado'); ?>">
        <div class="col-sm-12" style="text-align: right;">
            <a href="<?php echo base_url('Pagos')  ?>" class="btn btn-secondary btn-sm">
                Todos
            </a>
            <button type="submit" class="btn btn-secondary btn-sm">
                Buscar
            </button>
        </div>
        <div class="row">
            <div class="col">
                <label for="pagosros" class="form-label">Matrícula</label>
                <input type="text" class="form-control form-control-sm" id="matricula" name="matricula" value="<?php echo $matricula; ?>">
            </div>
            <div class="col">
                <label for="pagosros" class="form-label">Referencia</label>
                <input type="text" class="form-control form-control-sm" id="referencia" name="referencia" value="<?php echo $referencia; ?>">
            </div>
            <div class="col">
                <label for="pagosros" class="form-label">Status</label>
                <select name="status" class="form-select form-select-sm">
                    <option value="">--Seleccione--</value>
                    <option value="ACEPTADO" <?php if($status=='ACEPTADO') echo 'selected="selected"'; ?>>ACEPTADO</value>
                    <option value="EN PROCESO" <?php if($status=='EN PROCESO') echo 'selected="selected"'; ?>>EN PROCESO</value>
                    <option value="RECHAZADO" <?php if($status=='RECHAZADO') echo 'selected="selected"'; ?>>RECHAZADO</value>
                </select>
            </div>
            <div class="col">
                <label for="pagosros" class="form-label">Tipo</label>
                <select name="tipo" class="form-select form-select-sm">
                    <option value="">--Seleccione--</value>
                    <option value="EXTRAORDINARIO" <?php if($tipo=='EXTRAORDINARIO') echo 'selected="selected"'; ?>>EXTRAORDINARIO</value>
                    <option value="INSCRIPCION" <?php if($tipo=='INSCRIPCION') echo 'selected="selected"'; ?>>INSCRIPCION</value>
                    <option value="RECURSO" <?php if($tipo=='RECURSO') echo 'selected="selected"'; ?>>RECURSO</value>
                    <option value="REINSCRIPCION" <?php if($tipo=='REINSCRIPCION') echo 'selected="selected"'; ?>>REINSCRIPCION</value>
                </select>
            </div>

            <div class="col">
                <label for="pagosros" class="form-label">Fecha</label>
                <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" value="<?php echo $fecha; ?>">
            </div>
        </div>
        
    </form> 

    <hr />

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Matrícula</th>
                <th>Tipo</th>
                <th>Referencia</th>
                <th>Monto</th>
                <th>Status</th>
                <th>Materia (cuando aplique)</th>
                <th>Concepto</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $pagos as $pago ): ?>
                <tr>
                    <td><?php echo $pago->fecha; ?></td>
                    <td><?php echo $pago->alumno; ?></td>
                    <td><?php echo $pago->tipo; ?></td>
                    <td><?php echo $pago->referencia; ?></td>
                    <td><?php echo $pago->monto; ?></td>
                    <td><?php echo $pago->status; ?></td>
                    <td><?php echo $pago->materia; ?></td>
                    <td><?php echo $pago->conceptoTexto; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div>
        <?php echo $pager->links(); ?>
    </div>
</div>


<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>



<?php echo $this->endSection() ?>