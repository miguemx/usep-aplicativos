<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<div class="container ">
    <?php if (isset($rol)) : ?>
        <input hidden id='habilitar' type="text" value= "<?php echo $rol;?>">
    <?php endif; ?>
    <?php if (isset($mensaje)) : ?>
        <div class="alert alert-danger" role="alert">
        <?php echo $mensaje; ?>
    <?php endif; ?>
    <head>
        <!-- links para exportar a excel -->
        <script src="https://unpkg.com/xlsx@0.16.9/dist/xlsx.full.min.js"></script>
        <script src="https://unpkg.com/file-saverjs@latest/FileSaver.min.js"></script>
        <script src="https://unpkg.com/tableexport@latest/dist/js/tableexport.min.js"></script>
    </head>
        <style>
        .nombre{
            width: 800px;
        }
        table td:first-child{
        width: 100px;
        }
        .piedepagina2{
            padding-top: 0;
            padding-left: 29px;
            padding-right: -510px;
            padding-bottom: 0px;
            width: 100%;
            font-size:15;
        }
    </style>
    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
        <div class="row ">
            <h1 class="text-center my-2">Reporte de asistencias</h1>
            <div class="accordion accordion-flush mt-2" id="menu_estadisticcas">
                <form action="<?php echo base_url('docente/buscaAsistenciasReporte'); ?>" method="post">
                    <div class="row">
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">FECHA DE INICIO</span>
                            <input name="fechainicio" type="date" id="fechainicio" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $fechainicio?>" required>
                        </div>
                        <div class="input-group mb-3 col">
                            <span class="input-group-text" id="inputGroup-sizing-default">FECHA FINAL</span>
                            <input name="fechafin" type="date" id="fechafin" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="<?php echo $fechafinal?>" required>
                        </div>
                        <div class="input-group mb-3 col">
                            <select id="selectdocente" class="custom-select" style="width: 80%;" name="selectdocente" onchange="getMaterias()" required>
                                <option value = "">Selecciona Docente</option>
                                <?php foreach ($docentes as $arreglodocente) : ?>
                                    <option value = <?php echo $arreglodocente->grupo_docente ?> <?php if($docente == $arreglodocente->grupo_docente) echo 'selected="selected"'; ?>><?php
                                        echo $arreglodocente->grupo_docente.' - '.$arreglodocente->nombre.' '.$arreglodocente->empleado_ap_paterno.' '.$arreglodocente->empleado_ap_materno
                                    ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input hidden name="docenteid" type="text" id="docenteid" value="<?php echo $docente?>">
                        </div>
                        <?php if (isset($jefatura)) : ?>
                            <div class="input-group mb-3 col">
                                <select class="form-select" name="selectgrupo" id="selectgrupo" required>
                                </select>
                                <input hidden name="grupoid" type="text" id="grupoid" value="<?php echo $grupo?>">
                            </div>
                        <?php endif; ?>
                        <?php if (isset($rol)) : ?>
                            <div class="input-group mb-3 col">
                            <select id="selectgrupo" class="custom-select" style="width: 80%;" name="selectgrupo" required>
                                <option value = "">Selecciona Grupo</option>
                                <?php foreach ($grupos as $arreglogrupos) : ?>
                                    <option value = <?php echo $arreglogrupos->seccion ?> <?php if($grupo == $arreglogrupos->seccion) echo 'selected="selected"'; ?>><?php
                                        echo $arreglogrupos->seccion.' - '.$arreglogrupos->materia_nombre
                                    ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input hidden name="grupoid" type="text" id="grupoid" value="<?php echo $grupo?>">
                            </div>
                        <?php endif; ?>
                        <br>
                        <br>
                        <div class="text-center mt-1">
                            <button type="submit" class="btn btn-secondary" onclick="habilitar()">Generar</button>
                        </div>
                    </div>
                </form>
            <?php if (isset($tabla)) : ?>
                <button id="btnExportar" class="btn btn-success" onclick="exportarexcel()">Exportar</button>
                <div id='tabla'>
                    <div class="container-fluid">
                        <table id='tabla' class="table table-striped table-bordered table-responsive">
                            <thead style="background-color: #84112c  ; color:white; border: 1px solid black;">
                                <tr class="something">
                                    <th>NO</th>
                                    <th>MATRICULA</th>
                                    <th class="text-center" style="min-width:350px; max-width=350px">NOMBRE COMPLETO</th>
                                <?php foreach($catalogofechas as $fecha): ?>
                                    <th class="text-center" style="min-width:150px; max-width=150px"><?php echo $fecha?></th>
                                <?php endforeach; ?>
                                    <th>ASISTENCIAS</th>
                                    <th>INASISTENCIA</th>
                                    <th>RETARDO</th>
                                    <th class="text-center">FALTA JUSTIFICADA</th>
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $contador = 0;
                                foreach ($asistencias as $asistencia) :?>
                                    <tr>
                                        <td><?php echo $contador = $contador + 1; ?></td>
                                        <td><?php echo $asistencia['matricula']?></td>
                                        <td ><?php echo $asistencia['nombre']?></td>
                                        <?php foreach($asistencia['tipoasistencia'] as $asistencia_tipo => $tipo): ?>
                                            <?php foreach($tipo as $registro): ?>
                                                <td class="text-center mt-100"><?php echo $registro ?></td>
                                            <?php endforeach; ?>
                                        <?php endforeach;?>
                                        <td class="text-center"><?php echo $asistencia['asistencias']?></td>
                                        <td class="text-center"><?php echo $asistencia['inasistencias']?></td>
                                        <td class="text-center"><?php echo $asistencia['retardo']?></td>
                                        <td class="text-center"><?php echo $asistencia['faltajustificada']?></td>
                                        <td class="text-center"><?php echo $asistencia['total']?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    <div>
                <div>
            <?php endif; ?>
        </div>
    </div>
<!-- script para exportar a excel -->
<script>

    var getMaterias = async () => {
        
        let id_docente = document.getElementById('selectdocente').value;
        console.log(id_docente);
        const formData = new FormData();
        formData.append('tipo_campo', id_docente);
        document.getElementById('selectgrupo').innerHTML = '';

        let response = await fetch('<?php echo base_url("Docente/getmateriasxdocente") ?>' + '/' + id_docente, {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {
            return false;
        });
        if (response) {
            document.getElementById('selectgrupo').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'Seleccione una opcion';
            document.getElementById('selectgrupo').appendChild(option);
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].seccion;
                option.innerHTML = response[i].seccion+' - '+response[i].materia_nombre;
                document.getElementById('selectgrupo').appendChild(option);
            }
        } else {
            document.getElementById('selectgrupo').innerHTML = '';
            let option = document.createElement('option');
            option.value = "";
            option.innerHTML = 'No hay opciones disponibles';
            document.getElementById('selectgrupo').appendChild(option);
        }
    }
    getMaterias();
    function exportarexcel() {
        grupo = document.getElementById('selectgrupo').value;
        docente = document.getElementById('docenteid').value;
        fechafin = document.getElementById('fechafin').value;

        const $btnExportar = document.querySelector("#btnExportar"),
            $tabla = document.querySelector("#tabla");
            let tableExport = new TableExport($tabla, {
                exportButtons: false, // No queremos botones
                filename: "listamensual-"+fechafin+'-'+grupo+'-'+docente, //Nombre del archivo de Excel
                sheetname: "lista mensual-"+docente+'-'+'('+grupo+')',//Título de la hoja
            });
            let datos = tableExport.getExportData();
            let preferenciasDocumento = datos.tabla.xlsx;
            tableExport.export2file(preferenciasDocumento.data, preferenciasDocumento.mimeType, 
            preferenciasDocumento.filename, preferenciasDocumento.fileExtension, preferenciasDocumento.merges, preferenciasDocumento.RTL, 
            preferenciasDocumento.sheetname);
        
    }
    function deshabilitar() {
                habilitar = document.getElementById('habilitar').value;
                console.log(habilitar);
                if(habilitar != null){
                    document.getElementById("selectdocente").disabled = true;
                }
            }
            deshabilitar();
</script>
<?php echo $this->endSection() ?>