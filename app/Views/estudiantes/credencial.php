<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
   <div>
      &nbsp;
   </div>
   <?php if(isset($error)): ?>
      <div class="col-sm-12 alert alert-danger" role="alert">
         En este momento no cuentas con el beneficio de la credencial digital.<br />
         Si crees que existe algún problema, por favor contacta a 
         <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a>
      </div>
   <?php else: ?>
      <div class="container">
         <div>
         Te presentamos tu “Credencial Institucional Digital”. Con ella orgullosamente podrás identificarte como 
         estudiante de USEP; este documento digital cuenta con validez oficial, ya que se genera a partir de los 
         datos concentrados en los archivos que obran en la institución y cuenta con un código QR que permite la 
         validación electrónica. 
            
         </div>
         <div>
            <br />
            Para descargar e imprimir tu credencial, haz clic 
            <a href="<?php echo base_url('Estudiantes/ImprimeTuCredencial'); ?>" target="_blank">aquí</a>
            <br /><br />
         </div>
         <?php echo $this->include('estudiantes/disenocredencial'); ?>
         <div> Recuerda que en periodo de veda electoral, nuestro sitio sufre algunas modificaciones, pero al
               <b>descargar</b> tu credencial, esta contará con el diseño habitual.
      </div>
      
   <?php endif; ?>

   
</div>

<?php echo $this->endSection() ?>