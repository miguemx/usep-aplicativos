<?php
namespace App\Entities;

use CodeIgniter\Entity;

class VotacionPadron extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'correo' => null,
        'voto_abstencion'=> null,
        'proceso'=> null,
        'tipopadron'=> null
        
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'padron_id',
        'correo' => 'padron_correo',
        'voto_abstencion'=> 'padron_voto_abstencion',
        'proceso'=> 'padron_proceso',
        'tipopadron'=> 'padron_tipovotante'
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];

}
