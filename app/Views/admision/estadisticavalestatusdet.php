<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<div class="container ">
<div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
  <div class="card-header">
    <h1 style="display:flex; justify-content:center">Estadística de Aspirantes con folio y género</h1>
  </div>
  <div class="card-body">
    <figure class="highcharts-figure">
      <div id="container"></div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Estado del Folio</th>
            <th>Mujeres de Enfermería</th>
            <th>Hombres de Enfermería</th>
            <th>Mujeres de Medicina</th>
            <th>Hombres de Medicina</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          
          foreach ($estadistica as $estadisticas) :?>
            <tr>
              <td><?php if($estadisticas->aspirante_validado==1) {echo "Aspirantes con Folio";} else {echo "Aspirantes sin Folio";}?></td>
              <td><?php echo $estadisticas->m_enf; ?></td>
              <td><?php echo $estadisticas->h_enf; ?></td>
              <td><?php echo $estadisticas->m_med; ?></td>
              <td><?php echo $estadisticas->h_med; ?></td>
              <td><b><?php echo $estadisticas->m_enf+$estadisticas->h_enf+$estadisticas->m_med+$estadisticas->h_med; ?></b></td>
              
            </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </figure>
  </div>
</div>
</div>    

<div class="container ">
  <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="card-header">
      <h1 style="display:flex; justify-content:center">Estatus de aspirantes por género</h1>
    </div>
    <div class="card-body">
      <figure class="highcharts-figure">
          <div id="container_detail"></div>
            <table id="datatable">
              <thead>
                <tr>
                  <th>Estatus</th>
                  <th>Mujeres de Enfermería</th>
                  <th>Hombres de Enfermería</th>
                  <th>Mujeres de Medicina</th>
                  <th>Hombres de Medicina</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($est_st_det as $est_det) :?>
                  <tr>
                    <td><?php echo $est_det->aspirante_estatus?></td>
                    <td><?php echo $est_det->mujeres_enf?></td>
                    <td><?php echo $est_det->hombres_enf?></td>
                    <td><?php echo $est_det->mujeres_med?></td>
                    <td><?php echo $est_det->hombres_med?></td>
                    <td><b><?php echo $est_det->mujeres_enf+$est_det->hombres_enf+$est_det->mujeres_med+$est_det->hombres_med?><b></td>
                  </tr>
                <?php endforeach;?>
              </tbody>
            </table>
      </figure>
    </div>
  </div>
</div>
<script>
   Highcharts.chart('container', {

chart: {
  type: 'column'
},

title: {
  text: 'Aspirantes por Género y Carrera'
},

xAxis: {
  categories: ['Aspirantes sin folio','Aspirantes con folio'
]
},

yAxis: {
  allowDecimals: false,
  min: 0,
  title: {
    text: 'Cantidad de Aspirantes'
  }
},

tooltip: {
  formatter: function () {
    return '<b>' + this.x + '</b><br/>' +
      this.series.name + ': ' + this.y + '<br/>' +
      'Total: ' + this.point.stackTotal;
  }
},

plotOptions: {
  column: {
    stacking: 'normal'
  }
},
series: [{
  name: 'Mujeres de Enfermería',
  data: [<?php
    foreach ($estadistica as $est_det) :
          echo $est_det->m_enf.",";
        endforeach; ?>
  ],
  color: '#B37D0B',
  stack: 'Enfermeria'
}, {
  name: 'Hombres de Enfermería',
  data: [
    <?php
        foreach ($estadistica as $est_det) :
          echo $est_det->h_enf.",";
        endforeach; ?>
  ],
  color: '#051F03',
  stack: 'Enfermeria'
}, {
  name: 'Mujeres de Medicina',
  data: [
    <?php
        foreach ($estadistica as $est_det) :
          echo $est_det->m_med.",";
        endforeach; ?>
  ],
  color: '#630505',
  stack: 'Medicina'
}, {
  name: 'Hombres de Medicina',
  data: [
    <?php
        foreach ($estadistica as $est_det) :
          echo $est_det->h_med.",";
        endforeach; ?>
  ],
  color: '#5C5151',
  stack: 'Medicina'
}]
});

Highcharts.chart('container_detail', {

chart: {
  type: 'column'
},

title: {
  text: 'Aspirantes por Género y Carrera'
},

xAxis: {
  categories: ['EXITOSO','OBSERVACIONES','DECLINADO','ESPERA','REVISION','REGISTRO',
]
},

yAxis: {
  allowDecimals: false,
  min: 0,
  title: {
    text: 'Cantidad de Aspirantes'
  }
},

tooltip: {
  formatter: function () {
    return '<b>' + this.x + '</b><br/>' +
      this.series.name + ': ' + this.y + '<br/>' +
      'Total: ' + this.point.stackTotal;
  }
},

plotOptions: {
  column: {
    stacking: 'normal'
  }
},

series: [{
  name: 'Mujeres de Enfermería',
  data: [<?php
    foreach ($est_st_det as $est_det) :
          echo $est_det->mujeres_enf.",";
        endforeach; ?>
  ],
  color: '#B37D0B',
  stack: 'Enfermeria'
}, {
  name: 'Hombres de Enfermería',
  data: [
    <?php
        foreach ($est_st_det as $est_det) :
          echo $est_det->hombres_enf.",";
        endforeach; ?>
  ],
  color: '#051F03',
  stack: 'Enfermeria'
}, {
  name: 'Mujeres de Medicina',
  data: [
    <?php
        foreach ($est_st_det as $est_det) :
          echo $est_det->mujeres_med.",";
        endforeach; ?>
  ],
  color: '#630505',
  stack: 'Medicina'
}, {
  name: 'Hombres de Medicina',
  data: [
    <?php
        foreach ($est_st_det as $est_det) :
          echo $est_det->hombres_med.",";
        endforeach; ?>
  ],
  color: '#5C5151',
  stack: 'Medicina'
}]
});

  let creditos = document.getElementsByClassName('highcharts-credits');
    creditos[0].childNodes[0].data = '';
    creditos[1].childNodes[0].data = '';
</script>
<?php echo $this->endSection() ?>