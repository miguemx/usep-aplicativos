INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'getdocentesdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'cambiodocentegrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '9', 'programacionacademica', 'updatedocentegrupo');


INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'getdocentesdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'cambiodocentegrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '10', 'programacionacademica', 'updatedocentegrupo');

UPDATE `aplicativo_roles` SET `rol_nombre` = 'DIRECCION ACADEMICA' WHERE `aplicativo_roles`.`rol_id` = 17;
UPDATE `aplicativo_roles` SET `rol_menu` = 'academica' WHERE `aplicativo_roles`.`rol_id` = 17;


INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'getdocentesdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'cambiodocentegrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'updatedocentegrupo');


INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'menu');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'mapaaulas');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'seleccionargrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'eliminargrupoHora');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'editargrupohora');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'editargrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'asignarhoramateria');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'main');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'aulavirtual');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'pophorariovirtual');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'getlistadohorario');


INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'horariodocente');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'horariosecciones');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'getaulasdisponibles');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'programacionacademica', 'cambioaulas');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '17', 'bibliotecas', '*'), (NULL, '17', 'calificaciones', '*'), (NULL, '17', 'estudiantes', '*'), (NULL, '17', 'home', '*'), (NULL, '17', 'renderfiles', '*'), (NULL, '17', 'docente', '*'), (NULL, '17', 'escolar', 'listaescolar'), (NULL, '17', 'escolar', 'consultatablalista'), (NULL, '17', 'escolar', 'generalistapdf')