<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>&nbsp;</div>

    <?php if(isset($error)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php if(isset($exito)): ?>
        <div class="col-sm-12 alert alert-success" role="alert">
            <?php echo $exito; ?>
        </div>
    <?php endif; ?>

    <div class="container">
        <form method="post" action="">
            <label for="tipo" class="form-label">Selecciona el tipo de constancia que deseas generar.</label>
            <select class="form-select form-select-sm" aria-label="Seleccion" id="tipo" name="tipo">
                <option value="SIMPLE">Simple</option>
                <!-- <option value="CALIFICACIONES">Constancia con Calificaciones</option> -->
            </select>
            <button type="submit" class="btn btn-secondary btn-sm" name="gce" value="ok">Generar constancia</button>
        </form>
        <h5>Descarga tus constancias previas</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Periodo</th>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>&nbsp;</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php foreach( $constancias as $constancia ): ?>
                        <tr>
                            <td><?php echo $constancia->folio; ?></td>
                            <td><?php echo $constancia->periodo; ?></td>
                            <td><?php echo $constancia->fechaExp; ?></td>
                            <td><?php echo $constancia->tipo; ?></td>
                            <td>
                                <form method="post" action="<?php echo base_url('Estudiantes/imprimeConstancia'); ?>" target="_blank">
                                    <input type="hidden" name="ceid" value="<?php echo $constancia->id; ?>"  />
                                    <button type="submit" class="btn btn-secondary btn-sm">Descargar</button>
                                </form>
                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
   
</div>

<?php echo $this->endSection() ?>
