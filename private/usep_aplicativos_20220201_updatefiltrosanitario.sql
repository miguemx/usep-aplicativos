ALTER TABLE filtro_sanitario ADD COLUMN filtro_fechapruebapositiva date AFTER filtro_menorotro;
ALTER TABLE filtro_sanitario ADD COLUMN filtro_fechapruebanegativo date AFTER filtro_fechapruebapositiva;


ALTER TABLE filtro_sanitario ADD COLUMN filtro_fechacontactocovid date AFTER filtro_fechapruebanegativo;
UPDATE `filtro_sanitario` SET `filtro_fechacontactocovid` = CONCAT(`filtro_menorcovid`,`filtro_menorcovid`,`filtro_menorcovid`,`filtro_menorcovid`,
'-',`filtro_menorcovid`,`filtro_menorcovid`,'-',`filtro_menorcovid`,`filtro_menorcovid`)