ALTER TABLE `alumnos_generales` ADD `alumno_periodo` VARCHAR(7) NULL AFTER `alumno_else_regpass`;

ALTER TABLE `alumnos_generales` ADD FOREIGN KEY (alumno_periodo) REFERENCES aca_periodos(periodo_id);

UPDATE alumnos_generales SET alumno_periodo='2021A';