<?php

use Google\Service\SQLAdmin\Resource\Flags;

echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
<style>
    .error {
        border: red 2px solid;
    }

    datalist {
        width: 50000px;
        height: 80px;
    }

    .icono-1:hover {
        background-color: #4dff4d;
        /* Green */
        color: white;
        /* border: 2px solid red; */
    }

    .boton-agregar {
        background-color: #ceccb5;
    }

    .filtro_item {
        display: inline-block;
        /* width: 10%; */
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1;
        color: #495057;
        /* background-color: #fff; */
        /* background-clip: padding-box; */
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        /* transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out; */
    }

    .contendor_colapse {
        height: 20vh;
    }
</style>
<div class="container ">
    <div class="bg-white rounded shadow p-1 m-4 ">
        <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        Buscar espacios disponibles
                    </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">

                        <form action="<?php echo base_url('ProgramacionAcademica/EspaciosDisponibles'); ?>" method="post" id="EspaciosDisponibles">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="select_periodo" class="form-label">Periodo</label>
                                        <select class="form-select" name="select_periodo" id="select_periodo" onchange="getDescripccion(this)">
                                            <option value="0">Seleccione un periodo</option>
                                            <?php foreach ($periodos as $lista_periodo) : ?>
                                                <option value="<?php echo $lista_periodo->id ?>" <?php echo (($periodo_selected == $lista_periodo->id) ? 'selected' : ''); ?>><?php echo $lista_periodo->id ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <small></small>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="select_dia" class="form-label">Dia</label>
                                        <select class="form-select form-select-sm" name="select_dia" id="select_dia">
                                            <option selected value="0">Seleccione un dia disponible</option>
                                            <?php foreach ($dias as $dia) : ?>
                                                <option value="<?php echo $dia ?>" <?php echo (($dia_selected == $dia) ? 'selected' : ''); ?>><?php echo $dia ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col">
                                    <!-- bloque para filtrado de aulas -->
                                    <div class="mb-3">
                                        <label for="btn_despliegue" class="form-label">&nbsp;</label>
                                        <button id="btn_despliegue" class="form-select form-select-sm" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Filtro de aulas
                                        </button>
                                    </div>
                                    <div>
                                        <div class="collapse " id="collapseExample">
                                            <div>
                                                <input type="text" id="myInput" onkeyup="buscar_enlista(this)" class="form-control" placeholder="Bucar aula en el catalogo">

                                            </div>
                                            <div class="card card-body overflow-auto contendor_colapse">
                                                <div class="form-check ">

                                                    <input class="form-check-input" id="btn_activar_todo" type="checkbox" onclick="activar_todo(this)" name="btn">
                                                    <label class="form-check-label" for="btn_activar_todo" id="label_btn">
                                                        Seleccionar todo
                                                    </label>
                                                </div>
                                                <?php foreach ($aulas as $aula) : ?>
                                                    <div class="form-check" id="<?php echo $aula->id_aula . "_contenedor_checkbox" ?>">
                                                        <input class="form-check-input" name=" <?php echo $aula->nombre_aula ?>" type="checkbox" id="<?php echo $aula->id_aula ?>" onclick="agregarfiltro(this,'<?php echo $aula->nombre_aula ?>','<?php echo $aula->id_aula ?>')">
                                                        <label class="form-check-label" for="<?php echo $aula->id_aula  ?>">
                                                            <?php echo $aula->nombre_aula ?>
                                                        </label>
                                                    </div>
                                                <?php endforeach; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        .cont_filtro {
                                            border: 1px grey solid;
                                            padding: 6px;
                                        }
                                    </style>

                                    <script>
                                        function agregarfiltro(checkbox, nombre, id) {
                                            if (checkbox.checked == true) {
                                                var contenedor = document.getElementById('filtros');
                                                var div_contenedor = document.createElement('div');
                                                div_contenedor.classList.add('btn', 'cont_filtro');
                                                div_contenedor.id = 'item_' + id;
                                                div_contenedor.name =id;
                                                div_contenedor.innerHTML = nombre;

                                                var input = document.createElement('input');
                                                input.value = id;
                                                input.name = 'filtro_aulas[]';
                                                input.hidden = true;
                                                div_contenedor.appendChild(input);

                                                var boton = document.createElement('button');
                                                boton.type = 'button';
                                                boton.classList.add('btn-close');
                                                boton.setAttribute('onclick', 'borrar_item(item_' + id + ')');
                                                div_contenedor.appendChild(boton);
                                                contenedor.appendChild(div_contenedor);
                                            } else {
                                                console.log('apagado');
                                                var element = document.getElementById("filtros");
                                                var child = document.getElementById('item_' + id);
                                                element.removeChild(child);
                                            }
                                        }

                                        function borrar_item(item) {
                                            // console.log(item.id);
                                            var element = document.getElementById("filtros");
                                            // var child = document.getElementById();
                                            // var id = item.id.split('_');
                                            // console.log(id[1]);
                                            document.getElementById(item.name).checked = false;
                                            element.removeChild(item);
                                        }

                                        function buscar_enlista(filtro) {
                                            var buscador = filtro.value.toUpperCase();
                                            var lista = document.querySelectorAll('input[type=checkbox]');
                                            lista.forEach(checkbox => {
                                                if (checkbox.name != 'btn') {
                                                    var txtValue = checkbox.name;
                                                    if (txtValue.toUpperCase().indexOf(buscador) > -1) {
                                                        document.getElementById(checkbox.id + '_contenedor_checkbox').hidden = false;
                                                        document.getElementById(checkbox.id).hidden = false;
                                                    } else {
                                                        document.getElementById(checkbox.id + '_contenedor_checkbox').hidden = true;
                                                        document.getElementById(checkbox.id).hidden = true;
                                                    }
                                                }
                                            });
                                        }

                                        function activar_todo(boton) {
                                            var lista = document.querySelectorAll('input[type=checkbox]');
                                            var label = document.getElementById('label_btn');
                                            if (boton.checked == true) {
                                                label.innerHTML = 'Desmarcar todo';
                                                lista.forEach(objeto => {
                                                    if (objeto.name != 'btn') {
                                                        if (objeto.hidden == false) {

                                                            objeto.checked = true;

                                                            var contenedor = document.getElementById('filtros');
                                                            var div_contenedor = document.createElement('div');
                                                            div_contenedor.classList.add('btn', 'cont_filtro');
                                                            div_contenedor.id = 'item_' + objeto.id;
                                                            div_contenedor.name = objeto.id;
                                                            div_contenedor.innerHTML = objeto.name;

                                                            var input = document.createElement('input');
                                                            input.value = objeto.id;
                                                            input.name = 'filtro_aulas[]';
                                                            input.hidden = true;
                                                            div_contenedor.appendChild(input);

                                                            var boton = document.createElement('button');
                                                            boton.type = 'button';
                                                            boton.classList.add('btn-close');
                                                            boton.setAttribute('onclick', 'borrar_item(item_' + objeto.id + ')');
                                                            div_contenedor.appendChild(boton);
                                                            contenedor.appendChild(div_contenedor);
                                                        }
                                                    }
                                                });
                                            } else {
                                                label.innerHTML = 'Seleccionar todo';

                                                var contenedor = document.getElementById('filtros');
                                                contenedor.innerHTML = '';
                                                lista.forEach(objeto => {
                                                    if (objeto.name != 'btn') {
                                                        objeto.checked = false;
                                                    }
                                                });
                                            }
                                        }

                                        function enviar() {
                                            document.getElementById('form_busqueda').submit();
                                        }
                                    </script>
                                    <!-- fin del bloque para busqueda de aulas -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center" id="filtros">
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center mt-3">
                                    <button type="button" class="btn btn-secondary" onclick="buscar()">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function buscar() {
        var periodo = document.getElementById('select_periodo');
        var dia = document.getElementById('select_dia');
        if (periodo.value !== '0' && dia.value !== '0') {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
            Toast.fire({
                icon: 'success',
                title: 'Buscando.'
            })
            document.getElementById('EspaciosDisponibles').submit();

        } else {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'info',
                title: 'Selecciona un día y un periodo valido para realizar la búsqueda.'
            })
        }
    }
</script>

<style>
    .contenedor_tabla {
        width: 80hw;
        overflow-x: scroll;
    }

    .container_table {
        height: 73vh;
    }

    .bloque {
        border: #5c5c63 1px solid;
        border-radius: 5px;
    }
    .contenido{
        font-size: 12px;
    }
</style>
<div class="overflow-auto">
    <div class="container-fluid container_table">
        <?php if ($dia_selected != false && $periodo_selected != false) : ?>
            <div class="text-center">
                <label style="margin: 0 10px">Dia:<h4 class="card-title"> <?php echo $dia_selected;  ?></h4> </label>
                <label style="margin: 0 10px">Periodo:<h4 class="card-title"> <?php echo $periodo_selected;  ?></h4> </label>
            </div>
        <?php endif ?>
        <table class="table table-borderless   text-center table-hover">
            <thead class="thead-default" style="position: sticky;top: -1px; background-color:whitesmoke;">
                <tr>
                    <div class="row">

                        <?php $bandera_row_dias = []; ?>
                        <!-- <th >HORA</th> -->
                        <th data-field="hora" data-filter-control="input" data-sortable="true">HORA</th>
                        <?php foreach ($aulas_filtros as $aula_th) : ?>
                            <th class="contenido"><?php echo $aula_th->nombre_corto ?></th>
                            <?php $bandera_row_dias[] = 0; ?>
                        <?php endforeach; ?>
                    </div>
                </tr>
            </thead>
            <tbody>
                <?php $hora = 7; ?>
                <?php $operacion = 1; ?>
                <?php while ($hora < 21) : ?>
                    <?php $index = 0; ?>
                    <tr>
                        <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                        <?php foreach ($aulas_filtros as $aula) : ?>
                            <?php $bandera_coincidencia = false; ?>
                            <?php if ($horas_ocupadas != false) : ?>
                                <?php foreach ($horas_ocupadas as $hora_actual) : ?>
                                    <?php if ($hora_actual->aula == $aula->id_aula) : ?>
                                        <?php if ($hora_actual->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) :
                                            $inicio = explode(':', $hora_actual->h_inicio);
                                            $fin = explode(':', $hora_actual->h_fin);
                                            $row_span = (intval($fin[0]) - intval($inicio[0]));
                                        ?>
                                            <td class="align-middle bloque contenido" rowspan="<?php echo $row_span + 1 ?>">
                                                <div <?php echo $hora_actual->grupo ?>>
                                                    <?php if (($row_span + 1) > 1) {
                                                        $bandera_row_dias[$index] = ($row_span) + $bandera_row_dias[$index];
                                                    } ?>
                                                    <?php echo  $hora_actual->ma_nombre . '<br>' . (($hora_actual->docente == 1) ? 'Por asignar' : mb_strtoupper($hora_actual->em_paterno . ' ' . $hora_actual->em_materno . ' ' . $hora_actual->em_nombre)) .  "<br>" . $hora_actual->seccion
                                                    ?>
                                                </div>
                                            </td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif ?>
                            <?php if (!$bandera_coincidencia) : ?>
                                <?php if ($bandera_row_dias[$index] > 0) : ?>
                                    <?php $bandera_row_dias[$index]--; ?>
                                <?php else : ?>
                                    <td style="background-color: #bdb994;" class="align-middle contenido">
                                    </td>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $operacion++; ?>
                            <?php $index++; ?>
                        <?php endforeach; ?>
                    </tr>
                    <?php $hora++; ?>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>
<script></script>
<?php echo $this->endSection() ?>