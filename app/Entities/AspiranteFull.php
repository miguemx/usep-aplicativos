<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspiranteFull extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'carrera' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'nombre' => null,
        'curp' => null,
        'correo' => null,
        'telefono' => null,
        'calle' => null,
        'numero' => null,
        'numeroInterior' => null,
        'cp' => null,
        'colonia' => null,
        'municipio' => null,
        'estado' => null,
        'pais' => null,
        'nacimientoFecha' => null,
        'nacimientoMunicipio' => null,
        'nacimientoEstado' => null,
        'nacimientoPais' => null,
        'enviado' => null,
        
        'carreraNombre' => null,
        'carreraNumero' => null,
        'carreraClave' => null,
        
        'socioEstadoCivil' => null,
        'socioIngreso' => null,
        'socioHijos' => null,
        'socioDependientes' => null,
        'socioMpSoltero' => null,
        'socioPersonasCasa' => null,
        'socioViveEn' => null,
        'socioRadicar' => null,
        'socioTrabajaSalud' => null,
        'socioComunidadIndigena' => null,
        'socioSituacionVulnerable' => null,
        'socioVulnerableCual' => null,
        'socioAdmisionPrevio' => null,
        'socioAdmisionActual' => null,
        'socioTieneDesktop' => null,
        'socioTieneWebcam' => null,
        'socioTieneLaptop' => null,
        'socioTieneInternet' => null,
        'socioTieneTablet' => null,
        'socioTieneSmartphone' => null,
        'socioBeca' => null,
        'socioEnviado' => null,
        'socioExamenOnline' => null,
        'socioConseguirCompu' => null,
        
        'saludTipoSangre' => null,
        'saludPadecimientos' => null,
        'saludDiscapacidad' => null,
        'saludCertificadoDiscapacidad' => null,
        'saludUsaLentes' => null,
        'saludTieneProtesis' => null,
        'saludServicioMedico' => null,
        'saludEnviado' => null,
        
        'procedenciaAspirante' => null,
        'procedenciaTipo' => null,
        'procedenciaCct' => null,
        'procedenciaNombre' => null,
        'procedenciaCalle' => null,
        'procedenciaNum' => null,
        'procedenciaCp' => null,
        'procedenciaColonia' => null,
        'procedenciaMunicipio' => null,
        'procedenciaEstado' => null,
        'procedenciaPais' => null,
        'procedenciaFechaConclusion' => null,
        'procedenciaPromedio' =>null,
        'procedenciaEnviado' => null,
        
        'emergenciaAspirante' => null,
        'emergenciaNombre' => null,
        'emergenciaParentesco' => null,
        'emergenciaTelefono' => null,
        'emergenciaCorreo' => null,
        'emergenciaEnviado' => null,
        
        'procesoRegistro' => null,
        'procesoVerificacion' => null,
        'procesoCaptura' => null,
        'procesoDocumentos' => null,
        'procesoRevision' => null,
        'procesoExito' => null,
        'procesoDeclinado' => null,
        'procesoRechazado' => null,
        'procesoCarta' => null,
        
        'creacion' => null,
        'califP1' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'aspirante_id',
        'carrera' => 'aspirante_carrera',
        'apPaterno' => 'aspirante_ap_paterno',
        'apMaterno' => 'aspirante_ap_materno',
        'nombre' => 'aspirante_nombre',
        'curp' => 'aspirante_curp',
        'correo' => 'aspirante_correo',
        'telefono' => 'aspirante_telefono',
        'calle' => 'aspirante_calle',
        'numero' => 'aspirante_numero',
        'numeroInterior' => 'aspirante_numero_interior',
        'cp' => 'aspirante_cp',
        'colonia' => 'aspirante_colonia',
        'municipio' => 'aspirante_municipio',
        'estado' => 'aspirante_estado',
        'pais' => 'aspirante_pais',
        'nacimientoFecha' => 'aspirante_nacimiento_fecha',
        'nacimientoMunicipio' => 'aspirante_nacimiento_municipio',
        'nacimientoEstado' => 'aspirante_nacimiento_estado',
        'nacimientoPais' => 'aspirante_nacimiento_pais',
        'enviado' => 'aspirante_enviado',
        
        'carreraNombre' => 'carrera_nombre',
        'carreraNumero' => 'carrera_numero',
        'carreraClave' => 'carrera_clave',
        
        'socioEstadoCivil' => 'socio_estado_civil',
        'socioIngreso' => 'socio_ingreso',
        'socioHijos' => 'socio_hijos',
        'socioDependientes' => 'socio_dependientes',
        'socioMpSoltero' => 'socio_mp_soltero',
        'socioPersonasCasa' => 'socio_personas_casa',
        'socioViveEn' => 'socio_vive_en',
        'socioRadicar' => 'socio_radicar',
        'socioTrabajaSalud' => 'socio_trabaja_salud',
        'socioComunidadIndigena' => 'socio_comunidad_indigena',
        'socioSituacionVulnerable' => 'socio_situacion_vulnerable',
        'socioVulnerableCual' => 'socio_vulnerable_cual',
        'socioAdmisionPrevio' => 'socio_admision_previo',
        'socioAdmisionActual' => 'socio_admision_actual',
        'socioTieneDesktop' => 'socio_tiene_desktop',
        'socioTieneWebcam' => 'socio_tiene_webcam', 
        'socioTieneLaptop' => 'socio_tiene_laptop',
        'socioTieneInternet' => 'socio_tiene_internet',
        'socioTieneTablet' => 'socio_tiene_tablet',
        'socioTieneSmartphone' => 'socio_tiene_smartphone',
        'socioBeca' => 'socio_beca',
        'socioEnviado' => 'socio_enviado',
        'socioExamenOnline' => 'socio_examen_online',
        'socioConseguirCompu' => 'socio_conseguir_compu',
        
        'saludTipoSangre' => 'salud_tipo_sangre',
        'saludPadecimientos' => 'salud_padecimientos',
        'saludDiscapacidad' => 'salud_discapacidad',
        'saludCertificadoDiscapacidad' => 'salud_certificado_discapacidad',
        'saludUsaLentes' => 'salud_usa_lentes',
        'saludTieneProtesis' => 'salud_tiene_protesis',
        'saludServicioMedico' => 'salud_servicio_medico',
        'saludEnviado' => 'salud_enviado',
        
        'procedenciaAspirante' => 'procedencia_aspirante',
        'procedenciaTipo' => 'procedencia_tipo',
        'procedenciaCct' => 'procedencia_cct',
        'procedenciaNombre' => 'procedencia_nombre',
        'procedenciaCalle' => 'procedencia_calle',
        'procedenciaNum' => 'procedencia_num',
        'procedenciaCp' => 'procedencia_cp',
        'procedenciaColonia' => 'procedencia_colonia',
        'procedenciaMunicipio' => 'procedencia_municipio',
        'procedenciaEstado' => 'procedencia_estado',
        'procedenciaPais' => 'procedencia_pais',
        'procedenciaFechaConclusion' => 'procedencia_fecha_conclusion',
        'procedenciaPromedio' => 'procedencia_promedio',
        'procedenciaEnviado' => 'procedencia_enviado',
        
        'emergenciaAspirante' => 'emergencia_aspirante',
        'emergenciaNombre' => 'emergencia_nombre',
        'emergenciaParentesco' => 'emergencia_parentesco',
        'emergenciaTelefono' => 'emergencia_telefono',
        'emergenciaCorreo' => 'emergencia_correo',
        'emergenciaEnviado' => 'emergencia_enviado',
        
        'procesoRegistro' => 'proceso_registro',
        'procesoVerificacion' => 'proceso_validacion',
        'procesoCaptura' => 'proceso_captura',
        'procesoDocumentos' => 'proceso_documentos',
        'procesoRevision' => 'proceso_revision',
        'procesoExito' => 'proceso_exito',
        'procesoDeclinado' => 'proceso_declinado',
        'procesoRechazado' => 'proceso_rechazado',
        'procesoCarta' => 'proceso_carta',
        
        'creacion' => 'creacion',

        'califP1' => 'aspirante_calif_p1',
        
        
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}

/*

    aspirante_id,
    aspirante_carrera,
    aspirante_ap_paterno,
    aspirante_ap_materno,
    aspirante_nombre,
    aspirante_curp,
    aspirante_correo,
    aspirante_telefono,
    aspirante_calle,
    aspirante_numero,
    aspirante_numero_interior,
    aspirante_cp,
    aspirante_colonia,
    aspirante_municipio,
    aspirante_estado,
    aspirante_pais,
    aspirante_nacimiento_fecha,
    aspirante_nacimiento_municipio,
    aspirante_nacimiento_estado,
    aspirante_nacimiento_pais,
    aspirante_enviado,
    
    carrera_nombre,
    carrera_numero,
    carrera_clave,
    
    socio_estado_civil,
    socio_ingreso,
    socio_hijos,
    socio_dependientes,
    socio_mp_soltero,
    socio_personas_casa,
    socio_vive_en,
    socio_radicar,
    socio_trabaja_salud,
    socio_comunidad_indigena,
    socio_situacion_vulnerable,
    socio_vulnerable_cual,
    socio_admision_previo,
    socio_admision_actual,
    socio_tiene_desktop,
    socio_tiene_webcam, 
    socio_tiene_laptop,
    socio_tiene_internet,
    socio_tiene_tablet,
    socio_tiene_smartphone,
    socio_beca,
    socio_enviado,
    socio_examen_online,
    socio_conseguir_compu,
    
    salud_tipo_sangre,
    salud_padecimientos,
    salud_discapacidad,
    salud_certificado_discapacidad,
    salud_usa_lentes,
    salud_tiene_protesis,
    salud_servicio_medico,
    salud_enviado,
    
    procedencia_aspirante,
    procedencia_tipo,
    procedencia_cct,
    procedencia_nombre,
    procedencia_calle,
    procedencia_num,
    procedencia_cp,
    procedencia_colonia,
    procedencia_municipio,
    procedencia_estado,
    procedencia_pais,
    procedencia_fecha_conclusion,
    procedencia_promedio,
    procedencia_enviado,
    
    emergencia_aspirante,
    emergencia_nombre,
    emergencia_parentesco,
    emergencia_telefono,
    emergencia_correo,
    emergencia_enviado,
    
    proceso_registro,
    proceso_validacion,
    proceso_captura,
    proceso_documentos,
    proceso_revision,
    proceso_exito,
    proceso_declinado,
    
    prein_aspirantes.created_at as creacion */