-- se crea el nuevo rol para revisores documentales del procceso de admisión
INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'REVISOR DOCUMENTAL', 'escolar');

-- permisos para el nuevo rol
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'listadoaspirantes');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'etapasaspirantes');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'docente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'docente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'capturacalificaciones1');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'buscagrupo1');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'capturacalificacionesporgrupo1');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'listaescolar');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'generalistapdf');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'escolar', 'consultatablalista');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'estudiantes', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'home', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'renderfiles', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'documentopreview');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'getcomentarios');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'guardarcomentarios');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'generaralertas');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '15', 'controladmision', 'terminarproceso');

---------- permisos para rol de servicios escolares

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'documentopreview');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'getcomentarios');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'guardarcomentarios');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'generaralertas');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'terminarproceso');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'listadoaspirantes');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '3', 'controladmision', 'etapasaspirantes');