<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">

    <div class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 ">
        <div class="mb-3 d-flex justify-content-around">
            <label class="m-2">IDC:<h4 class="card-title"> <?php echo $grupo->id  ?></h4> </label>
            <label class="m-2">Materia:<h4 class="card-title"> <?php echo $materia->nombre  ?></h4> </label>
            <label class="m-2">Seccion:<h4 class="card-title"> <?php echo $grupo->clave  ?></h4> </label>
            <label class="m-2">Docente:<h4 class="card-title">
                    <?php
                    if ($docente_actual) {
                        echo $docente_actual->apPaterno . " " . $docente_actual->apMaterno . " " . $docente_actual->nombre;
                    } else {
                        echo 'Sin docente asignado';
                    }
                    ?></h4> </label>
        </div>
        <div>
            <form action="<?php echo base_url('ProgramacionAcademica/EditarGrupo/' . $grupo->id); ?>" method="post" id="form-guardar">
                <input type="hidden" name="materia" value="<?php echo $materia->nombre  ?>">
                <input type="hidden" name="clave" value=" <?php echo $grupo->clave  ?>">
                <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
                <input type="hidden" name="aula" value="<?php echo $aula ?>">
                <input type="hidden" name="select_periodo" value="<?php echo $grupo->periodo ?>">
                <div class="p-3">
                    <label for="docente_select" class="form-label">Seleccione un docente</label>
                    <select class="form-select" name="docente_select" id="docente_select">
                        <option value="1">Sin Asignar</option>
                        <?php if ($docente_actual) : ?>
                            <?php foreach ($docentes as $docente) : ?>
                                <option value="<?php echo $docente->id ?>" <?php echo (($docente->id == $docente_actual->id) ? 'selected' : '') ?>><?php echo $docente->apPaterno . " " . $docente->apMaterno . " " . $docente->nombre ?></option>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <?php foreach ($docentes as $docente) : ?>
                                <option value="<?php echo $docente->id ?>" ><?php echo $docente->apPaterno . " " . $docente->apMaterno . " " . $docente->nombre ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <small>Si se requiere cambiar el docente seleccione uno diferente</small>
                </div>
                <div class="m-3">
                    <div class="mb-3">
                        <label for="cupo_max" class="form-label">Ocupacion maxima del grupo</label>
                        <input type="number" class="form-control" name="cupo_max" id="cupo_max" aria-describedby="helpId" value="<?php echo $grupo->maximo ?>">
                        <small id="helpId" class="form-text text-muted">Seleccione la cantidad de alumnos que se permititan en el grupo seleccionado</small>
                    </div>
                </div>
                <div class="m-2 text-center">
                    <button type="button" class="btn btn-secondary" onclick="guardar()">Guardar cambios</button>
                    <button type="button" class="btn btn-secondary" onclick="cancelar()">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
    <?php if ($bandera == 'MapaAulas') : ?>
        <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo/' . $grupo->periodo); ?>" method="post" id="form-cancel">
            <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
            <input type="hidden" name="posicion" value="<?php echo $valores ?>">
            <input type="hidden" name="select_periodo" value="<?php echo $grupo->periodo ?>">

        </form>

    <?php elseif ($bandera == 'AulaVirtual') : ?>
        <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo/' . $grupo->periodo); ?>" method="post" id="form-cancel">
            <input type="hidden" name="posicion" value="<?php echo $valores ?>">
            <input type="hidden" name="bandera" value="<?php echo $bandera ?>">
            <input type="hidden" name="select_periodo" value="<?php echo $grupo->periodo ?>">

        </form>
    <?php else : ?>
        <?php $dato = explode('/', $bandera) ?>
        <form action="<?php echo base_url('ProgramacionAcademica/Home/' . (($dato[1]) ? $dato[1] : null)); ?>" method="post" id="form-cancel">
        </form>

    <?php endif; ?>


</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function cancelar() {
        var titulo = 'Descartar cambios';
        var mensaje = ('¿Salir sin guardar ningun cambio?');
        var submit = document.getElementById('form-cancel');
        alert_mensaje(titulo, mensaje, submit);
    }

    function guardar() {
        let docente = document.getElementById('docente_select').value;
        let cupo = document.getElementById('cupo_max').value;
        // console.log(docente);
        //  console.log(<?php // echo $grupo->maximo 
                        ?>);
        if (docente == '<?php echo ((isset($docente_actual->id))?$docente_actual->id:'') ?>' && cupo == '<?php echo $grupo->maximo ?>') {
            Swal.fire({
                'title': 'Cancelado',
                'text': 'No hay cambio de datos',
                'icon': 'info',
                confirmButtonColor: '#31BA31E3'
            });
        } else {

            validarCambios();
        }
    }

    function alert_mensaje(titulo, mensaje, submit) {
        Swal.fire({
            'title': titulo,
            'text': mensaje,
            'icon': 'question',
            confirmButtonColor: '#31BA31E3',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                submit.submit();

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#31BA31E3'
                });
            }

        });
    }
    var validarCambios = async () => {
        let docente = document.getElementById('docente_select').value;
        console.log(docente);
        console.log('<?php echo base_url("ProgramacionAcademica/cambioDocente/$grupo->id") ?>' + '/' + docente);
        let response = await fetch('<?php echo base_url("ProgramacionAcademica/cambioDocente/$grupo->id") ?>' + '/' + docente, {
            method: 'POST',
        }).then(response => response.json()).then(result => {
            return result;
        }).catch(error => {

            return true;
        });
        console.log(response);
        if (response == 'empalme') {
            Swal.fire({
                'title': 'Cancelado',
                'text': 'No se puede hacer el cambio de docente debido a un empalme con las materias ya asignadas',
                'icon': 'info',
                confirmButtonColor: '#31BA31E3'
            });
        } else if (response == true) {
            var titulo = 'Guardar cambios';
            var mensaje = ('¿Desea guardar los cambios realizados?');
            var submit = document.getElementById('form-guardar');
            alert_mensaje(titulo, mensaje, submit);
        } else if (response == 'Sin datos') {
            var titulo = 'Guardar cambios';
            var mensaje = ('¿Desea guardar los cambios realizados?');
            var submit = document.getElementById('form-guardar');
            alert_mensaje(titulo, mensaje, submit);
        }
    }
</script>
<?php echo $this->endSection() ?>