<?php
namespace App\Models;
use CodeIgniter\Model;

class AspirantesTiposCampoModel extends Model
{
    protected $table='aspirantes_tipos_campo';
    protected $primaryKey='tipocampo_id';
    protected $returnType='App\Entities\AspirantesTiposCampo';
    protected $useSoftDeletes = false;
    protected $allowedFields=['tipocampo_id','tipocampo_nombre','tipocampo_valores','tipocampo_clasificacion'];
    protected $db;
    protected $builder;
    
    /**
     * @param clasificacion corresponde a la clasificacion buscada
     * @return query devuelve un arreglo de campos de tipo seleccionado
     * 
     */
    public function getPorCalsificacion($clasificacion)
    {
        return $this->where('tipocampo_clasificacion',$clasificacion)->orderBy('tipocampo_nombre','ASC')->find();
    }
}

?>