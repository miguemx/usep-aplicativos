<?php
namespace App\Libraries;

use App\Models\AspirantesGeneralesModel;
use App\Models\AspirantesCamposModel;
use App\Models\AspiranteRespuestasModel;
use App\Models\AspirantesTodosCamposModel;
use App\Models\EstadosModel;
use App\Models\MunicipiosModel;
use App\Models\CarrerasModel;

class InformacionAspirantes {

    private $aspirantesModel;
    private $carrerasModel;
    private $camposModel;
    private $camposTodosModel;
    private $aspiranteRespuestasModel;
    private $info;
    private $currPos=0;
    private $carreras;
    private $campos;
    private $camposHead;
    private $camposIndexInicio = 0;

    public function __construct() {
        $this->aspirantesModel = new AspirantesGeneralesModel();
        $this->carrerasModel = new CarrerasModel();
        $this->camposModel = new AspirantesCamposModel();
        $this->camposTodosModel = new AspirantesTodosCamposModel();
        $this->aspiranteRespuestasModel = new AspiranteRespuestasModel();
        $this->resetInfo();
        $this->llenaCarreras();
        $this->campos = [];
    }

    /**
     * obtiene toda la información de cada aspirante, datos generales y sus respuestas
     */
    public function full() {
        ini_set('memory_limit', '500M');
        ini_set('max_execution_time', '300');
        $this->resetInfo();
        $aspirantes = $this->aspirantesModel->registroConcluido();
        $this->cabecerasGenerales();
        $this->cabecerasCampos();
        $this->currPos = count( $this->info );
        foreach ( $aspirantes as $aspirante ) {
            $this->rowGenerales( $aspirante );
            $this->rowRespuestas( $aspirante->folio );
            $tmp = $this->info[$this->currPos];
            $this->info[$this->currPos] = [];
            $this->info[$this->currPos] = $tmp;
            $this->currPos++;
        }
        return $this->info;
    }

    /**
     * Llena el arreglo info con la información de datos generales en la posición actual
     * @param aspirante el objeto \App\Entities\AspirantesGenerales
     */
    private function rowGenerales( $aspirante ) {
        $municipiosModel = new MunicipiosModel();
        $estadosModel = new EstadosModel();
        $municipios = $municipiosModel->arreglo();
        $estados = $estadosModel->arreglo();
        $this->info[ $this->currPos ] = [
            $aspirante->folio, $aspirante->apPaterno, $aspirante->apMaterno, $aspirante->nombre, 
            $aspirante->curp, $aspirante->sexo, $this->carreras[ $aspirante->carrera ]->nombre, 
            $aspirante->correo, $aspirante->telefono, 
            $aspirante->calle, $aspirante->numero, $aspirante->num_int, $aspirante->colonia, $aspirante->cp,
            $municipios[$aspirante->municipio], $estados[$aspirante->estado], $aspirante->pais,
            $aspirante->nac_fecha, $municipios[$aspirante->nac_municipio], $estados[$aspirante->nac_estado], $aspirante->nac_pais,
            $aspirante->estatus, date( "Y-m-d", strtotime($aspirante->created_at))
        ];
    }

    /**
     * obtiene y anexa al arreglo info las respuestas de un aspirante determinado
     * @param aspirante el Id del aspirante
     */
    private function rowRespuestas( $aspirante ) {
        $respuestas = $this->aspiranteRespuestasModel->getAspiranteArray( $aspirante );
        foreach ( $respuestas as $campo=>$respuesta ) {
            $this->info[ $this->currPos ][ $this->camposHead[$respuesta->campo] ] = $respuesta->valor;
        }
    }

    /**
     * reinicia la información del objeto
     */
    private function resetInfo() {
        $this->info = [];
    }

    /**
     * obtiene todas las carreras disponibles y llena el arreglo
     */
    private function llenaCarreras() {
        $carreras = $this->carrerasModel->findAll();
        foreach ( $carreras as $carrera ) {
            $this->carreras[ $carrera->id ] = $carrera;
        }
    }

    /**
     * Llena en el arreglo info la cabecera de los datos generales
     */
    private function cabecerasGenerales() {
        $this->info[0] = [
            'FOLIO', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO', 'NOMBRE(S)',
            'CURP', 'SEXO', 'OPCION DE INGRESO',
            'CORREO', 'TELEFONO',
            'CALLE', 'NÚMERO', 'NÚM. INTERIOR', 'COLONIA', 'C.P.',
            'MUNICIPIO', 'ESTADO', 'PAÍS',
            'FECHA DE NACIMIENTO', 'MUNICIPIO DE NACIMIENTO', 'ESTADO DE NACIMIENTO', 'PAÍS DE NACIMIENTO',
            'ESTATUS', 'FECHA DE REGISTRO',
        ];
    }

    /**
     * agrega al arreglo de info las cabeceras de los campos encontrados en un periodo determinado
     * @todo corregir el periodo para que sea dinámico
     */
    private function cabecerasCampos() {
        $periodo = '2022A';
        $this->campos = $this->camposTodosModel->periodoArray( $periodo );
        $this->camposIndexInicio = count( $this->info[0] );
        $i = $this->camposIndexInicio;
        foreach ( $this->campos as $campo ) {
            $this->info[0][$i] = $campo->pregunta;
            $this->camposHead[ $campo->campo_id ] = $i;
            $i++;
        }
    }

}