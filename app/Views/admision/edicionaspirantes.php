<?php

use SebastianBergmann\CodeCoverage\Report\PHP;

echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .body {
        margin: 5px;
    }

    .jjjdatos-aspirante {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }
</style>
<?php if (isset($mensaje)) : ?>
    <div class="alert <?php echo $mensaje['tipo'] ?>" role="alert">
        <?php echo $mensaje['texto']; ?>
    </div>
<?php endif; ?>
<div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="row">
        <div class="mt-1">
            <form action="<?php echo base_url('ControlAdmision/ModificarAspirante'); ?>" method="post" id="form-modificar">
                <h4 class="card-title ">
                    Carrera Aspirante
                </h4>
                <div>
                    <?php if ($bandera_rol) : ?>
                        <div class=" mb-3 col-1 d-flex align-items-center">
                            <button type="button" class="btn btn-warning m-2" onclick="eliminar_aspirante()">Eliminar aspirante</button>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">

                    <div class="mb-3 col">
                        <label for="carrera" class="form-label">Carrera</label>
                        <select name="carrera" class="form-control" id="carrera" disabled>
                            <?php foreach ($carreras as $carrera_datos) : ?>
                                <option value="<?php echo $carrera_datos->id ?>" <?php if ($carrera_datos->id == $carrera) echo 'selected="selected"'; ?>><?php echo $carrera_datos->nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <?php if (!is_null($etapa)) : ?>
                        <div class="mb-3 col">
                            <label for="etapa" class="form-label mb-1">Cambiar a etapa</label>
                            <select name="etapa" class="form-control mb-1" id="etapa">
                                <option selected value="">Revision documental</option>

                                <?php foreach ($etapas as $etapa_datos) : ?>
                                    <option value="<?php echo $etapa_datos->etapa ?>" <?php if ($etapa_datos->etapa == $etapa) echo 'selected="selected"'; ?>><?php echo $etapa_datos->nombre; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <small>Seleccione la etapa a la que se cambiara al aspirante</small>
                        </div>
                    <?php endif; ?>
                    <div class="mb-3 col ">

                        <label for="status" class="form-label">Estatus actual</label>
                        <input type="text" class="form-control" name="status" id="status" aria-describedby="helpId" disabled value="<?php echo $estatus  ?>">

                    </div>
                    <div class="mb-3 col mt-4">
                        <div class="form-check form-switch mt-1">
                            <input class="form-check-input" type="checkbox" id="switch_status" name="switch_status" <?php echo ($estatus != 'EXITOSO' && $estatus != 'DECLINADO'  && $estatus != 'OBSERVACIONES') ? 'disabled' : '' ?> onchange="cambiarestatus(this)">
                            <label class="form-check-label" for="switch_status">Cambiar a estatus de REVISION</label>
                        </div>
                    </div>
                </div>
                <h4 class="card-title">
                    Datos del Aspirante
                </h4>
                <div class="row">
                    <div class="mb-3 col">
                        <label for="aspirante-nombre" class="form-label">Folio</label>
                        <input disabled type="text" class="form-control" name="aspirante-nombre" id="aspirante-nombre" value="<?php echo $id ?>">
                    </div>
                    <div class="mb-3 col">
                        <label for="aspirante-nombre" class="form-label">CURP</label>
                        <input disabled type="text" class="form-control" name="aspirante-nombre" id="aspirante-nombre" value="<?php echo $curp ?>" disabled>
                    </div>
                    <div class="mb-3 col">
                        <label for="aspirante-nombre" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="aspirante-nombre" id="aspirante-nombre" value="<?php echo $nombre ?>">
                    </div>
                    <div class="mb-3 col">
                        <label for="aspirante-appaterno" class="form-label">Apellido Paterno</label>
                        <input type="text" class="form-control" name="aspirante-appaterno" id="aspirante-appaterno" value="<?php echo $apPaterno ?>">
                    </div>
                    <div class="mb-3 col">
                        <label for="aspirante-apmaterno" class="form-label">Apellido Materno</label>
                        <input type="text" class="form-control" name="aspirante-apmaterno" id="aspirante-apmaterno" value="<?php echo $apMaterno ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="mb-3 col">
                        <label for="aspirante-correo" class="form-label">Correo</label>
                        <input type="text" class="form-control" name="aspirante-correo" id="aspirante-correo" value="<?php echo $correo ?>">
                    </div>

                    <div class="mb-3 col">
                        <label for="aspirante-telefono" class="form-label">Telefono</label>
                        <input type="number" class="form-control" name="aspirante-telefono" id="aspirante-telefono" value="<?php echo $telefono ?>">
                    </div>

                    <div class="mb-3 col">
                        <label for="aspirante-sexo" class="form-label">Sexo</label>
                        <select name="aspirante-sexo" class="form-control" id="aspirante-sexo">
                            <option value="H" <?php if ($sexo == 'H') echo 'selected' ?>>Hombre</option>
                            <option value="M" <?php if ($sexo == 'M') echo 'selected' ?>>Mujer</option>
                        </select>
                    </div>

                </div>
                <h5 class="card-title">
                    Direccion
                </h5>
                <div class="row mt-3">
                    <div class="mb-3 col-4">
                        <label for="aspirante-calle" class="form-label">Calle</label>
                        <input type="text" class="form-control" name="aspirante-calle" id="aspirante-calle" value="<?php echo $calle ?>">
                    </div>
                    <div class="mb-3 col-2">
                        <label for="aspirante-numero" class="form-label">Numero</label>
                        <input type="text" class="form-control" name="aspirante-numero" id="aspirante-numero" value="<?php echo $numero ?>">
                    </div>
                    <div class="mb-3 col-2">
                        <label for="aspirante-numeroint" class="form-label">Interior</label>
                        <input type="text" class="form-control" name="aspirante-numeroint" id="aspirante-numeroint" value="<?php echo $numeroInterior ?>">
                    </div>
                    <div class="mb-3 col-4">
                        <label for="aspirante-cp" class="form-label">Codigo Postal</label>
                        <input type="number" class="form-control" name="aspirante-cp" id="aspirante-cp" value="<?php echo $cp ?>" min="1" max="99999">
                    </div>
                    <!-- ------------------------------------------------------------------------------------ -->
                    <div class="col-sm-12 mb-2">
                        <label for="cmbPaises" class="form-label">País</label>
                        <select name="pais" class="form-control" id="cmbPaises" onchange="cambiaEstados()">
                            <option value="MEXICO" <?php if ($pais == 'MEXICO') echo 'selected="selected"' ?>>MÉXICO</option>
                            <option value="EXTRANJERO" <?php if ($pais == 'EXTRANJERO') echo 'selected="selected"' ?>>Estudié en el extranjero</option>
                        </select>
                    </div>
                    <div class="col-sm-12 mb-2">
                        <label for="carrera" class="form-label">Estado</label>
                        <select name="estado" class="form-control" id="cmbEstados" onchange="cambiaMunicipios()">
                            <option value="">Selecciona un estado</option>
                            <?php foreach ($estados as $edo) : ?>
                                <option value="<?php echo $edo->id ?>" <?php if ($edo->id == $estado) echo 'selected="selected"'; ?>><?php echo $edo->nombre; ?></option>
                            <?php endforeach; ?>
                            <option value="EXTRANJERO" <?php if ($estado == 'EXTRANJERO') echo 'selected="selected"'; ?>>Extranjero</option>
                        </select>
                    </div>
                    <div class="col-sm-12 mb-2">
                        <label for="carrera" class="form-label">Municipio</label>
                        <select name="municipio" class="form-control" id="cmbMunicipios">
                            <option selected value="1">Selecciona un municipio</option>
                        </select>
                    </div>
                    <!-- ------------------------------------------------------------------------------------ -->
                    <h5 class="card-title">
                        Datos de Nacimiento
                    </h5>
                    <div class="row">

                        <div class="mb-3 col">
                            <label for="cmbPaises_nac" class="form-label">País</label>
                            <select name="pais_nac" class="form-control" id="cmbPaises_nac" onchange="cambiaEstados_nac()">
                                <option value="MEXICO" <?php if ($nacimientoPais == 'MEXICO') echo 'selected="selected"' ?>>MÉXICO</option>
                                <option value="EXTRANJERO" <?php if ($nacimientoPais == 'EXTRANJERO') echo 'selected="selected"' ?>>Nací en el extranjero</option>
                            </select>
                        </div>
                        <div class="mb-3 col">
                            <label for="cmbEstados_nac" class="form-label">Estado</label>
                            <select name="estado_nac" class="form-control" id="cmbEstados_nac" onchange="cambiaMunicipiosNac()">
                                <option value="">Selecciona un estado</option>
                                <?php foreach ($estados as $edo) : ?>
                                    <option value="<?php echo $edo->id ?>" <?php if ($edo->id == $nacimientoEstado) echo 'selected="selected"'; ?>><?php echo $edo->nombre; ?></option>
                                <?php endforeach; ?>
                                <option value="EXTRANJERO" <?php if ($estado == 'EXTRANJERO') echo 'selected="selected"'; ?>>Extranjero</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 col">
                        <label for="cmbMunicipios_nac" class="form-label">Municipio</label>
                        <select name="municipio_nac" class="form-control" id="cmbMunicipios_nac">
                            <option value="1" selected>Selecciona un municipio</option>
                        </select>
                    </div>
                    <div class="mb-3 col">
                        <label for="startDate">Fecha de Nacimiento</label>
                        <input id="startDate" class="form-control" type="date" name="aspirante-nac" value="<?php echo $nacimientoFecha ?>" />
                    </div>
                </div>
                <div>
                    <div class="text-center mt-2">
                        <input type="hidden" name="id_aspirante" value="<?php echo $id ?>">
                        <button name="id_aspirante" type="button" class="btn btn-secondary" id="btn-modificar">Modificar</button>
                        <button class="btn btn-secondary" type="button" id="cancelar-aspirante">Regresar al menu</button>
                    </div>
                </div>


        </div>
        </form>
    </div>
</div>

<?php if (isset($banderaaspirante)) : ?>
    <form action="ListadoAspirantes" method="post" id="home-fomr">
        <input type="hidden" name="bandera-cancel" value="true">
    </form>
    <form action="<?php echo base_url('ControlAdmision/EliminarAspirante'); ?>" method="post" id="delete-fomr">
        <input type="hidden" name="bandera-eliminar" value="<?php echo $id ?>">
    </form>
    <script>
        'use strict';
        const btn_cancelar = document.querySelector('#cancelar-aspirante');
        let regresarhome = () => {
            document.getElementById('home-fomr').submit()
        }
        btn_cancelar.addEventListener('click', regresarhome);
    </script>
    <script type="text/javascript">
        function cambiaEstados() {
            let cmbPais = document.getElementById('cmbPaises');
            if (cmbPais.value == 'EXTRANJERO') {
                document.getElementById('cmbEstados').value = "EXTRANJERO";
                document.getElementById('cmbMunicipios').value = "NA";
            } else {
                document.getElementById('cmbEstados').value = "";
            }
        }

        var cambiaMunicipios = async () => {
            let estado = document.getElementById('cmbEstados').value;
            const formData = new FormData();
            formData.append('estado', estado);
            document.getElementById('cmbMunicipios').innerHTML = '';
            let response = await fetch('<?php echo base_url("ControlAdmision/getmunicipioscct") ?>', {
                method: 'POST',
                body: formData,
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });
            document.getElementById('cmbMunicipios').innerHTML = '';
            let municipio = '<?php echo $municipio ?>';
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].id_municipio;
                option.innerHTML = response[i].nombre_municipio;
                // console.log(response[i].nombre_municipio);
                if (response[i].id_municipio == municipio) {
                    option.selected = true;
                }
                document.getElementById('cmbMunicipios').appendChild(option);
            }
        }

        var cambiaEscuelas = async () => {
            let mpio = document.getElementById('cmbMunicipios').value;
            const formData = new FormData();
            formData.append('municipio', mpio);
            document.getElementById('cmbEscuelas').innerHTML = '';
            let response = await fetch('<?php echo base_url("ControlAdmision/getccts") ?>', {
                method: 'POST',
                body: formData,
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });
            document.getElementById('cmbEscuelas').innerHTML = '';
            let opt = document.createElement('option');
            opt.value = "";
            opt.innerHTML = "OTRA";
            document.getElementById('cmbEscuelas').appendChild(opt);
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].cct_cct + '|' + response[i].nombre + '|' + response[i].calle + '|' + response[i].numero + '|' + response[i].colonia + '|' + response[i].cp;
                option.innerHTML = response[i].nombre + ' (' + response[i].colonia + ')';
                document.getElementById('cmbEscuelas').appendChild(option);
            }
            llenaEscuela();
        }

        var llenaEscuela = () => {
            let cmbEscuela = document.getElementById('cmbEscuelas');
            let datosEscuela = cmbEscuela.value.split('|');
            document.getElementById('txtCct').value = datosEscuela[0];
            document.getElementById('txtNombre').value = datosEscuela[1];
            document.getElementById('txtCalle').value = datosEscuela[2];
            document.getElementById('txtNum').value = datosEscuela[3];
            document.getElementById('txtColonia').value = datosEscuela[4];
            document.getElementById('txtCp').value = datosEscuela[5];
        }

        cambiaMunicipios();
    </script>
    <script type="text/javascript">
        function cambiaEstados_nac() {
            let cmbPais = document.getElementById('cmbPaises_nac');
            if (cmbPais.value == 'EXTRANJERO') {
                document.getElementById('cmbEstados_nac').value = "EXTRANJERO";
                document.getElementById('cmbMunicipios_nac').value = "NA";
            } else {
                document.getElementById('cmbEstados_nac').value = "";
            }
        }

        var cambiaMunicipiosNac = async () => {
            let estado_nac = document.getElementById('cmbEstados_nac').value;
            const formData = new FormData();
            formData.append('estado_nac', estado_nac);
            document.getElementById('cmbMunicipios_nac').innerHTML = '';
            let response = await fetch('<?php echo base_url("ControlAdmision/getmunicipios_nac_cct") ?>', {
                method: 'POST',
                body: formData,
            }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });
            document.getElementById('cmbMunicipios_nac').innerHTML = '';
            let municipio = '<?php echo $nacimientoMunicipio ?>';
            for (let i = 0; i < response.length; i++) {
                let option = document.createElement('option');
                option.value = response[i].id_municipio;
                option.innerHTML = response[i].nombre_municipio;
                if (response[i].id_municipio == municipio) {
                    option.selected = true;
                }
                document.getElementById('cmbMunicipios_nac').appendChild(option);
            }
        }

        cambiaMunicipiosNac();
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        'use strict';
        const btn_guardar = document.getElementById('btn-modificar');
        let guardar_cambios = () => {
            Swal.fire({
                'title': 'Confirmación',
                'text': '¡ATENCIÓN ! Estas por modificar los datos del Aspirante, ¿Has verificado que los datos son correctos y deseas continuar?',
                'icon': 'question',
                confirmButtonColor: '#31BA31E3',
                confirmButtonText: 'Continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',


            }).then((result) => {
                if (result.value) {
                    Swal.fire({

                        'title': 'Enviados correctamente',
                        'text': 'Success',
                        'icon': 'success',
                        confirmButtonColor: '#31BA31E3'

                    });
                    document.getElementById('form-modificar').submit();

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire({
                        'title': 'Cancelado',
                        'text': 'No se realizó ninguna modificación',
                        'icon': 'info',
                        confirmButtonColor: '#31BA31E3'
                    });
                }
            });
        }
        btn_guardar.addEventListener('click', guardar_cambios);

        function eliminar_aspirante() {
            <?php if ($bandera_rol) : ?>
                Swal.fire({
                    'title': 'Confirmación',
                    'text': '¡ATENCIÓN! Estas a punto de eliminar el registro del aspirante con el folio <?php echo $id ?>, esta acción es irreversible ¿Esta seguro que desea continuar?',
                    'icon': 'question',
                    confirmButtonColor: '#31BA31E3',
                    confirmButtonText: 'Continuar',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',


                }).then((result) => {
                    if (result.value) {
                        Swal.fire({

                            'title': 'Solicitud procesada correctamente',
                            'text': 'Solicitud aprobada',
                            'icon': 'success',
                            confirmButtonColor: '#31BA31E3'

                        });
                        document.getElementById('delete-fomr').submit();

                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        Swal.fire({
                            'title': 'Cancelado',
                            'text': 'Revise los campos capturados y vuelva a intentar cuando esté seguro de enviar la información rellenada',
                            'icon': 'info',
                            confirmButtonColor: '#31BA31E3'
                        });
                    }
                });
            <?php else : ?>
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No cuentas con los permisos necesarios para realizar esta acción.',
                    'icon': 'info',
                    confirmButtonColor: '#31BA31E3'
                });
            <?php endif; ?>
        }

        function cambiarestatus(check) {
            var status = document.getElementById('status');
            if (check.checked == true) {
                status.value = 'REVISION';
                alert('Oprima el boton Modificar para aplicar los cambios');
            } else {
                status.value = '<?php echo $estatus ?>';
            }
        }
    </script>
<?php endif; ?>
<?php echo $this->endSection() ?>