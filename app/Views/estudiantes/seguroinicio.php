<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>
        <h2>Seguro Facultativo</h2>
        <hr />
    </div>
    <?php if(isset($errors)): ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            Por favor verifica los siguientes errores:
            <ul>
            <?php foreach( $errors as $error ): ?>
                <li><?php echo $error; ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if( !isset($registrado) ): ?>
    
    <form method="post" action="<?php echo base_url('Estudiantes/Seguro/Datos') ?>" enctype="multipart/form-data">
        <a href="#">Manual de instrucciones</a>
        <div class="alert alert-light">
            1. Verifica tus datos, en caso de requerir alguna corrección en tu nombre o CURP, te pedimos 
            escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a> antes de continuar con tu proceso de afiliación al seguro facultativo.
        </div>
        
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Nombre Completo</label>
            <input type="text" class="form-control" id="nombre" value="<?php echo $nombre ?>" disabled="disabled" />
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">CURP</label>
            <input type="text" class="form-control" id="nombre" value="<?php echo $curp ?>" disabled="disabled" />
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Licenciatura</label>
            <input type="text" class="form-control" id="nombre" value="<?php echo $carrera ?>" disabled="disabled" />
        </div>
        <div>&nbsp;</div><div>&nbsp;</div>
        <div class="alert alert-light">
            2. Ingresa la información solicitada
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Selecciona tu clínica IMSS más cercana a tu domicilio</label>
            <select class="form-select" aria-label="Default select example" name="clinica">
                <option value="">-- Ubica por localidad y número de clínica --</option>
                <?php foreach( $clinicas as $clinica ): ?>
                    <option value="<?php echo $clinica->id ?>" ><?php echo $clinica->localidad.' - '.$clinica->nombre; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div>&nbsp;</div><div>&nbsp;</div>
        <div class="alert alert-light">
            3. Adjunta la información solicitada
        </div>
        <div class="col-sm-12">
            <label for="apPaterno" class="form-label">Sube tu constancia de vigencia de derechos de IMSS</label>
            <input type="file" class="form-control" name="imss" />
        </div>
        <div>&nbsp;</div><div>&nbsp;</div>
        <button class="btn btn-secondary" type="submit" name="gf" value="1">
            Enviar
        </button>

    </form>

    <?php else: ?>

        <div class="alert alert-success">
            <p>
                <strong>Has completado el registro para el seguro facutativo que te otorga la USEP.</strong>
            </p>
            <p>
                En caso de que hayas aceptado este beneficio, la documentación proporcionada será sujeta a validación.
                Si ésta es correcta, se solicitará tu afiliación al IMSS. 
            </p>
            <p>
                En un lapso máximo de 8 días hábiles deberás consultar nuevamente 
                tu constancia de vigencia de derechos en la cual deberás contar con un estatus de Activo por parte de la Universidad de la Salud
                y podrás acudir a tu clínica para el alta correspodiente, o visitar el siguiente enlace:
            </p>
            <p>
                <a href="https://serviciosdigitales.imss.gob.mx/portal-ciudadano-web-externo/derechohabientes/tramite/registro" target="_blank">
                Alta en clínica o UMF con CURP
                </a>
            </p>
            <p>Si tienes alguna duda, favor de escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a>.</p>
        </div>

        <div class="alert alert-danger">
            <p>
                En caso de haber rechazado el beneficio, aquí concluye tu proceso.
            </p>
            <p>Si tienes alguna duda, favor de escribir a <a href="mailto:escolar@usalud.edu.mx" class="alert-link">escolar@usalud.edu.mx</a>.</p>
        </div>
    
    <?php endif; ?>
   
</div>

<?php echo $this->endSection() ?>