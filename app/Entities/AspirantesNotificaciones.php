<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesNotificaciones extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'id' => null,
        'aspirante' => null,
        'mensaje' => null,
        'leido' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'id' => 'notificaciones_id',
        'aspirante' => 'notificaciones_aspirante',
        'mensaje' => 'notificaciones_mensaje',
        'formulario' => 'notificaciones_formulario',
        'leido' => 'notificaciones_leido',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
