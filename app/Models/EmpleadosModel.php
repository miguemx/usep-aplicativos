<?php

namespace App\Models;
use CodeIgniter\Model;

class EmpleadosModel extends Model {
    
    protected $table      = 'aplicativo_empleados';
    protected $primaryKey = 'empleado_id';

    protected $returnType    = 'App\Entities\Empleado';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'empleado_id','empleado_correo','empleado_horario','empleado_area','empleado_nombre','empleado_apellido','empleado_else_regid','empleado_else_regidpass',
        'empleado_ap_paterno','empleado_ap_materno','deleted_at','empleado_docente',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * busca un alumno mediante su direccion de correo
     * @param correo el correo que se va a buscar
     * @return Empleado entidad de Empleado con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByEmail($correo) {
        $empleado = $this->where('empleado_correo',$correo)->first();
        return $empleado;
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     * @param porpagina (opcional) numero de registros por página
     * @param eliminados (opcional) busca también los registros de los eliminados
     */
    public function buscar($filtros=array(), $porpagina=50, $eliminados=false) {
        $pager = \Config\Services::pager();
        $this->creaFiltros( $filtros ); 
        if ( $eliminados ) { 
            $this->withDeleted();
        }
        $empleados = $this->paginate($porpagina);
        // $empleados = $this->getCompiledSelect();
        return $empleados;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros) {
        // 'alumno_id','alumno_nombre','alumno_curp','alumno_sexo','alumno_correo','alumno_carrera','alumno_vigente','alumno_else_regid','alumno_else_regpass',
        // 'alumno_ap_paterno','alumno_ap_materno','alumno_nombres'
        if ( array_key_exists('id', $filtros) ) { // -- filtrar por ID
            if ( strlen($filtros['id']) ) {
                $this->where( 'empleado_id', trim($filtros['id']) );
            }
        }
        if ( array_key_exists('apPaterno', $filtros) ) { // -- filtrar por apPaterno
            if ( strlen($filtros['apPaterno']) ) {
                $this->like( 'empleado_ap_paterno', trim($filtros['apPaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('apMaterno', $filtros) ) { // -- filtrar por materno
            if ( strlen($filtros['apMaterno']) ) {
                $this->like( 'empleado_ap_materno', trim($filtros['apMaterno']), 'both' ); 
            }
        }
        if ( array_key_exists('nombre', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['nombre']) ) {
                $this->like( 'empleado_nombre', trim($filtros['nombre']), 'both' ); 
            }
        }
        if ( array_key_exists('docente', $filtros) ) { // -- filtrar por nombre
            if ( strlen($filtros['docente']) ) {
                // $this->where( 'empleado_docente', '1' );
                $this->notLike('empleado_correo', 'GEN');
            }
        }

        $this->orderBy('empleado_ap_paterno','ASC');
        $this->orderBy('empleado_ap_materno','ASC');
        $this->orderBy('empleado_nombre','ASC');
    }

    /**
     * retornar el listado de empleados que tienen correo y que son disponibles para dar clase
     */
    public function getEmpleadosDocentes()
    {
        return  $this->notLike('empleado_correo', 'GEN')
        ->where('empleado_docente', '1')
        ->orderBy('empleado_ap_paterno','ASC')
        ->orderBy('empleado_ap_materno','ASC')
        ->orderBy('empleado_nombre','ASC')
        ->find();
    }

    /**
     * busca a 1 empleado mediante su id
     * @param id id del empleado
     * @return info consulta del empleado proporcionado
     */
    public function findById($id){
        $info=$this->where('empleado_id',$id)
                   ->first();
        return $info;
    }

    /**
     * busca información de empleados apartír de un conjunto de id's
     * @param empleados array con los id's a consultar
     * @return consulta información del array proporcionado
     */

     public function findByIds($empleados){
        $consulta=$this->whereIn('empleado_id',$empleados)
                   ->findAll();
        return $consulta;
     }
}
