<?php
namespace App\Entities;

use CodeIgniter\Entity;

class FiltroAccesoReporte extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                    => null,
        'temperatura'           => null,
        'oximetria'             => null,
        'fechaAcceso'           => null,
        'folio'                 => null,
        'fechaCuestionario'     => null,
        'empleado'              => null,
        'empleadoCorreo'        => null,
        'empleadoNombre'        => null,
        'empleadoApPaterno'     => null,
        'empleadoApMaterno'     => null,
        'empleadoSexo'          => null,
        'alumno'                => null,
        'alumnoCorreo'          => null,
        'alumnoNombre'          => null,
        'alumnoApPaterno'       => null,
        'alumnoApMaterno'       => null,
        'alumnoSexo'            => null,
        'externoCorreo'         => null,
        'externoNombre'         => null,
        'externoDependencia'    => null,
        'externoEdad'           => null,
        'externoSexo'           => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                    => 'filtroacceso_id',
        'temperatura'           => 'filtroacceso_temperatura',
        'oximetria'             => 'filtroacceso_oximetria',
        'fechaAcceso'           => 'fecha_acceso',
        'folio'                 => 'filtrofolio_id',
        'fechaCuestionario'     => 'fecha_cuestionario',
        'empleado'              => 'empleado_id',
        'empleadoCorreo'        => 'empleado_correo',
        'empleadoNombre'        => 'empleado_nombre',
        'empleadoApPaterno'     => 'empleado_ap_paterno',
        'empleadoApMaterno'     => 'empleado_ap_materno',
        'empleadoSexo'          => 'empleado_sexo',
        'alumno'                => 'alumno_id',
        'alumnoCorreo'          => 'alumno_correo',
        'alumnoNombre'          => 'alumno_nombres',
        'alumnoApPaterno'       => 'alumno_ap_paterno',
        'alumnoApMaterno'       => 'alumno_ap_materno',
        'alumnoSexo'            => 'alumno_sexo',
        'externoCorreo'         => 'externo_correo',
        'externoNombre'         => 'externo_nombre',
        'externoDependencia'    => 'externo_dependencia',
        'externoEdad'           => 'externo_edad',
        'externoSexo'           => 'externo_sexo',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
