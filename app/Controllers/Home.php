<?php

namespace App\Controllers;

use App\Entities\AlumnosRipel;
use App\Models\CredencialModel;
use App\Models\AlumnosModel;
use App\Models\AlumnosRipelModel;
use App\Models\UsuariosModel;
use App\Models\VotacionPadronModel;
use App\Models\EvaluacionModel;
use App\Models\PeriodoModel;

class Home extends BaseController
{
	public function index(){
		$evaluacionmodel = new EvaluacionModel();
		$periodomodel = new PeriodoModel();
		$usuariomodel = new UsuariosModel();
		$evaluaciondocente = false;
		$data['notificaciones'] = $this->misnotificaciones;
		//echo $this->sesion->get('rol_id');
		$periodo = $periodomodel->periodoFsys ();
		$periodoid = $periodo->periodo_id;
		if ( $this->sesion->get('rol_id') == '4' ) {
			$evaluaciondocente = $evaluacionmodel->evaluacion_activa($periodoid,'ACTIVO',1);
			$alumnosModel = new AlumnosModel();
			$alumno = $alumnosModel->find( $this->sesion->get('id') );
			if ( $alumno ) {
				if ( $alumno->status != 'ACTIVO' ) {
					$data['banner'] = 'Tu cuenta se encuentra en estado de SUSPENDIDA. Por favor dirigete a la Dirección de Servicios Escolares y Titulación para evitar ser dado de baja.';
				}
			
			}
			//$data['bandera_modal'] = true;
		}
		if ( $this->sesion->get('rol_id') == '13' ){
			$data['bandera_modal'] = true;
			//echo "bandera";
		}
		$data['nombre'] = $this->sesion->get('nombre');
		$data['name'] = $this->sesion->get('nombre');
		$data['menu'] = $this->sesion->get('menu');
		$data['periodo'] = $periodoid;
		$data['evaluaciondocente'] = $evaluaciondocente;
		$consultavoto = $this->validavotos();
		if(!is_null($consultavoto)){
			$data['votoelectronico'] = $consultavoto;
		}
		return view('home', $data);
	}

	/**
	 * @deprecated No se utiliza mas este sistema temporal de notificaciones
	 */
	private function getNotificaciones() {
		$notificaciones = [];
		$credencialModel = new CredencialModel();
		$credencial = $credencialModel->find( $this->sesion->get('id') );
		if ( !is_null($credencial) ) {
			if ( !is_null($credencial->comentarioFoto) ) {
				$notificaciones[] = $credencial->comentarioFoto;
			}
			if ( !is_null($credencial->comentarioIdentificacion) ) {
				$notificaciones[] = $credencial->comentarioIdentificacion;
			}
		}
		if ( count($notificaciones) > 0 ) return $notificaciones;
		else return null;
	}

	/**
	 * validacion de ripel y cambio de rol para evitar mensaje 
	 */
	public function validacionripel()
	{
		if($this->sesion->get('rol_id')==13){
			$usuariomodel = new UsuariosModel();
			$datos = $usuariomodel->findByEmail($this->sesion->get('useremail'));
			if($datos){
				$datos->usuario_rol = 4;
				try{
					//update rol en tabla
					$usuariomodel->update($this->sesion->get('useremail'),$datos);
					//update rol en session
					$this->setSesion($this->sesion->get('useremail'));
					//insertar registro 
					$alumnoripelmodel = new AlumnosRipelModel();
					$alumnoripelent = new AlumnosRipel();
					$alumnoripelent->matricula = $this->sesion->get('id');
					$alumnoripelent->ip = $_SERVER['REMOTE_ADDR'];
					$alumnoripelmodel->insert($alumnoripelent);
					return redirect()->to(base_url());
				} catch (\Exception $ex) {
					$data['error'] = 'No actualizar el rol. ' . $ex->getMessage();
				}
				
			}
			
		}
		//echo $_SERVER['REMOTE_ADDR'];
		//var_dump($datos);
	}
	
	 /**
	 * coloca los valores de la sesion dependiendo si encuentra el usuario autenticado por google
	 * @param email el EMAIL que se recibe de google
	 * @return true en caso de que todos los valores se puedan settear de forma correcta
	 * @return error el mensaje de error en caso de fallo
	 */
	private function setSesion($email) {
		$usuariosModel = new UsuariosModel();
		$usuario = $usuariosModel->getSessionData( $email );
		if ( !is_null($usuario) ) {
			$this->sesion->set( 'useremail', $usuario['correo'] );
			$this->sesion->set( 'nombre', $usuario['nombre'] );
			$this->sesion->set( 'rol_id', $usuario['id_rol'] );
			$this->sesion->set( 'rol', $usuario['rol'] );
			$this->sesion->set( 'menu', $usuario['menu'] );
			$this->sesion->set( 'id', $usuario['id'] );
			return true;
		}
		return 'El usuario no está registrado en el sistema o no cuenta con los permisos para acceder. Por favor contacte al administrador.';
	}
	/**
	 * Busqueda de votos electrónicos disponibles por usuario 
	 */
	private function validavotos(){
		$correo =$this->sesion->get('useremail');
		$padronmodel = new VotacionPadronModel();
		$padron = $padronmodel->buscacorreovoto($correo);
		if($padron != null){
			return $padron;
		}else{
			return null;
		}
	}

	/**
	 * verifica si existe una evaluación docente a realizar
	 */
	private function evaluacionDocente(){
		
	}
}
