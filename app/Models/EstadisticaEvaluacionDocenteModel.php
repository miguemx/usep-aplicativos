<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\EstadisticaEvaluacionDocente;
use phpDocumentor\Reflection\Types\This;

class EstadisticaEvaluacionDocenteModel extends Model
{
    protected $table = "view_est_eval_doc";
    protected $returnType = 'App\Entities\EstadisticaEvaluacionDocente';
    protected $allowedFields = [];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * devuelve el número de los alumnos que han presentado la encuesta por sección y carrera
     * @param periodo periodo a consultar
     * @param alumnos array de alumnos que completaron las encuestas de todas sus materias
     * @return consulta arreglo con el conteo de los alumnos que han contestado la evaluacion agrupado por carrera y por sección
     */

    public function participacion_alumnos($periodo, $alumnos)
    {
        $consulta = $this->select('COUNT(distinct respuestas_matricula) as n_alumnos_p')
            ->select('materia_carrera')
            ->select('grupo_clave')
            ->where('grupo_periodo', $periodo)
            ->whereIn('respuestas_matricula', $alumnos)
            ->groupBy('materia_carrera')
            ->groupBy('grupo_clave')
            ->find();
        return $consulta;
    }

    /**
     * devuelve las calificaciones de los docentes
     * @param periodo periodo a consultar
     * @return consulta arreglo con el conteo de los preguntas agrupoadas por respuesta, docente y tipo de respuesta
     */

    public function resultados_docentes($periodo)
    {
        $consulta = $this->select('COUNT(respuesta_valor) as n_respuesta')
            ->select('respuesta_valor')
            ->select('preguntas_clasificacion')
            ->select('empleado_id')
            ->select('CONCAT (empleado_ap_paterno," ",empleado_ap_materno," ",empleado_nombre) as docente')
            ->where('periodo', $periodo)
            ->groupBy('preguntas_clasificacion')
            ->groupBy('respuesta_valor')
            ->groupBy('docente')
            ->groupBy('empleado_id')
            ->orderBy('empleado_id')
            ->orderBy('preguntas_clasificacion')
            ->orderBy('respuesta_valor')
            ->find();
        return $consulta;
    }
    /**
     * devuelve el conteo de preguntas de una evaluación para determinar la puntuación máxima
     * @param periodo periodo a consultar
     * @param materia id de materia a consultar
     * @param docente id del docente a consultar
     * @param clasificacion conjunto de preguntas que conforman 1 de los 4 aspectos a evaluar
     * @param evaluacion ID de evaluacion seleccionada
     * @param grupo IDC del grupo seleccionado
     * @return consulta conteo de preguntas
     */

    public function conteo_preguntas($periodo, $materia, $docente, $clasificacion, $evaluacion = false, $grupo = false)
    {
        if ($evaluacion) {
            $this->where('evaluacion_id', $evaluacion);
        }
        if ($grupo) {
            $this->where('respuestas_grupo', $grupo);
        }
        $this->select('COUNT(distinct respuestas_id) as n_preguntas')
            ->where('grupo_periodo', $periodo)
            ->where('materia_clave', $materia)
            ->where('empleado_id', $docente)
            ->where('preguntas_clasificacion', $clasificacion)
            ->where('preguntas_tipo', '4');
        
        $consulta = $this->first();
        return $consulta;
    }

    /**
     * devuelve el conteo de encuestas contestadas por un alumno de una evaluación
     * @param evaluacion evaluacion a consultar
     * @return consulta conteo de preguntas
     */

    public function conteo_grupos($evaluacion, $alumno)
    {
        $consulta = $this->select('COUNT(distinct grupo_id) as n_grupos')
            ->where('evaluacion_id', $evaluacion)
            ->where('respuestas_matricula', $alumno)
            ->first();
        return $consulta;
    }
    /**
     * cuenta a los alumnos por especialidad que hayan completado las evaluaciones de todos sus docentes
     * @param evaluacion evaluación a consultar
     * @param alumnos conjunto de matriculas a consultar
     * @param especialidad carrera a la cual pertenecen los alumnos
     * @return cuenta cantidad de alumnos
     */
    public function cuentaAlumnosEvaluacion($evaluacion, $alumnos, $especialidad)
    {
        $cantidad = $this->select('COUNT(distinct respuestas_matricula) as n_alumnos')
            ->where('evaluacion_id', $evaluacion)
            ->whereIn('respuestas_matricula', $alumnos)
            ->where('materia_carrera', $especialidad)
            ->first();
        return $cantidad;
    }
    /**
     * devuelve los diferentes grupos de una evaluacion
     * @param evaluacion evaluación a consultar
     * @return consulta grupos de la evaluacion consultada
     */
    public function gruposEvaluacion($evaluacion)
    {
        $cantidad = $this->select('distinct respuestas_grupo) as grupos')
            ->where('evaluacion_id', $evaluacion)
            ->findAll();
        return $cantidad;
    }
    /**
     * Genera el conjunto de datos para el reporte de respuestas de evaluación docente
     */
    public function respuestasExcel($evaluacion){
        return $this->select('grupo_periodo')
        ->select('empleado_id')
        ->select('empleado_ap_paterno')
        ->select('empleado_ap_materno')
        ->select('empleado_nombre')
        ->select('materia_carrera')
        ->select('materia_semestre')
        ->select('materia_clave')
        ->select('materia_nombre')
        ->select('respuestas_grupo')
        ->select('grupo_clave')
        ->select('preguntas_titulo')
        ->select('respuestas_valor')
        ->where('evaluacion_id',$evaluacion)
        ->orderBy('grupo_periodo')
        ->orderBy('empleado_id')
        ->orderBy('empleado_ap_paterno')
        ->orderBy('empleado_ap_materno')
        ->orderBy('empleado_nombre')
        ->orderBy('materia_carrera')
        ->orderBy('materia_semestre')
        ->orderBy('materia_clave')
        ->orderBy('materia_nombre')
        ->orderBy('respuestas_grupo')
        ->orderBy('grupo_clave')
        ->orderBy('preguntas_titulo')->find();
    }

    /**
     * retorna las diferentes secciones de una evaluacion
     * @param evaluacion id de la evaluacion a consultar
     * @return consulta  conjunto de secciones perteneciente a la evaluacion
     * 
     */
     public function est_secciones($evaluacion){
        //$cantidad = $this->select('distinct respuestas_grupo) as grupos')
        $consulta =$this->distinct(true)
                        ->select  ('grupo_clave')
                        ->where   ('evaluacion_id',$evaluacion)
                        ->orderBy ('grupo_clave')
                        ->findall ();
        return $consulta;
     }

}
