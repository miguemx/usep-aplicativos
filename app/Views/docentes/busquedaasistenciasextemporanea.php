<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<div class="container ">
    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <form method="post" action="<?php echo base_url('Docente/asistenciasextemporaneas') ?>">
        <h1 class="text-center my-2">Grupos asignados</h1> 
        <div style='display: flex;' class="bg-white rounded shadow p-4 mb-4 mt-4 ml-2 mr-2 contenido">
            <div class="input-group mb-3 col">
                <span class="input-group-text" id="inputGroup-sizing-default">GRUPO-CLAVE</span>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" id="grupo" name="grupo" value="<?php echo $grupo; ?>" /> 
                </div>
            </div>
            <div class="input-group mb-3 col">
                <span class="input-group-text" id="inputGroup-sizing-default">MATERIA</span>
                <div class="col-sm-12">
                    <select id="materia" class="custom-select" style="width: 80%;" name="materia">
                        <option value = ''>Selecciona una materia</option>
                        <?php foreach ($arrmaterias as $materia) : ?>
                            <option value = <?php echo $materia->grupo_materia ?> <?php if($materia == $materia->grupo_materia) echo 'selected="selected"'; ?>><?php
                                echo $materia->materia_nombre
                            ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="input-group mb-3 col">
                <span class="input-group-text" id="inputGroup-sizing-default">DÍAS DE CLASE</span>
                <div class="col-sm-10">
                    <select id="dias" class="custom-select" style="width: 80%;" name="dias" >
                        <option value = ''>Selecciona un día</option>
                        <option value = 'LUNES'> <?php if($dias == 'LUNES') echo 'selected="selected"'; ?>LUNES</option>
                        <option value = 'MARTES'> <?php if($dias == 'MARTES') echo 'selected="selected"'; ?>MARTES</option>
                        <option value = 'MIÉRCOLES'> <?php if($dias == 'MIÉRCOLES') echo 'selected="selected"'; ?>MIÉRCOLES</option>
                        <option value = 'JUEVES'> <?php if($dias == 'JUEVES') echo 'selected="selected"'; ?>JUEVES</option>
                        <option value = 'VIERNES'> <?php if($dias == 'VIERNES') echo 'selected="selected"'; ?>VIERNES</option>
                        <option value = 'SÁBADO'> <?php if($dias == 'SÁBADO') echo 'selected="selected"'; ?>SÁBADO</option>
                    </select>
                </div>
            </div>
            <div  style="text-align: right;"> 
                <a href="<?php echo base_url('Docente/asistenciasextemporaneas') ?>" class="btn btn-secondary btn-sm">Ver todos</a>
                <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
            </div>
        </div>       
    </form>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead class="  " style="background-color: #84112c  ; color:white; border: 1px solid black">
                <tr>
                    <th>GRUPO-SECCIÓN</th>
                    <th>NOMBRE DOCENTE</th>
                    <th>APELLIDO PATERNO</th>
                    <th>APELLIDO MATERNO</th>
                    <th>NOMBRE MATERIA</th>
                    <th>CAPTURAR ASISTENCIA</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($datosdocente as $datos): ?>
                    <tr>
                        <td><?php echo $datos->grupo_clave ?></td>
                        <td><?php echo $datos->empleado_nombre?></td>
                        <td><?php echo $datos->empleado_ap_paterno ?></td>
                        <td><?php echo $datos->empleado_ap_materno ?></td>
                        <td><?php echo $datos->materia_nombre?></td>
                        <td class="text-center">
                            <form  action="<?php echo base_url('Docente/modificacionasistencias'); ?>" method="post">
                                <input name="idgrupo" type="hidden" value="<?php echo $datos->grupo_id; ?>" />
                                <button type="submit" name="btnguardar" value="1" class="btn btn-secondary btn-sm">
                                    Consultar
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div>
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>

<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>

<script type="text/javascript">
    function confirma() {
        let mensaje = 'Está a punto de acceder al portal de este estudiante. \n\n Eso significa que se cerrará su sesión para acceder con la del estudiante seleccionado.';
        return confirm(mensaje);
    }
</script>

<?php echo $this->endSection() ?>
