-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-04-2022 a las 18:13:08
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usep_aplicativos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios_regiones`
--

CREATE TABLE `municipios_regiones` (
  `estado_id` int(5) DEFAULT NULL,
  `municipio_id` int(5) NOT NULL,
  `municipio_clave` int(5) DEFAULT NULL,
  `municipio_nombre` varchar(50) NOT NULL,
  `region_id` int(5) DEFAULT NULL,
  `region_nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `municipios_regiones`
--

INSERT INTO `municipios_regiones` (`estado_id`, `municipio_id`, `municipio_clave`, `municipio_nombre`, `region_id`, `region_nombre`) VALUES
(21, 1580, 1, 'Acajete', 32, 'Tepeaca'),
(21, 1581, 2, 'Acateno', 6, 'Teziutlán'),
(21, 1582, 3, 'Acatlán', 17, 'Acatlán'),
(21, 1583, 4, 'Acatzingo', 10, 'Acatzingo'),
(21, 1584, 5, 'Acteopan', 19, 'Atlixco'),
(21, 1585, 6, 'Ahuacatlán', 3, 'Zacatlán'),
(21, 1586, 7, 'Ahuatlán', 15, 'Izúcar de Matamoros'),
(21, 1587, 8, 'Ahuazotepec', 2, 'Huauchinango'),
(21, 1588, 9, 'Ahuehuetitla', 17, 'Acatlán'),
(21, 1589, 10, 'Ajalpan', 14, 'Sierra Negra'),
(21, 1590, 11, 'Albino Zertuche', 16, 'Chiautla'),
(21, 1591, 12, 'Aljojuca', 11, 'Ciudad Serdán'),
(21, 1592, 13, 'Altepexi', 13, 'Tehuacán'),
(21, 1593, 14, 'Amixtlán', 3, 'Zacatlán'),
(21, 1594, 15, 'Amozoc', 31, 'Amozoc'),
(21, 1595, 16, 'Aquixtla', 7, 'Chignahuapan'),
(21, 1596, 17, 'Atempan', 6, 'Teziutlán'),
(21, 1597, 18, 'Atexcal', 18, 'Tepexi de Rodríguez'),
(21, 1598, 19, 'Atlixco', 19, 'Atlixco'),
(21, 1599, 20, 'Atoyatempan', 32, 'Tepeaca'),
(21, 1600, 21, 'Atzala', 15, 'Izúcar de Matamoros'),
(21, 1601, 22, 'Atzitzihuacán', 19, 'Atlixco'),
(21, 1602, 23, 'Atzitzintla', 11, 'Ciudad Serdán'),
(21, 1603, 24, 'Axutla', 17, 'Acatlán'),
(21, 1604, 25, 'Ayotoxco de Guerrero', 6, 'Teziutlán'),
(21, 1605, 26, 'Calpan', 20, 'San Martín Texmelucan'),
(21, 1606, 27, 'Caltepec', 13, 'Tehuacán'),
(21, 1607, 28, 'Camocuautla', 3, 'Zacatlán'),
(21, 1608, 29, 'Caxhuacan', 4, 'Huehuetla'),
(21, 1609, 30, 'Coatepec', 3, 'Zacatlán'),
(21, 1610, 31, 'Coatzingo', 18, 'Tepexi de Rodríguez'),
(21, 1611, 32, 'Cohetzala', 16, 'Chiautla'),
(21, 1612, 33, 'Cohuecan', 19, 'Atlixco'),
(21, 1613, 34, 'Coronango', 30, 'Cuautlancingo'),
(21, 1614, 35, 'Coxcatlán', 13, 'Tehuacán'),
(21, 1615, 36, 'Coyomeapan', 14, 'Sierra Negra'),
(21, 1616, 37, 'Coyotepec', 18, 'Tepexi de Rodríguez'),
(21, 1617, 38, 'Cuapiaxtla de Madero', 10, 'Acatzingo'),
(21, 1618, 39, 'Cuautempan', 7, 'Chignahuapan'),
(21, 1619, 40, 'Cuautinchán', 32, 'Tepeaca'),
(21, 1620, 41, 'Cuautlancingo', 30, 'Cuautlancingo'),
(21, 1621, 42, 'Cuayuca de Andrade', 18, 'Tepexi de Rodríguez'),
(21, 1622, 43, 'Cuetzalan del Progreso', 5, 'Zacapoaxtla'),
(21, 1623, 44, 'Cuyoaco', 8, 'Libres'),
(21, 1624, 45, 'Chalchicomula de Sesma', 11, 'Ciudad Serdán'),
(21, 1625, 46, 'Chapulco', 13, 'Tehuacán'),
(21, 1626, 47, 'Chiautla', 16, 'Chiautla'),
(21, 1627, 48, 'Chiautzingo', 20, 'San Martín Texmelucan'),
(21, 1628, 49, 'Chiconcuautla', 2, 'Huauchinango'),
(21, 1629, 50, 'Chichiquila', 9, 'Quimixtlán'),
(21, 1630, 51, 'Chietla', 15, 'Izúcar de Matamoros'),
(21, 1631, 52, 'Chigmecatitlán', 18, 'Tepexi de Rodríguez'),
(21, 1632, 53, 'Chignahuapan', 7, 'Chignahuapan'),
(21, 1633, 54, 'Chignautla', 6, 'Teziutlán'),
(21, 1634, 55, 'Chila', 17, 'Acatlán'),
(21, 1635, 56, 'Chila de la Sal', 16, 'Chiautla'),
(21, 1636, 57, 'Honey', 1, 'Xicotepec'),
(21, 1637, 58, 'Chilchotla', 9, 'Quimixtlán'),
(21, 1638, 59, 'Chinantla', 17, 'Acatlán'),
(21, 1639, 60, 'Domingo Arenas', 20, 'San Martín Texmelucan'),
(21, 1640, 61, 'Eloxochitlán', 14, 'Sierra Negra'),
(21, 1641, 62, 'Epatlán', 15, 'Izúcar de Matamoros'),
(21, 1642, 63, 'Esperanza', 11, 'Ciudad Serdán'),
(21, 1643, 64, 'Francisco Z. Mena', 1, 'Xicotepec'),
(21, 1644, 65, 'General Felipe Ángeles', 10, 'Acatzingo'),
(21, 1645, 66, 'Guadalupe', 17, 'Acatlán'),
(21, 1646, 67, 'Guadalupe Victoria', 9, 'Quimixtlán'),
(21, 1647, 68, 'Hermenegildo Galeana', 3, 'Zacatlán'),
(21, 1648, 69, 'Huaquechula', 19, 'Atlixco'),
(21, 1649, 70, 'Huatlatlauca', 18, 'Tepexi de Rodríguez'),
(21, 1650, 71, 'Huauchinango', 2, 'Huauchinango'),
(21, 1651, 72, 'Huehuetla', 4, 'Huehuetla'),
(21, 1652, 73, 'Huehuetlán el Chico', 16, 'Chiautla'),
(21, 1653, 74, 'Huejotzingo', 20, 'San Martín Texmelucan'),
(21, 1654, 75, 'Hueyapan', 6, 'Teziutlán'),
(21, 1655, 76, 'Hueytamalco', 6, 'Teziutlán'),
(21, 1656, 77, 'Hueytlalpan', 4, 'Huehuetla'),
(21, 1657, 78, 'Huitzilan de Serdán', 4, 'Huehuetla'),
(21, 1658, 79, 'Huitziltepec', 32, 'Tepeaca'),
(21, 1659, 80, 'Atlequizayan', 4, 'Huehuetla'),
(21, 1660, 81, 'Ixcamilpa de Guerrero', 16, 'Chiautla'),
(21, 1661, 82, 'Ixcaquixtla', 18, 'Tepexi de Rodríguez'),
(21, 1662, 83, 'Ixtacamaxtitlán', 7, 'Chignahuapan'),
(21, 1663, 84, 'Ixtepec', 4, 'Huehuetla'),
(21, 1664, 85, 'Izúcar de Matamoros', 15, 'Izúcar de Matamoros'),
(21, 1665, 86, 'Jalpan', 1, 'Xicotepec'),
(21, 1666, 87, 'Jolalpan', 16, 'Chiautla'),
(21, 1667, 88, 'Jonotla', 5, 'Zacapoaxtla'),
(21, 1668, 89, 'Jopala', 2, 'Huauchinango'),
(21, 1669, 90, 'Juan C. Bonilla', 29, 'San Pedro Cholula'),
(21, 1670, 91, 'Juan Galindo', 2, 'Huauchinango'),
(21, 1671, 92, 'Juan N. Méndez', 18, 'Tepexi de Rodríguez'),
(21, 1672, 93, 'Lafragua', 9, 'Quimixtlán'),
(21, 1673, 94, 'Libres', 8, 'Libres'),
(21, 1674, 95, 'La Magdalena Tlatlauquitepec', 18, 'Tepexi de Rodríguez'),
(21, 1675, 96, 'Mazapiltepec de Juárez', 8, 'Libres'),
(21, 1676, 97, 'Mixtla', 32, 'Tepeaca'),
(21, 1677, 98, 'Molcaxac', 18, 'Tepexi de Rodríguez'),
(21, 1678, 99, 'Cañada Morelos', 11, 'Ciudad Serdán'),
(21, 1679, 100, 'Naupan', 1, 'Xicotepec'),
(21, 1680, 101, 'Nauzontla', 5, 'Zacapoaxtla'),
(21, 1681, 102, 'Nealtican', 20, 'San Martín Texmelucan'),
(21, 1682, 103, 'Nicolás Bravo', 13, 'Tehuacán'),
(21, 1683, 104, 'Nopalucan', 8, 'Libres'),
(21, 1684, 105, 'Ocotepec', 8, 'Libres'),
(21, 1685, 106, 'Ocoyucan', 28, 'San Andrés Cholula'),
(21, 1686, 107, 'Olintla', 4, 'Huehuetla'),
(21, 1687, 108, 'Oriental', 8, 'Libres'),
(21, 1688, 109, 'Pahuatlán', 1, 'Xicotepec'),
(21, 1689, 110, 'Palmar de Bravo', 11, 'Ciudad Serdán'),
(21, 1690, 111, 'Pantepec', 1, 'Xicotepec'),
(21, 1691, 112, 'Petlalcingo', 17, 'Acatlán'),
(21, 1692, 113, 'Piaxtla', 17, 'Acatlán'),
(21, 1693, 114, 'Puebla', 21, 'Puebla'),
(21, 1694, 115, 'Quecholac', 10, 'Acatzingo'),
(21, 1695, 116, 'Quimixtlán', 9, 'Quimixtlán'),
(21, 1696, 117, 'Rafael Lara Grajales', 8, 'Libres'),
(21, 1697, 118, 'Los Reyes de Juárez', 10, 'Acatzingo'),
(21, 1698, 119, 'San Andrés Cholula', 28, 'San Andrés Cholula'),
(21, 1699, 120, 'San Antonio Cañada', 13, 'Tehuacán'),
(21, 1700, 121, 'San Diego la Mesa Tochimiltzingo', 15, 'Izúcar de Matamoros'),
(21, 1701, 122, 'San Felipe Teotlalcingo', 20, 'San Martín Texmelucan'),
(21, 1702, 123, 'San Felipe Tepatlán', 3, 'Zacatlán'),
(21, 1703, 124, 'San Gabriel Chilac', 13, 'Tehuacán'),
(21, 1704, 125, 'San Gregorio Atzompa', 28, 'San Andrés Cholula'),
(21, 1705, 126, 'San Jerónimo Tecuanipan', 19, 'Atlixco'),
(21, 1706, 127, 'San Jerónimo Xayacatlán', 17, 'Acatlán'),
(21, 1707, 128, 'San José Chiapa', 8, 'Libres'),
(21, 1708, 129, 'San José Miahuatlán', 13, 'Tehuacán'),
(21, 1709, 130, 'San Juan Atenco', 11, 'Ciudad Serdán'),
(21, 1710, 131, 'San Juan Atzompa', 18, 'Tepexi de Rodríguez'),
(21, 1711, 132, 'San Martín Texmelucan', 20, 'San Martín Texmelucan'),
(21, 1712, 133, 'San Martín Totoltepec', 15, 'Izúcar de Matamoros'),
(21, 1713, 134, 'San Matías Tlalancaleca', 20, 'San Martín Texmelucan'),
(21, 1714, 135, 'San Miguel Ixitlán', 17, 'Acatlán'),
(21, 1715, 136, 'San Miguel Xoxtla', 30, 'Cuautlancingo'),
(21, 1716, 137, 'San Nicolás Buenos Aires', 9, 'Quimixtlán'),
(21, 1717, 138, 'San Nicolás de los Ranchos', 20, 'San Martín Texmelucan'),
(21, 1718, 139, 'San Pablo Anicano', 17, 'Acatlán'),
(21, 1719, 140, 'San Pedro Cholula', 29, 'San Pedro Cholula'),
(21, 1720, 141, 'San Pedro Yeloixtlahuaca', 17, 'Acatlán'),
(21, 1721, 142, 'San Salvador el Seco', 8, 'Libres'),
(21, 1722, 143, 'San Salvador el Verde', 20, 'San Martín Texmelucan'),
(21, 1723, 144, 'San Salvador Huixcolotla', 10, 'Acatzingo'),
(21, 1724, 145, 'San Sebastián Tlacotepec', 14, 'Sierra Negra'),
(21, 1725, 146, 'Santa Catarina Tlaltempan', 18, 'Tepexi de Rodríguez'),
(21, 1726, 147, 'Santa Inés Ahuatempan', 18, 'Tepexi de Rodríguez'),
(21, 1727, 148, 'Santa Isabel Cholula', 19, 'Atlixco'),
(21, 1728, 149, 'Santiago Miahuatlán', 13, 'Tehuacán'),
(21, 1729, 150, 'Huehuetlán el Grande', 18, 'Tepexi de Rodríguez'),
(21, 1730, 151, 'Santo Tomás Hueyotlipan', 32, 'Tepeaca'),
(21, 1731, 152, 'Soltepec', 8, 'Libres'),
(21, 1732, 153, 'Tecali de Herrera', 32, 'Tepeaca'),
(21, 1733, 154, 'Tecamachalco', 12, 'Tecamachalco'),
(21, 1734, 155, 'Tecomatlán', 17, 'Acatlán'),
(21, 1735, 156, 'Tehuacán', 13, 'Tehuacán'),
(21, 1736, 157, 'Tehuitzingo', 17, 'Acatlán'),
(21, 1737, 158, 'Tenampulco', 6, 'Teziutlán'),
(21, 1738, 159, 'Teopantlán', 15, 'Izúcar de Matamoros'),
(21, 1739, 160, 'Teotlalco', 16, 'Chiautla'),
(21, 1740, 161, 'Tepanco de López', 13, 'Tehuacán'),
(21, 1741, 162, 'Tepango de Rodríguez', 3, 'Zacatlán'),
(21, 1742, 163, 'Tepatlaxco de Hidalgo', 32, 'Tepeaca'),
(21, 1743, 164, 'Tepeaca', 32, 'Tepeaca'),
(21, 1744, 165, 'Tepemaxalco', 19, 'Atlixco'),
(21, 1745, 166, 'Tepeojuma', 15, 'Izúcar de Matamoros'),
(21, 1746, 167, 'Tepetzintla', 3, 'Zacatlán'),
(21, 1747, 168, 'Tepexco', 15, 'Izúcar de Matamoros'),
(21, 1748, 169, 'Tepexi de Rodríguez', 18, 'Tepexi de Rodríguez'),
(21, 1749, 170, 'Tepeyahualco', 8, 'Libres'),
(21, 1750, 171, 'Tepeyahualco de Cuauhtémoc', 32, 'Tepeaca'),
(21, 1751, 172, 'Tetela de Ocampo', 7, 'Chignahuapan'),
(21, 1752, 173, 'Teteles de Ávila Castillo', 6, 'Teziutlán'),
(21, 1753, 174, 'Teziutlán', 6, 'Teziutlán'),
(21, 1754, 175, 'Tianguismanalco', 19, 'Atlixco'),
(21, 1755, 176, 'Tilapa', 15, 'Izúcar de Matamoros'),
(21, 1756, 177, 'Tlacotepec de Benito Juárez', 12, 'Tecamachalco'),
(21, 1757, 178, 'Tlacuilotepec', 1, 'Xicotepec'),
(21, 1758, 179, 'Tlachichuca', 9, 'Quimixtlán'),
(21, 1759, 180, 'Tlahuapan', 20, 'San Martín Texmelucan'),
(21, 1760, 181, 'Tlaltenango', 29, 'San Pedro Cholula'),
(21, 1761, 182, 'Tlanepantla', 32, 'Tepeaca'),
(21, 1762, 183, 'Tlaola', 2, 'Huauchinango'),
(21, 1763, 184, 'Tlapacoya', 2, 'Huauchinango'),
(21, 1764, 185, 'Tlapanalá', 15, 'Izúcar de Matamoros'),
(21, 1765, 186, 'Tlatlauquitepec', 6, 'Teziutlán'),
(21, 1766, 187, 'Tlaxco', 1, 'Xicotepec'),
(21, 1767, 188, 'Tochimilco', 19, 'Atlixco'),
(21, 1768, 189, 'Tochtepec', 12, 'Tecamachalco'),
(21, 1769, 190, 'Totoltepec de Guerrero', 17, 'Acatlán'),
(21, 1770, 191, 'Tulcingo', 17, 'Acatlán'),
(21, 1771, 192, 'Tuzamapan de Galeana', 5, 'Zacapoaxtla'),
(21, 1772, 193, 'Tzicatlacoyan', 32, 'Tepeaca'),
(21, 1773, 194, 'Venustiano Carranza', 1, 'Xicotepec'),
(21, 1774, 195, 'Vicente Guerrero', 14, 'Sierra Negra'),
(21, 1775, 196, 'Xayacatlán de Bravo', 17, 'Acatlán'),
(21, 1776, 197, 'Xicotepec', 1, 'Xicotepec'),
(21, 1777, 198, 'Xicotlán', 16, 'Chiautla'),
(21, 1778, 199, 'Xiutetelco', 6, 'Teziutlán'),
(21, 1779, 200, 'Xochiapulco', 5, 'Zacapoaxtla'),
(21, 1780, 201, 'Xochiltepec', 15, 'Izúcar de Matamoros'),
(21, 1781, 202, 'Xochitlán de Vicente Suárez', 4, 'Huehuetla'),
(21, 1782, 203, 'Xochitlán Todos Santos', 12, 'Tecamachalco'),
(21, 1783, 204, 'Yaonáhuac', 6, 'Teziutlán'),
(21, 1784, 205, 'Yehualtepec', 12, 'Tecamachalco'),
(21, 1785, 206, 'Zacapala', 18, 'Tepexi de Rodríguez'),
(21, 1786, 207, 'Zacapoaxtla', 5, 'Zacapoaxtla'),
(21, 1787, 208, 'Zacatlán', 3, 'Zacatlán'),
(21, 1788, 209, 'Zapotitlán', 13, 'Tehuacán'),
(21, 1789, 210, 'Zapotitlán de Méndez', 4, 'Huehuetla'),
(21, 1790, 211, 'Zaragoza', 5, 'Zacapoaxtla'),
(21, 1791, 212, 'Zautla', 5, 'Zacapoaxtla'),
(21, 1792, 213, 'Zihuateutla', 1, 'Xicotepec'),
(21, 1793, 214, 'Zinacatepec', 13, 'Tehuacán'),
(21, 1794, 215, 'Zongozotla', 4, 'Huehuetla'),
(21, 1795, 216, 'Zoquiapan', 5, 'Zacapoaxtla'),
(21, 1796, 217, 'Zoquitlán', 14, 'Sierra Negra');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `municipios_regiones`
--
ALTER TABLE `municipios_regiones`
  ADD PRIMARY KEY (`municipio_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- creación de la vista para la estadística

create view view_estadistica_regiones as (
SELECT
	count(distinct(a.aspirante_id)) as n_alumnos,
    a.aspirante_municipio,
    b.municipio_nombre,
    b.region_id,
    b.region_nombre
FROM
	`aspirantes_generales` as a,
    `municipios_regiones` as b 
WHERE 
	a.aspirante_estado =21 AND
    a.aspirante_municipio = b.municipio_id
Group by
	a.aspirante_municipio);