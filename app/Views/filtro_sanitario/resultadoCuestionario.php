<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>
<style>
	body {
		font-family: 'Open Sans', sans-serif;
		background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
		background-size: 794px 1100px;
		background-repeat: no-repeat;
		background-position: left;
	}

	h3 {
		text-align: center;
	} 
	.main{
		
		padding-top: 10px;
		display: flex;
		flex-direction: row;
		justify-content: center;
	}.vacuna{
		text-align: center;
		border: 2.5px solid #000;
		border-color: red;
	}
	.ima1{
        background-image: url(<?php echo base_url('img/logos/Img5.jpg') ?>);
    }
</style>
<section style="background-color:#dcdcdc">
	<div class="container py-5 h-100">
		<div class="row d-flex justify-content-center align-items-center h-100">
			<div class="col col-xl-10">
				<div class="card" style="border-radius: 1rem; background-color:white;">
					<div class="card">
						<div class="card-header" style="background-color:#841138 ; color:antiquewhite;">
							<center>
								USEP CUESTIONARIO

							</center>

						</div>

						<div class="card-body">
							<div class="container">
								<center><?php echo $mensaje; ?></center>
								<?php if ($mensaje == "QR Válido") { ?>
									<div style="display: flex; justify-content: space-around;">
										<div style="display:flex; align-content:space-between;  flex-direction:column">
											<label for="contador">Sintomas mayores</label>
											<form>
												<input disabled="disabled" type="number" name="contador" id="contador" style="width: 50px;" value="<?php echo $smayores; ?>" ;>
											</form>
										</div>
										<div>
											<form>
													<?php if (isset($banderaverde)) : ?>
													<div>
														<img src="<?php echo base_url('img/icons/correct.png') ?>" alt="" height ="60">
													</div>
													<?php endif; ?>
													<?php if (isset($banderaroja)) : ?>
													<div>
														<img src="<?php echo base_url('img/icons/wrong.png') ?>" alt="" height ="60">
													</div>
													<?php endif; ?>
											</form>
										</div>
										<div style="display:flex; justify-content:space-between;  flex-direction:column">
											<label for="contador">Sintomas menores</label>
											<form method="POST">
												<input disabled="disabled" type="number" name="contador" id="contador" style="width: 50px;" value="<?php echo $smenores; ?>" ;>
											</form>
										</div>
										
									</div>
									
							</div>
							<br>
								<div class="vacuna">
									<br>
									<h2>
										Informe de vacunación SARS-CoV-2 (COVID-19)
									</h2>
									<div class="form-check form-check-inline">
										<label>1ra dosis</label><br>
										<input style="margin-left:30px; margin-right:auto;" disabled="disabled" class="form-check-input" type="checkbox" name="vacuna" id="idvacuna1" value="1" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($vacuna==1) {?> checked <?php } ?>  required>
									</div>
									<div class="form-check form-check-inline">
										<label>2da dosis</label><br>
										<input style="margin-left:30px; margin-right:auto;" disabled="disabled" class="form-check-input" type="checkbox" name="vacuna" id="idvacuna2" value="2" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($vacuna==2) {?> checked <?php } ?> required>
									</div>
									<div class="form-check form-check-inline">
										<label>Refuerzo</label><br>
										<input style="margin-left:30px; margin-right:auto;" disabled="disabled" class="form-check-input" type="checkbox" name="vacuna" id="idvacuna3" value="3" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($vacuna==3) {?> checked <?php } ?> required>
									</div>
									<div class="form-check form-check-inline">
										<label>Prefiero no contestar</label><br>
										<input style="margin-left:70px; margin-right:auto;" disabled="disabled" class="form-check-input" type="checkbox" name="vacuna" id="idvacuna4" value="4" pattern="^@?(\w){1,15}$" title="Seleccione una opción" <?php if ($vacuna==4) {?> checked <?php } ?> required>
									</div>
									<br><br>
								</div>
							<div class="main" >
								<div class="contianer text-center " style="width: 50%;">
											<form id="datoscomplemento" method="POST" action="<?php echo base_url('FiltroSanitario/agrega_temp_oxi')?>">
											<div class="form-group text-left">
													<label for="temp">Temperatura:</label>
													<input class="form-control" type="text" name="temp" id="temp" />
											</div>
											<div class="form-group text-left">
													<label for="oxigen">Oxigenación: </label>
													<input class="form-control" type="text" name="oxigen" id="oxigen" />
											</div>
													<input type="folio" name="folio" id="folio" value ="<?php echo $Nofolio; ?>"; hidden>
											
													<button class="btn btn-secondary text-center" type="submit" id="btnfolio"> Guardar datos complementarios </button>
												</form>
											</div>												
							</div>
							<div id="div_principal">
								<form id="contenedor2" method="POST" action="<?php echo base_url('FiltroSanitario/llamarQr/' . $Nofolio) ?>">
									<div style="display: none;">
										<input type="text" name="Nofolio" id="Nofolio" value="<?php echo $Nofolio; ?>" ;>
										<button id="btnfolio"> consulta </button>
									</div>
									<br>
									<h3 id="hdiagnostico">Información a quien corresponda:</h3>
									<table class="table table-borderless" style="width: 95%; display:flex; justify-content:center">
										<tbody>
											<tr>
												<td>Usuario </td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="usuario" id="usuario" value="<?php echo $usuario; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Diabetes mellitus </td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="diabetes" id="diabetes" value="<?php echo $diabetes; ?>" ;><br></td>

											</tr>
											<TR>
												<td>Hipertensión arterial</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="hipertension" id="hipertension" value="<?php echo $hipertension; ?>" ;><br></td>

											</TR>
											<tr>
												<td>Antecedentes cardiovasculares</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="antecedentecard" id="antecedentecard" value="<?php echo $antecedentecard; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Enfermedad respiratoria crónica</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="enfermedadres" id="enfermedadres" value="<?php echo $enfermedadres; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Otro padecimiento</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="otropadecimiento" id="otropadecimiento" value="<?php echo $otropadecimiento; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Es Usted fumador</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="fumador" id="fumador" value="<?php echo $fumador; ?>" ;><br></td>
											</tr>
										</tbody>
									</table>
									<br>
									<h3>
										En los últimos 10 días ha presentado:
										<br>
									</h3>
									<table class="table table-borderless" style="width: 95%; display:flex; justify-content:center">
										<tbody>
											<tr>
												<td>Fiebre </td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="fiebre" id="fiebre" value="<?php echo $fiebre; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Tos</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="tos" id="tos" value="<?php echo $tos; ?>" ;><br></td>
											</tr>
											<TR>
												<td>Dificultad para respirar o falta de aire</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="aire" id="aire" value="<?php echo $aire; ?>" ;><br></td>

											</TR>
											<tr>
												<td>Dolor de cabeza</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="dolorcabeza" id="dolorcabeza" value="<?php echo $dolorcabeza; ?>" ;><br></td>
											</tr>

										</tbody>
									</table>

									<br>
									<h3>Signos y síntomas menores que pueden acompañar a los mayores</h3>
									<table class="table table-borderless" style="width: 95%; display:flex; justify-content:center">
										<tbody>
											<tr>
												<td>dolores musculares </td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="dolorm" id="dolorm" value="<?php echo $dolorm; ?>" ;><br></td>
											</tr>
											<tr>
												<td>dolor a nivel de articulaciones</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="dolora" id="dolora" value="<?php echo $dolora; ?>" ;><br></td>
											</tr>
											<TR>
												<td>sensación de fatiga y debilidad muscular</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="fatiga" id="fatiga" value="<?php echo $fatiga; ?>" ;><br></td>

											</TR>
											<tr>
												<td>Dolor al tragar alimentos o liquidos</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="dolort" id="dolort" value="<?php echo $dolort; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Dolor abdominal o diarrea</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="diarrea" id="diarrea" value="<?php echo $diarrea; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Alteraciones del gusto</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="gusto" id="gusto" value="<?php echo $gusto; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Alteraciones del olfato</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="olfato" id="olfato" value="<?php echo $olfato; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Escurrimiento nasal</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="nasal" id="nasal" value="<?php echo $nasal; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Conjuntivitis</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="conjuntivitis" id="conjuntivitis" value="<?php echo $conjuntivitis; ?>" ;><br></td>
											</tr>
											<tr>
												<td>Escalofríos</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="escalofrio" id="escalofrio" value="<?php echo $escalofrio; ?>" ;><br></td>
											</tr>
											<tr>
												<td>¿Ha estado de viaje (nacional o en el extranjero)?</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="viaje" id="viaje" value="<?php echo $viaje; ?>" ;><br></td>
											</tr>
											<tr>
												<td>¿Ha sido diagnosticado por un médico con SARS Cov-2 (COVID 19)?</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="diagnosticado" id="diagnosticado" value="<?php echo $diagnosticado; ?>" ;><br></td>
												<td>Fecha de prueba</td>
												<td style="text-align:RIGHT"><input type="date" disabled="disabled" name="datediagnosticado" id="datediagnosticado" value="<?php echo $datediagnosticado; ?>" ;><br></td>
											<tr>
											<tr>
												<td>¿Cursa con más de 14 días de cuarentena?</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="dias" id="dias" value="<?php echo $dias; ?>" ;><br></td>
												<td>Fecha de prueba</td>
												<td style="text-align:RIGHT"><input type="date" disabled="disabled" name="datedias" id="datedias" value="<?php echo $datedias; ?>" ;><br></td>
											<tr>
											<tr>
												<td>¿Ha tenido contacto con personas sospechosas o diagnosticada con COVID-19?</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="contacto" id="contacto" value="<?php echo $contacto; ?>" ;><br></td>
												<td>Fecha de prueba</td>
												<td style="text-align:RIGHT"><input type="date" disabled="disabled" name="datecontacto" id="datecontacto" value="<?php echo $datecontacto; ?>" ;><br></td>
											<tr>
												<td>Otro</td>
												<td style="text-align:RIGHT"><input disabled="disabled" name="otro" id="otro" value="<?php echo $otro; ?>" ;><br></td>
											</tr>
											</tr>
										</tbody>
									</table>
									<div class="text-center">
										<input class="btn btn-secondary text-center" type="submit" id="codigoQr" value="Genera código">
									</div>
								</form>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>


<?php echo $this->endSection() ?>