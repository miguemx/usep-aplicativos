<?php
namespace App\Models;
use CodeIgniter\Model;

class PermisosModel extends Model {
    protected $table      = 'aplicativo_permisos';
    protected $primaryKey = 'permiso_id';

    protected $returnType    = 'App\Entities\Permiso';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * obtiene la lista de permisos de acuerdo a su rol
     * @param rol el ID del rol a buscar
     * @return permisos el arreglo con los permisos del rol; devolvera vacio si no se encuentra
     */
    public function findByRol($rol) {
        $this->where('permiso_rol',$rol);
        return $this->findAll();
    }

    /**
     * indica si un modulo y funcion esta permitido para el rol especificado
     * @param rol el ID del rol a buscar
     * @param modulo el modulo (controller) a buscar
     * @param funcion (opcional)la funcion o metodo del controller para verificar 
     * @return true en caso de tener permiso
     * @return false en caso de que no se encuentre el permiso
     */
    public function permitido($rol, $modulo, $funcion=null) {
        $this->where('permiso_rol',$rol);
        $this->where('permiso_modulo',$modulo);
        if ( !is_null($funcion) ) {
            $this->groupStart();
            $this->where('permiso_funcion',$funcion);
            $this->orWhere('permiso_funcion','*');
            $this->groupEnd();
        }
        else {
            $this->groupStart();
            $this->where('permiso_funcion','*');
            $this->orWhere('permiso_funcion IS NULL');
            $this->groupEnd();
        }
        $permisos = $this->findAll();
        if ( count($permisos) > 0 ) return true;
        else return false; 
    }
    
}
