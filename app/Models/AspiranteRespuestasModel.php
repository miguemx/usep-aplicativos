<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class AspiranteRespuestasModel extends Model {
    protected $table      = 'view_aspirantes_respuestas';
    protected $primaryKey = 'respuesta_id';

    protected $returnType    = 'App\Entities\RespuestaAspirante';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * devuelve un arreglo con las respuestas realizadas por un aspirante de un formulario determinado
     * @param aspirante el ID del aspirante
     * @param formulario el ID del formulario a buscar
     * @return respuestas el arreglo con las respuestas [ idCampo => valor, otroIdCapo => otroValor ]
     */
    public function getFromFormulario($aspirante, $formulario) {
        $this->where( 'respuesta_aspirante', $aspirante );
        $this->where( 'formulario_id', $formulario );
        $resp = $this->findAll();
        $respuestas = [];
        foreach ( $resp as $respuesta ) {
            $respuestas[ $respuesta->campo ]['valor'] = $respuesta->valor;
            $respuestas[ $respuesta->campo ]['id'] = $respuesta->respuesta_id;
            $respuestas[ $respuesta->campo ]['comentario'] = $respuesta->comentario;
            $respuestas[ $respuesta->campo ]['aspirante'] = $respuesta->aspirante;
        }
        return $respuestas;
    }

    /**
     * devuelve  todas las respuestas de todas las etapas del aspirante cuando sean tipo archivo
     * @param aspirante corresponde as ID del aspirante
     */
    public function getDocumentosAspirantes($aspirante)
    {
        $this->where( 'respuesta_aspirante', $aspirante );
        $this->where( 'tipocampo_clasificacion', 'ARCHIVO' );
        $this->orderBy('formulario_etapa','ASC');
        $this->orderBy('formulario_id','ASC');
        return  $this->find();
    }
    /**
     * devuelve  todas las respuestas de todas las etapas del aspirante 
     * @param aspirante corresponde as ID del aspirante
     */
    public function getAspiranteRespuestas($aspirante)
    {
        $this->where( 'respuesta_aspirante', $aspirante );
        $this->orderBy('formulario_etapa','ASC');
        $this->orderBy('formulario_id','ASC');
        return  $this->find();
    }

    /**
     * obtiene las respuestas de una etapa en particular de un aspirante en particular
     * @param aspirante el ID del aspirante (folio)
     * @param etapa el Id de la etapa a obtener
     * @param obligatorio (opcional) indica si se obtienen solo los obligatorios (true)
     * @return campos un array con los campos de la etapa
     */
    public function getByEtapa( $aspirante, $etapa, $obligatorio=false ) {
        $this->where( 'respuesta_aspirante', $aspirante );
        $this->where( 'formulario_etapa', $etapa );
        if ( $obligatorio ) {
            $this->where( 'campo_obligatorio', '1' );
        }
        return $this->findAll();
    }

    /**
     * busca en la lista de preguntas el campo que indique que deben ser visibles
     * @param aspirante corresponde al folio del aspirante 
     * 
     */
    public function getCamposVisibles($aspirante)
    {
      return $this->where('respuesta_aspirante',$aspirante)->where('campo_visualizar','1')->orderBy( 'formulario_orden', 'ASC' )->orderBy( 'campo_orden', 'ASC' )->find();
    }

    /**
     * @return query coleccion de respuestas con tipo de campo 16 ( coleccion de discapacidades)
     */
    public function getDiscapacidades()
    {   
        return $this->asArray()->where('tipocampo_id','16')->find();
    }

    /**
     * retorna respuestas correspondientes al id del campo
     * @param id_campo  corresponde al id del campo seleccionado
     * @return query arreglo de respuestas 
     */
    public function getRespuestasPorCampo($id_campo)
    {
        return $this->where('campo_id',$id_campo)->find();
    }

    /**
     * @param tipo_dato corresponde al nombre del tipo de dato seleccionado
     * @return query con la coleccion de respuestas correspondientes al campo buscado 
     */
    public function getRespuestaPorTipo($tipo_dato)
    {
        return $this->where('tipocampo_nombre',$tipo_dato)->orderBy('respuesta_valor','ASC')->find();
    }

    /**
     * obtiene las respuestas con observaciones
     * @param aspirante el ID del aspirante
     * @return respuestas un arreglo con las respuestas que tienen observaciones
     */
    public function getObservaciones($aspirante) {
        $this->where( 'respuesta_aspirante', $aspirante );
        $this->where( 'respuesta_comentario IS NOT NULL' );
        $this->where( "respuesta_comentario != '' " );
        $respuestas = $this->find();
        return $respuestas;
    }

    /**
     * obtiene todas las respuestas de un aspirante
     * @param aspirante el ID del aspirante
     * @return respuestas un arreglo con todas las respuestas del aspirante, o vacío si no existe
     */
    public function getAspirante( $aspirante ) {
        $this->where( 'respuesta_aspirante', $aspirante );
        $respuestas = $this->findAll();
        return $respuestas;
    }

    /**
     * obtiene todas las respuestas de un aspirante y las asocia a un arreglo con la llave del campo ID
     * @param aspirante el ID del aspirante
     * @return respuestas un arreglo con todas las respuestas del aspirante, o vacío si no existe
     */
    public function getAspiranteArray( $aspirante ) {
        $resp = $this->getAspirante( $aspirante );
        $respuestas = [];
        foreach ( $resp as $respuesta ) {
            $respuestas[ $respuesta->campo ] = $respuesta;
        }
        return $respuestas;
    }

    /**
     * 
     */
    public function obtenerModa(){
        $consulta =$this->selectCount('respuesta_aspirante')
             ->select('respuesta_valor')
             ->where('tipocampo_id','21')
             ->groupBy('respuesta_valor')
             ->orderBy('respuesta_valor', 'DESC')
             ->find();
        return $consulta;
    }

     /**
     * indica si un campo ha sido contestado por un aspirante, devolviendo el registro del campo contestado, o null si no existe
     * @param campo el ID del campo
     * @param aspirante el ID del aspirante
     * @return AspirantesRespuestas un objeto con la respuesta realizada
     * @return null en caso de que no se haya respondido
     */
    public function contestado($campo, $aspirante) {
        $this->where( 'respuesta_campo', $campo );
        $this->where( 'respuesta_aspirante', $aspirante );
        $respuestas = $this->find(); 
        if ( count($respuestas) ) {
            return $respuestas[0];
        } 
        return null;
    }
    
}