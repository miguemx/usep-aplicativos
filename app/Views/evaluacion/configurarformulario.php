<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<style>
    .error {
        border: red 2px solid;
    }
</style>
<div class="container">
    <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="datos text-center">
            <div class="datos text-center">
                <label style="margin: 0 10px">Titulo:<h4 class="card-title"> <?php echo $evaluacion->titulo;  ?></h4> </label>
                <label style="margin: 0 10px">Periodo:<h4 class="card-title"> <?php echo $evaluacion->periodo  ?></h4> </label>
                <label style="margin: 0 10px">Tipo :<h4 class="card-title">
                        <?php
                        foreach ($tipos as  $tipo) {
                            if ($tipo->id == $evaluacion->tipo)
                                echo $tipo->nombre;
                        }
                        ?>
                    </h4> </label>
                <label style="margin: 0 10px">Estado :<h4 class="card-title"> <?php echo $evaluacion->estatus;  ?></h4> </label>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <?php if ($edicion) : ?>
        <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <form action="<?php echo base_url('EvaluacionDocenteController/CrearFormulario/' . $evaluacion->id) ?>" method="get" id="form_evaluacion">
                <div class="mb-3">
                    <label for="titulo_formulario" class="form-label">Titulo del formulario</label>
                    <input type="text" class="form-control" name="titulo_formulario" id="titulo_formulario" aria-describedby="help_titulo" placeholder="Aspectos generales de la clase...">
                    <small id="help_titulo" class="form-text text-muted">Indique el nombre que tendra el formulario</small>
                </div>
                <div class="mb-3">
                    <label for="order_formulario" class="form-label">Orden</label>
                    <input type="number" class="form-control" name="order_formulario" id="order_formulario" aria-describedby="ordertext" min=1 max=20 value="<?php echo (($contador_formularios) ? count($contador_formularios) + 1 : 1) ?>">
                    <small id="ordertext" class="form-text text-muted">Seleccione el orde que tendrá este formulario</small>
                </div>
                <div class="mb-3">
                    <label for="instrucciones" class="form-label">Instrucciones del formulario</label>
                    <textarea class="form-control" name="instrucciones" id="instrucciones" rows="3"></textarea>
                </div>


                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="verificar('form_evaluacion','¿Seguro que desea generar un formulario para ' + '<?php echo $evaluacion->titulo;  ?>' + ' ?','Se generará un nuevo formulario')">Guardar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Formularios/' . $evaluacion->id) ?>" role="button">Cancelar</a>
                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        </div>
    <?php else : ?>
        <div class="bg-white rounded-3 shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
            <form action="<?php echo base_url('EvaluacionDocenteController/EditaFormulario/' . $formulario->id) ?>" method="get" id="form_update">
                <div class="mb-3">
                    <label for="titulo_formulario_update" class="form-label">Título del formulario</label>
                    <input type="text" class="form-control" name="titulo_formulario_update" id="titulo_formulario_update" aria-describedby="help_titulo" placeholder="Aspectos generales de la clase..." value="<?php echo $formulario->nombre ?>">
                    <small id="help_titulo" class="form-text text-muted">Indique el nombre que tendra el formulario</small>
                </div>
                <div class="mb-3">
                    <label for="order_formulario_update" class="form-label">Orden</label>
                    <input type="number" class="form-control" name="order_formulario_update" id="order_formulario_update" aria-describedby="ordertext" min=1 max=20 value="<?php echo $formulario->orden ?>">
                    <small id="ordertext" class="form-text text-muted">Seleccione el orde que tendrá este formulario</small>
                </div>
                <div class="mb-3">
                    <label for="instrucciones_update" class="form-label">Instrucciones del formulario</label>
                    <textarea class="form-control" name="instrucciones_update" id="instrucciones_update" rows="3"> <?php echo $formulario->instrucciones ?></textarea>
                </div>
                <div class="m-3">
                    <button type="button" class="btn btn-secondary" onclick="mensaje('form_update','¿Seguro que desea modificar el formulario <?php echo $formulario->nombre ?> para la evaluación ' + '<?php echo $evaluacion->titulo;  ?>' + ' ?','Se modificará el formulario')">Modificar</button>
                    <a name="btn_cancelar" id="btn_cancelar" class="btn btn-secondary" href="<?php echo base_url('EvaluacionDocenteController/Formularios/' . $evaluacion->id) ?>" role="button">Cancelar</a>
                </div>
                <input type="hidden" name="bandera" value="Menu">
            </form>
        </div>
    <?php endif; ?>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function verificar(submit,texto,confirm) {
        var titulo = document.getElementById('titulo_formulario');
        var orden = document.getElementById('order_formulario');
        var instrucciones = document.getElementById('instrucciones');
        var bandera = false;
        if (titulo.value == '') {
            titulo.classList.add('error');
            bandera = true

        } else {
            titulo.classList.remove('error');
        }

        if (orden.value <= 0) {
            orden.classList.add('error');
            bandera = true

        } else {
            orden.classList.remove('error');
        }
        if (!bandera) {
            mensaje(submit,texto,confirm);
        } else {
            Swal.fire({
                'title': 'Error',
                'text': 'Verifica los elementos marcados en rojo y vuelve a intentarlo',
                'icon': 'info',
                confirmButtonColor: '#840f31'
            });
        }

    }

    function mensaje(submit,texto,confirm) {
        Swal.fire({
            'title': 'Confirmación',
            'html': texto,
            'icon': 'question',
            confirmButtonColor: '#840f31',
            confirmButtonText: 'Continuar',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',


        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    'title': 'Enviado',
                    'text': confirm,
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
                document.getElementById(submit).submit();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire({
                    'title': 'Cancelado',
                    'text': 'No se realizó ningun movimiento',
                    'icon': 'info',
                    confirmButtonColor: '#840f31'
                });
            }
        });
    }
</script>
<?php echo $this->endSection() ?>