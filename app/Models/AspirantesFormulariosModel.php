<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesFormularios;
class AspirantesFormulariosModel extends Model
{
    protected $table='aspirantes_formularios';
    protected $primaryKey='formulario_id';
    protected $returnType='App\Entities\AspirantesFormularios';
    protected $useSoftDeletes = true;
    protected $allowedFields=['formulario_id', 'formulario_etapa','formulario_nombre','formulario_titulo','formulario_orden',
                              'formulario_instrucciones','created_at','updated_at','deleted_at'];
    protected $db;
    protected $builder;
    
    protected $useTimestamps = true;
    
    /**
     * busca todos los formularios de cierta etapa
     * @param etapa el ID de la etapa
     * @return formularios los formularios que corresponden a la etapa
     */
    public function getFromEtapa($etapa) {
        $this->where( 'formulario_etapa', $etapa );
        $this->orderBy( 'formulario_orden', 'ASC' );
        $formularios = $this->findAll();
        return $formularios;
    }
    
    /**
     * regresa el total de formularios activos
    */
    public function buscarFormularios()
    {
        $this->orderBy('formulario_orden','ASC');
        return $this->findAll();
    }

   
    public function formulariocompleto($id)
    {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_etapas', 'etapa_id=formulario_etapa', 'inner' );
        $this->builder->where('formulario_id',$id);
        $query = $this->builder->get();
        return $query->getResult();
    }
}

?>