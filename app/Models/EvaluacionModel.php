<?php

namespace App\Models;

use CodeIgniter\Model;
use Google\Service\CloudFunctions\Retry;

class EvaluacionModel extends Model
{
    protected $table      = 'aplicativo_evaluacion';
    protected $primaryKey = 'evaluacion_id';

    protected $returnType    = 'App\Entities\Evaluacion';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'evaluacion_id', 'evaluacion_periodo', 'evaluacion_estatus' , 'evaluacion_titulo', 'evaluacion_tipo','evaluacion_rol',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    /**
     * busca las evaluaciones exites de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros = array())
    {
       $this->creaFiltros($filtros);
       $evaluaciones =$this->findAll();
        if($evaluaciones){
            return $evaluaciones;
        }
        return false;
    }

    /**
     * crea los filtros where para la busqueda de alumnos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('id', $filtros)) { // -- filtrar por ID
            if (strlen($filtros['id'])) {
                $this->where('evaluacion_id', $filtros['id']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['periodo'])) {
                $this->where('evaluacion_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('titulo', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['titulo'])) {
                $this->like('evaluacion_titulo', trim($filtros['titulo']), 'both');
            }
        }
        if (array_key_exists('estatus', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['estatus'])) {
                $this->where('evaluacion_estatus', $filtros['estatus']);
            }
        }
        if (array_key_exists('tipo', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['tipo'])) {
                $this->where('evaluacion_tipo', $filtros['tipo']);
            }
        }
        if (array_key_exists('rol', $filtros)) { // -- filtrar por nombre
            if (strlen($filtros['rol'])) {
                $this->where('evaluacion_rol', $filtros['rol']);
            }
        }
        $this->orderBy('created_at', 'ASC');
    }


    /**
     * verefica si una evaluación está activa
     * @param periodo periodo de la evaluación
     * @param estado estado en el que se ecnuentra la evaluación
     * @param tipo típo de evaluación a verificar
     * @return existe contiene verdadero/falso
     */

    public function evaluacion_activa($periodo,$estado,$tipo){
        $consulta=$this ->where('evaluacion_periodo',$periodo)
                        ->where('evaluacion_estatus',$estado)
                        ->where('evaluacion_tipo',$tipo)
                        ->find();
        if (count($consulta)==1){
           return $consulta;
        }
        return false;
    }

    /**
     * busca una evaluación a partír de su id
     * @param evaluacion id de la evaluacion
     * @return consulta datos de la evaluacion
     */
    public function findById($evaluacion) {
        $consulta=$this->where('evaluacion_id',$evaluacion)
                       ->first();
        return $consulta;
    }
}
