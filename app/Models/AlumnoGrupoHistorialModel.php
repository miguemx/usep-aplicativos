<?php

namespace App\Models;

use CodeIgniter\Model;

class AlumnoGrupoHistorialModel extends Model
{
    protected $table      = 'view_alumno_grupos';
    protected $primaryKey = 'gpoalumno_alumno';

    protected $returnType    = 'App\Entities\AlumnoGrupo';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'gpoalumno_id','gpoalumno_alumno', 'gpoalumno_grupo', 'gpoalumno_ordinario', 'gpoalumno_extraordinario', 'gpoalumno_comentario', 'aprobada',
        'gpoalumno_flg_pago', 'gpoalumno_num_oficio', 'gpoalumno_autor', 'gpoalumno_fecha_modificacion', 'grupo_id',
        'grupo_idc', 'grupo_clave', 'grupo_folio', 'grupo_materia', 'grupo_docente', 'grupo_periodo', 'grupo_max',
        'grupo_ocupado', 'grupo_flag_acta', 'grupo_fechacaptura', 'grupo_fechaimpresion', 'materia_clave', 'materia_carrera',
        'materia_nombre', 'materia_nombre_corto', 'materia_creditos', 'materia_obligatoria', 'materia_tipo', 'materia_clasificacion',
        'materia_seriacion', 'materia_semestre', 'carrera_id', 'carrera_nombre', 'carrera_numero', 'carrera_clave', 'carrera_clave_programa',
        'carrera_facultad', 'periodo_id', 'periodo_nombre', 'periodo_inicio', 'periodo_fin', 'alumno_id', 'alumno_correo', 'alumno_nombre',
        'alumno_nombres', 'alumno_ap_paterno', 'alumno_ap_materno', 'alumno_curp', 'alumno_sexo', 'alumno_carrera', 'alumno_vigente',
        'alumno_else_regid', 'alumno_else_regpass', 'alumno_periodo', 'alumno_semestre', 'created_at', 'updated_at', 'deleted_at',
        'empleado_nombre',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * retorna el listado completo de todas las materias en la que ha estado cursando el alumno
     * @param id ID del aspirante
     */
    public function gethistoria($id)
    {
        return $this->where('gpoalumno_alumno',$id)->orderBy('grupo_materia','ASC')->find();
       // $query = $this->db->query("SELECT * FROM view_alumno_grupos WHERE gpoalumno_alumno ='$id' ");
        //return $query->getResultArray();
    }

    /**
     * retorna las materias cursadas por el alumno en un periodo determinado
     * @param periodo ID del periodo de busqueda
     * @param id ID del alumno
     */
    public function historialperiodo($id,$periodo){
        return $this->where('gpoalumno_alumno',$id)->where('grupo_periodo',$periodo)->find();
    }


    /**
     * retorna el registro completo de la materia solicitada del alumno
     * @param alumno ID del alumno
     * @param materia ID de materia
     */
    public function getByMateria($alumno,$materia)
    {
        return $this->where('gpoalumno_alumno',$alumno)->where('grupo_materia',$materia)->orderBy('grupo_materia','ASC')->find();
    }

}
