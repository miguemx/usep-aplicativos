<?php

namespace App\Entities;

use CodeIgniter\Entity;

class HistorialCalificaciones extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'IDC' => null,
        'Periodo' => null,
        'Ordinario' => null,
        'Extraordinaria' => null,
        'Seccion' => null,
        'Materia' => null,
        'ID_Docente' => null,
        'Nombre_docente' => null,
        'Martricula' => null,
        'Nombre_alumno' => null,
        'Correo_Alumno' => null,
        'Carrera' => null,
        'Aprobada' => null,
        'Materia_clave' => null,

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'IDC' => 'IDC',
        'Periodo' => 'Periodo',
        'Ordinario' => 'Ordinario',
        'Extraordinaria' => 'Extraordinaria',
        'Seccion' => 'Seccion',
        'Materia' => 'Materia',
        'ID_Docente' => 'ID_Docente',
        'Nombre_docente' => 'Nombre_docente',
        'Martricula' => 'Martricula',
        'Nombre_alumno' => 'Nombre_alumno',
        'Correo_Alumno' => 'Correo_Alumno',
        'Carrera' => 'Carrera',
        'Aprobada' => 'Aprobada',
        'Materia_clave' => 'Materia_clave',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
