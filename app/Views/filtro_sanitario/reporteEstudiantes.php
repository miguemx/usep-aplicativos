<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>
<style>
    body {
        padding: 40px 5px;
        font-family: 'Open Sans', sans-serif;
        background-color: #dcdcdc;
        height: 100vh;
        width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
        background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
        background-size: 794px 1100px;
        background-repeat: no-repeat;
        background-position: left;
       /*filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/
    }
    .QR {
        display: flex;
        justify-content: center;
    }
    .card-qr {
        max-width: 40rem;
        border-radius: 20px;
        background-color: whitesmoke;
    }
    .contenedor-qr {
        border-radius: 50px;
        max-width: 40rem;
        background-color: #dcdcdc;
    }
    .logo-cabecera {
        background-color: #841138;
        color: antiquewhite;
        text-align: center;
        border-top-left-radius: 20px !important;
        border-top-right-radius: 20px !important;
        padding: 20px;
        min-height: 100px;
        background-repeat: no-repeat;
        background-size: 200px;
        background-position: center;
        position: relative;
    }
    .correo{
        text-align: center;
    }
    .matricula{
        text-align: center;
    }
    .fecha{
        text-align: center;
    }
</style>
<div class="container contenedor-qr">
<?php
if (isset($msjexito)) : ?>
        <div class="alert alert-success" role="alert">
            <?php echo $msjexito; ?>
        </div>
<?php endif; ?>
    <div class="card card-qr">
        <div class="card-header logo-cabecera">
        <H1>Ingresa los datos solicitados</H1>
        </div>
        <div class="card-body QR">
            <form class="row g-3" method="post" action="<?php echo base_url('FiltroSanitario/guardareporte'); ?>">
                <div class="correo">
                    <label for="correo" class="form-label">Ingresa correo de estudiante</label>
                    <input type="text" class="form-control" style="justify-content:center" id="correo" name="correo">
                </div>
                <div class="matricula">
                    <label for="matricula" class="form-label">Ingresa matricula</label>
                    <input type="text" class="form-control" style="justify-content:center" id="matricula" name="matricula" ;?>
                </div>
                <div class="fecha">
                    <label for="fecha" class="form-label">Ingresa fecha de contagio (día 1)</label>
                    <br>
                    <input type="date" id="fecha" name="fecha"?>
                </div>
                <div class="busqueda" style="display:flex; justify-content:center">
                    <button type="submit" class="btn btn-secondary">Guardar</button>
                </div>
                <br>
            </form>
        </div>

    </div>
</div>

<?php echo $this->endSection() ?>