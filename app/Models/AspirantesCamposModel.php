<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesCampos;
class AspirantesCamposModel extends Model
{
    protected $table='aspirantes_campos';
    protected $primaryKey='campo_id';
    protected $returnType='App\Entities\AspirantesCampos';
    protected $useSoftDeletes = true;
    protected $allowedFields=['campo_id','campo_formulario','campo_tipo','campo_dependiente','campo_nombre','campo_descripcion','campo_orden','campo_obligatorio',
                               'campo_visualizar'];
    protected $db;
    protected $builder;
    protected $useTimestamps = true;

    /**
     * Obtiene todos los campos de un formulario determinado
     * @param idFormulario el ID del formulario
     * @return campos devuelve todos los campos relacionados con dicho formulario
     */
    public function getFromFormulario($idFormulario) {
        $this->where( 'campo_formulario', $idFormulario );
        $this->orderBy( 'campo_orden', 'ASC' );
        $campos = [];
        $rows = $this->findAll();
        foreach ( $rows as $row ) {
            $campos[ $row->campo ] = $row;
        }
        return $campos;
    }
    
    public function camposformularios($id_formulario)
    {
        return $this->where('campo_formulario',$id_formulario)->find();
    }
    public function camposdetalles($formulario)
    {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_tipos_campo', 'campo_tipo=tipocampo_id', 'inner' );
        $this->builder->where('campo_formulario',$formulario);
        $this->builder->where('deleted_at IS NULL');
        $this->builder->orderBy('campo_orden','ASC');
        $query = $this->builder->get();
        return $query->getResult();
    }

    /**
     * @return query retorna el total de campos  ordenado por el formulario perteneciente
     */
    public function getAllOrderByFormulario()
    {
        return $this->orderBy('campo_formulario','ASC')->orderBy('campo_orden','ASC')->find();
    }

    /**
     * obtiene todos los campos que corresponden a una etapa
     * @param etapa el Id de la etapa a obtener
     * @param obligatorio (opcional) indica si se obtienen solo los obligatorios (true)
     * @return campos un array con los campos de la etapa
     */
    public function getByEtapa( $etapa, $obligatorio=false ) {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'aspirantes_formularios', 'campo_formulario=formulario_id', 'inner' );
        $this->builder->join( 'aspirantes_etapas', 'formulario_etapa=etapa_id', 'inner' );
        $this->builder->where( 'formulario_etapa', $etapa );
        $this->builder->where( $this->table.'.deleted_at IS NULL' );
        if ( $obligatorio == true ) {
            $this->builder->where( 'campo_obligatorio', '1' );
        }
        $this->builder->orderBy( 'formulario_orden', 'ASC' );
        $this->builder->orderBy( 'campo_orden', 'ASC' );
        $query = $this->builder->get();
        return $query->getResult();
    }

    /**
     * @param id_tipo coresponde al id del tipocampo correspondiente
     * @return query contiene todos los campos del tipo solicitado
     * 
     */
    public function getCamposByTipo($id_tipo)
    {
        return $this->where('campo_tipo',$id_tipo)->find();
    }

}

?>