<?php
namespace App\Entities;

use CodeIgniter\Entity;

class RespuestaAspirante extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'aspirante' => null,
        'valor' => null,
        'comentario' => null,
        'campo' => null,
        'nombreCampo' => null,
        'descCampo' => null,
        'ordenCampo' => null,
        'obligatorio' => null,
        'visualizar' => null,
        'clasificacion' => null,
        'tipoCampo' => null,
        'tipoNombre' => null,
        'regex' => null,
        'formulario' => null,
        'etapa' => null,
        'nombreFormulario' => null,
        'tituloFormulario' => null,
        'ordenFormulario' => null,
        'instrucciones' => null,
        'nombreEtapa' => null,
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'respuesta_id',
        'aspirante' => 'respuesta_aspirante',
        'valor' => 'respuesta_valor',
        'comentario' => 'respuesta_comentario',
        'campo' => 'campo_id',
        'nombreCampo' => 'campo_nombre',
        'descCampo' => 'campo_descripcion',
        'ordenCampo' => 'campo_orden',
        'obligatorio' => 'campo_obligatorio',
        'visualizar' => 'campo_visualizar',
        'clasificacion' => 'tipocampo_clasificacion',
        'tipoCampo' => 'tipocampo_id',
        'tipoNombre' => 'tipocampo_nombre',
        'regex' => 'tipocampo_regex',
        'formulario' => 'formulario_id',
        'etapa' => 'formulario_etapa',
        'nombreFormulario' => 'formulario_nombre',
        'tituloFormulario' => 'formulario_titulo',
        'ordenFormulario' => 'formulario_orden',
        'instrucciones' => 'formulario_instrucciones',
        'nombreEtapa' => 'etapa_nombre',
        
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
