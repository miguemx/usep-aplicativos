<?php
namespace App\Controllers\Api;

use App\Models\AccesosBibliotecaModel;
use App\Models\EmpleadosModel;
use App\Models\AlumnosModel;
use App\Helpers\AccesosBiblioteca;

class AccesoBiblioteca extends BaseApi {

    private $accesosBiblioteca;
    private $accesosBibliotecaModel;
    private $alumnosModel;
    private $empleadosModel;

    public function __construct() {
        $this->accesosBiblioteca = new AccesosBiblioteca();
        $this->accesosBibliotecaModel = new AccesosBibliotecaModel();
        $this->alumnosModel = new AlumnosModel();
        $this->empleadosModel = new EmpleadosModel();
    }

    /**
     * devuelve un json con todos los usuarios registrados en el sistema
     */
    public function index() {
        $usuarios = $this->usuariosGeneralModel->findAll();
        $data = [
            'status' => 'ok',
            'message' => 'Usuarios listados correctamente.',
            'count' => count($usuarios),
            'results' => $usuarios
        ];
        $this->response->setHeader('xauth', 'bearer:JWT');
        return $this->respond($data,200);
    }

    /**
     * registra un acceso a la biblioteca
     */
    public function nuevo() {
        $this->response->setHeader('xauth', 'bearer:JWT');
        $code = 201;
        try {
            $info = $this->request->getJSON(true);
            $esAlumno = ( $info['rolid'] == '4' )? true: false;
            $folio = null;
            $acceso = $this->accesosBibliotecaModel->vigente( $info['id'], $esAlumno );
            if ( !$acceso ) {
                $user = null;
                if ( $esAlumno ) $user = $this->alumnosModel->find( $info['id'] );
                else $user = $this->empleadosModel->find( $info['id'] );
                $folio = $this->accesosBiblioteca->crea( $user, $esAlumno );
            }
            else {
                $folio = $acceso;
            }
            $registro = $this->accesosBiblioteca->entrada( $folio );
            if ( $registro ) {
                $data = [
                    'status' => 'ok',
                    'message' => 'Creado correctamente.',
                    'acceso' => $registro
                ];
            }
            else {
                $data = [
                    'status' => 'error',
                    'message' => $this->accesosBiblioteca->getError(),
                ];
                $code = 400;
            }
            return $this->respond($data,$code);
        }
        catch ( \Exception $ex ) {
            log_message( 'error', 'APIERROR: {ex}', [ 'ex'=>$ex ] );
            $data = [
                'status' => 'error', 'message'=>$ex->getMessage()
            ];
            return $this->respond($data,405);
        }
    }
    

}