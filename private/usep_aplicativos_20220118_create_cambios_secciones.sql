CREATE TABLE `usep_aplicativos`.`aca_cambios_secciones`(
    `cambios_matricula` VARCHAR(9) NOT NULL,
    `cambios_seccion_procedencia` VARCHAR(10) NOT NULL,
    `cambios_seccion_final` VARCHAR(10) NOT NULL,
    `cambios_oficio` VARCHAR(60) NOT NULL,
    `cambios_observaciones` TEXT NOT NULL,
    `cambios_usuario` INT(10) UNSIGNED NOT NULL,
    `cambios_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (cambios_matricula) REFERENCES alumnos_generales(alumno_id),
    FOREIGN KEY (cambios_usuario) REFERENCES aplicativo_empleados(empleado_id)
) ENGINE = INNODB;