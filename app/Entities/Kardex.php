<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Kardex extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'ordinario' => null,
        'extraordinario' => null,
        'aprobada' => null,
        'grupo' => null,
        'idc' => null,
        'seccion' => null,
        'folio' => null,
        'docente' => null,
        'periodo' => null,
        'periodoNombre' => null,
        'periodoInicio' => null,
        'periodoFin' => null,
        'materiaClave' => null,
        'materiaCarrera' => null,
        'materia' => null,
        'semestre' => null,
        'creditos' => null,
        'pago' => null,
        'estado' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'alumno' => 'gpoalumno_alumno',
        'ordinario' => 'gpoalumno_ordinario',
        'extraordinario' => 'gpoalumno_extraordinario',
        'aprobada' => 'aprobada',
        'pago' => 'gpoalumno_flg_pago',

        'grupo' => 'grupo_id',
        'idc' => 'grupo_idc',
        'seccion' => 'grupo_clave',
        'folio' => 'grupo_folio',
        'docente' => 'grupo_docente',

        'periodo' => 'periodo_id',
        'periodoNombre' => 'periodo_nombre',
        'periodoInicio' => 'periodo_inicio',
        'periodoFin' => 'periodo_fin',
        'estado' => 'periodo_estado',

        'materiaClave' => 'materia_clave',
        'materiaCarrera' => 'materia_carrera',
        'materia' => 'materia_nombre',
        'semestre' => 'materia_semestre',
        'creditos' => 'materia_creditos',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
