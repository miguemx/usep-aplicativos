<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<?php if (isset($alumnosarr)) : ?>
    <div class="container">
        <BR></BR>
        <div class="card">
            <div class="card-header">
                <h3>
                    Listado de  Alumnos que han firmado el RIPEL
                </h3>
            </div>
            <div class="card-body">

                <table class="table table-hover table-inverse table-fluid table-fluid">
                    <thead class="thead-default">
                        <tr>
                            <th>Matricula</th>
                            <th>Nombre</th>
                            <th>Carrera</th>
                            <th>Correo</th>
                            <th>Fecha</th>
                            <th>IP</th>
                            <th>Ultima conexion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($alumnosarr as $alumno) : ?>
                            <tr>
                                <td> <?php echo $alumno->ripel_matricula ?></td>
                                <td> <?php echo $alumno->alumno_ap_paterno . " " . $alumno->alumno_ap_materno . " " .$alumno->alumno_nombres ?></td>
                                <td> <?php echo $alumno->carrera_nombre ?></td>
                                <td> <?php echo $alumno->usuario_correo ?></td>
                                <td> <?php echo $alumno->ripel_fecha ?></td>
                                <td> <?php echo $alumno->ripel_ip ?></td>
                                <td> <?php echo $alumno->usuario_ultcnx ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <div class="card-footer text-muted">
                <form action="" method="post">

                </form>
                <form action="" method="post">

                </form>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php echo $this->endSection() ?>