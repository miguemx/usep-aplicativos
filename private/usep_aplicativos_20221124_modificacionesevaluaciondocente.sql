-- se agrega el campo de horas conducidas
ALTER TABLE `aca_materias` ADD `materia_horas_semanales` INT(5) NULL AFTER `materia_horas_conducidas`;

-- se llenan los datos con las horas conducidas
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-01";
update aca_materias set materia_horas_conducidas =75 where materia_clave ="LEO-02";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-03";
update aca_materias set materia_horas_conducidas =52 where materia_clave ="LEO-04";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-05";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-06";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-07";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-08";
update aca_materias set materia_horas_conducidas =72 where materia_clave ="LEO-09";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-10";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-11";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-12";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-13";
update aca_materias set materia_horas_conducidas =112 where materia_clave ="LEO-14";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-15";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-16";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-17";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-18";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-19";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-20";
update aca_materias set materia_horas_conducidas =112 where materia_clave ="LEO-21";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-22";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-23";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-24";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-25";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-26";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-27";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-28";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-29";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-30";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-31";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-32";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-33";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-34";
update aca_materias set materia_horas_conducidas =112 where materia_clave ="LEO-35";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-36";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-37";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-38";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-39";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-40";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-41";
update aca_materias set materia_horas_conducidas =112 where materia_clave ="LEO-42";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-43";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-44";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-45";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-46";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-47";
update aca_materias set materia_horas_conducidas =112 where materia_clave ="LEO-48";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-49";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-50";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-51";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="LEO-52";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-53";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="LEO-54";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="OPT01";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="OPT02";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="OPT03";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="OPT04";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-111";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-112";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-113";
update aca_materias set materia_horas_conducidas =72 where materia_clave ="MC-114";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-115";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-116";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-117";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-121";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-122";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-123";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-124";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-125";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-126";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-211";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-212";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-213";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="MC-214";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-215";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-216";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-218";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-221";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-222";
update aca_materias set materia_horas_conducidas =108 where materia_clave ="MC-223";
update aca_materias set materia_horas_conducidas =72 where materia_clave ="MC-224";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-225";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-226";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-228";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-311";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-312";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-313";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-314";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-315";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-316";
update aca_materias set materia_horas_conducidas =72 where materia_clave ="MC-317";
update aca_materias set materia_horas_conducidas =72 where materia_clave ="MC-318";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-321";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-322";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-323";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-324";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-325";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-326";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-327";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-328";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-411";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-412";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-414";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-415";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-416";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-417";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-418";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="MC-419";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-421";
update aca_materias set materia_horas_conducidas =90 where materia_clave ="MC-422";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-423";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-424";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-425";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-426";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="MC-427";
update aca_materias set materia_horas_conducidas =36 where materia_clave ="MC-428";
update aca_materias set materia_horas_conducidas =880 where materia_clave ="MC-511";
update aca_materias set materia_horas_conducidas =880 where materia_clave ="MC-521";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-111";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-112";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-113";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-114";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-115";
update aca_materias set materia_horas_conducidas =54 where materia_clave ="OPT-116";

-- se modifica la vista
ALTER
 ALGORITHM = UNDEFINED
DEFINER=`usep`@`localhost` 
 SQL SECURITY DEFINER
 VIEW `view_grupos_materias`
 AS SELECT
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_flag_acta` AS `grupo_flag_acta`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechacaptura` AS `grupo_fechacaptura`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechaimpresion` AS `grupo_fechaimpresion`,
    `usep_aplicativos`.`aca_grupos`.`grupo_historial_acta` AS `grupo_historial_acta`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`aca_materias`.`materia_clasificacion` AS `materia_clasificacion`,
    `usep_aplicativos`.`aca_materias`.`materia_seriacion` AS `materia_seriacion`,
    `usep_aplicativos`.`aca_materias`.`materia_semestre` AS `materia_semestre`,
    `usep_aplicativos`.`aca_materias`.`materia_horas_semanales` AS `materia_horas_semanales`,
    `usep_aplicativos`.`aca_materias`.`materia_horas_conducidas` AS `materia_horas_conducidas`,
    `usep_aplicativos`.`aca_periodos`.`periodo_id` AS `periodo_id`,
    `usep_aplicativos`.`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,
    `usep_aplicativos`.`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,
    `usep_aplicativos`.`aca_periodos`.`periodo_fin` AS `periodo_fin`,
    `usep_aplicativos`.`aca_periodos`.`periodo_estado` AS `periodo_estado`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` AS `empleado_id`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo` AS `empleado_correo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_horario` AS `empleado_horario`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_area` AS `empleado_area`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `empleado_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_apellido` AS `empleado_apellido`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `empleado_ap_materno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_categoria` AS `empleado_categoria`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_edad` AS `empleado_edad`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_sexo` AS `empleado_sexo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regid` AS `empleado_else_regid`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regidpass` AS `empleado_else_regidpass`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_flg_captura` AS `empleado_flg_captura`,
    `usep_aplicativos`.`aplicativo_empleados`.`created_at` AS `created_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`updated_at` AS `updated_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`deleted_at` AS `deleted_at`
FROM
    (
        (
            (
                `usep_aplicativos`.`aca_grupos`
            JOIN `usep_aplicativos`.`aca_materias` ON
                (
                    `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
                )
            )
        JOIN `usep_aplicativos`.`aca_periodos` ON
            (
                `usep_aplicativos`.`aca_periodos`.`periodo_id` = `usep_aplicativos`.`aca_grupos`.`grupo_periodo`
            )
        )
    LEFT JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` = `usep_aplicativos`.`aca_grupos`.`grupo_docente`
        )
    );