<?php echo $this->extend('plantillabase'); ?>

<?php echo $this->section('content') ?>
<script>
	function mostrar(dato) {
		if (dato == "1") {
			document.getElementById("defsexo").style.display = "block";
			document.getElementById("txtsexo").style.display = "block";
		}
		if (dato == "Femenino" || dato == "Masculino") {
			document.getElementById("defsexo").style.display = "none";
			document.getElementById("txtsexo").style.display = "none";
		}
	}
</script>
<style>
	* {
		padding: 0;
		margin: 0;
		box-sizing: border-box;
		font-family: 'Open Sans', sans-serif;
	}

	body {
		padding-top: 4.2rem;
		padding-bottom: 4.2rem;
		background: #dcdcdc;
		background-image: url(<?php echo base_url('img/fondozaca.png') ?>);
		background-size: 794px 1100px;
		background-repeat: no-repeat;
		background-position: left;
		/*filter: grayscale(100%) !important; //comentar/descomentar en vedas esta linea y la siguiente
        background: var(--grisFondo) !important;*/
	}

	.myform {
		position: relative;
		display: -ms-flexbox;
		display: flex;
		padding: 1rem;
		-ms-flex-direction: column;
		flex-direction: column;
		width: 100%;
		pointer-events: auto;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid rgba(0, 0, 0, .2);
		border-radius: 1.1rem;
		outline: 0;
		max-width: 500px;
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 0px;

	}

	input,
	select {
		margin-bottom: 10px;
	}

	.logo {
		background-image: url(<?php echo base_url('img/Logo_USEP_negativo.png') ?>);
		background-color: #841138;
		color: antiquewhite;
		text-align: center;
		padding: 20px;
		min-height: 180px;
		background-repeat: no-repeat;
		background-size: 300px;
		background-position: center;
		position: relative;
		border-top-left-radius: 18px;
		border-top-right-radius: 18px;
	}

	.announcement-top-bar {
		position: -webkit-sticky;
		position: -moz-sticky;
		position: -ms-sticky;
		position: -o-sticky;
		position: absolute;
		top: 0;
		width: 100%;
	}

	.container {
		margin-top: 20px;
		border-top-right-radius: 60px;
		border-top-left-radius: 60px;
		border-bottom-left-radius: 20px;
		border-bottom-right-radius: 20px;

	}

	.contenido {

		padding-left: 16px;
		padding-right: 16px;
		padding-top: 16px;
	}

	.announcement-top-bar p {
		margin: 0;
	}

	.announcement-top-bar.alert-dismissible .close {
		top: 50%;
		transform: translateY(-55%);
	}

	@media only screen and (max-width: 721px) {
		.container {
			margin-top: 60px;
		}
	}

	@media only screen and (max-width: 410px) {
		.container {
			margin-top: 80px;
		}
	}

	@media only screen and (max-width: 360px) {
		.container {
			margin-top: 130px;
		}
	}

	@media only screen and (max-width: 290px) {
		.container {
			margin-top: 130px;
		}
	}
</style>

<div class="alert alert-primary alert-dismissible fade show announcement-top-bar text-center" role="alert">
	<p class="d-none d-md-block">Si se presenta fiebre asociada con el resto de los sintomas,
		acuda a la unidad de salud más cercana a su domicilio y siga las indicaciones del personal médico.</p>
	<p class="d-block d-md-none">Si se presenta fiebre asociada con el resto de los sintomas,
		acuda a la unidad de salud más cercana a su domicilio y siga las indicaciones del personal médico.</p>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-6 mx-auto">
			<div class="myform form ">
				<div class="logo">

				</div>
				<div class="contenido">

				</div>
				<div class="text-center">
					<h3>CUESTIONARIO DE FILTRO SANITARIO</h3>

					<h5>Datos generales</h5>
					<br>
				</div>
				<form class="contenido" action="<?php echo base_url('FiltroSanitario/validaDatos'); ?>" method="post" name="login" id="contenedor1">
					<input type="hidden" name="varcorr" id="varcorr" value="<?php echo $varcorr; ?>" ;>
					<div class="form-group text-left">
						<label for="correo">Correo:</label>
						<input type="text" name="correo" id="correo" class="form-control" value="<?php echo $correo; ?>" placeholder="ejemplo@algo.com" required>
						</label>
					</div>
					<div class="form-group text-left required">
						<label for="nombre">Nombre:</label>
						<input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>" placeholder=" Nombre" required>
						</label>
					</div>
					<div class="form-group text-left required">
						<label for="nodependenciambre">Dependencia:</label>
						<input class="form-control" type="text" name="dependencia" id="dependencia" value="<?php echo $dependencia; ?>" placeholder=" DEPENDENCIA" required>
						</label>
					</div>


					<div class="form-group text-left required">
						<label for="categoria">Categoria: </label>
						<select class="custom-select" aria-label="Default select example" name="categoria" id="categoria" required>
							<option value="">Selecciona una opción</option>
							<option value="Servicios profesionales" <?php if ($categoria == "Servicios profesionales") echo 'selected="selected"' ?>>Servicios profesionales</option>
							<option value="Visitantes" <?php if ($categoria == "Visitantes") echo 'selected="selected"' ?>>Visitantes</option>
							<option value="Proveedor" <?php if ($categoria == "Proveedor") echo 'selected="selected"' ?>>Proveedor</option>
						</select>

					</DIV>
					<div class="form-group text-left required">
						<label for="edad">Edad: </label>
						<select class="custom-select" aria-label="Default select example" name="edad" id="edad" required>
							<option value="">Selecciona una opción</option>
							<option value="18 a 30" <?php if ($edad == "18 a 30") echo 'selected="selected"' ?>>18 a 30</option>
							<option value="31 a 40" <?php if ($edad == "31 a 40") echo 'selected="selected"' ?>>31 a 40</option>
							<option value="41 a 50" <?php if ($edad == "41 a 50") echo 'selected="selected"' ?>>41 a 50</option>
							<option value="61 y más" <?php if ($edad == "61 y más") echo 'selected="selected"' ?>>61 y más</option>
						</select>
					</div>
					<div class="form-group text-left required">
						<label for="sexo">Sexo: </label>
						<select class="custom-select" aria-label="Default select example" name="sexo" required onchange="mostrar(this.value);" id="sexo">
							<option value="">Selecciona una opción</option>
							<option value="M" <?php if ($sexo == "M") echo 'selected="selected"' ?>>Femenino</option>
							<option value="H" <?php if ($sexo == "H") echo 'selected="selected"' ?>>Masculino</option>
							<option value="1">Otro</option>
						</select>
					</div>

					<div class="form-group text-left required">
						<label class="custom-select" id="defsexo" style="display: none;">Define sexo</label>
						<input class="form-control" type="text" id="txtsexo" name="txtsexo" style="display: none;" placeholder="TIPO DE SEXO">

						<div class="col-12 text-center">
							<button type="submit" class="btn btn-secondary">Registra/valida</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php echo $this->endSection() ?>