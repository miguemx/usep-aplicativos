CREATE OR REPLACE VIEW view_asistencias_alumnos AS
SELECT
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_grupo` AS `asistencia_grupo`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_matricula` AS `asistencia_matricula`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_docente` AS `asistencia_docente`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_fecha` AS `asistencia_fecha`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_aula` AS `asistencia_aula`,
    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_tipo` AS `asistencia_tipo`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_id` AS `alumno_id`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_nombres` AS `alumno_nombres`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_paterno` AS `alumno_ap_paterno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_ap_materno` AS `alumno_ap_materno`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_status` AS `alumno_status`,
    `usep_aplicativos`.`alumnos_generales`.`alumno_periodo` AS `alumno_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `empleado_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `empleado_ap_materno`
FROM
    (
        (
            (
                (
                    `usep_aplicativos`.`aca_asistencias_diarias`
                JOIN `usep_aplicativos`.`alumnos_generales` ON
                    (
                        `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_matricula` = `usep_aplicativos`.`alumnos_generales`.`alumno_id`
                    )
                )
            JOIN `usep_aplicativos`.`aca_grupos` ON
                (
                    `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_grupo` = `usep_aplicativos`.`aca_grupos`.`grupo_id`
                )
            )
        JOIN `usep_aplicativos`.`aca_materias` ON
            (
                `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
            )
        )
    JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` = `usep_aplicativos`.`aca_asistencias_diarias`.`asistencia_docente`
        )
    );

INSERT INTO `aplicativo_roles` (`rol_id`, `rol_nombre`, `rol_menu`) VALUES (NULL, 'JEFATURA ACADEMICA', 'default');

INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'bibliotecas', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'capturacalificaciones');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'estudiantes', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'home', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'renderfiles', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'buscagrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'capturacalificacionesporgrupo');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'docente', '*');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'listaescolar');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'generalistapdf');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'consultatablalista');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'asistencia');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'consultahorario');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'conviertedias');
INSERT INTO `aplicativo_permisos` (`permiso_id`, `permiso_rol`, `permiso_modulo`, `permiso_funcion`) VALUES (NULL, '24', 'escolar', 'guardaasistencias');

UPDATE `aplicativo_usuarios` SET `usuario_rol` = '24' WHERE `aplicativo_usuarios`.`usuario_correo` = 'cinthya.garrigos@usalud.edu.mx';
UPDATE `aplicativo_usuarios` SET `usuario_rol` = '24' WHERE `aplicativo_usuarios`.`usuario_correo` = 'liliana.delarosa@usalud.edu.mx';