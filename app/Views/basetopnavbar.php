<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
  <div class="container-fluid py-1 px-3">
    <nav aria-label="breadcrumb">

      <!-- <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Template</li>
          </ol> -->
      <h6>Folio <strong><?php echo $aspirante->folio; ?></strong></h6>
      <!-- <h6 class="font-weight-bolder mb-0">Template</h6> -->
    </nav>

    <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
      <div class="ms-md-auto pe-md-3 d-flex align-items-center">
        <!-- <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div> -->
      </div>

      <ul class="navbar-nav  justify-content-end"  >

        <li class="nav-item d-xl-none ps-3 d-flex align-items-center" >
          <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav" >
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </a>
        </li>
        <li>&nbsp;</li>
        <li class="nav-item dropdown pe-2 d-flex align-items-center">
          <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-bell cursor-pointer" onclick="update_leido()"></i>
            <?php if ($notificaciones != '') : ?>
              <?php 
              $contador = 0;
                foreach($notificaciones as $notificacion){
                  $contador = ($notificacion->leido==null)? $contador+1 : $contador;
                } 
              ?>
              <?php if($contador>0): ?>
              <span class="position-absolute top-5 start-100 translate-middle badge rounded-pill bg-danger border border-white small py-1 px-2"  onclick="update_leido()">
                <span class="small"> <?php echo $contador ?></span>
                <span class="visually-hidden">Sin leer</span>
              </span>
              <?php endif; ?>
            <?php endif; ?>
          </a>
          <?php if ($notificaciones != '') : ?>
            <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton" style="width: 300px;" >
              <?php foreach ($notificaciones as $notificacion) : ?>
                <?php
                $url = 'javascript:;';
                if( !is_null($notificacion->formulario) ) {
                  $url = base_url('/Admision/Formulario').'/'.$notificacion->formulario;
                }
                ?>
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="<?php echo $url; ?>" style="white-space: normal;">
                    <div class="d-flex py-1">
                      <div class="my-auto">
                        <!-- img src="<?php echo base_url('img/icons/info.png'); ?>" class="avatar avatar-sm  me-3 "> -->
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold"><i class="fa fa-clock me-1"></i>
                          <?php $fecha = explode(' ', $notificacion->created_at);
                          echo $fecha[0]; ?></span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <?php $cabecera = explode(':', $notificacion->mensaje) ?>
                          <strong>
                            <?php echo $cabecera[0] . ":" ?>
                          </strong>
                          <?php echo $cabecera[1] ?>
                        
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </li>
      </ul>
    </div>
  </div>
</nav>
<script>
  var update_leido = async () => {
    let folio = <?php echo $aspirante->folio ?>;
    let response = await fetch('<?php echo base_url("Admision/updateAlerts") . "/" ?>' + folio, {
      method: 'POST',
    });
  }
</script>