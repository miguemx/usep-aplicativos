<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<style>
    .imagen {

        height: 50vh;
        width: 50vw;
        background-image: url(<?php echo base_url('img/working.png') ?>);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        opacity: 0.5;
    }
</style>
<div class="container">
    <div class="bg-white rounded shadow p-2 px-4 mb-4 mt-4 ml-2 mr-2 table-responsive-lg ">
        <div class="row">
        <div class="col align-middle">
            <!--<h1 class="display-3">Periodo en construccion</h1>
            <p class="lead">Actualmente no hay un periodo disponible para la consulta de horario, espera a que este modulo este disponible, gracias</p>-->
            <h1 class="display-3">Horario en construcción</h1>
        </div>
        <div class="col imagen">
          

        </div>
    </div>

</div>
<?php echo $this->endSection() ?>