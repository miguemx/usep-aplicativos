ALTER VIEW
    `view_grupos_materias` AS
SELECT
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_lunes` AS `grupo_lunes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_martes` AS `grupo_martes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_miercoles` AS `grupo_miercoles`,
    `usep_aplicativos`.`aca_grupos`.`grupo_jueves` AS `grupo_jueves`,
    `usep_aplicativos`.`aca_grupos`.`grupo_viernes` AS `grupo_viernes`,
    `usep_aplicativos`.`aca_grupos`.`grupo_sabado` AS `grupo_sabado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_domingo` AS `grupo_domingo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_periodos`.`periodo_id` AS `periodo_id`,
    `usep_aplicativos`.`aca_periodos`.`periodo_nombre` AS `periodo_nombre`,
    `usep_aplicativos`.`aca_periodos`.`periodo_inicio` AS `periodo_inicio`,
    `usep_aplicativos`.`aca_periodos`.`periodo_fin` AS `periodo_fin`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`aca_materias`.`materia_semestre` AS `materia_semestre`
FROM
  aca_grupos
  INNER JOIN aca_materias ON aca_materias.materia_clave = aca_grupos.grupo_materia
INNER JOIN aca_periodos ON aca_periodos.periodo_id = aca_grupos.grupo_periodo;

ALTER TABLE `aca_materias` ADD `materia_horas_totales` INT(10) NOT NULL DEFAULT '1' AFTER `materia_semestre`;