<?php

namespace App\Entities;

use CodeIgniter\Entity;

class AlumnoGrupo extends Entity
{

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        # 'alumno'=>null,
        # 'grupo'=>null,
        'ordinario' => null,
        'extraordinario' => null,
        'comentario' => null,
        'aprobada' => null,
        'pago' => null,
        'oficio' => null,
        'autor' => null,
        'modificacion' => null,
        'grupo' => null,
        'idc' => null,
        'seccion' => null,
        'folio' => null,
        # 'materia'=>null,
        'docente' => null,
        # 'periodo'=>null,
        'cupo' => null,
        'ocupacion' => null,
        'acta' => null,
        'captura' => null,
        'impresion' => null,
        'materia' => null,
        # 'carrera'=>null,
        'mat_nombre' => null,
        # ''=>null,
        'creditos' => null,
        'obligatoria' => null,
        'tipo' => null,
        'clasificacion' => null,
        'seriacion' => null,
        'semestre' => null,
        'carrera' => null,
        'car_nombre' => null,
        'numero' => null,
        'clave' => null,
        'programa' => null,
        'facultad' => null,
        'periodo' => null,
        'per_nombre' => null,
        'inicio' => null,
        'fin' => null,
        'alumno' => null,
        'correo' => null,
        'nombre' => null,
        'nombres' => null,
        'paterno' => null,
        'materno' => null,
        'curp' => null,
        'sexo' => null,
        # ''=>null,
        'vigente' => null,
        'regid' => null,
        'regpass' => null,
        # ''=>null,
        'semestre_alumno' => null,
        'empleado' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'gpoalumno_id',
        # 'alumno'=>'gpoalumno_alumno' ,------------------>foranea
        # 'grupo'=>'gpoalumno_grupo' ,-------------------->foranea
        'ordinario' => 'gpoalumno_ordinario',
        'extraordinario' => 'gpoalumno_extraordinario',
        'comentario' => 'gpoalumno_comentario',
        'aprobada' => 'aprobada',
        'pago' => 'gpoalumno_flg_pago',
        'oficio' => 'gpoalumno_num_oficio',
        'autor' => 'gpoalumno_autor',
        'modificacion' => 'gpoalumno_fecha_modificacion',
        'grupo' => 'grupo_id',
        'idc' => 'grupo_idc',
        'seccion' => 'grupo_clave',
        'folio' => 'grupo_folio',
        # 'materia'=>'grupo_materia', ---------------> foranea
        'docente' => 'grupo_docente',
        # 'periodo'=>'grupo_periodo', ---------------> foranea
        'cupo' => 'grupo_max',
        'ocupacion' => 'grupo_ocupado',
        'acta' => 'grupo_flag_acta',
        'captura' => 'grupo_fechacaptura',
        'impresion' => 'grupo_fechaimpresion',
        'materia' => 'materia_clave',
        # 'carrera'=>'materia_carrera', ------------------>foranea
        'mat_nombre' => 'materia_nombre',
        # ''=>'materia_nombre_corto',
        'creditos' => 'materia_creditos',
        'obligatoria' => 'materia_obligatoria',
        'tipo' => 'materia_tipo',
        'clasificacion' => 'materia_clasificacion',
        'seriacion' => 'materia_seriacion',
        'semestre' => 'materia_semestre',
        'carrera' => 'carrera_id',
        'car_nombre' => 'carrera_nombre',
        'numero' => 'carrera_numero',
        'clave' => 'carrera_clave',
        'programa' => 'carrera_clave_programa',
        'facultad' => 'carrera_facultad',
        'periodo' => 'periodo_id',
        'per_nombre' => 'periodo_nombre',
        'inicio' => 'periodo_inicio',
        'fin' => 'periodo_fin',
        'alumno' => 'alumno_id',
        'correo' => 'alumno_correo',
        'nombre' => 'alumno_nombre',
        'nombres' => 'alumno_nombres',
        'paterno' => 'alumno_ap_paterno',
        'materno' => 'alumno_ap_materno',
        'curp' => 'alumno_curp',
        'sexo' => 'alumno_sexo',
        # ''=>'alumno_carrera',----------------------------->foranea
        'vigente' => 'alumno_vigente',
        'regid' => 'alumno_else_regid',
        'regpass' => 'alumno_else_regpass',
        # ''=>'alumno_periodo',------------------------------->foranea
        'semestre_alumno' => 'alumno_semestre',
        'empleado' => 'empleado_nombre',
    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];
}
