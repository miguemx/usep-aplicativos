<?php echo $this->include('menus/_inicio'); ?>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url() ?>">Inicio</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Estudiantes
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/Estudiantes') ?>">Estudiantes activos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/EstudiantesBaja') ?>">Estudiantes no activos</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/ActasDeCalificaciones') ?>">Actas de Calificaciones</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Escolar/ReporteCalificaciones') ?>">Reportes de Calificaciones</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <!-- <li><a class="dropdown-item" href="<?php // echo base_url('Escolar/CargarCalificaciones') ?>">Horarios</a></li> -->
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/listaescolar') ?>">Lista de grupos</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/DatosRIPEL') ?>">Reporte RIPEL</a></li>
      </ul>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Admisión
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('ControlAdmision/ListadoAspirantes') ?>">Lista de Aspirantes</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="<?php echo base_url('EstadisticasAdmision/MenuEstadisticas') ?>">Estadisticas por Respuestas</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('ControlAdmision/InformacionGeneral') ?>">Descarga de información</a></li>
      </ul>    
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Docente
      </a>
      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        <li><a class="dropdown-item" href="<?php echo base_url('Escolar/CapturaCalificaciones') ?>">Captura de calificaciones</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/Horario'); ?>">Horarios</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/bitacoraAulas'); ?>">Bitacora de Aulas</a></li>
        <li>
          <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/asistencia'); ?>">Captura de asistencias</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/asistenciasextemporaneas'); ?>">Modificación de asistencias o captura extemporanea</a></li>
        <li><a class="dropdown-item" href="<?php echo base_url('Docente/resportesAsistencias'); ?>">Reportería de asistencias</a></li>
      </ul>
    </li>

    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Academia
      </a>
      <ul class="dropdown-menu mx-1" aria-labelledby="navbarDropdown">
        <div class="m-2">
          <strong>
            <span>Programación académica</span>
          </strong>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/Menu'); ?>">Mapa de Aulas</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/MenuDocentes/'); ?>">Horario por Docentes</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/HorarioSecciones/'); ?>">Horario por Secciones</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('ProgramacionAcademica/EspaciosDisponibles/'); ?>">Espacios Disponibles</a></li>
          <strong>
            <span>Evaluaciones</span>
          </strong>
          <li><a class="dropdown-item" href="<?php echo base_url('EvaluacionDocenteController/Menu/'); ?>">Evaluación Docente</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('EstadisticasEvaluacionDocente/participacionalumnos/'); ?>">Estadísticas de Evaluación Docente</a></li>
        </div>
      </ul>
    </li>


        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Voto electrónico
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
          <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/procesos'); ?>">Registro de procesos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/candidatos'); ?>">Registro de candidatos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/padron'); ?>">Registro de padrón</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/gestionprocesos'); ?>">Gestión de procesos</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Votaciones/estadisticas'); ?>">Consulta de estadísticas</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Herramientas
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="https://mail.google.com/" target="_blank">Mi correo</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Bibliotecas') ?>">Bibliotecas</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Filtro/Consulta') ?>">Reporte de Filtro Sanitario</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="<?php echo base_url('Login/Logout'); ?>">Cerrar Sesión</a></li>
          </ul>
        </li>
      </ul>
      <div style="color: #fff;">
            Bienvenido
            <?php if ( !is_null(session('nombre')) ):
                echo session('nombre');
            endif;  
            ?>
      </div>
    </div>



<?php echo $this->include('menus/_fin'); ?>