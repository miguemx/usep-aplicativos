<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">Matrícula</th>
            <th scope="col">Nombre</th>
            <th scope="col">Carrera</th>
            <th scope="col">Foto</th>
            <th scope="col">Identificación</th>
            <th scope="col">Documentos Válidos</th>
            <th scope="col">Revisada</th>
            <th scope="col">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($credenciales as $credencial): ?>
            <tr>
                <th scope="row"><?php echo $credencial['matricula']; ?></th>
                <td><?php echo $credencial['nombre']; ?></td>
                <td><?php echo $credencial['carrera']; ?></td>
                <td><?php echo $credencial['foto']; ?></td>
                <td><?php echo $credencial['identificacion']; ?></td>
                <td><?php echo $credencial['valido']; ?></td>
                <td><?php echo $credencial['revisada']; ?></td>
                <td>
                    <a href="<?php echo base_url('Credenciales/Validar/').'/'.$credencial['matricula']; ?>" class="btn btn-primary btn-sm">Ver / Validar</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>





<?php echo $this->endSection() ?>