<?php
namespace App\Libraries;

use App\Models\AspirantesTiposCampoModel;

class RespuestaValidacion {

    private $errors;
    private $tiposCampo;

    /**
     * @param tiposCampo obtiene la tabla de entidades de todos los tipos campo registrados
     */
    public function __construct($tiposCampo = null) {
        if(is_null($tiposCampo) ){
            $tipoCampoModel = new AspirantesTiposCampoModel();
            $tiposCampo= $tipoCampoModel->findAll();
        }
        $this->tiposCampo = $this->tiposArray( $tiposCampo );

    }

    /**
     * realiza la validación con base en el campo
     * @param campo la entidad App\Entities\AspirantesCampos 
     * @param valor el valor que se intenta introducir
     */
    public function do( $campo, $valor ) {
        $this->errors = [];
        $tiposCamposModel = new AspirantesTiposCampoModel();
        if ( $campo->obligatorio == '1' ) {
            if ( is_null($valor) || strlen($valor) === 0 ) {
                $this->errors[] = 'El campo <strong>'.$campo->nombre.'</strong> es requerido.';
                return false;
            }
        }
        if ( $valor ) {
            switch( $this->tiposCampo[ $campo->tipo ]->clasificacion ) {
                case 'BASIC': $this->basic( $this->tiposCampo[ $campo->tipo ], $valor, $campo->nombre ); break;
                case 'ARCHIVO': $this->archivo( $this->tiposCampo[ $campo->tipo ], $valor, $campo->nombre ); break;
                case 'COLECCION': $this->coleccion( $this->tiposCampo[ $campo->tipo ], $valor, $campo->nombre ); break;
                default: break;
            }
        }

        if ( count($this->errors) ) {
            return false;
        }
        return true;
    }

    /**
     * obtiene el arreglo de errores generado
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * valida un tipo de dato básico que refiere a una expresión regular
     * @param tipoCampo la entidad App\Entities\TipoCampo con los datos para validar
     * @param valor el valor a verificar
     * @param nombre (opcional) el nombre de campo
     */
    public function basic($tipoCampo, $valor, $nombre='') {
        $regex = $tipoCampo->valores;
        if ( !preg_match($regex, $valor) ) {
            $this->errors[] = 'El campo <strong>'.$nombre.'</strong> no tiene formato correcto.';
        }
    }

    /**
     * valida un tipo de dato archivo, donde buscará que esté dentro de las extensiones permitidas
     * @param tipoCampo la entidad App\Entities\TipoCampo con los datos para validar
     * @param valor el valor a verificar
     * @param nombre (opcional) el nombre de campo
     */
    public function archivo($tipoCampo, $valor, $nombre='') {
        $valores = explode( ',', mb_strtolower($tipoCampo->valores) );
        $fileNameParts = explode( '.', mb_strtolower($valor) );
        $parts = count($fileNameParts);
        if ( $parts < 2 ) {
            $this->errors[] = 'El archivo <strong>'.$nombre.'</strong> no es del formato correcto.';
        }
        else {
            $extension = $fileNameParts[ $parts-1 ];
            if ( !in_array($extension, $valores ) ) {
                $this->errors[] = 'El tipo de archivo para <strong>'.$nombre.'</strong> no es válido.';
            }
        }
    }

    /**
     * valida un tipo de dato de colección que buscará que esté en las opciones permitidas
     * @param tipoCampo la entidad App\Entities\TipoCampo con los datos para validar
     * @param valor el valor a verificar
     * @param nombre (opcional) el nombre de campo
     */
    public function coleccion($tipoCampo, $valor, $nombre='') {
        $tipos = explode( ',', $tipoCampo->valores) ;
        if ( !in_array($valor, $tipos ) ) {
            $this->errors[] = 'El valor para <strong>'.$nombre.'</strong> no es válido.';
        }
    }

    /**
     * convierte los tipos de campo en un array para su manejo más sencillo
     * @param tiposCampo todas las entidades App\Entites\TiposCampo existentes en la base de datos
     * @return tipos un array asociativo con los tipos campo ordenados por llaves
     */
    private function tiposArray($tiposCampo) {
        $tipos = [];
        foreach ( $tiposCampo as $tipoCampo ) {
            $tipos[$tipoCampo->id] = $tipoCampo;
        }
        return $tipos;
    }

}