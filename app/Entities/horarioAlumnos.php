<?php
namespace App\Entities;
use CodeIgniter\Entity;
class HorarioAlumnos extends Entity {
    protected $attributes= [
        'id' => null,
        'matricula' => null,
        'idc' => null,
        'idmateria' => null,
        'seccion' => null,
        'periodo' => null,
        'iddocente' => null,
        'correodocente' => null,
        'n_docente' => null,
        'app_docente' => null,
        'apm_docente' => null,
        'dia' => null,
        'h_ini' => null,
        'h_fin' => null,
        'aula' => null,
        'aul_nombre' => null,
        'materia' => null,
    ];

    protected $datamap= [
        'id'=>'gpoalumno_id',
        'matricula'=>'gpoalumno_alumno',
        'idc'=>'gpoalumno_grupo',
        'idmateria'=>'grupo_materia',
        'seccion'=>'grupo_clave',
        'periodo'=>'grupo_periodo',
        'iddocente'=>'grupo_docente',
        'correodocente'=>'empleado_correo',
        'n_docente'=>'empleado_nombre',
        'app_docente'=>'empleado_ap_paterno',
        'apm_docente'=>'empleado_ap_materno',
        'dia'=>'gpoaula_dia',
        'h_ini'=>'gpoaula_inicio',
        'h_fin'=>'gpoaula_fin',
        'aula'=>'aula_id',
        'aul_nombre'=>'aula_nombre',
        'materia'=>'materia_nombre',
    ];
    
}
?>