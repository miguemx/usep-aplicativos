


<?php if ($horario) : ?>
    <div class="container-fluid">
        <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
            <div class="text-center form-group mt-4">
                <h4><?php echo $aula_nombre  ?></h4>
            </div>
            <table class="table table-borderless  mt-3 text-center">
                <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                    <tr>
                        <th scope="row">HORA</th>
                        <th>LUNES</th>
                        <th>MARTES</th>
                        <th>MIERCOLES</th>
                        <th>JUEVES</th>
                        <th>VIERNES</th>
                        <th>SABADO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hora = 7;
                    ?>

                    <?php while ($hora < 21) : ?>

                        <tr>
                            <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                            <?php foreach ($dias as $dia) : ?>
                                <?php $bandera_coincidencia = false; ?>
                                <?php foreach ($grupos as $grupo) : ?>
                                    <?php if ($grupo->dia == $dia) : ?>
                                        <?php if ($grupo->h_inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                            <td id="<?php echo $grupo->grupo . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?>">
                                                <div>
                                                    <?php echo  $grupo->ma_nombre . '<br>' . (($grupo->docente == 1) ? 'Por asignar' : ($grupo->em_paterno . ' ' . $grupo->em_materno . ' ' . $grupo->em_nombre)) . "<br>" . $grupo->seccion ?>
                                                </div>
                                                <?php if ($grupo->carrera == $rol || $rol == 'administrador') : ?>
                                                    <?php if ($bandera_edicion) : ?>
                                                        <div>
                                                            <button class="btn" name="btn_editar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre . "|$dia"  ?>" onclick="editar(this)"><i class="bi bi-pencil-square" style="font-size: 1.2rem; color: grey;"></i></button>
                                                            <button class="btn" name="btn_eliminar" value="<?php echo $grupo->grupo . '|' . $dia . '|' . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "-" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $grupo->ma_nombre ?>" onclick="eliminar(this)"><i class="bi bi-trash container-fluid" style="font-size: 1.2rem; color: grey;"></i></button>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <?php $bandera_coincidencia = true; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (!$bandera_coincidencia) : ?>
                                    <td style="background-color: #bdb994;">
                                        <?php if ($bandera_edicion) : ?>
                                            <form action="<?php echo base_url('ProgramacionAcademica/SeleccionarGrupo/' . $periodo); ?>" method="post">
                                                <input type="hidden" name="posicion" value="<?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . "|" . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) . '|' . $aula_clave . '|' . $dia . '|' . $periodo . '|' . $rol ?>">
                                                <button type="submit" class="btn" name="bandera" value="<?php echo $bandera ?>"><i class="bi bi-plus-circle" style="font-size: 2rem; color: grey;"></i></button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                        <?php $hora++; ?>

                    <?php endwhile; ?>


                </tbody>
            </table>

        </div>
    </div>
<?php endif; ?>
