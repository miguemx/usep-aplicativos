<?php

namespace App\Controllers;


namespace App\Controllers;
//require 'vendor/autoload.php';
use \App\Models\AlumnosModel;
use \App\Models\UsuariosModel;

use \App\Models\GrupoModel;
use \App\Models\AlumnoGrupoHistorialModel;
use \App\Models\GrupoAlumnoModel;
use \App\Models\MateriaModel;
use \App\Models\AlumnosCalificacionModel;
use App\Models\AspirantesModel;
use App\Models\CredencialModel;
use App\Models\DocumentosAspiranteModel;
use App\Models\CapturaCalificacionesModel;
use App\Models\PeriodoModel;
use App\Models\GruposMateriasModel;
use App\Models\EmpleadosModel;
use \App\Entities\CapturaCalificaciones_en;
use \App\Entities\GrupoAlumno;
use \App\Entities\Grupo;
use App\Entities\Credencial;
use App\Entities\AlumnoGrupo;
use App\Entities\CambioSecciones;
use App\Models\ActaHistorial;
use PhpParser\Node\Stmt\Foreach_;
use App\Models\NombreMateriaModel;
use CodeIgniter\Debug\Toolbar\Collectors\History;
use Picqer\Barcode\BarcodeGeneratorJPG;
use App\Entities\Historial;
use App\Entities\MovimientosAlumnos;
use App\Models\CambioSeccionesModel;
use App\Models\HistorialCalificacionesModel;
use App\Models\HorarioAlumnosModel;
use App\Models\KardexModel;
use App\Models\ListaEscolarModel;
use App\Models\MovimientosAlumnosModel;
use App\Models\RipelModel;
use PhpParser\Node\Stmt\Else_;
use App\Models\HorarioDocentesModel;
use App\Models\AsistenciasDiariasModel;
use App\Entities\AsistenciasDiarias;
use App\Models\CarrerasModel;
use App\Models\ReporteCalificacionesModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Coordinate;
use CodeIgniter\Files\File;
use ZipArchive;

class Escolar extends BaseController
{
    private $periodoModel;
    private $HorarioDocentesModel;
    private $CapturaCalificacionesModel;
    private $AsistenciasDiarias;
    private $alumnosModel = null;
    private $gruposModel;
    private $materiasModel;
    private $alumnoHistorialModel;
    private $alumnoGrupo;
    private $grupoMateria;
    private $alumnoGrupoHistorial;
    private $carreraModel;
    private $reporteCalificacionesModel;
    private $listaEscolarModel;
    private $usuariosModel;

    public function __construct()
    {
        $this->periodoModel = new PeriodoModel();
        $this->HorarioDocentesModel = new HorarioDocentesModel();
        $this->CapturaCalificacionesModel = new CapturaCalificacionesModel();
        $this->AsistenciasDiarias = new AsistenciasDiariasModel();
        $this->alumnosModel = new AlumnosModel();
        $this->gruposModel = new GrupoModel();
        $this->materiasModel = new MateriaModel();
        $this->alumnoHistorialModel = new AlumnoGrupoHistorialModel();
        $this->alumnoGrupo = new GrupoAlumnoModel();
        $this->grupoMateria = new GruposMateriasModel();
        $this->alumnoGrupoHistorial = new AlumnoGrupoHistorialModel();
        $this->carreraModel = new CarrerasModel();
        $this->reporteCalificacionesModel = new ReporteCalificacionesModel();
        $this->listaEscolarModel = new ListaEscolarModel();
        $this->usuariosModel = new UsuariosModel();
    }

    /**
     * muestra la vista con el listado de estudiantes
     */
    public function estudiantes()
    {
        $pager = \Config\Services::pager();
        $alumnosModel = new AlumnosModel();
        $data = $this->request->getGet();
        if (!count($data) || !array_key_exists('id', $data)) {
            $data = ['id' => '', 'carrera' => '', 'apPaterno' => '', 'apMaterno' => '', 'nombre' => '', 'status' => ''];
        }
        $alumnos = $alumnosModel->buscar($data);
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['alumnos'] = $alumnos;
        $data['pager'] = $alumnosModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/alumnos', $data);
    }

    /**
     * cierra la sesión del escolar para ingresar con la del estudiante
     */
    public function estudiantePerfil()
    {
        $perm = $this->request->getPost('bd');
        $correoEscolar = $this->sesion->get('useremail');
        if (!is_null($perm) && $perm == 'entra') {
            $matricula = $this->request->getPost('matricula');
            $alumnosModel = new AlumnosModel();
            $alumno = $alumnosModel->find($matricula);
            if (!is_null($alumno)) {
                $usuariosModel = new UsuariosModel();
                $usuario = $usuariosModel->getSessionData($alumno->correo);
                if (!is_null($usuario)) {
                    $this->sesion->set('useremail', $usuario['correo']);
                    $this->sesion->set('nombre', $usuario['nombre']);
                    $this->sesion->set('rol', $usuario['rol']);
                    $this->sesion->set('rol_id', '4');
                    $this->sesion->set('menu', $usuario['menu']);
                    $this->sesion->set('id', $usuario['id']);
                    $this->sesion->set('trproced', $correoEscolar);
                    return redirect()->to(base_url());
                    die();
                }
            }
        }
        $this->sesion->destroy();
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * cierra la sesión del escolar para ingresar con la del estudiante
     */
    public function vuelvePerfil()
    {
        $perm = $this->request->getPost('prback');
        if (!is_null($perm)) {
            $usuariosModel = new UsuariosModel();
            $usuario = $usuariosModel->getSessionData($perm);
            if (!is_null($usuario)) {
                $this->sesion->set('useremail', $usuario['correo']);
                $this->sesion->set('nombre', $usuario['nombre']);
                $this->sesion->set('rol', $usuario['rol']);
                $this->sesion->set('menu', $usuario['menu']);
                $this->sesion->set('id', $usuario['id']);
                $this->sesion->set('rol_id', $usuario['rol_id']);
                return redirect()->to(base_url());
                // die();
            }
        }
        $this->sesion->destroy();
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * renderiza los formularios para carga de calificaciones y muestra el resultado si se detecta una solicitud post
     */
    public function cargarCalificaciones()
    {
        $data = $this->cargaArchivoCalificaciones();
        $proceso = $this->request->getPost('lay');
        if (array_key_exists('uploadedfile', $data)) {
            if ($proceso == 'layout') {
                $data = $this->cargaLayout($data);
            } else if ($proceso == 'educat') {
                $data = $this->cargaEducat($data);
            }
        }
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/cargarcalificaciones', $data);
    }

    /**
     * carga los datos de las calificaciones desde un archivo layout definido en el sistema
     */
    private function cargaLayout($data)
    {
        $grupoAlumnosModel = new GrupoAlumnoModel();
        $gruposModel = new GrupoModel();
        $alumnosModel = new AlumnosModel();
        $errores = '';
        try {
            $file = fopen($data['uploadedfile'], 'r');
            if ($file) {
                $i = 1;
                while ($datos = fgetcsv($file)) {
                    if ($i > 1) {
                        $grupo = $gruposModel->busca(['materia' => $datos[6], 'seccion' => $datos[0]]);
                        if (!is_null($grupo)) {
                            if (count($grupo) === 1) {
                                $alumno = $alumnosModel->find($datos[1]);
                                if (!is_null($alumno)) {
                                    $grupoAlumno = new GrupoAlumno();
                                    $grupoAlumno->alumno = $datos[1];
                                    $grupoAlumno->grupo = $grupo[0]->id;
                                    if (strtolower($datos[4]) == 'ordinario') {
                                        $grupoAlumno->ordinario = $datos[3];
                                    } else {
                                        $grupoAlumno->extraordinario = $datos[3];
                                    }
                                    $grupoAlumno->aprobada = ($datos[3] >= 6) ? '1' : '0';
                                    if (!$grupoAlumnosModel->existe($grupo[0]->id, $grupoAlumno->alumno)) {
                                        $grupoAlumnosModel->insert($grupoAlumno);
                                    }
                                } else {
                                    $errores  .= 'No se ha encontrado el alumno en la línea ' . $i . ' <br />';
                                }
                            } else {
                                $errores  .= 'Se ha detectado más de un grupo en la línea ' . $i . ' <br />';
                            }
                        } else {
                            $errores  .= 'El grupo no existe en la linea ' . $i . ' <br />';
                        }
                    }
                    $i++;
                }
                fclose($file);
            }
        } catch (\Exception $ex) {
            $data['error'] = "No se puede procesar el archivo. $ex";
        }
        if (strlen($errores)) $data['error'] = $errores;
        return $data;
    }

    /**
     * carga los datos de las calificaciones desde un archivo exportado de openeducat
     */
    private function cargaEducat($data)
    {
        $grupoAlumnosModel = new GrupoAlumnoModel();
        $gruposModel = new GrupoModel();
        $alumnosModel = new AlumnosModel();
        $grupoAlumno = new GrupoAlumno();
        $errores = '';
        try {
            $file = fopen($data['uploadedfile'], 'r');
            if ($file) {
                $i = 1;
                $grupo = NULL;
                while ($datos = fgetcsv($file)) {
                    $linea = trim(implode(',', $datos));
                    if (preg_match('/ordinario/', strtolower($linea))) {
                        $ord = true;
                    }
                    if (preg_match('/extraordinario/', strtolower($linea))) {
                        $ord = false;
                    }

                    if (preg_match("/^MED|^LEO/", $linea)) { // buscar el grupo siguiente
                        $datosGpo = explode(' ', $linea);
                        $seccion = $datosGpo[0];
                        if (preg_match("/^LMC/", $datosGpo[1])) { // la materia es de medicina
                            $datosMateria = explode('-', $datosGpo[1]);
                            $materia = 'MC-' . $datosMateria[1];
                        } else if (preg_match("/^LEO/", $datosGpo[1])) { // la materia es de enfermeria
                            $materia = 'LEO-' . substr($datosGpo[1], 3, 2);
                        } else {
                            $materia = $datosGpo[1];
                        }
                        $grupo = $gruposModel->encuentra($materia, $seccion);
                    } else { // intentar buscar al alumno
                        if ($grupo) {
                            try {
                                $datosLinea = explode(',', $linea);
                                $alumno = $alumnosModel->buscaPorCompleto($datosLinea[0]);
                                if (!is_null($alumno)) {
                                    $grupoAlumno = new GrupoAlumno();
                                    $grupoAlumno->alumno = $alumno->id;
                                    $grupoAlumno->grupo = $grupo->id;
                                    if ($ord) $grupoAlumno->ordinario = $datosLinea[3];
                                    if (!$ord) $grupoAlumno->extraordinario = $datosLinea[3];
                                    if ($grupoAlumno->ordinario >= 6 || $grupoAlumno->extraordinario >= 6) $grupoAlumno->aprobada = '1';
                                    if (!$grupoAlumnosModel->existe($grupo->id, $alumno->id)) {
                                        $grupoAlumnosModel->insert($grupoAlumno);
                                    } else {
                                        $grupoAlumnosModel->quita($grupoAlumno->alumno, $grupoAlumno->grupo);
                                        $grupoAlumnosModel->insert($grupoAlumno);
                                    }
                                } else {
                                    $errores .= "Linea $i:" . $datosLinea[0] . ' no fue encontrado debido a que no existe en la base de datos o bien por incompatibilidad de nombres.<br />';
                                }
                            } catch (\Exception $ex) {
                                $errores .= "Linea $i:" . $datosLinea[0] . ' No registrar calificación debido a ' . $ex->getMessage() . '<br />';
                            }
                        }
                    }
                    $i++;
                }
                fclose($file);
            }
        } catch (\Exception $ex) {
            $data['error'] = "No se puede procesar el archivo. $ex";
        }
        if (strlen($errores)) $data['error'] = $errores;
        return $data;
    }

    /**
     * sube el archivo para lectura de calificaciones
     */
    private function cargaArchivoCalificaciones()
    {
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $fileRules = ['layout' => 'uploaded[layout]'];
        $fileRules_errors = [
            'layout' => ['uploaded' => 'Por favor sube un archivo correcto',],
        ];
        try {
            $esValido = $this->validate($fileRules, $fileRules_errors);
            if ($esValido) {
                $newName = time() . '.csv';
                $file = $files['layout'];
                $file->move(WRITEPATH . 'uploads', $newName);
                if ($file->hasMoved()) {
                    $data['uploadedfile'] = WRITEPATH . 'uploads/' . $newName;
                }
            } else {
                $mensajes = $validation->getErrors();
                $data['error'] = $mensajes['layout'];
            }
        } catch (\Exception $ex) {
            $data['error'] = 'No se pudo procesar el archivo. ' . $ex->getMessage();
        }
        return $data;
    }

    /**
     * genera un CSV exportable con las calificaciones ya relacionadas
     */
    public function exportarCalificaciones()
    {
        $gruposModel = new GrupoModel();
        $acm = new AlumnosCalificacionModel();
        $grupos = $gruposModel->findAll();
        $data = '';
        foreach ($grupos as $grupo) {
            $alumnos = $acm->getGrupo($grupo->materia, $grupo->clave);
            $data .= "$grupo->materia,$grupo->clave\n";
            $i = 1;
            foreach ($alumnos as $alumno) {
                $calificacion = (!is_null($alumno->ordinario)) ? $alumno->ordinario : $alumno->extraordinario;
                $tipo = (!is_null($alumno->ordinario)) ? 'Ordinario' : 'Extraordinario';
                $data .= $i . ',' . $alumno->id . ',' . $alumno->apPaterno . ' ' . $alumno->apMaterno . ' ' . $alumno->nombres . ',';
                $data .= $calificacion . ',' . $this->getLetra($calificacion) . ',' . $tipo;
                $data .= "\n";
                $i++;
            }
        }
        $filename = date("Y-m-d_H_i_s") . '-calificaciones.csv';
        return $this->response->download($filename, $data);
    }

    /**
     * devuelve el numero con letra del 1 al 10 para calificacion
     */
    private function getLetra($num)
    {
        $letra = 'Cero';
        switch ($num) {
            case '1':
                $letra = "Uno";
                break;
            case '2':
                $letra = "Dos";
                break;
            case '3':
                $letra = "Tres";
                break;
            case '4':
                $letra = "Cuatro";
                break;
            case '5':
                $letra = "Cinco";
                break;
            case '6':
                $letra = "Seis";
                break;
            case '7':
                $letra = "Siete";
                break;
            case '8':
                $letra = "Ocho";
                break;
            case '9':
                $letra = "Nueve";
                break;
            case '10':
                $letra = "Diez";
                break;
        }
        return $letra;
    }

    public function actualizaCalificaciones()
    {
        //$arregloCalificaciones  = ['matricula'=> '','nombre'=> '','nofolio' => '','seccion'=> '','calificacion'=> '','tipo'=> '','comentario'=> ''];
        $seccionmateria = new CapturaCalificacionesModel();
        $gruposModel = new GrupoModel();
        $alumnosModel = new AlumnosModel();
        $data = ['nofolio' => '', 'matricula' => '', 'nombre' => '', 'seccion' => '', 'calificacion' => '', 'tipo' => '', 'secciones' => [], 'comentario' => '', 'bandera_captura' => '', 'arregloCalificaciones' => []];
        $botonagregar = $this->request->getPost('btnagrega');
        $matricula = $this->request->getPost('matricula');
        $confirma = $this->request->getPost('confirma');
        $nombre = $this->request->getPost('nombre_id');
        if (!is_null($matricula)) { // viene de una consulta
            $data['bandera_captura'] = '';
            if ($confirma) { // confirmar el guardado
                $gpm = new GrupoAlumnoModel();
                $data['bandera_captura'] = 'siguiente';
                //if (!$gpm->existe($this->request->getPost('seccion'), $matricula)) {

                $fechaactual = getdate();
                $fecha = $fechaactual["year"] . "/" . $fechaactual["mon"] . "/" . $fechaactual["mday"] . "/" . $fechaactual["hours"] . "/" . $fechaactual["minutes"];
                $grupoAlumno = new GrupoAlumno();
                ($grupoAlumno);
                $grupoAlumno->alumno = $matricula;
                $seccion = $this->request->getPost('seccion');
                $grupoAlumno->comentarios = $this->request->getPost('comentario');
                $grupoAlumno->autor = $this->sesion->get('id');
                $grupoAlumno->fecha = $fecha;
                $grupoAlumno->Nooficio = $this->request->getPost('nofolio');
                if ($this->request->getPost('tipo') === 'ordinario') {
                    $grupoAlumno->ordinario = $this->request->getPost('calificacion');
                } else {
                    $grupoAlumno->extraordinario = $this->request->getPost('calificacion');
                }
                if ($this->request->getPost('calificacion') > 5) {
                    $grupoAlumno->aprobada = '1';
                }
                if ($botonagregar == 1) {
                    $arregloCalificaciones  = [
                        [
                            'matricula' => $matricula, 'nombre' => $nombre,
                            'nofolio' => $this->request->getPost('nofolio'), 'seccion' => $this->request->getPost('seccion'),
                            'calificacion' => $this->request->getPost('calificacion'), 'tipo' => $this->request->getPost('tipo'),
                            'comentario' => $this->request->getPost('comentario')
                        ],
                        [
                            'matricula' => 1000, 'nombre' => $nombre,
                            'nofolio' => $this->request->getPost('nofolio'), 'seccion' => $this->request->getPost('seccion'),
                            'calificacion' => $this->request->getPost('calificacion'), 'tipo' => $this->request->getPost('tipo'),
                            'comentario' => $this->request->getPost('comentario')
                        ]
                    ];
                    $data = ['matricula' => '', 'nombre' => '', 'seccion' => '', 'calificacion' => '', 'tipo' => '', 'nofolio' => '', 'secciones' => [], 'comentario' => ''];
                    $data['arregloCalificaciones'] = $arregloCalificaciones;
                    $data['bandera_captura'] = '';
                    //$arregloCalificaciones= $arregloCalificaciones+$arregloCalificaciones;
                } else {
                    echo "entra actualizar";
                    try {
                        $gpm->actualizarCalificacion($matricula, $seccion, $grupoAlumno);
                        $data = ['matricula' => '', 'nombre' => '', 'seccion' => '', 'calificacion' => '', 'tipo' => '', 'nofolio' => '', 'secciones' => [], 'comentario' => '', 'arregloCalificaciones' => []];
                        $data['exito'] = 'Calificación agregada correctamente.';
                        $data['bandera_captura'] = '';
                    } catch (\Exception $ex) {
                        $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
                    }
                }
                /* else {
                    $data['error'] = 'faltan datos por capturar. ';
                }*/
            } else { // es una consulta
                $alumno = $alumnosModel->find($matricula);
                if (!is_null($alumno)) {
                    $seccionmaterias = $seccionmateria->buscamateria($matricula);
                    $data = [
                        'matricula' => $alumno->id,
                        'nombre' => $alumno->nombres . ' ' . $alumno->apPaterno . ' ' . $alumno->apMaterno,
                        'secciones' => $seccionmaterias,
                        'seccion' => $this->request->getPost('seccion'),
                        'calificacion' => $this->request->getPost('calificacion'),
                        'tipo' => $this->request->getPost('tipo'),
                        'nofolio' => "",
                        'comentario' => '',
                        'arregloCalificaciones' => []
                    ];
                    $data['bandera_captura'] = 'siguiente';
                    $data['exito'] = '<strong>Estudiante encontrado. Por favor confirme la sección y datos del estudiante</strong>.';
                } else {
                    $data['error'] = 'Estudiante no encontrado.';
                }
            }
        }

        // $data['secciones'] = $gruposModel->lista();
        $data['menu'] = $this->sesion->get('menu');
        $data['carrera'] = "";
        return view('escolar/actualizarcalificacion', $data);
    }

    /**
     * lee el archivo de calificaciones para poder guardar las notas de los alumnos
     */
    public function llenaCalificaciones($nombre)
    {
        $grupoAlumnosModel = new GrupoAlumnoModel();
        $gruposModel = new GrupoModel();
        $alumnosModel = new AlumnosModel();
        try {
            $file = fopen(WRITEPATH . 'uploads/' . $nombre . '.csv', 'r');
            if ($file) {
                $cabeceras = [];
                $linea0 = null;
                $i = 1;
                while ($linea = fgetcsv($file)) {
                    if ($i == 1) { // obtener los encabezados
                        $linea0 = $linea;
                        $j = 0;
                        foreach ($linea as $cabecera) {
                            $cabecera = mb_strtoupper(trim($cabecera));
                            switch ($cabecera) {
                                case 'MATRICULA':
                                    $cabeceras['MATRICULA'] = $j;
                                    break;
                                case 'SECCION 1':
                                    $cabeceras['SECCION1'] = $j;
                                    break;
                                case 'SECCION 2':
                                    $cabeceras['SECCION2'] = $j;
                                    break;
                                case '2020A':
                                    $cabeceras['2020A'] = $j;
                                    break;
                                case '2021B':
                                    $cabeceras['2021B'] = $j;
                                    break;
                            }
                            $j++;
                        }
                        $cabeceras['FIN'] = $j;
                    } else {
                        $matricula = $linea[$cabeceras['MATRICULA']];
                        $alumno = $alumnosModel->find($matricula);
                        if (!is_null($alumno)) {
                            $seccion = $linea[$cabeceras['SECCION1']];
                            for ($j = $cabeceras['2020A'] + 1; $j < $cabeceras['2021B']; $j++) {
                                $materia = $linea0[$j];
                                $calificacion = intval($linea[$j]);
                                $grupo = $this->obtenGrupo($materia, $seccion, '2020A');
                                $grupoAlumno = new GrupoAlumno();
                                $grupoAlumno->alumno = $matricula;
                                $grupoAlumno->grupo = $grupo->id;
                                $grupoAlumno->ordinario = $calificacion;
                                $grupoAlumno->aprobada = ($calificacion >= 6) ? '1' : '0';
                                if (!$grupoAlumnosModel->existe($grupo->id, $matricula)) {
                                    $grupoAlumnosModel->insert($grupoAlumno);
                                    echo " $seccion $materia -- $calificacion ||||||||||||| ";
                                } else {
                                    $grupoAlumnosModel->actualiza($grupoAlumno);
                                }
                            }
                            $seccion =  $linea[$cabeceras['SECCION2']];
                            for ($j = $cabeceras['2021B'] + 1; $j < $cabeceras['FIN']; $j++) {
                                if (strlen($linea0[$j])) {
                                    $materia = $linea0[$j];
                                } else {
                                    $materia = $linea[$cabeceras['FIN'] - 1];
                                }
                                $calificacion = intval($linea[$j]);
                                $grupo = $this->obtenGrupo($materia, $seccion, '2021B');
                                $grupoAlumno = new GrupoAlumno();
                                $grupoAlumno->alumno = $matricula;
                                $grupoAlumno->grupo = $grupo->id;
                                $grupoAlumno->ordinario = $calificacion;
                                $grupoAlumno->aprobada = ($calificacion >= 6) ? '1' : '0';
                                if (!$grupoAlumnosModel->existe($grupo->id, $matricula)) {
                                    $grupoAlumnosModel->insert($grupoAlumno);
                                    echo " $seccion $materia -- $calificacion ||||||||||||| ";
                                } else {
                                    $grupoAlumnosModel->actualiza($grupoAlumno);
                                }
                            }
                        } else {
                            echo "Linea $i: ERROR. Alumno no encontrado: $matricula <br />\n";
                        }
                    }
                    echo '<hr /> \n\n\n';
                    $i++;
                }
                fclose($file);
            }
        } catch (\Exception $ex) {
            echo '<hr /><hr /><hr /><hr /><hr /><hr /><hr />';
            var_dump($ex->getMessage(), $ex);
        }
    }

    /**
     * busca o crea un grupo de acuerdo a su materia y seccion
     */
    private function obtenGrupo($materia, $seccion, $periodo)
    {
        $gruposModel = new GrupoModel();
        $grupo = $gruposModel->encuentra($materia, $seccion, $periodo);
        if ($grupo) {
            return $grupo;
        }
        $id = substr($periodo, 0, 2);
        $id .= (substr($periodo, 4, 1) == 'A') ? '01' : '02';
        $id .= substr($materia, -3);
        $id .= substr($seccion, -3);
        $grupo = new Grupo();
        $grupo->id = $id;
        $grupo->clave = $seccion;
        $grupo->folio = 150150;
        $grupo->materia = $materia;
        $grupo->docente = '1';
        $grupo->periodo = $periodo;

        //var_dump($materia, $seccion, $periodo, $grupo->id);

        $gruposModel->insert($grupo);
        return $grupo;
    }

    /**
     * carga los horarios de tercer semestre desde el archivo de horarios pasado por escolar
     * para ser usado solo en 2021, a menos que se cambie
     */
    public function cargaHorario3($archivo)
    {
        $periodo = '2021A';
        $alumnosModel = new AlumnosModel();
        $grupoAlumnosModel = new GrupoAlumnoModel();
        $file = fopen(WRITEPATH . 'uploads/' . $archivo . '.csv', 'r');
        if ($file) {
            $l = 1;
            $claves = [];
            while ($linea = fgetcsv($file)) {
                if ($l === 1) {  // va iniciando y hay que obtener las cabeceras
                    foreach ($linea as $pos => $val) {
                        if (strlen($val)) $claves[$pos] = $val;
                    }
                } else {  // ya se pueden leer los datos
                    $matricula = $linea[0];
                    $alumno = $alumnosModel->find($matricula);
                    if (!is_null($alumno)) {
                        echo " +++++++++ Para el alumno $matricula: ";
                        foreach ($linea as $pos => $valor) {
                            if ($pos > 1) {
                                if ($valor === 'REINSCRIBIR') {
                                    $grupo = $this->obtenGrupo($claves[$pos], $linea[1], $periodo);
                                    echo " Inscribir en $grupo->id";
                                    $grupoAlumno = new GrupoAlumno();
                                    $grupoAlumno->alumno = $alumno->id;
                                    $grupoAlumno->grupo = $grupo->id;
                                    if (!$grupoAlumnosModel->existe($grupo->id, $alumno->id)) {
                                        $grupoAlumnosModel->insert($grupoAlumno);
                                        echo " Inscrito ::::: ";
                                    }
                                }
                            }
                        }
                    } else {
                        echo " ========= Linea $l: Alumno $matricula no encontrado.";
                    }
                }
                $l++;
                echo '<hr />';
            }
            fclose($file);
        }
    }
    /**
     * migra las fotos de los estudiantes que ya estaban inscritos y actualiza los registros de las credenciales
     */
    public function migraCedencialesAlumnos()
    {
        $credencialModel = new CredencialModel();
        $credenciales = $credencialModel->findAll();
        foreach ($credenciales as $credencial) {
            if (!is_null($credencial->foto)) {
                try {
                    $datosRuta = explode('/', $credencial->foto);
                    $file = $datosRuta[6];
                    $newdir = WRITEPATH . '/estudiantes/' . $credencial->alumno;
                    if (!is_dir($newdir)) mkdir($newdir);
                    if (copy(WRITEPATH . '/credenciales/' . $credencial->alumno . '-foto.jpg', $newdir . '/' . $credencial->alumno . '.jpg')) {
                        $credencial->foto = $credencial->alumno . '.jpg';
                        $credencialModel->save($credencial);
                        echo "$credencial->alumno -- ok.<br />";
                    } else {
                        echo "$credencial->alumno -- ERROR: No se pudo copiar la foto de $credencial->alumno <br />";
                    }
                } catch (\Exception $ex) {
                    echo $credencial->alumno . ' -- ERROR: ' . $ex->getMessage();
                }
            } else {
                echo $credencial->alumno . ' no tiene foto cargada';
            }
            echo '<hr />';
        }
    }

    /**
     * migra las fotos de los aspirantes que ya son alumnos y que necesitan tener las nuevas fotos
     */
    public function migraCedencialesAspirantes()
    {
        $aspirantesModel = new AspirantesModel();
        $credencialesModel = new CredencialModel();
        $documentosAspiranteModel = new DocumentosAspiranteModel();
        $aspirantes = $aspirantesModel->findAll();
        foreach ($aspirantes as $aspirante) {
            if ($aspirante->aceptado === '1') { // buscar aspirantes aceptados
                $alumnosModel = new AlumnosModel();
                $alumno = $alumnosModel->find($aspirante->id);
                if ($alumno) { // si ya es alumno inscrito, entonces crear y migrar sus credenciales
                    try {
                        $foto = $documentosAspiranteModel->getFoto($alumno->id);
                        if ($foto) {
                            $datosRuta = explode('/', $foto->ruta);
                            $file = $datosRuta[6];
                            $newdir = WRITEPATH . '/estudiantes/' . $alumno->id;
                            if (!is_dir($newdir)) mkdir($newdir);
                            if (copy(WRITEPATH . '/admision2021/' . $alumno->id . '.jpg', $newdir . '/' . $alumno->id . '.jpg')) {
                                $credencial = new Credencial();
                                $credencial->alumno = $alumno->id;
                                $credencial->foto = $alumno->id . '.jpg';
                                $credencial->vigencia = '01/2022';
                                $credencial->valida = '1';
                                $credencialesModel->insert($credencial);
                                echo "$credencial->alumno -- ok.<br />";
                            } else {
                                echo "$credencial->alumno -- ERROR: No se pudo copiar la foto <br />";
                            }
                        } else {
                            echo $alumno->id . ' -- ERROR: No hay foto';
                        }
                    } catch (\Exception $ex) {
                        echo $alumno->id . ' -- ERROR: ' . $ex->getMessage();
                    }
                    echo '<hr />';
                }
            }
        }
    }
    /**
     * muestra la vista de captura de calificaciones
     */
    public function capturaCalificaciones()
    {
        $modeloNombreMateria = new GruposMateriasModel();
        $docentes = [];
        $materiaNombre = [];
        $docente = $this->sesion->get('id');
        $periodo = $this->obtenerperiodo();
        if ($periodo == null) {
            $data['error'] = 'No se encuentra un periodo en estatus de CALIFICACIONES.';
        }
        $materiaNombre = $modeloNombreMateria->busquedageneral($periodo, $docente);
        $data['grupo'] = $materiaNombre;
        $data['idgrupo'] = "";
        $data['alumno'] = [];
        $data['docente'] = $docente;
        $data['grupo_flg_acta'] = "";
        $data['grupo_clave'] = "";
        $data['validacion'] = "";
        $data['menu'] = $this->sesion->get('menu');
        $data['informacion'] = "";
        $data['idc'] = "";
        $data['periodo'] = "";
        $data['seccion'] = "";
        $data['carrera'] = "";
        $data['folio'] = "";
        $data['docentes'] = $docentes;

        return view('escolar/capturarcalificacion', $data);
    }
    /**
     * busca grupos por id de profesor
     */
    public function buscagrupo()
    {
        $grupoclave = "";
        $docente = "";
        $idc = "";
        $carrera = "";
        $materia = "";
        $folio = "";
        $periodo = $this->obtenerperiodo();
        $alumnos = new CapturaCalificacionesModel();
        $materiasmodel = new GruposMateriasModel();
        $grupo_mod = new GrupoModel();
        $grupoid = $this->request->getpost('grupo');
        $docente = $this->request->getpost('docente');
        $idgrupo = "";
        $bandera_acta = "";
        $grupo_comp = $grupo_mod->obtenerBandera($grupoid);
        foreach ($grupo_comp as $bandera) {
            $bandera_acta = $bandera->acta;
        }

        $gpoalumno = $alumnos->buscarAlumno($grupoid, $periodo);
        $materiaNombre = $materiasmodel->busquedageneral($periodo, $docente);
        foreach ($gpoalumno as $pgoalumnos) {
            $idc = $pgoalumnos->grupoidc;
            $carrera = $pgoalumnos->carrera_nombre;
            $materia = $pgoalumnos->materia_nombre;
            $grupoclave = $pgoalumnos->grupo_clave;
            $folio = $pgoalumnos->grupo_folio;
        }
        $idgrupo = $this->request->getpost('grupo');
        $idgrupo = (($idgrupo) ? $idgrupo : '');
        $data['grupo'] = $materiaNombre;
        $data['idgrupo'] = $idgrupo;
        $data['alumno'] = $gpoalumno;
        $data['docente'] = $docente;
        $data['grupo_clave'] = $grupoid;
        $data['grupo_flg_acta'] = $bandera_acta;
        $data['informacion'] = $materia;
        $data['idc'] = $grupoid;
        $data['periodo'] = $periodo;
        $data['seccion'] = $grupoclave;
        $data['carrera'] = $carrera;
        $data['materia'] = $materia;
        $data['folio'] = $folio;
        $data['docentes'] = [];
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/capturarcalificacion', $data);
    }

    public function capturaCalificacionesporGrupo()
    {
        $folio = $this->request->getPost('folio');

        $data['idc'] = "";
        $data['periodo'] = "";
        $data['seccion'] = "";
        $data['carrera'] = "";

        $modeloNombreMateria = new GruposMateriasModel();

        $docente = $this->sesion->get('id');
        $periodo = $this->obtenerperiodo();
        $materiaNombre = $modeloNombreMateria->busquedageneral($periodo, $docente);
        $data['grupo'] = $materiaNombre;
        $data['alumno'] = [];
        $data['docente'] = $docente;
        $data['grupo_flg_acta'] = "";
        $data['grupo_clave'] = "";
        $data['validacion'] = "";
        $data['menu'] = $this->sesion->get('menu');
        $data['informacion'] = "";

        $grupoalumno = new GrupoAlumnoModel();
        $grupo =  $this->request->getPost('grupo_clave');
        $id =  $this->request->getPost('idalumno');
        $calif =  $this->request->getPost('calif');
        $id_extra =  $this->request->getPost('idalumnoextra');
        $calif_extra =  $this->request->getPost('califextra');
        /* echo "update $id[$i]<br>"; */

        for ($i = 0; $i < count($id); $i++) {
            if ($calif[$i] <= 5) {
                $status = 0;
            } else {
                $status = 1;
            }
            try {
                $arr_update = [
                    'gpoalumno_ordinario ' => $calif[$i],
                    'aprobada' => $status
                ];
                $resultados[] = $grupoalumno->updatecalificacion($id[$i], $grupo, $arr_update);
            } catch (\Exception $ex) {
                $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
            }
        }
        if ($id_extra) {
            for ($j = 0; $j < count($id_extra); $j++) {
                if ($calif_extra[$j] <= 5) {
                    $status2 = 0;
                } else {
                    $status2 = 1;
                }
                try {

                    $arr_update = [
                        'gpoalumno_ordinario' => '5',
                        'gpoalumno_extraordinario ' => $calif_extra[$j],
                        'aprobada' => $status2
                    ];
                    $resultados2[] = $grupoalumno->updatecalificacion($id_extra[$j], $grupo, $arr_update);
                } catch (\Exception $ex) {
                    $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
                }
            }
        }
        $respuesta = $this->arreglodatos($grupo, $folio);
        if ($respuesta == 1) {
            $grupoFlag = new GrupoModel();
            $grupoFlag->actaCapturada($grupo);
        }

        //Metodo para generacion de estructura json para guardar historico

        //regresa a la vista con el mensaje si logro insertar datos o tuvo algun problema
        return view('escolar/capturarcalificacion', $data);
    }
    /**
     * muestra la vista de captura de calificaciones
     */
    public function ActualizacionDeCalificaciones()
    {
        $modeloNombreMateria = new GruposMateriasModel();
        $docentes = [];
        $materiaNombre = [];
        $docentes = $this->encuentradocente();
        $data['grupo'] = $materiaNombre;
        $data['alumno'] = [];
        $data['grupo_flg_acta'] = "";
        $data['grupo_clave'] = "";
        $data['validacion'] = "";
        $data['menu'] = $this->sesion->get('menu');
        $data['informacion'] = "";
        $data['idc'] = "";
        $data['periodo'] = "";
        $data['seccion'] = "";
        $data['carrera'] = "";
        $data['folio'] = "";
        $data['docentes'] = $docentes;
        $data['docente'] = '';
        $data['estatus'] = 0;
        return view('escolar/actualizacalificacion', $data);
    }
    /**
     * busca todos los docentes existentes
     */
    public function encuentradocente()
    {
        $empleados = new EmpleadosModel();
        $docentes = $empleados->findAll();
        return $docentes;
    }
    /**
     * busca los grupos asignados por docente
     */
    public function ActualizacionDeCalificacionesGrupos()
    {
        $idc = "";
        $carrera = "";
        $materia = "";
        $folio = "";
        $seccion = "";

        $periodo = $this->obtenerperiodo();
        $alumnos = new CapturaCalificacionesModel();
        $materiasmodel = new GruposMateriasModel();
        $grupo_mod = new GrupoModel();


        $docente = $this->request->getPost('selectdocente');
        echo $docente;
        $estatus = $this->request->getPost('estatus');
        echo "status: " . $estatus;
        //die();
        $grupos = $this->request->getpost('grupo');
        if ($estatus == 0) {
            $materiaNombre = $materiasmodel->busquedageneral($periodo, $docente);
            $data['grupo'] = $materiaNombre;
            $data['alumno'] = [];
            $data['docente'] = $docente;
            $data['grupo_clave'] = "";
            $data['grupo_flg_acta'] = "";
            $data['informacion'] = "";
            $data['idc'] = "";
            $data['periodo'] = "";
            $data['seccion'] = "";
            $data['carrera'] = "";
            $data['materia'] = "";
            $data['folio'] = "";
            $data['docentes'] = $this->encuentradocente();
            $data['menu'] = $this->sesion->get('menu');
            $data['idgrupo'] = "";
            $data['estatus'] = 1;
        }
        if ($estatus == 1) {
            $docente = $this->request->getpost('docente');
            $materia = $materiasmodel->buscamateria($grupos);
            $idgrupo = "";
            $bandera_acta = "";
            foreach ($materia as $materias) {
                $idgrupo = $materias->grupo_id;
                $folio = $materias->folio;
            }
            $grupo_comp = $grupo_mod->obtenerBandera($idgrupo);
            foreach ($grupo_comp as $bandera) {
                $bandera_acta = $bandera->acta;
            }
            $gpoalumno = $alumnos->buscarAlumno($grupos, $periodo);
            $materiaNombre = $materiasmodel->busquedageneral($periodo, $docente);
            foreach ($gpoalumno as $pgoalumnos) {
                $seccion = $pgoalumnos->grupo_clave;
                $idc = $pgoalumnos->grupoidc;
                $carrera = $pgoalumnos->carrera_nombre;
                $materia = $pgoalumnos->materia_nombre;
            }
            $data['grupo'] = $materiaNombre;
            $data['idgrupo'] = $grupos;
            $data['alumno'] = $gpoalumno;
            $data['docente'] = $docente;
            $data['grupo_clave'] = $idgrupo;
            $data['grupo_flg_acta'] = $bandera_acta;
            $data['informacion'] = $materia;
            $data['idc'] = $idc;
            $data['periodo'] = $periodo;
            $data['seccion'] = $seccion;
            $data['carrera'] = $carrera;
            $data['materia'] = $materia;
            $data['folio'] = $folio;
            $data['docentes'] = $this->encuentradocente();
            $data['menu'] = $this->sesion->get('menu');
            $data['estatus'] = 0;
        }
        return view('escolar/actualizacalificacion', $data);
    }
    /**
     * actualiza calificaciones
     */
    public function capturacalificacionesServicios()
    {
        $grupoalumno = new GrupoAlumnoModel();
        $modeloNombreMateria = new GruposMateriasModel();
        $data['idc'] = "";
        $data['periodo'] = "";
        $data['seccion'] = "";
        $data['carrera'] = "";
        $docente = $this->sesion->get('id');
        $periodo = $this->obtenerperiodo();
        $materiaNombre = $modeloNombreMateria->busquedageneral($periodo, $docente);
        $data['grupo'] = $materiaNombre;
        $data['alumno'] = [];
        $data['docente'] = $docente;
        $data['grupo_flg_acta'] = "";
        $data['grupo_clave'] = "";
        $data['validacion'] = "";
        $data['menu'] = $this->sesion->get('menu');
        $data['informacion'] = "";
        $data['docentes'] = [];
        $grupo =  $this->request->getPost('grupo_clave');
        $id =  $this->request->getPost('idalumno');
        $calif =  $this->request->getPost('calif');
        $calif_extra =  $this->request->getPost('califextra');
        $ids =  $this->request->getPost('ids');
        $oficio =  $this->request->getPost('oficio');
        $comentario =  $this->request->getPost('comentario');

        $arreglodatos = [];
        $arreglodatosExtra = [];
        $ordinario = 0;
        $extra = 0;
        if ($comentario != null) {
            for ($i = 0; $i < count($comentario); $i++) {
                if ($comentario[$i] != null && $calif_extra[$i] != null) {
                    $arrayupdateExtra = [
                        'matricula' => $ids[$i],
                        'calificacion' => $calif_extra[$i],
                        'oficio' => $oficio[$i],
                        'comentario' => $comentario[$i]
                    ];
                    $arreglodatosExtra[] = $arrayupdateExtra;
                    $extra = 1;
                }
                if ($comentario[$i] != null && $calif_extra[$i] == null) {
                    $arrayupdate = [
                        'matricula' => $ids[$i],
                        'calificacion' => $calif[$i],
                        'oficio' => $oficio[$i],
                        'comentario' => $comentario[$i]
                    ];
                    $arreglodatos[] = $arrayupdate;
                    $ordinario = 1;
                }
            }
        }
        if ($ordinario == 1) {
            $fechaactual = getdate();
            $fecha = $fechaactual["year"] . "/" . $fechaactual["mon"] . "/" . $fechaactual["mday"] . "/" . $fechaactual["hours"] . "/" . $fechaactual["minutes"];
            for ($i = 0; $i < count($arreglodatos); $i++) {

                $matricula = $arreglodatos[$i]["matricula"];
                $calificacion = $arreglodatos[$i]["calificacion"];
                $oficio = $arreglodatos[$i]["oficio"];
                $comentario = $arreglodatos[$i]["comentario"];
                if ($calif[$i] <= 6) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                try {
                    $grupoalumno->actualizarCalificacionordinario($calificacion, $comentario, $status, $oficio, $docente, $fecha, $grupo, $matricula);
                } catch (\Exception $ex) {
                    $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
                }
            }
        }
        if ($extra == 1) {
            $fechaactual = getdate();
            $fecha = $fechaactual["year"] . "/" . $fechaactual["mon"] . "/" . $fechaactual["mday"] . "/" . $fechaactual["hours"] . "/" . $fechaactual["minutes"];
            for ($i = 0; $i < count($arreglodatosExtra); $i++) {
                $matricula = $arreglodatosExtra[$i]["matricula"];
                $calificacion = $arreglodatosExtra[$i]["calificacion"];
                $oficio = $arreglodatosExtra[$i]["oficio"];
                $comentario = $arreglodatosExtra[$i]["comentario"];
                if ($calif[$i] <= 6) {
                    $status = 0;
                } else {
                    $status = 1;
                }
                try {
                    $grupoalumno->actualizarCalificacionExtra($calificacion, $comentario, $status, $oficio, $docente, $fecha, $grupo, $matricula);
                } catch (\Exception $ex) {
                    $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
                }
            }
        }
        //llamar a funcion para guardar Json      
        $this->guardarJson($grupo, $fecha);

        return view('escolar/actualizacalificacion', $data);
    }

    /**
     * obtiene, genera y guarda Json en base de datos
     */
    public function guardarJson($idcgrupo, $fecha)
    {
        $historialmodel = new ActaHistorial();
        $historico = new Historial();
        $contador = $historialmodel->contador($idcgrupo);
        $n = count($contador);
        $datosacta = new CapturaCalificacionesModel();
        $alumnosjson = $datosacta->buscagrupoJson($idcgrupo, $fecha);
        $datos = $datosacta->buscaidcgrupo($idcgrupo);
        foreach ($datos as $dato) {
            $idc = $dato->gpoalumno_grupo;
            $periodo = $dato->grupo_periodo;
            $seccion = $dato->grupo_clave;
            $carrera = $dato->carrera_nombre;
            $clavemateria = $dato->grupo_materia;
            $nombremateria = $dato->materia_nombre;
            $facultad = $dato->carrera_facultad;
            $claveprograma = $dato->carrera_clave_programa;
            $docentenombre = $dato->empleado_apellido . " " . $dato->empleado_nombre;
            $docenteid = $dato->grupo_docente;
            $fechacaptura = $dato->fecha_captura;
            $nombre = $dato->apellido_p . " " . $dato->apellido_m . " " . $dato->nombres;
            $folio = $dato->grupo_folio;
        }
        foreach ($alumnosjson as $alumno) {
            if (is_null($alumno->gpo_extraordinario)) {
                $calificacion = $alumno->gpo_ordinario;
                $tipo = 'ORDINARIO';
            } else {
                $calificacion = $alumno->gpo_extraordinario;
                $tipo = 'EXTRAORDINARIO';
            }
            switch ($calificacion) {
                case 1:
                    $numeroletra = 'UNO';
                    break;
                case 2:
                    $numeroletra = 'DOS';
                    break;
                case 3:
                    $numeroletra = 'TRES';
                    break;
                case 4:
                    $numeroletra = 'CUATRO';
                    break;
                case 5:
                    $numeroletra = 'CINCO';
                    break;
                case 6:
                    $numeroletra = 'SEIS';
                    break;
                case 7:
                    $numeroletra = 'SIETE';
                    break;
                case 8:
                    $numeroletra = 'OCHO';
                    break;
                case 9:
                    $numeroletra = 'NUEVE';
                    break;
                case 10:
                    $numeroletra = 'DIEZ';
                    break;
                case 0:
                    $numeroletra = 'CERO';
                    break;
            }

            $listaalumnos = [
                'id_alumno'  => $alumno->gpoalumno,
                'nombre' => $alumno->apellido_p . " " . $alumno->apellido_m . " " . $alumno->nombres,
                'calificacion' => $calificacion,
                'letra' => $numeroletra,
                'tipo' => $tipo
            ];
            $listado[] = $listaalumnos;
        }

        $data['folio'] = $folio . "-" . $n;
        $data['idc'] = $idc;
        $data['periodo'] = $periodo;
        $data['seccion'] = $seccion;
        $data['carrera'] = $carrera;
        $data['clavemateria'] = $clavemateria;
        $data['nombremateria'] = $nombremateria;
        $data['facultad'] = $facultad;
        $data['claveprograma'] = $claveprograma;
        $data['docentenombre'] = $docentenombre;
        $data['docenteid'] = $docenteid;
        $data['fechacaptura'] = $fechacaptura;
        $alumnos['lista'] = $listado;
        $historico->id = $folio . "-" . $n;
        $historico->idc = $idc;
        $historico->historico = json_encode($data);
        $historico->alumnos = json_encode($alumnos);
        $historico->fecha = $fecha;
        try {
            $historialmodel->insert($historico);
            return redirect()->to('/escolares/capturacalificaion');
        } catch (\Exception $ex) {
            $data = 'No fue posible guardar el registro por duplicidad de datos. ' . $ex->getMessage();
        }
    }
    /**
     * genera el periodo actual haciendo comparación con el sistema
     */
    public function obtenerperiodo()
    {
        $periodo = $this->periodoModel->getPeriodoEstatus("CALIFICACIONES");
        $id = null;
        foreach ($periodo as $periodos) {
            $id = $periodos->periodo_id;
        }
        return $id;
    }
    public function ActasDeCalificaciones()
    {
        $data['id_docente'] = "";
        $data['carrera'] = "";
        $data['periodo'] = "";
        $data['grupo'] = "";
        $data['docente'] = "";
        $data['materia'] = "";
        $data['lista'] = [];
        $data['actas'] = [];
        $data['idcgrupo'] = "";
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/generadordeactas', $data);
        //$this->pdfConstancia();
        /*         if ( $url = $this->pdfConstancia() ) {
            return redirect()->to( $url );
        }
 */
    }
    public function ListaDeActas()
    {
        $consultagrupos = new GrupoModel();
        $data = $this->request->getPost();
        if (!count($data)) {
            $data = ['docente' => '', 'periodo' => '', 'seccion' => '', 'materia' => ''];
        }
        $datosgrupo = $consultagrupos->busca($data);
        $acta = new ActaHistorial();
        $resultado = $acta->tablacompleta();
        $arr_actas = [];
        foreach ($resultado as $key) {
            $auxiliar = json_decode($key['historial_json_acta'], true);
            $temp = [
                'id' => $key['historial_id'],
                'idc' => $key['historial_idc_grupo'],
                'folio' => $auxiliar['folio']
            ];
            $arr_actas[] = $temp;
        }
        $data['lista'] = $datosgrupo;
        $data['actas'] = $arr_actas;
        $data['id_docente'] = "";
        $data['carrera'] = "";
        $data['periodo'] = "";
        $data['grupo'] = "";
        $data['docente'] = "";
        $data['materia'] = "";
        $data['idcgrupo'] = "";
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/generadordeactas', $data);
    }
    public function generadoracta($id)
    {
        $acta = new ActaHistorial();
        $detalles = $acta->getarreglos($id);
        $total = [];
        $data = [];
        $alumnos = [];
        $idc = [];
        $fecha = [];
        foreach ($detalles as $key) {
            // $idc[] = $key['historial_idc_grupo'];
            $data[] = $key['historial_json_acta'];
            //$alumnos[] = $key['historial_json_alumnos'];
            //$fecha[] = $key['historial_fecha_capturada'];
            # code...
            $total[] = $key['historial_idc_grupo'] . "," . $key['historial_json_acta'] . "," . $key['historial_json_alumnos'] . "," . $key['historial_fecha_capturada'];

            $info_grupo = $key['historial_json_acta'];
            /* $uno[] = $key['historial_json_acta'];
            $uno['idc'] = $key['historial_idc_grupo'];
            $uno['folio'] = $key['historial_idc_grupo'];
            $dos[] = json_decode($key['historial_json_alumnos'], true); */
        }
        $arre_info = json_decode($info_grupo, true);

        $arre_alumnos = json_decode($key['historial_json_alumnos'], true);
        $datos_grupo['folio'] = $arre_info['folio'];
        $datos_grupo['periodo'] = $arre_info['periodo'];
        $datos_grupo['seccion'] = $arre_info['seccion'];
        $datos_grupo['carrera'] = $arre_info['carrera'];
        $datos_grupo['clavemateria'] = $arre_info['clavemateria'];
        $datos_grupo['nombremateria'] = $arre_info['nombremateria'];
        $datos_grupo['facultad'] = $arre_info['facultad'];
        $datos_grupo['claveprograma'] = $arre_info['claveprograma'];
        $datos_grupo['docentenombre'] = $arre_info['docentenombre'];
        $datos_grupo['docenteid'] = $arre_info['docenteid'];
        $datos_grupo['fechacaptura'] = $arre_info['fechacaptura'];
        $datos_grupo['total'] = $total;
        $datos_grupo['data'] = $data;
        $datos_grupo['idc'] = $arre_info['idc'];

        /*  echo "<pre>";
        var_dump($arre_alumnos);
        echo "</pre>";
        die(); */
        $this->gneradorpdf($datos_grupo, $arre_alumnos);
    }
    public function generadoractaCSV($id)
    {
        $id_sesion = $this->sesion->get('id');
        $acta = new ActaHistorial();
        $detalles = $acta->getarreglos($id);
        $total = [];
        $data = [];
        foreach ($detalles as $key) {
            $data[] = $key['historial_json_acta'];
            $total[] = $key['historial_idc_grupo'] . "," . $key['historial_json_acta'] . "," . $key['historial_json_alumnos'] . "," . $key['historial_fecha_capturada'];
            $info_grupo = $key['historial_json_acta'];
        }
        $arre_info = json_decode($info_grupo, true);
        $arre_alumnos = json_decode($key['historial_json_alumnos'], true);
        $contador = 1;
        $arr_csv = [];
        foreach ($arre_alumnos as $listas => $arr) {
            foreach ($arr as $key => $value) {
                $arr_csv[] = $contador . "," . $value['id_alumno'] . "," . $value['nombre'] . "," . $value['calificacion'] . "," . $value['letra'] . "," . $value['tipo'];
                $contador++;
            }
        }
        //$cfile = fopen($id_sesion . "-" . $arre_info['idc'] . "-" . $arre_info['folio'] . ".csv", 'w');

        //Inserting the table headers

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . $id_sesion . "-" . $arre_info['idc'] . "-" . $arre_info['folio'] . ".csv");
        $fp = fopen('php://output', 'wb');
        foreach ($arr_csv as $line) {
            $val = explode(",", $line);
            fputcsv($fp, $val);
        }
        fclose($fp);
    }

    public function arreglodatos($idcgrupo, $folio)
    {
        $datosacta = new CapturaCalificacionesModel();
        $datos = $datosacta->buscaidcgrupo($idcgrupo);
        foreach ($datos as $dato) {
            $idc = $dato->gpoalumno_grupo;
            $periodo = $dato->grupo_periodo;
            $seccion = $dato->grupo_clave;
            $carrera = $dato->carrera_nombre;
            $clavemateria = $dato->grupo_materia;
            $nombremateria = $dato->materia_nombre;
            $facultad = $dato->carrera_facultad;
            $claveprograma = $dato->carrera_clave_programa;
            $docentenombre = mb_strtoupper($dato->empleado_apellido . " " . $dato->empleado_nombre);
            $docenteid = $dato->grupo_docente;
            $fechacaptura = date('Y-m-d H:i:s');
            $nombre = $dato->apellido_p . " " . $dato->apellido_m . " " . $dato->nombres;
            if (is_null($dato->gpo_extraordinario)) {
                $calificacion = $dato->gpo_ordinario;
                $tipo = 'ORDINARIO';
            } else {
                $calificacion = $dato->gpo_extraordinario;
                $tipo = 'EXTRAORDINARIO';
            }
            switch ($calificacion) {
                case 1:
                    $numeroletra = 'UNO';
                    break;
                case 2:
                    $numeroletra = 'DOS';
                    break;
                case 3:
                    $numeroletra = 'TRES';
                    break;
                case 4:
                    $numeroletra = 'CUATRO';
                    break;
                case 5:
                    $numeroletra = 'CINCO';
                    break;
                case 6:
                    $numeroletra = 'SEIS';
                    break;
                case 7:
                    $numeroletra = 'SIETE';
                    break;
                case 8:
                    $numeroletra = 'OCHO';
                    break;
                case 9:
                    $numeroletra = 'NUEVE';
                    break;
                case 10:
                    $numeroletra = 'DIEZ';
                    break;
                case 0:
                    $numeroletra = 'CERO';
                    break;
            }
            $var = [
                'id_alumno'  => $dato->id_alumno,
                'nombre' => $nombre,
                'calificacion' => $calificacion,
                'letra' => $numeroletra,
                'tipo' => $tipo

            ];
            $al[] = $var;
        }
        $data['folio'] = $folio;
        $data['idc'] = $idc;
        $data['periodo'] = $periodo;
        $data['seccion'] = $seccion;
        $data['carrera'] = $carrera;
        $data['clavemateria'] = $clavemateria;
        $data['nombremateria'] = $nombremateria;
        $data['facultad'] = $facultad;
        $data['claveprograma'] = $claveprograma;
        $data['docentenombre'] = $docentenombre;
        $data['docenteid'] = $docenteid;
        $data['fechacaptura'] = $fechacaptura;
        $alumnos['lista'] = $al;
        $historico = new Historial();
        $historialmodel = new ActaHistorial();
        $historico->id = $folio;
        $historico->idc = $idc;
        $historico->historico = json_encode($data);
        $historico->alumnos = json_encode($alumnos);
        $historico->fecha = $fechacaptura;
        try {

            //$data['mensaje']= "mensaje desde el controlador";
            $historialmodel->insert($historico);
            return 1;
            //return redirect()->to('/escolares/capturacalificaion');
        } catch (\Exception $ex) {
            return 0;
        }
    }
    // La funcion toma como parametro un arreglo de datos para el encabezado y el pie de pagina asi como un arreglo de datos de alumnos para rellenar la lista y una bandera
    private function gneradorpdf($info, $alumnos)
    {
        $fechaactual = getdate();
        $fechaimpresion = $fechaactual["mday"] . "/" . $fechaactual["mon"] . "/" . $fechaactual["year"];

        $info['fechaimpresion'] = date('Y-m-d H:i:s');

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'letter', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);
        $idcgrupo = $info['idc'];
        //die();
        $generator = new BarcodeGeneratorJPG();
        $barcode = $generator->getBarcode($idcgrupo, $generator::TYPE_CODE_128);
        $id = $this->sesion->get('id');
        $info['codigo'] = base64_encode($barcode);
        $info['lista'] = $alumnos;
        $html =  view('escolar/actaporgrupo', $info);
        //return view('escolar/actaporgrupo', $info);

        $mpdf->WriteHTML($html);
        $mpdf->Output('Acta_de_grupo_' . $idcgrupo . '-' . $id . '-' . $info['folio'] . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);



        /* if ($bandera == 0) {
            # code...
        }else if ($bandera == 1) {
            return redirect()->to('/escolares/capturacalificaion');
            # code...
        }else if ($bandera == 2) {
            //redirigir a ventana modificaciones
            return redirect()->to('/escolares/capturacalificaion');
        } */
    }
    private function arreglo($data)
    {
        return json_decode($data, true);
    }
    private function updatejason($grupo, $folio, $dir_acta)
    {
        $grupomo = new GrupoModel();
        $rest =  $grupomo->printjson($grupo);
        $arr = [];
        //die();
        $json_datos = [
            'folio' => $folio,
            "grupo" => $grupo,
            "fecha" => date("y-m-d h:m:s"),
            "autor" => $this->sesion->get('id'),
            "ruta_acta" => $dir_acta
        ];
        //$sup_arr = json_encode($json_datos, JSON_UNESCAPED_SLASHES);
        if ($rest != null) {

            $arr = json_decode($rest, true);
        }
        $arr[] = $json_datos;

        //var_dump($arr," aaaa <br>", json_encode($arr));
        //die();

        try {
            $grupomo->agregarjson(json_encode($arr, JSON_UNESCAPED_SLASHES), $grupo);
        } catch (\Exception $ex) {
            $data = 'No fue posible guardar el registro del acta. ' . $ex->getMessage();
            var_dump($data);
        }
    }
    public function generadogrupos()
    {
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/generadorgrupos', $data);
    }
    public function mostrarpdf()
    {
        $grupo = $this->request->getPost('idc');
        $model = new GrupoModel();
        //$aux = json_decode ($model->printjson($grupo),true);
        $aux = $model->printjson($grupo);
        echo "<pre>";
        // var_dump( json_decode($aux),true);
        //var_dump( $aux);
        echo "</pre>";
        $arr = json_decode($aux, true);
        //echo $arr[0]->folio;
        foreach ($arr as $key => $value) {
            echo WRITEPATH . $value['ruta_acta'] . "<br>";
            $direcctorio = WRITEPATH . $value['ruta_acta'];
            header("Content-type: application/pdf");
            header('Content-Disposition: attachment; filename=' . $direcctorio);
            readfile($direcctorio);
        }
    }


    public function generador()
    {
        return view('estudiantes/bloques');
    }
    /**
     * muestra la vista de listas escolares
     */
    public function listaescolar_v1()
    {
        $periodo = $this->getPeriodo();
        $rol = $this->sesion->get('rol');
        //$rol = "ADMINISTRADORFILTROSANITARIO";
        $iddocente = $this->sesion->get('id');
        //$iddocente = 100084;
        $nrol = null;
        if ($rol === "DIRECTOR MEDICINA") {
            $nrol = 9;
        } elseif ($rol === "DIRECTOR ENFERMERIA") {
            $nrol = 10;
        } elseif ($rol === "GENERAL") {
            $nrol = 5;
        } elseif ($rol === "SERVICIOS ESCOLARES") {
            $nrol = 3;
        } elseif ($rol === "ADMINISTRADOR") {
            $nrol = 1;
        } elseif ($rol === "ADMINISTRADORFILTROSANITARIO") {
            $nrol = 11;
        } elseif ($rol === "SECRETARIOS") {
            $nrol = 12;
        }

        $listaescolarmodel = new ListaEscolarModel();
        if ($rol === "GENERAL") {
            $data = [
                'id' => '', 'seccion' => '', 'iddocente' => $iddocente, 'periodo' => '', 'idc' => '', 'periodo' => '', 'seccion' => '', 'carrera' => '', 'nombremateria' => '',
                'nombredocente' => '', 'idlista' => '', 'bandera' => $rol
            ];
        } else {
            $data = $this->request->getPost();
        }
        if (!$nrol) {
            $data = [
                'id' => '', 'seccion' => '', 'iddocente' => '', 'periodo' => '', 'idc' => '', 'periodo' => '', 'seccion' => '', 'carrera' => '', 'nombremateria' => '',
                'nombredocente' => '', 'idlista' => ''
            ];
            $data['alumnos'] = [];
        } elseif (!count($data) || !array_key_exists('id', $data)) {
            $data = [
                'id' => '', 'seccion' => '', 'iddocente' => '', 'periodo' => '', 'idc' => '', 'periodo' => '', 'seccion' => '', 'carrera' => '', 'nombremateria' => '',
                'nombredocente' => '', 'idlista' => ''
            ];
            $data['alumnos'] = [];
        } else {
            $alumnos = $listaescolarmodel->buscar($data, $nrol, $iddocente, $periodo);
            $data['alumnos'] = $alumnos;
        }
        $data['rol'] = $nrol;
        $data['periodos_disponibles'] = $this->periodoModel->lista();
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/listaescolar', $data);
    }

    /**
     * despliega la vista del listado escolar
     */
    public function listaescolar()
    {
        $periodo = $this->getPeriodo();
        $rol_sesion = $this->sesion->get('rol');
        $data = $this->request->getPost();
        $alumnos = [];
        $bandera = true;
        if (!count($data)) {
            if ($rol_sesion === 'GENERAL') {
                $bandera = false;
                $data = [
                    'id' => '', 'seccion' => '', 'iddocente' => $this->sesion->get('id'), 'periodo' => $periodo, 'idc' => '',   'carrera' => '', 'nombremateria' => '',
                    'nombredocente' => '', 'idlista' => '', 'bandera' => true, 'rol' => 'docente'
                ];
            } else if ($rol_sesion === 'DIRECTOR ENFERMERIA' || $rol_sesion === 'SUBDIRECCION ENFERMERIA') {
                $data = [
                    'id' => '', 'seccion' => '', 'iddocente' => '', 'periodo' => '', 'idc' => '',   'carrera' => '1', 'nombremateria' => '',
                    'nombredocente' => '', 'idlista' => '', 'rol' => 'enfermeria'
                ];
            } else if ($rol_sesion === 'DIRECTOR MEDICINA' || $rol_sesion === 'SUBDIRECCION MEDICINA') {
                $data = [
                    'id' => '', 'seccion' => '', 'iddocente' => '', 'periodo' => '', 'idc' => '',   'carrera' => '2', 'nombremateria' => '',
                    'nombredocente' => '', 'idlista' => '', 'rol' => 'medicina'
                ];
            } else if ($rol_sesion === 'ADMINISTRADOR' || $rol_sesion === 'SECRETARIOS' || $rol_sesion === 'ADMINISTRADORFILTROSANITARIO' || $rol_sesion === 'SERVICIOS ESCOLARES') {
                $data = [
                    'id' => '', 'seccion' => '', 'iddocente' => '', 'periodo' => '', 'idc' => '',  'carrera' => '', 'nombremateria' => '',
                    'nombredocente' => '', 'idlista' => '', 'rol' => 'todos'
                ];
            }
        }
        // var_dump($data);
        $alumnos = $this->listaEscolarModel->buscar($data);
        $data['alumnos'] = $alumnos;
        $data['rol_bandera'] = $bandera;
        $data['periodos_disponibles'] = $this->periodoModel->lista();
        $data['pager'] = $this->listaEscolarModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/listaescolar', $data);
    }

    public function consultatablalista()
    {
        $listaescolarmodel = new ListaEscolarModel();
        $id = ['id' => $this->request->getPost('matricula')];
        //var_dump($this->request->getPost('matricula'));

        $carrera = "";
        $alumnos = $listaescolarmodel->buscargrupo($id);
        foreach ($alumnos as $alumno) {
            $info['folio'] = $alumno->folio_grupo;
            $info['idc'] = $alumno->id_grupo;
            $info['periodo'] = $alumno->periodo_grupo;
            $info['seccion'] = $alumno->clave_grupo;
            if ($alumno->numero_materia === '1') {
                $carrera = 'LIC. EN ENFERMERÍA Y OBSTETRICIA';
            } elseif ($alumno->numero_materia === '2') {
                $carrera = 'LIC. EN MEDICO CIRUJANO';
            }
            $info['carrera'] = $carrera;
            $info['clavemateria'] = $alumno->clave_materia;
            $info['nombremateria'] = $alumno->nombre_materia;
            $info['alumnos'] = $alumnos;
            $info['menu'] = $this->sesion->get('menu');
        }
        return view('escolar/tabla_listaescolar', $info);
    }

    public function consultatablalistaExcel($id, $nombre_archivo)
    {
        $listaescolarmodel = new ListaEscolarModel();
        $carrera = "";
        $alumnos = $listaescolarmodel->buscargrupo($id);
        // var_dump($alumnos);
        // die();
        foreach ($alumnos as $alumno) {
            $info['folio'] = $alumno->folio_grupo;
            $info['idc'] = $alumno->id_grupo;
            $info['periodo'] = $alumno->periodo_grupo;
            $info['seccion'] = $alumno->clave_grupo;
            if ($alumno->numero_materia === '1') {
                $carrera = 'LIC. EN ENFERMERÍA Y OBSTETRICIA';
            } elseif ($alumno->numero_materia === '2') {
                $carrera = 'LIC. EN MEDICO CIRUJANO';
            }
            $info['carrera'] = $carrera;
            $info['clavemateria'] = $alumno->clave_materia;
            $info['nombremateria'] = $alumno->nombre_materia;
            $info['alumnos'] = $alumnos;
            $info['menu'] = $this->sesion->get('menu');
        }
        header("Content-Type:   application/vnd.ms-excel; charset=utf-16");
        header("Content-Disposition: attachment; filename= Listaescolar-$nombre_archivo-" . date('Ymd') . ".xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        return view('escolar/tabla_listaescolarexcel', $info);
    }

    public function generalistapdf()
    {
        $listaescolarmodel = new ListaEscolarModel();
        $id = ['id' => $this->request->getPost('matricula')];
        //var_dump($this->request->getPost('matricula'));
        $contador = 0;
        $carrera = "";
        $nuevoalumno = [];
        $alumnos = $listaescolarmodel->buscargrupo($id);
        foreach ($alumnos as $alumno) {
            $contador = $contador + 1;
            $info['folio'] = $alumno->folio_grupo;
            $info['idc'] = $alumno->id_grupo;
            $idc = $alumno->id_grupo;
            $info['periodo'] = $alumno->periodo_grupo;
            $info['seccion'] = $alumno->clave_grupo;
            if ($alumno->numero_materia === '1') {
                $carrera = 'Lic. en Enfermería y Obstetricia';
            } elseif ($alumno->numero_materia === '2') {
                $carrera = 'Lic. en Medico Cirujano';
            }
            $info['carrera'] = $carrera;
            $info['docente'] = $alumno->nombre_docente . " " . $alumno->app_docente . " " . $alumno->apm_docente;
            $info['clavemateria'] = $alumno->clave_materia;
            $info['nombremateria'] = $alumno->nombre_materia;
            $info['alumnos'] = $alumnos;
            //echo "entra";
            if ($contador > 43) {
                //echo $contador."<br>";
                $nuevoalumno[] = [
                    'id' => $alumno->gpoalumno,
                    'nombre' => $alumno->app_estudiante . " " . $alumno->apm_estudiante . " " . $alumno->nombre_estudiante
                ];
            }
        }
        //var_dump($nuevoalumno);
        if ($nuevoalumno) {
            $info['nuevoalumno'] = $nuevoalumno;
        }

        //var_dump(count($alumnos));

        // die();
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'letter', 'mode' => 'utf-8', 'collapseBlockMargins' => true, 'tempDir' => WRITEPATH, 'allow_output_buffering' => true,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);

        $info['lista'] = $alumnos;
        $html =  view('escolar/listaescolarpdf', $info);
        $mpdf->WriteHTML($html);
        $mpdf->Output('lista_escolar_' . $idc . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
        die();
    }
    public function getPeriodo()
    {
        $periodo = $this->periodoModel->getPeriodoEstatus("CURSO");
        $id = null;
        foreach ($periodo as $periodos) {
            $id = $periodos->periodo_id;
        }
        return $id;
    }

    /**
     * Despliga la vista principal para el cambio de seccion
     */
    public function secciones()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['busqueda'] = true;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/alumnossecciones', $data);
    }

    /**
     * desoliega las materias actuales del alumno buscado y te permite elegir la seccion a la que se cambiará
     */
    public function alumnoSeccionActual()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['menu'] = $this->sesion->get('menu');
        $matricula = $this->request->getPost('matricula');
        $horario = new AlumnoGrupoHistorialModel();
        $alumno = $this->alumnosModel->find($matricula);
        $data['alumno'] = $alumno;
        $info = $horario->historialperiodo($matricula, $alumno->periodo);
        $data['secciones'] = $this->gruposModel->getSeccionesPeriodo($alumno->periodo, (($alumno->carrera == 1) ? 'LEO-' : 'MED-') . $alumno->semestre);
        $data['materias_actuales'] = $info;
        $data['banderas'] = false;
        return view('escolar/alumnossecciones', $data);
    }

    /**
     * Retorna el arreglo de materias que el alumno puede tomar de acuerdo a las cargas pasadas
     */
    public function MateriasDisponibles($matricula, $periodo, $seccion, $lista_grupos)
    {
        if ($lista_grupos) {
            foreach ($lista_grupos as $grupo) {
                $grupo->permiso = false;
                // echo $grupo->materia;
                $materia = $this->materiasModel->find($grupo->materia);
                $serie =  json_decode($materia->serie, true);
                if (isset($serie['prerrequisitos'])) {
                    // echo json_encode($serie['prerrequisitos']);
                    foreach ($serie['prerrequisitos']  as $prerrequisito) {
                        $lista_materia_cursada = $this->alumnoHistorialModel->getByMateria($matricula, $prerrequisito);
                        if ($lista_materia_cursada) {
                            foreach ($lista_materia_cursada as $materia_lista) {
                                if ($grupo->permiso == false) {
                                    if ($materia_lista->aprobada == 1) {
                                        $mensaje[] = "$grupo->materia Se aprobó el prerrequisito $prerrequisito <br>";
                                        $grupo->permiso = true;
                                    }
                                }
                            }
                        } else {
                            $mensaje[] = " $grupo->materia Alumno no ha cursado el prerrequisito $prerrequisito <br>";
                        }
                    }
                } else {
                    $mensaje[] = "$grupo->materia Materia sin prerrequisitos <br>";
                    $grupo->permiso = true;
                }
                if ($grupo->permiso == false) $mensaje[] = "$grupo->materia Requisito no aprobado <br>";
                // echo "<br>";
            }
        } else {
            $mensaje[] = 'No se encontraron materias para la seccion ' . $seccion . " en el periodo $periodo <br>";
        }
        return $lista_grupos;
    }

    /**
     * busca las materias que pertenecen a la seccion buscada
     * te permite seleccionar si una materia no debe tomarla
     */
    public function cambioSeccion()
    {
        $seccion = $this->request->getPost('clave_seccion');
        $matricula = $this->request->getPost('matricula');
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $alumno = $this->alumnosModel->find($matricula);
        $data['alumno'] = $alumno;
        $grupo = new GruposMateriasModel();
        $alumno = $this->alumnosModel->find($matricula);
        $data['materias_cambio_seccion'] = $this->MateriasDisponibles($matricula, $alumno->periodo, $seccion, $grupo->buscarporseccion($seccion, $alumno->periodo));
        $data['menu'] = $this->sesion->get('menu');
        $data['seccion_siguiente'] = $seccion;
        $data['cambio'] = true;
        return view('escolar/alumnossecciones', $data);
    }

    /**
     * Realiza el proceso de baja de materias y reasignacion de materias para la siguiente seccion
     * @param matricula ID  alumno  
     * @param periodo ID periodo
     * @param seccion Seccion de destino
     */
    public function reasignacionAlumno($matricula, $periodo, $seccion)
    {
        // var_dump($this->request->getPost());
        // echo "<br> $periodo $matricula <br>";
        $grupos_actuales =  $this->alumnoGrupoHistorial->historialperiodo($matricula, $periodo);
        $mensaje = [
            'tipo' => 'alert-info',
            'texto' => "No se pudo realizar el cambio de seccion."
        ];
        foreach ($grupos_actuales as $grupo) {
            try {
                $this->alumnoGrupo->delete($grupo->id);
            } catch (\Exception $ex) {
                log_message("error", "No se pudo eliminar el registro de alumnosgrupos {id}: {exception}: ", ["id" => $grupo->id, 'exception' => $ex]);
            }
        }
        $bandera = false;
        $alumnogrupoent = new GrupoAlumno();
        foreach ($this->request->getPost('id_materia') as $materia) {
            $alumnogrupoent->alumno = $matricula;
            $alumnogrupoent->grupo = $materia;
            try {
                $this->alumnoGrupo->insert($alumnogrupoent);
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Alumno insertado correctamente ."
                ];
                $bandera = true;
            } catch (\Exception $ex) {
                log_message("error", "No se pudo insertar el registro de alumnosgrupos {matricula}: {grupo} : {exception}: ", ["matricula" => $matricula, 'grupo' => $materia, 'exception' => $ex]);
            }
        }
        if ($bandera) {
            log_message("notice", "{matricula} ALUMNO FUE REASIGNADO A LA SECCION {seccion} POR EL USUARIO {usuario}", ["matricula" => $matricula, "seccion" => $seccion, "usuario" => $this->sesion->get('useremail')]);
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/Escolar/secciones');
    }

    public function getdatos()
    {
        $valores = $this->request->getPost('idc_eliminar');
        $matricula = $this->request->getPost('matricula');
        $seccion_anterior = $this->request->getPost('seccion_anterior');
        $seccion_siguiente = $this->request->getPost('seccion_siguiente');
        $materias_para_asignar = $this->request->getPost('id_materia');
        $comentario_para_cambio = $this->request->getPost('comentario_cambio');
        $numero_de_oficio = $this->request->getPost('num_oficio');
        $materias_eliminadas = [];
        $materias_errores = [];
        $grupoalumno = new GrupoAlumnoModel();
        //echo "eliminar <br>";
        foreach ($valores as $idc) {
            try {

                $grupoalumno->quita(trim($matricula), $idc);
                // echo "$matricula ::  $idc <br>";
                $materias_eliminadas[] = $idc;
            } catch (\Exception $ex) {
                $materias_errores[] = $idc;
                $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
            }
        }
        $materias_asignadas = [];
        $materias_asignadas_errores = [];
        //echo "insertar <br>";
        $grupo_alumnoEnt = new GrupoAlumno();
        $grupo_alumnoModel = new GrupoAlumnoModel();
        $grupo_alumnoEnt->alumno = trim($matricula);
        foreach ($materias_para_asignar as $asignacion) {
            try {
                $grupo_alumnoEnt->grupo = $asignacion;

                $grupo_alumnoModel->insert($grupo_alumnoEnt);

                $materias_asignadas[] = $asignacion;
                $data['exito'] = "Materia asignada con exito";
            } catch (\Exception $ex) {
                $materias_asignadas_errores[] = $asignacion;
                $data['error'] = 'No fue posible guardar la calificación. ' . $ex->getMessage();
            }
        }
        $cambiosmodel = new CambioSeccionesModel();
        $cambioent = new CambioSecciones();

        $cambioent->matricula =  trim($matricula);
        $cambioent->procedencia = trim($this->request->getPost('seccion_anterior'));
        $cambioent->final =  trim($this->request->getPost('seccion_siguiente'));
        $cambioent->oficio =  $this->request->getPost('num_oficio');
        $cambioent->observaciones =  $this->request->getPost('comentario_cambio');
        $cambioent->usuario = $this->sesion->get('id');
        $cambiosmodel->insert($cambioent);
        //die();
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/alumnossecciones', $data);
    }

    /**
     * funcion para que el escolar realiza la baja de una materia del alumno
     * y lo pueda reasignar en una seccion diferente
     * 
     */
    public function BajadeMateria()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/bajademateria', $data);
    }
    /**
     * busca las materias actuales del alumno seleccionado 
     * @param periodo requiere agregar funcion para seleccion de periodo automatico
     */
    public function buscarmaterias()
    {
        $data['bandera_busqueda'] = true;
        $data['menu'] = $this->sesion->get('menu');
        $matricula = $this->request->getPost('matricula');
        $horario = new AlumnoGrupoHistorialModel();
        $alumno = $this->alumnosModel->find($matricula);
        $data['materias'] = $horario->historialperiodo($alumno->id, $alumno->periodo);
        $data['alumno'] = $alumno;
        return view('escolar/bajademateria', $data);
    }
    /**
     * proceso para eliminar al alumno de un grupo en especifico
     * @param matricula respecto al alumno 
     * @param idc grupo del que se eliminara el alumno 
     */
    public function Bajadesecciones()
    {
        $materias_eliminar = $this->request->getPost('id_materia');
        $matricula = $this->request->getPost('matricula');
        $grupoalumno = new GrupoAlumnoModel();
        if ($materias_eliminar) {
            foreach ($materias_eliminar as $idc) {
                try {
                    $grupoalumno->popAlumnoGrupo(trim($matricula), $idc);
                    $mensaje = [
                        'tipo' => 'alert-success',
                        'texto' => "Materias eliminadas con exito"
                    ];
                    log_message("notice", "{matricula} ALUMNO ELIMINADO CON EXITO DE LA MATERIA {materia} POR EL USUARIO {usuario}", ["matricula" => $matricula, "materia" => $idc, "usuario" => $this->sesion->get('useremail')]);
                } catch (\Exception $ex) {
                    $mensaje = [
                        'tipo' => 'alert-alert',
                        'texto' => "No fue posible eliminar la materia."
                    ];
                    log_message("error", "ERROR {matricula}: {exception}: ", ["matricula" => $matricula, 'exception' => $ex]);
                }
            }
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Escolar/BajadeMateria');
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => "No selecciono ninguna materia para eliminar."
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Escolar/BajadeMateria');
        }
    }
    /**
     * csv para listado del historial de los recursos por alumno y su estatus actual
     */
    public function historialalumnos()
    {
        $alumno = new AlumnosModel();
        $lista = $alumno->findssall();
        $contador = 1;
        $compilado_de_recursos = [];
        $compilado_de_recursos[] = ['Consecutivo,IDC, Martricula, Nombre del alumno, Carrera, Clave de Materia, Nombre de Materia, Seccion, Periodo , Calificacion, ID_Docente, Nombre del Docente, Clasificacion, Aprobada'];
        foreach ($lista as $key) {
            $recurso = $this->historialdereporbados($key->alumno_id, $contador);
            if ($recurso != false) {
                $contador++;
                if (count($recurso) > 1) {
                    $compilado_de_recursos[] = [$recurso[0]];
                    $compilado_de_recursos[] = [$recurso[1]];
                } else {
                    $compilado_de_recursos[] = $recurso;
                }
            }
        }
        if (count($compilado_de_recursos) > 1) {
            header('Content-Encoding: UTF-8');
            header('Content-type: text/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename=' . "Lista_de_recursos.csv");
            echo "\xEF\xBB\xBF"; // UTF-8 BOM
            $fp = fopen('php://output', 'wb');
            foreach ($compilado_de_recursos as $datos => $posiciones) {
                foreach ($posiciones as $line) {
                    $val = explode(",", $line);
                    fputcsv($fp, $val, ",", '"');
                }
            }
            fclose($fp);
            die();
        } else {
            //echo "no hay datos";
        }
    }

    private function historialdereporbados($matricula, $consecutivo)
    {
        $historialmodel = new HistorialCalificacionesModel();
        $datos = $historialmodel->buscarReprobados($matricula);
        $arreglo_materias_reprobadas = [];
        $contador = 0;
        if ($datos) {
            foreach ($datos as $valores) {
                if ($valores->Aprobada == 0) {
                    $recurso = $historialmodel->buscarRecruso($matricula, $valores->Materia_clave);
                }
            }
        }
        if (isset($recurso)) {
            if (count($recurso) > 1) {
                $contador_recursos = 0;
                $status = "irregular";
                foreach ($recurso as $reprobadas) {
                    if ($reprobadas->Aprobada == 1) {
                        $contador_recursos++;
                    }
                }
                if ($contador_recursos >= 1) {
                    $status = "regular";
                }
                foreach ($recurso as $reprobadas) {
                    $IDC = $reprobadas->IDC;
                    $Martricula = $reprobadas->Martricula;
                    $Nombre_alumno = utf8_encode($reprobadas->Nombre_alumno);
                    $Carrera = $reprobadas->Carrera;
                    $Materia_clave = $reprobadas->Materia_clave;
                    $Materia = utf8_decode(str_replace(',', ' ', $reprobadas->Materia));
                    $Seccion = $reprobadas->Seccion;
                    $Periodo = $reprobadas->Periodo;
                    $ID_Docente = $reprobadas->ID_Docente;
                    $Nombre_docente = $reprobadas->Nombre_docente;
                    $Aprobada = $reprobadas->Aprobada;
                    //agregar materias cursadas
                    if ($reprobadas->Extraordinaria != null) {
                        $calificacion = $reprobadas->Extraordinaria;
                    } else {
                        $calificacion = $reprobadas->Ordinario;
                    }
                    $arreglo_materias_reprobadas[] = "$consecutivo,$IDC,$Martricula,$Nombre_alumno, $Carrera, $Materia_clave , $Materia , $Seccion,$Periodo,$calificacion, $ID_Docente, $Nombre_docente, $status, $Aprobada";
                }
            } else {
                $IDC = $recurso[0]->IDC;
                $Martricula = $recurso[0]->Martricula;
                $Nombre_alumno = $recurso[0]->Nombre_alumno;
                $Carrera = $recurso[0]->Carrera;
                $Materia_clave = $recurso[0]->Materia_clave;
                $Materia =  str_replace(',', ' ', $recurso[0]->Materia);
                $Seccion = $recurso[0]->Seccion;
                $Periodo = $recurso[0]->Periodo;
                $ID_Docente = $recurso[0]->ID_Docente;
                $Nombre_docente = $recurso[0]->Nombre_docente;
                $Aprobada = $recurso[0]->Aprobada;
                if ($recurso[0]->Extraordinaria != null) {
                    $calificacion = $recurso[0]->Extraordinaria;
                } else {
                    $calificacion = $recurso[0]->Ordinario;
                }
                $arreglo_materias_reprobadas[] = "$consecutivo,$IDC,$Martricula,$Nombre_alumno, $Carrera, $Materia_clave , $Materia , $Seccion,$Periodo,$calificacion, $ID_Docente, $Nombre_docente, irregular, $Aprobada";
            }
        } else {
            // echo "no hay valores <br>";
        }
        // var_dump($recurso);
        if (count($arreglo_materias_reprobadas) > 0) {


            //echo  json_encode($arreglo_materias_reprobadas)."<br><br>";
            return $arreglo_materias_reprobadas;
        } else {
            return false;
        }
    }
    /**
     * recibe paramaetros para la impresion 
     * @param datos arreglo de datos que componen la informacion a mostrar en los reportes del filtro sanitario
     * @param nombre_archivo nombre de salida del archivo 
     */
    public function generadorCSV($datos, $nombre_archivo)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . $nombre_archivo . ".csv");
        $fp = fopen('php://output', 'wb');
        foreach ($datos as $line) {
            $val = explode(",", $line);
            fputcsv($fp, $val);
        }
        fclose($fp);
    }
    /**
     * funcion para que el escolar realiza la baja de una materia del alumno
     * y lo pueda reasignar en una seccion diferente
     * 
     */
    public function AltaMateria()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['bandera_inicio'] = true;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/altadematerias', $data);
    }
    /**
     * @param matricula correspondiente al alumno buscado
     * esto devolvera la informacion del alumno
     */
    public function buscardatos()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $data['bandera_busqueda'] = true;
        $data['menu'] = $this->sesion->get('menu');
        $matricula = $this->request->getPost('matricula');
        $horario = new AlumnoGrupoHistorialModel();
        $alumno =  $this->alumnosModel->find($matricula);
        $data['secciones'] = $this->gruposModel->getSeccionesPeriodo($alumno->periodo, (($alumno->carrera == 1) ? 'LEO' : 'MED'));
        $data['materias'] = $horario->historialperiodo($matricula, $alumno->periodo);
        $data['alumno'] = $alumno;
        return view('escolar/altadematerias', $data);
    }
    /**
     * funcion para buscar las materias pertenecientes a la seccion solicitada
     * @param seccion   apunta a la seccion que cambiara
     */
    public function buscarSeccion()
    {
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $horario = new AlumnoGrupoHistorialModel();
        $alumno =  $this->alumnosModel->find(trim($this->request->getPost('matricula')));
        $data['materias'] = $horario->historialperiodo($alumno->id, $alumno->periodo);
        $valores = mb_strtoupper(trim($this->request->getPost('seccion-busqueda')));
        $grupo = new GruposMateriasModel();
        $aux = $grupo->buscarporseccion($valores, $alumno->periodo);
        $data['menu'] = $this->sesion->get('menu');
        $data['alumno'] = $alumno;
        $data['materias_cambio_seccion'] = $aux;
        $data['matricula'] = trim($this->request->getPost('matricula'));
        $data['seccion_anterior'] = $this->request->getPost('seccion_procedencia');
        $data['seccion_siguiente'] = $valores;
        $data['seccionbusqueda'] = true;
        /* echo $this->request->getPost('matricula');
        die(); */
        return view('escolar/altadematerias', $data);
    }
    /**
     * funcion para dar de alta las materias seleccionaras del la seccion buscada
     */
    public function altamaterias()
    {
        $grupos_seleccionados = $this->request->getPost('id_materia');
        if ($grupos_seleccionados) {
            $gruposalumnosmodel = new GrupoAlumnoModel();
            foreach ($grupos_seleccionados as $idc) {
                $gruposalumnosent = new GrupoAlumno();
                $matricula = trim($this->request->getPost('matricula'));
                if (!$gruposalumnosmodel->existe($idc, $matricula)) {
                    try {
                        $gruposalumnosent->alumno = $matricula;
                        $gruposalumnosent->grupo = $idc;
                        $gruposalumnosmodel->insert($gruposalumnosent);
                        $data['exito'] = "Materias agregadas con exito";
                        $mensaje = [
                            'tipo' => 'alert-success',
                            'texto' => "Materias agregadas con exito"
                        ];
                        log_message("notice", "{matricula} ALUMNO ASIGNADO A LA MATERIA {materia} POR EL USUARIO {usuario}", ["matricula" => $matricula, "materia" => $idc, "usuario" => $this->sesion->get('useremail')]);
                    } catch (\Exception $ex) {
                        $materias_errores[] = $idc;
                        $data['error'] = 'No fue posible asignar la materia. ';
                        $mensaje = [
                            'tipo' => 'alert-alert',
                            'texto' => "No fue posible asignar la materia."
                        ];
                        log_message("error", "ERROR {matricula}: {exception}: ", ["matricula" => $matricula, 'exception' => $ex]);
                    }
                } else {
                    $mensaje = [
                        'tipo' => 'alert-info',
                        'texto' => "Materia actualmente asignada."
                    ];
                }
            }
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Escolar/AltaMateria');
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => "No selecciono ninguna materia para asignar."
            ];
            $this->sesion->setFlashdata('datos', $mensaje);
            return redirect()->to('/Escolar/AltaMateria');
        }
    }
    /**
     * funcion para generar los datos de las frimas del Ripel
     */
    public function DatosRIPEL()
    {
        $ripelmodel = new RipelModel();
        $data['alumnosarr'] = $ripelmodel->getAlumnosRipel();
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/alumnosripel', $data);
    }
    public function estudiantesBaja()
    {
        $pager = \Config\Services::pager();
        $alumnosModel = new AlumnosModel();
        $data = $this->request->getGet();
        if (!count($data) || !array_key_exists('id', $data)) {
            $data = ['id' => '', 'carrera' => '', 'apPaterno' => '', 'apMaterno' => '', 'nombre' => '', 'status' => ''];
        }
        $alumnos = $alumnosModel->buscarBaja($data);
        $data['mensaje'] = ($this->sesion->getFlashdata('datos')) ? $this->sesion->getFlashdata('datos') : false;
        $contador = null;
        $alumnosbaja = null;
        $data['alumnos'] = $alumnos;
        $data['pager'] = $alumnosModel->pager;
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/alumnosbaja', $data);
    }

    /**
     * cambia el estatus del aspirante 
     * @param bandera a donde se redirecciona 
     */
    public function cambiarStatus($bandera)
    {
        // var_dump($this->request->getPost());
        // die();
        $matricula = $this->request->getPost('alumno_id');
        $alumno = $this->alumnosModel->withDeleted()->find($matricula);
        $usuario = $this->usuariosModel->withDeleted()->find($alumno->correo);
        if ($alumno) {
            $estatus = $this->request->getPost('select_status');
            $comentario = $this->request->getPost('motivo_cambio');
            $alumno->status = $estatus;
            $alumno->comentario = $comentario;
            try {
                if ($estatus == 'ACTIVO') {
                    $alumno->comentario = $comentario;
                    $alumno->deleted_at = NULL;
                    $this->alumnosModel->update($matricula, $alumno);
                    $usuario->deleted_at = NULL;
                    $this->usuariosModel->update($usuario->correo,$usuario);
                }  else {
                    $this->alumnosModel->update($matricula, $alumno);
                    $this->alumnosModel->delete($matricula);
                    $this->usuariosModel->delete($alumno->correo);
                }
                $mensaje = [
                    'tipo' => 'alert-success',
                    'texto' => "Se ha cambiado a $estatus el estatus del alumno $alumno->apPaterno $alumno->apMaterno $alumno->nombres con matricula $alumno->id"
                ];
                log_message("notice", "{matricula} ALUMNO CAMBIADO DE STATUS A {estatus} POR EL USUARIO {usuario}", ["matricula" => $matricula, "estatus" => $estatus, "usuario" => $this->sesion->get('useremail')]);
            } catch (\Exception $ex) {
                $mensaje = [
                    'tipo' => 'alert-alert',
                    'texto' => "No fue posible cambiar el estatus del alumno $alumno->apPaterno $alumno->apMaterno $alumno->nombres con matricula $alumno->id."
                ];
                log_message("error", "ERROR {matricula}: {exception}: ", ["matricula" => $matricula, 'exception' => $ex]);
            }
        } else {
            $mensaje = [
                'tipo' => 'alert-info',
                'texto' => "Alumno no encontrado"
            ];
            log_message("error", "Alumno no encontrado $matricula");
        }
        $this->sesion->setFlashdata('datos', $mensaje);
        return redirect()->to('/Escolar/' . $bandera);
    }

    /**
     * Menu para descarga de calificaciones
     */
    public function ReporteCalificaciones()
    {
        $data['periodos'] = $this->periodoModel->findAll();
        $data['carreras'] = $this->carreraModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('escolar/menureporte', $data);
    }

    /**
     * Descargar reporte de calificaciones
     */
    public function DescargarReporte()
    {
        $datos = false;
        (($this->request->getPost('periodo') != 0) ? $datos['periodo'] = $this->request->getPost('periodo') : '');
        (($this->request->getPost('seccion') != null || $this->request->getPost('seccion') != "") ? $datos['seccion'] = $this->request->getPost('seccion') : '');
        (($this->request->getPost('carrera') != 0) ? $datos['carrera'] = $this->request->getPost('carrera') : '');
        if ($datos) {
            $reporte = $this->reporteCalificacionesModel->buscar($datos);
        } else {
            $reporte = $this->reporteCalificacionesModel->buscar();
        }
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=' . "lista_de_calificaiones" . date('Ydm') . ".csv");
        $fp = fopen('php://output', 'wb');
        fputcsv($fp, ["MATRICULA,NOMBRE,CALIFICACION,LETRA,EVALUACION,SECCION,GRUPO,PERIODO,MATERIA,SEMESTRE,CARRERA,DOCENTE"]);
        foreach ($reporte as $line => $registro) {
            $registro->docente_nom . "<br>";
            fputcsv($fp, [
                $registro->matricula . "," .
                    utf8_decode($registro->ap_paterno) . " " . utf8_decode($registro->ap_materno) . " " . utf8_decode($registro->nombre) . "," .
                    $registro->calificacion . "," . $registro->letra . "," .
                    $registro->evaluacion . "," .
                    $registro->seccion . "," .
                    $registro->grupo . "," .
                    $registro->periodo . "," .
                    $registro->materia . "-" . str_replace(',', '', (utf8_decode($registro->materia_nombre))) . "," .
                    $registro->semestre . "," .
                    $registro->docente . "," .
                    utf8_decode($registro->docente_nom) . ","
            ]);
        }
        fclose($fp);
        die();
    }

    /**
     * crea una carpeta para guardar los archivos de listas generados 
     */
    public function ListasGrupo($periodo = false)
    {
        if (!$periodo) $periodo = $this->request->getPost('periodo');
        $bandera = $this->request->getPost('bandera_concatenar');
        $logdate = date("Y-m-d-H-i-s");
        // echo $logdate . "<br>";
        $dir = WRITEPATH . 'descargas/' . $logdate;
        // echo $dir . "<br>";
        $zip = new ZipArchive();
        if (!is_dir($dir)) mkdir($dir, 0700, true);
        $secciones = $this->gruposModel->getSeccionesPeriodo($periodo);
        $zip->open(WRITEPATH . "descargas/Listas_de_grupo_" . $periodo . ".zip", ZipArchive::CREATE);
        $contador_grupos = 0;
        foreach ($secciones as $seccion) {
            try {

                $archivo =  $this->generarListasGrupo($periodo, $seccion->clave, $dir, $bandera);

                try {
                    $archivos_generados[] = $archivo;
                    $zip->addEmptyDir($seccion->clave);
                    $zip->addFile($dir . '/' . $archivo, $seccion->clave . '/' . $periodo . "_" . $seccion->clave . '.xlsx');
                    $contador_grupos++;
                } catch (\Exception $ex) {
                    $errores[] = 'Se generó un problema al agregar las listas del grupo ' . $seccion->clave . ' al archivo descargable';
                    log_message("error", "ERROR {seccion}: {exception}: ", ["seccion" => $seccion->clave, 'exception' => $ex]);
                }
            } catch (\Exception $ex) {
                $errores[] = 'No se pudieron generar las listas del grupo ' . $seccion->clave;
                log_message("error", "ERROR {seccion}: {exception}: ", ["seccion" => $seccion->clave, 'exception' => $ex]);
            }
        }
        $zip->close();
        $this->descargaZip("Listas_de_grupo_" . $periodo . ".zip");
        foreach ($archivos_generados as $file) {
            unlink(WRITEPATH . 'descargas/' . $logdate . '/' . $file);
        }
        rmdir($dir);
    }



    public function descargaZip($archivo)
    {
        header("Content-type: application/octet-stream");
        header("Content-disposition: attachment; filename=" . $archivo);
        readfile(WRITEPATH . 'descargas/' . $archivo);
        unlink(WRITEPATH . 'descargas/' . $archivo);
    }

    /**
     * Genera archivo excel de las materias totales de la seccion
     * @param periodo ID del periodo
     * @param seccion 
     * @param directorio ruta de guardado
     */
    public function generarListasGrupo($periodo, $seccion, $direcctorio, $bandera)
    {
        $grupos = $this->gruposModel->buscar(['periodo' => $periodo, 'seccion' => $seccion]);
        $spreadsheet = new Spreadsheet();
        $index = 0;
        $spreadsheet->getProperties()->setCreator('USEP.DIGITAL')->setTitle($seccion)->setKeywords($periodo);
        $maximo = count($grupos);
        foreach ($grupos as $grupo) {
            $materias = $this->listaEscolarModel->buscargrupo($grupo->id);
            $row = 5;
            $spreadsheet->setActiveSheetIndex($index)->setCellValueByColumnAndRow(1, 1,  'IDC')->setCellValueByColumnAndRow(2, 1,  'PERIODO')->setCellValueByColumnAndRow(3, 1,  'SECCIÓN')->setCellValueByColumnAndRow(4, 1,  'CARRERA')->setCellValueByColumnAndRow(5, 1,  'MATERIA');
            if(!$bandera){
                $spreadsheet->setActiveSheetIndex($index)->setCellValueByColumnAndRow(1, 4,  'No.')->setCellValueByColumnAndRow(2, 4,  'Matrícula')->setCellValueByColumnAndRow(3, 4,  'Apellido Paterno')->setCellValueByColumnAndRow(4, 4,  'Apellido Materno')->setCellValueByColumnAndRow(5, 4,  'Nombre');
            }else{
                $spreadsheet->getActiveSheet()->mergeCells('C4:E4');
                $spreadsheet->setActiveSheetIndex($index)->setCellValueByColumnAndRow(1, 4,  'No.')->setCellValueByColumnAndRow(2, 4,  'Matrícula')->setCellValueByColumnAndRow(3, 4,  'Nombre Completo');
                
            }
            $spreadsheet->getActiveSheet()->mergeCells('B3:E3');
            $spreadsheet->setActiveSheetIndex($index)->setCellValueByColumnAndRow(1, 3,  'Docente:')->setCellValueByColumnAndRow(2, 3,  $materias[0]->app_docente." ".$materias[0]->apm_docente." ".$materias[0]->nombre_docente);
            $spreadsheet->setActiveSheetIndex($index)
                ->setCellValueByColumnAndRow(1, 2,  $materias[0]->id_grupo)
                ->setCellValueByColumnAndRow(2, 2,  $materias[0]->periodo_grupo)
                ->setCellValueByColumnAndRow(3, 2,  $materias[0]->clave_grupo)
                ->setCellValueByColumnAndRow(4, 2, (($materias[0]->numero_materia == 1) ? 'LIC. EN ENFERMERÍA Y OBSTETRICIA' : 'LIC. EN MEDICO CIRUJANO'))
                ->setCellValueByColumnAndRow(5, 2,  $materias[0]->nombre_materia);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('A1:E3')->getFill()->setFillType(FILL::FILL_SOLID)->getStartColor()->setARGB('840F31');
            $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('D')->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex($index)->getColumnDimension('E')->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('A1')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('B1')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('C1')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('D1')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('E1')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('A3')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('B3')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('C3')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('D3')->getFont()->setBold(true);
            $spreadsheet->setActiveSheetIndex($index)->getStyle('E3')->getFont()->setBold(true);
            $contador = 1;
            if (!$bandera) {

                foreach ($materias as $materia) {
                    $spreadsheet->setActiveSheetIndex($index)
                        ->setCellValueByColumnAndRow(1, $row,  $contador)
                        ->setCellValueByColumnAndRow(2, $row,  $materia->gpoalumno)
                        ->setCellValueByColumnAndRow(3, $row,  $materia->app_estudiante)
                        ->setCellValueByColumnAndRow(4, $row,  $materia->apm_estudiante)
                        ->setCellValueByColumnAndRow(5, $row,  $materia->nombre_estudiante);
                    $row++;
                    $contador++;
                }
            } else {
                foreach ($materias as $materia) {
                    $spreadsheet->getActiveSheet()->mergeCells($this->cellsToMergeByColsRow(3,5,$row));
                    $spreadsheet->setActiveSheetIndex($index)
                        ->setCellValueByColumnAndRow(1, $row,  $contador)
                        ->setCellValueByColumnAndRow(2, $row,  $materia->gpoalumno)
                        ->setCellValueByColumnAndRow(3, $row,  $materia->app_estudiante . " " . $materia->apm_estudiante ." ".$materia->nombre_estudiante);
                    $row++;
                    $contador++;
                }

            }
            $spreadsheet->getActiveSheet()->setTitle($materias[0]->clave_materia);
            ($index < $maximo - 1) ? $spreadsheet->createSheet() : '';
            $index++;
        }
        $spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $writer->save($direcctorio . '/Lista_' . $seccion . '_' . $periodo . '.xlsx');
        $archivo = fopen($direcctorio . '/Lista_' . $seccion . '_' . $periodo . '.xlsx', 'r');
        if ($archivo) {
            fclose($archivo);
            return 'Lista_' . $seccion . '_' . $periodo . '.xlsx';
        } else return false;
    }

    public function cellsToMergeByColsRow($start = -1, $end = -1, $row = -1){
        $merge = 'A1:A1';
        if($start>=0 && $end>=0 && $row>=0){
            $start =  \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($start);
            $end =  \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($end);
            $merge = "$start{$row}:$end{$row}";
        }
        return $merge;
    }
}
