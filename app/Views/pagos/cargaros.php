<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <h4>Carga de información del ROS</h4>
    <hr />

    <?php if( isset($mensaje) ): ?>
        <div class="alert alert-success">
            <?php echo $mensaje; ?>
        </div>
    <?php endif; ?>
    
    <?php if( isset($errors) ): ?>
        <div class="alert alert-danger">
            <?php foreach( $errors as $error ): ?>
                <?php echo $error; ?> <br />
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <form method="post" action="<?php echo base_url('Pagos/CargaRos'); ?>" enctype="multipart/form-data">
        <div class="col-sm-12">
            <label for="pagosros" class="form-label">Selecciona el archivo</label>
            <input type="file" class="form-control" id="pagosros" name="pagosros" aria-describedby="archivoHelp">
            <div id="archivoHelp" class="form-text">Selecciona el archivo CSV que contiene la información del ROS.</div>
        </div>
        <button type="submit" name="smb" value="fros" class="btn btn-secondary btn-sm">
            Subir y guardar
        </button>
    </form> 
</div>



<?php echo $this->endSection() ?>