<?php
namespace App\Entities;

use CodeIgniter\Entity;

class UsuarioGeneral extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'correo' => null,
        'rolid' => null,
        'ultconexion' => null,
        'id' => null,
        'mail' => null,
        'nombre' => null,
        'apPaterno' => null,
        'apMaterno' => null,
        'edad' => null,
        'sexo' => null,
        'foto' => null,
        'carrera' => null,

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'correo' => 'correo',
        'rolid' => 'rolid',
        'ultconexion' => 'ultconexion',
        'id' => 'id',
        'mail' => 'mail',
        'nombre' => 'nombre',
        'apPaterno' => 'apPaterno',
        'apMaterno' => 'apMaterno',
        'edad' => 'edad',
        'sexo' => 'sexo',
        'foto' => 'foto',
        'carrera' => 'carrera',

       
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
