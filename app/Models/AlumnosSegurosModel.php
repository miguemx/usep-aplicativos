<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AlumnoSeguro;

class AlumnosSegurosModel extends Model {
    protected $table      = 'alumnos_seguro';
    protected $primaryKey = 'seguro_matricula';

    protected $returnType    = 'App\Entities\AlumnoSeguro';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'seguro_matricula','seguro_reg_patrimonial','seguro_ssn','seguro_salario','seguro_generico_6',
        'seguro_tipo_contratacion','seguro_tipo_salario','seguro_semana','seguro_fecha','seguro_umf',
        'seguro_generico_2','seguro_tipo_movimiento','seguro_guia','seguro_generico_1','seguro_formato','seguro_archivo',
        'seguro_comentarios','seguro_status','seguro_razon','seguro_razon_secundaria','seguro_comentarios','seguro_fecha_descarga'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * obtiene todos los alumnos con seguro solicitado
     */
    public function getAll($cuales, $filtros=array()) {
        $this->builder = $this->db->table( $this->table );
        $this->builder->join( 'alumnos_generales', 'alumno_id=seguro_matricula', 'inner' );
        $this->builder->join( 'prein_carreras', 'alumno_carrera=carrera_id', 'inner' );
        if( $cuales != 'TODOS' ) $this->builder->where('seguro_status',$cuales);
        $this->addFilter( $filtros );
        $this->builder->orderBy( 'alumno_ap_paterno', 'ASC' );
        $this->builder->orderBy( 'alumno_ap_materno', 'ASC' );
        $this->builder->orderBy( 'alumno_nombres', 'ASC' );

        $query = $this->builder->get();
        $rows = $query->getResult();
        $alumnos = [];
        foreach ( $rows as $row ) {
            $alumno = new \App\Entities\AlumnoSeguro();
            $alumno->id = $row->alumno_id;
            $alumno->apPaterno = $row->alumno_ap_paterno;
            $alumno->apMaterno = $row->alumno_ap_materno;
            $alumno->nombre = $row->alumno_nombres;
            $alumno->ssn = $row->seguro_ssn;
            $alumno->curp = $row->alumno_curp;
            $alumno->licenciatura = $row->carrera_nombre;
            $alumno->archivo = $row->seguro_archivo;
            $alumno->status = $row->seguro_status;
            $alumno->razon = $row->seguro_razon;
            $alumno->razonSecundaria = $row->seguro_razon_secundaria;
            $alumno->comentarios = $row->seguro_comentarios;
            $alumno->fechaDescarga = $row->seguro_fecha_descarga;
            $alumnos[] = $alumno;
        }

        return( $alumnos );
    }

    /**
     * crea una entrada de seguro facultativo de estudiantes con los valores por defecto
     * @param id el Id o matricula del estudiante
     * @return App\Entities\AlumnoSeguro la entidad creada despues de la insercion
     * @return null en caso de error
     */
    public function crea($id) {
        $alumnoSeguro = new AlumnoSeguro();
        $alumnoSeguro->matricula = $id;
        $alumnoSeguro->umf = 1;
        $alumnoSeguro->regPatrimonial = '12312312312'; // es patronal pero me pasé de artista
        $alumnoSeguro->ssn = '';
        $alumnoSeguro->salario = '000000';
        $alumnoSeguro->generico6 = '      ';
        $alumnoSeguro->tipoContratacion = '2';
        $alumnoSeguro->tipoSalario = '1';
        $alumnoSeguro->semana = '0';
        $alumnoSeguro->fecha = date("Y-m-d");
        $alumnoSeguro->generico2 = '  ';
        $alumnoSeguro->tipoMovimiento = '08';
        $alumnoSeguro->guia = '     ';
        $alumnoSeguro->generico1 = ' ';
        $alumnoSeguro->formato = '9';
        $alumnoSeguro->archivo = null;
        $alumnoSeguro->status = 'ESPERA';
        $this->insert( $alumnoSeguro );
        return $alumnoSeguro;
    }

    /**
     * obtiene un listado de todas las fechas de subido
     */
    public function getFechasDescarga() {
        $fechas = [];
        $this->builder = $this->db->table( $this->table );
        $this->builder->select( 'seguro_fecha_descarga' );
        $this->builder->orderBy( 'seguro_fecha_descarga', 'ASC' );
        $this->builder->distinct();
        $query = $this->builder->get();
        $rows = $query->getResult();
        foreach ( $rows as $row ) {
            if ( $row->seguro_fecha_descarga ) {
                $fechas[] = $row->seguro_fecha_descarga;
            }
        }
        return $fechas;
    }

    /**
     * agrega los filtros a las busquedas
     */
    private function addFilter($filtros) {
        
        if ( array_key_exists( 'fechaDescarga', $filtros ) ) {
            if ( strlen($filtros['fechaDescarga']) ) {
                $this->builder->where( 'seguro_fecha_descarga', $filtros['fechaDescarga'] );
            }
        }
    }
    
}
