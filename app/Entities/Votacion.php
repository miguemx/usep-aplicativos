<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Votacion extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id_proceso' => null,
        'nombre_proceso' => null,
        'inicio_proceso'=> null,
        'fin_proceso'=> null,
        'id_padron'=> null,
        'correo_padron'=> null,
        'voto_padron'=> null,
        'id_candidatos'=> null,
        'correo_candidatos'=> null,
        'nombre_candidatos'=> null,
        'voto_candidatos'=> null,
        'ganador_candidatos'=> null,

    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id_proceso' => "proceso_id",
        'nombre_proceso' => "proceso_nombre",
        'inicio_proceso'=> "proceso_inicio",
        'fin_proceso'=> "proceso_fin",
        'id_padron'=> "padron_id",
        'correo_padron'=> "padron_correo",
        'voto_padron'=> "padron_correo",
        'id_candidatos'=> "candidatos_id",
        'correo_candidatos'=> "candidatos_correo",
        'nombre_candidatos'=> "candidatos_nombre",
        'voto_candidatos'=> "candidatos_voto",
        'ganador_candidatos'=> "candidatos_ganador",

    ];

    protected $casts = [
        'options' => 'array',
        'options_object' => 'json',
        'options_array' => 'json-array'
    ];

}