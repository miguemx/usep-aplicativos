<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\AspirantesEstadisticaRegDet;

class AspirantesEstadisticaRegDetModel extends Model
{
    protected $table="view_aspirantes_estadistica_regiones";
    protected $returnType='App\Entities\AspirantesEstadisticaRegDet';
    protected $allowedFields=['region_nombre','n_alumnos_m_enf','n_alumnos_h_enf','n_alumnos_m_med','n_alumnos_h_med'];
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }
/**
 * muestra la estadística por regiones de puebla
 * @return consulta colección de registros agrupados por región
 */
    public function conteodetxregiones(){
        $consulta = $this->orderBy('region_nombre')->findAll();
                         
        //$consulta = $this->selectSum('n_alumnos')->select('region_nombre')->groupBy('region_nombre')->orderBy('region_nombre')->findAll();
        return $consulta;
    }

 


}