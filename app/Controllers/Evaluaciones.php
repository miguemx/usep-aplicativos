<?php

namespace App\Controllers;

use App\Entities\EvaluacionesRespuestas;
use App\Libraries\RespuestaValidacion;
use App\Models\AlumnosModel;
use App\Models\EmpleadosModel;
use App\Models\EvaluacionesFormulariosModel;
use App\Models\EvaluacionesPreguntasModel;
use App\Models\EvaluacionesRespuestasModel;
use App\Models\EvaluacionesTipoPreguntasModel;
use App\Models\EvaluacionModel;
use App\Models\GruposMateriasModel;
use App\Models\KardexModel;
use App\Models\TipoEvaluacionesModel;

class Evaluaciones extends BaseController
{
    private $evaluacionModel;
    private $tipoEvaluacionModel;
    private $kardexModel;
    private $alumnoModel;
    private $empleadosModel;
    private $grupoMateriasModel;
    private $formulariosModel;
    private $preguntasModel;
    private $tipoPreguntaModel;
    private $respuestasModel;

    public function __construct()
    {
        $this->evaluacionModel = new EvaluacionModel;
        $this->tipoEvaluacionModel = new TipoEvaluacionesModel();
        $this->kardexModel = new KardexModel();
        $this->alumnoModel = new AlumnosModel();
        $this->empleadosModel = new EmpleadosModel;
        $this->grupoMateriasModel = new GruposMateriasModel();
        $this->formulariosModel = new EvaluacionesFormulariosModel();
        $this->preguntasModel = new EvaluacionesPreguntasModel();
        $this->tipoPreguntaModel = new EvaluacionesTipoPreguntasModel();
        $this->respuestasModel = new EvaluacionesRespuestasModel();
    }

    /**
     * Despliga el menu de evaluaciones disponibles
     */
    public function Menu()
    {
        $data['evaluaciones'] = $this->evaluacionModel->buscar(['rol' => $this->sesion->get('rol_id')]);
        $data['tipos_evaluacion'] = $this->tipoEvaluacionModel->findAll();
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/evaluaciones', $data);
    }

    /**
     * despliega los formularios correspondientes a la evaluacion seleccionada
     * @param evaluacion ID de la evalucion seleccionada 
     */
    public function Contestar($evaluacion)
    {   
        $flgEvaluacionActiva=$this->evaluacionModel->findById($evaluacion)->evaluacion_estatus;
        if ($flgEvaluacionActiva=="ACTIVO"){
            $alumno = $this->alumnoModel->find($this->sesion->get('id'));
            $data['errores'] = ($this->sesion->getFlashdata('errores')) ? $this->sesion->getFlashdata('errores') : false;
            $data['guardado'] = ($this->sesion->getFlashdata('guardado')) ? $this->sesion->getFlashdata('guardado') : false;
            $data['evaluacion'] = $this->evaluacionModel->find($evaluacion);
            if ($data['evaluacion']->tipo == 1) {
                $kardex = $this->kardexModel->getMateriasActulales($alumno->id, $data['evaluacion']->periodo);
                $data['formularios'] = $this->formulariosModel->getByEvaluacion($evaluacion);
                $array_preguntas = [];
                foreach ($data['formularios'] as $formulario) {
                    $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
                    foreach ($preguntas as $pregunta) {
                        $array_preguntas[] = $pregunta->id;
                    }
                }
                foreach ($kardex as $materia) {
                    $empledo = $this->empleadosModel->find($materia->docente);
                    $materia->docente_nombre = mb_strtoupper($empledo->apPaterno . ' ' . $empledo->apMaterno . ' ' . $empledo->nombre);
                    $respuestas = $this->respuestasModel->getArrayRespuestasPreguntas($array_preguntas, $this->sesion->get('id'),  $materia->idc);
                    $materia->respuestas = ($respuestas)? count($respuestas):0;
                }
                $data['preguntas_totales'] = ($array_preguntas)? count($array_preguntas):0;
                $data['materias'] =  $kardex;
                $data['menu'] = $this->sesion->get('menu');
                return view('estudiantes/contenidoevaluacion', $data);
            } else {
            }

        }
        else{
            return redirect()->to('home');
        }
    }

    /**
     * Despliega la evaluacion docente de la materia seleccionada
     * @param evaluacion ID de evaluacion 
     * @param grupo ID del grupo que evaluará
     */
    public function Materia($evaluacion, $grupo)
    {
        $evaluacionModel = $this->evaluacionModel->find($evaluacion);
        $grupoModel = $this->grupoMateriasModel->find($grupo);
        $data['evaluacion'] =  $evaluacionModel;
        $data['grupo'] = $grupoModel;
        $data['formularios'] = $this->formulariosModel->getByEvaluacion($evaluacion);
        $arr_preguntas_id = [];
        $array_preguntas = [];
        foreach ($data['formularios'] as $formulario) {
            $preguntas = $this->preguntasModel->getByFormulario($formulario->id);
            foreach ($preguntas as $pregunta) {
                $tipo_pregunta = $this->tipoPreguntaModel->find($pregunta->tipo);
                $pregunta->objeto_tipo = $tipo_pregunta;
                $array_preguntas[] = $pregunta->id;
            }
            $arr_preguntas_id[] = $preguntas;
        }
        $data['arr_preguntas'] = $arr_preguntas_id;
        $resp = $this->respuestasModel->getArrayRespuestasPreguntas($array_preguntas, $this->sesion->get('id'), $grupo);
        $respuestas = [];
        foreach ($resp as $respuesta) {
            $respuestas[$respuesta->pregunta]['valor'] = $respuesta->valor;
            $respuestas[$respuesta->pregunta]['id'] = $respuesta->pregunta;
            $respuestas[$respuesta->pregunta]['comentario'] = $respuesta->comentario;
            $respuestas[$respuesta->pregunta]['matricula'] = $respuesta->matricula;
        }
        $data['alumno'] = $this->alumnoModel->find($this->sesion->get('id'));
        $data['arr_respuestas'] = $respuestas;
        $data['menu'] = $this->sesion->get('menu');
        return view('estudiantes/formulariosevaluacion', $data);
    }

    /**
     * Recibe las respuestas para guardarlas
     * @param evaluacion ID de la evaluacion
     * @param grupo ID del grupo respndido
     */
    public function GuardarRespuestasEvaluacionDocente($evaluacion, $grupo)
    {
        $respuestas = $this->request->getPost();
        $tiposCampo = $this->tipoPreguntaModel->findAll();
        $errores = '';
        foreach ($respuestas as $idCampo => $valor) {
            // echo "$idCampo => $valor";
            $res = $this->guardaRespuesta($idCampo, $valor, $tiposCampo, $grupo);
            if ($res !== true) {
                $errores .= $res . '<br />';
            }
        }
        if ($errores !== '')  $this->sesion->setFlashdata('errores', $errores);
        else $this->sesion->setFlashdata('guardado', true);
        return redirect()->to('/Evaluaciones/Contestar/' . $evaluacion);
    }

    /**
     * realiza la subida de archivos del usuario
     * @param idCampo el Id del campo a guardar
     * @param valor la respuesta que se desea guardar
     * @param tiposCampo la lista completa de tipos de campo
     */
    private function guardaRespuesta($idCampo, $valor, $tiposCampo, $grupo)
    {
        $errores = true;
        try {
            $campo = $this->preguntasModel->find($idCampo);
            if (!is_null($campo)) {
                $res = $this->validarRespuesta($campo, $valor, $tiposCampo);
                if ($res === true) {
                    $respuesta = $this->respuestasModel->contestado($idCampo, $this->sesion->get('id'), $grupo); // traer 
                    if (!is_null($respuesta)) {
                        $respuesta->valor = $valor;
                        $this->respuestasModel->update($respuesta->id, $respuesta);
                    } else {
                        $respuesta = new EvaluacionesRespuestas();
                        $respuesta->matricula = $this->sesion->get('id');
                        $respuesta->pregunta = $idCampo;
                        $respuesta->valor = $valor;
                        $respuesta->grupo = $grupo;
                        $this->respuestasModel->insert($respuesta);
                    }
                } else {
                    $errores = $res;
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DataException $ex) {
            if ($ex->getMessage() != 'There is no data to update.') {
                log_message("error", "ERROR {exception}", ["exception" => $ex]);
                $errores = 'Ocurrió un error al guardar las respuestas de la evaluación. Por favor verifica o intenta nuevamente. <br />';
            }
        } catch (\Exception $ex) {
            log_message("error", "ERROR {exception}", ["exception" => $ex]);
            $errores = 'Ha ocurrido un error inesperado. Por favor cierra tu sesión y vuelve a iniciar. <br />';
        }
        return $errores;
    }
    /**
     * valida una respuesta de acuerdo a las reglas establecidas en la base de datos
     * @param campo el registro completo del campo
     * @param valor el valor que intenta introducir el usuario
     * @return true en caso de que el valor sea válido
     * @return error una cadena con el error reportado
     */
    private function validarRespuesta($campo, $valor, $tiposCampo)
    {
        $validador = new RespuestaValidacion($tiposCampo);
        if ($validador->do($campo, $valor)) {
            return true;
        } else {
            return implode('<br />', $validador->getErrors());
        }
    }
}
