<?php
namespace App\Models;
use CodeIgniter\Model;

class RolesModel extends Model {
    protected $table      = 'aplicativo_roles';
    protected $primaryKey = 'rol_id';

    protected $returnType    = 'App\Entities\Rol';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'rol_nombre','rol_menu'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * retorna los roles disponibles para aplicacion de evaluaciones
     */
    public function getRolesEvaluaciones()
    {
        $array_id = [1,6,7,8,11,13,14,15,16,18,21];
        return $this->whereNotIn('rol_id',$array_id)->find();
    }

    
}