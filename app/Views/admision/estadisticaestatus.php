<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<div class="container my-sm-5">
  <br>
  <form class="row g-3" method="post" action="<?php echo base_url('EstadisticasAdmision/estatusregistro'); ?>">
    <h1 style="display:flex; justify-content:center">Estatus en etapa de <?php echo $nombreetapa; ?> </h1>
    <input type="hidden" name="nombreetapa" type="text" value="<?php echo $nombreetapa?>"/>
        <div>
          <div class="row" style="padding-top: 10px;" style= 'text-align:"center"'>
            <?php if (isset($validacorreo)) : ?>
              <h2>Selecciona una opción</h2>
                <div class="mb-1 row col-sm-2" >
                    <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                        <label for="novalidado" class="form-label">Validados</label>
                        <input type='radio' id="validado" name="validado" value="1" onchange="this.form.submit();"/>
                    </div>
                    <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                        <label for="novalidado" class="form-label">No validados</label>
                        <input type='radio' id="novalidado" name="novalidado" value="0" onchange="this.form.submit();"/>
                    </div>
                    <div style="width: 500px;" class="col-sm-10" style= 'text-align:"center"'>
                        <label for="todos" class="form-label">Todos</label>
                        <input type='radio' id="todos" name="todos" value="1" onchange="this.form.submit();"/>
                    </div>
                </div>
            <?php endif; ?>
          </div>
        </div>
    </form>
</div>
    <h1 style="display:flex; justify-content:center">Número total de registros</h1>
    <input style="text-align: center;"  class="form-control" name="numero" type="text" value="<?php echo $numero ?>" disabled/>
    <br>
<figure class="highcharts-figure">
  <div id="container"></div>
  <?php if (isset($general)) : ?>
    <table id="datatable">
      <thead>
        <tr>
          <th></th>
          <th>Hombres</th>
          <th>Mujeres</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <th>Candidad</th>
          <td><?php echo $hombre?></td>
          <td><?php echo $mujer?></td>
        </tr>
      </tbody>
    </table>
  <?php endif; ?>
  <?php if (isset($total)) : ?>
    <table id="datatable">
      <thead>
        <tr>
          <th></th>
          <th>Correo validado</th>
          <th>Correo no validado</th>
          <th>Revisión documental</th>
          <th>Observaciones</th>
          <th>Registro validado</th>
          <th>Registro Declinado</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <th>Candidad</th>
        <td><?php echo $correovalidado?></td>
        <td><?php echo $correonovalidado?></td>
        <td><?php echo $revision?></td>
        <td><?php echo $observaciones?></td>
        <td><?php echo $validado?></td>
        <td><?php echo $declinado?></td>
        </tr>
      </tbody>
    </table>
  <?php endif; ?>
</figure>
<script>
    Highcharts.chart('container', {
    data: {
      table: 'datatable'
    },
    chart: {
      type: 'column'
    },
    title: {
      text: 'grafica en tiempo real'
    },
    yAxis: {
      allowDecimals: false,
      title: {
        text: 'Cantidad de aspirantes'
      }
    },
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          this.point.y + ' ' + this.point.name.toLowerCase();
      }
    }
  });
</script>
<?php echo $this->endSection() ?>