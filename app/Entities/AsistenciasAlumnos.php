<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AsistenciasAlumnos extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id_grupo' => NULL,
        'matricula' => NULL,
        'docente' => NULL,
        'fecha' => NULL,
        'nombre_aula' => NULL,
        'tipo_asistencia' => NULL,
        'id_alumno' => NULL,
        'nombre_alumno' => NULL,
        'ap_paterno_alumno' => NULL,
        'ap_materno_alumno' => NULL,
        'status_alumno' => NULL
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
        'id_asistencia' => 'asistencia_id',
        'id_grupo' => 'asistencia_grupo',
        'matricula' => 'asistencia_matricula',
        'docente' => 'asistencia_docente',
        'fecha' => 'asistencia_fecha',
        'nombre_aula' => 'asistencia_aula',
        'tipo_asistencia' => 'asistencia_tipo',
        'id_alumno' => 'alumno_id',
        'nombre_alumno' => 'alumno_nombres',
        'ap_paterno_alumno' => 'alumno_ap_paterno',
        'ap_materno_alumno' => 'alumno_ap_materno',
        'status_alumno' => 'alumno_status'
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}