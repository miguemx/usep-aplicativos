-- se inserta un nuevo espacio para iniciar el proceso
INSERT INTO `aca_aulas` (`aula_id`, `aula_nombre`, `aula_descripccion`, `aula_cupo_mx`) VALUES ('2', 'POR ASIGNAR', 'POR ASIGNAR', '50');

-- se actualiza aca_grupos_aulas para que tengan este nuevo espacio
update aca_grupos_aulas set gpoaula_aula ="2" where gpoaula_periodo="2022A" and gpoaula_aula="1";