--
-- Volcado de datos para la tabla `aspirantes_tipos_campo`
--

INSERT INTO `aspirantes_tipos_campo` VALUES(1, 'TEXTO', '/^[\\w\\s]+$/', 'BASIC');
INSERT INTO `aspirantes_tipos_campo` VALUES(2, 'NUMERO', '/^[0-9]+$/', 'BASIC');
INSERT INTO `aspirantes_tipos_campo` VALUES(3, 'FECHA', '/^[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}$/', 'BASIC');
INSERT INTO `aspirantes_tipos_campo` VALUES(4, 'DOCUMENTO', 'pdf,jpg,jpeg,png', 'ARCHIVO');
INSERT INTO `aspirantes_tipos_campo` VALUES(5, 'RESPUESTA SI O NO', 'SI,NO', 'COLECCION');