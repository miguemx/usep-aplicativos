<?php
namespace App\Models;
use CodeIgniter\Model;


class AspirantesConfiguracionModel extends Model {
    protected $table      = 'aspirantes_configuracion';
    protected $primaryKey = 'configuracion_id';

    protected $returnType    = 'App\Entities\AspirantesConfiguracion';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'configuracion_id','configuracion_nombre','configuracion_status','created_at','updated_at','deleted_at'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

     /**
     * consulta una configuración 
     * @param conf id de la configuración a consultar
     * @return configuracion registro completo de la configuracion consultada
     */
        public function configuracion( $conf ) {
            $configuracion = $this->where( 'configuracion_id', $conf)->first();
            return $configuracion;
            //$this->find($conf);
        
    }
}