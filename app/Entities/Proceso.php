<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Proceso extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'aspirante'                    => null,
        'registro'               => null,
        'validacion'             => null,
        'captura'             => null,
        'documentos'                => null,
        'revision'                  => null,
        'exito'                => null,
        'declinado'              => null,
        'carta'                 => null,
        'rechazado'                 => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'aspirante'                    => 'proceso_aspirante',
        'registro'               => 'proceso_registro',
        'validacion'             => 'proceso_validacion',
        'captura'             => 'proceso_captura',
        'documentos'                => 'proceso_documentos',
        'revision'                  => 'proceso_revision',
        'exito'                => 'proceso_exito',
        'declinado'              => 'proceso_declinado',
        'carta'                 => 'proceso_carta',
        'rechazado'                 => 'proceso_rechazado',
        
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
