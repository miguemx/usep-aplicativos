<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>


<div class="container-fluid">
    <div>&nbsp;</div>

    <div class="container">
    <div class="bg-white rounded shadow p-4 p-2 mt-2 mb-4">
        <div class="col-sm-12">
        
        <?php if(isset($errores) && count($errores) > 0): ?>
            <div class="alert alert-danger">
                <?php foreach($errores as $error): ?>
                    <?php echo $error.'<br />'; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        
        
        <h5>Agregar una cuota</h5>
        <form name="agregarcuota" method="post" action="<?php echo base_url('Inscripcion/GuardaCuota').'/'.($alumno->id); ?>" enctype="multipart/form-data">
        <!-- <form name="agregarcuota" method="post" action="<?php // echo base_url('Inscripcion/GuardaCuota').'/'.\App\Libraries\Cifrado::cifrar($alumno->id); ?>" enctype="multipart/form-data"> -->
            
            <div class="col-sm-12 mt-3">
                <label for="concepto" class="form-label">Concepto*</label>
                <select class="form-select" name="concepto" id="concepto" onchange="actualizaMonto(this)" >
                    <option value="4">USEP CUOTA INTEGRAL DE INSCRIPCION</option>
                </select>
            </div>

            <div class="col-sm-12 mt-3">
                <label for="monto" class="form-label">Monto*</label>
                <input type="text" class="form-control" id="monto" name="monto" readonly="readonly" value="2220" />
            </div>

            <div class="col-sm-12 mt-3">
                <label for="referencia" class="form-label">Núm. de referencia*</label>
                <input type="text" class="form-control" id="referencia" name="referencia" value="<?php echo $referencia; ?>" />
            </div>

            <!-- <div class="col-sm-12 mt-3"> -->
                <!-- <label for="comprobante" class="form-label">Comprobante*</label> -->
                <input type="file" class="form-control" id="comprobante" name="comprobante" hidden value="" />
            <!-- </div> -->

            <div class="col-sm-12 mt-3">
                <label for="tipo" class="form-label">Tipo de cuota*</label>
                <select class="form-select" name="tipo" id="tipo" >
                    <option value="INSCRIPCION" selected>INSCRIPCIÓN (NUEVO INGRESO)</option>
                </select>
            </div>
            <select class="form-select" name="materia" id="materia" hidden>
            </select>
            <div class="col-sm-12 mt-3">
                <button type="submit" name="pago" value="pago" class="btn btn-secondary">
                    Agregar
                </button>
            </div>
        </form>
        </div>
    </div>
    </div>
   
</div>


<script type="text/javascript">

var conceptos = {};

<?php foreach( $conceptos as $concept ): ?>
    conceptos[ '<?php echo $concept->id ?>' ] = '<?php echo $concept->monto; ?>';
<?php endforeach; ?>

function actualizaMonto(control) {
    document.getElementById('monto').value = ( !isNaN(conceptos[control.value]) ) ? conceptos[control.value] : '' ;
}

function muestraMaterias(control) {
    let valor = control.value;
    if ( valor == 'EXTRAORDINARIO' || valor == 'RECURSO' ) {
        document.getElementById('divmateria').style.display = 'block';
    }
    else {
        document.getElementById('divmateria').style.display = 'none';
    }
}

</script>


<?php echo $this->endSection() ?>