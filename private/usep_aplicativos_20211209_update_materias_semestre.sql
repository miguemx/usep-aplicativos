UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-01';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-02';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-03';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-04';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-05';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-06';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'LEO-07';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-01"]}', `materia_semestre`='2' WHERE `materia_clave` = 'LEO-08';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-02"]}', `materia_semestre`='2' WHERE `materia_clave` = 'LEO-09';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-03"]}', `materia_semestre`='2' WHERE `materia_clave` = 'LEO-10';
UPDATE `aca_materias` SET  `materia_semestre`='2' WHERE `materia_clave` = 'LEO-11';
UPDATE `aca_materias` SET  `materia_semestre`='2' WHERE `materia_clave` = 'LEO-12';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-07"]}', `materia_semestre`='2' WHERE `materia_clave` = 'LEO-13';
UPDATE `aca_materias` SET  `materia_semestre`='2' WHERE `materia_clave` = 'LEO-14';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-08"]}', `materia_semestre`='3' WHERE `materia_clave` = 'LEO-15';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-09"]}', `materia_semestre`='3' WHERE `materia_clave` = 'LEO-16';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-10"]}', `materia_semestre`='3' WHERE `materia_clave` = 'LEO-17';
UPDATE `aca_materias` SET `materia_semestre`='3' WHERE `materia_clave` = 'LEO-18';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-12"]}', `materia_semestre`='3' WHERE `materia_clave` = 'LEO-19';
UPDATE `aca_materias` SET `materia_semestre`='3' WHERE `materia_clave` = 'LEO-20';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-14"]}', `materia_semestre`='3' WHERE `materia_clave` = 'LEO-21';


UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-20"]}',`materia_semestre`='4' WHERE `materia_clave` = 'LEO-22';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-17"]}',`materia_semestre`='4' WHERE `materia_clave` = 'LEO-23';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'LEO-24';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'LEO-25';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'LEO-26';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-19"]}',`materia_semestre`='4' WHERE `materia_clave` = 'LEO-27';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-21"]}',`materia_semestre`='4' WHERE `materia_clave` = 'LEO-28';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-22"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-29';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-23"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-30';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-18"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-31';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-31"],"correquisito":["LEO-31"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-32';
UPDATE `aca_materias` SET `materia_semestre`='5' WHERE `materia_clave` = 'LEO-33';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-24"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-34';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-28"]}',`materia_semestre`='5' WHERE `materia_clave` = 'LEO-35';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-29"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-36';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-30"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-37';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-31"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-38';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-32"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-39';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-33"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-40';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-34"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-41';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-35"]}',`materia_semestre`='6' WHERE `materia_clave` = 'LEO-42';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-36"]}',`materia_semestre`='7' WHERE `materia_clave` = 'LEO-43';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-38"]}',`materia_semestre`='7' WHERE `materia_clave` = 'LEO-44';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'LEO-45';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-40"]}',`materia_semestre`='7' WHERE `materia_clave` = 'LEO-46';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'LEO-47';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-42"]}',`materia_semestre`='7' WHERE `materia_clave` = 'LEO-48';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-43"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-49';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-37"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-50';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-45"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-51';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-46"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-52';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-48"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-53';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["LEO-53"],"correquisito":["LEO-53"]}',`materia_semestre`='8' WHERE `materia_clave` = 'LEO-54';


UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-111';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-112';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-113';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-114';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-115';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-116';
UPDATE `aca_materias` SET `materia_semestre`='1' WHERE `materia_clave` = 'MC-117';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-111"]}' ,`materia_semestre`='2' WHERE `materia_clave` = 'MC-121';
UPDATE `aca_materias` SET `materia_semestre`='2' WHERE `materia_clave` = 'MC-122';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-113"]}' ,`materia_semestre`='2' WHERE `materia_clave` = 'MC-123';
UPDATE `aca_materias` SET `materia_semestre`='2' WHERE `materia_clave` = 'MC-124';
UPDATE `aca_materias` SET `materia_semestre`='2' WHERE `materia_clave` = 'MC-125';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-116"]}' ,`materia_semestre`='2' WHERE `materia_clave` = 'MC-126';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-112"]}',`materia_semestre`='3' WHERE `materia_clave` = 'MC-211';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-122"]}',`materia_semestre`='3' WHERE `materia_clave` = 'MC-212';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-114"]}',`materia_semestre`='3' WHERE `materia_clave` = 'MC-213';
UPDATE `aca_materias` SET `materia_semestre`='3' WHERE `materia_clave` = 'MC-214';
UPDATE `aca_materias` SET `materia_semestre`='3' WHERE `materia_clave` = 'MC-215';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-126"]}',`materia_semestre`='3' WHERE `materia_clave` = 'MC-216';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-117"]}',`materia_semestre`='3' WHERE `materia_clave` = 'MC-218';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-211"]}',`materia_semestre`='4' WHERE `materia_clave` = 'MC-221';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'MC-222';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'MC-223';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'MC-224';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'MC-225';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-216"]}',`materia_semestre`='4' WHERE `materia_clave` = 'MC-226';
UPDATE `aca_materias` SET `materia_semestre`='4' WHERE `materia_clave` = 'MC-228';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-221"]}',`materia_semestre`='5' WHERE `materia_clave` = 'MC-311';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-212"]}',`materia_semestre`='5' WHERE `materia_clave` = 'MC-312';
UPDATE `aca_materias` SET `materia_semestre`='5' WHERE `materia_clave` = 'MC-313';
UPDATE `aca_materias` SET `materia_semestre`='5' WHERE `materia_clave` = 'MC-314';
UPDATE `aca_materias` SET `materia_semestre`='5' WHERE `materia_clave` = 'MC-315';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-226"]}',`materia_semestre`='5' WHERE `materia_clave` = 'MC-316';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-115"]}',`materia_semestre`='5' WHERE `materia_clave` = 'MC-317';
UPDATE `aca_materias` SET `materia_semestre`='5' WHERE `materia_clave` = 'MC-318';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-311"]}',`materia_semestre`='6' WHERE `materia_clave` = 'MC-321';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-114"]}',`materia_semestre`='6' WHERE `materia_clave` = 'MC-322';
UPDATE `aca_materias` SET `materia_semestre`='6' WHERE `materia_clave` = 'MC-323';
UPDATE `aca_materias` SET `materia_semestre`='6' WHERE `materia_clave` = 'MC-324';
UPDATE `aca_materias` SET `materia_semestre`='6' WHERE `materia_clave` = 'MC-325';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-316"]}',`materia_semestre`='6' WHERE `materia_clave` = 'MC-326';
UPDATE `aca_materias` SET `materia_semestre`='6' WHERE `materia_clave` = 'MC-327';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-228"]}',`materia_semestre`='6' WHERE `materia_clave` = 'MC-328';

UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'MC-411';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-322"]}',`materia_semestre`='7' WHERE `materia_clave` = 'MC-412';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'MC-414';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'MC-415';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-326"]}',`materia_semestre`='7' WHERE `materia_clave` = 'MC-416';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'MC-417';
UPDATE `aca_materias` SET `materia_semestre`='7' WHERE `materia_clave` = 'MC-418';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-218"]}',`materia_semestre`='7' WHERE `materia_clave` = 'MC-419';

UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-411"]}',`materia_semestre`='8' WHERE `materia_clave` = 'MC-421';
UPDATE `aca_materias` SET `materia_semestre`='8' WHERE `materia_clave` = 'MC-422';
UPDATE `aca_materias` SET `materia_semestre`='8' WHERE `materia_clave` = 'MC-423';
UPDATE `aca_materias` SET `materia_semestre`='8' WHERE `materia_clave` = 'MC-424';
UPDATE `aca_materias` SET `materia_semestre`='8' WHERE `materia_clave` = 'MC-425';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-416"]}',`materia_semestre`='8' WHERE `materia_clave` = 'MC-426';
UPDATE `aca_materias` SET `materia_semestre`='8' WHERE `materia_clave` = 'MC-427';
UPDATE `aca_materias` SET `materia_seriacion`='{"prerrequisitos":["MC-419"]}',`materia_semestre`='8' WHERE `materia_clave` = 'MC-428';

UPDATE `aca_materias` SET `materia_semestre`='9',`materia_seriacion`='{"prerrequisitos":["MC-121","MC-123","MC-124","MC-125","MC-213","MC-214","MC-215","MC-222","MC-223","MC-224","MC-225","MC-312","MC-313","MC-314","MC-315","MC-318","MC-321","MC-323","MC-324","MC-325","MC-327","MC-328","MC-412","MC-414","MC-415","MC-417","MC-418","MC-421","MC-422","MC-423","MC-424","MC-425","MC-426","MC-427","MC-428"]}' WHERE `materia_clave` = 'MC-511';
UPDATE `aca_materias` SET `materia_semestre`='10',`materia_seriacion`='{"prerrequisitos":["MC-511"]}' WHERE `materia_clave` = 'MC-521';