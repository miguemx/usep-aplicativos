<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>
<style>
    #container {
      height: 400px;
    }
    .highcharts-figure,
  .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
  }
  
  #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }

  #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
  }

  #datatable th {
    font-weight: 600;
    padding: 0.5em;
  }

  #datatable td,
  #datatable th,
  #datatable caption {
    padding: 0.5em;
  }

  #datatable thead tr,
  #datatable tr:nth-child(even) {
    background: #f8f8f8;
  }

  #datatable tr:hover {
    background: #f1f7ff;
  }
</style>
<head>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>
<?php $suma_foraneos=$foraneo_m_enf->aspirante_id + $foraneo_h_enf->aspirante_id + $foraneo_m_med->aspirante_id + $foraneo_h_med->aspirante_id;
           $suma_extranjeros=$ext_m_enf->aspirante_id + $ext_h_enf->aspirante_id + $ext_m_med->aspirante_id + $ext_h_med->aspirante_id;?>

<div class="container ">
  <div class="bg-white rounded shadow p-5 mb-4 mt-4 ml-2 mr-2">
    <div class="card-header">
      <h1 style="display:flex; justify-content:center">Estadística de Aspirantes detallada por Región</h1>
    </div>
    <div class="card-body">
      <figure class="highcharts-figure">
          <div id="container_detail"></div>
            <table id="datatable">
              <thead>
                <tr>
                  <th>Región</th>
                  <th>Mujeres de Enfermería</th>
                  <th>Hombres de Enfermería</th>
                  <th>Mujeres de Medicina</th>
                  <th>Hombres de Medicina</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($est_region_sum as $est_det) :?>
                  <tr>
                    <td><?php echo $est_det->region_nombre; ?></td>
                    <td><?php echo $est_det->n_alumnos_m_enf; ?></td>
                    <td><?php echo $est_det->n_alumnos_h_enf; ?></td>
                    <td><?php echo $est_det->n_alumnos_m_med; ?></td>
                    <td><?php echo $est_det->n_alumnos_h_med; ?></td>
                    <td><b><?php echo $est_det->n_alumnos_m_enf+$est_det->n_alumnos_h_enf+$est_det->n_alumnos_m_med+$est_det->n_alumnos_h_med; ?></b></td>
                  </tr>
                <?php endforeach?>
                <tr>
                  <td>Otros Estados</td>
                  <td><?php echo $foraneo_m_enf->aspirante_id; ?></td>
                  <td><?php echo $foraneo_h_enf->aspirante_id; ?></td>
                  <td><?php echo $foraneo_m_med->aspirante_id; ?></td>
                  <td><?php echo $foraneo_h_med->aspirante_id; ?></td>
                  <td><b><?php echo $suma_foraneos; ?></b></td>
                </tr>
                <tr>
                  <td>Extranjeros</td>
                  <td><?php echo $ext_m_enf->aspirante_id; ?></td>
                  <td><?php echo $ext_h_enf->aspirante_id; ?></td>
                  <td><?php echo $ext_m_med->aspirante_id; ?></td>
                  <td><?php echo $ext_h_med->aspirante_id; ?></td>
                  <td><b><?php echo $suma_extranjeros; ?></b></td>
                </tr>
              </tbody>
            </table>
      </figure>
    </div>
  </div>
</div>
<script>
    

 Highcharts.chart('container_detail', {

chart: {
  type: 'column'
},

title: {
  text: 'Aspirantes por Género y Carrera'
},

xAxis: {
  categories: [<?php foreach ($estadistica as $estadisticas) :
           echo "'".$estadisticas->region_nombre."',";
         endforeach; 
         ?>
         'Otros Estados','Extranjeros'
]
},

yAxis: {
  allowDecimals: false,
  min: 0,
  title: {
    text: 'Cantidad de Aspirantes'
  }
},

tooltip: {
  formatter: function () {
    return '<b>' + this.x + '</b><br/>' +
      this.series.name + ': ' + this.y + '<br/>' +
      'Total: ' + this.point.stackTotal;
  }
},

plotOptions: {
  column: {
    stacking: 'normal'
  }
},

series: [{
  name: 'Mujeres de Enfermería',
  data: [<?php
    foreach ($est_region_sum as $est_det) :
          echo $est_det->n_alumnos_m_enf.",";
        endforeach; 
        echo $foraneo_m_enf->aspirante_id.",";
        echo $ext_m_enf->aspirante_id.",";
        ?>
  ],
  color: '#B37D0B',
  stack: 'Enfermeria'
}, {
  name: 'Hombres de Enfermería',
  data: [
    <?php
        foreach ($est_region_sum as $est_det) :
          echo $est_det->n_alumnos_h_enf.",";
        endforeach;
        echo $foraneo_h_enf->aspirante_id.",";
        echo $ext_h_enf->aspirante_id.",";
        ?>
  ],
  color: '#051F03',
  stack: 'Enfermeria'
}, {
  name: 'Mujeres de Medicina',
  data: [
    <?php
        foreach ($est_region_sum as $est_det) :
          echo $est_det->n_alumnos_m_med.",";
        endforeach; 
        echo $foraneo_m_med->aspirante_id.",";
        echo $ext_m_med->aspirante_id.",";
        ?>
  ],
  color: '#630505',
  stack: 'Medicina'
}, {
  name: 'Hombres de Medicina',
  data: [
    <?php
        foreach ($est_region_sum as $est_det) :
          echo $est_det->n_alumnos_h_med.",";
        endforeach; 
        echo $foraneo_h_med->aspirante_id.",";
        echo $ext_h_med->aspirante_id.",";
        ?>
  ],
  color: '#5C5151',
  stack: 'Medicina'
}]
});

  
  let creditos = document.getElementsByClassName('highcharts-credits');
    creditos[0].childNodes[0].data = '';
    creditos[1].childNodes[0].data = '';
</script>
<?php echo $this->endSection() ?>