<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container-fluid">
    <div class="text-center form-group mt-4">
        </div>
        <?php if ( isset($error) ): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($mensajeok)) : ?>
            <div class="alert alert-success" role="alert">
                <?php echo $mensajeok; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($mensajeerror)) : ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $mensajeerror; ?>
            </div>
        <?php endif; ?>
        <table class="table table-borderless table-hover mt-3 text-center">
            <div class="bg-white rounded shadow p-2 mb-4 mt-4 ml-2 mr-2 ">
            <thead class="thead-default" style="position: sticky;top: 50px; background-color:whitesmoke;">
                <tr>
                    <th scope="row">HORA</th>
                    <th>LUNES</th>
                    <th>MARTES</th>
                    <th>MIERCOLES</th>
                    <th>JUEVES</th>
                    <th>VIERNES</th>
                    <th>SABADO</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $indicador = 0;
                $hora = 7;
                ?>
                <?php $bandera_row_dias = [0, 0, 0, 0, 0, 0, 0]; ?>
                <?php while ($hora < 21) : ?>
                    <?php $index = 0; ?>
                    <tr>
                        <td scope="row" style="background-color: #5c5c63; color:aliceblue;"><?php echo (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59')) ?> </td>
                        <?php //echo json_encode($bandera_row_dias)." $indicador <br>" 
                            $contador = 0; 
                            ?>
                        <?php foreach ($dias as $dia) : ?>
                            <?php //echo json_encode($bandera_row_dias)." $indicador <br>" 
                            $contador = $contador+1; 
                            ?>
                            <?php $bandera_coincidencia = false; ?>
                            <?php foreach ($grupos as $grupo) : ?>
                                <?php if ($grupo->dia == $dia) : ?>
                                    <?php if ($grupo->inicio == (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00'))) : ?>
                                        <?php
                                        $inicio = explode(':', $grupo->inicio);
                                        $fin = explode(':', $grupo->fin);
                                        $row_span = date_diff(date_create($grupo->inicio), date_create($grupo->fin));
                                        ?>
                                        <td class="align-middle" rowspan="<?php echo (($row_span->h >= 1) ? $row_span->h : 1);  ?>">
                                            <div <?php echo $grupo->h_inicio . " " . $grupo->h_fin ?>>
                                                <?php if ($row_span->h  >= 1) {
                                                    $bandera_row_dias[$index] += $row_span->h;
                                                } ?>
                                                <?php echo  $grupo->materia_nombre . "<br>" . $grupo->seccion . '<br>' . $grupo->aula_nombre .'<br>'?>
                                                <?php 
                                                ?>
                                                <form  action="<?php echo base_url('Docente/actualizacion_capturaEx'); ?>" method="post">
                                                    <input hidden name="docente" type="" value="<?php echo $docente; ?>" />
                                                    <input hidden name="idgrupo" type="" value="<?php echo $grupo->grupo_id; ?>" />
                                                    <input hidden name="hora" type="" value="<?php echo $hora.':00' ?>" />
                                                    <input hidden name="nombre_materia" type="" value="<?php echo $grupo->materia_nombre ?>" />
                                                    <input hidden name="aula" type="" value="<?php echo $grupo->aula_nombre?>" />
                                                    <input hidden id='' type="" value="<?php echo $contador?>" />
                                                    <input hidden name='seccion' type="" value="<?php echo $grupo->seccion?>" />
                                                    <input hidden name='dia' type="" value="<?php echo $grupo->dia?>" />
                                                    <input type="date" id="fecha" name="fecha" required="1" min="<?php echo $fechamin; ?>" max="<?php echo $fechamax;?>" onchange="validar(event,<?php echo $contador?>)"/>
                                                    <br>                            
                                                    <br>                            
                                                    <button type="submit" name="modificación" value="1" class="btn btn-secondary btn-sm">
                                                        Modificación de asistencias
                                                    </button>
                                                    <br>
                                                    <br>
                                                    <button type="submit" name="captura_ex" value="1" class="btn btn-secondary btn-sm">
                                                       Captura extemporanea
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <?php $bandera_coincidencia = true; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if (!$bandera_coincidencia) : ?>
                                <?php if ($bandera_row_dias[$index] > 1) : ?>
                                    <?php $bandera_row_dias[$index]--; ?>
                                <?php else : ?>
                                    <td style="background-color: #bdb994;">
                                        <?php //echo $dia . " " . (($hora <= 9) ? ('0' . $hora . ':00') : ($hora . ':00')) . " - " . (($hora <= 9) ? ('0' . $hora . ':59') : ($hora . ':59'))." ".$indicador 
                                        ?>
                                    </td>
                                <?php endif; ?>
                            <?php endif; ?>


                            <?php $index++; ?>
                            <?php $indicador++; ?>
                        <?php endforeach; ?>
                    </tr>
                    <?php $hora++; ?>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function validar(e, num_dia) {
    const dia = (new Date(e.target.value)).getDay() + 1;
    console.log('dia_seleccionado' + dia);
    if (dia != num_dia) { //Comparación del día seleccionado con el día correcto
        e.target.value = ""; //Resetear la fecha
        alert("Fecha inválida, selecciona el día correcto"); //Dar feedback al usuario
    }
    }    
</script>
<?php echo $this->endSection() ?>