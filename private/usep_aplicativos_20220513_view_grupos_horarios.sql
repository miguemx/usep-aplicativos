CREATE VIEW view_grupos_horarios AS
SELECT
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_grupo` AS `gpoaula_grupo`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_dia` AS `gpoaula_dia`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_inicio` AS `gpoaula_inicio`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_fin` AS `gpoaula_fin`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_aula` AS `gpoaula_aula`,
    `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_periodo` AS `gpoaula_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_id` AS `grupo_id`,
    `usep_aplicativos`.`aca_grupos`.`grupo_idc` AS `grupo_idc`,
    `usep_aplicativos`.`aca_grupos`.`grupo_clave` AS `grupo_clave`,
    `usep_aplicativos`.`aca_grupos`.`grupo_folio` AS `grupo_folio`,
    `usep_aplicativos`.`aca_grupos`.`grupo_materia` AS `grupo_materia`,
    `usep_aplicativos`.`aca_grupos`.`grupo_docente` AS `grupo_docente`,
    `usep_aplicativos`.`aca_grupos`.`grupo_periodo` AS `grupo_periodo`,
    `usep_aplicativos`.`aca_grupos`.`grupo_max` AS `grupo_max`,
    `usep_aplicativos`.`aca_grupos`.`grupo_ocupado` AS `grupo_ocupado`,
    `usep_aplicativos`.`aca_grupos`.`grupo_flag_acta` AS `grupo_flag_acta`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechacaptura` AS `grupo_fechacaptura`,
    `usep_aplicativos`.`aca_grupos`.`grupo_fechaimpresion` AS `grupo_fechaimpresion`,
    `usep_aplicativos`.`aca_grupos`.`grupo_historial_acta` AS `grupo_historial_acta`,
    `usep_aplicativos`.`aca_aulas`.`aula_id` AS `aula_id`,
    `usep_aplicativos`.`aca_aulas`.`aula_nombre` AS `aula_nombre`,
    `usep_aplicativos`.`aca_aulas`.`aula_descripccion` AS `aula_descripccion`,
    `usep_aplicativos`.`aca_aulas`.`aula_cupo_mx` AS `aula_cupo_mx`,
    `usep_aplicativos`.`aca_materias`.`materia_clave` AS `materia_clave`,
    `usep_aplicativos`.`aca_materias`.`materia_carrera` AS `materia_carrera`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre` AS `materia_nombre`,
    `usep_aplicativos`.`aca_materias`.`materia_nombre_corto` AS `materia_nombre_corto`,
    `usep_aplicativos`.`aca_materias`.`materia_creditos` AS `materia_creditos`,
    `usep_aplicativos`.`aca_materias`.`materia_obligatoria` AS `materia_obligatoria`,
    `usep_aplicativos`.`aca_materias`.`materia_tipo` AS `materia_tipo`,
    `usep_aplicativos`.`aca_materias`.`materia_clasificacion` AS `materia_clasificacion`,
    `usep_aplicativos`.`aca_materias`.`materia_seriacion` AS `materia_seriacion`,
    `usep_aplicativos`.`aca_materias`.`materia_semestre` AS `materia_semestre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` AS `empleado_id`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_correo` AS `empleado_correo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_horario` AS `empleado_horario`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_area` AS `empleado_area`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_nombre` AS `empleado_nombre`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_apellido` AS `empleado_apellido`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_paterno` AS `empleado_ap_paterno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_ap_materno` AS `empleado_ap_materno`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_categoria` AS `empleado_categoria`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_edad` AS `empleado_edad`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_sexo` AS `empleado_sexo`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regid` AS `empleado_else_regid`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_else_regidpass` AS `empleado_else_regidpass`,
    `usep_aplicativos`.`aplicativo_empleados`.`empleado_flg_captura` AS `empleado_flg_captura`,
    `usep_aplicativos`.`aplicativo_empleados`.`created_at` AS `created_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`updated_at` AS `updated_at`,
    `usep_aplicativos`.`aplicativo_empleados`.`deleted_at` AS `deleted_at`
FROM
    (
        (
            (
                (
                    `usep_aplicativos`.`aca_grupos_aulas`
                JOIN `usep_aplicativos`.`aca_grupos` ON
                    (
                        `usep_aplicativos`.`aca_grupos`.`grupo_id` = `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_grupo`
                    )
                )
            JOIN `usep_aplicativos`.`aca_aulas` ON
                (
                    `usep_aplicativos`.`aca_aulas`.`aula_id` = `usep_aplicativos`.`aca_grupos_aulas`.`gpoaula_aula`
                )
            )
        JOIN `usep_aplicativos`.`aca_materias` ON
            (
                `usep_aplicativos`.`aca_materias`.`materia_clave` = `usep_aplicativos`.`aca_grupos`.`grupo_materia`
            )
        )
    JOIN `usep_aplicativos`.`aplicativo_empleados` ON
        (
            `usep_aplicativos`.`aplicativo_empleados`.`empleado_id` = `usep_aplicativos`.`aca_grupos`.`grupo_docente`
        )
    );