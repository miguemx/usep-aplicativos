<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroUsuariosExternosModel extends Model {
    protected $table      = 'filtro_usuarios_ext';
    protected $primaryKey = 'externo_correo';

    protected $returnType    = 'App\Entities\FiltroUsuarioExterno';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'externo_correo','externo_nombre','externo_dependencia','externo_edad','externo_sexo','externo_categoria',
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca los datos de un usuario externo
     * @param correo el correo para buscar al usuario
     * @return usuario los datos de usuario en un arreglo con llave ID y NOMBRE
     */
    public function sessionData( $correo ) {
        $usuario = [ 'id'=>'0', 'nombre'=>'', 'correo'=>'' ];
        $usr = $this->find( $correo );
        if ( !is_null($usr) ) {
            $usuario = [ 'id'=>$usr->correo, 'nombre'=>$usr->nombre, 'correo'=>$correo ];
        }
        return $usuario;
    }
    
}