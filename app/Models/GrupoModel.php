<?php

namespace App\Models;

use CodeIgniter\Model;

use \App\Entities\Alumno;

class GrupoModel extends Model
{
    protected $table      = 'aca_grupos';
    protected $primaryKey = 'grupo_id';

    protected $returnType    = 'App\Entities\Grupo';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'grupo_id', 'grupo_idc', 'grupo_clave', 'grupo_folio', 'grupo_materia', 'grupo_docente',
        'grupo_periodo', 'grupo_ocupado', 'grupo_flag_acta','grupo_fechacaptura','grupo_fechaimpresion',
        'grupo_historial_acta','grupo_max', 'grupo_classroom_id',
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId()
    {
        return $this->db->insertID();
    }

    public function lista()
    {
        $this->orderBy('grupo_clave', 'ASC');
        $this->orderBy('grupo_materia', 'ASC');
        $grupos = $this->findAll();
        return $grupos;
    }
    /**
     * busca los grupos de acuerdo a los criterios de busqueda
     * @return grupos el listado de grupos encontrados
     */
    public function busca($filtros)
    {
        $this->creaFiltros($filtros);
        $grupos = $this->findAll();
        return $grupos;
    }

    /**
     * encuentra un grupo de acuerdo a la materia y la seccion (clave) del mismo
     * @param materia la materia a buscar (ID)
     * @param seccion  la seccion (clave) a buscar
     * @param periodo el periodo del grupo
     * @return Grupo la entidad de grupo con los datos encontrados
     * @return null en caso de que no exista o exista mas de uno
     */
    public function encuentra($materia, $seccion, $periodo)
    {
        $this->creaFiltros(['materia' => $materia, 'seccion' => $seccion, 'periodo' => $periodo]);
        $grupos = $this->findAll();
        if (count($grupos) === 1) {
            return $grupos[0];
        } else if (count($grupos) === 2) {
            if ($grupos[0]->folio < $grupos[1]->folio)
                return $grupos[0];
            else {
                return $grupos[1];
            }
        }
        return null;
    }

    /**
     * crea los filtros where para la busqueda de grupos
     * @param filtros el arreglo con los filtros de busqueda
     */
    private function creaFiltros($filtros)
    {
        if (array_key_exists('materia', $filtros)) { // -- filtrar por materia
            if (strlen($filtros['materia'])) {
                $this->where('grupo_materia', $filtros['materia']);
            }
        }
        if (array_key_exists('seccion', $filtros)) { // -- filtrar por seccion
            if (strlen($filtros['seccion'])) {
                $this->where('grupo_clave', $filtros['seccion']);
            }
        }
        if (array_key_exists('periodo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['periodo'])) {
                $this->where('grupo_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('docente', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['docente'])) {
                $this->where('grupo_docente', $filtros['docente']);
            }
        }
        if (array_key_exists('idcgrupo', $filtros)) { // -- filtrar por periodo
            if (strlen($filtros['idcgrupo'])) {
                $this->where('grupo_id', $filtros['idcgrupo']);
            }
        }
        if (array_key_exists('order', $filtros)) { // -- metodos de ordenamiento
            foreach($filtros['idcgrupo'] as $order){
                $this->orderBy($order);
            }
        }

    }


    // ================================================================================================
    /**
     * busca un alumno mediante su direccion de correo
     * @param correo el correo que se va a buscar
     * @return Alumno entidad de Alumno con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByEmail($correo)
    {
        $alumno = $this->where('alumno_correo', $correo)->first();
        return $alumno;
    }

    /**
     * Ontiene toda la informacion de un estudiante
     * @param matricula el ID o matricula del estudiante
     * @return App\Models\Alumno el objeto alumno con toda su informacion
     * @return null si no se encuentra el estudiante
     */
    public function getInfo($matricula)
    {
        $alumno = $this->find($matricula);
        if (!is_null($alumno)) {
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find($alumno->carrera);
            $alumno->carrera = $carrera;

            $segurosModel = new AlumnosSegurosModel();
            $seguro = $segurosModel->find($alumno->id);
            $alumno->ssn = $seguro->ssn;
        }
        return $alumno;
    }

    /**
     * busca estudiantes de acuerdo a sus filtros
     * @param filtros (opcional) un arreglo con los filtros para buscar
     */
    public function buscar($filtros = array())
    {
        $pager = \Config\Services::pager();
        $this->creaFiltros($filtros);
        $alumnos = $this->paginate(25);
        return $alumnos;
    }

    public function buscarGrupos($docente, $periodo)
    {
        $consulta = $this->where('grupo_docente', trim($docente))->where('grupo_periodo', $periodo)->findAll();
        return $consulta;
    }
    public function buscar_id($grupo, $periodo)
    {
        $consulta = $this->where('grupo_clave', $grupo)->where('grupo_periodo', $periodo)->find();
        $id = $consulta[0]->grupo_id;
        return $id;
    }
    public function actaCapturada($id_grupo)
    {
        return $this->db
        ->table('aca_grupos')
        ->where(["grupo_id" => $id_grupo])
        ->set(['grupo_flag_acta' => '1', 'grupo_fechacaptura' => date('Y-m-d H:i:s')])
        ->update();
    }
    public function obtenerBandera($id_grupo)
    {
        $consulta = $this->where('grupo_id', $id_grupo)->find();
        return $consulta;
    }
    public function agregarjson($array_json, $grupo)
    {
        $sql = "UPDATE `aca_grupos` SET `grupo_historial_acta` = '$array_json' WHERE `grupo_id` = '$grupo'";
        $this->db->query($sql);
    }
    public function printjson($grupo)
    {
        $consulta = $this->select('grupo_historial_acta')->where('grupo_id', $grupo)->find();
        return $consulta[0]->grupo_historial_acta;
        //return $consulta;
    }
    public function buscarclave($clave){
        $consulta = $this->where('grupo_materia', $clave)->find();
        return $consulta;
    }
    public function buscargrupoclave($clave,$periodo){
        $consulta = $this->where('grupo_clave', $clave)->where('grupo_periodo',$periodo)->find();
        return $consulta;
    }
    public function buscargrupoclavefetch($clave,$periodo){
        $consulta = $this->where('grupo_clave', $clave)->where('grupo_periodo',$periodo)->getCompiledSelect();
        return $consulta;
    }
    public function buscargrupoidc($id_grupo)
    {
        $consulta = $this->where('grupo_idc', $id_grupo)->find();
        return $consulta;
    }
    public function getgrupos($periodo){
        $consulta = $this->where('grupo_periodo',$periodo )->notLike('grupo_materia','MC-TUT')->find();
        return $consulta;
    }
    public function updategrupo($id,$folio){
        //$resultados = $this->db
        return $this->db
        ->table('aca_grupos')
        ->where(["grupo_id" => $id])
        ->set($folio)
        //->getCompiledUpdate();
        ->update();
        //return $resultados;
    }
    public function updategrupostut(){
        //$resultados = $this->db
        return $this->db
        ->table('aca_grupos')
        ->where(["grupo_periodo" => '2021A'])
        ->Like('grupo_materia','MC-TUT')
        ->set('grupo_folio',null)
        //->getCompiledUpdate();
        ->update();
        //return $resultados;
    }
    /**
     * @param perido 
     * @return query contiene todos los grupos del periodo seleccionado
     */
    public function getByPeriodo($periodo)
    {
        return $this->where('grupo_periodo',$periodo)->find();
    }

    public function getGruposPorCarrea($carrera,$periodo)
    {
        return $this->like('grupo_id',$carrera,'after')->where('grupo_periodo',$periodo)->orderBy('grupo_id','ASC')->find();
    }

    /**
     * retorna los grupos que pertenezcan a la seccion en el periodo correspondiente ordenados por la clave de la materia
     * @param seccion ID de seccion
     * @param periodo ID del periodo
     */
    public function getBySeccion($seccion,$periodo){
        $consulta = $this->where('grupo_clave', $seccion)->where('grupo_periodo',$periodo)->orderBy('grupo_materia','ASC')->find();
        return $consulta;
    }

    /**
     * retorna el listado de secciones 
     * @param periodo
     */
    public function getSeccionesPeriodo($periodo,$carrera=false,$semestre = false)
    {
        $this->where('grupo_periodo',$periodo);
        if($carrera){
            $this->like('grupo_clave',$carrera,'after');
        }
        $consulta = $this->groupBy('grupo_clave')->find();

            return $consulta;
    }


    /**
     * retorna el bloque de grupos pertenecientes a un periodo omitiendo materias no curruculares
     * @param filtros array asociativo que contiene los filtros con los parametros de busqueda
     * @param 
     */
    public function getGruposCurriculares($filtros = array())
    {
        if (array_key_exists('periodo', $filtros)) { 
            if (strlen($filtros['periodo'])) {
                $this->where('grupo_periodo', $filtros['periodo']);
            }
        }
        if (array_key_exists('docentes_disponibles', $filtros)) { 
            
            $this->notLike('grupo_materia','TUT','before')->notLike('grupo_materia','SIM','before');
            $this ->groupBy('grupo_docente');
        }
        return $this->find();
        // return $this->notLike('grupo_materia','TUT','before')->notLike('grupo_materia','SIM','before')->where('grupo_periodo',$periodo)->find();
    }
    /**
     * retorna los docentes y las materias de un periodo
     * @param periodo
     * @param docente
     * @param grupos array que contiene un conjunto de grupos
     * @return consulta
     */
    public function getDocentesMaterias($periodo,$docente,$grupos)
    {
        $consulta = $this->distinct(true)
                         ->select('grupo_materia')
                         ->where('grupo_periodo',$periodo)
                         ->where('grupo_docente',$docente)
                         ->whereIn('grupo_id',$grupos)
                         ->orderBy('grupo_materia')
                         ->findall();
            return $consulta;
    }
    
    /**
     * retorna los DIFERENTES docentes y las materias de un periodo
     * @param periodo
     * @return consulta
     */
    public function getDocentes($periodo)
    {
        $consulta = $this->distinct(true)
                         ->select('grupo_docente')
                         ->where('grupo_periodo',$periodo)
                         ->orderBy('grupo_docente')
                         ->findall();
            return $consulta;
    }
}
