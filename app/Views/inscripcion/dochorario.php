<style type="text/css">
    *, div, p {
        font-size: 1.08rem;
        font-family: Arial, Helvetica, sans-serif;
    }
    p {
        text-align: justify;
        margin: 20px 35px;
    }
    table {
        font-family: Arial, Helvetica, sans-serif;
    }
    td {
        border-bottom: 1px dotted #969696;
    }
</style>


<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    Bienvenida(o) a la “Universidad de la Salud” del Estado de Puebla
</p>
<p>
    ¡Ya es parte de la comunidad USEP! Por ello, debe tomar en cuenta lo siguiente:
</p>
<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    I. Horario de clases
</p>

<div style="margin: 10px; padding: 10px; border: 1px solid #555;">
    <table class="table table-borered" style="font-size: 12px; font-family: Roboto+Mono">
        <thead>
            <tr>
                <th>Sección</th>    
                <th>Materia / Docente</th>

                <th width="12%" style="text-align: center;">Lunes</th>
                <th width="12%" style="text-align: center;">Martes</th>
                <th width="12%" style="text-align: center;">Miércoles</th>
                <th width="12%" style="text-align: center;">Jueves</th>
                <th width="12%" style="text-align: center;">Viernes</th>
                <th width="12%" style="text-align: center;">Sábado</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $horario as $materia ): ?>
                <tr>
                    <td style="text-align: center; font-size: 0.74rem;"><?php echo $materia['seccion']; ?></td>
                    <td style="text-align: left; font-size: 0.74rem;">
                        <?php echo $materia['materia']; ?><br />
                        <em><?php echo mb_strtoupper($materia['docente']); ?></em>
                    </td>
                    <?php if( array_key_exists('LUNES', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['LUNES'] as $hora=>$aula ): ?>
                                <?php echo $hora; ?><br />
                                <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?><br />
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>

                    <?php if( array_key_exists('MARTES', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['MARTES'] as $hora=>$aula ): ?>
                                
                                    <?php echo $hora; ?><br />
                                    <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>

                    <?php if( array_key_exists('MIERCOLES', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['MIERCOLES'] as $hora=>$aula ): ?>
                                
                                    <?php echo $hora; ?><br />
                                    <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>

                    <?php if( array_key_exists('JUEVES', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['JUEVES'] as $hora=>$aula ): ?>
                                
                                    <?php echo $hora; ?><br />
                                    <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>

                    <?php if( array_key_exists('VIERNES', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['VIERNES'] as $hora=>$aula ): ?>
                                
                                    <?php echo $hora; ?><br />
                                    <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>

                    <?php if( array_key_exists('SABADO', $materia) ): ?>
                        <td style="text-align: center; font-size: 0.74rem;">
                            <?php foreach( $materia['SABADO'] as $hora=>$aula ): ?>
                                
                                    <?php echo $hora; ?><br />
                                    <?php echo (($aula==1 ||$aula == 'SM' || $aula == 101)?'EN LINEA':$aula); ?>
                                
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td>&nbsp;</td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<p style="text-align: center;">
    Inicio de clases: 1 de agosto de 2022. <br />
    Recibirás mediante tu correo electrónico institucional información importante por parte de tu Dirección de Licenciatura.
</p>

<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    II. Correo electrónico institucional
</p>
<p>
    <strong style="text-align: left;">Correo:</strong>
    <?php echo $alumno->correo; ?>
    <br />
    <strong style="text-align: left;">Contraseña:</strong>
    <?php echo substr( $alumno->curp, 0, 10).'*'; ?>
</p>

<p>
    Podrá acceder al mismo en un lapso de 48 a 72 horas, a través de Gmail (https://mail.google.com). 
</p>
<p>
    El correo electrónico institucional le da acceso a USEP Digital (https://usep.digital/login), portal en el que encontrará la gama de servicios escolares ofertados por la Dirección de Servicios Escolares y Titulación (DSEyT). 
</p>

<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    III. Filtro sanitario
</p>
<p>
    Actualmente, la “Universidad de la Salud” del Estado de Puebla, cuenta con un filtro sanitario <strong>obligatorio </strong>
    para permitir el acceso a sus instalaciones, el cual consiste en la aplicación de un cuestionario, toma de 
    temperatura y sanitización. Deberá contestar el referido ingresando su correo electrónico institucional en la 
    siguiente liga: https://usep.digital/FiltroSanitario o bien, ingresando al portal de USEP Digital, apartado de 
    “Enlaces rápidos”. 
</p>

<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    IV.	Contactos de atención institucional
</p>

<p>
    <table width="100%" style="margin: 0px 20px;">
        <tr>
            <th width="33%" style="font-size: 0.8rem;">Oficina</th>
            <th width="33%" style="font-size: 0.8rem;">Titular</th>
            <th width="34%" style="font-size: 0.8rem;">Correo electrónico institucional</th>
        </tr>
        <tr>
            <td style="font-size: 0.8rem;">Secretaria académica</td>
            <td style="font-size: 0.8rem;">C. Yaneth Martínez Tovilla</td>
            <td style="font-size: 0.8rem;">yaneth.martinez@usalud.edu.mx</td>
        </td>
        <?php if($alumno->carrera=='1'): ?>
            <tr>
                <td style="font-size: 0.8rem;">Director de Licenciatura</td>
                <td style="font-size: 0.8rem;">C. Maribel Pérez Marín</td>
                <td style="font-size: 0.8rem;">maribel.perez@usalud.edu.mx</td>
            </td>
            <tr>
                <td style="font-size: 0.8rem;">Subdirección académica </td>
                <td style="font-size: 0.8rem;">C. José Luis Mendoza García</td>
                <td style="font-size: 0.8rem;">luis.mendoza@usalud.edu.mx</td>
            </td>
            <tr>
                <td style="font-size: 0.8rem;">Subdirección de acompañamiento y seguimiento</td>
                <td style="font-size: 0.8rem;">C. Lilia Leticia Ortega Cruz</td>
                <td style="font-size: 0.8rem;">lilia.ortega@usalud.edu.mx</td>
            </td>
        <?php else: ?>
            <tr>
                <td style="font-size: 0.8rem;">Director de Licenciatura</td>
                <td style="font-size: 0.8rem;">C. Héctor Alfonso López Santos</td>
                <td style="font-size: 0.8rem;">hector.lopez@usalud.edu.mx</td>
            </td>
            <tr>
                <td style="font-size: 0.8rem;">Subdirección académica </td>
                <td style="font-size: 0.8rem;">C. Flavia Marisol Aguilar Rivera</td>
                <td style="font-size: 0.8rem;">flavia.aguilar@usalud.edu.mx</td>
            </td>
            <tr>
                <td style="font-size: 0.8rem;">Subdirección de acompañamiento y seguimiento</td>
                <td style="font-size: 0.8rem;">C. María Goretti Cordero Álvarez</td>
                <td style="font-size: 0.8rem;">goretti.cordero@usalud.edu.mx</td>
            </td>
        <?php endif; ?>
        <tr>
            <td style="font-size: 0.8rem;">Dirección de Servicios Escolares y Titulación</td>
            <td style="font-size: 0.8rem;">C. Paola Ninel Agustín García</td>
            <td style="font-size: 0.8rem;">ninel.agustin@usalud.edu.mx</td>
        </td>
        <tr>
            <td style="font-size: 0.8rem;">Subdirección de Servicios Bibliotecarios</td>
            <td style="font-size: 0.8rem;">C. Alejandra Cueto Lira</td>
            <td style="font-size: 0.8rem;">alejandra.cueto@usalud.edu.mx</td>
        </td>
    </table>
</p>

<p style="text-align: center; font-weight: bold; text-align: center; margin-bottom: 15px;">
    &iexcl;Somos USEP!
</p>
