<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    .verificacion {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        align-items: center;
    }

    .datos {
        display: flex;
        justify-content: space-around;
    }

    .row1 {
        width: 7%;
    }

    .row2 {
        width: 20%;
    }

    .row3 {
        width: 7%;
    }

    .row4 {
        width: 40%;
    }

    .row5 {
        width: 10%;
        background-color: #ffcccc;
    }

    .formularios-botones,
    .iconos-grupo {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }
    .formulario-cabecera, .formulario-datos{
        display: flex;
        justify-content: space-around;
        padding-bottom: 10px;
    }
</style>
<br>
<?php if (isset($error_etapa)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $error_etapa; ?>
    </div>
<?php endif; ?>
<?php if (isset($exito_etapa)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $exito_etapa; ?>
    </div>
<?php endif; ?>
<?php if (isset($error_formulario)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $error_formulario; ?>
    </div>
<?php endif; ?>
<?php if (isset($exito_formulario)) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $exito_formulario; ?>
    </div>
<?php endif; ?>
<?php if (isset($etapas)) : ?>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Etapas de Registro</h3>
            </div>
            <?php if (!isset($etapasexistentes)) : ?>
                <div class="card-body">
                    <div class="text-center">
                        <h4>No hay Etapas Registradas</h4>
                        <form action="agregaretapa" method="post">
                            <div>
                                <br>
                                <button class="btn btn-secondary" type="submit">Agregar nueva etapa</button>
                            </div>
                            <td class="text-center"></td>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (isset($etapasexistentes)) : ?>
                <div class="card-body">
                    <table class="table table-hover  text-center">
                        <thead class="thead-default">
                            <tr>
                                <th>Periodo Etapa</th>
                                <th>Nombre de Etapa</th>
                                <th>Etapa Orden</th>
                                <th>Controles de Formularios </th>
                                <th>Controles de Etapas</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($etapasexistentes as $etapa_actuales) : ?>
                                <tr>
                                    <td class="row1"><?php echo $etapa_actuales->etapa_periodo ?></td>
                                    <td class="row2"><?php echo $etapa_actuales->etapa_nombre ?></td>
                                    <td class="row3"><?php echo $etapa_actuales->etapa_orden ?></td>
                                    <td class="row4">
                                        <?php if ($etapa_actuales->etapa_formulario == 0) : ?>
                                            <?php echo 'No tiene Formulario vinculado'; ?>
                                        <?php else : ?>
                                            <?php if (isset($formularios)) : ?>
                                                <?php foreach ($formularios as $formetapa) : ?>
                                                    <?php if ($formetapa->formulario_etapa ==  $etapa_actuales->etapa_id) : ?>
                                                        <div class="formularios-botones">
                                                            <div>
                                                                <?php echo $formetapa->formulario_nombre ?>
                                                            </div>
                                                            <div class="iconos-grupo">

                                                                <div>
                                                                    <form action="detalleformulario" method="post">
                                                                        <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                        <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                                            <i class="material-icons">visibility</i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                <div >

                                                                    <form action="editarformulario" method="post">
                                                                        <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                        <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                                            <i class="material-icons">mode_edit</i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                <div style="background-color: red;">

                                                                    <form action="eliminaformulario" method="post">
                                                                        <input type="hidden" name="formulario-identificador" value="<?php echo $formetapa->formulario_id ?>">
                                                                        <button type="button" class="btn btn-default btn-sm" style="font-size:24px">
                                                                            <i class="material-icons">delete_forever</i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <form action="agregarFormulario" method="post">
                                                <button class="btn btn-secondary" type="submit">Agregar nuevo Formulario</button>
                                                <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center row5">
                                        <div class="iconos-grupo">
                                            <div>
                                                <form action="modificaretapa" method="post">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                        <i class="material-icons">mode_edit</i>
                                                    </button>
                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                </form>
                                            </div>
                                            <div style="background-color: red;">
                                                <form action="eliminaretapa" method="post">
                                                    <button type="submit" class="btn btn-default btn-sm" style="font-size:24px">
                                                        <i class="material-icons">delete_forever</i>
                                                    </button>
                                                    <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_actuales->etapa_id ?>">
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <form action="agregaretapa" method="post">
                                    <td class="text-center" colspan="5"><button class="btn btn-secondary" type="submit">Agregar nueva etapa</button></td>
                                </form>
                            </tr>

                        </tbody>
                    </table>

                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (isset($configuradoretapa)) : ?>
    <div class="container">
        <?php if (isset($bandera_updateetapa)) : ?>
            <form action="modificaretapaexistente" method="post">
                <input type="hidden" name="etapa-identificador" value="<?php echo $identificacdor ?>">
            <?php else : ?>
                <form action="generaretapa" method="post">
                <?php endif; ?>
                <div class="card">
                    <div class="card-header">
                        <h4>Configurardor de Etapas</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <div>
                                <label for="select-periodo" class="form-label">Selecciona el Periodo de la Etapa</label>
                                <select class="form-select" name="select-periodo" id="select-periodo" required>
                                    <?php if (!isset($periodo_selected)) : ?>
                                        <option selected value="0">Selecciona un Periodo</option>
                                    <?php else : ?>
                                        <option value="0">Selecciona un Periodo</option>
                                    <?php endif;  ?>
                                    <?php foreach ($periodos as $periodo) : ?>
                                        <?php if (isset($periodo_selected)) : ?>

                                            <?php if ($periodo->periodo_id == $periodo_selected) : ?>
                                                <option selected value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                            <?php else :  ?>
                                                <option value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                            <?php endif;  ?>

                                        <?php else : ?>
                                            <option value="<?php echo $periodo->periodo_id ?>"><?php echo $periodo->periodo_id ?></option>
                                        <?php endif;  ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <br>
                            <div>

                                <label for="nombre-etapa" class="form-label">Nombre de la Etapa</label>
                                <?php if (isset($nombre_selected)) : ?>
                                    <input type="text" class="form-control" name="nombre-etapa" id="nombre-etapa" aria-describedby="helpId" required value="<?php echo $nombre_selected ?>">
                                <?php else : ?>
                                    <input type="text" class="form-control" name="nombre-etapa" id="nombre-etapa" aria-describedby="helpId" required>
                                <?php endif; ?>
                                <small id="helpId" class="form-text text-muted">Ingrese el nombre que se le asignara a la Etapa</small>
                            </div>
                            <br>
                            <div>
                                <label for="orden-etapa" class="form-label">Numero de Secuencia</label> <br>
                                <?php if (isset($secuencia_selected)) : ?>
                                    <input class="form-control" type="number" name="orden-etapa" id="orden-etapa" required value="<?php echo $secuencia_selected ?>">
                                <?php else : ?>
                                    <input class="form-control" type="number" name="orden-etapa" id="orden-etapa" required>
                                <?php endif; ?>
                                <small id="helpId" class="form-text text-muted">Elegir el numero de secuencia </small>
                            </div>
                            <br>
                            <div class="verificacion">

                                <div class="form-check form-switch">
                                    <?php if (isset($vinculado_selected)) : ?>
                                        <input class="form-check-input" type="checkbox" id="form-vinculado" name="form-vinculado" value="true" checked>
                                    <?php else : ?>
                                        <input class="form-check-input" type="checkbox" id="form-vinculado" name="form-vinculado" value="true">
                                    <?php endif; ?>
                                    <label class="form-check-label" for="form-vinculado">Esta etapa tiene algun formulario vinculado?</label>
                                </div>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="responsabilidad-rol" name="responsabilidad-rol" value="true">
                                    <label class="form-check-label" for="responsabilidad-rol">Esta etapa es responsable del aspirante?</label>
                                </div>
                            </div>
                            <br>

                        </div>

                    </div>
                    <div class="card-footer text-muted text-center">
                        <?php if (isset($bandera_updateetapa)) : ?>
                            <button type="submit" class="btn btn-secondary btn-lg">Modificar Etapa</button>
                        <?php else : ?>
                            <button type="submit" class="btn btn-secondary btn-lg">Agregar Etapa</button>
                        <?php endif; ?>

                    </div>
                </div>
                </form>
    </div>
<?php endif; ?>

<?php if (isset($configuracionformulario)) : ?>
    <?php if (isset($etapa_detalles)) : ?>

        <div class="container">
            <form action="configurarformulario" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Configurador de Formulario</h3>
                    </div>
                    <div class="card-body">
                        <div class="datos text-center">
                            <label>
                                Periodo:
                                <h5 class="card-title"><?php echo $etapa_periodo ?></h5>
                            </label>
                            <label>
                                Nombre:
                                <h5 class="card-title"><?php echo $etapa_nombre ?></h5>
                            </label>
                            <label>
                                Numero de Secuiencia:
                                <h5 class="card-title"><?php echo $etapa_orden ?></h5>
                            </label>
                            <input type="hidden" name="etapa-identificador" value="<?php echo $etapa_detalles ?>">
                        </div>
                        <br>
                        <div>
                            <div class="mb-3">
                                <div>
                                    <label for="nombre-formulario" class="form-label">Nombre del Formulario:</label>
                                    <input type="text" class="form-control" name="nombre-formulario" id="nombre-formulario" aria-describedby="helpId" placeholder="ej.Datos iniciales" required>
                                    <small id="helpId" class="form-text text-muted">Ingrese el nombre que se le asignara al formulario</small>
                                </div>
                                <br>
                                <div>
                                    <label for="titulo-formulario" class="form-label">Titulo del Formulario:</label>
                                    <input type="text" class="form-control" name="titulo-formulario" id="titulo-formulario" aria-describedby="helpId" required>
                                    <small id="helpId" class="form-text text-muted">Ingrese el Titulo del Formulario que se le mostrara al aspirante</small>
                                </div>
                                <br>
                                <div>
                                    <label for="orden-formulario" class="form-label">Numero de Secuencia:</label> <br>
                                    <input class="form-control" type="number" name="orden-formulario" id="orden-formulario" required>
                                    <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                                </div>
                                <br>
                                <div>
                                    <label for="comment">Instrucciones del Formulario:</label>
                                    <textarea class="form-control" rows="5" id="comment" name="help-text" required></textarea>
                                    <small id="helpId" class="form-text text-muted">Ingresa a una breve indicación de lo que deberán realizar en este formulario</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <button type="submit" class="btn btn-secondary">Crear Formulario</button>
                    </div>
                </div>
            </form>
        </div>
    <?php else : ?>
        <h3>
            No se encontro la etapa
        </h3>
    <?php endif; ?>
<?php endif; ?>



<?php if (isset($detallesformulario)) : ?>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Detalles
            </div>
            <div class="card-body">
                <?php foreach ($formularioinformacion as $datos) : ?>
                    <div class="info-formulario-main">
                        <div class="formulario-cabecera text-center">
                            <label>Etapa:<h3 class="card-title"> <?php echo $datos->etapa_nombre  ?></h3> </label>
                            <label>Periodo de Etapa:<h3 class="card-title"> <?php echo $datos->etapa_periodo  ?></h3> </label>
                            <label>Formulario:<h3 class="card-title"> <?php echo $datos->formulario_nombre  ?></h3> </label>
                        </div>
                        <div class="formulario-datos text-center">
                            <label>Titulo del Formulario:<h4 class="card-title"> <?php echo $datos->formulario_titulo  ?></h4> </label>
                            <label>Secuencia del formulario:<h4 class="card-title"> <?php echo $datos->formulario_orden  ?></h4> </label>
                        </div>
                        <div class="formulario-datos text-center"> 
                            <label>Instrucciones del formulario:<h4 class="card-title"> <?php echo $datos->formulario_instrucciones  ?></h4> </label>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="card-footer text-muted text-center">
                <form action="DiseñadordeModelo" method="post">
                    <button type="submit" class="btn btn-secondary">Regresar</button>
                </form>
            </div>
        </div>
    </div>

<?php endif; ?>


<?php if (isset($creacionformulario)) : ?>
    <form action="agregarFormulario" method="post">
        <div class="card">
            <div class="card-header">
                <h3>
                    Generador de Formularios
                </h3>
            </div>
            <div class="card-body text-center">
                <?php if (isset($formularioscreados)) : ?>
                    <table class="table table-hover  ">
                        <thead class="thead-default">
                            <tr>
                                <th>Nombre de Formulario</th>
                                <th>Número de Orden</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($formularioscreados as $formulario) : ?>
                                <tr>
                                    <td></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div class="card-footer text-muted text-center">
                <button type="submit" class="btn btn-secondary btn-lg">Agregar Formulario</button>
            </div>
        </div>
    </form>
<?php endif; ?>

<?php if (isset($agregaritem)) : ?>
    <div class="container">
        <form action="agregaritem" method="post">

            <div class="card">
                <div class="card-header">
                    Generador de Modulos
                </div>
                <div class="card-body">
                    <h4 class="card-title">Selecciona que item agregar</h4>
                    <div>
                        <div class="mb-3">
                            <div>

                                <label for="etiqueta-nombre" class="form-label">Nombre de la Etiqueta</label>
                                <input type="text" class="form-control" name="etiqueta-nombre" id="etiqueta-nombre" aria-describedby="helpId" placeholder="ej.Nombre" required>
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>
                            <br>
                            <div>

                                <label for="selector-tipo" class="form-label">Tipo de dato</label>
                                <select class="form-select" name="selector-tipo" id="selector-tipo" required>
                                    <option selected>Selecciona una opccion</option>
                                    <option value="text">text</option>
                                    <option value="password">password</option>
                                    <option value="email">email</option>
                                    <option value="number">number</option>
                                    <option value="date">date</option>
                                    <option value="datetime">datetime</option>
                                    <option value="datetime-local">datetime-local</option>
                                    <option value="month">month</option>
                                    <option value="tel">telefono</option>
                                    <option value="url">url</option>
                                </select>
                                <small id="helpId" class="form-text text-muted">Ingresa el título que tendrá la etiqueta como una referencia de lo que tendrá que rellenar el usuario </small>
                            </div>
                            <br>
                            <div>
                                <div class="mb-3">
                                    <label for="help-text" class="form-label">Etiqueta de ayuda</label>
                                    <input type="text" class="form-control" name="help-text" id="help-text" aria-describedby="helpId" placeholder="" require>
                                    <small id="helpId" class="form-text text-muted">Ingresa a una breve indicación de lo que deberán realizar en ese campo</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <button type="button">Agregar

                    </button>
                </div>
            </div>
        </form>
    </div>
<?php endif; ?>


<?php echo $this->endSection() ?>