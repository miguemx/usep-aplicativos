<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>
    
<div class="container-fluid">
    <form class="row g-3" method="post">
        
        <!-- <div class="col-12">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div> -->
        
    </form>
</div>
<div class="container-fluid" style="margin-top: 15px;">

    <?php if ( isset($error) ): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <form method="get" action="<?php echo base_url('CapitalHumano/Docentes') ?>">
        
        <div class="row" style="padding-top: 10px;">
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-2 col-form-label">ID</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm" id="id" name="id" value="<?php echo $id; ?>" /> 
                </div>
            </div>
            
            <div class="mb-3 col-sm-4" style="text-align: right;"> 
                <a href="<?php echo base_url('CapitalHumano/Docentes') ?>" class="btn btn-secondary btn-sm">Ver todos</a>
                <button class="btn btn-secondary btn-sm" type="submit">Buscar</button>
            </div>

        </div>
        <div class="row" >
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Apellido Paterno</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="apPaterno" value="<?php echo $apPaterno; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Apellido Materno</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="apMaterno" value="<?php echo $apMaterno; ?>" />
                </div>
            </div>
            <div class="mb-3 row col-sm-4">
                <label for="matricula" class="col-sm-4 col-form-label">Nombre</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="matricula" name="nombre" value="<?php echo $nombre; ?>" />
                </div>
            </div>
        </div>
        
    </form>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Nombre</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($empleados as $empleado): ?>
                    <tr>
                        <td><?php echo $empleado->id; ?></td>
                        <td><?php echo $empleado->apPaterno; ?></td>
                        <td><?php echo $empleado->apMaterno; ?></td>
                        <td><?php echo $empleado->nombre; ?></td>
                        <td>
                            <form  action="<?php echo base_url('CapitalHumano/AccesoDocente'); ?>" method="post" onsubmit="return confirma()">
                                <input name="id" type="hidden" value="<?php echo $empleado->id; ?>" />
                                <button type="submit" name="bd" value="entra" class="btn btn-secondary btn-sm">
                                    Acceder
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div>
            <?php echo $pager->links(); ?>
        </div>
    </div>
</div>

<script>
    function confirma() {
        return confirm( 'Al acceder como este docente, verá su perfil y perderá los accesos actuales. ¿Está seguro de continuar?' );
    }
</script>

<style type="text/css">
    .pagination {
        margin: 5px;
        margin-bottom: 20px;
    }

    .pagination li a {
        padding: 5px 15px;
        border: 1px solid #575757;
    }

    .pagination .active {
        font-weight: bold;
        background: #dedede;
    }
</style>

<?php echo $this->endSection() ?>
