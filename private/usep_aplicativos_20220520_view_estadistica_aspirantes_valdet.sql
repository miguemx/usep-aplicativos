drop view if exists view_estadistica_aspirantes_valdet;

create view view_estadistica_aspirantes_valdet as (
select
	if(aspirante_carrera=1 and aspirante_sexo="H",count(aspirante_id),0) AS h_enf,
    if(aspirante_carrera=1 and aspirante_sexo="M",count(aspirante_id),0) AS m_enf,
    if(aspirante_carrera=2 and aspirante_sexo="H",count(aspirante_id),0) AS h_med,
    if(aspirante_carrera=2 and aspirante_sexo="M",count(aspirante_id),0) AS m_med,
    
    aspirante_validado,
    aspirante_carrera,
    aspirante_sexo
  from
  	aspirantes_generales
  where
    deleted_at IS NULL
  group by
  	aspirante_validado,aspirante_carrera,aspirante_sexo);