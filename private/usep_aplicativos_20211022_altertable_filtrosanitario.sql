alter table filtro_sanitario add fecha_filtro timestamp;

ALTER TABLE `filtro_sanitario` CHANGE `fecha_filtro` `filtro_fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `filtro_usuarios_ext` ADD `externo_categoria` VARCHAR(60) NULL AFTER `externo_sexo`;