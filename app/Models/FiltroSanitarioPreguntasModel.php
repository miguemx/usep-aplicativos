<?php
namespace App\Models;
use CodeIgniter\Model;

use \App\Entities\Alumno;

class FiltroSanitarioPreguntasModel extends Model {
    protected $table      = 'filtro_preguntas';
    protected $primaryKey = 'filtropreg_id';

    protected $returnType    = 'App\Entities\FiltroPregunta';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * devuelve todas las preguntas ordenadas por síntomas mayores y menores
     * @return preguntas las preguntas ordenadas
     */
    public function orderBySintomas() {
        $this->orderBy( 'filtropreg_mayor', 'ASC' );
        $this->orderBy( 'filtropreg_id', 'ASC' );
        $preguntas = $this->findAll();
        return $preguntas;
    }

}