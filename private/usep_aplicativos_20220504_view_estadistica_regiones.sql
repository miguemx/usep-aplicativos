create view view_estadistica_regiones as(
    SELECT
        COUNT(DISTINCT `a`.`aspirante_id`) AS `n_alumnos`,
        `b`.`region_nombre` AS `region_nombre`,
        `a`.`aspirante_carrera` AS `aspirante_carrera`,
        `a`.`aspirante_sexo` AS `aspirante_sexo`
    FROM
        (
            `usep_aplicativos`.`aspirantes_generales` `a`
        JOIN `usep_aplicativos`.`municipios_regiones` `b`
        )
    WHERE
        `a`.`aspirante_estado` = 21 AND `a`.`aspirante_municipio` = `b`.`municipio_id` AND `a`.`aspirante_validado` = '1'
    GROUP BY
        `b`.`region_nombre`,
        `a`.`aspirante_carrera`,
        `a`.`aspirante_sexo`
);