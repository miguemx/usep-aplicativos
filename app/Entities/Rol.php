<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Rol extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'        => null,
        'nombre'    => null,
        'menu'      => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'        => 'rol_id',
        'nombre'    => 'rol_nombre',
        'menu'      => 'rol_menu',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
