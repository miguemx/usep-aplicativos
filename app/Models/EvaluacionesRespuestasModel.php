<?php

namespace App\Models;

use CodeIgniter\Model;

class EvaluacionesRespuestasModel extends Model
{
    protected $table = 'evaluacion_respuestas';
    protected $primaryKey = 'respuestas_id';
    protected $returnType = 'App\Entities\EvaluacionesRespuestas';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['respuestas_id', 'respuestas_matricula', 'respuestas_grupo','respuestas_pregunta','respuestas_valor','respuestas_comentario', 'created_at', 'updated_at', 'deleted_at'];
    protected $db;
    protected $builder;

    protected $useTimestamps = true;


    /**
     * retorna el arreglo de respuestas de un alumno de una evaluacion 
     * @param preguntas array de id de preguntas
     * @param matricula ID del alumno
     * @param grupo ID del grupo 
     */
    public function getArrayRespuestasPreguntas($preguntas,$matricula,$grupo)
    {
        $respuestas =  $this->whereIn('respuestas_pregunta',$preguntas)->where('respuestas_matricula',$matricula)->where('respuestas_grupo',$grupo)->findAll();
        // return $respuestas;
        return $respuestas;
    }


    /**
     * indica si un pregunta ha sido contestado por un aspirante, devolviendo el registro de la pregunta contestada, o null si no existe
     * @param pregunta el ID de la pregunta
     * @param estudiante el ID del estudiante
     * @param grupo IDC del grupo correspondiente a la respuesta
     * @return EvaluacionRespuestas un objeto con la respuesta realizada
     * @return null en caso de que no se haya respondido
     */
    public function contestado($pregunta, $estudiante, $grupo) {
        $this->where( 'respuestas_pregunta', $pregunta );
        $this->where( 'respuestas_grupo', $grupo );
        $this->where( 'respuestas_matricula', $estudiante );
        $respuestas = $this->find(); 
        if ( count($respuestas) ) {
            return $respuestas[0];
        } 
        return null;
    }

    /**
     * Retorna un conteo de alumnos que contestaron al menos una pregunta de la evaluacion de un grupo
     * @param grupo IDC del grupo
     * @param preguntas array de preguntas pertenecientes a una evaluacion especifica
     * @return alumnos alumnos que pertenecen al grupo
     */
    public function getAlumnosGrupo($grupo,$preguntas)
    {
        $alumnos = $this
        // ->distinct('respuestas_matricula')
        ->whereIn('respuestas_pregunta',$preguntas)
        ->where('respuestas_grupo',$grupo)
        ->groupBy('respuestas_matricula')
        ->findAll()
        ;
        return $alumnos;
    }
    
    /**
     * Retorna un conteo de alumnos que contestaron al menos una pregunta de la evaluacion de un grupo
     * @param evaluación evaluación a revisar
     * @param grupos array grupos de la evaluacion
     * @param grupos array grupos de la evaluacion
     */

    public function getGruposEvaluacion ($preguntas){
        $grupos=array();
        $gruposconsulta = $this->distinct(true)->select ('respuestas_grupo')
                         ->whereIn  ('respuestas_pregunta',$preguntas)
                         ->orderBy ('respuestas_grupo')
                         ->findAll();
        foreach($gruposconsulta as $gruposarray){
            $grupos[]=$gruposarray->respuestas_grupo;
        }
        return $grupos;
    }

    /**
     * Retorna el conteo de alumnos que tiene cada grupo (idc)
     * @param grupo grupo a consultar
     * @return consulta conteo del grupo revisado
     */
    public function getNumAlumnosxGrupo($grupo,$preguntas){
        $consulta = $this
                         ->select('COUNT(distinct respuestas_matricula) as n_alumnos')
                         ->where  ('respuestas_grupo',$grupo)
                         ->whereIn('respuestas_pregunta',$preguntas)
                         ->first();
        return $consulta;
    }
}
