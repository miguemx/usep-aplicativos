<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container ">
    <div class="text-center mt-5">
        <h2>Bibliotecas</h2>
        <?php if( isset($error) ): ?>
            <?php echo $error; ?>
        <?php else: ?>
            <h5>
                Presenta este código para registrar tu acceso a la Biblioteca
            </h5>
            <div class="mt-5 mb-5">
                <img src="<?php echo $qr; ?>" />
            </div>
            <p class="mt-5">
                <a href="/">Volver al inicio</a>
            </p>
        <?php endif; ?>
    </div>
</div>

<?php echo $this->endSection() ?>