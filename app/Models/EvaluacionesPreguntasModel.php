<?php

namespace App\Models;

use CodeIgniter\Model;

class EvaluacionesPreguntasModel extends Model
{
    protected $table = 'evaluacion_preguntas';
    protected $primaryKey = 'preguntas_id';
    protected $returnType = 'App\Entities\EvaluacionesPreguntas';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['preguntas_id', 'preguntas_formulario', 'preguntas_titulo','preguntas_tipo','preguntas_orden','preguntas_obligatorio','preguntas_clasificacion', 'created_at', 'updated_at', 'deleted_at'];
    protected $db;
    protected $builder;

    protected $useTimestamps = true;


    /**
     * retorna las preguntas pertenecientes a un formulario
     * @param formulario ID del formulario
     */
    public function getByFormulario($formulario)
    {
        return $this->where('preguntas_formulario',$formulario)->find();
    }
}
