DROP VIEW IF EXISTS view_usuarios_generales;

CREATE VIEW view_usuarios_generales AS

SELECT 
    usuario_correo as correo,
    usuario_rol as rolid,
    usuario_ultcnx as ultconexion,

    empleado_id as id,
    empleado_correo as mail,
    empleado_nombre as nombre,
    empleado_ap_paterno as apPaterno,
    empleado_ap_materno as apMaterno,
    empleado_edad as edad,
    empleado_sexo as sexo,
    empleado_flg_captura as foto,
    empleado_docente as carrera
FROM 
    aplicativo_usuarios
    INNER JOIN aplicativo_empleados ON empleado_correo=usuario_correo
WHERE aplicativo_empleados.deleted_at IS NULL

UNION

SELECT 
    usuario_correo as correo,
    usuario_rol as rolid,
    usuario_ultcnx as ultconexion,

    alumno_id as id,
    alumno_correo as mail,
    alumno_nombres as nombre,
    alumno_ap_paterno as apPaterno,
    alumno_ap_materno as apMaterno,
    alumno_curp as edad,
    alumno_sexo as sexo,
    alumno_foto as foto,
    carrera_nombre as carrera

FROM 
    aplicativo_usuarios
    INNER JOIN alumnos_generales ON alumno_correo=usuario_correo
    INNER JOIN prein_carreras ON alumno_carrera = carrera_id
WHERE alumnos_generales.deleted_at IS NULL
;