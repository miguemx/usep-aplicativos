<?php echo $this->extend('plantillamenus'); ?>
<?php echo $this->section('workarea') ?>


<br>
<div class="container contenedor-cuestionario" style="background-color:#dcdcdc ;">
	<div class="card card-qr">
		<div class="card-header d-flex justify-content-center" style="background-color:#841138 ; color:antiquewhite;">
			Validación de Código de Acceso para Filtro Sanitario
		</div>
		<br>
		<div class="card-body">
            <p id="lcont" class="center mb-6">Esta información es de carácter informativo. <br> No se usa para evaluar el riesgo de la persona de ser portadora de COVID-19</p>
            <hr />
            <?php if ( !is_null($folio) ): ?>
				<?php
				$sospechoso = false;
				$mayores = 0;
				$menores = 0;
				foreach ( $preguntas as $pregunta) {
					if ( $pregunta->mayor == '1' ) {
						if ( $respuestas[ $pregunta->id ] == 'SI' ) {
							$mayores++;
						}
					}
					else {
						if ( $respuestas[ $pregunta->id ] == 'SI' ) {
							$menores++;
						}
					}
                }
				
				if ( $mayores >= 1 && $menores >= 1) {
					$sospechoso = true;
				}
				?>

				<div class="mb-5" style="text-align: center;">
					<?php if($sospechoso==false): ?>
						<img src="<?php echo base_url('img/icons/correct.png') ?>" alt="OK" height="100" /><br />
						Acceso correcto.
					<?php else: ?>
						<img src="<?php echo base_url('img/icons/wrong.png') ?>" alt="DENEGADO" height="100" /><br />
						Acceso negado. Por favor acuda con el médico del filtro sanitario
					<?php endif; ?>
				</div>
                <form id="contenedor2" method="POST" action="<?php echo base_url('Filtro/Accede'); ?>" class="needs-validation"  >
					<input type="hidden" value="<?php echo $folio->id; ?>" name="folio" />
					<div class="row ml-3 mr-3 mb-2">
						<div class="col">
							<label for="exampleFormControlInput1" class="form-label">Temperatura</label>
							<input type="number" class="form-control" name="temperatura" id="temperatura" step=".01" />
						</div>
						<div class="col">
							<label for="exampleFormControlInput1" class="form-label">Oximetría</label>
							<input type="number" class="form-control" name="oximetria" step=".01" />
						</div>
					</div>
					<div class="row ml-3 mr-3 mb-5">
						<div>
							<button type="submit" class="btn btn-sm btn-secondary">
								Guardar
							</button>
						</div>
					</div>
					
					<?php 
						$sintomas = -10;
						?>
                    <?php foreach ( $preguntas as $pregunta): ?>

						<?php
							$cambio = false;
							$letrero = '';
							if ( $pregunta->mayor != $sintomas ) {
								$sintomas = $pregunta->mayor;
								if ( $sintomas == 1 ) {
									$letrero = 'Síntomas mayores';
									$cambio = true;
								}
								else if ( $sintomas == 2 ) {
									$letrero = 'Síntomas menores';
									$cambio = true;
								}
							}
						?>

						<?php if ( $cambio ): ?>
							<h5><?php echo $letrero; ?></h5>
							<hr />
						<?php endif; ?>

                        <div class="mb-5 ml-3 mr-3">
                            <div>
                                <label for="exampleFormControlInput1" class="form-label"><?php echo $pregunta->pregunta; ?></label>
                                <input type="text" class="form-control" value="<?php echo $respuestas[ $pregunta->id ]; ?>" disabled="disabled" />
                            </div>
                        </div>
                    <?php endforeach; ?>
                </form>
            <?php else: ?>
                <div style="text-align: center;">
					<img src="<?php echo base_url('img/icons/wrong.png') ?>" alt="DENEGADO" height="100" /><br />
                    El código QR presentado no se ha encontrado en el sistema o bien ha expirado.
                </div>
            <?php endif; ?>
		</div>
	</div>
</div>

<script>
	let temp = document.getElementById("temperatura");
	temp.focus();
</script>


<?php echo $this->endSection() ?>