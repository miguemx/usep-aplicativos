<?php echo $this->extend('baselayout'); ?>

<?php echo $this->section('content') ?>

<style>

.input-grupo.input-grupo-outline .formulario-control {
    background: none;
    border: 1px solid red;
    border-radius: 0.375rem;
    border-top-left-radius: 0.375rem !important;
    border-bottom-left-radius: 0.375rem !important;
    padding: 0.625rem 0.75rem
rem
 !important;
    line-height: 1.3 !important;
}

.div-select{

}

.seleccion{
    border: 1px red solid !important;
}

.clase1{
    display: none;
}

.input-error {
        border: 3px red solid !important;
        border-radius: 10px;
        border-top: 0px !important;
        border-left: 0px !important;
    }

</style>
<h4>Bienvenido(a)</h4>
<div>
    <p class="negro" style="color: #000;">
        Completa tu registro como aspirante llenando los siguientes datos:
    <p>
<hr />
<div class="container"> 
    <?php if(isset($msg)): ?>
        <div class="alert alert-secondary">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <form role="form" method="post" name ="formulario" id="formulario" action="<?php echo base_url('Admision/regAspiranteComp') ?>">

    <div class="card mb-2">
                    <div class="card-body">
        <?php if (($carrera=='2') and ($revalida_flg=='1')){ ?>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">¿Ingreso por revalidación de materias?</label>
            <input type="text" class="form-control" value="<?php echo $revalidacion; ?>" disabled="disabled" />
        </div>
        <?php } ?>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">CURP</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->curp; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Apellido Paterno</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->apPaterno; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Apellido Materno</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->apMaterno; ?>" disabled="disabled" />
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Nombre</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->nombre; ?>" disabled="disabled" />
        </div>


    </div>
    </div>

    <div class="card mb-2">
                    <div class="card-body">



        <p>
            <strong>Dirección:<strong>
        </p>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Calle</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->calle; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Colonia</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->colonia; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Número Exterior</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->numero; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Número Interior</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->num_int; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Teléfono (10 dígitos)</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->telefono; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3 div-select">
            <label class="form-label">País de residencia</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->pais; ?>" disabled="disabled"/>
        </div>

        <div class="input-group input-group-outline mb-3 div-select"  id="selectestado" >
            <label class="form-label">Estado</label>
            <input type="text" class="form-control" value="<?php echo $estado; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3 div-select" name="selectmunicipio" id="selectmunicipio">
            <label class="form-label">Municipio</label>
            <input type="text" class="form-control" value="<?php echo $municipio; ?>" disabled="disabled"/>
        </div> 
        <div class="input-group input-group-outline mb-3" id ="codigopostal">
            <label class="form-label">Código Postal</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->cp; ?>" disabled="disabled"/>
        </div>


    </div>
    </div>


    <div class="card mb-2">
                    <div class="card-body">

        <div>       
            <p>
                <b>Datos de nacimiento:</b>
            </p>
        </div>
        
        <div class="input-group input-group-outline mb-3 div-select">
            <label class="form-label">Sexo</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->sexo; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3">
            <label class="form-label">Fecha de Nacimiento</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->nac_fecha; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3 div-select">
            <label class="form-label">País de nacimiento</label>
            <input type="text" class="form-control" value="<?php echo $aspirante->nac_pais; ?>" disabled="disabled"/>
        </div>
        
        <div class="input-group input-group-outline mb-3"  id="selectestado_nac" >
            <label class="form-label">Estado</label>
            <input type="text" class="form-control" value="<?php echo $nac_estado; ?>" disabled="disabled"/>
        </div>
        <div class="input-group input-group-outline mb-3" id ="selectmunicipio_nac">
            <label class="form-label">Municipio</label>
            <input type="text" class="form-control" value="<?php echo $nac_municipio; ?>" disabled="disabled"/>
        </div> 

    </div>
    </div>


    </form>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<?php echo $this->endSection() ?>