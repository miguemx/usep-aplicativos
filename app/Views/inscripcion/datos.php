<?php echo $this->extend('plantillamenus'); ?>

<?php echo $this->section('workarea') ?>

<div class="container">
    <div class="bg-white rounded shadow p-2 mt-2 mb-4">
        <div class="col-sm-12">
            <h4 class="mb-2">Datos del nuevo estudiante</h4>

            <?php if(!is_null($errores)): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $errores; ?>
                </div>
            <?php endif; ?>
                
            <?php if( isset($aspirante) ): ?>
                <form method="post" action="<?php echo base_url('Inscripcion/Inscribir'); ?>" class="row g3" onsubmit="return confirma()" >
                    <div class="col-sm-6 mb-3" style="text-ailgn: center;">
                        <?php 
                        $foto = '';
                        $filename = '';
                        foreach ( $archivos as $archivo ) {
                            if ( $archivo->tipoNombre == 'FOTO' ) {
                                $foto = $archivo->id;
                                $filename = $archivo->valor;
                            }
                        }
                        ?>
                        <img src="<?php echo base_url('Inscripcion/Doc').'/'.$aspirante->folio.'/'.$foto; ?>" alt="FOTO" style="max-width: 100px;" />
                        <input type="hidden" name="foto" value="<?php echo $filename; ?>" />
                    </div>    
                    <div class="col-sm-3 mb-3" style="text-ailgn: center;">
                        Fecha de inscripción: <strong><?php echo date("d/m/Y", strtotime($aspirante->fechaInscripcion)); ?></strong>
                    </div>
                    <div class="col-sm-3 mb-3" style="text-ailgn: center;">
                        Horario de inscripción: <strong><?php echo $aspirante->horaInscripcion; ?></strong>
                    </div>
                    <div class="col-sm-2 mb-3">
                        <label for="folio" class="form-label">Matrícula</label>
                        <input type="text" class="form-control form-control-sm" name="id" value="<?php echo $aspirante->folio; ?>" readonly="readonly" />
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">CURP</label>
                        <input type="text" class="form-control form-control-sm" name="curp" value="<?php echo $aspirante->curp; ?>" />
                    </div>
                    <div class="col-sm-2 mb-3">
                        <label for="folio" class="form-label">Sexo</label>
                        <select class="form-select form-select-sm" name="sexo">
                            <option value="M" <?php if($aspirante->sexo=='M') echo 'selected="selected"'; ?>>MUJER</option>
                            <option value="H" <?php if($aspirante->sexo=='H') echo 'selected="selected"'; ?>>HOMBRE</option>
                        </select>
                    </div>
                    
                    <div class="col-sm-5 mb-3">
                        <label for="folio" class="form-label">Carrera</label>
                        <?php foreach ( $carreras as $carrera ): ?>
                            <?php if( $carrera->id == $aspirante->carrera ): ?>
                                <input type="text" class="form-control form-control-sm" name="x" value="<?php echo mb_strtoupper($carrera->nombre) ?>" disabled="disabled" />
                                <input type="hidden" name="carrera" value="<?php echo $aspirante->carrera; ?>" />
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Primer Apellido</label>
                        <input type="text" class="form-control form-control-sm" name="apPaterno" value="<?php echo $aspirante->apPaterno; ?>" />
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Segundo Apellido</label>
                        <input type="text" class="form-control form-control-sm" name="apMaterno" value="<?php echo $aspirante->apMaterno; ?>" />
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Nombre(s)</label>
                        <input type="text" class="form-control form-control-sm" name="nombre" value="<?php echo $aspirante->nombre; ?>" />
                    </div>
                    
                    
                    <div>&nbsp;</div>
                    <div class="col-sm-12 mb-3">
                        <label for="folio" class="form-label">Correo personal</label>
                        <input type="text" class="form-control form-control-sm" name="correoPersonal" value="<?php echo mb_strtolower($aspirante->correo); ?>" />
                    </div>
                    <div class="col-sm-6 mb-3">
                        <label for="folio" class="form-label">Calle</label>
                        <input type="text" class="form-control form-control-sm" name="calle" value="<?php echo mb_strtoupper($aspirante->calle); ?>" />
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">Núm.</label>
                        <input type="text" class="form-control form-control-sm" name="numero" value="<?php echo mb_strtoupper($aspirante->numero); ?>" />
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">Int.</label>
                        <input type="text" class="form-control form-control-sm" name="interior" value="<?php echo mb_strtoupper($aspirante->num_int); ?>" />
                    </div>
                    <div class="col-sm-8 mb-3">
                        <label for="folio" class="form-label">Colonia</label>
                        <input type="text" class="form-control form-control-sm" name="colonia" value="<?php echo mb_strtoupper($aspirante->colonia); ?>" />
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">C.P.</label>
                        <input type="text" class="form-control form-control-sm" name="cp" value="<?php echo $aspirante->cp; ?>" />
                    </div>

                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">País</label>
                        <select class="form-select form-select-sm" name="pais">
                            <option value="MÉXICO" <?php if($aspirante->pais=='MÉXICO') echo 'selected="selected"'; ?>>MÉXICO</option>
                            <option value="EXTRANJERO" <?php if($aspirante->pais!='MÉXICO') echo 'selected="selected"'; ?>>EXTRANJERO</option>
                        </select>
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Estado</label>
                        <select class="form-select form-select-sm" name="estado" onchange="cambiaEstado(this,'cmbMunicipios');" id="cmbEstados">
                            <?php foreach ( $estados as $estado ): ?>
                                <option value="<?php echo $estado->id ?>" <?php if($aspirante->estado==$estado->id) echo 'selected="selected"'; ?> class="estado <?php echo $estado->id; ?>">
                                    <?php echo mb_strtoupper($estado->nombre); ?>
                                </option>
                            <?php endforeach; ?>
                            <option value="0" <?php if($aspirante->pais == '0') echo 'selected="selected"'; ?>>EXTRANJERO</option>
                        </select>
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Municipio</label>
                        <select class="form-select form-select-sm" name="municipio" id="cmbMunicipios">
                            
                        </select>
                    </div>
                    
                    <div>&nbsp;</div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">Fecha de nacimiento</label>
                        <input type="date" class="form-control form-control-sm" name="fechaNacimiento" value="<?php echo $aspirante->nac_fecha; ?>" />
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">País de nacimiento</label>
                        <select class="form-select form-select-sm" name="paisNacimiento">
                            <option value="MÉXICO" <?php if($aspirante->nac_pais=='MÉXICO') echo 'selected="selected"'; ?>>MÉXICO</option>
                            <option value="EXTRANJERO" <?php if($aspirante->nac_pais!='MÉXICO') echo 'selected="selected"'; ?>>EXTRANJERO</option>
                        </select>
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">Estado de nacimiento</label>
                        <select class="form-select form-select-sm" name="estadoNacimiento" onchange="cambiaEstado(this,'cmbmunicipioNacimiento');" id="cmbEstados">
                            <?php foreach ( $estados as $estado ): ?>
                                <option value="<?php echo $estado->id ?>" <?php if($aspirante->nac_estado==$estado->id) echo 'selected="selected"'; ?> class="estado <?php echo $estado->id; ?>">
                                    <?php echo mb_strtoupper($estado->nombre); ?>
                                </option>
                            <?php endforeach; ?>
                            <option value="0" <?php if($aspirante->nac_pais == '0') echo 'selected="selected"'; ?>>EXTRANJERO</option>
                        </select>
                    </div>
                    <div class="col-sm-3 mb-3">
                        <label for="folio" class="form-label">Municipio de nacimiento</label>
                        <select class="form-select form-select-sm" name="municipioNacimiento" id="cmbmunicipioNacimiento">
                            
                        </select>
                    </div>

                    <div>&nbsp;</div>
                    <?php foreach( $respuestas as $respuesta ): ?>
                        <?php if( $respuesta->clasificacion != 'ARCHIVO' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo; ?></label>
                                <input type="text" class="form-control form-control-sm" name="<?php echo $respuesta->campo; ?>" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="col-sm-4 mb-3">
                        <label for="folio" class="form-label">Teléfono</label>
                        <input type="text" class="form-control form-control-sm" name="telefono" value="<?php echo $aspirante->telefono;?>" />
                    </div>
                    <?php foreach( $respuestasAll as $respuesta ): ?>                   
                        <?php if( $respuesta->campo == '4' ): ?>
                            <div class="col-sm-2 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="estadocivil">
                                    <option value="Casado(a)" <?php if($respuesta->valor=='Casado(a)') echo 'selected="selected"'; ?>>Casado(a)</option>
                                    <option value="Soltero(a)" <?php if($respuesta->valor=='Soltero(a)') echo 'selected="selected"'; ?>>Soltero(a)</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '7' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="padremadresoltero">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '5' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <input type="text" class="form-control form-control-sm" name="numerohijos" value="<?php echo $respuesta->valor; ?>"/>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '6' ): ?>
                            <div class="col-sm-4 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <input type="text" class="form-control form-control-sm" name="numerodependientes" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '54' ): ?>
                            <div class="col-sm-2 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="trabaja">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '26' ): ?>
                            <div class="col-sm-5 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="beca">
                                    <option value="Manutención" <?php if($respuesta->valor=='Manutención') echo 'selected="selected"'; ?>>Manutención</option>
                                    <option value="Jóvenes escribiendo el futuro" <?php if($respuesta->valor=='Jóvenes escribiendo el futuro') echo 'selected="selected"'; ?>>Jóvenes escribiendo el futuro</option>
                                    <option value="No cuento con beca" <?php if($respuesta->valor=='No cuento con beca') echo 'selected="selected"'; ?>>No cuento con beca</option>
                                    <option value="Otra" <?php if($respuesta->valor=='Otra') echo 'selected="selected"'; ?>>Otra</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '12' ): ?>
                            <div class="col-sm-4 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="comunidadindigena">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '56' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="lenguaindigena">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '27' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="tiposangre">
                                    <option value="O+" <?php if($respuesta->valor=='O+') echo 'selected="selected"'; ?>>O+</option>
                                    <option value="O-" <?php if($respuesta->valor=='O-') echo 'selected="selected"'; ?>>O-</option>
                                    <option value="A+" <?php if($respuesta->valor=='A+') echo 'selected="selected"'; ?>>A+</option>
                                    <option value="A-" <?php if($respuesta->valor=='A-') echo 'selected="selected"'; ?>>A-</option>
                                    <option value="B+" <?php if($respuesta->valor=='B+') echo 'selected="selected"'; ?>>B+</option>
                                    <option value="B-" <?php if($respuesta->valor=='B-') echo 'selected="selected"'; ?>>B-</option>
                                    <option value="AB+" <?php if($respuesta->valor=='AB+') echo 'selected="selected"'; ?>>AB+</option>
                                    <option value="AB-" <?php if($respuesta->valor=='AB-') echo 'selected="selected"'; ?>>AB-</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '28' ): ?>
                            <div class="col-sm-5 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <input type="text" class="form-control form-control-sm" name="padecimiento" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '29' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="discapacidad">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '57' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <input type="text" class="form-control form-control-sm" name="cualdiscapacidad" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '30' ): ?>
                            <div class="col-sm-5 mb-2">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="certificadomedico">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '31' ): ?>
                            <div class="col-sm-2 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="lentes">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '32' ): ?>
                            <div class="col-sm-2 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="protesis">
                                    <option value="Si" <?php if($respuesta->valor=='Si') echo 'selected="selected"'; ?>>Si</option>
                                    <option value="No" <?php if($respuesta->valor=='No') echo 'selected="selected"'; ?>>No</option>
                                </select>
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '33' ): ?>
                            <div class="col-sm-3 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="serviciosalud">
                                    <option value="IMSS" <?php if($respuesta->valor=='IMSS') echo 'selected="selected"'; ?>>IMSS</option>
                                    <option value="ISSSTE" <?php if($respuesta->valor=='ISSSTE') echo 'selected="selected"'; ?>>ISSSTE</option>
                                    <option value="ISSSTEP" <?php if($respuesta->valor=='ISSSTEP') echo 'selected="selected"'; ?>>ISSSTEP</option>
                                    <option value="HU" <?php if($respuesta->valor=='HU') echo 'selected="selected"'; ?>>HU</option>
                                    <option value="No cuento con servicio de salud" <?php if($respuesta->valor=='No cuento con servicio de salud') echo 'selected="selected"'; ?>>No cuento con servicio de salud</option>
                                    <option value="Otro" <?php if($respuesta->valor=='Otro') echo 'selected="selected"'; ?>>Otro</option>
                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '58' ): ?>
                            <div class="col-sm-4 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo;?></label>
                                <select class="form-select form-select-sm" name="vacunacovid">
                                    <option value="Primera Dosis" <?php if($respuesta->valor=='Primera Dosis') echo 'selected="selected"'; ?>>Primera Dosis</option>
                                    <option value="Segunda Dosis" <?php if($respuesta->valor=='Segunda Dosis') echo 'selected="selected"'; ?>>Segunda Dosis</option>
                                    <option value="Tercera Dosis" <?php if($respuesta->valor=='Tercera Dosis') echo 'selected="selected"'; ?>>Tercera Dosis</option>
                                    <option value="Cuarta Dosis" <?php if($respuesta->valor=='Cuarta Dosis') echo 'selected="selected"'; ?>>Cuarta Dosis</option>
                                    <option value="No estoy vacunado(a)" <?php if($respuesta->valor=='No estoy vacunado(a)') echo 'selected="selected"'; ?>>No estoy vacunado(a)</option>
                                </select>
                            </div>
                        <?php endif; ?>

                        <?php if( $respuesta->campo == '43' ): ?>
                            <div class="col-sm-8 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo." de contacto de emergencia";?></label>
                                <input type="text" class="form-control form-control-sm" name="nombrecontacto" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '44' ): ?>
                            <div class="col-sm-4 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo." de contacto de emergencia";?></label>
                                <input type="text" class="form-control form-control-sm" name="telefonocontacto" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '42' ): ?>
                            <div class="col-sm-5 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo." de contacto de emergencia";?></label>
                                <select class="form-select form-select-sm" name="parentescocontacto">
                                    <option value="Madre" <?php if($respuesta->valor=='Madre') echo 'selected="selected"'; ?>>Madre</option>
                                    <option value="Padre" <?php if($respuesta->valor=='Padre') echo 'selected="selected"'; ?>>Padre</option>
                                    <option value="Cónyuge" <?php if($respuesta->valor=='Cónyuge') echo 'selected="selected"'; ?>>Cónyuge</option>
                                    <option value="Hermano(a)" <?php if($respuesta->valor=='Hermano(a)') echo 'selected="selected"'; ?>>Hermano(a)</option>
                                    <option value="Abuelo(a)" <?php if($respuesta->valor=='Abuelo(a)') echo 'selected="selected"'; ?>>Abuelo(a)</option>
                                    <option value="Tío(a)" <?php if($respuesta->valor=='Tío(a)') echo 'selected="selected"'; ?>>Tío(a)</option>
                                    <option value="Primo(a)" <?php if($respuesta->valor=='Primo(a)') echo 'selected="selected"'; ?>>Primo(a)</option>
                                    <option value="Otro" <?php if($respuesta->valor=='Otro') echo 'selected="selected"'; ?>>Otro</option>
                                </select>
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                        <?php if( $respuesta->campo == '45' ): ?>
                            <div class="col-sm-12 mb-3">
                                <label for="folio" class="form-label"><?php echo $respuesta->nombreCampo." de contacto de emergencia";?></label>
                                <input type="text" class="form-control form-control-sm" name="correocontacto" value="<?php echo $respuesta->valor; ?>" />
                            </div>
                            <div>&nbsp;</div>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <div>&nbsp;</div>
                    <div class="d-flex flex-row-reverse">
                        
                        <button class="btn btn-secondary" type="submit">
                            Inscribir
                        </button>
                        <div>&nbsp;</div>
                        <button class="btn btn-danger" type="button" onclick="rechazar()">
                            Rechazar
                        </button>
                    </div>
                    
                </form>

                <form method="post" action="<?php echo base_url('Inscripcion/Rechazar'); ?>" class="row g3" onsubmit="return confirmarechazo()" id="rechazo" style="display: none;">
                    <div class="d-flex mt-3 mb-3" >
                        <label class="flex-fill">Razón del rechazo: </label>
                        <input type="text" name="razon" id="razon" value="" class="form-control flex-fill" />
                        <input type="hidden" name="id" value="<?php echo $aspirante->folio; ?>"  />
                        <input type="hidden" name="pantalla" value="1"  />
                        <button class="btn btn-danger flex-fill" type="submit">
                            Confirmar aspirante rechazado.
                        </button>
                    </div>
                </form>

                <script type="text/javascript">
                    var mpios = {};
                    <?php foreach( $municipios as $municipio ):  ?>
                        mpios[ <?php echo $municipio->estado_id; ?> ]= [];
                    <?php endforeach; ?>
                    <?php foreach( $municipios as $municipio ):  ?>
                        mpios[ <?php echo $municipio->estado_id; ?> ].push( { id:"<?php echo $municipio->id; ?>", nombre:"<?php echo mb_strtoupper($municipio->nombre); ?>"} );
                    <?php endforeach; ?>
                    
                    muestraEstado("<?php echo $aspirante->estado; ?>", "<?php echo $aspirante->municipio; ?>", "cmbMunicipios");
                    muestraEstado("<?php echo $aspirante->nac_estado; ?>", "<?php echo $aspirante->nac_municipio; ?>", "cmbmunicipioNacimiento");
                    
                    function muestraEstado(estadoId, mpioSel, control) {
                        let municipios = mpios[estadoId];
                        document.getElementById(control).innerHTML = '';
                        municipios.map( mpio => {
                            let option = document.createElement('option');
                            option.value = mpio.id;
                            option.text = mpio.nombre;
                            if ( mpioSel == mpio.id ) {
                                option.selected = true;
                            }
                            document.getElementById(control).appendChild( option );
                        } );      
                        let option = document.createElement('option');
                        option.value = "0";
                        option.text = "EXTRANJERO";
                        if ( mpioSel == "0" ) {
                            option.selected = true;
                        }
                        document.getElementById(control).appendChild( option );
                    }
                    function cambiaEstado(estado, control) {
                        muestraEstado(estado.value,null, control);
                    }
                    function rechazar() {
                        document.getElementById('rechazo').style.display = 'block';
                    }
                </script>



            <?php else: ?>
                <div>
                    El aspirante no ha sido sido seleccionado para formar parte de la comunidad de la Universidad de la Salud.
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    function confirma() {
        if ( confirm("¿Confirma que desea inscribir a este aspirante?\nA partir de este momento se convertirá en estudiante y se generará su acceso a las aplicaciones y correo electrónico.") ) {
            return true;
        }
        return false;
    }
    function confirmarechazo() {
        if ( confirm("¿Confirma que desea rechazar este aspirante?\nNo podrá volver a inscribirlo ni lo verá en la lista de aceptados.\n\nEsta acción no se puede deshacer.") ) {
            let razon  = document.getElementById('razon').value;
            if( razon.length > 10 ) {
                return true;
            }
            alert( "Por favor escriba la razón del rechazo" );
        }
        return false;
    }
</script>



<?php echo $this->endSection() ?>