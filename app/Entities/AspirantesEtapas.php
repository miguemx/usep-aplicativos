<?php
namespace App\Entities;

use CodeIgniter\Entity;

class AspirantesEtapas extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
       
        'etapa' => null,
        'periodo' => null,
        'nombre' => null,
        'orden' => null,
        'formulario' => null,
        'responsable' => null,
        'creado_al' => null,
        'actualizado_al' => null,
        'borrado_al' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
 
    protected $datamap = [
   
        'etapa' => 'etapa_id',
        'periodo' => 'etapa_periodo',
        'nombre' => 'etapa_nombre',
        'orden' => 'etapa_orden',
        'formulario' => 'etapa_formulario',
        'responsable' => 'etapa_responsable',
        'creado_al' => 'created_at',
        'actualizado_al' => 'updated_at',
        'borrado_al' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
