<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>

    </style>
</head>

<body style="background-color:crimson; color:darkgray;  display: flex;  justify-content: center;     align-items: center;">
    <div class="container-fluid" style="display: flex;     flex-direction: column;     justify-content: center;     align-items: center; padding-top:20px; display: none;">
        <div class="" style="width: 500px;">
            <div class="card-group">
                <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Bloque 1</h3>
                            <p class="card-text">
                            <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>idc</th>
                                            <th>nombre</th>
                                            <th>hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="101">101</td>
                                            <td>bloque 1 materia 1</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="" value="102">102</td>
                                            <td>bloque 1 materia 2</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="103">103</td>
                                            <td>bloque 1 materia 3</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="104">104</td>
                                            <td>bloque 1 materia 4</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="105">105</td>
                                            <td>bloque 1 materia 5</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="106">106</td>
                                            <td>bloque 1 materia 6</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit">ok</button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Bloque 2</h3>
                            <p class="card-text">
                            <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>idc</th>
                                            <th>nombre</th>
                                            <th>hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="201">201</td>
                                            <td>bloque 2 materia 1</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="202">202</td>
                                            <td>bloque 2 materia 2</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="203">203</td>
                                            <td>bloque 2 materia 3</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="204">204</td>
                                            <td>bloque 2 materia 4</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="205">205</td>
                                            <td>bloque 2 materia 5</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="206">206</td>
                                            <td>bloque 2 materia 6</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit">ok</button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Bloque 4</h3>
                            <p class="card-text">
                            <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>idc</th>
                                            <th>nombre</th>
                                            <th>hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="201">201</td>
                                            <td>bloque 2 materia 1</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="202">202</td>
                                            <td>bloque 2 materia 2</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="203">203</td>
                                            <td>bloque 2 materia 3</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="204">204</td>
                                            <td>bloque 2 materia 4</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="205">205</td>
                                            <td>bloque 2 materia 5</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input type="hidden" name="idgrupo[]" value="206">206</td>
                                            <td>bloque 2 materia 6</td>
                                            <td> 12:00 a 13:00 </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit">ok</button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-group text-center" style="width: 80%; background-color:darkgreen;">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col-xs|sm|md|lg|xl-1-12" style="padding: 20px;">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Bloque 1</h3>
                        <p class="card-text">
                        <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>idc</th>
                                        <th>nombre</th>
                                        <th>hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="101">101</td>
                                        <td>bloque 1 materia 1</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="" value="102">102</td>
                                        <td>bloque 1 materia 2</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="103">103</td>
                                        <td>bloque 1 materia 3</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="104">104</td>
                                        <td>bloque 1 materia 4</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="105">105</td>
                                        <td>bloque 1 materia 5</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="106">106</td>
                                        <td>bloque 1 materia 6</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit">ok</button>
                        </form>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Bloque 1</h3>
                        <p class="card-text">
                        <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>idc</th>
                                        <th>nombre</th>
                                        <th>hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="101">101</td>
                                        <td>bloque 1 materia 1</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="" value="102">102</td>
                                        <td>bloque 1 materia 2</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="103">103</td>
                                        <td>bloque 1 materia 3</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="104">104</td>
                                        <td>bloque 1 materia 4</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="105">105</td>
                                        <td>bloque 1 materia 5</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="106">106</td>
                                        <td>bloque 1 materia 6</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit">ok</button>
                        </form>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Bloque 4</h3>
                        <p class="card-text">
                        <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>idc</th>
                                        <th>nombre</th>
                                        <th>hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="201">201</td>
                                        <td>bloque 2 materia 1</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="202">202</td>
                                        <td>bloque 2 materia 2</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="203">203</td>
                                        <td>bloque 2 materia 3</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="204">204</td>
                                        <td>bloque 2 materia 4</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="205">205</td>
                                        <td>bloque 2 materia 5</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="206">206</td>
                                        <td>bloque 2 materia 6</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit">ok</button>
                        </form>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs|sm|md|lg|xl-1-12" style="padding-top: 20px;">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Bloque 4</h3>
                        <p class="card-text">
                        <form method="post" action="<?php echo base_url('Estudiantes/agregarbloque') ?>">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>idc</th>
                                        <th>nombre</th>
                                        <th>hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="201">201</td>
                                        <td>bloque 2 materia 1</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="202">202</td>
                                        <td>bloque 2 materia 2</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="203">203</td>
                                        <td>bloque 2 materia 3</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="204">204</td>
                                        <td>bloque 2 materia 4</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="205">205</td>
                                        <td>bloque 2 materia 5</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><input type="hidden" name="idgrupo[]" value="206">206</td>
                                        <td>bloque 2 materia 6</td>
                                        <td> 12:00 a 13:00 </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit">ok</button>
                        </form>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>